$(function () {
	var active_tab = $('#page').val();

	if (active_tab == 'index') {
		$('#fabric_report_daily_cutting_instruction_table').DataTable({
			dom: 'Bfrtip',
			processing: true,
			serverSide: true,
			pageLength: 100,
			scroller: true,
			deferRender: true,
			ajax: {
				type: 'GET',
				url: '/fabric/report/daily-cutting-instruction/data',
				data: function (d) {
					return $.extend({}, d, {
						"warehouse": $('#select_warehouse').val(),
						"start_date": $('#start_date').val(),
						"end_date": $('#end_date').val(),
					});
				}
			},
			columns: [{
					data: 'id',
					name: 'id',
					searchable: true,
					visible: false,
					orderable: false
				},
				{
					data: 'planning_date',
					name: 'planning_date',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'warehouse_id',
					name: 'warehouse_id',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'article_no',
					name: 'article_no',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'style',
					name: 'style',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'po_buyer',
					name: 'po_buyer',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'item_code',
					name: 'item_code',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'uom',
					name: 'uom',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'qty_need',
					name: 'qty_need',
					searchable: false,
					visible: true,
					orderable: false
				},
				{
					data: 'total_relax',
					name: 'total_relax',
					searchable: false,
					visible: true,
					orderable: false
				},
				{
					data: 'total_out',
					name: 'total_out',
					searchable: false,
					visible: true,
					orderable: false
				},
				{
					data: 'action',
					name: 'source',
					searchable: false,
					visible: true,
					orderable: false
				},
			]
		});

		var dtable = $('#fabric_report_daily_cutting_instruction_table').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtable.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtable.search("").draw();
				}
				return;
			});
		dtable.draw();

		$('#select_warehouse').on('change', function () {
			dtable.draw();
		});

		$('#start_date').on('change', function () {
			dtable.draw();
		});

		$('#end_date').on('change', function () {
			dtable.draw();
		});
	} else {
		var url_fabric_report_daily_cutting_instruction_detail = $('#url_fabric_report_daily_cutting_instruction_detail').val();

		$('#fabric_report_daily_cutting_instruction_detail_table').DataTable({
			dom: 'Bfrtip',
			processing: true,
			serverSide: true,
			pageLength: 100,
			scroller: true,
			deferRender: true,
			ajax: {
				type: 'GET',
				url: url_fabric_report_daily_cutting_instruction_detail,
				data: function (d) {
					return $.extend({}, d, {
						"style": $('#style').val(),
						"article_no": $('#article_no').val(),
						"warehouse_id": $('#warehouse_id').val(),
						"planning_date": $('#planning_date').val(),
					});
				}

			},
			columns: [{
					data: 'TANGGAL_PREPARE',
					name: 'TANGGAL_PREPARE',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'PIC',
					name: 'PIC',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'last_movement_date',
					name: 'last_movement_date',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'last_user_movement',
					name: 'last_user_movement',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'last_status_movement',
					name: 'last_status_movement',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'user_receive_cutting_id',
					name: 'user_receive_cutting_id',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'date_receive_cutting',
					name: 'date_receive_cutting',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'piping',
					name: 'piping',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'NO_PO_SUPPLIER',
					name: 'NO_PO_SUPPLIER',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'KODE_ITEM',
					name: 'KODE_ITEM',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'WARNA',
					name: 'WARNA',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'NOMOR_ROLL',
					name: 'NOMOR_ROLL',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'AKTUAL_LOT',
					name: 'AKTUAL_LOT',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'TOP_WIDTH',
					name: 'TOP_WIDTH',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'MIDDLE_WIDTH',
					name: 'MIDDLE_WIDTH',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'BOTTOM_WIDTH',
					name: 'BOTTOM_WIDTH',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'AKTUAL_WIDTH',
					name: 'AKTUAL_WIDTH',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'QTY_RESERVED',
					name: 'QTY_RESERVED',
					searchable: false,
					visible: true,
					orderable: true
				},
			]
		});

		var dtable = $('#fabric_report_daily_cutting_instruction_detail_table').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtable.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtable.search("").draw();
				}
				return;
			});
		dtable.draw();
	}
});