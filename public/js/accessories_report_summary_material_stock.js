$(function()
{
    $('#select_warehouse').on('change',function(){
        $('#_warehouse_id').val($(this).val());
        $('#_detail_warehouse_id').val($(this).val());
    });

    var page = $('#page').val();
    if(page == 'index')
    {
        $('#select_warehouse').trigger('change');
        var summaryStockTable = $('#summary_stock_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            scroller:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/accessories/report/material-summary-stock/data',
                data: function(d) {
                    return $.extend({}, d, {
                        "warehouse"     : $('#select_warehouse').val(),
                    });
            }
            },
            fnCreatedRow: function (row, data, index) {
                var info = summaryStockTable.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'warehouse', name: 'warehouse',searchable:true,orderable:false},
                {data: 'item_id', name: 'item_id',searchable:true,visible:false,orderable:false},
                {data: 'item_code', name: 'item_code',searchable:true,orderable:true},
                {data: 'category', name: 'category',searchable:true,orderable:true},
                {data: 'uom', name: 'uom',searchable:true,visible:true,orderable:true},
                {data: 'total_per_item', name: 'total_per_item',searchable:true,visible:true,orderable:true},
                {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
            
            ]
        });

        var dtable = $('#summary_stock_table').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();
        
        $('#select_warehouse').on('change',function(){
            dtable.draw();
        });

    }
    else
    {
        var url_material_stock_detail_data  = $('#url_material_stock_detail_data').val();
        var item_id                         = $('#item_id').val();
        var warehouse_id                    = $('#warehuose_id').val();
    
        var acccDetailStockTable =  $('#detail_stock_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:-1,
            scroller:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: url_material_stock_detail_data,
                data: function(d) {
                    return $.extend({}, d, {
                        item_id : item_id,
                        warehouse_id : warehouse_id,
                    });
               }
               
            },
            fnCreatedRow: function (row, data, index) {
                var info = acccDetailStockTable.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
    
                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                // computing column Total of the complete result
                var total = api
                    .column( 8 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
    
                //$('#balance').text(total_balance);
                // Update footer by showing the total with the reference of the column index
                $( api.column( 7 ).footer() ).html('Total');
                $( api.column( 8 ).footer() ).html(total.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'warehouse', name: 'warehouse',searchable:true,orderable:false},
                {data: 'locator', name: 'locator',searchable:true,orderable:false},
                {data: 'item_id', name: 'item_id',searchable:true,visible:false,orderable:false},
                {data: 'item_code', name: 'item_code',searchable:true,orderable:true},
                {data: 'document_no', name: 'document_no',searchable:true,orderable:true},
                {data: 'category', name: 'category',searchable:true,orderable:true},
                {data: 'uom', name: 'uom',searchable:true,visible:true,orderable:true},
                {data: 'qty', name: 'qty',searchable:true,visible:true,orderable:true},
               
            ]
        });
    
        var dtable = $('#detail_stock_table').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();
    }
    
});