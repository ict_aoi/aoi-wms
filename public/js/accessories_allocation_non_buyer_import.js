list_allocations = JSON.parse($('#allocation_non_po_buyers').val());

$(function()
{
	render();
});

$('#select_warehouse').on('change',function(){
	$('#warehouse_id').val($(this).val());
});

function render() 
{
	$('#allocation_non_po_buyers').val(JSON.stringify(list_allocations));
	var tmpl = $('#allocation-non-buyer-upload-table').html();
	Mustache.parse(tmpl);
	var data = { item: list_allocations };
	var html = Mustache.render(tmpl, data);
	$('#tbody-upload-allocation-non-buyer').html(html);
}

$('#upload_button').on('click', function () 
{
	$('#upload_file').trigger('click');
});

$('#upload_file_allocation').on('change', function () 
{
	//$('#upload_file_allocation').submit();
	$.ajax({
		cache: false,
		type: "post",
		url: $('#upload_file_allocation').attr('action'),
		data: new FormData(document.getElementById("upload_file_allocation")),
		processData: false,
		contentType: false,
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) {
			$("#alert_info").trigger("click", 'Upload successfully.');
			list_allocations = response;
		},
		error: function (response) {
			$.unblockUI();
			$('#upload_file_allocation').trigger('reset');

			if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT.');
			if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);

		}
	})
	.done(function (response) {
		$('#upload_file_allocation').trigger('reset');
		render();
	});

})



