
$(function()
{
	list_new_auto_allocations = JSON.parse($('#new_auto_allocations').val());
	render();
});

$('#upload_button').on('click', function () 
{
	$('#upload_file').trigger('click');
});

$('#upload_file').on('change', function () 
{
	$.ajax({
		type: "post",
		url: $('#upload_file_allocation').attr('action'),
		data: new FormData(document.getElementById("upload_file_allocation")),
		processData: false,
		contentType: false,
		beforeSend: function () {
			list_new_auto_allocations = [];
			$.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) {
			$("#alert_info").trigger("click", 'Upload delete successfully.');
			for(idx in response){
				var input = response[idx];
				list_new_auto_allocations.push(input);
			}
		},
		error: function (response) {
			$.unblockUI();
			$('#upload_file_allocation').trigger('reset');
			if (response['status'] == 500)
				$("#alert_error").trigger("click", 'Please Contact ICT.');

			if (response['status'] == 422)
				$("#alert_error").trigger("click", response.responseJSON);

		}
	})
	.done(function () {
		$('#upload_file_allocation').trigger('reset');
		render();
	});

})

function render() 
{
	getIndex();
	$('#new_auto_allocations').val(JSON.stringify(list_new_auto_allocations));
	var tmpl = $('#auto-allocation-table').html();
	Mustache.parse(tmpl);
	var data = { item: list_new_auto_allocations };
	var html = Mustache.render(tmpl, data);
	$('#tbody-auto-allocation').html(html);
}

function getIndex() 
{
	for (idx in list_new_auto_allocations) {
		list_new_auto_allocations[idx]['_id'] = idx;
		list_new_auto_allocations[idx]['no'] = parseInt(idx) + 1;
	}
}
