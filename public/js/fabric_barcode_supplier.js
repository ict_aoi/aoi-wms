$('#documentNoSearch').unbind();
$('#itemCodeSearch').unbind();
$('#suratJalanSearch').unbind();
$('#noInvoiceSearch').unbind();

$(window).on('hashchange', function() 
{
	if (window.location.hash) {
		var page = window.location.hash.replace('#', '');
		if (page == Number.NaN || page <= 0) {
			return false;
		}else
		{
			var document_no 	= $('#documentNoSearch').val();
			var item_code 		= $('#itemCodeSearch').val();
			var surat_jalan 	= $('#suratJalanSearch').val();
			var no_invoice 		= $('#noInvoiceSearch').val();
			var warehouse_id	= $('#select_warehouse').val();
			
			getData(page,document_no,item_code,surat_jalan,no_invoice,warehouse_id);
		}
	}
});

$(document).ready(function()
{
	$(document).on('click', '.pagination a',function(event){
		$('li').removeClass('active');
		$(this).parent('li').addClass('active');
		event.preventDefault();


		var myurl 			= $(this).attr('href');
		var document_no 	= $('#documentNoSearch').val();
		var item_code 		= $('#itemCodeSearch').val();
		var surat_jalan 	= $('#suratJalanSearch').val();
		var no_invoice 		= $('#noInvoiceSearch').val();
		var warehouse_id	= $('#select_warehouse').val();
		var page			= $(this).attr('href').split('page=')[1];
		
		getData(page,document_no,item_code,surat_jalan,no_invoice,warehouse_id);
	});
});

$('#select_warehouse').on('change',function(){
	$('#_warehouse_id').val($(this).val());
	$('#warehouse_id').val($(this).val());
});

$('#ButtonSrc').on('click', function(e)
{
	var document_no 	= $('#documentNoSearch').val();
	var item_code 		= $('#itemCodeSearch').val();
	var surat_jalan 	= $('#suratJalanSearch').val();
	var no_invoice 		= $('#noInvoiceSearch').val();
	var warehouse_id	= $('#select_warehouse').val();
	var page 			= window.location.hash.replace('#', '');

	getData(page,document_no,item_code,surat_jalan,no_invoice,warehouse_id);
});

$('#documentNoSearch').on('keypress', function (e) 
{
	var document_no 	= $('#documentNoSearch').val();
	var item_code 		= $('#itemCodeSearch').val();
	var surat_jalan 	= $('#suratJalanSearch').val();
	var no_invoice		= $('#noInvoiceSearch').val();
	var warehouse_id	= $('#select_warehouse').val();
	var page 			= window.location.hash.replace('#', '');

	if (e.keyCode == 13)
		getData(page,document_no,item_code,surat_jalan,no_invoice,warehouse_id);
});

$('#itemCodeSearch').on('keypress', function (e) 
{
	var document_no 	= $('#documentNoSearch').val();
	var item_code 		= $('#itemCodeSearch').val();
	var surat_jalan 	= $('#suratJalanSearch').val();
	var no_invoice 		= $('#noInvoiceSearch').val();
	var warehouse_id	= $('#select_warehouse').val();
	var page 			= window.location.hash.replace('#', '');

	if (e.keyCode == 13)
		getData(page,document_no,item_code,surat_jalan,no_invoice,warehouse_id);
});

$('#suratJalanSearch').on('keypress', function (e) 
{
	var document_no 	= $('#documentNoSearch').val();
	var item_code 		= $('#itemCodeSearch').val();
	var surat_jalan 	= $('#suratJalanSearch').val();
	var no_invoice 		= $('#noInvoiceSearch').val();
	var warehouse_id	= $('#select_warehouse').val();
	var page 			= window.location.hash.replace('#', '');

	if (e.keyCode == 13)
		getData(page,document_no,item_code,surat_jalan,no_invoice,warehouse_id);
});

$('#noInvoiceSearch').on('keypress', function (e) 
{
	var document_no 	= $('#documentNoSearch').val();
	var item_code 		= $('#itemCodeSearch').val();
	var surat_jalan 	= $('#suratJalanSearch').val();
	var no_invoice 		= $('#noInvoiceSearch').val();
	var warehouse_id	= $('#select_warehouse').val();
	var page 			= window.location.hash.replace('#', '');

	if (e.keyCode == 13)
		getData(page,document_no,item_code,surat_jalan,no_invoice,warehouse_id);
});

function chooseItem()
{
	var id 				= $(this).data('id');
	var document 		= $(this).data('documentno');
	var packing_list 	= $(this).data('packinglist');
	var item 			= $(this).data('item');
	var no_invoice 		= $(this).data('invoice');
	var is_exists 		= $(this).data('exists');
	var qty 			= $(this).data('qty');
	var uom 			= $(this).data('uom');
	var no_surat_jalan 	= $(this).data('sj');
	var is_available 	= isAvailable(id);

	$('#document_no').val(document);
	$('#no_packing_list').val(packing_list);
	$('#item_code').val(item);
	$('#no_invoice').val(no_invoice);
	
	if(is_available > 0)
	{
		$("#alert_info").trigger("click", 'Data has ready to print.');
		return false;
	}

	var input = {
		'po_detail_id'		: id,
		'document_no'		: document,
		'no_packing_list'	: packing_list,
		'item_code'			: item,
		'no_invoice'		: no_invoice,
		'no_surat_jalan'	: no_surat_jalan
	};
	list_data_print.push(input);

	var summary = {
		'document_no'	: document,
		'item_code'		: item,
		'no_invoice'	: no_invoice,
		'qty' 			: qty,
		'qty_format' 	: parseFloat(qty).toFixed(2),
		'uom'			: uom,
		'no_surat_jalan': packing_list
	};

	var difSummary = checkSummary(document, item, no_invoice,packing_list, qty);
	if (difSummary) 
	{
		list_data_summary.push(summary);
	}
	
	

	$('#'+id+'_UnSelect').removeClass('hidden');
	$('#'+id+'_Select').addClass('hidden');
	$('#data_print').val(JSON.stringify(list_data_print));
	drawEachLine(id);
	render();
}

function checkSummary(document, item, no_invoice,no_surat_jalan, qty) 
{
	for (var i in list_data_summary) {
		var data = list_data_summary[i];
		
		if (data.document_no == document && data.item_code == item && data.no_invoice == no_invoice && data.no_surat_jalan == no_surat_jalan){
			var qty_curr = parseFloat(data.qty);
			data.qty = parseFloat(qty_curr + parseFloat(qty));
			data.qty_format = parseFloat(qty_curr + parseFloat(qty)).toFixed(2);
			return false;
		}
			
	}

	return true;
}

function unchooseItem()
{
	var id 				= $(this).data('id');
	var document 		= $(this).data('documentno');
	var packing_list 	= $(this).data('packinglist');
	var item 			= $(this).data('item');
	var no_invoice 		= $(this).data('invoice');
	var is_exists 		= $(this).data('exists');
	var qty 			= $(this).data('qty');
	var uom 			= $(this).data('uom');
	var no_surat_jalan 	= $(this).data('sj');

	for (var idx in list_data_print) 
	{
		var data = list_data_print[idx];
		if (id == data.po_detail_id) {
			list_data_print.splice(idx, 1);
			if(is_exists){
				$('#'+data.po_detail_id).css('background-color', '#d9ffde');
			}else{
				$('#'+data.po_detail_id).css('background-color', '#ffffff');
			}
			
		}
	}

	for (var i in list_data_summary) 
	{
		var data = list_data_summary[i];
		
		if (data.document_no == document 
			&& data.item_code == item 
			&& data.no_invoice == no_invoice 
			&& data.no_surat_jalan == packing_list)
			{

			var qty_curr 	= parseFloat(data.qty);
			var new_qty 	= parseFloat(qty_curr - parseFloat(qty),2);

			if(new_qty <=0) list_data_summary.splice(i, 1);

			data.qty 		= new_qty;
			data.qty_format = parseFloat(new_qty).toFixed(2);
		}
			
	}
	
	$('#'+id+'_UnSelect').addClass('hidden');
	$('#'+id+'_Select').removeClass('hidden');
	$('#data_print').val(JSON.stringify(list_data_print));
	render();
}

function getData(page,document_no,item_code,surat_jalan,no_invoice,warehouse_id)
{
	$.ajax({
		url: '?page=' + page +'&document_no=' + document_no + '&item_code=' + item_code + '&no_surat_jalan=' + surat_jalan +'&no_invoice=' + no_invoice+'&warehouse_id=' + warehouse_id,
		type: "get",
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		
	})
	.done(function(data){
		$("#item-lists").empty().html(data['view']);
		$('#item-lists').find('.btn-choose').on('click', chooseItem);
		$('#item-lists').find('.btn-unchoose').on('click', unchooseItem);
		location.hash = page;
		draw(data['data']);
	}).fail(function(jqXHR, ajaxOptions, thrownError){
		alert('No response from server');
	});
}

function draw(data){
	for (var id in data.data) {
		var _data = data.data[id];
		for (var idx in list_data_print){
			var list_data = list_data_print[idx];
			if (_data.po_detail_id == list_data.po_detail_id){
				$('#'+_data.po_detail_id).css('background-color', '#ffcc66');
				$('#'+_data.po_detail_id+'_UnSelect').removeClass('hidden');
				$('#'+_data.po_detail_id+'_Select').addClass('hidden');
			}
		}
	}
	
}

function drawEachLine(po_detail_id) {
	for (var idx in list_data_print) {
		var data = list_data_print[idx];
		if (po_detail_id == data.po_detail_id) {
			$('#'+data.po_detail_id).css('background-color', '#ffcc66');
		}
	}

}

function isAvailable(po_detail_id){
	var flag = 0;
	for (var idx in list_data_print) {
		var data = list_data_print[idx];
		if (po_detail_id == data.po_detail_id) {
			flag++;
		}
	}
	return flag;
}


list_data_summary = JSON.parse($('#data_summary').val());
list_data_print = JSON.parse($('#data_print').val());

function render() 
{
	$('#data_print').val(JSON.stringify(list_data_print));
	$('#data_summary').val(JSON.stringify(list_data_summary));
	var tmpl = $('#summary-table').html();
	Mustache.parse(tmpl);
	var data = { item: list_data_summary };
	var html = Mustache.render(tmpl, data);
	$('#tbody-summary').html(html);
}

render();

$('#form').submit(function (event) {
	var url_reprint_index = $('#url_reprint_index').attr('href');
	event.preventDefault();
	
	if(list_data_print.length == 0){
		$("#alert_warning").trigger("click", 'Please select material first.');
		return false;
	}
	bootbox.confirm("Are you sure want to print this ?", function (result) {
		if (result) {
			$.ajax({
				type: "POST",
				url: $('#form').attr('action'),
				data: $('#form').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				complete: function () {
					$.unblockUI();
				},
				error: function (response) {
					$.unblockUI();
					
					if (response['status'] == 500)
						$("#alert_error").trigger("click", 'Please contact ICT');
					
				}
			})
			.done(function (response){
				$('#list_barcodes').val(JSON.stringify(response));
				$('#getBarcode').submit();
				$('#list_barcodes').val('');
				$('#form').trigger('reset');

				

				list_data_summary = [];
				list_data_print = [];
				$("#item-lists").empty().html('<center><h1>Please search based on criteria above</h1></center>');
				$('#documentNoSearch').val('');
				$('#itemCodeSearch').val('');
				$('#suratJalanSearch').val('');
				$('#noInvoiceSearch').val('');

				render();
			});
		}
	});



});