$(function () {
    $('#select_warehouse').trigger('change');
    $('#fabric_report_material_stock_opname_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength: 100,
        scroller: true,
        deferRender: true,
        ajax: {
            type: 'GET',
            url: '/fabric/report/material-stock-opname/data',
            data: function (d) {
                return $.extend({}, d, {
                    "warehouse": $('#select_warehouse').val(),
                    "start_date": $('#start_date').val(),
                    "end_date": $('#end_date').val(),
                });
            }
        },
        columns: [{
                data: 'id',
                name: 'id',
                searchable: true,
                visible: false,
                orderable: false
            },
            {
                data: 'created_at',
                name: 'created_at',
                searchable: false,
                visible: true,
                orderable: true
            },
            {
                data: 'name',
                name: 'name',
                searchable: false,
                visible: true,
                orderable: true
            },
            {
                data: 'warehouse_name',
                name: 'warehouse_name',
                searchable: false,
                visible: true,
                orderable: true
            },
            {
                data: 'document_no',
                name: 'document_no',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'supplier_code',
                name: 'supplier_code',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'supplier_name',
                name: 'supplier_name',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'no_invoice',
                name: 'no_invoice',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'nomor_roll',
                name: 'nomor_roll',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'item_code',
                name: 'item_code',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'color',
                name: 'color',
                searchable: false,
                visible: true,
                orderable: true
            },
            {
                data: 'uom',
                name: 'uom',
                searchable: false,
                visible: true,
                orderable: false
            },
            {
                data: 'available_stock_old',
                name: 'available_stock_old',
                searchable: false,
                visible: true,
                orderable: false
            },
            {
                data: 'available_stock_new',
                name: 'available_stock_new',
                searchable: false,
                visible: true,
                orderable: false
            },
            {
                data: 'old_locator',
                name: 'old_locator',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'new_locator',
                name: 'new_locator',
                searchable: true,
                visible: true,
                orderable: true
            },
        ]
    });

    var dtable = $('#fabric_report_material_stock_opname_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
        });
    dtable.draw();

    $('#select_warehouse').on('change', function () {
        dtable.draw();
    });
    $('#start_date').on('change', function () {
        dtable.draw();
    });

    $('#end_date').on('change', function () {
        dtable.draw();
    });
});