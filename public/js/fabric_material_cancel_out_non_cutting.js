list_items = JSON.parse($('#items').val());

$(function () 
{
  render();
  setFocusToTextBox();

  $('#form').submit(function (event) 
  {
    event.preventDefault();
    if (list_items.length == 0) 
    {
      $("#alert_warning").trigger("click", 'Please scan barcode first.');
      setFocusToTextBox();
      return false;
    }

    bootbox.confirm("Are you sure want to save this data ?", function (result) 
    {
      if (result) 
      {
        $.ajax({
          type: "POST",
          url: $('#form').attr('action'),
          data: $('#form').serialize(),
          beforeSend: function () {
            setFocusToTextBox();
            $.blockUI({
              message: '<i class="icon-spinner4 spinner"></i>',
              overlayCSS: {
                  backgroundColor: '#fff',
                  opacity: 0.8,
                  cursor: 'wait'
              },
              css: {
                  border: 0,
                  padding: 0,
                  backgroundColor: 'transparent'
              }
          });
          },
          complete: function () {
            $.unblockUI();
          },
          success: function () {
            list_items = [];
          },
          error: function (response) 
          {
            $.unblockUI();

            render();
            setFocusToTextBox();
            if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');


            if (response['status'] == 422)$("#alert_error").trigger("click", response.responseJSON);
          }
        })
        .done(function () 
        {
            render();
            setFocusToTextBox();
            $("#alert_success").trigger("click", 'Data successfully canceled');
        });
      }
    });

  });
});

$('#select_warehouse').on('change',function(){
	$('#warehouse_id').val($(this).val());
});

function render() 
{
  getIndex();
  $('#items').val(JSON.stringify(list_items.sort()));
  var tmpl = $('#material-cancel-out-handover-fabric-table').html();
  Mustache.parse(tmpl);
  var data = { list: list_items };
  var html = Mustache.render(tmpl, data);
  $('#tbody-material-cancel-out-handover-fabric').html(html);
  bind();
}

function getIndex() 
{
  for (id in list_items) {
    list_items[id]['_id'] = id;
    list_items[id]['no']  = parseInt(id)+1;
  }
}

function bind() 
{
  $('#barcode_value').on('change', tambahItem);
  $('.btn-delete-item').on('click', deleteItem);
}

function deleteItem() 
{
  var i = $(this).data('id');
  list_items.splice(i, 1);
  render();
}

function checkBarcode(barcode) 
{
  var flag = 0;
  for (idx in list_items) {
    var data = list_items[idx];

    if (data.barcode == barcode) {
      flag++;
      break;
    }

  }
  return flag;
}

function tambahItem()
{
  var warehouse_id                                        = $('#select_warehouse').val();
  var barcode                                             = $('#barcode_value').val();
  var url_fabric_material_cancel_out_non_cutting_create   = $('#url_fabric_material_cancel_out_non_cutting_create').val();
  var check_barcode                                       = checkBarcode(barcode);

  if (check_barcode > 0) 
  {
    $("#alert_warning").trigger("click", 'Barcode already scanned.');
    $('#barcode_value').val('');
    setFocusToTextBox();
    return false;
  }

  $.ajax({
    type: "GET",
    url: url_fabric_material_cancel_out_non_cutting_create,
    data: {
      barcode       : barcode,
      warehouse_id  : warehouse_id,
    },
    beforeSend: function () 
    {
      $.blockUI({
        message: '<i class="icon-spinner4 spinner"></i>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'transparent'
        }
    });
    },
    complete: function () 
    {
      $.unblockUI();
    },
    success: function (response) {
      list_items.push(response);
    },
    error: function (response) 
    {
      $.unblockUI();

      render();
      setFocusToTextBox();

      if (response['status'] == 500) $("#alert_error").trigger('click', 'Please contact ICT');
      if (response['status'] == 422) $("#alert_warning").trigger('click', response.responseJSON);
    }
  })
  .done(function () {
    render();
    setFocusToTextBox();
    
  });
}

function setFocusToTextBox()
{
  $('#barcode_value').val('');
  $('#barcode_value').focus();
}