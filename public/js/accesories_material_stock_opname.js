list_barcodes = JSON.parse($('#barcode_products').val());

$(function () 
{
	setFocusToTextBox();
	render();

	$('#form').submit(function (event) 
	{
		event.preventDefault();
		bootbox.confirm("Are you sure want to save this data ?.", function (result) 
		{
			if (result) {
				$.ajax({
					type: "POST",
					url: $('#form').attr('action'),
					data: $('#form').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function () {
						$('#barcode_value').val('');
						$('#form').trigger("reset");
						list_barcodes = [];
					},
					error: function (response) {
						$.unblockUI();
						$('#barcode_value').val('');
						setFocusToTextBox();
					
						if (response['status'] == 500)
							$("#alert_error").trigger("click", 'Please Contact ICT');
						
						
						if (response['status'] == 422)
							$("#alert_warning").trigger("click", response.responseJSON);

					}
				})
				.done(function (){
					$("#alert_success").trigger("click", 'Data successfully saved.');
					setFocusToTextBox();
					render();
				});
			}
		});
	});
});

function render() 
{
	getIndex();
	$('#barcode_products').val(JSON.stringify(list_barcodes));
	var tmpl = $('#accessories_material_stock_opname_table').html();
	Mustache.parse(tmpl);
	var data = { list: list_barcodes };
	var html = Mustache.render(tmpl, data);
	$('#tbody_accessories_material_stock_opname').html(html);
	setFocusToTextBox();
	bind();
}

function bind() 
{
	$('#barcode_value').on('change', tambahItem);
}

function getIndex() 
{
	for (idx in list_barcodes) 
	{
		list_barcodes[idx]['_id'] = idx;
		list_barcodes[idx]['no'] = parseInt(idx) + 1;
	}
}

function checkItem(barcode)
{
	var has_dash = barcode.indexOf("-");
	var _barcode = '';
	var is_exist = 0;

	if (has_dash != -1) 
	{
		_barcode = barcode.split("-")[0];
	} else {
		_barcode = barcode;
	}

	for (var i in list_barcodes)
	{
		var data = list_barcodes[i];
		if (data.barcode.trim() == _barcode.trim())
		{
			is_exist++;
		}
	}

	return is_exist;

}

function tambahItem()
{
	var url_sto_create_accessories 	= $('#url_sto_create_accessories').val();
	var barcode 					= $('#barcode_value').val();
	
	if (!barcode) 
	{
		$("#alert_warning").trigger("click", 'Please scan barcode first. (Silahkan scan barcode terlebih dahulu)');
		return;
	}

	var diff = checkItem(barcode);
	if (diff > 0) 
	{
		$("#barcode_value").val("");
		$(".barcode_value").focus();
		$("#alert_error").trigger("click", 'Barcode already scanned.');
		return;
	}

	$.ajax({
		type: "GET",
		url: url_sto_create_accessories,
		data: {
			barcode: barcode
		},
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) {
			list_barcodes.push(response);
		},
		error: function (response) {
			$.unblockUI();
			$('#barcode_value').val('');
			$('.barcode_value').focus();

			if (response['status'] == 500)
				$("#alert_error").trigger('click', 'Please contact ICT');

			if (response['status'] == 422)
				$("#alert_error").trigger('click', response.responseJSON);

		}
	})
	.done(function () {
		render();
	});
}

function setFocusToTextBox() 
{
	$('#barcode_value').val('');
	$('#barcode_value').focus();
}