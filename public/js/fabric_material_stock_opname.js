list_barcodes = JSON.parse($('#barcode_products').val());


$(function () 
{
	$('.barcode_value').focus();
	setFocusToTextBox();
	render();

	var warehouse_id = $('#select_warehouse').val();
	locatorPicklist('locator_in_fabric',warehouse_id, '/fabric/material-stock-opname/locator-picklist?');
	$('#form').submit(function (event) 
	{
		var check = checkRemark();

		if(check)
		{
			$("#alert_warning").trigger("click", 'Please check remark, some remarks are still empty');
			return false;
		}

		event.preventDefault();
		bootbox.confirm("Are you sure want to save this data ?.", function (result) 
		{
			if (result) 
			{
				$.ajax({
					type: "POST",
					url: $('#form').attr('action'),
					data: $('#form').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function () {
						$('#barcode_value').val('');
						$('#form').trigger("reset");
						list_barcodes = [];
					},
					error: function (response) {
						$.unblockUI();
						$('#barcode_value').val('');
						setFocusToTextBox();
					
						if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
						if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);

					}
				})
				.done(function (){
					$("#alert_success").trigger("click", 'Data successfully saved.');
					setFocusToTextBox();
					render();
				});
			}
		});



	});
});

$('#select_warehouse').on('change',function(){
	var warehouse_id = $(this).val();
	$('#warehouse_id').val(warehouse_id);
	locatorPicklist('locator_in_fabric',warehouse_id, '/fabric/material-stock-opname/locator-picklist?');
});


$('#btn_check_all').click(function () 
{
	var _selectAll = $('#_selectAll').val();
	if (_selectAll == 0) 
	{
		$('#_selectAll').val('1');
		$("#btn_check_all").css("background-color", '#ffd400');
		$("#btn_check_all").css("color", 'white');
		$("#btn_check_all").css("border-color", '#ffd400');
		checked();

	}else 
	{
		$('#_selectAll').val('0');
		$("#btn_check_all").css("background-color", '#f5f5f5');
		$("#btn_check_all").css("color", 'black');
		$("#btn_check_all").css("border-color", '#f5f5f5');
		checked();
	}
});

$('#remark_all').change(function()
{
	var remark = $(this).val();
	for (idx in list_barcodes) 
	{
		var data = list_barcodes[idx];
		data.remark = remark;
	}
	render();
});

function checkedItem()
{
	var i 			= $(this).data('id');
	var _selectAll 	= $('#_selectAll').val();
	var data 		= list_barcodes[i];

	if (_selectAll == 1) 
	{
		if ($('#check_' + data.id).prop('checked') == true)
		{
			data.sto_note = 'BAPB';
			data.check_all = true;
			data.is_exclude	= false;
			BAPB();
		}else if ($('#check_' + data.id).prop('checked') == false)
		{
			data.is_exclude	= true;
			data.check_all 	= false;
			var available_qty 		= data.available_qty_on_db;
			var _adj_stock			= data.adjustment_stock ;
			
			if(_adj_stock) adj_stock = _adj_stock;
			else adj_stock = available_qty;

			$('#adjStockInput_' + data.id).val(adj_stock).trigger('change');
		}
	}
		

	render();
}

function checked() 
{
	var _selectAll = $('#_selectAll').val();
	for (var i in list_barcodes) 
	{
		var data = list_barcodes[i];
		if(!data.is_exclude)
		{
			if (_selectAll == 1) 
			{
				$('#check_' + i).prop('checked', true);
				data.check_all = true;
				BAPB();
			}else 
			{
				$('#check_' + i).prop('checked', false);
				data.check_all = false;
				BAPB();
				
			}
		}
	}
}

function BAPB() 
{
	var _selectAll = $('#_selectAll').val();
	for (var i in list_barcodes) 
	{
		var data = list_barcodes[i];
		if(!data.is_exclude)
		{
			if (_selectAll == 1) 
			{
				$('#adjStockInput_' + data.id).val('0').trigger('change');
			}else 
			{
				var available_qty 		= data.available_qty_on_db;
				var _adj_stock			= data.adjustment_stock ;
				
				if(_adj_stock) adj_stock = _adj_stock;
				else adj_stock = available_qty;
				
				$('#adjStockInput_' + data.id).val(adj_stock).trigger('change');
				
				if ($('#check_' + data.id).prop('checked') == true) 
				{
					
				}
			}
		}
		
	}

}

function checkBAPB(data)
{
	var _selectAll = $('#_selectAll').val();
	if(!data.is_exclude)
	{
		if (_selectAll == 1) 
		{
			$('#adjStockInput_' + data.id).val('0').trigger('change');
		}else 
		{
			var available_qty 		= data.available_qty_on_db;
			var _adj_stock			= data.adjustment_stock ;
			
			if(_adj_stock) adj_stock = _adj_stock;
			else adj_stock = available_qty;
			
			console.log(adj_stock);
			console.log(data.id);
			$('#adjStockInput_' + data.id).val(adj_stock).trigger('change');
			
			
		}
	}
}

function removeLastUsed(id) 
{
	var url_remove_last_user = $('#url_remove_last_user').val();
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$.ajax({
		type: "put",
		url: url_remove_last_user,
		data: {
			id: id
		}
	});
}

function render() 
{
	getIndex();
	$('#barcode_products').val(JSON.stringify(list_barcodes));
	var tmpl = $('#material-sto-table').html();
	Mustache.parse(tmpl);
	var data = { list: list_barcodes };
	var html = Mustache.render(tmpl, data);
	$('#tbody-material-sto-fabric').html(html);
	setFocusToTextBox();
	bind();
}

function getIndex() 
{
	for (idx in list_barcodes) 
	{
		list_barcodes[idx]['_id'] 	= idx;
		list_barcodes[idx]['no'] 	= parseInt(idx) + 1;
	}
}

function checkItem(barcode, index) 
{
	for (var i in list_barcodes) 
	{
		var data = list_barcodes[i];

		if (i == index)
			continue;
		
		if (data.barcode_supplier == barcode.toUpperCase()) {
			return false;
		}

	}

	return true;
}

function bind() 
{
	$('.btn-delete-item').on('click', deleteItem);
	$('#barcode_value').on('change', tambahItem);
	$('.checkbox_item').on('click', checkedItem);
	$('.adj-stock').on('change', changeStock);
	$('.remark-user').on('change', changeRemark);

	$('.input-number').keypress(function (e) 
	{

		if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
			return false;
		}
		return true;
	});
}

function changeStock()
{
	var i 					= $(this).data('id');
	var data 				= list_barcodes[i];
	var available_qty_on_db = parseFloat(data.available_qty_on_db);
	var adj_stock 			= $('#adjStockInput_'+data.id).val();
	var check_all 			= data.check_all;
	var _adj_stock			= data.adjustment_stock ;
	var _sto_note			= data.sto_note ;

	
	if (adj_stock != '')
	{
		data.adjustment_stock = adj_stock;
		if (adj_stock == 0)
		{
			data.available_qty = 0;
			
			if (check_all) data.sto_note = 'BAPB';
			else data.sto_note = 'STOK HABIS';

		}else
		{
			var diff 				= available_qty_on_db - parseFloat(adj_stock);
			var new_available_qty 	= available_qty_on_db - diff;
			data.available_qty 		= new_available_qty;
					
			if (diff > 0)
			{
				data.sto_note = 'STOK BERKURANG ' + diff;
			}else
			{
				
				data.is_increase = true;
				data.available_qty = new_available_qty;
				data.sto_note = 'STOK BERTAMBAH ' + (-1 * diff);
			}
		}
		
	}else
	{
		data.adjustment_stock 	= _adj_stock;
		data.available_qty 		= available_qty_on_db;
		data.sto_note 			= _sto_note
	}

	render();
	setFocusToTextBox();
}

function changeRemark()
{
	var i 			= $(this).data('id');
	var remark_sto 	= $('#remarkInput_'+i).val();
	var data 		= list_barcodes[i];
	
	data.remark 	= remark_sto;
	render();
}

function deleteItem()
{
	var i 		= $(this).data('id');
	var data 	= list_barcodes[i];
	removeLastUsed(data.id);
	list_barcodes.splice(i, 1);
	render();
}

function tambahItem()
{
	var warehouse_id 								= $('#select_warehouse').val();
	var barcode 									= $('#barcode_value').val();
	var url_fabric_material_stock_opname_create 	= $('#url_fabric_material_stock_opname_create').val();
	var type										= $('#select_type').val();

	if (!barcode)
	{
		$("#alert_warning").trigger("click", 'Please scan barcode first.');
		return;
	}
	var diff_dup_item = checkItem(barcode);

	if (!diff_dup_item) 
	{
		$('#barcode_value').val('');
		$('.barcode_value').focus();
		$("#alert_warning").trigger("click", 'Barcode already scanned.');
		return;
	}

	$.ajax({
		type: "GET",
		url: url_fabric_material_stock_opname_create,
		data: {
			barcode			: barcode,
			warehouse_id	: warehouse_id,
			type			: type,	
		},
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) {
			var _selectAll 	= $('#_selectAll').val();
			var data 		= response['data'];

			if (data)
			{
				var remark_all = $('#remark_all').val();
				if(remark_all != null)
				{
					data.remark = remark_all;
				}

				list_barcodes.push(data);
			}

			
			//BAPB(response.id);

			if (response['locator'])
			{
				var locator_id 		= response['locator']['id'];
				var locator_code 	= response['locator']['code'];
				$("#locator_in_fabricName").val(locator_code);
				$("#locator_in_fabricId").val(locator_id);

			}
			
		},
		error: function (response) 
		{
			$.unblockUI();
			$('#barcode_value').val('');
			$('.barcode_value').focus();
			
			if (response['status'] == 500) $("#alert_error").trigger('click','Please contact ICT');
			if (response['status'] == 422) $("#alert_warning").trigger('click', response.responseJSON);
		}
	})
	.done(function (response)
	{
		render();
		var data 		= response['data'];
		if(!data.is_exclude)
		{
			if (_selectAll == 1) 
			{
				$('#check_' + data.id).prop('checked', true);
				data.check_all = true;
				checkBAPB(data);
			}else 
			{
				$('#check_' + data.id).prop('checked', false);
				data.check_all = false;
				
				checkBAPB(data);
				
			}
		}
		$('#barcode_value').val('');
	});
}

function setFocusToTextBox() 
{
	$('#barcode_value').focus();
}

function checkRemark()
{
	i = 0;
	for (idx in list_barcodes) 
	{
		var data = list_barcodes[idx];
		if(data.remark == null || data.remark == '')
		{
			data.is_error 	= true;
			data.is_sto 	= false;
			i++;
		}else
		{
			data.is_error 	= false;
			data.is_sto 	= data._is_sto;
		}
	}

	render();

	if(i > 0) return true;
	else return false;

}

function locatorPicklist(name,warehouse_id, url) 
{
	var search 		= '#' + name + 'Search';
	var item_id 	= '#' + name + 'Id';
	var item_name 	= '#' + name + 'Name';
	var modal 		= '#' + name + 'Modal';
	var table 		= '#' + name + 'Table';
	var buttonSrc 	= '#' + name + 'ButtonSrc';
	var buttonDel 	= '#' + name + 'ButtonDel';

	function itemAjax()
	{
		var q = $(search).val();
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&q=' + q+ '&warehouse_id=' + warehouse_id
		})
		.done(function (data) {
			$(table).html(data);
			pagination(name);
			$(search).focus();
			$(search).val('');
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem()
	{
		var id 			= $(this).data('id');
		var name 		= $(this).data('name');

		$(item_id).val(id);
		$(item_name).val(name);
	}

	function pagination()
	{
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	itemAjax();
}
