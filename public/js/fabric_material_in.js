list_barcodes = JSON.parse($('#barcode_products').val());

$(function () 
{
	$('.barcode_value').focus();
	setFocusToTextBox();
	render();

	$('#form').submit(function (event) 
	{
		event.preventDefault();
		var check_destination 	= $('#locator_in_fabricId').val();
		var rack 				= $("#locator_in_fabricName").val();
		var check_source 		= hasOneSource();

		if (check_destination == null || check_destination == '') 
		{
			$("#alert_warning").trigger("click", 'Please select locator first.');
			return false;
		}

		if (!check_source) 
		{
			$("#alert_warning").trigger("click", 'One locator only accept one group, either barcode stock group or barcode relax group');
			return false;
		}

		bootbox.confirm("Are you sure want to save material to locator " + rack + " ?.", function (result) 
		{
			if (result) 
			{
				$.ajax({
					type: "POST",
					url: $('#form').attr('action'),
					data: $('#form').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function (response) 
					{
						$('#barcode_value').val('');
						$('#locator_in_fabricName').val('');
						$('#locator_in_fabricId').val('');
						$('.barcode_value').focus();
						
						$('#alert_locator').addClass('hidden');
						$('#form').trigger("reset");


						list_barcodes = [];
						//jika error
						console.log(response.length);
						if (response.length > 0)
						{
							for (var i in response)
							 {
								var data = response[i];
								list_barcodes.push(data);
							}
						}
						
						render();

						if (list_barcodes.length == 0) 
						{
							$('#_from_rackin_id').val('');
							$('#from_rackin_id').val('');
							$('#from_rackinName').val('');

							$("#alert_locator").addClass('hidden');
							$("#alert_out").addClass('hidden');
						}

					},
					error: function (response) 
					{
						$.unblockUI();
						$('#barcode_value').val('');
						$('.barcode_value').focus();
					
						if (response['status'] == 500)
							$("#alert_error").trigger("click", 'Please concatc ICT');
						
						
						if (response['status'] == 422)
							$("#alert_warning").trigger("click", response.responseJSON);

						if (response['status'] == 400){
							for (i in response['responseJSON']['errors']) 
							{
								$('#' + i + '_error').addClass('has-error');
								$('#' + i + '_danger').text(response['responseJSON']['errors'][i]);
							}
						}
					}
				})
				.done(function (response)
				{
					$("#alert_success").trigger("click", 'Data successfully saved.');
				});
			}
		});
	});

	$('#reset_all_checkin').click(function () 
	{
		bootbox.confirm("Are you sure want to reset this ?.", function (result) 
		{
			if (result) {
				var url_reset_temporary = $('#url_temporaryreset').attr('href');
				var token = $('meta[name="csrf-token"]').attr('content');
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
					type: "DELETE",
					url: url_reset_temporary,
					data: {
						status: 'transit'
					},
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					error: function (response) {
						$.unblockUI();
						if (response['status'] == 500) 
							$("#alert_error").trigger("click", 'Please contact ICT');

						if (response['status'] == 422) 
							$("#alert_error").trigger("click", response.responseJSON);


						
					}
				})
				.done(function (response) {
					$('#barcode_value').val('');
					$('#locator_in_fabricName').val('');
					$('#locator_in_fabricId').val('');
					$('.barcode_value').focus();

					$('#form').trigger("reset");
					list_barcodes = [];
					render();
				});
			}
		});
	})
});


$('#locator_in_fabricName').click(function () 
{
	var warehouse_id = $('#select_warehouse').val();
	locatorPicklist('locator_in_fabric',warehouse_id, '/fabric/material-in/locator-picklist?');
});

$('#locator_in_fabricButtonLookup').click(function () 
{
	var warehouse_id = $('#select_warehouse').val();
	locatorPicklist('locator_in_fabric',warehouse_id, '/fabric/material-in/locator-picklist?');
});

$('#select_warehouse').on('change',function(){
	var warehouse_id = $(this).val();
	$('#warehouse_id').val(warehouse_id);

	locatorPicklist('locator_in_fabric',warehouse_id, '/fabric/material-in/locator-picklist?');

	$('#form').trigger('reset');
	$('#alert_locator').addClass('hidden');
	
});

function hasOneSource() 
{
	var last_locator_used 			= $('#last_locator_used').val();
	
	if (last_locator_used) 
	{
		for (var i in list_barcodes) 
		{
			var barcode = list_barcodes[i];

			if(last_locator_used == 'INV' && barcode.is_barcode_preparation_fabric) return false;
			else if (last_locator_used == 'RLX' && !barcode.is_barcode_preparation_fabric) return false;
		}
		return true;
	}else 
	{
		var flag_source_barcode_relax 	= 0;
		var flag_source_barcode_stock 	= 0;

		for (var i in list_barcodes) 
		{
			var barcode = list_barcodes[i];

			if(barcode.is_barcode_preparation_fabric) flag_source_barcode_relax ++;
			else if(!barcode.is_barcode_preparation_fabric) flag_source_barcode_stock ++;
		}
		

		if(flag_source_barcode_relax > 0 && flag_source_barcode_stock > 0)return false;
		else return true;
	}

	

}

function render() 
{
	getIndex();
	$('#barcode_products').val(JSON.stringify(list_barcodes));
	var tmpl = $('#fabric_material_in_table').html();
	Mustache.parse(tmpl);
	var data = { list: list_barcodes };
	var html = Mustache.render(tmpl, data);
	$('#tbody_fabric_material_in').html(html);
	$('.barcode_value').focus();

	bind();
}

function getIndex() 
{
	for (idx in list_barcodes) {
		list_barcodes[idx]['_id'] = idx;
		list_barcodes[idx]['no'] = parseInt(idx) + 1;
	}
}

function checkItem(barcode, index) 
{
	for (var i in list_barcodes) {
		var data = list_barcodes[i];

		if (i == index)
			continue;
		
		if (data.barcode_supplier == barcode) {
			return false;
		}

	}

	return true;
}

function bind() 
{
	$('.btn-delete-item').on('click', deleteItem);
	$('#barcode_value').on('change', tambahItem);
}

function deleteItem()
{
	var i = parseInt($(this).data('id'), 10);
	list_barcodes.splice(i, 1);
	render();
}

function tambahItem()
{
	var warehouse_id 					= $('#select_warehouse').val();
	var barcode 						= $('#barcode_value').val();
	var url_fabric_material_in_create 	= $('#url_fabric_material_in_create').val();
	var diff_dup_item					= checkItem(barcode);

	if (!diff_dup_item) 
	{
		$('#barcode_value').val('');
		$('.barcode_value').focus();
		$("#alert_warning").trigger("click", 'Data already scanned.');
		return;
	}

	$.ajax({
		type: "GET",
		url: url_fabric_material_in_create,
		data: {
			barcode			: barcode,
			warehouse_id	: warehouse_id,
		},
		beforeSend: function () 
		{
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) {
			if (response['data'])
			{
				var data 				= response['data'];
				var last_locator_used 	= $("#last_locator_used").val();
				var locator_code 		= $("#locator_in_fabricName").val();

				if(data.is_barcode_preparation_fabric && last_locator_used == 'INV')
				{
					$("#alert_warning").trigger('click', 'barcode '+data.barcode+' cannot move to locator '+locator_code); 
					return false;
				}
				list_barcodes.push(data);
			}

			if (response['locator'])
			{
				var locator_id 		= response['locator']['id'];
				var locator_code 	= response['locator']['code'];
				var last_po_buyer 	= response['locator']['last_po_buyer'];
				$("#locator_in_fabricName").val(locator_code);
				$("#locator_in_fabricId").val(locator_id);
				$("#last_locator_used").val(last_po_buyer);
				$('#alert_locator').removeClass('hidden');

				if(last_po_buyer == 'INV')
				{
					$('#msg_locator').text('Locator only accept barcode material stock');
				}else
				{
					$('#msg_locator').text('Locator only accept barcode material relax');
				}

			}
			
		},
		error: function (response) 
		{
			$.unblockUI();
			$('#barcode_value').val('');
			$('.barcode_value').focus();
			
			if (response['status'] == 500) $("#alert_error").trigger('click','Please contact ICT');
			if (response['status'] == 422) $("#alert_warning").trigger('click', response.responseJSON);
		}
	})
	.done(function ()
	{
		$('#barcode_value').val('');
		$('.barcode_value').focus();
		render();
	});
}

function setFocusToTextBox() 
{
	$('#barcode_value').focus();
}

function locatorPicklist(name,warehouse_id, url) 
{
	var search 		= '#' + name + 'Search';
	var item_id 	= '#' + name + 'Id';
	var item_name 	= '#' + name + 'Name';
	var modal 		= '#' + name + 'Modal';
	var table 		= '#' + name + 'Table';
	var buttonSrc 	= '#' + name + 'ButtonSrc';

	function itemAjax()
	{
		var q = $(search).val();
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url:  url + '&q=' + q +'&warehouse_id=' + warehouse_id
		})
		.done(function (data) 
		{
			
			$(table).html(data);
			pagination(name);
			$(search).focus();
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem()
	{
		var id 			= $(this).data('id');
		var name 		= $(this).data('name');
		var last_used 	= $(this).data('lastused');
		
		$(item_id).val(id);
		$(item_name).val(name);
		$('#last_locator_used').val(last_used);

		$('#alert_locator').removeClass('hidden');

		if(last_used == 'INV')
		{
			$('#msg_locator').text('Locator only accept barcode material stock');
		}else if(last_used == 'RLX')
		{
			$('#msg_locator').text('Locator only accept barcode material relax');
		}else $('#msg_locator').text('Locator accept either barcode material relax or barcode material stock');
	}

	function pagination()
	{
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	itemAjax();
}
