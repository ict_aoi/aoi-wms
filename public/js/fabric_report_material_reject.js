$(function()
{
    $('#select_warehouse').trigger('change');
    var fabricRejectTable = $('#fabric_report_material_reject_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/fabric/report/material-reject-fabric/data',
            data: function(d) {
                return $.extend({}, d, {
                    "warehouse"     : $('#select_warehouse').val(),
                });
           }
        },
        fnCreatedRow: function (row, data, index) {
            var info = fabricRejectTable.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'barcode_supplier', name: 'barcode_supplier',searchable:true,visible:true,orderable:true},
            {data: 'nomor_roll', name: 'nomor_roll',searchable:true,visible:true,orderable:true},
            {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
            {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'item_desc', name: 'item_desc',searchable:true,visible:true,orderable:true},
            {data: 'batch_number', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'color', name: 'color',searchable:false,visible:true,orderable:true},
            {data: 'no_packing_list', name: 'no_packing_list',searchable:true,visible:true,orderable:false},
            {data: 'no_invoice', name: 'no_invoice',searchable:true,visible:true,orderable:false},
            {data: 'qty_reject', name: 'qty_reject',searchable:false,visible:true,orderable:false},
            {data: 'remark', name: 'remark',searchable:false,visible:true,orderable:false},
        ]
    });

    var dtable = $('#fabric_report_material_reject_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();
    
    $('#select_warehouse').on('change',function(){
        dtable.draw();
    });
});