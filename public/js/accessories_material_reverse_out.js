list_barcodes = JSON.parse($('#barcode_products').val());

$(function () 
{
	$('.barcode_value').focus();
	setFocusToTextBox();
	render();

	$('#form').submit(function (event) 
	{
		event.preventDefault();
		
		if (list_barcodes.length == 0) {
			$('#barcode_value').val('');
			$('.barcode_value').focus();
			$("#alert_error").trigger("click", 'Please scan barcode first.');
			return false;
		}

		bootbox.confirm("Are you sure want to save this ?.", function (result) {
			if (result) {
				$.ajax({
					type: "POST",
					url: $('#form').attr('action'),
					data: $('#form').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function (response) {
						$('#barcode_value').val('');
						$('.barcode_value').focus();
						
						$('#form').trigger("reset");
						list_barcodes = [];
						render();
					},
					error: function (response) {
						$.unblockUI();
						$('#barcode_value').val('');
						$('.barcode_value').focus();

						if (response['status'] == 500)
							$("#alert_error").trigger("click", 'Please contact ICT');


						if (response['status'] == 422)
							$("#alert_info_2").trigger("click", response.responseJSON);
					}
				})
				.done(function () {
					$("#alert_success").trigger("click", 'Data successfully saved.');
					$("#alert_checkout").addClass('hidden');
					$("#alert_cancel_order").addClass('hidden');
					$("#to_destinationName").attr('disabled', false)
					$("#to_destinationButtonDel").attr('disabled', false)
				});
			}
		});

	});
});

$('#select_warehouse').on('change',function(){
	var _warehouse_id = $(this).val();
	$('#warehouse_id').val(_warehouse_id);
});

$('#is_unintegrated').click(function () 
{
	var _is_unintegrated 	= $('#_is_unintegrated').val();
	var is_unintegrated 	= $('#is_unintegrated').val();
	if (_is_unintegrated == 0) 
	{
		$('#_is_unintegrated').val('1');
		$("#is_unintegrated").css("background-color", '#ffd400');
		$("#is_unintegrated").css("color", 'white');
		checked();
		selected(is_unintegrated);
		
	} else 
	{
		$('#_is_unintegrated').val('0');
		$("#is_unintegrated").css("background-color", '#f5f5f5');
		$("#is_unintegrated").css("color", 'black');
		checked();
		selected();
	}
	render();
});

function checked()
{
	var _is_unintegrated = $('#_is_unintegrated').val();
	for (var i in list_barcodes) 
	{
		if (_is_unintegrated == 1)
		{
			$('#check_' + i).prop('checked', true);
			list_barcodes[i].is_unintegrated = true; 
			if(list_barcodes[i]._ict_log==null)
			{
				list_barcodes[i]._ict_log=list_barcodes[i].ict_log;
			}
			list_barcodes[i].ict_log='GAGAL INTEGRASI';
		}else
		{
			$('#check_'+ i).prop('checked', false);
			list_barcodes[i].is_unintegrated = false; 
			console.log(list_barcodes[i]);
			if(list_barcodes[i]._ict_log!=null){
				list_barcodes[i].ict_log=list_barcodes[i]._ict_log;
			}
		}
	}
}

function selected(selected)
{
	var _is_unintegrated = $('#_is_unintegrated').val();
	for (var i in list_barcodes) 
	{
		//var data = list_barcodes[i];
		if (_is_unintegrated == 1) 
		{
			$('#status_' + i).removeClass('hidden');
			$('#status_' + i).text(selected);
			$('#status_option_' + i).addClass('hidden');
			list_barcodes[i].status = selected;
		}else
		{
			$('#status_' + i).addClass('hidden');
			$('#status_option_' + i).removeClass('hidden');
			list_barcodes[i].status = list_barcodes[i]._status;
		}
			
	}
	render();
}

function render() 
{
	getIndex();
	$('#barcode_products').val(JSON.stringify(list_barcodes));
	var tmpl = $('#accessories_material_reverse_out_table').html();
	Mustache.parse(tmpl);
	var data = { list: list_barcodes };
	var html = Mustache.render(tmpl, data);
	$('#tbody_accessories_material_reverse_out').html(html);
	$('.barcode_value').focus();
	bind();
}

function getIndex() 
{
	for (id in list_barcodes) 
	{
		list_barcodes[id]['_id'] 	= id;
		list_barcodes[id]['no'] 	= parseInt(id) + 1;
	}
}

function bind() 
{
	$('.btn-delete-item').on('click', deleteItem);
	$('#barcode_value').on('change', tambahItem);
	$('.ict-log').on('change',gantiValueICTLog);
}

function checkItem(barcode)
{
	var has_dash 	= barcode.indexOf("-");
	var _barcode 	= '';
	var num_of_row 	= 0;
	var is_exist 	= 0;

	if (has_dash != -1) 
	{
		_barcode 	= barcode.split("-")[0];
		num_of_row 	= barcode.split("-")[1];
	}else 
	{
		_barcode 	= barcode;
	}

	for (var i in list_barcodes)
	{
		var data = list_barcodes[i];
		if (data.barcode.trim() == _barcode.trim())
		{
			is_exist 			= 1;
			var qty_carton 		= parseInt(data.total_carton);
			var counter 		= parseInt(data.counter);
			var flag_is_exists	= false;

			if (counter >= qty_carton) return 4;

			if (num_of_row != 0) 
			{
				if (num_of_row > qty_carton) 
				{
					return 5;
				}
			}
			
			for (var id in data.details) 
			{
				var detail = data.details[id];

				if (barcode == detail['barcode_supplier']) flag_is_exists = true;

				if (counter <= qty_carton) 
				{
					var input = {
						'barcode_supplier': barcode,
					};
				}else 
				{
					return 3;
				}
			}

			if (flag_is_exists == false) 
			{
				data.counter = counter + 1;
				data.details.push(input);
			}else 
			{
				return 1;
			}

			render();
			return 2;
		}
	}

	if (is_exist == 0);
		return 6;

	/*for (var i in list_barcodes) {
		var data = list_barcodes[i];
		if (i == index)
			continue;

		if (data.barcode == barcode) {
			$("#barcode_value").val("");
			$(".barcode_value").focus();
			return false;
		}

	}

	return true;*/
}



function deleteItem() 
{
	var i = $(this).data('id');
	list_barcodes.splice(i, 1);
	render();
}


function gantiValueICTLog()
{
	var i 						= $(this).data('id');
	list_barcodes[i].ict_log 	= $(this).val();
	render();
}

function tambahItem()
{
	var warehouse_id 									= $('#select_warehouse').val();
	var barcode 										= $('#barcode_value').val();
	var url_accessories_material_reverse_out_create 	= $('#url_accessories_material_reverse_out_create').val();
	var diff 											= checkItem(barcode);

	if (diff == 1) 
	{
		$("#barcode_value").val("");
		$(".barcode_value").focus();
		$("#alert_error").trigger("click", 'Barcode already scanned.');
		return;
	}else if (diff == 2) 
	{
		$("#barcode_value").val("");
		$(".barcode_value").focus();
		return;
	}else
	{
		$.ajax({
			type: "GET",
			url: url_accessories_material_reverse_out_create,
			data: {
				barcode			: barcode,
				warehouse_id	: warehouse_id
			},
			beforeSend: function () 
			{
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			complete: function () {
				$.unblockUI();
			},
			success: function (response) {
				list_barcodes.push(response);

			},
			error: function (response) {
				$.unblockUI();
				$('#barcode_value').val('');
				$('.barcode_value').focus();

				if (response['status'] == 500) $("#alert_error").trigger('click', 'Please contact ICT');
				if (response['status'] == 422) $("#alert_warning").trigger('click', response.responseJSON);

			}
		})
		.done(function (response) 
		{
			checked();
			selected(is_unintegrated);
			render();
		});
	}
}

function setFocusToTextBox()
{
	$('#barcode_value').focus();
}