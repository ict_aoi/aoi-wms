$(document).ready(function() 
{
	lov('po_buyer_locator', '/accessories/locator/buyer-picklist?');
})

$('.btn-show-item').on('click', function (e) 
{
	var header 	= $(this).data('header');
	var rack 	= $(this).data('rack');
	var locator = $(this).data('id');

	var area 		= $('#areaName_' + header + '_' + rack + '_' + locator).val();
	var rack_name 	= $('#rack_' + header + '_' + rack + '_' + locator).val();
	var locator_id 	= $('#locatorId_' + header + '_' + rack + '_' + locator).val();
	var po_buyer 	= $('#poBuyer_' + header + '_' + rack + '_' + locator).val();
	var counter_in 	= $('#counterIn_' + header + '_' + rack + '_' + locator).val();
	
	if (area == 'FREE STOCK')
	{
		if (counter_in == 0) 
		{
			$("#alert_info").trigger("click", 'Locator ' + rack_name + ' is empty.');
			return false;
		}

		$('#locator_free_stock_area_id').val(locator_id);
		$('#detail_header_free_stock_table').text(rack_name);
		$('#detilFreeStockModal').modal();
		detailFreeStockTable();
	}else
	{
		if(area == 'QC' || area == 'RECEIVING')
		{
			if (counter_in == 0) 
			{
				$("#alert_info").trigger("click", 'Locator ' + rack_name + ' is empty.');
				return false;
			}
		}else
		{
			if (po_buyer == '') 
			{
				$("#alert_info").trigger("click", 'Locator ' + rack_name + ' is empty.');
				return false;
			}
		}
		

		$('#locator_non_free_stock_id').val(locator_id);
		$('#detail_header_non_free_stock_area').text(rack_name);
		$('#detilNonFreeStockModal').modal();
		detailNonFreeStockTable();
	}
});

function detailFreeStockTable()
{
	$('#detail_free_stock_table').DataTable().destroy();
	$('#detail_free_stock_table tbody').empty();
	
	$('#detail_free_stock_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
			url: '/accessories/locator/data-rack-free-stock',
			data: function(d) {
                return $.extend({}, d, {
                    "locator_id" : $('#locator_free_stock_area_id').val(),
                });
            }
        },
        columns: [
			{data: 'document_no', name: 'document_no'},
			{data: 'po_buyer', name: 'po_buyer'},
			{data: 'item_code', name: 'item_code'},
			{data: 'uom', name: 'uom'},
			{data: 'available_qty', name: 'available_qty',searchable:false}
		]
	});
	
	var dtable = $('#detail_free_stock_table').dataTable().api();
	$(".dataTables_filter input")
		.unbind() // Unbind previous default bindings
		.bind("keyup", function (e) { // Bind our desired behavior
			// If the user pressed ENTER, search
			if (e.keyCode == 13) {
				// Call the API search function
				dtable.search(this.value).draw();
			}
			// Ensure we clear the search if they backspace far enough
			if (this.value == "") {
				dtable.search("").draw();
			}
			return;
	});
	dtable.draw();

	$('#locator_free_stock_area_id').on('change',function(){
		dtable.draw();
	});
}

function detailNonFreeStockTable()
{
	$('#detail_non_free_stock_table').DataTable().destroy();
	$('#detail_non_free_stock_table tbody').empty();
	
	$('#detail_non_free_stock_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
			url: '/accessories/locator/data-rack-non-free-stock',
			data: function(d) {
                return $.extend({}, d, {
                    "locator_id" : $('#locator_non_free_stock_id').val()
                });
            }
        },
        columns: [
			{data: 'barcode', name: 'barcode'},
			{data: 'po_supplier', name: 'po_supplier'},
			{data: 'item_code', name: 'item_code'},
			{data: 'po_buyer', name: 'po_buyer'},
			{data: 'style', name: 'style'},
			{data: 'article_no', name: 'article_no'},
			{data: 'uom_conversion', name: 'uom_conversion'},
			{data: 'qty_conversion', name: 'qty_conversion',searchable:false},
			{data: 'qty_need', name: 'qty_need',searchable:false},
		]
	});
	
	var dtable = $('#detail_non_free_stock_table').dataTable().api();
	$(".dataTables_filter input")
		.unbind() // Unbind previous default bindings
		.bind("keyup", function (e) { // Bind our desired behavior
			// If the user pressed ENTER, search
			if (e.keyCode == 13) {
				// Call the API search function
				dtable.search(this.value).draw();
			}
			// Ensure we clear the search if they backspace far enough
			if (this.value == "") {
				dtable.search("").draw();
			}
			return;
	});
	dtable.draw();

	$('#locator_non_free_stock_id').on('change',function(){
		dtable.draw();
	});
}