list_barcode_header = JSON.parse($('#barcode-header').val());

$(function()
{
	$('.barcode_value').focus();
	render();

	$('#form').submit(function (event) 
	{
		event.preventDefault();
		
		if (list_barcode_header.length == 0){
			$("#alert_warning").trigger("click", 'Please scan barcode first');
			return false;
		}

		var zero_input = checkInput();
		if (zero_input) 
		{
			$("#alert_warning").trigger("click", 'Please input qty reject on barcode number ' + zero_input + ' .');
			return false;
		}
		
		bootbox.confirm("Are you sure want to save this data ?.", function (result) {
			if (result) {
				$.ajax({
					type: "POST",
					url: $('#form').attr('action'),
					data: $('#form').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function (response) {
						$('#barcode_value').val('');
						$('.barcode_value').focus();
					},
					error: function (response) {
						$.unblockUI();
						$('#barcode_value').val('');
						$('.barcode_value').focus();

						if(response['return'] == 500)
							$("#alert_error").trigger("click", 'Please contact ICT');

						if (response['return'] == 422)
							$("#alert_warning").trigger("click", response.responseJSON);
						
					}
				})
				.done(function (response) {
					/*var arrStr = encodeURIComponent(JSON.stringify(response['message']));
					var win = window.open('/material-receive/barcode/reject?id=' + arrStr);

					if (win)
						win.focus();*/
					$("#alert_success").trigger("click", 'Data successfully saved.');
					list_barcode_header = [];
					render();
				});
			}
		});
	});
});

$('#select_warehouse').on('change',function(){
	$('#warehouse_id').val($(this).val());
});

$('#selectAll').click(function () 
{
	var _selectAll = $('#_selectAll').val();
	var status_all = $('#status_all').val();
	if (_selectAll == 0) 
	{
		$('#_selectAll').val('1');
		$("#selectAll").css("background-color", '#ffd400');
		$("#selectAll").css("color", 'white');
		checked();
		selected(status_all);
		
	} else 
	{
		$('#_selectAll').val('0');
		$("#selectAll").css("background-color", '#f5f5f5');
		$("#selectAll").css("color", 'black');
		checked();
		selected();
	}
	render();
});

$('#status_all').change(function () 
{
	var status_all = $(this).val();
	var _selectAll = $('#_selectAll').val();
	if (_selectAll == 1){
		selected(status_all);
		render();
	}
	
});

function render()
{
	getIndex();
	$('#barcode-header').val(JSON.stringify(list_barcode_header));
	var tmpl = $('#accessories_material_quality_control_table').html();
	Mustache.parse(tmpl);
	var data = { item: list_barcode_header };
	//console.log(data);
	var html = Mustache.render(tmpl, data);
	$('#tbody_accessories_material_quality_control').html(html);
	$('.barcode_value').focus();
	bind();
}

function bind()
{
	$('#barcode_value').on('change', tambahItem);
	$('.checkbox_item').on('click', checkedItem);
	$('.status_item').on('change', changeStatus);
	$('.input-item').on('change', ChangeQty);
	$('.input-remark').on('change', ChangeRemark);
	$('.btn-delete-item').on('click', deleteItem);
	$('.btn-edit-item').on('click', editItem);
	$('.btn-save-item').on('click', saveItem);
	$('.btn-cancel-item').on('click', cancelEdit);

	$('.input-number').keypress(function (e) 
	{

		if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
			return false;
		}
		return true;
	});
}

function getIndex()
{
	var status_all = $('#status_all').val();

	for (idx in list_barcode_header) 
	{
		list_barcode_header[idx]['_idx'] 	= idx;
		list_barcode_header[idx]['no'] 		= parseInt(idx) + 1;

		for (var id in list_barcode_header[idx]['status_options']) 
		{
			if (list_barcode_header[idx]['check_all'] == true)
			{
				if (status_all == list_barcode_header[idx]['status_options'][id]['id']) 
				{
					list_barcode_header[idx]['status_options'][id]['selected'] = 'selected';
				}else
				{
					list_barcode_header[idx]['status_options'][id]['selected'] = '';
				}
			}else
			{
				if (list_barcode_header[idx]['status'] == list_barcode_header[idx]['status_options'][id]['id']) 
				{
					list_barcode_header[idx]['status_options'][id]['selected'] = 'selected';
				}else
				{
					list_barcode_header[idx]['status_options'][id]['selected'] = '';
				}
			}
		}
	}
}

function checked()
{
	var _selectAll = $('#_selectAll').val();
	for (var i in list_barcode_header) 
	{
		var data = list_barcode_header[i];
		if (_selectAll == 1)
		{
			$('#check_' + i).prop('checked', true);
			data.check_all = true; 
		}else
		{
			$('#check_' + i).prop('checked', false);
			data.check_all = false; 
		}
	}
	
}

function changeStatus()
{
	var i 								= $(this).data('id');
	var selected_item 					= $('#status_option_'+i).val();
	list_barcode_header[i].check_all 	= false;
	list_barcode_header[i].status 		= selected_item;
	render();
}

function ChangeQty()
{
	var i 				= $(this).data('id');
	var qty_reject 		= $('#qtyInput_' + i).val();
	var qty 			= list_barcode_header[i]._qty;
	var new_qty 		= qty - qty_reject;
	
	if (new_qty < 0)
	{
		$("#alert_warning").trigger("click", 'Qty reject shoud be less than qty available.');
		$('#qtyInput_' + i).val('');
		return false;
	}
		
	list_barcode_header[i].qty 			= new_qty;
	list_barcode_header[i].qty_check 	= qty_reject;
	render();
}

function ChangeRemark()
{
	var i 							= $(this).data('id');
	var remark 						= $('#remarkInput_' + i).val();
	list_barcode_header[i].remark 	= remark;
	render();
}

function checkedItem()
{
	var i 				= $(this).data('id');
	var status_all 		= $('#status_all').val();
	var _selectAll 		= $('#_selectAll').val();

	if (_selectAll == 1) 
	{
		list_barcode_header[i].check_all 	= true;
		list_barcode_header[i].status 		= status_all;
	}
	render();
}

function selected(selected)
{
	var _selectAll = $('#_selectAll').val();

	for (var i in list_barcode_header) 
	{
		var data = list_barcode_header[i];
		if (_selectAll == 1) 
		{
			$('#status_' + i).removeClass('hidden');
			$('#status_' + i).text(selected);
			$('#status_option_' + i).addClass('hidden');
			data.status = selected;
		}else
		{
			$('#status_' + i).addClass('hidden');
			$('#status_option_' + i).removeClass('hidden');
			data.status = data._status;
		}
			
	}
	render();
}

function checkItem(barcode_val, index) 
{
	var n = barcode_val.includes("-");
	
	if(n) var _temp = barcode_val.split("-")[0];
	else var _temp = barcode_val;

	for (var i in list_barcode_header) 
	{
		var barcode = list_barcode_header[i];
		
		if (i == index) continue;

		if (barcode.barcode == _temp) return false;
	}
	return true;
}

function tambahItem()
{
	var warehouse_id 								= $('#select_warehouse').val();
	var barcode 									= $('#barcode_value').val();
	var status_all 									= $('#status_all').val();
	var url_accessories_material_quality_control 	= $('#url_accessories_material_quality_control').val();
	
	var diff = checkItem(barcode);
	
	if (!diff) 
	{
		$("#alert_error").trigger("click", 'Barcode already scanned.');
		$("#barcode_value").val("");
		$(".barcode_value").focus();
		return;
	}

	$.ajax({
		type: "GET",
		url: url_accessories_material_quality_control,
		data: {
			barcode			: barcode,
			warehouse_id	: warehouse_id,
		},
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () 
		{
			$.unblockUI();
		},
		success: function (response) 
		{
			list_barcode_header.push(response);
			$('#barcode_value').val('');
			$('.barcode_value').focus();
		},
		error: function (response) 
		{
			$.unblockUI();
			$('#barcode_value').val('');
			$('.barcode_value').focus();

			if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
			if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);
		}
	})
	.done(function (response) 
	{
		checked();
		selected(status_all);
		render();
	});;
}

function deleteItem()
{
	var i = $(this).data('id');

	list_barcode_header.splice(i, 1);
	render();
}

function editItem() 
{
	var i 		= $(this).data('id');
	var status 	= $('#_statusInput_' + i).val();

	$('#qtyReject_' + i).addClass('hidden');
	$('#status_' + i).addClass('hidden');
	$('#edit_' + i).addClass('hidden');
	$('#delete_' + i).addClass('hidden');

	//remove hidden textbox
	$('#qtyInput_' + i).removeClass('hidden');
	$('#status_option_' + i).removeClass('hidden');
	
	$('#status_option_' + i).val(status);

	$('#simpan_' + i).removeClass('hidden');
	$('#cancel_' + i).removeClass('hidden');
	
	
}

function cancelEdit() 
{
	var i = $(this).data('id');

	//remove hidden div
	$('#qtyReject_' + i).removeClass('hidden');
	$('#status_option_' + i).removeClass('hidden');
	$('#edit_' + i).removeClass('hidden');
	$('#delete_' + i).removeClass('hidden');

	//add hidden textbox
	$('#qtyInput_' + i).addClass('hidden');

	$('#simpan_' + i).addClass('hidden');
	$('#cancel_' + i).addClass('hidden');
}

function saveItem() 
{
	var i 					= $(this).data('id');
	var _temp 				= $('#_temp_' + i).val();
	var qty_rcv 			= $('#qtyRcvInput_' + i).val();
	var qty_input 			= $('#qtyInput_' + i).val();
	var status_option 		= $('#status_option_' + i).val();

	if (qty_input == '') 
	{
		$("#alert_warning").trigger("click", 'Please insert Qty reject first.');
		return false;
	}

	if (parseFloat((parseFloat(_temp) - parseFloat(qty_input))) < 0) 
	{
		$("#alert_warning").trigger("click", 'Please check you qty reject. (Qty reject yang diinput melibihi qty available, silahkan cek kembali)');
		return false;
	}

	if (qty_input == 0)
	{
		list_barcode_header[i].qty = _temp;
	}else
	{
		list_barcode_header[i].qty = _temp - qty_input;
	}
	
	list_barcode_header[i].status 		= status_option;
	list_barcode_header[i].qty_check 	= qty_input;
	render();
}

//check inputan
function checkInput() 
{
	for (var i in list_barcode_header) 
	{
		if (list_barcode_header[i].status == "REJECT") 
		{
			var inputan = parseFloat(list_barcode_header[i].qty_check).toFixed(2);
			if (inputan <= 0) 
			{
				return list_barcode_header[i].barcode;
			}
		}
	}
	return false;
}

