list_barcodes = JSON.parse($('#barcode_products').val());

$(function () 
{
	$('.barcode_value').focus();
	setFocusToTextBox();
	checkDestination();
	checkMaterialOut();
	render();

	$('#form').submit(function (event) 
	{
		event.preventDefault();
		var check_po_buyer 		= hasOnePobuyer();
		var check_destination 	= $('#_from_rackin_id').val();
		var rack 				= $("#from_rackinName").val();
		var has_many_po_buyer 	= $("#has_many_po_buyer").val();
		var is_not_complated 	= isNotComplated();

		if (check_destination == null || check_destination == '') 
		{
			$("#alert_error").trigger("click", 'Please select locator first. ( Silahkan pilih locator anda terlebih dahulu )');
			return false;
		}

		if (has_many_po_buyer == '') 
		{
			for (var i in list_barcodes) 
			{
				var barcode = list_barcodes[i];
				var diff_pobuyer_item = checkPoBuyer(barcode.po_buyer,i);

				if (!diff_pobuyer_item) 
				{
					$("#alert_warning").trigger("click", 'Po buyer must be same (Po buyer harus sama).');
					return;
				}
			}
		}

		if (!check_po_buyer && has_many_po_buyer != 1) 
		{
			$("#alert_warning").trigger("click", 'Po buyer on locator does\'nt same with this, please select other locator. (Po buyer yang ada di rak tidak sama, silahkan pilih locator lain)');
			return;
		}

		if (check_destination == -1) 
		{
			$("#alert_warning").trigger("click", 'Po buyer has exists in many locator, please choose locator first. (Po buyer terdapat di lebih dari 1 locator,silahkan pilih locator terlebih dahulu)');
			return false;
		}

		if (list_barcodes.length == 0) 
		{
			$("#alert_warning").trigger("click", 'Please scan material first. (Silahkan scan barcode terlebih dahulu)');
			setFocusToTextBox();
			return false;
		}


		if (is_not_complated > 0) 
		{
			$('#barcode_value').val('');
			$('.barcode_value').focus();
			$("#alert_error").trigger("click", 'Ada item yang belum lengkap total karton, silahkan cek kembali.');
			return false;
		}


		bootbox.confirm("Are you sure want to save material to locator " + rack + " ?.", function (result) 
		{
			if (result) 
			{
				$.ajax({
					type: "POST",
					url: $('#form').attr('action'),
					data: $('#form').serialize(),
					beforeSend: function () 
					{
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () 
					{
						$.unblockUI();
					},
					success: function (response) 
					{
						$('#barcode_value').val('');
						$('.barcode_value').focus();
						$("#alert_locator").addClass('hidden');
						$("#alert_out").addClass('hidden');

						$('#_from_rackin_id').val('');
						$('#from_rackin_id').val('');
						$('#from_rackinName').val('');
						$('#rack_po_buyer').val('');
						$('#has_many_po_buyer').val('');
						$('#__po_buyer').val('');

						$('#form').trigger("reset");
						
						list_barcodes = [];
						//jika error
						if (response.length > 0)
						{
							for (var i in response)
							 {
								var data = response[i];
								list_barcodes.push(data);
							}
						}
						
						render();
						if (list_barcodes.length == 0) 
						{
							$('#_from_rackin_id').val('');
							$('#from_rackin_id').val('');
							$('#from_rackinName').val('');

							$("#alert_locator").addClass('hidden');
							$("#alert_out").addClass('hidden');
						}

					},
					error: function (response) 
					{
						$.unblockUI();
						$('#barcode_value').val('');
						$('.barcode_value').focus();

						if (response['status'] == 500)
							$("#alert_error").trigger("click", 'Please Contact ICT');


						if (response['status'] == 422)
							$("#alert_warning").trigger("click", response.responseJSON);

					}
				})
				.done(function (response){
					$("#alert_success").trigger("click", 'Material successfully saved.');
				});
			}
		});



	});

	$('#reset_all_checkin').click(function (event) 
	{
		bootbox.confirm("Are you sure want to reset this ?.", function (result) 
		{
			if (result) 
			{
				var url_reset_temporary = $('#url_temporary_reset').val();
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
					type: "DELETE",
					url: url_reset_temporary,
					data: {
						status: 'in'
					},
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					error: function (response) 
					{
						$.unblockUI();
						if (response['status'] == 500) {
							$("#alert_error").trigger("click", 'Please contact ICT');
						} else {
							for (i in response.responseJSON) {
								$("#alert_error").trigger("click", response.responseJSON[i]);
							}
						}
					}
				})
				.done(function (response) 
				{
					$('#barcode_value').val('');
					$('.barcode_value').focus();
					list_barcodes = [];
					render();
				});
			}
		});
	})
});


$('#from_rackinName').click(function () 
{
	var warehouse_id = $('#select_warehouse').val();
	locatorPicklist('from_rackin',warehouse_id, '/accessories/material-in/rack-picklist?');
});

$('#from_rackinButtonLookup').click(function () 
{
	var warehouse_id = $('#select_warehouse').val();
	locatorPicklist('from_rackin',warehouse_id, '/accessories/material-in/rack-picklist?');
});

$('#select_warehouse').on('change',function(){
	warehouse_id = $(this).val();
	$('#warehouse_id').val($(this).val());
	locatorPicklist('from_rackin',warehouse_id, '/accessories/material-in/rack-picklist?');
});

function locatorPicklist(name,warehouse_id, url) 
{
	var search 		= '#' + name + 'Search';
	var item_id 	= '#' + name + 'Id';
	var item_name 	= '#' + name + 'Name';
	var modal 		= '#' + name + 'Modal';
	var table 		= '#' + name + 'Table';
	var buttonSrc 	= '#' + name + 'ButtonSrc';
	var buttonDel 	= '#' + name + 'ButtonDel';

	function itemAjax(){
		var q = $(search).val();
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&q=' + q +'&warehouse_id=' + warehouse_id
		})
		.done(function (data) {
			$(table).html(data);
			pagination(name);
			$(search).focus();
			$(search).val('');
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem()
	{
		var id 			= $(this).data('id');
		var name 		= $(this).data('name');
		var has_many_po_buyer = $(this).data('manypobuyer');
		var pobuyer 	= $(this).data('pobuyer');

		$(item_id).val(id);
		$(item_name).val(name);
		$('#_from_rackin_id').val(id);
		$('#rack_po_buyer').val(pobuyer);
		$('#has_many_po_buyer').val(has_many_po_buyer);
		$(item_id).trigger("change", id);
	}

	function pagination()
	{
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	$(search).val("");
	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	$(buttonDel).on('click', function () {
		$(item_id).val('');
		$(item_name).val('');


		var flag = ['#from_rackinName'];
		if (flag.indexOf(item_name) != -1)
			$(item_id).trigger("change", '');

	});

	itemAjax();
}

$('#from_rackinButtonDel').click(function () 
{
	$('#from_rackinId').val('');
	$('#from_rackinName').val('');
	$('#rack_po_buyer').val('');
	$('#has_many_po_buyer').val('');
	if (list_barcodes.length == 0) $('#__po_buyer').val('');
});

function render() 
{
	getIndex();
	$('#barcode_products').val(JSON.stringify(list_barcodes));
	var tmpl 		= $('#accessories_material_in_table').html();
	Mustache.parse(tmpl);
	var data 		= { list: list_barcodes };
	var html 		= Mustache.render(tmpl, data);
	$('#tbody_accessories_material_in').html(html);
	$('.barcode_value').focus();
	isExists();
	bind();
}

function getIndex() 
{
	for (idx in list_barcodes) 
	{
		list_barcodes[idx]['_id'] 	= idx;
		list_barcodes[idx]['no'] 	= parseInt(idx) + 1;
	}
}

function isNotComplated() 
{
	var is_not_complated = 0;
	for (var i in list_barcodes) 
	{
		var barcode = list_barcodes[i];
		if (barcode.counter != barcode.total_carton) 
		{
			$('#panel_' + i).css('background-color', '#edb8fc');
			is_not_complated++;
		}

	}
	return is_not_complated;
}

function checkItem(barcode) 
{
	var has_dash = barcode.indexOf("-");
	var _barcode = '';
	var num_of_row = 0;
	var is_exist = 0;

	if (has_dash != -1) {
		_barcode = barcode.split("-")[0];
		num_of_row = barcode.split("-")[1];
	} else {
		_barcode = barcode;
	}

	for (var i in list_barcodes) {
		var data = list_barcodes[i];
		if (data.barcode.trim() == _barcode.trim()) {
			is_exist = 1;
			var qty_carton = parseInt(data.total_carton);
			var counter = parseInt(data.counter);
			var flag_is_exists = false;

			if (counter >= qty_carton)
				return 4;

			if (num_of_row != 0) {
				if (num_of_row > qty_carton) {
					return 5;
				}
			}

			for (var id in data.details) {
				var detail = data.details[id];

				if (barcode == detail['barcode_supplier'])
					flag_is_exists = true;

				if (counter <= qty_carton) {
					var input = {
						'barcode_supplier': barcode,
					};
				} else {
					return 3;
				}
			}

			if (flag_is_exists == false) {
				data.counter = counter + 1;
				data.details.push(input);
			} else {
				return 1;
			}

			render();
			return 2;
		}
	}

	if (is_exist == 0);
	return 6;
}

function checkPoBuyer(po_buyer,index) 
{
	for (var i in list_barcodes) 
	{
		var barcode = list_barcodes[i];
		if (i == index)
			continue;

		if (barcode.po_buyer != po_buyer) 
		{
			return false;
		}

	}

	return true;
}

function bind() 
{
	$('.btn-delete-item').on('click', deleteItem);
	$('#barcode_value').on('change', tambahItem);
}

function deleteItem()
{
	var i = parseInt($(this).data('id'), 10);
	//deleteTemporary(list_barcodes[i].barcode);
	list_barcodes.splice(i, 1);
	render();

	if(list_barcodes.length == 0) {
		$("#alert_locator").addClass('hidden');
		$("#alert_out").addClass('hidden');
	}
}

function isExists() 
{
	for (var i in list_barcodes) 
	{
		var barcode = list_barcodes[i];
		if (barcode.is_po_buyer_cancel ) 
		{
			$('#panel_' + i).css('background-color', '#f54336');
			$('#panel_' + i).css('color', 'white');
		}else
		{
			if(barcode.is_others_already_out)
			{
				$('#panel_' + i).css('background-color', 'black');
				$('#panel_' + i).css('color', 'white');
			}else
			{
				if (barcode.is_backlog)
				{
					$("#alert_info").trigger('click', 'Po Buyer ini Backlog');
					$('#panel_' + i).css('background-color', '#ffff99');
					$('#panel_' + i).css('color', 'black');
				}
	
				if (barcode.is_need_to_handover) 
				{
					$('#panel_' + i).css('background-color', '#666699');
					$('#panel_' + i).css('color', 'white');
				}
			}
		}
	}
	
}

function tambahItem()
{
	var warehouse_id 						= $('#select_warehouse').val();
	var barcode 							= $('#barcode_value').val();
	var url_accessories_material_in_create 	= $('#url_accessories_material_in_create').val();
	
	var diff = checkItem(barcode);

	if (diff == 1) 
	{
		$("#barcode_value").val("");
		$(".barcode_value").focus();
		$("#alert_warning").trigger("click", 'Barcode already scanned.');
		return false;
	} else if (diff == 2) 
	{
		$("#barcode_value").val("");
		$(".barcode_value").focus();
		return false;
	} else if (diff == 3) 
	{
		$("#barcode_value").val("");
		$(".barcode_value").focus();
		$("#alert_warning").trigger("click", 'Total Carton is higher than data.');
		return false;
	} else if (diff == 4) 
	{
		$("#barcode_value").val("");
		$(".barcode_value").focus();
		$("#alert_warning").trigger("click", 'All carton is ready to supply. (Semua karton sudah siap untuk disupplai)');
		return false;
	} else if (diff == 5) 
	{
		$("#barcode_value").val("");
		$(".barcode_value").focus();
		$("#alert_warning").trigger("click", 'Barcode is wrong. (Barcode salah)');
		return false;
	}

	$.ajax({
		type: "GET",
		url: url_accessories_material_in_create,
		data: {
			barcode: barcode,
			warehouse_id: warehouse_id,
		},
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) 
		{
			if (response.is_reroute)
				$("#alert_info").trigger('click', 'This Po buyer ' + response.po_buyer+' is re-route, please check out to reroute destination to reallocation.');


			if (response.is_po_buyer_cancel ) 
			{
				$("#alert_info").trigger('click', 'Po Buyer '+response.po_buyer+' is cancel');
			}

			if (response.is_need_to_handover) 
			{
				if (response.warehouse_place == '1000002') var warehouse_place = 'AOI 1'; 
				else if (response.warehouse_place == '1000013') var warehouse_place = 'AOI 2';

				$("#alert_info").trigger('click', 'This Po Buyer running production in ' + warehouse_place + ', please check out to ' + warehouse_place);
			}
			
			list_barcodes.push(response);
		},
		error: function (response) 
		{
			$.unblockUI();
			$('#barcode_value').val('');
			$('.barcode_value').focus();

			if (response['status'] == 500)
				$("#alert_error").trigger('click','Please contact ICT');

			if (response['status'] == 422)
				$("#alert_warning").trigger('click', response.responseJSON);
		}
	})
	.done(function (response)
	{
		$('#barcode_value').val('');
		$('.barcode_value').focus();

		checkDestination();
		checkMaterialOut();
		render();
	});
}

function checkDestination()
{
	for (var i in list_barcodes) 
	{
		var barcode 			= list_barcodes[i];
		var destination 		= barcode.locator_id;
		var destination_name 	= barcode.locator;
		var material_out 		= barcode.material_out;

		if(!material_out)
		{
			if (barcode.locator)
			{
				var return_msg_info = true;
			}else
			{
				var return_msg_info = false;
			}

			if (destination != null)
			{
				var show_destination_name = true;
			}else
			{
				var show_destination_name = false;
			}
		}else
		{
			show_destination_name 	= false;
			return_msg_info 		= false;
			destination 			= false;
			destination_name 		= false;
		}
		var po_buyer = barcode.po_buyer;
	}

	$('#__po_buyer').val(po_buyer);



	if (return_msg_info)
	{
		$("#alert_locator").removeClass('hidden');
		$("#msg_alert").text('Some items for this po buyer already in locator (Beberapa item untuk po buyer ini sudah ada di rak ');
		$("#list_rack").text(destination_name+')');
	}

	if (show_destination_name)
	{
		$('#_from_rackin_id').val(destination);
		$('#from_rackinName').val(destination_name);
	}

}

function checkMaterialOut() 
{
	var po_buyer 	= '';
	var _tujuan_out = '';

	for (var i in list_barcodes) 
	{
		var barcode = list_barcodes[i];
		if (barcode.material_out == 'true_wms_old') 
		{
			var return_msg_info_1 	= true;
			po_buyer 				+= barcode.po_buyer+',';
		}

		if (barcode.material_out == 'true_wms_new') 
		{
			var return_msg_info_2 	= true;
			po_buyer 				+= barcode.po_buyer+',';
			_tujuan_out 			+= barcode.tujuan_out;
		}
		
	}

	var _po_buyer = po_buyer.slice(0,-1);
	if (return_msg_info_1) 
	{
		$("#alert_out").removeClass('hidden');
		$("#msg_out").text('Some material for this po buyer '+_po_buyer+' already supplied in wms old. (Beberapa material untuk po buyer '+_po_buyer+' sudah disuplai di wms lama).');
	}
	
	if (return_msg_info_2) {
		$("#alert_out").removeClass('hidden');
		$("#msg_out").text('Some material for this po buyer '+_po_buyer+' already supplied to '+_tujuan_out+'. (Beberapa material untuk po buyer '+_po_buyer+' sudah disuplai ke '+_tujuan_out+').' );
	}
}

function setFocusToTextBox() 
{
	$('#barcode_value').focus();
}

function hasOnePobuyer() 
{
	var po_buyer_on_rack = $('#rack_po_buyer').val();
	if (po_buyer_on_rack) 
	{
		for (var i in list_barcodes) 
		{
			var barcode = list_barcodes[i];

			if (barcode.po_buyer != po_buyer_on_rack) 
			{
				return false;
			}

		}
		return true;
	}else 
	{
		return true;
	}

}