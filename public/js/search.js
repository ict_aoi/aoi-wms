$(function () 
{
	lov('po_buyer','item_code','/search/po-buyer-picklist?');
	list_movement_history = JSON.parse($('#movement_history').val());
	render();
});

function lov(name,name2, url) 
{
	var search 		= '#' + name + 'Search';
	var search2 	= '#' + name2 + 'Search2';
	var item_id 	= '#' + name + 'Id';
	var item_name 	= '#' + name + 'Name';
	var modal 		= '#' + name + 'Modal';
	var table 		= '#' + name + 'Table';
	var buttonSrc 	= '#ButtonSrc';
	var buttonDel 	= '#' + name + 'ButtonDel';

	function itemAjax()
	{
		var q 	= $(search).val();
		var q2 = $(search2).val();

		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&po_buyer=' + q+'&item_code=' + q2
		})
		.done(function (data) 
		{
			$(table).html(data);
			pagination(name);
			$(search).focus();
			$(search2).val('');
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem()
	{
		var id 		= $(this).data('id');
		var name 	= $(this).data('name');

		$(item_id).val(id);
		$(item_name).val(name);

		$('#form-search').submit();
	}

	function pagination()
	{
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	//$(search).val("");
	$(buttonSrc).unbind();
	$(search).unbind();
	$(search2).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	$(search2).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	$(buttonDel).on('click', function () {
		$(item_id).val('');
		$(item_name).val('');

	});

	itemAjax();
}

function render() 
{
	$('#movement_history').val(JSON.stringify(list_movement_history));
	var tmpl = $('#_draw').html();
	Mustache.parse(tmpl);
	var data = { items: list_movement_history };
	var html = Mustache.render(tmpl, data);
	$('#result_history').html(html);
}

$('#form').submit(function (event) 
{
	event.preventDefault();
	var barcode = $('#barcode').val();

	if(!barcode)
	{
		$("#alert_error").trigger("click", 'Please scan barcode / select po buyer first');
		return false;
	}

	$.ajax({
		type: "GET",
		url: $('#form').attr('action'),
		data: $('#form').serialize(),
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		success: function (response) 
		{
			$.unblockUI();
			var item_desc =  response.item_desc + '('+ response.category +')';
			$('#item_code').html(response.item_code);
			$('#item_desc').text(item_desc);
			$('#document_no').text(response.document_no);
			$('#po_buyer').text(response.po_buyer);
			$('#job_order').text(response.job_order);
			$('#style').text(response.style);
			$('#article_no').text(response.article_no);
			$('#qc_status').text(response.qc_status);
			$('#qc_last_date_check').text(response.last_qc_date);

			$('#barcode').val('');
			$('.barcode').focus();

			$("#result").removeClass('hidden');
			$('#form_print_barcode').attr('action',response.action);
			list_movement_history = response.movement_history;
			$('#form_print_barcode').attr('action',response.url_reprint_barcode);

			if(response.qc_status == 'FULL REJECT')
			{
				$('#btn_reprint').addClass('hidden');
				$('#btn_disabled_reprint').removeClass('hidden');
			}else
			{
				$('#btn_reprint').removeClass('hidden')
				$('#btn_disabled_reprint').addClass('hidden')
			}
			render();

		},
		error: function (response) {
			$.unblockUI();
			$('#barcode').val('');
			$('.barcode').focus();

			if(response['status'] == 422)
				$("#alert_error").trigger("click", 'data yang ada cari tidak ditemukan');

			if(response['status'] == 500)
				$("#alert_error").trigger("click", 'Please Contact ICT');

			$("#result").addClass('hidden');
		}

	});

});


$('#form-search').submit(function (event) 
{
	event.preventDefault();
	var barcode = $('#po_buyerId').val();

	if(!barcode)
	{
		$("#alert_warning").trigger("click", 'Please scan barcode / select po buyer first');
		return false;
	}

	$.ajax({
		type: "GET",
		url: $('#form-search').attr('action'),
		data: $('#form-search').serialize(),
		beforeSend: function () 
		{
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		success: function (response) 
		{
			$.unblockUI();
			var item_desc =  response.item_desc + '('+ response.category +')';
			$('#item_code').html(response.item_code);
			$('#item_desc').text(item_desc);
			$('#document_no').text(response.document_no);
			$('#po_buyer').text(response.po_buyer);
			$('#job_order').text(response.job_order);
			$('#style').text(response.style);
			$('#article_no').text(response.article_no);
			$('#qc_status').text(response.qc_status);
			$('#qc_last_date_check').text(response.last_qc_date);

			$('#po_buyerId').val('');
			$('#po_buyerName').val('');
			$('.barcode').focus();
			$("#result").removeClass('hidden');
			list_movement_history = response.movement_history;
			$('#form_print_barcode').attr('action',response.url_reprint_barcode);

			if(response.qc_status == 'FULL REJECT')
			{
				$('#btn_reprint').addClass('hidden');
				$('#btn_disabled_reprint').removeClass('hidden');
			}else
			{
				$('#btn_reprint').removeClass('hidden')
				$('#btn_disabled_reprint').addClass('hidden')
			}
			render();

		},
		error: function (response) {
			$.unblockUI();
			$('#barcode').val('');
			$('.barcode').focus();

			if(response['status'] == 422) $("#alert_error").trigger("click", 'data yang ada cari tidak ditemukan');

			if(response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');

			$("#result").addClass('hidden');
		}

	});

});