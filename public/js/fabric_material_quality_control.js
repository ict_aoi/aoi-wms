list_barcode_header = JSON.parse($('#barcode-header').val());
list_defect_codes = JSON.parse($('#defect_codes').val());

$(function () 
{
	renderDefect();
	render();
	setFocusToTextBox();

	$('#form').submit(function (event) 
	{
		event.preventDefault();

		if (list_barcode_header.length == 0) {
			$("#alert_warning").trigger("click", 'Please scan barcode first.');
			return false;
		}

		//value qty_Input tidak boleh 0
		let zero_input = checkInput();
		if (zero_input) {
			$("#alert_warning").trigger("click", 'Qty on barcode ' + zero_input + ' cannot be 0.');
			return false;
		}

		if (checkActualWidth()) {
			$("#alert_warning").trigger("click", 'Please check again actual width.');
			return false;
		}

		bootbox.confirm("Are you sure want to save this ?.", function (result) 
		{
			if (result) {
				$.ajax({
						type: "POST",
						url: $('#form').attr('action'),
						data: $('#form').serialize(),
						beforeSend: function () {
							$.blockUI({
								message: '<i class="icon-spinner4 spinner"></i>',
								overlayCSS: {
									backgroundColor: '#fff',
									opacity: 0.8,
									cursor: 'wait'
								},
								css: {
									border: 0,
									padding: 0,
									backgroundColor: 'transparent'
								}
							});
						},
						complete: function () {
							$.unblockUI();
						},
						success: function (response) {
							$('#barcode_value').val('');
							$('.barcode_value').focus();
						},
						error: function (response) {
							$.unblockUI();
							$('#barcode_value').val('');
							$('.barcode_value').focus();

							if (response['return'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
							if (response['return'] == 422) $("#alert_warning").trigger("click", response.responseJSON);

						}
					})
					.done(function (response) {
						$("#alert_success").trigger("click", 'Data successfully saved.');
						list_barcode_header = [];
						render();
						renderDefect();
						setFocusToTextBox();
					});
			}
		});
	});
});

function render() 
{
	getIndex();
	$('#barcode-header').val(JSON.stringify(list_barcode_header));
	var tmpl = $('#reject-table').html();
	Mustache.parse(tmpl);
	var data = {
		item: list_barcode_header
	};
	var html = Mustache.render(tmpl, data);
	$('#tbody-reject').html(html);
	bind();
}

function renderDefect() 
{
	$('#defect_codes').val(JSON.stringify(list_defect_codes));
	var data = {
		item: list_defect_codes
	};
}

function bind() 
{
	$('#barcode_value').on('change', tambahItem);
	$('.btn-add-detail-point').on('click', tambahDetailPointYard);
	$('.btn-delete-detail-point').on('click', deleteDetailPointYard);
	$('.input-edit-defect-value').on('click', editDetailDefectValue);

	$('.input-begin-width').on('change', editBeginWidthValue);
	$('.input-midle-width').on('change', editMidleWidthValue);
	$('.input-end-width').on('change', editEndWidthValue);
	$('.input-actual-length').on('change', editActualLength);
	$('.input-kg').on('change', editKg);
	$('.input-edit-remark').on('change', changeRemark);

	$('.input-edit-point-from').on('change', editInputPointFrom);
	$('.input-edit-point-to').on('change', editInputPointTo);
	$('.result-status').on('change', changeResult);
	$('.input-defect-code').on('change', cekDefectCode);
	$('.input-edit-point-defect').on('change', cekDefectCode);
	$('.btn-delete-item').on('click', deleteItem);
	$('.btn-remove-row').on('click', removeRow);

	$('.input-number').keypress(function (e) 
	{
		let charCode = (e.which) ? e.which : e.keyCode;
		if ((charCode >= 46 && charCode <= 57) || (charCode >= 37 && charCode <= 40) || charCode == 9 || charCode == 8 || charCode == 46) {
			return true;
		}
		return false;
	});

	$('.input-edit').keypress(function (e) 
	{
		let i = $(this).data('id');

		if (e.keyCode == 13) {
			e.preventDefault();
			$('#simpan_' + i).click();
		}
	});
}

function removeRow() 
{
	let id = $(this).attr('data-id');
	for (let i = list_barcode_header.length - 1; i >= 0; i--) 
	{
		if (i == id) {
			list_barcode_header.splice(i, 1);
		}
	}
	render();
}

function changeRemark() 
{
	let id = $('#' + this.id);
	let data_id = id.data("id");
	let data_idx = id.data("idx");
	let value = id.val();
	list_barcode_header[data_id]['detail'][data_idx].remark = (value ? value.trim() : null );
}

function editInputPointFrom() 
{
	let id = $('#' + this.id);
	let data_id = id.data("id");
	let data_idx = id.data("idx");
	let value = id.val();
	list_barcode_header[data_id]['detail'][data_idx].point_check_from = value;
}

function editInputPointTo() 
{
	let id = $('#' + this.id);
	let data_id = id.data("id");
	let data_idx = id.data("idx");
	let value = id.val();
	list_barcode_header[data_id]['detail'][data_idx].point_check_to = value;
}

function editKg()
{
	let id = $(this).data('id');
	let value = $('#' + this.id).val();
	list_barcode_header[id].kg = value;
	render();
}

function editBeginWidthValue() 
{
	let id = $(this).data('id');
	let value = $('#' + this.id).val();
	list_barcode_header[id].begin_width = cekIsNullOrDot(value);
	changeActualWidth(id);
	render();
}

function editMidleWidthValue() 
{
	let id = $(this).data('id');
	let value = $('#' + this.id).val();
	list_barcode_header[id].middle_width = cekIsNullOrDot(value);
	changeActualWidth(id);
	render();
}

function editEndWidthValue() 
{
	let id = $(this).data('id');
	let value = $('#' + this.id).val();
	list_barcode_header[id].end_width = cekIsNullOrDot(value);
	changeActualWidth(id);
	render();
}

function cekIsNullOrDot(value) 
{
	if (value.length == 0 || value == '.') {
		return null;
	} else {
		return value;
	}
}

function changeActualWidth(id) 
{
	let begin_width = list_barcode_header[id].begin_width;
	let middle_width = list_barcode_header[id].middle_width;
	let end_width = list_barcode_header[id].end_width;

	let actual_width = null
	if (begin_width != null && middle_width != null && end_width != null) {
		width = [begin_width, middle_width, end_width];
		Array.min = function (array) {
			return Math.min.apply(Math, array);
		};
		actual_width = Array.min(width);
	}
	list_barcode_header[id].actual_width = actual_width;
}

function cekDefectCode() 
{
	let id = $(this).data('id');
	let idx = $(this).data('idx');

	if (idx !== undefined)
		var code = $('#defect_code_' + id + '_' + idx).val();
	else
		var code = $('#defect_code_' + id).val();

	if (code != null) {
		//list_defect_codes
		let upper_code = code.toUpperCase();
		let temp = 0;
		for (i in list_defect_codes) {
			let data = list_defect_codes[i];

			if (data.code == upper_code) {
				temp++;
				break;
			}

		}

		if (temp == 0) {
			$("#alert_warning").trigger("click", 'Wrong defect code.');
			if (idx !== undefined) {
				let data = list_barcode_header[id]['detail'][idx];
				let code = $('#defect_code_' + id + '_' + idx).val(data.defect_code);
			} else {
				let code = $('#defect_code_' + id).val('');
			}
		} else {
			if (idx !== undefined) {
				let code = $('#defect_code_' + id + '_' + idx).val(upper_code);
				list_barcode_header[id]['detail'][idx].defect_code = upper_code;
				render();
			} else {
				let code = $('#defect_code_' + id).val(upper_code);
			}


		}
	}
}

function editActualLength() 
{
	let id = $(this).data('id');
	let value = $('#' + this.id).val();
	list_barcode_header[id].actual_length = value;
	render();
}

function tambahDetailPointYard() 
{
	let id 					= $(this).data('id');
	let point_check_from	= $('#point_check_from_' + id).val();
	let point_check_to 		= $('#point_check_to_' + id).val();
	let defect_code 		= $('#defect_code_' + id).val();
	let defect_value 		= $('input[name=defect_code_value_' + id + ']:checked').val();
	let remark 				= $('#remark_' + id).val();
	let data 				= list_barcode_header[id];
	let _point_check_to 	= (point_check_to) ? point_check_to : point_check_from;

	if (!point_check_from) 
	{
		$("#alert_warning").trigger("click", 'Please insert start point first');
		return false;
	}

	let multiply = parseInt((parseInt(_point_check_to) - parseInt(point_check_from)) + 1);

	if ((parseInt(_point_check_to) - parseInt(point_check_from)) < 0) 
	{
		$("#alert_warning").trigger("click", 'Please check again start point and last point.');
		return false;
	}


	let is_selected_1 = false;
	let is_selected_2 = false;
	let is_selected_3 = false;
	let is_selected_4 = false;

	let curr_point_1 = parseInt(data.point_1);
	let curr_point_2 = parseInt(data.point_2);
	let curr_point_3 = parseInt(data.point_3);
	let curr_point_4 = parseInt(data.point_4);

	let check_defect_value = 0;
	let total_point_defect = 0;
	if (document.getElementById('defect_code_value_' + id + '_1').checked == true) 
	{
		is_selected_1 		= true;
		total_point_defect 	= 1 * multiply;
		curr_point_1 		+= total_point_defect;
		check_defect_value 	= 1;
	} else if (document.getElementById('defect_code_value_' + id + '_2').checked == true) {
		is_selected_2 = true;
		total_point_defect = 2 * multiply;
		curr_point_2 += total_point_defect;
		check_defect_value = 1;
	} else if (document.getElementById('defect_code_value_' + id + '_3').checked == true) {
		is_selected_3 = true;
		total_point_defect = 3 * multiply;
		curr_point_3 += total_point_defect;
		check_defect_value = 1;
	} else if (document.getElementById('defect_code_value_' + id + '_4').checked == true) {
		is_selected_4 = true;
		total_point_defect = 4 * multiply;
		curr_point_4 += total_point_defect;
		check_defect_value = 1;
	}

	if (!defect_code) {
		$("#alert_info").trigger("click", 'Please insert defect code first.');
		return false;
	}

	if (check_defect_value == 0) {
		$("#alert_info").trigger("click", 'Please select point defect first');
		return false;
	}


	let input = 
	{
		point_check_from: point_check_from,
		point_check_to: _point_check_to,
		defect_code: defect_code.toUpperCase(),
		defect_value: defect_value,
		is_selected_1: is_selected_1,
		is_selected_2: is_selected_2,
		is_selected_3: is_selected_3,
		is_selected_4: is_selected_4,
		multiply: multiply,
		total_point_defect: total_point_defect,
		remark: (remark) ? remark.trim() : remark,
	}
	let total_point = (curr_point_1 + curr_point_2 + curr_point_3 + curr_point_4);
	setPercentage(total_point, id);
	data.point_1 = curr_point_1;
	data.point_2 = curr_point_2;
	data.point_3 = curr_point_3;
	data.point_4 = curr_point_4;
	data.total_all_point = total_point;
	list_barcode_header[id]['detail'].push(input);

	render();
}

function changeResult() {
	let i = $(this).data('id');
	let selected_item = $('#status_option_' + i).val();
	list_barcode_header[i].status = selected_item;
	render();
}

function deleteDetailPointYard() {
	let id = $(this).data('id');
	let idx = $(this).data('idx');
	let data = list_barcode_header[id];
	let detail = list_barcode_header[id]['detail'][idx];
	let curr_defect_value = detail.defect_value;
	let point_check_from = detail.point_check_from;
	let point_check_to = detail.point_check_to;
	let diff_check_point = parseInt((parseInt(point_check_to) - parseInt(point_check_from)) + 1);


	let curr_point_1 = parseInt(data.point_1);
	let curr_point_2 = parseInt(data.point_2);
	let curr_point_3 = parseInt(data.point_3);
	let curr_point_4 = parseInt(data.point_4);

	if (curr_defect_value == 1)
		curr_point_1 -= 1 * diff_check_point;
	else if (curr_defect_value == 2)
		curr_point_2 -= 2 * diff_check_point;
	else if (curr_defect_value == 3)
		curr_point_3 -= 3 * diff_check_point;
	else if (curr_defect_value == 4)
		curr_point_4 -= 4 * diff_check_point;

	let total_point = (curr_point_1 + curr_point_2 + curr_point_3 + curr_point_4);
	setPercentage(total_point, id);

	data.point_1 = curr_point_1;
	data.point_2 = curr_point_2;
	data.point_3 = curr_point_3;
	data.point_4 = curr_point_4;
	data.total_all_point = total_point;
	list_barcode_header[id]['detail'].splice(idx, 1);
	render();
}

function editDetailDefectValue() {
	let id = $(this).data('id');
	let idx = $(this).data('idx');
	let data = list_barcode_header[id];
	let detail = list_barcode_header[id]['detail'][idx];
	let curr_defect_value = detail.defect_value;
	let point_check_from = detail.point_check_from;
	let point_check_to = detail.point_check_to;
	let multiply = parseInt((parseInt(point_check_to) - parseInt(point_check_from)) + 1);


	let defect_value = $('input[name=defect_code_value_' + id + '_' + idx + ']:checked').val();

	let is_selected_1 = false;
	let is_selected_2 = false;
	let is_selected_3 = false;
	let is_selected_4 = false;

	let curr_point_1 = parseInt(data.point_1);
	let curr_point_2 = parseInt(data.point_2);
	let curr_point_3 = parseInt(data.point_3);
	let curr_point_4 = parseInt(data.point_4);

	let total_point_defect = 0;
	if (curr_defect_value == 1)
		curr_point_1 -= 1 * multiply;
	else if (curr_defect_value == 2)
		curr_point_2 -= 2 * multiply;
	else if (curr_defect_value == 3)
		curr_point_3 -= 3 * multiply;
	else if (curr_defect_value == 4)
		curr_point_4 -= 4 * multiply;

	if (document.getElementById('defect_code_value_' + id + '_' + idx + '_1').checked == true) {
		is_selected_1 = true;
		total_point_defect = 1 * multiply;
		curr_point_1 += total_point_defect;
	} else if (document.getElementById('defect_code_value_' + id + '_' + idx + '_2').checked == true) {
		is_selected_2 = true;
		total_point_defect = 2 * multiply;
		curr_point_2 += total_point_defect;
	} else if (document.getElementById('defect_code_value_' + id + '_' + idx + '_3').checked == true) {
		is_selected_3 = true;
		total_point_defect = 3 * multiply;
		curr_point_3 += total_point_defect;
	} else if (document.getElementById('defect_code_value_' + id + '_' + idx + '_4').checked == true) {
		is_selected_4 = true;
		total_point_defect = 4 * multiply;
		curr_point_4 += total_point_defect;
	}

	let total_point = (curr_point_1 + curr_point_2 + curr_point_3 + curr_point_4);
	setPercentage(total_point, id);

	data.point_1 = curr_point_1;
	data.point_2 = curr_point_2;
	data.point_3 = curr_point_3;
	data.point_4 = curr_point_4;
	data.total_all_point = total_point;


	detail.defect_value = defect_value;
	detail.is_selected_1 = is_selected_1;
	detail.is_selected_2 = is_selected_2;
	detail.is_selected_3 = is_selected_3;
	detail.is_selected_4 = is_selected_4;
	detail.multiply = multiply;
	detail.total_point_defect = total_point_defect;


	render();

}

function setPercentage(total_point, id) {
	let data = list_barcode_header[id];
	let qty = parseFloat(data.qty);

	let percentage = parseFloat((parseFloat(total_point) / qty) * 100).toFixed(2);
	data.percentage = percentage;

	if (percentage > 15.00) {
		data.status = 'REJECT';
	} else {
		data.status = 'RELEASE';
	}
}

function getIndex() 
{
	for (id in list_barcode_header) {
		list_barcode_header[id]['_id'] = id;
		list_barcode_header[id]['no'] = parseInt(id) + 1;


		for (let idy in list_barcode_header[id]['status_options']) {
			if (list_barcode_header[id]['status'] == list_barcode_header[id]['status_options'][idy]['id']) {
				list_barcode_header[id]['status_options'][idy]['selected'] = 'selected';
			} else {
				list_barcode_header[id]['status_options'][idy]['selected'] = '';
			}

		}

		let details = list_barcode_header[id]['detail'];

		for (let idx in details) {
			details[idx]['_idx'] = idx;
			details[idx]['nox'] = parseInt(idx) + 1;
		}
	}
}

function checkItem(barcode_val, index) {
	let n = barcode_val.includes("-");

	if (n)
		var _temp = barcode_val.split("-")[0];
	else
		var _temp = barcode_val;
	for (var i in list_barcode_header) {
		var barcode = list_barcode_header[i];
		if (i == index)
			continue;

		if (barcode.barcode == _temp) {
			return false;
		}

	}
	return true;
}

function tambahItem() 
{
	var warehouse_id 	= $('#select_warehouse').val();
	var barcode 		= $('#barcode_value').val();

	var url_fabric_material_quality_control_create 	= $('#url_fabric_material_quality_control_create').val();
	var diff 										= checkItem(barcode);

	if (!diff) 
	{
		$("#alert_warning").trigger("click", 'Barcode already scanned.');
		$("#barcode_value").val("");
		$(".barcode_value").focus();
		return;
	}

	$.ajax({
			type: "GET",
			url: url_fabric_material_quality_control_create,
			data: {
				barcode			: barcode,
				warehouse_id	: warehouse_id
			},
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			complete: function () {
				$.unblockUI();
			},
			success: function (response) {
				list_barcode_header.push(response);
				$('#barcode_value').val('');
				$('.barcode_value').focus();
			},
			error: function (response) {
				$.unblockUI();
				$('#barcode_value').val('');
				$('.barcode_value').focus();

				if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
				if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);
			}
		})
		.done(function () {
			render();
			$('.barcode_value').focus();

		});;
}

function deleteItem() 
{
	var i = $(this).data('id');
	var material_stock_id = list_barcode_header[i]['material_stock_id'];
	deleteTemporary(material_stock_id);
	list_barcode_header.splice(i, 1);
	render();
}

function deleteTemporary(id) 
{
	var url_destroy_temporary = $('#url_temporary_delete').attr('href');
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$.ajax({
		type: "DELETE",
		url: url_destroy_temporary,
		data: {
			id: id,
			status: 'qc-fabric'
		},
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		error: function (response) {
			$.unblockUI();
			if (response != '')
				for (i in response.responseJSON) {
					$("#alert_error").trigger("click", response.responseJSON[i]);
				}
			else
				$("#alert_error").trigger("click", 'Please Contact ICT');
		}
	});
}

function setFocusToTextBox() 
{
	$('#barcode_value').focus();
}

function checkInput() 
{
	for (var i in list_barcode_header) {
		if (list_barcode_header[i].status == "REJECT") {
			var inputan = parseFloat(list_barcode_header[i].qty_check).toFixed(2);
			if (inputan <= 0) {
				return list_barcode_header[i].barcode;
			}
		}
	}
	return false;
}

function checkActualWidth() 
{
	let findResult = false;
	for (let i in list_barcode_header) {
		if (list_barcode_header[i].actual_width == 0 || list_barcode_header[i].actual_width == null) {
			findResult = true;
			break;
		}
	}
	return findResult;
}

$('#is_release').click(function () 
{
	var _is_release 	= $('#_is_release').val();
	var is_release 	= $('#is_release').val();
	if (_is_release == 0) 
	{status_release
		$('#_is_release').val('1');
		$("#is_release").css("background-color", '#ffd400');
		$("#is_release").css("color", 'white');
		$("#status_release").val('release');
		
	} else 
	{
		$('#_is_release').val('0');
		$("#is_release").css("background-color", '#f5f5f5');
		$("#is_release").css("color", 'black');
		$("#status_release").val('');
	}
	render();
});