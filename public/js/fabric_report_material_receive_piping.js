$(function()
{
    $('#select_warehouse').trigger('change');
    $('#fabric_report_material_receive_piping_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/fabric/report/material-receive-piping/data',
            data: function(d) {
                return $.extend({}, d, {
                    "warehouse"     : $('#select_warehouse').val(),
                    "start_date": $('#start_date').val(),
                    "end_date": $('#end_date').val(),
                    "status": $('#select_status').val(),
                });
           }
        },
        columns: [
            {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
            {data: 'planning_date', name: 'planning_date',searchable:true,visible:true,orderable:true},
            {data: 'barcode', name: 'barcode',searchable:true,visible:true,orderable:true},
            {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
            {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'nomor_roll', name: 'nomor_roll',searchable:true,visible:true,orderable:true},
            {data: 'batch_number', name: 'batch_number',searchable:true,visible:true,orderable:true},
            {data: 'color', name: 'color',searchable:false,visible:true,orderable:true},
            {data: 'lot', name: 'uom',searchable:false,visible:true,orderable:false},
            {data: 'qty', name: 'qty',searchable:true,visible:true,orderable:false},
            {data: 'received_piping_date', name: 'received_piping_date',searchable:true,visible:true,orderable:false},
            {data: 'name', name: 'name',searchable:true,visible:true,orderable:false},
            {data: 'last_status_movement', name: 'last_status_movement',searchable:true,visible:true,orderable:false},
        ]
    });

    var dtable = $('#fabric_report_material_receive_piping_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();
    
    $('#select_warehouse').on('change',function(){
        dtable.draw();
    });
    
    $('#start_date').on('change', function () {
        dtable.draw();
    });

    $('#end_date').on('change', function () {
        dtable.draw();
    });

    $('#select_status').on('change', function () {
        dtable.draw();
    });
});