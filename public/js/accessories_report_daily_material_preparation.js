$(function()
{
    $('#daily_material_preparation_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/accessories/report/daily-material-preparation/data',
            data: function(d) {
                return $.extend({}, d, {
                    "warehouse"     : $('#select_warehouse').val(),
                    "start_date"    : $('#start_date').val(),
                    "end_date"      : $('#end_date').val(),
                });
           }
        },
        columns: [
            {data: 'created_at', name: 'created_at',searchable:false,visible:true,orderable:true},
            {data: 'pic_receive', name: 'pic_receive',searchable:false,visible:true,orderable:true},
            {data: 'warehouse', name: 'warehouse',searchable:true,visible:true,orderable:true},
            {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
            {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
            {data: 'po_buyer', name: 'po_buyer',searchable:true,visible:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'category', name: 'category',searchable:true,visible:true,orderable:true},
            {data: 'style', name: 'style',searchable:true,visible:true,orderable:true},
            {data: 'article_no', name: 'article_no',searchable:true,visible:true,orderable:true},
            {data: 'uom_conversion', name: 'uom_conversion',searchable:true,visible:true,orderable:false},
            {data: 'qty_conversion', name: 'qty_conversion',searchable:false,visible:true,orderable:false},
            {data: 'qc_status', name: 'qc_status',searchable:false,visible:true,orderable:false},
            {data: 'qc_checker', name: 'qc_checker',searchable:false,visible:true,orderable:false},
            {data: 'last_status_movement', name: 'last_status_movement',searchable:false,visible:true,orderable:false},
            {data: 'code', name: 'code',searchable:false,visible:true,orderable:false},
            {data: 'last_movement_date', name: 'last_movement_date',searchable:false,visible:true,orderable:false},
	    ]
    });

    var dtable = $('#daily_material_preparation_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();
    
    $('#select_warehouse').on('change',function(){
        dtable.draw();
    });

    $('#start_date').on('change',function(){
        dtable.draw();
    });

    $('#end_date').on('change',function(){
        dtable.draw();
    });
})