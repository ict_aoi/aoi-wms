$(function()
{
    var page = $('#page').val();
	itemLov('erpItemPicklist', '/master-data/item/erp-item-picklist?');
		
	
    if(page == 'index')
    {
        $('#master_data_item_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            scroller:true,
            deferRender:true,
            ajax: {
                type: 'GET',
				url: '/master-data/item/data',
				data: function(d) {
                    return $.extend({}, d, {
                        "category"       : $('#select_category').val()
                    });
               }
            },
            columns: [
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'item_id', name: 'item_id',searchable:true,visible:false,orderable:true},
				{data: 'upc', name: 'upc',searchable:true,visible:true,orderable:true},
				{data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
				{data: 'item_desc', name: 'item_desc',searchable:false,visible:true,orderable:true},
				{data: 'category', name: 'category',searchable:false,visible:true,orderable:true},
				{data: 'composition', name: 'composition',searchable:false,visible:true,orderable:true},
				{data: 'uom', name: 'uom',searchable:false,visible:true,orderable:true},
				{data: 'action', name: 'action',searchable:false,visible:true,orderable:true},
            ]
        });
    
        var dtable = $('#master_data_item_table').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
		dtable.draw();
		
		$('#select_category').on('change',function(){
			dtable.draw();
		});
    }
});

function itemLov(name, url) 
{
    var search      = '#' + name + 'Search';
    var list        = '#' + name + 'List';
    var item_id     = '#' + name + 'Id';
    var item_name   = '#' + name + 'Name';
    var modal       = '#' + name + 'Modal';
    var table       = '#' + name + 'Table';
    var buttonSrc   = '#' + name + 'ButtonSrc';
    var buttonDel   = '#' + name + 'ButtonDel';

    function itemAjax() 
    {
        var q = $(search).val();
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
            url: url + '&q=' + q
        })
        .done(function (data) {
            $(table).html(data);
            pagination(name);
            $(search).focus();
            $(search).val('');
            $(table).removeClass('hidden');
            $(modal).find('.shade-screen').addClass('hidden');
            $(modal).find('.form-search').removeClass('hidden');

            $(table).find('.btn-choose').on('click', chooseItem);
        });
    }

    function chooseItem() 
    {
        var id              = $(this).data('id');
       
        $.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
			type: "post",
			url: '/master-data/item/store',
			data : {
				item_id : id
			},
			beforeSend: function () 
			{
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function () {
				$.unblockUI();
			},
			error: function () {
				$.unblockUI();
				$("#alert_error").trigger("click", 'Please contact ict.');
			},
		}).done(function () {
			$("#alert_success").trigger("click", 'Data successfully synchronized.');
			$('#master_data_item_table').DataTable().ajax.reload();
		});
    }

    function pagination() 
    {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(search).val("");
    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) 
    {
        if (e.keyCode == 13)
            itemAjax();
    });

    itemAjax();
}

function uomConversion(url)
{
    $('#detailModal').modal();
    $('#master_data_uom_conversion_table').DataTable().destroy();
    $('#master_data_uom_conversion_table tbody').empty();
    
    $('#master_data_uom_conversion_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        ajax: {
            type: 'GET',
            url: url
        },
        columns: [
            {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'item_desc', name: 'item_desc',searchable:false,visible:true,orderable:true},
            {data: 'uom_from', name: 'uom_from',searchable:false,visible:true,orderable:true},
            {data: 'uom_to', name: 'uom_to',searchable:false,visible:true,orderable:true},
            {data: 'dividerate', name: 'dividerate',searchable:false,visible:true,orderable:true},
            {data: 'multiplyrate', name: 'multiplyrate',searchable:false,visible:true,orderable:true},
        ]
    });

    var dtable = $('#master_data_uom_conversion_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
}