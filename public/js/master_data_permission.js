
$(function () 
{
    var page = $('#page').val();

    if(page == 'index')
    {
        var flag_msg = $('#flag_msg').val();
        if (flag_msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');
        else if (flag_msg == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated.');
        
        
        $('#master_data_permission_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            scroller:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/master-data/user-management/permission/data',
            },
            columns: [
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'display_name', name: 'display_name',searchable:true,visible:true,orderable:true},
                {data: 'description', name: 'description',searchable:false,visible:true,orderable:false},
                {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
            ]
        });
    
        var dtable = $('#master_data_permission_table').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
		dtable.draw();
		
    }else
    {
        list_material_saving = JSON.parse($('#material-saving').val());
        render();
    }
})

function hapus(url) 
{
	bootbox.confirm("Are you sure want to delete this data ?.", function (result) {
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: "DELETE",
				url: url,
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function () 
				{
					$.unblockUI();
				},
			})
			.done(function ($result) {
				$("#alert_success").trigger("click", 'Data successfully deleted.');
				$('#master_data_permission_table').DataTable().ajax.reload();
			});
		}
	});
}

$('#form').submit(function (event) 
{
	event.preventDefault();
	var name = $('#name').val();

	if (!name) 
	{
		$("#alert_warning").trigger("click", 'Please insert name first.');
		return false;
	}

	bootbox.confirm("Are you sure want to save this data ?.", function (result) 
	{
		if (result) {
			$.ajax({
				type: "POST",
				url: $('#form').attr('action'),
				data: $('#form').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
					

				},
				error: function (response) {
					$.unblockUI();
					if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
					
				}
			})
			.done(function () {
				var url                 = $('#url_master_data_permission').attr('href');
            	document.location.href  = url;
			});
		}
	});
});