$(function()
{
    $('#report_material_bapb_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/accessories/report/material-bapb/data',
            data: function(d) {
                return $.extend({}, d, {
                    "warehouse"     : $('#select_warehouse').val(),
                    "start_date"    : $('#start_date').val(),
                    "end_date"      : $('#end_date').val(),
                });
           }
        },
        columns: [
            {data: 'created_at', name: 'created_at',searchable:true,visible:true,orderable:false},
            {data: 'name', name: 'name',searchable:false,visible:true,orderable:true},
            {data: 'document_no', name: 'document_no',searchable:false,visible:true,orderable:true},
            {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
            {data: 'locator', name: 'locator',searchable:true,visible:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'item_desc', name: 'item_desc',searchable:true,visible:true,orderable:true},
            {data: 'category', name: 'category',searchable:true,visible:true,orderable:true},
            {data: 'uom', name: 'uom',searchable:false,visible:true,orderable:false},
            {data: 'qty_booking', name: 'qty_booking',searchable:false,visible:true,orderable:false},
            {data: 'remark', name: 'remark',searchable:false,visible:true,orderable:false}
	    ]
    });

    var dtable = $('#report_material_bapb_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();
    
    $('#select_warehouse').on('change',function(){
        dtable.draw();
    });

    $('#start_date').on('change',function(){
        dtable.draw();
    });

    $('#end_date').on('change',function(){
        dtable.draw();
    });;
});