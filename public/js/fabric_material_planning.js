$(function () 
{
	var fabricPlanningTable = $('#fabric_material_planning_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/fabric/material-planning/data',
            data: function(d) {
                return $.extend({}, d, {
                    "warehouse"    	: $('#select_warehouse').val(),
                    "start_date"    : $('#start_date').val(),
                    "end_date"      : $('#end_date').val(),
                });
		   },
		},
		fnCreatedRow: function (row, data, index) {
			var info = fabricPlanningTable.page.info();
			var value = index+1+info.start;
			$('td', row).eq(0).html(value);
		},
		columns: [
			{data: null, sortable: false, orderable: false, searchable: false},
			{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
			{data: 'planning_date', name: 'planning_date',searchable:true,visible:true,orderable:false},
            {data: 'po_buyer', name: 'po_buyer',searchable:true,visible:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'color', name: 'color',searchable:true,visible:true,orderable:true},
            {data: 'article_no', name: 'article_no',searchable:true,visible:true,orderable:true},
            {data: 'style', name: 'style',searchable:true,visible:true,orderable:true},
            {data: 'is_piping', name: 'is_piping',searchable:true,visible:true,orderable:true},
            {data: 'qty_consumtion', name: 'qty_consumtion',searchable:true,visible:true,orderable:true}
	    ]
    });

    var dtable = $('#fabric_material_planning_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();

	$('#getPlanningCutting').submit(function (event)
	{
		event.preventDefault();
		$.ajax({
			type: "post",
			url: $('#getPlanningCutting').attr('action'),
			data: $('#getPlanningCutting').serialize(),
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			complete: function () {
				$.unblockUI();
			},
			success: function (response) 
			{
				
				if (response.total == 0)
				{
					$.unblockUI();
					$("#alert_info").trigger("click", 'Planning not found');
					return false;
				}
				
				var dtable = $('#fabric_material_planning_table').dataTable().api();
				dtable.draw();
				$.unblockUI();
			},
			error: function (response) 
			{
				$.unblockUI();

				if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
				if (response['status'] == 400) $("#alert_warning").trigger("click", 'Please check planning cutting date first');
			}
		});
	});
});

$('#select_warehouse').on('change',function(){
	var warehouse_id = $(this).val();
	$('#warehouse_id').val(warehouse_id);
});

$('#start_date').on('change',function(){
	var start_date =  $('#start_date').val();
	$('#_start_date').val(start_date);
});

$('#end_date').on('change',function(){
	var end_date =  $('#end_date').val();
	$('#_end_date').val(end_date);
});

