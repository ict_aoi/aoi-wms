$(function()
{
	poBuyerLov('old_buyer','/master-data/reroute/buyer-picklist?');
	poBuyerLov('new_buyer', '/master-data/reroute/buyer-picklist?');

	var page = $('#page').val();

	if(page == 'index')
	{
		$('#master_data_reroute_buyer').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            scroller:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/master-data/reroute/data',
            },
            columns: [
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'old_po_buyer', name: 'old_po_buyer',searchable:true,visible:true,orderable:true},
                {data: 'new_po_buyer', name: 'new_po_buyer',searchable:true,visible:true,orderable:false},
				{data: 'note', name: 'note',searchable:false,visible:true,orderable:false},
                {data: 'is_prefix', name: 'is_prefix',searchable:false,visible:true,orderable:false},
                {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
            ]
        });
    
        var dtable = $('#master_data_reroute_buyer').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
		dtable.draw();
	}else
	{
		data_reroutes = JSON.parse($('#data_reroutes').val());
		render();
	}
});


$('#upload_button').on('click', function () 
{
	$('#upload_file').trigger('click');
});

$('#upload_file').on('change', function () 
{

	$.ajax({
		type: "POST",
		url: $('#upload_file_reroute').attr('action'),
		data: new FormData(document.getElementById("upload_file_reroute")),
		processData: false,
		contentType: false,
		beforeSend: function () {
			$.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) 
		{
			console.log(response);
			$("#alert_info").trigger("click", 'Upload successfully.');
			for(idx in response)
			{
				var input = response[idx];
				data_reroutes.push(input);
			}
		},
		error: function (response) {
			$.unblockUI()
			$('#upload_file_allocation').trigger('reset');
			if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT.');
			if (response['status'] == 422) $("#alert_error").trigger("click",response['responseJSON']['message']);
		}
	})
	.done(function(){
		render();
	});

});

$('#form').submit(function (event) 
{
	event.preventDefault();

	bootbox.confirm("Are you sure want to reroute this data ?", function (result) {
		if (result) {
			$.ajax({
				type: "POST",
				url: $('#form').attr('action'),
				data: $('#form').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				complete: function () {
					$.unblockUI();
				},
				success: function (response) {

					$("#alert_success").trigger("click", 'data berhasil disimpan');
					$('#form').trigger('reset');

					$('#master_data_reroute_buyer').DataTable().ajax.reload();

				},
				error: function (response) {
					$.unblockUI();
					if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
					if (response['status'] == 422) $("#alert_error").trigger("click", response.responseJSON);
					
				}
			});
		}
	});
});

function render() 
{
	getIndex();
	$('#data_reroutes').val(JSON.stringify(data_reroutes));
	var tmpl = $('#reroute-table').html();
	Mustache.parse(tmpl);
	var data = { item: data_reroutes };
	var html = Mustache.render(tmpl, data);
	$('#tbody-upload-reroute').html(html);
}

function getIndex() 
{
	for (idx in data_reroutes) {
		data_reroutes[idx]['_id'] = idx;
		data_reroutes[idx]['no'] = parseInt(idx) + 1;
	}
}

function refresh(url) 
{
	bootbox.confirm("Are you sure want refresh this data ?.", function (result) 
	{
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: "post",
				url: url,
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function () {
					$.unblockUI();
				},
				error: function (response) {
					$.unblockUI();
					if (response['status'] == 422) $("#alert_info").trigger("click", response['responseJSON']);
				}
			})
			.done(function () {
				$("#alert_success").trigger("click", 'Data successfully refreshed');
				$('#master_data_reroute_buyer').DataTable().ajax.reload();
				
			});
		}
	});;
}

function poBuyerLov(name, url) 
{
	var search = '#' + name + 'Search';
	var list = '#' + name + 'List';
	var item_id = '#' + name + 'Id';
	var item_name = '#' + name + 'Name';
	var modal = '#' + name + 'Modal';
	var table = '#' + name + 'Table';
	var buttonSrc = '#' + name + 'ButtonSrc';
	var buttonDel = '#' + name + 'ButtonDel';

	function itemAjax() {
		var q = $(search).val();
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&q=' + q
		})
		.done(function (data) {
			$(table).html(data);
			pagination(name);
			$(search).focus();
			$(search).val('');
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem() 
	{
		var id = $(this).data('id');
		var name = $(this).data('name');


		$(item_id).val(id);
		$(item_name).val(name);

	}

	function pagination() 
	{
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	$(search).val("");
	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) 
	{
		if (e.keyCode == 13)
			itemAjax();
	});

	$(buttonDel).on('click', function () 
	{
		$(item_id).val('');
		$(item_name).val('');

	});

	itemAjax();
}


