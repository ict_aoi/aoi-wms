$(function () 
{
    var page = $('#page').val();

    if(page == 'index')
    {
        var flag_msg = $('#flag_msg').val();
        if (flag_msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');
        else if (flag_msg == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated.');
        
        
        $('#master_data_mapping_stock_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            scroller:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/master-data/mapping-stock/data',
            },
            columns: [
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'document_number', name: 'document_number',searchable:true,visible:true,orderable:true},
                {data: 'type_stock_erp_code', name: 'type_stock_erp_code',searchable:false,visible:true,orderable:false},
                {data: 'type_stock', name: 'type_stock',searchable:true,visible:true,orderable:true},
                {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
            ]
        });
    
        var dtable = $('#master_data_mapping_stock_table').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();
        
        $('#select_warehouse').on('change',function()
        {
            var warehouse_id = $(this).val();
            $('#_warehouse_id').val(warehouse_id);
            dtable.draw();
        });
    
        $('#start_date').on('change',function()
        {
            var start_date = $(this).val();
            $('#_start_date').val(start_date);
            dtable.draw();
        });

        $('#end_date').on('change',function()
        {
            var end_date = $(this).val();
           $('#_end_date').val(end_date);
            dtable.draw();
        });
        
    }else
    {
        list_material_saving = JSON.parse($('#material-saving').val());
        render();
    }
})

function hapus(url) 
{
	bootbox.confirm("Are you sure want to save this data ?.", function (result) {
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: "DELETE",
				url: url,
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function () {
					$.unblockUI();
				},
			})
			.done(function () {
				$("#alert_success").trigger("click", 'Data successfully deleted.');
				$('#master_data_mapping_stock_table').DataTable().ajax.reload();
			});
		}
	});


}

function render() 
{
    $('#material-saving').val(JSON.stringify(list_material_saving));

    var tmpl = $('#material-saving-table').html();
    Mustache.parse(tmpl);
    var data = { item: list_material_saving };
    var html = Mustache.render(tmpl, data);
    $('#tbody-upload-material-saving').html(html);
}

$('#upload_button').on('click', function () 
{
    $('#upload_file').trigger('click');
});


$('#upload_file').on('change', function () 
{
    //$('#upload_file_material_saving').submit();
    $.ajax({
        type: "post",
        url: $('#upload_file_material_saving').attr('action'),
        data: new FormData(document.getElementById("upload_file_material_saving")),
        processData: false,
        contentType: false,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            $("#alert_info").trigger("click", 'Upload successfully.');
            for (idx in response) 
            {
                var data = response[idx];
                var input = {
                    'pcd'           : data.pcd,
                    'style'         : data.style,
                    'article'       : data.article,
                    'uom'           : data.uom,
                    'item_code'     : data.item_code,
                    'po_supplier'   : data.po_supplier,
                    'qty_saving'    : data.qty_saving,
                    'warehouse'     : data.warehouse,
                    'is_error'      : data.is_error,
                    'status'        : data.status,
                };
                
                list_material_saving.push(input);
            }

        },
        error: function (response) 
        {
            $.unblockUI();
            $('#upload_file_material_saving').trigger('reset');
            if (response['status'] == 500)  $("#alert_error").trigger("click", 'Please Contact ICT.');
            if (response['status'] == 422) $("#alert_error").trigger("click", response.responseJSON);

        }
    })
    .done(function () {
        $('#upload_file_material_saving').trigger('reset');
        render();
    });

})

$('#form').submit(function (event) 
{
	event.preventDefault();
	var document_number     = $('#document_number').val();
    var type_stock_erp_code = $('#type_stock_erp_code').val();
    var type_stock          = $('#type_stock').val();

    if (!document_number) 
    {
		$("#alert_warning").trigger("click", 'Please insert document number first');
		return false;
	}

    if (!type_stock_erp_code) 
    {
		$("#alert_warning").trigger("click", 'Please insert type stock erp code first.');
		return false;
    }

    if (!type_stock) 
    {
		$("#alert_warning").trigger("click", 'Please insert type stock first.');
		return false;
	}

	bootbox.confirm("Are you sure want to save this data ?.", function (result) {
		if (result) {
			$.ajax({
				type: "POST",
				url: $('#form').attr('action'),
				data: $('#form').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
                error: function (response) 
                {
					$.unblockUI();
					if (response['status'] == 500){
						$("#alert_error").trigger("click", 'Please Contact ICT');
					}else{
						for (i in response.responseJSON) {
							$("#alert_error").trigger("click", response.responseJSON[i]);
						}
					}
				}
			})
			.done(function () {
				var url                 = $('#url_master_data_mapping_stock').attr('href');
                document.location.href  = url;
			});
		}
	});
});
