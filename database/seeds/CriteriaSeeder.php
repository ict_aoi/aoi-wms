<?php

use Illuminate\Database\Seeder;
use App\Models\Criteria;
use App\Models\User;

class CriteriaSeeder extends Seeder
{
    public function run()
    {
        $user_acc_aoi1 = User::where('name','Admin ICT ACC AOI1')->first();
        $user_acc_aoi2 = User::where('name','Admin ICT ACC AOI2')->first();
        
        $criteria_know_data_source = [
            'CANCEL ORDER',
            'PERUBAHAN ITEM',
            'REDUCE QTY',
            'REMOVE',
            'REMOVE COLOR',
            'REMOVE ITEM',
            'PFAO'
        ];

        foreach ($criteria_know_data_source as $key => $criteria) {
            Criteria::create([
                'name' => $criteria,
                'warehouse' => $user_acc_aoi1->warehouse,
                'is_know_data_source' => true,
                'is_active' => true,
                'user_id' => $user_acc_aoi1->id
            ]);
        }

        foreach ($criteria_know_data_source as $key => $criteria) {
            Criteria::create([
                'name' => $criteria,
                'warehouse' => $user_acc_aoi2->warehouse,
                'is_know_data_source' => true,
                'is_active' => true,
                'user_id' => $user_acc_aoi2->id
            ]);
        }

        $criteria_dont_know_data_source = [
            'RETUR',
            'ALLOWANCE',
        ];

        foreach ($criteria_dont_know_data_source as $key => $criteria) {
            Criteria::create([
                'name' => $criteria,
                'warehouse' => $user_acc_aoi2->warehouse,
                'is_know_data_source' => false,
                'is_active' => true,
                'user_id' => $user_acc_aoi2->id
            ]);
        }

        foreach ($criteria_dont_know_data_source as $key => $criteria) {
            Criteria::create([
                'name' => $criteria,
                'warehouse' => $user_acc_aoi1->warehouse,
                'is_know_data_source' => false,
                'is_active' => true,
                'user_id' => $user_acc_aoi1->id
            ]);
        }
    }
}
