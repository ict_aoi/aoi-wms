<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class UserSeeder extends Seeder
{
    public function run()
    {
        $roles  = Role::Select('id')->get();

        $user = new User();
        $user->name = 'Admin ICT ACC AOI2';
        $user->nik = '11111111';
        //$user->factory = 2;
        $user->division = 'ICT';
        $user->warehouse = '1000013';
        $user->email = 'adminict.acc_aoi2@aoi.co.id';
        $user->password =  bcrypt('password1');

        if($user->save())
            $user->attachRoles($roles);   

        $user2 = new User();
        $user2->name = 'Admin ICT ACC AOI1';
        $user2->nik = '22222222';
        $user2->division = 'ICT';
        //$user2->factory = 2;
        $user2->warehouse = '1000002';
        $user2->email = 'adminict.acc_aoi1@aoi.co.id';
        $user2->password =  bcrypt('password1');

        if($user2->save())
            $user2->attachRoles($roles);   

        $user3 = new User();
        $user3->name = 'Admin ICT FEBRIC AOI1';
        $user3->nik = '33333333';
        //$user3->factory = 1;
        $user3->division = 'ICT';
        $user3->warehouse = '1000001';
        $user3->email = 'adminict.febric_aoi1@aoi.co.id';
        $user3->password =  bcrypt('password1');

        if($user3->save())
            $user3->attachRoles($roles);
            
        $user3 = new User();
        $user3->name = 'Admin ICT FEBRIC AOI2';
        $user3->nik = '44444444';
        //$user3->factory = 1;
        $user3->division = 'ICT';
        $user3->warehouse = '1000011';
        $user3->email = 'adminict.febric_aoi2@aoi.co.id';
        $user3->password =  bcrypt('password1');

        if($user3->save())
            $user3->attachRoles($roles);
    }
}
