<?php

use Illuminate\Database\Seeder;
use App\Models\TrimCrad;

class TrimCardTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $trim_card = TrimCard::Create([
            'style' => 'S19P23019M',
            'job_order' => 'S19P23019M::0121945594::DU9582',
            'po_buyer' => '0121945594',
            'article_no' => 'DU9582',
            'season' => 'SS19',
            'user_id' => '1',
        ]);
    }
}
