<?php

use Illuminate\Database\Seeder;
use App\Models\Area;
use App\Models\User;


class AreaSeeder extends Seeder
{
    public function run()
    {
        /**
         * 
         * ACCESSORIES
         * 
         */
        
        $user_acc_aoi1 = User::where('name','Admin ICT ACC AOI1')->first();
        $user_acc_aoi2 = User::where('name','Admin ICT ACC AOI2')->first();
        
        $areas_accessories = [
            'RECEIVING',
            'PREPARATION',
            'REJECT',
            'FREE STOCK'
        ];

        foreach ($areas_accessories as $key => $area) {
            Area::create([
                'name' => $area,
                'warehouse' => $user_acc_aoi1->warehouse,
                'type_po' => 2,
                'user_id' => $user_acc_aoi1->id
            ]);
        }

        foreach ($areas_accessories as $key => $area) {
            Area::create([
                'name' => $area,
                'warehouse' => $user_acc_aoi2->warehouse,
                'type_po' => 2,
                'user_id' => $user_acc_aoi2->id
            ]);
        }

        Area::create([
            'name' => 'DISTRIBUTION',
            'warehouse' => $user_acc_aoi1->warehouse,
            'is_destination' => true,
            'user_id' => $user_acc_aoi1->id,
            'type_po' => 2,
        ]);

        Area::create([
            'name' => 'DISTRIBUTION',
            'warehouse' => $user_acc_aoi2->warehouse,
            'is_destination' => true,
            'user_id' => $user_acc_aoi2->id,
            'type_po' => 2,
        ]);

        Area::create([
            'name' => 'SUBCONT',
            'warehouse' => $user_acc_aoi1->warehouse,
            'is_destination' => true,
            'is_subcont' => TRUE,
            'user_id' => $user_acc_aoi1->id,
            'type_po' => 2,
        ]);

        Area::create([
            'name' => 'SUBCONT',
            'warehouse' => $user_acc_aoi2->warehouse,
            'is_destination' => true,
            'is_subcont' => TRUE,
            'user_id' => $user_acc_aoi2->id,
            'type_po' => 2,
        ]);

         /**
         * 
         * FEBRIC
         * 
         */

        $user_febric_aoi1 = User::where('name','Admin ICT FEBRIC AOI1')->first();
        $user_febric_aoi2 = User::where('name','Admin ICT FEBRIC AOI2')->first();
        

        $areas_febric = [
            'RECEIVING',
            'ARRIVAL',
            'INVENTORY',
        ];

        foreach ($areas_febric as $key => $area) {
            Area::create([
                'name' => $area,
                'warehouse' => $user_febric_aoi1->warehouse,
                'type_po' => 1,
                'user_id' => $user_febric_aoi1->id
            ]);
        }

        foreach ($areas_febric as $key => $area) {
            Area::create([
                'name' => $area,
                'warehouse' => $user_febric_aoi2->warehouse,
                'type_po' => 1,
                'user_id' => $user_febric_aoi2->id
            ]);
        }

        Area::create([
            'name' => 'DISTRIBUTION',
            'warehouse' => $user_febric_aoi1->warehouse,
            'is_destination' => true,
            'user_id' => $user_febric_aoi1->id,
            'type_po' => 1,
        ]);

        Area::create([
            'name' => 'DISTRIBUTION',
            'warehouse' => $user_febric_aoi2->warehouse,
            'is_destination' => true,
            'user_id' => $user_febric_aoi2->id,
            'type_po' => 1,
        ]);

        Area::create([
            'name' => 'SUBCONT',
            'warehouse' => $user_febric_aoi1->warehouse,
            'is_destination' => true,
            'is_subcont' => TRUE,
            'user_id' => $user_febric_aoi1->id,
            'type_po' => 1,
        ]);

        Area::create([
            'name' => 'SUBCONT',
            'warehouse' => $user_febric_aoi2->warehouse,
            'is_destination' => true,
            'is_subcont' => TRUE,
            'user_id' => $user_febric_aoi2->id,
            'type_po' => 1,
        ]);

      
    }
}
