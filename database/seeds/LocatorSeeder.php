<?php

use Illuminate\Database\Seeder;
use App\Models\Area;
use App\Models\User;
use App\Models\Locator;


class LocatorSeeder extends Seeder
{
   public function run()
    {
         /**
         * 
         * ACCESSORIES
         * 
         */
        
        $user_acc_aoi1 = User::where('name','Admin ICT ACC AOI1')->first();
        $user_acc_aoi2 = User::where('name','Admin ICT ACC AOI2')->first();

        $preparation_acc_aoi1 = Area::where([
            ['name','=','PREPARATION'],
            ['warehouse','=','1000002'],
        ])
        ->first();

        $preparation_acc_aoi2 = Area::where([
            ['name','=','PREPARATION'],
            ['warehouse','=','1000013']
        ])
        ->first();

        $free_stock_acc_aoi1 = Area::where([
            ['name','=','FREE STOCK'],
            ['warehouse','=','1000002']
        ])
        ->first();

        $free_stock_acc_aoi2 = Area::where([
            ['name','=','FREE STOCK'],
            ['warehouse','=','1000013']
        ])
        ->first();

        $receiving_acc_aoi1 = Area::where([
            ['name','=','RECEIVING'],
            ['warehouse','=','1000002']
        ])
        ->first();

        $receiving_acc_aoi2 = Area::where([
            ['name','=','RECEIVING'],
            ['warehouse','=','1000013']
        ])
        ->first();

        $distribution_acc_aoi1 = Area::where([
            ['name','=','DISTRIBUTION'],
            ['warehouse','=','1000002']
        ])
        ->first();

        $distribution_acc_aoi2 = Area::where([
            ['name','=','DISTRIBUTION'],
            ['warehouse','=','1000013']
        ])
        ->first();

        $subcont_acc_aoi1 = Area::where([
            ['name','=','SUBCONT'],
            ['warehouse','=','1000002']
        ])
        ->first();

        $subcont_acc_aoi2 = Area::where([
            ['name','=','SUBCONT'],
            ['warehouse','=','1000013']
        ])
        ->first();
        
        $locators_a = array();
        for ($i=0; $i < 16 ; $i++) { 
            for ($j=0; $j < 20; $j++) { 
                $locators_a [] = [
                    'code' => 'PREPARATION-'.'A'.'.'.($i+1).'.'.($j+1),
                    'rack' => 'A',
                    'y_row' => $i+1,
                    'z_column' => $j+1
                ];
            }
        }

        foreach ($locators_a as $key => $locator) {
            Locator::create([
                'code' => $locators_a[$key]['code'],
                'area_id' => $preparation_acc_aoi1->id,
                'rack' => $locators_a[$key]['rack'],
                'y_row' => $locators_a[$key]['y_row'],
                'z_column' => $locators_a[$key]['z_column'],
                'user_id' => $user_acc_aoi1->id
             ]);
        }

        foreach ($locators_a as $key => $locator) {
            Locator::create([
                'code' => $locators_a[$key]['code'],
                'area_id' => $preparation_acc_aoi2->id,
                'rack' => $locators_a[$key]['rack'],
                'y_row' => $locators_a[$key]['y_row'],
                'z_column' => $locators_a[$key]['z_column'],
                'user_id' => $user_acc_aoi2->id
             ]);
        }

        $locators_b = array();
        for ($i=0; $i < 16 ; $i++) { 
            for ($j=0; $j < 20; $j++) { 
                $locators_b [] = [
                    'code' => 'PREPARATION-'.'B'.'.'.($i+1).'.'.($j+1),
                    'rack' => 'B',
                    'y_row' => $i+1,
                    'z_column' => $j+1
                ];
            }
        }

        foreach ($locators_b as $key => $locator) {
            Locator::create([
                'code' => $locators_b[$key]['code'],
                'area_id' => $preparation_acc_aoi1->id,
                'rack' => $locators_b[$key]['rack'],
                'y_row' => $locators_b[$key]['y_row'],
                'z_column' => $locators_b[$key]['z_column'],
                'user_id' => $user_acc_aoi1->id
             ]);
        }

        foreach ($locators_b as $key => $locator) {
            Locator::create([
                'code' => $locators_b[$key]['code'],
                'area_id' => $preparation_acc_aoi2->id,
                'rack' => $locators_b[$key]['rack'],
                'y_row' => $locators_b[$key]['y_row'],
                'z_column' => $locators_b[$key]['z_column'],
                'user_id' => $user_acc_aoi2->id
             ]);
        }

        $locators_c = array();
         for ($i=0; $i < 16 ; $i++) { 
            for ($j=0; $j < 20; $j++) { 
                $locators_c [] = [
                    'code' => 'PREPARATION-'.'C'.'.'.($i+1).'.'.($j+1),
                    'rack' => 'C',
                    'y_row' => $i+1,
                    'z_column' => $j+1
                ];
            }
        }

        foreach ($locators_c as $key => $locator) {
            Locator::create([
                'code' => $locators_c[$key]['code'],
                'area_id' => $preparation_acc_aoi1->id,
                'rack' => $locators_c[$key]['rack'],
                'y_row' => $locators_c[$key]['y_row'],
                'z_column' => $locators_c[$key]['z_column'],
                'user_id' => $user_acc_aoi1->id
             ]);
        }

        foreach ($locators_c as $key => $locator) {
            Locator::create([
                'code' => $locators_c[$key]['code'],
                'area_id' => $preparation_acc_aoi2->id,
                'rack' => $locators_c[$key]['rack'],
                'y_row' => $locators_c[$key]['y_row'],
                'z_column' => $locators_c[$key]['z_column'],
                'user_id' => $user_acc_aoi2->id
             ]);
        }
        
        $free_stock_array_acc = array();
        for ($i=0; $i < 16 ; $i++) { 
            for ($j=0; $j < 20; $j++) { 
                $free_stock_array_acc [] = [
                    'code' => 'FREE STOCK-'.'H'.'.'.($i+1).'.'.($j+1),
                    'rack' => 'H',
                    'y_row' => $i+1,
                    'z_column' => $j+1
                ];
            }
        }

        foreach ($free_stock_array_acc as $key => $value) {
            Locator::create([
                'code' => $value['code'],
                'area_id' => $free_stock_acc_aoi1->id,
                'rack' => $value['rack'],
                'y_row' => $value['y_row'],
                'z_column' => $value['z_column'],
                'user_id' => $user_acc_aoi1->id
             ]);
        }

        foreach ($free_stock_array_acc as $key => $value) {
            Locator::create([
                'code' => $value['code'],
                'area_id' => $free_stock_acc_aoi2->id,
                'rack' => $value['rack'],
                'y_row' => $value['y_row'],
                'z_column' => $value['z_column'],
                'user_id' => $user_acc_aoi2->id
             ]);
        }

        //RECEIVING ACC
        Locator::create([
            'code' => $receiving_acc_aoi1->name.'-RCV.1.1',
            'area_id' => $receiving_acc_aoi1->id,
            'rack' => 'RCV',
            'y_row' => '1',
            'z_column' => '1',
            'user_id' => $user_acc_aoi1->id
        ]);

        Locator::create([
            'code' => $receiving_acc_aoi2->name.'-RCV.1.1',
            'area_id' => $receiving_acc_aoi2->id,
            'rack' => 'RCV',
            'y_row' => '1',
            'z_column' => '1',
            'user_id' => $user_acc_aoi2->id
        ]);
        
        //DISTRIBUTION
        Locator::create([
            'code' => $distribution_acc_aoi1->name.'-LINE.1.0',
            'area_id' => $distribution_acc_aoi1->id,
            'rack' => 'LINE',
            'y_row' => '1',
            'user_id' => $user_acc_aoi1->id
        ]);

        Locator::create([
            'code' => $distribution_acc_aoi2->name.'-LINE.1.0',
            'area_id' => $distribution_acc_aoi2->id,
            'rack' => 'LINE',
            'y_row' => '1',
            'user_id' => $user_acc_aoi2->id
        ]);

        //SUBCONT
        Locator::create([
            'code' => $subcont_acc_aoi2->name.'-AOI1',
            'area_id' => $subcont_acc_aoi2->id,
            'rack' => 'AOI1',
            'y_row' => '1',
            'z_column'=>1000002,
            'user_id' => $user_acc_aoi1->id
        ]);

        Locator::create([
            'code' => $subcont_acc_aoi1->name.'-AOI2',
            'area_id' => $subcont_acc_aoi1->id,
            'rack' => 'AOI2',
            'y_row' => '1',
            'z_column'=>1000013,
            'user_id' => $user_acc_aoi2->id
        ]);


        /**
         * 
         * FEBRIC
         * 
         */

        $user_febric_aoi1 = User::where('name','Admin ICT FEBRIC AOI1')->first();
        $user_febric_aoi2 = User::where('name','Admin ICT FEBRIC AOI2')->first();
        

        $receiving_febric_aoi1 = Area::where([
            ['name','=','RECEIVING'],
            ['warehouse','=','1000001'],
        ])
        ->first();

        $receiving_febric_aoi2 = Area::where([
            ['name','=','RECEIVING'],
            ['warehouse','=','1000011']
        ])
        ->first();

        $subcont_febric_aoi1 = Area::where([
            ['name','=','SUBCONT'],
            ['warehouse','=','1000001']
        ])
        ->first();

        $subcont_febric_aoi2 = Area::where([
            ['name','=','SUBCONT'],
            ['warehouse','=','1000011']
        ])
        ->first();

        $distribution_febric_aoi1 = Area::where([
            ['name','=','DISTRIBUTION'],
            ['warehouse','=','1000001']
        ])
        ->first();

        $distribution_febric_aoi2 = Area::where([
            ['name','=','DISTRIBUTION'],
            ['warehouse','=','1000011']
        ])
        ->first();

       
        $receiving_febrics = array();
        for ($i=0; $i < 16 ; $i++) { 
            for ($j=0; $j < 20; $j++) { 
                $receiving_febrics [] = [
                    'code' => 'RECEIVING-'.'A'.'.'.($i+1).'.'.($j+1),
                    'rack' => 'A',
                    'y_row' => $i+1,
                    'z_column' => $j+1
                ];
            }
        }

        foreach ($receiving_febrics as $key => $value) {
            Locator::create([
                'code' => $value['code'],
                'area_id' => $receiving_febric_aoi1->id,
                'rack' => $value['rack'],
                'y_row' => $value['y_row'],
                'z_column' => $value['z_column'],
                'user_id' => $user_febric_aoi1->id
             ]);
        }

        foreach ($receiving_febrics as $key => $value) {
            Locator::create([
                'code' => $value['code'],
                'area_id' => $receiving_febric_aoi2->id,
                'rack' => $value['rack'],
                'y_row' => $value['y_row'],
                'z_column' => $value['z_column'],
                'user_id' => $user_febric_aoi2->id
             ]);
        }

         //DISTRIBUTION
        Locator::create([
            'code' => $distribution_febric_aoi1->name.'-LINE.1.0',
            'area_id' => $distribution_febric_aoi1->id,
            'rack' => 'LINE',
            'y_row' => '1',
            'user_id' => $user_febric_aoi1->id
        ]);

        Locator::create([
            'code' => $distribution_febric_aoi2->name.'-LINE.1.0',
            'area_id' => $distribution_febric_aoi2->id,
            'rack' => 'LINE',
            'y_row' => '1',
            'user_id' => $user_febric_aoi2->id
        ]);

        //SUBCONT
        Locator::create([
            'code' => $subcont_febric_aoi2->name.'-AOI2',
            'area_id' => $subcont_febric_aoi2->id,
            'rack' => 'AOI2',
            'y_row' => '1',
            'z_column'=>1000011,
            'user_id' => $user_febric_aoi1->id
        ]);

        Locator::create([
            'code' => $subcont_febric_aoi1->name.'-AOI1',
            'area_id' => $subcont_febric_aoi1->id,
            'rack' => 'AOI1',
            'y_row' => '1',
            'z_column'=>1000011,
            'user_id' => $user_febric_aoi2->id
        ]);
    }
}
