<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialStocksTable extends Migration
{
    public function up(){
        Schema::create('material_stocks', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('locator_id',36)->nullable();
            $table->char('material_arrival_id',36)->nullable();
            $table->string('barcode_supplier');
            $table->string('po_buyer')->nullable();
            $table->string('document_no')->nullable();
            $table->string('item_code')->nullable();
            $table->string('item_desc')->nullable();
            $table->string('category')->nullable();
            $table->string('type_po')->nullable();
            $table->string('batch_number')->nullable();
            $table->string('nomor_roll')->nullable();
            $table->string('warehouse_id')->nullable();
            $table->string('uom')->nullable();
            $table->double('stock',15,8)->nullable();
            $table->integer('qty_carton')->nullable();
            $table->integer('user_id')->unsigned();
            $table->boolean('is_active')->default(false);
            $table->boolean('is_allocated')->default(false);
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('locator_id')->references('id')->on('locators')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('material_arrival_id')->references('id')->on('material_arrivals')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    public function down(){
        Schema::dropIfExists('material_transits');
    }
}
