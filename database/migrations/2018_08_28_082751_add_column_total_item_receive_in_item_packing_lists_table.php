<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTotalItemReceiveInItemPackingListsTable extends Migration
{
    public function up()
    {
        Schema::table('item_packing_lists', function (Blueprint $table) {
            $table->integer('total_item_receive')->nullable()->default(0);
        });
    }

    public function down()
    {
        //
    }
}
