<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialSubcontsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_subconts', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('po_detail_id')->nullable();
            $table->string('barcode')->nullable();
            $table->string('item_id')->nullable();
            $table->string('item_code')->nullable();
            $table->string('po_buyer')->nullable();
            $table->string('item_desc')->nullable();
            $table->string('supplier_name')->nullable();
            $table->string('document_no')->nullable();
            $table->string('c_order_id')->nullable();
            $table->string('c_orderline_id')->nullable();
            $table->string('warehouse_id')->nullable();
            $table->string('category')->nullable();
            $table->string('uom')->nullable();
            $table->string('no_packing_list')->nullable();
            $table->string('type_po')->nullable();
            $table->string('no_resi')->nullable();
            $table->string('no_surat_jalan')->nullable();
            $table->string('no_invoice')->nullable();
            $table->datetime('etd_date')->nullable();
            $table->datetime('eta_date')->nullable();
            $table->double('qty_carton',15,8)->nullable();
            $table->double('qty_ordered',15,8)->nullable();
            $table->double('qty_entered',15,8)->nullable();
            $table->double('qty_upload',15,8)->nullable();
            $table->boolean('is_active')->default(true);
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            $table->datetime('date_receive')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_subconts');
    }
}
