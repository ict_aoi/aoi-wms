<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnWidthsInBarcodePreparationFabricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('barcode_preparation_fabrics', function (Blueprint $table) {
            $table->double('begin_width')->nullable();
            $table->double('middle_width')->nullable();
            $table->double('end_width')->nullable();
            $table->double('actual_width')->nullable();
            $table->double('actual_length')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
