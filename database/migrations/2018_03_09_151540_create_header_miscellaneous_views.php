<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeaderMiscellaneousViews extends Migration
{
    public function up()
    {
        /*DB::statement("
            CREATE OR REPLACE VIEW miscellaneous_header AS
                SELECT miscellaneous_items.item_id,
                    stocks.item_code,
                    stocks.item_desc,
                    miscellaneous_items.item_category,
                    miscellaneous_items.uom,
                    coalesce(miscellaneous.total_qty,0) as total
                FROM miscellaneous_items
                LEFT JOIN stocks on stocks.item_id = miscellaneous_items.item_id
                left join (
                        select item_id,
                        sum(qty) as total_qty
                        FROM miscellaneous_items
                        where miscellaneous_items.is_active = true
                        group by item_id
                )as miscellaneous on miscellaneous.item_id = miscellaneous_items.item_id
                ORDER BY miscellaneous_items.created_at DESC
        ");*/
    }

    public function down()
    {
        //DB::statement('DROP VIEW miscellaneous_header CASCADE');
    }
}
