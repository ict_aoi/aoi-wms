<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntegrasiRmaViews extends Migration
{
    public function up()
    {
        /*DB::statement("
            CREATE OR REPLACE VIEW public.integrasi_rma as
                select material_checks.id as material_check_id,
                    material_arrivals.c_bpartner_id,	
                    material_arrivals.c_order_id,
                    material_arrivals.c_orderline_id,
                    material_arrivals.item_id,
                    material_preparations.warehouse as warehouse_id,
                    material_preparations.po_buyer,
                    material_arrivals.supplier_name,
                    material_arrivals.document_no,
                    material_arrivals.item_code,
                    material_arrivals.item_desc,
                    material_checks.qty_reject,
                    material_checks.is_integrate,
                    material_checks.integration_date
                FROM material_checks
                    LEFT JOIN material_preparations ON material_preparations.id = material_checks.material_preparation_id::bpchar
                    LEFT JOIN detail_material_preparations ON detail_material_preparations.material_preparation_id = material_preparations.id
                    LEFT JOIN material_arrivals ON detail_material_preparations.material_arrival_id = material_arrivals.id
                    LEFT JOIN users ON users.id = material_arrivals.user_id
                where material_checks.status = 'REJECT' and material_arrivals.document_no is not null
                GROUP BY material_checks.id,
                    material_arrivals.c_bpartner_id,
                    material_arrivals.supplier_name,
                    material_arrivals.document_no,
                    material_arrivals.item_code,
                    material_arrivals.item_desc,
                    material_checks.qty_reject,
                    material_arrivals.c_orderline_id,
                    material_preparations.warehouse,
                    material_checks.is_integrate,
                    material_arrivals.item_id,
                    material_arrivals.c_order_id,
                    material_checks.integration_date,
                    material_preparations.po_buyer
      
        ");*/
    }

    public function down()
    {
        DB::statement('DROP VIEW integrasi_rma CASCADE');
    }
}
