<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialRollHandoverFabricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_roll_handover_fabrics', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('material_stock_id',36)->nullable();
            $table->string('c_bpartner_id')->nullable();
            $table->string('supplier_name')->nullable();
            $table->string('c_order_id')->nullable();
            $table->string('no_invoice')->nullable();
            $table->string('document_no')->nullable();
            $table->string('item_code')->nullable();
            $table->string('color')->nullable();
            $table->string('nomor_roll')->nullable();
            $table->string('batch_number')->nullable();
            $table->string('actual_lot')->nullable();
            $table->double('actual_width',15,8)->nullable();
            $table->string('uom')->nullable();
            $table->double('qty_handover',15,8)->nullable();
            $table->integer('user_id')->unsigned();
            $table->string('warehouse_from_id')->nullable();
            $table->string('warehouse_to_id')->nullable();
            $table->datetime('receive_date')->nullable();
            $table->integer('user_receive_id')->nullable();
           
            $table->timestamps();
            $table->foreign('user_receive_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('material_stock_id')->references('id')->on('material_stocks')->onUpdate('cascade')->onDelete('cascade');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_roll_handover_fabrics');
    }
}
