<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnErpInMaterialMovementLinesTable extends Migration
{
    public function up()
    {
        Schema::table('material_movement_lines', function (Blueprint $table) {
            $table->boolean('is_integrate')->nullable();
            $table->datetime('integration_date')->nullable();
        });
    }

    public function down()
    {
        Schema::table('material_movement_lines', function (Blueprint $table) {
            $table->dropColumn('is_integrate');
            $table->dropColumn('integration_date');
        });
    }
}
