<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnMaterialMovementIdInMaterialMovementLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_movement_lines', function (Blueprint $table) {
            $table->char('material_movement_id',36);
            $table->foreign('material_movement_id')->references('id')->on('material_movements')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_movement_lines', function (Blueprint $table) {
            $table->dropForeign('material_movements_material_movement_id_foreign');
            $table->dropColumn('material_movement_id'); 
        });
    }
}
