<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddItemDescColumnInMaterialPreparationsTable extends Migration
{
    public function up()
    {
        Schema::table('material_preparations', function (Blueprint $table) {
            $table->string('item_desc')->nullable();
        });
    }

    public function down()
    {
        Schema::table('material_preparations', function (Blueprint $table) {
            $table->dropColumn('item_desc');
        });
    }
}
