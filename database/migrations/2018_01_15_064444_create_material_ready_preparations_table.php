<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialReadyPreparationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_ready_preparations', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('material_arrival_id',36);
            $table->bigInteger('item_id');
            $table->string('item_code');
            $table->string('item_desc');
            $table->string('category');
            $table->string('document_no');
            $table->string('type_po');
            $table->string('po_buyer');
            $table->integer('user_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('material_arrival_id')->references('id')->on('material_arrivals')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_ready_preparations');
    }
}
