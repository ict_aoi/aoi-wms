<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInMaterialPreparationsTable extends Migration
{
    public function up()
    {
        Schema::table('detail_material_preparations', function (Blueprint $table) {
            $table->char('material_stock_id',36)->nullable()->unsigned();
            $table->foreign('material_stock_id')->references('id')->on('material_stocks')->onDelete('cascade')->onUpdate('cascade');
        
            $table->string('barcode_supplier')->nullable();
            $table->double('qty_allocated',15,8)->nullable();
        });
    }

    public function down()
    {
        Schema::table('detail_material_preparations', function (Blueprint $table) {
            $table->dropForeign('material_stocks_material_stock_id_foreign');
            $table->dropColumn('material_stock_id'); 
            $table->dropColumn('barcode_supplier'); 
            $table->dropColumn('qty_allocated'); 
        });
    }
}
