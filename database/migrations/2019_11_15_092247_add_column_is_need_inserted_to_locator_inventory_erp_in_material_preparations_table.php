<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsNeedInsertedToLocatorInventoryErpInMaterialPreparationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_preparations', function (Blueprint $table) 
        {
            $table->boolean('is_need_inserted_to_locator_inventory_erp')->default(false)->after('is_from_closing');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
