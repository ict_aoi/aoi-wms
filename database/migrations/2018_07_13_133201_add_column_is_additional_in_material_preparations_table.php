<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsAdditionalInMaterialPreparationsTable extends Migration
{
    public function up()
    {
        Schema::table('allocation_items', function (Blueprint $table) {
            $table->boolean('is_additional')->nullable()->default(false);
            
        });
    }

    public function down()
    {
        Schema::table('allocation_items', function (Blueprint $table) {
            $table->dropColumn('is_additional'); 
            
        });
    }
}
