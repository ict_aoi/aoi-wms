<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnMappingStockIdInMaterialStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_stocks', function (Blueprint $table) {
            $table->char('mapping_stock_id',36)->nullable();
            $table->foreign('mapping_stock_id')->references('id')->on('mapping_stocks')->onUpdate('cascade')->onDelete('cascade');
       
        });

        Schema::table('history_material_stocks', function (Blueprint $table) {
            $table->char('mapping_stock_id',36)->nullable();
            $table->foreign('mapping_stock_id')->references('id')->on('mapping_stocks')->onUpdate('cascade')->onDelete('cascade');
       
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
