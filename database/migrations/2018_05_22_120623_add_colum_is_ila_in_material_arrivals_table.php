<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumIsIlaInMaterialArrivalsTable extends Migration
{
    public function up()
    {
        Schema::table('material_arrivals', function (Blueprint $table) {
            $table->boolean('is_ila')->nullable()->default(false);
        });
    }

    public function down()
    {
        Schema::table('material_arrivals', function (Blueprint $table) {
            $table->dropColumn('is_ila');
        });
    }
}
