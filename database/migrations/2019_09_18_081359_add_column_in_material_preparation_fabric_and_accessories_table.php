<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInMaterialPreparationFabricAndAccessoriesTable extends Migration
{
    public function up()
    {
        Schema::table('material_preparation_fabrics', function (Blueprint $table) {
            $table->text('remark_planning')->nullable();
        });

        Schema::table('material_preparations', function (Blueprint $table) {
            $table->date('planning_cutting_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
