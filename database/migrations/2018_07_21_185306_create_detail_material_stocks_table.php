<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailMaterialStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_material_stocks', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('material_stock_id',36);
            $table->char('material_preparation_id',36)->nullable();
            $table->string('remark')->nullable();
            $table->string('uom')->nullable();
            $table->double('qty',15,8)->nullable();
            $table->integer('user_id')->unsigned();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();

            $table->foreign('material_preparation_id')->references('id')->on('material_preparations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('material_stock_id')->references('id')->on('material_stocks')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_material_stocks');
    }
}
