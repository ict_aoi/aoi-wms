<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailMaterialPreparationsTable extends Migration
{
    public function up()
    {
        Schema::create('detail_material_preparations', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('material_preparation_id',36)->nullable()->unsigned();
            $table->char('material_arrival_id',36)->nullable()->unsigned();
            $table->integer('user_id')->unsigned();
            
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('material_preparation_id')->references('id')->on('material_preparations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('material_arrival_id')->references('id')->on('material_arrivals')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    public function down()
    {
        Schema::dropIfExists('detail_material_preparations');
    }
}
