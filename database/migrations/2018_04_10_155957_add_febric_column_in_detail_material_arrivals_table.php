<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFebricColumnInDetailMaterialArrivalsTable extends Migration
{
    public function up()
    {
        Schema::table('detail_material_arrivals', function (Blueprint $table) {
            $table->string('batch_number')->nullable();
            $table->string('nomor_roll')->nullable();
        });
    }

    public function down()
    {
        Schema::table('detail_material_arrivals', function (Blueprint $table) {
            $table->dropColumn('batch_number');
            $table->dropColumn('nomor_roll');
        });
    }
}
