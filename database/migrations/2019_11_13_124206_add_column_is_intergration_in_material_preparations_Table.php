<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsIntergrationInMaterialPreparationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('material_preparations', function (Blueprint $table) 
        {
            $table->boolean('is_from_adjustment')->default(false)->after('is_from_barcode_bom');;
            $table->boolean('is_from_cancel_or_reroute')->default(false)->after('is_from_adjustment');;
            $table->timestamp('is_need_inserted_to_locator_free_stock_for_adjustment_erp_date')->nullable();
            $table->timestamp('is_need_inserted_to_locator_free_stock_for_cancel_or_reroute_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
