<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFunctionGetMrp extends Migration
{
    public function up()
    {
        /*DB::statement("
            CREATE OR REPLACE VIEW public.mrp_report AS
 SELECT mr.po_buyer,
    mr.item_code,
    mr.item_desc,
    mr.category,
    mr.job_order,
    mr.article_no,
    string_agg(mr.style::text, ','::text) AS style,
    sum(mr.qty_required) AS qty_need,
        CASE
            WHEN mm.status IS NULL THEN 'NOT RECEIVE'::text
            ELSE mm.status
        END AS status,
    mm.locator_name AS location,
    mr.statistical_date,
    mm.created_at AS movement_date,
    mm.user_name,
    COALESCE(mp.qty_conversion, 0::double precision) AS qty_in
   FROM material_requirements mr
     LEFT JOIN ( SELECT material_preparations.item_code,
            material_preparations.po_buyer,
            material_preparations.category,
            material_preparations.qty_conversion
           FROM material_preparations
          GROUP BY material_preparations.item_code, material_preparations.po_buyer, material_preparations.category, material_preparations.qty_conversion) mp ON mp.item_code::text = mr.item_code::text AND mp.po_buyer::text = mr.po_buyer::text AND mp.category::text = mr.category::text
     LEFT JOIN ( SELECT union_table.status,
            union_table.locator_name,
            union_table.item_code,
            union_table.po_buyer,
            union_table.created_at,
            union_table.user_name
           FROM ( SELECT 'RECEIVE'::text AS status,
                    'RECEVING'::character varying AS locator_name,
                    mp_1.item_code,
                    mp_1.po_buyer,
                    mp_1.created_at,
                    users.name AS user_name
                   FROM material_preparations mp_1
                     JOIN users ON mp_1.user_id = users.id
                  GROUP BY mp_1.item_code, mp_1.po_buyer, mp_1.created_at, users.name
                UNION ALL
                 SELECT 'READY SUPPLAI'::text AS status,
                    l.code AS locator_name,
                    ml.item_code,
                    mp_1.po_buyer,
                    ml.created_at,
                    users.name AS user_name
                   FROM material_movements mm_1
                     JOIN material_movement_lines ml ON mm_1.id = ml.material_movement_id
                     JOIN locators l ON l.id = mm_1.to_destination
                     JOIN users ON ml.user_id = users.id
                     JOIN material_preparations mp_1 ON mp_1.id = ml.material_preparation_id
                  WHERE mm_1.status::text = 'in'::text
                  GROUP BY l.code, ml.item_code, mp_1.po_buyer, ml.created_at, users.name
                union all
                SELECT 'REJECT'::text AS status,
                    'REJECT' AS locator_name,
                    mp_1.item_code,
                    mp_1.po_buyer,
                    mc.created_at,
                    users.name AS user_name
                   FROM material_checks mc
                     --JOIN material_movement_lines ml ON mm_1.id = ml.material_movement_id
                     --JOIN locators l ON l.id = mm_1.to_destination
                     JOIN users ON mc.user_id = users.id
                     JOIN material_preparations mp_1 ON mp_1.id = mc.material_preparation_id
                  WHERE mc.status::text = 'REJECT'::text and mc.uom_po::float8 = mc.qty_reject
                  GROUP BY mp_1.item_code, mp_1.po_buyer, mc.created_at, users.name
                union all
                SELECT 'PINDAH RAK'::text AS status,
                    l.code AS locator_name,
                    ml.item_code,
                    mp_1.po_buyer,
                    ml.created_at,
                    users.name AS user_name
                   FROM material_movements mm_1
                     JOIN material_movement_lines ml ON mm_1.id = ml.material_movement_id
                     JOIN locators l ON l.id = mm_1.to_destination
                     JOIN users ON ml.user_id = users.id
                     JOIN material_preparations mp_1 ON mp_1.id = ml.material_preparation_id
                  WHERE mm_1.status::text = 'change'::text
                  GROUP BY l.code, ml.item_code, mp_1.po_buyer, ml.created_at, users.name
                UNION ALL
                 SELECT 'SUPPLIED'::text AS status,
                    l.code AS locator_name,
                    ml.item_code,
                    mp_1.po_buyer,
                    ml.created_at,
                    users.name AS user_name
                   FROM material_movements mm_1
                     JOIN material_movement_lines ml ON mm_1.id = ml.material_movement_id
                     JOIN locators l ON l.id = mm_1.to_destination
                     JOIN users ON ml.user_id = users.id
                     JOIN material_preparations mp_1 ON mp_1.id = ml.material_preparation_id
                  WHERE mm_1.status::text = 'out'::text
                  GROUP BY l.code, ml.item_code, mp_1.po_buyer, ml.created_at, users.name) union_table
          WHERE ((union_table.item_code::text, union_table.po_buyer::text, union_table.created_at) IN ( SELECT union_table_1.item_code,
                    union_table_1.po_buyer,
                    max(union_table_1.created_at) AS max
                   FROM ( SELECT 'RECEIVE'::text AS status,
                            'RECEVING'::character varying AS locator_name,
                            material_preparations.item_code,
                            material_preparations.po_buyer,
                            material_preparations.created_at
                           FROM material_preparations
                          GROUP BY material_preparations.item_code, material_preparations.po_buyer, material_preparations.category, material_preparations.qty_conversion, material_preparations.created_at
                        UNION ALL
                         SELECT 'READY SUPPLAI'::text AS status,
                            l.code AS locator_name,
                            ml.item_code,
                            mp_1.po_buyer,
                            ml.created_at
                           FROM material_movements mm_1
                             JOIN material_movement_lines ml ON mm_1.id = ml.material_movement_id
                             JOIN locators l ON l.id = mm_1.to_destination
                             LEFT JOIN material_preparations mp_1 ON mp_1.id = ml.material_preparation_id
                          WHERE mm_1.status::text = 'in'::text
                        union all
		                SELECT 'REJECT'::text AS status,
		                    'REJECT' AS locator_name,
		                    mp_1.item_code,
		                    mp_1.po_buyer,
		                    mc.created_at
		                   FROM material_checks mc
		                     --JOIN material_movement_lines ml ON mm_1.id = ml.material_movement_id
		                     --JOIN locators l ON l.id = mm_1.to_destination
		                     JOIN material_preparations mp_1 ON mp_1.id = mc.material_preparation_id
		                  WHERE mc.status::text = 'REJECT'::text and mc.uom_po::float8 = mc.qty_reject
		                  GROUP BY mp_1.item_code, mp_1.po_buyer, mc.created_at
                        UNION ALL
                         SELECT 'PINDAH RAK'::text AS status,
                            l.code AS locator_name,
                            ml.item_code,
                            mp_1.po_buyer,
                            ml.created_at
                           FROM material_movements mm_1
                             JOIN material_movement_lines ml ON mm_1.id = ml.material_movement_id
                             JOIN locators l ON l.id = mm_1.to_destination
                             LEFT JOIN material_preparations mp_1 ON mp_1.id = ml.material_preparation_id
                          WHERE mm_1.status::text = 'change'::text
                        UNION ALL
                         SELECT 'SUPPLIED'::text AS status,
                            l.code AS locator_name,
                            ml.item_code,
                            mp_1.po_buyer,
                            ml.created_at
                           FROM material_movements mm_1
                             JOIN material_movement_lines ml ON mm_1.id = ml.material_movement_id
                             JOIN locators l ON l.id = mm_1.to_destination
                             LEFT JOIN material_preparations mp_1 ON mp_1.id = ml.material_preparation_id
                          WHERE mm_1.status::text = 'out'::text) union_table_1
                  GROUP BY union_table_1.item_code, union_table_1.po_buyer))) mm ON mm.item_code::text = mr.item_code::text AND mm.po_buyer::text = mr.po_buyer::text
  WHERE (mr.po_buyer::text IN ( SELECT material_ready_preparations.po_buyer
           FROM material_ready_preparations
          GROUP BY material_ready_preparations.po_buyer))
  GROUP BY mr.po_buyer, mr.item_code, mr.item_desc, mr.category, mr.job_order, mr.article_no, mp.qty_conversion, mm.status, mm.locator_name, mm.created_at, mr.statistical_date, mm.user_name
  ORDER BY mr.po_buyer;
        
            CREATE OR REPLACE FUNCTION public.get_mrp(_po_buyer character varying)
 RETURNS TABLE(po_buyer character varying, item_code character varying, item_desc character varying, category character varying, job_order character varying, article_no character varying, style character varying, qty_need double precision, status character varying, location character varying, statistical_date timestamp without time zone, movement_date timestamp without time zone, user_name character varying, qty_in double precision)
 LANGUAGE sql
AS $function$
select mr.po_buyer,
 mr.item_code,
 mr.item_desc,
 mr.category,
 mr.job_order,
 mr.article_no,
 string_agg(mr."style",',') as "style",
 sum(mr.qty_required) as qty_need,
 case 
 	when mm.status is null then 'NOT RECEIVE'
 	else mm.status
 end status,
 mm.locator_name as location,
 mr.statistical_date,
 mm.created_at as movement_date,
 mm.user_name,
 coalesce(mp.qty_conversion,0) as qty_in
 from material_requirements mr
 left join (
 	select item_code,po_buyer,category,qty_conversion
 	from material_preparations
 	group by item_code,po_buyer,category,qty_conversion
 ) mp on mp.item_code = mr.item_code and mp.po_buyer = mr.po_buyer and mp.category = mr.category
 left join (
	 select 
		union_table.status,
		union_table.locator_name,
		union_table.item_code,
		union_table.po_buyer,
		union_table.created_at,
		union_table.user_name
		from (
		select 'RECEIVE' as status,'RECEVING'as locator_name,item_code,po_buyer,mp.created_at,users."name" as user_name
		from material_preparations mp
		join users on mp.user_id = users.id
		where po_buyer = _po_buyer
		group by item_code,po_buyer,mp.created_at,users."name"
		union all
		select 'READY SUPPLY' as status, l.code as locator_name,ml.item_code, mp.po_buyer,ml.created_at,users."name" as user_name
		from material_movements mm
		join material_movement_lines ml on mm.id = ml.material_movement_id
		join locators l on l.id = mm.to_destination
		join users on ml.user_id = users.id
		left join material_preparations mp on mp.id = ml.material_preparation_id
		where mm.status = 'in' and mp.po_buyer = _po_buyer
		group by l.code,ml.item_code,mp.po_buyer,ml.created_at,users."name"
		union all
		SELECT 'REJECT'::text AS status,'REJECT' AS locator_name,mp_1.item_code,mp_1.po_buyer,mc.created_at,users.name AS user_name
        FROM material_checks mc
        JOIN users ON mc.user_id = users.id
        JOIN material_preparations mp_1 ON mp_1.id = mc.material_preparation_id
        WHERE mc.status::text = 'REJECT'::text and mc.uom_po::float8 = mc.qty_reject
        GROUP BY mp_1.item_code, mp_1.po_buyer, mc.created_at, users.name
        union all
		select 'SUPPLIED' as status, l.code as locator_name,ml.item_code, mp.po_buyer,ml.created_at,users."name" as user_name
		from material_movements mm
		join material_movement_lines ml on mm.id = ml.material_movement_id
		join locators l on l.id = mm.to_destination
		join users on ml.user_id = users.id
		left join material_preparations mp on mp.id = ml.material_preparation_id
		where mm.status = 'out'  and mp.po_buyer = _po_buyer
		group by l.code,ml.item_code,mp.po_buyer,ml.created_at,users."name"
		) union_table
		where (union_table.item_code,union_table.po_buyer,union_table.created_at) in (
		select 
		union_table.item_code,
		union_table.po_buyer,
		max(union_table.created_at)
		from (
		select 'RECEIVE' as status,'RECEVING'as locator_name,item_code,po_buyer,created_at
		from material_preparations
		where po_buyer = _po_buyer
		group by item_code,po_buyer,category,qty_conversion,created_at
		union all
		select 'READY SUPPLY' as status, l.code as locator_name,ml.item_code, mp.po_buyer,ml.created_at
		from material_movements mm
		join material_movement_lines ml on mm.id = ml.material_movement_id
		join locators l on l.id = mm.to_destination
		left join material_preparations mp on mp.id = ml.material_preparation_id
		where mm.status = 'in' and mp.po_buyer = _po_buyer
		union all
		SELECT 'REJECT'::text AS status,'REJECT' AS locator_name,mp_1.item_code,mp_1.po_buyer,mc.created_at
        FROM material_checks mc
        JOIN material_preparations mp_1 ON mp_1.id = mc.material_preparation_id
        WHERE mc.status::text = 'REJECT'::text and mc.uom_po::float8 = mc.qty_reject
        GROUP BY mp_1.item_code, mp_1.po_buyer, mc.created_at
        union all
		select 'SUPPLIED' as status, l.code as locator_name,ml.item_code, mp.po_buyer,ml.created_at
		from material_movements mm
		join material_movement_lines ml on mm.id = ml.material_movement_id
		join locators l on l.id = mm.to_destination
		left join material_preparations mp on mp.id = ml.material_preparation_id
		where mm.status = 'out' and mp.po_buyer = _po_buyer
		) union_table
		group by union_table.item_code,
		union_table.po_buyer
		)
 ) mm on mm.item_code = mr.item_code and mm.po_buyer = mr.po_buyer 
 where mr.po_buyer = _po_buyer
 group by  mr.po_buyer,
 mr.item_code,
 mr.item_desc,
 mr.category,
 mr.job_order,
 mr.article_no,
 mp.qty_conversion,
 mm.status,
 mm.locator_name,
 mr.statistical_date,
 mm.created_at,
 mm.user_name
  

$function$

        
        
                ");*/
    }

    public function down()
    {
        DB::statement('DROP function get_mrp CASCADE');
    }
}
