<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoBuyersTable extends Migration
{
    public function up()
    {
        Schema::create('po_buyers', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('type_po')->nullable();
            $table->string('po_buyer')->nullable();
            $table->date('lc_date')->nullable();
            $table->statistical_date('lc_date')->nullable();
            $table->string('season')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('po_buyers');
    }
}
