<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentIdInMaterialMovementLinesTable extends Migration
{
    public function up()
    {
        Schema::table('material_movement_lines', function (Blueprint $table) {
            $table->char('parent_id',36)->nullable();
            $table->foreign('parent_id')->references('id')->on('material_movement_lines')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    public function down()
    {
        Schema::table('material_movement_lines', function (Blueprint $table) {
            $table->dropForeign('material_movement_lines_parent_id_foreign');
            $table->dropColumn('parent_id'); 
        });
    }
}
