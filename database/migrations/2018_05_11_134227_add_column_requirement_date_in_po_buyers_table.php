<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnRequirementDateInPoBuyersTable extends Migration
{
    public function up()
    {
        Schema::table('po_buyers', function (Blueprint $table) {
            $table->datetime('requirement_date')->nullable();
        });
    }

    public function down()
    {
        Schema::table('po_buyers', function (Blueprint $table) {
            $table->dropColumn('requirement_date');
        });
    }
}
