<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemMiscellaneousViews extends Migration
{
    public function up()
    {
       /* DB::statement("
            CREATE OR REPLACE VIEW miscellaneous_item AS
                SELECT miscellaneous_items.id,
                    miscellaneous_items.item_id,
                    stocks.item_code,
                    stocks.item_desc,
                    miscellaneous_items.item_category,
                    miscellaneous_items.uom,
                    miscellaneous_items.qty,
                    miscellaneous_items.approval_status,
                    criterias.id AS criteria_id,
                    criterias.name AS criteria,
                    stock_lines.document_no,
                    miscellaneous_items.rack,
                    case 
                            when stock_lines.note is not null then stock_lines.note 
                            when stock_lines.note is null then miscellaneous_items.system_note 
                    end AS system_note
                FROM miscellaneous_items
                    LEFT JOIN stocks ON stocks.item_id = miscellaneous_items.item_id
                    LEFT JOIN stock_lines ON stock_lines.id = miscellaneous_items.stock_line_id
                    LEFT JOIN criterias ON criterias.id = miscellaneous_items.criteria_id AND criterias.is_active = true
                WHERE miscellaneous_items.is_active = true
                GROUP BY miscellaneous_items.id, stock_lines.note, stocks.item_code, stocks.item_desc, miscellaneous_items.item_category, miscellaneous_items.uom, miscellaneous_items.qty, miscellaneous_items.approval_status, criterias.id, criterias.name, stock_lines.document_no, miscellaneous_items.rack
                ORDER BY miscellaneous_items.created_at DESC;
        ");*/
    }
    public function down()
    {
        DB::statement('DROP VIEW miscellaneous_item CASCADE');
    }
}
