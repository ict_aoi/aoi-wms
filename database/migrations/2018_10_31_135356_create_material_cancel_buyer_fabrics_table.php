<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialCancelBuyerFabricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_cancel_buyer_fabrics', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('material_stock_id',36)->nullable();
            $table->char('new_material_stock_id',36)->nullable();
            $table->string('document_no')->nullable();
            $table->string('po_buyer')->nullable();
            $table->string('item_code')->nullable();
            $table->double('reserved_qty',15,8)->nullable();
            $table->integer('user_id')->unsigned();
            $table->string('warehouse_id');
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('material_stock_id')->references('id')->on('material_stocks')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('new_material_stock_id')->references('id')->on('material_stocks')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_cancel_buyer_fabrics');
    }
}
