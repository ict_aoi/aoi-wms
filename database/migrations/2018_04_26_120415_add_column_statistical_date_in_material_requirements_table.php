<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStatisticalDateInMaterialRequirementsTable extends Migration
{
    public function up()
    {
        
        Schema::table('material_requirements', function (Blueprint $table) {
            $table->datetime('statistical_date')->nullable();
        });
         
    }
    
    public function down()
    {
        Schema::table('material_requirements', function (Blueprint $table) {
            $table->dropColumn('statistical_date');
        });
    }
}
