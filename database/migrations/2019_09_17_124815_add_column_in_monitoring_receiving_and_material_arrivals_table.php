<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInMonitoringReceivingAndMaterialArrivalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('monitoring_receiving_fabrics', function (Blueprint $table) {
            $table->string('po_detail_id')->nullable();
            $table->string('summary_handover_material_id')->nullable();
            $table->string('no_pt')->nullable();
            $table->string('no_bc')->nullable();
            $table->string('no_kk')->nullable();
        });

        Schema::table('material_arrivals', function (Blueprint $table) {
            $table->string('summary_handover_material_id')->nullable();
        });

        Schema::table('material_stocks', function (Blueprint $table) {
            $table->string('summary_handover_material_id')->nullable();
        });

        Schema::table('material_preparation_fabrics', function (Blueprint $table) {
            $table->string('item_code_source')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
