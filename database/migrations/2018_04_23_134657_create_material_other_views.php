<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialOtherViews extends Migration
{
    public function up()
    {
        // DB::statement("
        //     CREATE OR REPLACE VIEW public.material_others AS
        //         SELECT material_arrivals.id as material_arrival_id,
        //             locators.id as locator_id,
        //             locators.code as locator_code,
        //             material_arrivals.document_no,
        //             material_arrivals.item_code,
        //             material_arrivals.item_desc,
        //             material_arrivals.category,
        //             material_arrivals.remark,
        //             material_arrivals.approval_status,
        //             case
        //             		when uom_conversions.uom_to is null then material_arrivals.uom
        //             		else uom_conversions.uom_to
        //             	end as uom,
        //         	 	case
        //             		when uom_conversions.dividerate is null then material_arrivals.qty_upload
        //             		else uom_conversions.dividerate * material_arrivals.qty_upload
        //             	end as total_qty,
        //             material_arrivals.qty_upload as qty_reconversion,
        //             material_arrivals.warehouse_id
        //         FROM material_arrivals
        //         LEFT JOIN locators on locators.id = material_arrivals.locator_id
        //         LEFT JOIN uom_conversions on uom_conversions.item_code = material_arrivals.item_code
        //         and uom_conversions.uom_from = material_arrivals.uom and uom_conversions.category = material_arrivals.category
        //         WHERE po_buyer = '' AND ( material_arrivals.is_material_other = TRUE  OR material_arrivals.is_general_item = true)
        //         GROUP BY  material_arrivals.id,
        //             locators.id,
        //             locators.code,
        //             material_arrivals.document_no,
        //             material_arrivals.item_code,
        //             material_arrivals.item_desc,
        //             material_arrivals.category,
        //             material_arrivals.uom,
        //             material_arrivals.qty_upload ,
        //             material_arrivals.remark,
        //             material_arrivals.approval_status,
        //             uom_conversions.uom_to,
        //             uom_conversions.dividerate,
        //             uom_conversions.multiplyrate,
        //             material_arrivals.warehouse_id;
        // ");
    }

    public function down()
    {
         DB::statement('DROP VIEW material_others CASCADE');
    }
}
