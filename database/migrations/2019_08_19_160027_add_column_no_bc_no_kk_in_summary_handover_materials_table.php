<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnNoBcNoKkInSummaryHandoverMaterialsTable extends Migration
{
    public function up()
    {
        Schema::table('summary_handover_materials', function (Blueprint $table) {
           $table->string('no_bc')->nullable();
           $table->string('no_kk')->nullable();
        });
    }

    public function down()
    {
        Schema::table('summary_handover_materials', function (Blueprint $table) {
            $table->dropColumn('no_bc');
            $table->dropColumn('no_kk');
        });
    }
}
