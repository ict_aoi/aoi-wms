<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnLoadActualInMaterialStocksTable extends Migration
{
    public function up()
    {
        Schema::table('material_stocks', function (Blueprint $table) {
            $table->string('load_actual')->default(0)->nullable();
        });
    }

    public function down()
    {
        Schema::table('material_stocks', function (Blueprint $table) {
           $table->dropColumn('load_actual');
        });
    }
}
