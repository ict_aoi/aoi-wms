<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoulumnsIsPrefixInTableReRoute extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reroute_po_buyers', function (Blueprint $table) {
            $table->boolean('is_prefix')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reroute_po_buyers', function (Blueprint $table) {
            $table->dropColumn('is_prefix');
        });
    }
}
