<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPromiseDateInMaterialRequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_requirements', function (Blueprint $table) {
            $table->datetime('promise_date')->nullable();
        });

        Schema::table('material_requirement_per_parts', function (Blueprint $table) {
            $table->datetime('promise_date')->nullable();
        });

        Schema::table('detail_material_requirements', function (Blueprint $table) {
            $table->datetime('promise_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
