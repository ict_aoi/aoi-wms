<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWarehouseColumnInTemporariesTable extends Migration
{
    public function up()
    {
        Schema::table('temporaries', function (Blueprint $table) {
            $table->string('warehouse')->nullable();
        });
    }

    public function down()
    {
        Schema::table('temporaries', function (Blueprint $table) {
            $table->dropColumn('warehouse'); 
        });
    }
}
