<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIntegrationInMaterailMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_movements', function (Blueprint $table) {
            $table->boolean('is_integrate')->default(false);
            $table->datetime('integration_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_movements', function (Blueprint $table) {
            $table->dropColumn('is_integrate'); 
            $table->dropColumn('integration_date'); 
        });
    }
}
