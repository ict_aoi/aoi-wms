<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCBpartnerIdInDetailMaterialPlanningFabrics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_material_planning_fabrics', function (Blueprint $table) {
            $table->string('c_bpartner_id')->nullable();
            $table->double('qty_booking',15,8)->nullable()->default(0);
            $table->boolean('is_from_additional')->nullable();
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
