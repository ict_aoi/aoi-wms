<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgingStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trim_cards', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('c_order_id')->nullable();
            $table->string('item_id')->nullable();
            $table->string('warehouse_id')->nullable();
            $table->date('date')->nullable();
            $table->double('total_in',15,8)->default(0);
            $table->double('total_out',15,8)->default(0);
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aging_stocks');
    }
}
