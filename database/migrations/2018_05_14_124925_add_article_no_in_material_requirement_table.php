<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArticleNoInMaterialRequirementTable extends Migration
{
    public function up()
    {
        Schema::table('material_requirements', function (Blueprint $table) {
            $table->string('article_no')->nullable();
        });
    }

    public function down()
    {
        Schema::table('material_requirements', function (Blueprint $table) {
            $table->dropColumn('article_no');
        });
    }
}
