<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnQtyBookingActualInMaterialPreparationFabrics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_preparation_fabrics', function (Blueprint $table) {
            $table->double('qty_booking_actual',15,8)->default(0)->nullable();
            $table->double('addition_qty',15,8)->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_preparation_fabrics', function (Blueprint $table) {
           $table->dropColumn('addition_qty');
           $table->dropColumn('qty_booking_actual');
        });
    }
}
