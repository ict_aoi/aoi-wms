<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsNeedToHandoverInAllocationItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('allocation_items', function (Blueprint $table) {
            $table->boolean('is_need_to_handover')->default(false);
            $table->string('color')->nullable();
            $table->string('warehouse_inventory')->nullable();
            $table->date('handover_date')->nullable();
            $table->date('handover_receive_date')->nullable();
            $table->integer('handover_user_receive_id')->nullable();
            $table->foreign('handover_user_receive_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
