<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPreparationDateInMaterialReadyPreparationsTahle extends Migration
{
    public function up()
    {
        Schema::table('material_ready_preparations', function (Blueprint $table) {
            $table->integer('user_preparation_id')->unsigned()->nullable();
            $table->datetime('preparation_date')->nullable();
            $table->foreign('user_preparation_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    public function down()
    {
        Schema::table('material_ready_preparations', function (Blueprint $table) {
            $table->dropColumn('preparation_date'); 
            $table->dropForeign('users_user_preparation_id_foreign');
            $table->dropColumn('user_preparation_id'); 
        });
    }
}
