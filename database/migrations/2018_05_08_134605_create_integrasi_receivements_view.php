<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntegrasiReceivementsView extends Migration
{
    public function up()
    {
        /*DB::statement("
            CREATE OR REPLACE VIEW public.integerasi_receiving AS
                SELECT material_arrivals.id AS material_arrival_id,
                    material_arrivals.c_bpartner_id,
                    material_arrivals.c_orderline_id,
                    material_arrivals.no_packing_list,
                    material_arrivals.no_invoice,
                    material_arrivals.no_resi,
                    material_arrivals.item_id,
                    material_arrivals.item_desc,
                    material_arrivals.item_code,
                    material_arrivals.category,
                    material_arrivals.uom,
                    material_arrivals.warehouse_id,
                    material_arrivals.qty_carton::integer AS qty_carton,
                    carton_in.total AS qty_carton_receive,
                    material_arrivals.qty_upload,
                    item_rcv.total_rcv,
                    COALESCE(item_packinglist.total_item, 0) AS total_item_packing_list,
                    material_arrivals.is_integrate,
                    material_arrivals.integration_date,
                    material_arrivals.created_at AS receiving_date
                FROM material_arrivals
                    LEFT JOIN ( SELECT detail_material_arrivals.material_arrival_id,
                            count(0) AS total
                        FROM detail_material_arrivals
                        GROUP BY detail_material_arrivals.material_arrival_id) carton_in ON carton_in.material_arrival_id = material_arrivals.id
                    LEFT JOIN ( SELECT material_arrivals_1.no_invoice,
                            material_arrivals_1.no_packing_list,
                            material_arrivals_1.no_resi,
                            material_arrivals_1.c_bpartner_id,
                            count(0) AS total_rcv
                        FROM material_arrivals material_arrivals_1
                        WHERE material_arrivals_1.is_subcont = false
                        GROUP BY material_arrivals_1.no_invoice, material_arrivals_1.no_packing_list, material_arrivals_1.no_resi, material_arrivals_1.c_bpartner_id) item_rcv ON item_rcv.no_invoice::text = material_arrivals.no_invoice::text AND item_rcv.no_packing_list::text = material_arrivals.no_packing_list::text AND item_rcv.no_resi::text = material_arrivals.no_resi::text AND item_rcv.c_bpartner_id::text = material_arrivals.c_bpartner_id::text
                    LEFT JOIN ( SELECT item_packing_lists.no_invoice,
                            item_packing_lists.no_packing_list,
                            item_packing_lists.no_resi,
                            item_packing_lists.c_bpartner_id,
                            item_packing_lists.total_item
                        FROM item_packing_lists) item_packinglist ON item_packinglist.no_invoice::text = material_arrivals.no_invoice::text AND item_packinglist.no_packing_list::text = material_arrivals.no_packing_list::text AND item_packinglist.no_resi::text = material_arrivals.no_resi::text AND item_packinglist.c_bpartner_id::text = material_arrivals.c_bpartner_id::text
                WHERE material_arrivals.is_material_other = false and material_arrivals.is_ila = false AND carton_in.total >= material_arrivals.qty_carton::integer AND material_arrivals.is_subcont = false AND item_rcv.total_rcv = COALESCE(item_packinglist.total_item, 0)
                GROUP BY material_arrivals.created_at, item_packinglist.total_item, item_rcv.total_rcv, material_arrivals.id, material_arrivals.c_bpartner_id, material_arrivals.c_orderline_id, material_arrivals.no_packing_list, material_arrivals.no_invoice, material_arrivals.no_resi, material_arrivals.item_id, material_arrivals.item_desc, material_arrivals.item_code, material_arrivals.category, material_arrivals.uom, material_arrivals.warehouse_id, (material_arrivals.qty_carton::integer), carton_in.total, material_arrivals.qty_upload, material_arrivals.is_integrate, material_arrivals.integration_date;
        ");*/
    }

    public function down()
    {
        DB::statement('DROP VIEW integerasi_receiving CASCADE');
    }
}
