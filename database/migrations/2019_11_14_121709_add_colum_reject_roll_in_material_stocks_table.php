<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumRejectRollInMaterialStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_stocks', function (Blueprint $table) 
        {
            $table->boolean('is_reject_by_lot')->default(false);
            $table->boolean('is_reject_by_rma')->default(false);
            $table->timestamp('reject_by_rma_date')->nullable()->after('is_reject');
            $table->timestamp('reject_by_lot_date')->nullable()->after('is_reject');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
