<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnBarcodeInDetailMaterialPreparationsTable extends Migration
{
    public function up()
    {
        Schema::table('detail_material_preparations', function (Blueprint $table) {
            $table->string('barcode')->nullable();
        });
    }

    public function down()
    {
        Schema::table('detail_material_preparations', function (Blueprint $table) {
            $table->dropColumn('barcode'); 
        });
    }
}
