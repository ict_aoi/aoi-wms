<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnRemarkInMaterialPreparationsTable extends Migration
{
    public function up()
    {
        Schema::table('material_preparations', function (Blueprint $table) {
           $table->text('remark')->nullable();
        });
    }

    public function down()
    {
        Schema::table('material_preparations', function (Blueprint $table) {
            $table->dropColumn('remark'); 
        });
    }
}
