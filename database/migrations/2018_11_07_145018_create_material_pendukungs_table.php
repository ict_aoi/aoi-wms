<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialPendukungsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_pendukungs', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->integer('user_id');
            $table->char('kontrak_id',36);
            $table->string('material');
            $table->integer('qty');
            $table->string('uom',10);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('kontrak_id')->references('id')->on('kontraks')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_pendukungs');
    }
}
