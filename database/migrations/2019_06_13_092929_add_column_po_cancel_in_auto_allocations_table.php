<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPoCancelInAutoAllocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auto_allocations', function (Blueprint $table) {
            $table->string('status_po_buyer')->nullable();
            $table->timestamp('cancel_date')->nullable();
        });

        Schema::table('purchase_items', function (Blueprint $table) {
            $table->string('status_po_buyer')->nullable();
            $table->timestamp('cancel_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
