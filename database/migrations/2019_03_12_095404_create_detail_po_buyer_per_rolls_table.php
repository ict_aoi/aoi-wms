<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailPoBuyerPerRollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_po_buyer_per_rolls', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('detail_material_preparation_fabric_id',36)->nullable();
            $table->date('planning_date')->nullable();
            $table->string('po_buyer')->nullable();
            $table->string('article_no')->nullable();
            $table->string('style')->nullable();
            $table->string('style_in_job_order')->nullable();
            $table->string('job_order')->nullable();
            $table->double('qty_booking',15,8)->nullable();
            $table->string('warehouse_id')->nullable();
            $table->integer('user_id')->unsigned();
            $table->datetime('deleted_at')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('detail_material_preparation_fabric_id')->references('id')->on('detail_material_preparation_fabrics')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_po_buyer_per_rolls');
    }
}
