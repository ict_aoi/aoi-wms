<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnFromLocatorErpIdInMaterialMovements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_movements', function (Blueprint $table) 
        {
            $table->string('from_locator_erp_id')->nullable();
            $table->string('to_locator_erp_id')->nullable();
        });


        Schema::table('material_movement_lines', function (Blueprint $table) 
        {
            $table->string('supplier_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
