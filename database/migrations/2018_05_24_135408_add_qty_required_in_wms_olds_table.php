<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQtyRequiredInWmsOldsTable extends Migration
{
   public function up()
    {
        Schema::table('wms_olds', function (Blueprint $table) {
            $table->double('qty_in',15,8)->nullable();
        });
    }

    public function down()
    {
        Schema::table('wms_olds', function (Blueprint $table) {
            $table->dropColumn('qty_in');
            
        });
    }
}
