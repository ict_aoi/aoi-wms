<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumIsInsertedToAllocationItemInPurchaseItemTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_items', function (Blueprint $table) {
            $table->boolean('is_inserted_to_allocation_item')->default(false);
        });

        Schema::table('allocation_items', function (Blueprint $table) {
            $table->char('purchase_item_id',36)->nullable();
            $table->foreign('purchase_item_id')->references('id')->on('purchase_items')->onUpdate('cascade');
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
