<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnApprovalAccountingMmInDetailMaterialStocks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_material_stocks', function (Blueprint $table) {
        $table->integer('mm_user_id')->nullable();
        $table->integer('accounting_user_id')->nullable();
        $table->datetime('approve_date_mm')->nullable();
        $table->datetime('approve_date_accounting')->nullable();
        $table->integer('reject_user_id')->nullable();
        $table->datetime('reject_date')->nullable();
        $table->foreign('mm_user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        $table->foreign('accounting_user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_material_stocks', function (Blueprint $table) {
            $table->dropColumn('mm_user_id');
            $table->dropColumn('accounting_user_id');
            $table->dropColumn('approve_date_mm');
            $table->dropColumn('approve_date_accounting');
            $table->dropColumn('reject_user_id');
            $table->dropColumn('reject_date');
        });

    }
}
