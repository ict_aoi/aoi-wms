<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_items', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('c_order_id')->nullable();
            $table->string('c_bpartner_id')->nullable();
            $table->string('no_invoice')->nullable();
            $table->date('eta_date')->nullable();
            $table->string('document_no')->nullable();
            $table->string('po_buyer')->nullable();
            $table->string('item_code')->nullable();
            $table->string('supplier_name')->nullable();
            $table->string('warehouse')->nullable();
            $table->string('category')->nullable();
            $table->string('uom')->nullable();
            $table->string('qty')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_items');
    }
}
