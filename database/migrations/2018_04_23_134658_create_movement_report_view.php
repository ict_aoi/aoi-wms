<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovementReportView extends Migration
{
    public function up()
    {
        // DB::statement("
        // CREATE OR REPLACE VIEW public.movements_report AS
        //     SELECT union_tbl.po_buyer,
        //         union_tbl.document_no,
        //         union_tbl.item_code,
        //         union_tbl.item_desc,
        //         union_tbl.category,
        //         union_tbl.warehouse,
        //         union_tbl.from_location,
        //         union_tbl.to_destination,
        //         union_tbl.uom_conversion,
        //         union_tbl.job_order,
        //         union_tbl.style,
        //         union_tbl.qty_need,
        //         union_tbl.qty_movement,
        //         union_tbl.status,
        //         union_tbl.movement_date,
        //         union_tbl.created_at,
        //         union_tbl.user_name
        //     FROM ( SELECT material_requirements.po_buyer,
        //                 NULL::text AS document_no,
        //                 btrim(upper(material_requirements.item_code::text)) AS item_code,
        //                 btrim(upper(material_requirements.item_desc::text)) AS item_desc,
        //                 material_requirements.category,
        //                 NULL::text AS warehouse,
        //                     CASE
        //                         WHEN material_requirements.item_code in ('62002417-0000-00', 'ST140050.00-0000-00', 'ST140051.00-0000-00', 'ST140005.00-0000-00', 'ST140041.00-0000-00', 'ST140042.00-0000-00', 'HT140022.00-0000-00', 'ST140006.00-0000-00', 'ST140035.00-0000-00', 'ST140023.00-0000-00', 'HT140028.00-0000-00', 'ST140012.00-0000-00', 'HT140023.00-0000-00', 'HT150001.00-0000-00', 'ST140015.00-0000-00', 'ST140047.00-0000-00', 'ST140026.00-0000-00', 'ST150021.00-0000-00', 'ST140054.00-0000-00', 'ST140014.00-0000-00', 'ST140013.00-0000-00', 'ST140045.00-0000-00', 'ST140007.00-0000-00', 'ST140020.00-0000-00', 'ST140024.00-0000-00', 'ST140057.00-0000-00', 'ST140011.00-0000-00', 'ST140009.00-0000-00', 'ST140046.00-0000-00', 'ST150027.00-0000-00', 'ST150026.00-0000-00', 'ST140032.00-0000-00', 'ST140017.00-0000-00', 'ST150024.00-0000-00', 'ST140031.00-0000-00', 'ST140027.00-0000-00',
        //                         'ST140055.00-0000-00', 'HT140026.00-0000-00', 'ST140029.00-0000-00', 'HT140024.00-0000-00',
        //                         'ST140016.00-0000-00', 'ST140028.00-0000-00', 'ST140008.00-0000-00', 'ST140021.00-0000-00',
        //                         'ST150022.00-0000-00', 'ST140030.00-0000-00', 'ST140034.00-0000-00', 'ST140022.00-0000-00', 'ST150006.00-0000-00',
        //                         'ST150004.00-0000-00', 'ST150003.00-0000-00', 'ST150008.00-0000-00', 'HT150007.00-0000-00', 'HT150004.00-0000-00',
        //                         'ST150015.00-0000-00', 'ST150014.00-0000-00', 'ST150013.00-0000-00', 'ST150018.00-0000-00', 'ST150016.00-0000-00',
        //                         'ST150017.00-0000-00', 'HT150008.00-0000-00', 'ST150019.00-0000-00', 'ST150023.00-0000-00', 'ST150020.00-0000-00',
        //                         'HT150009.00-0000-00', 'ST150028.00-0000-00', 'LB150004.00-0000-00', 'ST170004.00-0000-00', 'HT170002.00-0000-00',
        //                         'ST150029.00-0000-00', 'ST150030.00-0000-00', 'ST170012.00-0000-00', 'HT170005.00-0000-00', 'HT170006.00-0000-00',
        //                         'ST170013.00-0000-00 ','ST170014.00-0000-00','HT140025.00-0000-00', 'ST170002.00-0000-00',
        //                         'HT170007.00-0000-00 ','ST170015.00-0000-00','ST170016.00-0000-00','HT170003.00-0000-00',
        //                         'ST170005.00-0000-00', 'ST170017.00-0000-00', 'ST170013.00-0000-00', 'ST170014.00-0000-00', 'HT170007.00-0000-00') or material_requirements.category::text = ANY (ARRAY['LP'::character varying::text, 'MO'::character varying::text]) THEN 'expanse'::text
        //                         ELSE NULL::text
        //                     END AS from_location,
        //                     CASE
        //                         WHEN material_requirements.item_code in ('62002417-0000-00', 'ST140050.00-0000-00', 'ST140051.00-0000-00', 'ST140005.00-0000-00', 'ST140041.00-0000-00', 'ST140042.00-0000-00', 'HT140022.00-0000-00', 'ST140006.00-0000-00', 'ST140035.00-0000-00', 'ST140023.00-0000-00', 'HT140028.00-0000-00', 'ST140012.00-0000-00', 'HT140023.00-0000-00', 'HT150001.00-0000-00', 'ST140015.00-0000-00', 'ST140047.00-0000-00', 'ST140026.00-0000-00', 'ST150021.00-0000-00', 'ST140054.00-0000-00', 'ST140014.00-0000-00', 'ST140013.00-0000-00', 'ST140045.00-0000-00', 'ST140007.00-0000-00', 'ST140020.00-0000-00', 'ST140024.00-0000-00', 'ST140057.00-0000-00', 'ST140011.00-0000-00', 'ST140009.00-0000-00', 'ST140046.00-0000-00', 'ST150027.00-0000-00', 'ST150026.00-0000-00', 'ST140032.00-0000-00', 'ST140017.00-0000-00', 'ST150024.00-0000-00', 'ST140031.00-0000-00', 'ST140027.00-0000-00',
        //                         'ST140055.00-0000-00', 'HT140026.00-0000-00', 'ST140029.00-0000-00', 'HT140024.00-0000-00',
        //                         'ST140016.00-0000-00', 'ST140028.00-0000-00', 'ST140008.00-0000-00', 'ST140021.00-0000-00',
        //                         'ST150022.00-0000-00', 'ST140030.00-0000-00', 'ST140034.00-0000-00', 'ST140022.00-0000-00', 'ST150006.00-0000-00',
        //                         'ST150004.00-0000-00', 'ST150003.00-0000-00', 'ST150008.00-0000-00', 'HT150007.00-0000-00', 'HT150004.00-0000-00',
        //                         'ST150015.00-0000-00', 'ST150014.00-0000-00', 'ST150013.00-0000-00', 'ST150018.00-0000-00', 'ST150016.00-0000-00',
        //                         'ST150017.00-0000-00', 'HT150008.00-0000-00', 'ST150019.00-0000-00', 'ST150023.00-0000-00', 'ST150020.00-0000-00',
        //                         'HT150009.00-0000-00', 'ST150028.00-0000-00', 'LB150004.00-0000-00', 'ST170004.00-0000-00', 'HT170002.00-0000-00',
        //                         'ST150029.00-0000-00', 'ST150030.00-0000-00', 'ST170012.00-0000-00', 'HT170005.00-0000-00', 'HT170006.00-0000-00',
        //                         'ST170013.00-0000-00 ','ST170014.00-0000-00','HT140025.00-0000-00', 'ST170002.00-0000-00',
        //                         'HT170007.00-0000-00 ','ST170015.00-0000-00','ST170016.00-0000-00','HT170003.00-0000-00',
        //                         'ST170005.00-0000-00', 'ST170017.00-0000-00', 'ST170013.00-0000-00', 'ST170014.00-0000-00', 'HT170007.00-0000-00') or material_requirements.category::text = ANY (ARRAY['LP'::character varying::text, 'MO'::character varying::text]) THEN 'expanse'::text
        //                         ELSE NULL::text
        //                     END AS to_destination,
        //                 material_requirements.uom AS uom_conversion,
        //                 material_requirements.job_order,
        //                 string_agg(material_requirements.style::text, ','::text) AS style,
        //                     CASE
        //                         WHEN material_requirements.category::text = ANY (ARRAY['LP'::character varying::text, 'MO'::character varying::text]) THEN NULL::double precision
        //                         ELSE sum(material_requirements.qty_required)
        //                     END AS qty_need,
        //                 NULL::double precision AS qty_movement,
        //                     CASE
        //                         WHEN material_requirements.category::text = ANY (ARRAY['LP'::character varying::text, 'MO'::character varying::text]) THEN 'expanse'::text
        //                         ELSE 'NOT RECEIVE'::text
        //                     END AS status,
        //                 NULL::text AS movement_date,
        //                 NULL::timestamp without time zone AS created_at,
        //                 NULL::character varying AS user_name
        //             FROM material_requirements
        //             WHERE NOT ((material_requirements.po_buyer::text, material_requirements.item_code::text) IN ( SELECT material_ready_preparations.po_buyer,
        //                         material_ready_preparations.item_code
        //                     FROM material_ready_preparations))
        //             GROUP BY material_requirements.po_buyer, material_requirements.item_code, material_requirements.item_desc, material_requirements.category, material_requirements.uom, material_requirements.job_order
        //             UNION
        //             SELECT material_preparations.po_buyer,
        //                 material_ready_preparations.document_no,
        //                 btrim(upper(material_ready_preparations.item_code::text)) AS item_code,
        //                 btrim(upper(material_ready_preparations.item_desc::text)) AS item_desc,
        //                 material_ready_preparations.category,
        //                 material_preparations.warehouse,
        //                 'SUPPLIER'::text AS from_location,
        //                 'RECEIVING ACC-RCV.1.1'::text AS to_destination,
        //                 material_preparations.uom_conversion,
        //                 material_preparations.job_order,
        //                 material_preparations.style,
        //                 material_requirements.total AS qty_need,
        //                 material_preparations.qty_conversion AS qty_movement,
        //                 'RECEIVE'::text AS status,
        //                 to_char(material_preparations.created_at, 'dd/mm/yyyy HH24:MI:SS'::text) AS movement_date,
        //                 material_preparations.created_at,
        //                 users.name AS user_name
        //             FROM material_preparations
        //                 JOIN detail_material_preparations ON detail_material_preparations.material_preparation_id = material_preparations.id
        //                 JOIN material_arrivals ON detail_material_preparations.material_arrival_id = material_arrivals.id
        //                 JOIN material_ready_preparations ON material_ready_preparations.material_arrival_id = material_arrivals.id AND material_ready_preparations.po_buyer::text = material_preparations.po_buyer::text
        //                 JOIN users ON users.id = material_preparations.user_id
        //                 LEFT JOIN ( SELECT material_requirements_1.po_buyer,
        //                         material_requirements_1.item_code,
        //                         sum(material_requirements_1.qty_required) AS total
        //                     FROM material_requirements material_requirements_1
        //                     GROUP BY material_requirements_1.po_buyer, material_requirements_1.item_code) material_requirements ON material_requirements.po_buyer::text = material_ready_preparations.po_buyer::text AND material_requirements.item_code::text = material_ready_preparations.item_code::text
        //             GROUP BY material_ready_preparations.item_code, material_preparations.warehouse, material_ready_preparations.category, material_preparations.po_buyer, material_ready_preparations.document_no, material_ready_preparations.item_desc, material_preparations.uom_conversion, material_preparations.job_order, material_preparations.style, material_requirements.total, material_preparations.qty_conversion, material_preparations.created_at, users.name
        //             UNION
        //             SELECT material_preparations.po_buyer,
        //                 material_arrivals.document_no,
        //                 material_arrivals.item_code,
        //                 material_arrivals.item_desc,
        //                 material_arrivals.category,
        //                 material_preparations.warehouse,
        //                 'RECEIVING ACC-RCV.1.1'::text AS from_location,
        //                 'REJECT-RJT.1.1'::text AS to_destination,
        //                 material_preparations.uom_conversion,
        //                 material_preparations.job_order,
        //                 material_preparations.style,
        //                 material_requirements.total AS qty_need,
        //                 0 AS qty_movement,
        //                 'REJECT'::text AS status,
        //                 to_char(material_checks.created_at, 'dd/mm/yyyy HH24:MI:SS'::text) AS movement_date,
        //                 material_checks.created_at,
        //                 users.name AS user_name
        //             FROM material_checks
        //                 LEFT JOIN material_preparations ON material_preparations.id = material_checks.material_preparation_id::bpchar
        //                 LEFT JOIN detail_material_preparations ON detail_material_preparations.material_preparation_id = material_preparations.id
        //                 LEFT JOIN material_arrivals ON detail_material_preparations.material_arrival_id = material_arrivals.id
        //                 LEFT JOIN users ON users.id = material_checks.user_id
        //                 LEFT JOIN ( SELECT material_requirements_1.po_buyer,
        //                         material_requirements_1.item_code,
        //                         sum(material_requirements_1.qty_required) AS total
        //                     FROM material_requirements material_requirements_1
        //                     GROUP BY material_requirements_1.po_buyer, material_requirements_1.item_code) material_requirements ON material_requirements.po_buyer::text = material_preparations.po_buyer::text AND material_requirements.item_code::text = material_preparations.item_code::text
        //             WHERE material_checks.status::text = 'REJECT'::text
        //             UNION
        //             SELECT material_movements.po_buyer,
        //                 material_ready_preparations.document_no,
        //                 material_ready_preparations.item_code,
        //                 material_ready_preparations.item_desc,
        //                 material_ready_preparations.category,
        //                 material_preparations.warehouse,
        //                 from_location.code AS from_location,
        //                 destination.code AS to_destination,
        //                 material_preparations.uom_conversion,
        //                 material_preparations.job_order,
        //                 material_preparations.style,
        //                 material_requirements.total AS qty_need,
        //                 material_movement_lines.qty_movement,
        //                     CASE
        //                         WHEN material_movements.status::text = 'in'::text THEN 'READY PREPARE'::text
        //                         WHEN material_movements.status::text = 'out'::text THEN 'SUPPLIED'::text
        //                         ELSE NULL::text
        //                     END AS status,
        //                 to_char(material_movement_lines.created_at, 'dd/mm/yyyy HH24:MI:SS'::text) AS movement_date,
        //                 material_movement_lines.created_at,
        //                 users.name AS user_name
        //             FROM material_movement_lines
        //                 JOIN material_movements ON material_movements.id = material_movement_lines.material_movement_id
        //                 JOIN users ON users.id = material_movement_lines.user_id
        //                 JOIN material_preparations ON material_preparations.id = material_movement_lines.material_preparation_id
        //                 JOIN detail_material_preparations ON detail_material_preparations.material_preparation_id = material_preparations.id
        //                 JOIN material_arrivals ON detail_material_preparations.material_arrival_id = material_arrivals.id
        //                 JOIN material_ready_preparations ON material_ready_preparations.material_arrival_id = material_arrivals.id AND material_ready_preparations.po_buyer::text = material_movements.po_buyer::text
        //                 JOIN ( SELECT locators.id,
        //                         locators.code
        //                     FROM locators
        //                     WHERE locators.is_active = true
        //                     GROUP BY locators.id, locators.code) from_location ON from_location.id = material_movements.from_location
        //                 JOIN ( SELECT locators.id,
        //                         locators.code
        //                     FROM locators
        //                     WHERE locators.is_active = true
        //                     GROUP BY locators.id, locators.code) destination ON destination.id = material_movements.to_destination
        //                 LEFT JOIN ( SELECT material_requirements_1.po_buyer,
        //                         material_requirements_1.item_code,
        //                         sum(material_requirements_1.qty_required) AS total
        //                     FROM material_requirements material_requirements_1
        //                     GROUP BY material_requirements_1.po_buyer, material_requirements_1.item_code) material_requirements ON material_requirements.po_buyer::text = material_ready_preparations.po_buyer::text AND material_requirements.item_code::text = material_ready_preparations.item_code::text
        //             GROUP BY material_preparations.warehouse, material_ready_preparations.item_code, material_ready_preparations.category, material_movements.po_buyer, material_ready_preparations.document_no, material_ready_preparations.item_desc, from_location.code, destination.code, material_preparations.uom_conversion, material_preparations.job_order, material_preparations.style, material_requirements.total, material_movement_lines.qty_movement, material_movements.status, material_movement_lines.created_at, users.name) union_tbl
        //     GROUP BY union_tbl.po_buyer, union_tbl.document_no, union_tbl.item_code, union_tbl.item_desc, union_tbl.category, union_tbl.warehouse, union_tbl.from_location, union_tbl.to_destination, union_tbl.uom_conversion, union_tbl.job_order, union_tbl.style, union_tbl.qty_need, union_tbl.qty_movement, union_tbl.status, union_tbl.movement_date, union_tbl.created_at, union_tbl.user_name
        //     ORDER BY union_tbl.po_buyer DESC, union_tbl.item_desc DESC, (
        //             CASE
        //                 WHEN union_tbl.status = 'NOT RECEIVE'::text THEN 1
        //                 WHEN union_tbl.status = 'RECEIVE'::text THEN 2
        //                 WHEN union_tbl.status = 'REJECT'::text THEN 3
        //                 WHEN union_tbl.status = 'READY PREPARE'::text THEN 4
        //                 WHEN union_tbl.status = 'SUPPLIED'::text THEN 5
        //                 ELSE NULL::integer
        //             END), union_tbl.movement_date;
        // ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW movements_report CASCADE');
    }
}
