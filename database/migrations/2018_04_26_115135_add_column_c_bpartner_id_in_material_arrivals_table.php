<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCBpartnerIdInMaterialArrivalsTable extends Migration
{
    public function up()
    {
        Schema::table('material_arrivals', function (Blueprint $table) {
            $table->string('c_bpartner_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('material_arrivals', function (Blueprint $table) {
            $table->dropColumn('c_bpartner_id');
        });
    }
}
