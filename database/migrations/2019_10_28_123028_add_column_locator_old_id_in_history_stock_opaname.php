<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnLocatorOldIdInHistoryStockOpaname extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('history_material_stock_opnames', function (Blueprint $table) {
            $table->char('locator_old_id',36)->nullable();
            $table->char('locator_new_id',36)->nullable();
            
            $table->foreign('locator_old_id')->references('id')->on('locators')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('locator_new_id')->references('id')->on('locators')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
