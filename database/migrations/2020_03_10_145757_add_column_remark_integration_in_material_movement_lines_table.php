<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnRemarkIntegrationInMaterialMovementLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_movement_lines', function (Blueprint $table) 
        {
            $table->text('remark_integration')->nullable();
        });

        Schema::table('material_movement_per_sizes', function (Blueprint $table) 
        {
            $table->text('remark_integration')->nullable();
        });

        Schema::table('material_movements', function (Blueprint $table) 
        {
            $table->text('remark_integration')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
