<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoAllocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_allocations', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->date('lc_date')->nullable();
            $table->string('season')->nullable();
            $table->string('type_stock')->nullable();
            $table->string('document_no')->nullable();
            $table->string('c_bpartner_id')->nullable();
            $table->string('supplier_name')->nullable();
            $table->string('po_buyer')->nullable();
            $table->string('item_code')->nullable();
            $table->string('item_desc')->nullable();
            $table->string('category')->nullable();
            $table->string('warehouse_name')->nullable();
            $table->string('warehouse_id')->nullable();
            $table->string('uom')->nullable();
            $table->double('qty_allocation',18,2)->nullable();
            $table->double('qty_outstanding',18,2)->nullable();
            $table->double('qty_allocated',18,2)->nullable();
            $table->boolean('is_fabric')->nullable();
            $table->boolean('is_already_generate_form_booking')->nullable();
            $table->datetime('generate_form_booking')->nullable();
            $table->boolean('is_inserted_to_allocation_item')->default(false);
            $table->timestamps();
        });

        Schema::table('allocation_items', function (Blueprint $table) {
            $table->char('auto_allocation_id',36)->nullable();
            $table->foreign('auto_allocation_id')->references('id')->on('auto_allocations')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auto_allocations');
    }
}
