<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnMaterialPlanningFabricIdInDetailPoBuyerPreRollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_po_buyer_per_rolls', function (Blueprint $table) {
            $table->char('material_planning_fabric_id',36)->nullable();
            $table->foreign('material_planning_fabric_id')->references('id')->on('material_planning_fabrics')->onUpdate('cascade')->onDelete('cascade');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
