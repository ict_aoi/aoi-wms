<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsSubcontColumnInAreasTables extends Migration
{
    public function up()
    {
        Schema::table('areas', function (Blueprint $table) {
            $table->boolean('is_subcont')->nullable()->default(false);
        });
    }

    public function down()
    {
        Schema::table('areas', function (Blueprint $table) {
            $table->dropColumn('is_subcont');
            
        });
    }
}
