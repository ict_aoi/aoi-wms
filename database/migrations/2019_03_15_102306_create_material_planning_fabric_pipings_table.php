<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialPlanningFabricPipingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_planning_fabric_pipings', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('material_planning_fabric_id',36)->nullable();
            $table->string('part_no')->nullable();
            $table->double('qty_booking',15,8)->nullable();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
            $table->foreign('material_planning_fabric_id')->references('id')->on('material_planning_fabrics')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_planning_fabric_pipings');
    }
}
