<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIntegrationFreeStockDate2Tables extends Migration
{
    public function up()
    {
        Schema::table('material_preparations', function (Blueprint $table) 
        {
            $table->boolean('is_from_closing')->default(false)->after('is_from_barcode_bom');;
            $table->boolean('is_need_inserted_to_locator_free_stock_erp')->default(false)->after('is_from_closing');;
            $table->timestamp('first_inserted_to_locator_date')->nullable()->after('first_print_date');;
            $table->timestamp('insert_to_locator_free_stock_erp_date')->nullable()->after('first_inserted_to_locator');;
            $table->timestamp('insert_to_locator_inventory_erp_date')->nullable()->after('first_inserted_to_locator');;
        });
    }

    public function down()
    {
        //
    }
}
