<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailKontraksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_kontraks', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->integer('user_id');
            $table->char('kontrak_id',36);
            $table->string('no_order',15);
            $table->string('keterangan',20);
            $table->string('style',20);
            $table->string('kategori',10);
            $table->string('destination',30);
            $table->integer('total_qty');
            $table->date('statistic_date');
            $table->double('price');
            $table->double('amount');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('kontrak_id')->references('id')->on('kontraks')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_kontraks');
    }
}
