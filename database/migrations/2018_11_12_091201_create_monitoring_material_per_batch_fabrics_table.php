<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonitoringMaterialPerBatchFabricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monitoring_material_per_batch_fabrics', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('document_no')->nullable();
            $table->string('no_invoice')->nullable();
            $table->string('c_bpartner_id')->nullable();
            $table->string('supplier_name')->nullable();
            $table->string('item_code')->nullable();
            $table->string('item_desc')->nullable();
            $table->string('color')->nullable();
            $table->integer('total_batch_number')->default(0);
            $table->string('warehouse_id')->nullable();
            $table->datetime('receive_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monitoring_material_per_batch_fabrics');
    }
}
