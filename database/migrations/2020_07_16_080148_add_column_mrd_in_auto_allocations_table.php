<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnMrdInAutoAllocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auto_allocations', function (Blueprint $table) 
        {
            $table->date('mrd')->nullable();
            $table->date('dd_pi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auto_allocations', function (Blueprint $table) 
        {
            $table->dropColumn('mrd');
            $table->dropColumn('dd_pi');
        });
    }
}
