<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnMaterialStockIdInMaterialChecksTable extends Migration
{
    public function up()
    {
        Schema::table('material_checks', function (Blueprint $table) {
            $table->char('material_stock_id',36)->nullable()->unsigned();
            $table->foreign('material_stock_id')->references('id')->on('material_stocks')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
