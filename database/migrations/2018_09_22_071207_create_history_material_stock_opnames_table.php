<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryMaterialStockOpnamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_material_stock_opnames', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('material_stock_id',36)->unsigned();
            $table->double('available_stock_old',15,8)->nullable();
            $table->double('available_stock_new',15,8)->nullable();
            $table->double('operator',15,8)->nullable();
            $table->text('note')->nullable();
            $table->datetime('sto_date')->nullable();
            $table->text('sto_note')->nullable();
            $table->integer('user_approval_id')->unsigned()->nullable();
            $table->datetime('approval_date')->nullable();
            $table->integer('user_reject_id')->unsigned()->nullable();
            $table->datetime('reject_date')->nullable();
            $table->integer('user_id')->unsigned();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
            $table->foreign('user_approval_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_reject_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('material_stock_id')->references('id')->on('material_stocks')->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_material_stock_opnames');
    }
}
