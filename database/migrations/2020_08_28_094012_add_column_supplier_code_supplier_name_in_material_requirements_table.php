<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSupplierCodeSupplierNameInMaterialRequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_requirements', function (Blueprint $table) 
        {
            $table->string('supplier_code')->nullable();
            $table->string('supplier_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_requirements', function (Blueprint $table) 
        {
            $table->dropColumn('supplier_code');
            $table->dropColumn('supplier_name');
        });
    }
}
