<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailMaterialArrivalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_material_arrivals', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('material_arrival_id',36);
            $table->text('po_buyer')->nullable();
            $table->string('barcode_supplier');
            $table->double('qty_delivered',15,8)->nullable();
            $table->double('qty_upload',15,8)->nullable();
            $table->integer('user_id')->unsigned();
            $table->boolean('is_active')->default(true);
            $table->datetime('preparation_date')->nullable();
           

            $table->foreign('material_arrival_id')->references('id')->on('material_arrivals')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_material_arrivals');
    }
}
