<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPoBuyerOldInHistoryAutoAllocationsTable extends Migration
{
    public function up()
    {
        Schema::table('history_auto_allocations', function (Blueprint $table) {
            $table->string('po_buyer_old')->nullable();
            $table->string('item_code_old')->nullable();
            $table->string('item_id_old')->nullable();

            $table->string('po_buyer_new')->nullable();
            $table->string('item_code_new')->nullable();
            $table->string('item_id_new')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
