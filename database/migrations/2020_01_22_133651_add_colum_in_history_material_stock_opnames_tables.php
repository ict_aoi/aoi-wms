<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumInHistoryMaterialStockOpnamesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_stocks', function (Blueprint $table) 
        {
            $table->boolean('is_roll_cancel')->default(false);
        });

        Schema::table('history_material_stock_opnames', function (Blueprint $table) 
        {
            $table->string('source')->nullable();
            $table->timestamp('accounting_approval_date')->nullable();
            $table->integer('accounting_user_id')->nullable();
            $table->foreign('accounting_user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
