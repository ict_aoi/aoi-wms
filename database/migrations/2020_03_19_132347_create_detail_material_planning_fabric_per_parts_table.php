<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailMaterialPlanningFabricPerPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_material_planning_fabric_per_parts', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('material_planning_fabric_id',36)->unsigned();
            $table->string('part_no')->nullable();
            $table->double('qty_per_part',15,8)->nullable(0);
            $table->timestamps();

            $table->foreign('material_planning_fabric_id')->references('id')->on('material_planning_fabrics')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_material_planning_fabric_per_parts');
    }
}
