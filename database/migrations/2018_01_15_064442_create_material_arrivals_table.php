<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialArrivalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_arrivals', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('po_detail_id')->nullable();
            $table->char('material_subcont_id',36)->nullable()->unsigned();
            $table->char('locator_id',36)->nullable()->unsigned();
            $table->string('item_id')->nullable();
            $table->string('item_code')->nullable();
            $table->string('item_desc')->nullable();
            $table->string('po_buyer')->nullable();
            $table->string('supplier_name')->nullable();
            $table->string('document_no')->nullable();
            $table->string('c_order_id')->nullable();
            $table->string('c_orderline_id')->nullable();
            $table->string('warehouse_id')->nullable();
            $table->string('category')->nullable();
            $table->string('uom')->nullable();
            $table->string('no_packing_list')->nullable();
            $table->string('type_po')->nullable();
            $table->string('no_resi')->nullable();
            $table->string('prepared_status')->nullable();
            $table->string('remark')->nullable();
            $table->string('no_surat_jalan')->nullable();
            $table->string('no_invoice')->nullable();
            $table->datetime('etd_date')->nullable();
            $table->datetime('eta_date')->nullable();
            $table->double('qty_delivered',15,8)->nullable();
            $table->double('qty_carton',15,8)->nullable();
            $table->double('qty_ordered',15,8)->nullable();
            $table->double('qty_entered',15,8)->nullable();
            $table->double('qty_upload',15,8)->nullable();
            $table->double('foc',15,8)->nullable();
            $table->datetime('preparation_date')->nullable();
            $table->string('approval_status')->nullable();
            $table->boolean('is_active')->default(true);
            $table->boolean('is_ready_to_prepare')->default(false);
            $table->boolean('is_subcont')->default(false);
            $table->boolean('is_material_other')->default(false);
            $table->datetime('allocated_date')->nullable();
            $table->boolean('is_allocated')->default(false);
            $table->integer('user_id')->unsigned();
            $table->datetime('approval_date')->nullable();
            $table->integer('user_approval_id')->unsigned()->nullable();
            $table->datetime('reject_date')->nullable();
            $table->integer('user_reject_id')->unsigned()->nullable();
           
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_approval_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_reject_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('locator_id')->references('id')->on('locators')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('material_subcont_id')->references('id')->on('material_subconts')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
           
        });
    }

    public function down()
    {
        Schema::dropIfExists('material_arrivals');
    }
}
