<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCOrderId extends Migration
{
    public function up()
    {
        Schema::table('po_buyer_logs', function (Blueprint $table) {
            $table->string('c_order_id')->nullable();
            $table->string('c_orderline_id')->nullable();
            $table->string('c_bpartner_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('po_buyer_logs', function (Blueprint $table) {
            $table->dropColumn('c_order_id');
            $table->dropColumn('c_orderline_id');
            $table->dropColumn('c_bpartner_id');
        });
    }
}
