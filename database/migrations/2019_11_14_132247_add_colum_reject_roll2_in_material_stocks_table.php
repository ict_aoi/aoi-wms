<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumRejectRoll2InMaterialStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_stocks', function (Blueprint $table) 
        {
            $table->double('qty_reject_by_inspect',15,8)->default(0);
            $table->double('qty_reject_non_by_inspect',15,8)->default(0);
            $table->double('qty_reject_by_short_roll',15,8)->default(0);
            $table->timestamp('movement_to_locator_reject_date_from_rma')->nullable()->after('is_reject');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
