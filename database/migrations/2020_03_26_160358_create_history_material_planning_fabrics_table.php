<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryMaterialPlanningFabricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_material_planning_fabrics', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('po_buyer')->nullable();
            $table->string('item_id')->nullable();
            $table->string('item_code')->nullable();
            $table->string('style')->nullable();
            $table->string('planning_date')->nullable();
            $table->string('warehouse_id')->nullable();
            $table->string('article_no')->nullable();
            $table->string('color')->nullable();
            $table->string('job_order')->nullable();
            $table->boolean('is_piping')->default(false);
            $table->string('uom')->nullable();
            $table->double('qty_consumtion',15,8)->default(0);
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_material_planning_fabrics');
    }
}
