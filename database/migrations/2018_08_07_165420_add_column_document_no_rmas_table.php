<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDocumentNoRmasTable extends Migration
{
    public function up()
    {
        Schema::table('rmas', function (Blueprint $table) {
            $table->string('document_no')->nullable();
            $table->string('no_packing_list')->nullable();
            $table->string('no_invoice')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rmas', function (Blueprint $table) {
            $table->dropColumn('document_no');
            $table->dropColumn('no_packing_list');
            $table->dropColumn('no_invoice');
        });
    }
}
