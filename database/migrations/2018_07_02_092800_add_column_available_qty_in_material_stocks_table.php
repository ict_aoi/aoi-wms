<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAvailableQtyInMaterialStocksTable extends Migration
{
    public function up()
    {
        Schema::table('material_stocks', function (Blueprint $table) {
            $table->double('available_qty',15,8)->nullable();
        });
    }

    public function down()
    {
        Schema::table('material_stocks', function (Blueprint $table) {
            $table->dropColumn('available_qty'); 
        });
    }
}
