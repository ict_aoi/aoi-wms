<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumDetailMaterialPreparationFabricIdInMovementStockHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movement_stock_histories', function (Blueprint $table) 
        {
            $table->char('detail_material_preparation_fabric_id',36)->unsigned()->nullable();
            $table->foreign('detail_material_preparation_fabric_id')->references('id')->on('detail_material_preparation_fabrics')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
