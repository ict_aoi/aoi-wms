<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIntegrationInMaterialChecksTable extends Migration
{
    public function up()
    {
        Schema::table('material_checks', function (Blueprint $table) 
        {
            $table->boolean('is_reject')->default(false);
            $table->timestamp('insert_to_locator_reject_erp_date')->nullable()->after('is_reject');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
