<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSummaryStockFabricIdInAllocationItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('allocation_items', function (Blueprint $table) {
            $table->char('summary_stock_fabric_id',36)->nullable();
            $table->foreign('summary_stock_fabric_id')->references('id')->on('summary_stock_fabrics')->onUpdate('cascade')->onDelete('cascade');

            $table->string('po_buyer_source')->nullable();
            $table->string('c_bpartner_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
