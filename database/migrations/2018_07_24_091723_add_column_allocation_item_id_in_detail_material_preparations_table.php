<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAllocationItemIdInDetailMaterialPreparationsTable extends Migration
{
   public function up()
    {
        Schema::table('detail_material_preparations', function (Blueprint $table) {
            $table->char('allocation_item_id',36)->nullable()->unsigned();
            $table->foreign('allocation_item_id')->references('id')->on('allocation_items')->onDelete('cascade')->onUpdate('cascade');
        });
    }

   public function down()
    {
        Schema::table('detail_material_preparations', function (Blueprint $table) {
            $table->dropForeign('allocation_items_allocation_item_id_foreign');
            $table->dropColumn('allocation_item_id'); 
        });
    }
}
