<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('po_suppliers', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('c_order_id')->unsigned()->nullable();
            $table->string('supplier_id')->unsigned()->nullable();
            $table->string('c_bpartner_id')->nullable();
            $table->string('document_no')->nullable();
            $table->date('integration_date')->nullable();
            $table->timestamps();
            $table->foreign('supplier_id')->references('id')->on('suppliers')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('po_suppliers');
    }
}
