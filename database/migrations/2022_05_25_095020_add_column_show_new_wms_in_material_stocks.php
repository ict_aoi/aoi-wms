<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnShowNewWmsInMaterialStocks extends Migration
{
    
    public function up()
    {
        Schema::table('material_stocks', function (Blueprint $table) {
            $table->boolean('show_new_wms')->default(false);
        });
    }

    
}
