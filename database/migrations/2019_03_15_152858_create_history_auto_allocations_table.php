<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryAutoAllocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_auto_allocations', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('auto_allocation_id',36)->nullable();
            
            $table->string('document_no_old')->nullable();
            $table->string('c_bpartner_id_old')->nullable();
            $table->string('supplier_name_old')->nullable();
            $table->double('qty_allocated_old',15,8)->nullable();
            $table->string('warehouse_id_old')->nullable();
            
            $table->string('document_no_new')->nullable();
            $table->string('c_bpartner_id_new')->nullable();
            $table->string('supplier_name_new')->nullable();
            $table->double('qty_allocated_new',15,8)->nullable();
            $table->string('warehouse_id_new')->nullable();
            
            $table->double('operator',15,8)->nullable();
            $table->text('note')->nullable();
            $table->boolean('is_integrate')->default(false);
            $table->datetime('integration_date')->nullable();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
            $table->foreign('auto_allocation_id')->references('id')->on('auto_allocations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_auto_allocations');
    }
}
