<?php use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialOthersTable extends Migration
{
    public function up()
    {
        /*Schema::create('material_others', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('criteria_id',36)->nullable()->unsigned();
            
            $table->char('material_arrival_id',36)->nullable()->unsigned();
            $table->string('type')->nullable(); // GENERAL OR MISCELLANEOUS ITEM
            $table->string('document_no')->nullable();
            $table->string('item_code')->nullable();
            $table->string('item_desc')->nullable();
            $table->string('category')->nullable();
            $table->text('system_note')->nullable();
            $table->string('warehouse')->nullable();
            $table->string('uom')->nullable();
            $table->decimal('qty',15,8);
            $table->string('approval_status');
            $table->boolean('is_know_datasource')->nullable();
            $table->boolean('is_active')->default(true);
            $table->date('approval_date')->nullable();
            $table->date('reject_date')->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('user_approval_id')->unsigned()->nullable();
            $table->foreign('user_approval_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('user_reject_id')->unsigned()->nullable();
            $table->foreign('user_reject_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
      
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();

            $table->foreign('material_arrival_id')->references('id')->on('material_arrivals')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('criteria_id')->references('id')->on('criterias')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
           
        });*/
    }

    public function down()
    {
        //Schema::dropIfExists('material_others');
    }
}
