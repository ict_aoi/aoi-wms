<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnArticleNoInAllocationItemsTable extends Migration
{
    public function up()
    {
         Schema::table('allocation_items', function (Blueprint $table) {
            $table->string('article_no')->nullable();
        });
    }

    public function down()
    {
        Schema::table('allocation_items', function (Blueprint $table) {
            $table->dropColumn('article_no');
        });
    }
}
