<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsReverseInMaterialMovementSizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     //reverse_date
    public function up()
    {
        Schema::table('material_movement_per_sizes', function (Blueprint $table) 
        {
            $table->timestamp('reverse_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
