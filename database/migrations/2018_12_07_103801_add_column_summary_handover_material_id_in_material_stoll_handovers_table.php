<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSummaryHandoverMaterialIdInMaterialStollHandoversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_roll_handover_fabrics', function (Blueprint $table) {
            $table->char('summary_handover_material_id',36)->nullable();
            $table->foreign('summary_handover_material_id')->references('id')->on('summary_handover_materials')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
