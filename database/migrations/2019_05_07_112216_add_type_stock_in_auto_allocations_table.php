<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeStockInAutoAllocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auto_allocations', function (Blueprint $table) {
            
            $table->string('type_stock_erp_code')->nullable();
        });

        Schema::table('history_auto_allocations', function (Blueprint $table) {
            $table->string('type_stock_old')->nullable();
            $table->string('type_stock_erp_code_old')->nullable();
            $table->string('type_stock_new')->nullable();
            $table->string('type_stock_erp_code_new')->nullable();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
