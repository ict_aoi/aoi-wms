<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPlanningPullCuttingIdInDetailMaterialPreparationFabricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_material_preparation_fabrics', function (Blueprint $table) {
            $table->char('planning_pull_cutting_id',36)->nullable();
            $table->foreign('planning_pull_cutting_id')->references('id')->on('planning_pull_cuttings')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
