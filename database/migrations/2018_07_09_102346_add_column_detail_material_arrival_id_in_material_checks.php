<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDetailMaterialArrivalIdInMaterialChecks extends Migration
{
    public function up()
    {
        Schema::table('material_checks', function (Blueprint $table) {
            $table->char('detail_material_arrival_id',36)->nullable();
            $table->foreign('detail_material_arrival_id')->references('id')->on('detail_material_arrivals')->onUpdate('cascade')->onDelete('cascade');
        });
        
    }

    public function down()
    {
        Schema::table('material_checks', function (Blueprint $table) {
            $table->dropForeign('detail_material_arrivals_detail_material_arrival_id_foreign');
            $table->dropColumn('detail_material_arrival_id'); 
        });
    }
}
