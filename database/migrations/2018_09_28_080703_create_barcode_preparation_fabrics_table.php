<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarcodePreparationFabricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barcode_preparation_fabrics', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('material_stock_id',36)->unsigned();
            $table->date('actual_planning_warehouse_date')->nullable();
            $table->string('barcode')->nullable();
            $table->double('total_qty_preparation',15,8)->nullable();
            $table->char('last_locator_id',36)->unsigned();
            $table->string('warehouse_id')->nullable();
            $table->string('last_status_movement')->nullable();
            $table->string('last_movement_date')->nullable();
            $table->integer('last_user_movement_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            //$table->foreign('detail_material_preparation_fabric_id')->references('id')->on('detail_material_preparation_fabrics')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('last_locator_id')->references('id')->on('locators')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('material_stock_id')->references('id')->on('material_stocks')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('last_user_movement_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barcode_preparation_fabrics');
    }
}
