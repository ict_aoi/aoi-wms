<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailMaterialChecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_material_checks', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('material_check_id',36)->nullable();
            $table->double('start_point_check',15,8)->nullable();
            $table->double('end_point_check',15,8)->nullable();
            $table->string('defect_code')->nullable();
            $table->integer('defect_value')->nullable();
            $table->boolean('is_selected_1')->default(false);
            $table->boolean('is_selected_2')->default(false);
            $table->boolean('is_selected_3')->default(false);
            $table->boolean('is_selected_4')->default(false);
            $table->timestamps();
            $table->integer('user_id')->unsigned();
            $table->datetime('deleted_at')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('material_check_id')->references('id')->on('material_checks')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_material_checks');
    }
}
