<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStoInMaterialStocksTale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('material_stocks', function (Blueprint $table) {
            $table->text('sto_remark')->nullable();
            $table->datetime('sto_date')->nullable();
            $table->boolean('is_sto')->nullable();
            $table->integer('sto_user_id')->nullable();
            $table->foreign('sto_user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
