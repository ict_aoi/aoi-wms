<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaterialStockIdInAllocationItemsTable extends Migration
{
    public function up()
    {
        Schema::table('allocation_items', function (Blueprint $table) {
            $table->char('material_stock_id',36)->nullable()->unsigned();
            $table->foreign('material_stock_id')->references('id')->on('material_stocks')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    public function down()
    {
        Schema::table('allocation_items', function (Blueprint $table) {
            $table->dropForeign('material_stocks_material_stock_id_foreign');
            $table->dropColumn('material_stock_id'); 
        });
    }
}
