<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnBarcodesPreparationInDetailMaterialPreparationFabricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_material_preparation_fabrics', function (Blueprint $table) {
            $table->double('begin_width',15,2)->nullable();
            $table->double('middle_width',15,2)->nullable();
            $table->double('end_width',15,2)->nullable();
            $table->double('actual_width',15,2)->nullable();
            $table->double('actual_length',15,2)->nullable();
            $table->string('upc_item')->nullable();
            $table->double('qty_saving',15,2)->nullable();
            $table->date('actual_preparation_date')->nullable();
            $table->string('last_status_movement')->nullable();
            $table->datetime('last_movement_date')->nullable();
            $table->char('last_locator_id',36)->nullable();
            $table->integer('last_user_movement_id')->nullable();
            $table->string('barcode')->nullable();
            $table->string('referral_code')->nullable();
            $table->biginteger('sequence')->nullable();
            $table->text('cancel_reason')->nullable();
            $table->integer('cancel_user_id')->nullable();

            $table->foreign('last_locator_id')->references('id')->on('locators')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('last_user_movement_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('cancel_user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
