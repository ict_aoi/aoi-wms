<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStatusWarehouseInMaterialChecksTable extends Migration
{
    public function up()
    {
        Schema::table('material_checks', function (Blueprint $table) {
            $table->boolean('status_warehouse')->default(false);
         });
    }

    public function down()
    {
        Schema::table('material_checks', function (Blueprint $table) {
            $table->dropColumn('status_warehouse'); 
        });
    }
}
