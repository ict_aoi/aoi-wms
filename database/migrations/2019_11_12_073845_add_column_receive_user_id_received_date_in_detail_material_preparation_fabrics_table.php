<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnReceiveUserIdReceivedDateInDetailMaterialPreparationFabricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_material_preparation_fabrics', function (Blueprint $table) {
            $table->integer('receive_piping_user_id')->nullable();
            $table->datetime('received_piping_date')->nullable();
            $table->foreign('receive_piping_user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
