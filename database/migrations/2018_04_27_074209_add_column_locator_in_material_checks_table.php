<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnLocatorInMaterialChecksTable extends Migration
{
    public function up()
    {
        Schema::table('material_checks', function (Blueprint $table) {
            $table->char('locator_id',36)->nullable();
            $table->foreign('locator_id')->references('id')->on('locators')->onDelete('cascade')->onUpdate('cascade');
        
        });
    }

   public function down()
    {
        Schema::table('material_checks', function (Blueprint $table) {
            $table->dropForeign('locators_locator_id_foreign');
            $table->dropColumn('locator_id'); 
        });
    }
}
