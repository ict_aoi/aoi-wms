<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnArticleStyleInHistoryMaterialStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('history_material_stocks', function (Blueprint $table) {
            $table->date('lc_date_po_buyer_allocation')->nullable()->after('lc_date');
            $table->string('style_po_buyer_allocation')->nullable()->after('po_buyer_allocation');
            $table->string('article_po_buyer_allocation')->nullable()->after('style_po_buyer_allocation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
