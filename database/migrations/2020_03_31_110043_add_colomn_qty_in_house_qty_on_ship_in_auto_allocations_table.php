<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColomnQtyInHouseQtyOnShipInAutoAllocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auto_allocations', function (Blueprint $table) 
        {
            $table->double('qty_in_house',18,2)->nullable();
            $table->double('qty_on_ship',18,2)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auto_allocations', function (Blueprint $table) {
            $table->dropColumn('qty_in_house');
            $table->dropColumn('qty_on_ship');
        });
    }
}
