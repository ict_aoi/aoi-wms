<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaptureMaterialStocks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capture_material_stocks', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('locator')->nullable();
            $table->string('planning_date')->nullable();
            $table->string('barcode')->nullable();
            $table->string('document_no')->nullable();
            $table->string('po_buyer')->nullable();
            $table->string('item_id')->nullable();
            $table->string('item_code')->nullable();
            $table->string('category')->nullable();
            $table->string('nomor_roll')->nullable();
            $table->string('uom')->nullable();
            $table->double('qty',18,2)->nullable();
            $table->string('status_stock')->nullable();
            $table->string('warehouse')->nullable();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capture_material_stocks');
    }
}
