<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCOrderlineIdInMaterialPreparationsTable extends Migration
{
    public function up()
    {
        Schema::table('material_preparations', function (Blueprint $table) {
            $table->string('c_order_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('material_preparations', function (Blueprint $table) {
            $table->dropColumn('c_order_id'); 
        });
    }
}
