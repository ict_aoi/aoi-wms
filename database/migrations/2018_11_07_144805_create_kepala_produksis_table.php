<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKepalaProduksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kepala_produksis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('perseroan_id');
            $table->string('nama_lengkap',200);
            $table->string('no_ktp',20);
            $table->string('alamat',255);
            $table->string('kewarganegaraan',25);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('perseroan_id')->references('id')->on('perseroans')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kepala_produksis');
    }
}
