<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInDetailMaterialStocksTable extends Migration
{
   public function up()
    {
        Schema::table('detail_material_stocks', function (Blueprint $table) {
            $table->char('material_arrival_id',36)->nullable()->unsigned();
            $table->foreign('material_arrival_id')->references('id')->on('material_arrivals')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('detail_material_stocks', function (Blueprint $table) {
            $table->dropForeign('material_arrivals_material_arrival_id_foreign');
            $table->dropColumn('material_arrival_id'); 
        });
    }
}
