<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWarehouseColumnInMaterialPreperationTables extends Migration
{
    public function up()
    {
        Schema::table('material_preparations', function (Blueprint $table) {
            $table->string('warehouse');
        });
    }

    public function down()
    {
        Schema::table('material_preparations', function (Blueprint $table) {
            $table->dropColumn('warehouse'); 
        });
    }
}
