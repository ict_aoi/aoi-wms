<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsAdditionalInAllocationItemsTable extends Migration
{
    public function up()
    {
        Schema::table('material_preparations', function (Blueprint $table) {
            $table->boolean('is_additional')->nullable()->default(false);
            
        });
    }

    public function down()
    {
        Schema::table('material_preparations', function (Blueprint $table) {
            $table->dropColumn('is_additional'); 
            
        });
    }
}
