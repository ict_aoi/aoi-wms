<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPoDetailInMaterialPreparationsTable extends Migration
{
    public function up()
    {
        Schema::table('material_preparations', function (Blueprint $table) 
        {
            $table->string('po_detail_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
