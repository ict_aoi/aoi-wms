<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSavingRollDateInMaterialSavingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_savings', function (Blueprint $table) {
            $table->datetime('actual_saving_roll_date')->nullable();
            $table->string('item_id')->nullable();
            $table->string('color')->nullable();
            $table->string('uom')->nullable();
            $table->string('c_order_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
