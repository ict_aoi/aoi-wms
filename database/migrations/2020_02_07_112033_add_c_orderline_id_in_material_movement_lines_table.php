<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCOrderlineIdInMaterialMovementLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_movement_lines', function (Blueprint $table) 
        {
            $table->string('c_orderline_id')->nullable();
            $table->string('document_no')->nullable();
            $table->string('nomor_roll')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
