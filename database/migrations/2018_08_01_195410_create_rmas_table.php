<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRmasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rmas', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('material_check_id',36);
            $table->string('item_id')->nullable();
            $table->string('item_code')->nullable();
            $table->string('c_bpartner_id')->nullable();
            $table->string('c_orderline_id')->nullable();
            $table->string('uom')->nullable();
            $table->double('qty_reject',15,8)->nullable();
            $table->boolean('is_integrate')->default(false);
            $table->datetime('integration_date')->nullable();
            
            $table->timestamps();
            $table->foreign('material_check_id')->references('id')->on('material_checks')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rmas');
    }
}
