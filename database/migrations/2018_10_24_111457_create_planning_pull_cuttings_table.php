<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanningPullCuttingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planning_pull_cuttings', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('c_bpartner_id');
            $table->string('document_no');
            $table->string('item_code');
            $table->string('article_no');
            $table->string('style');
            $table->date('planning_date');
            $table->string('actual_width');
            $table->timestamps();
            $table->integer('user_id')->unsigned();
            $table->datetime('deleted_at')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planning_pull_cuttings');
    }
}
