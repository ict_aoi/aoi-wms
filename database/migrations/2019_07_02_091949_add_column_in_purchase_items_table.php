<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInPurchaseItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_items', function (Blueprint $table) {
            $table->integer('counter_out')->default(0); 
            $table->integer('counter_handover')->default(0); 
            $table->boolean('is_stock_already_created')->default(false); 
        });

        Schema::table('material_preparations', function (Blueprint $table) {
            $table->boolean('is_stock_already_created')->default(false);
        });

        Schema::table('items', function (Blueprint $table) {
            $table->boolean('required_lot')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
