<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnNoInvoiceNoPackingListInMaterialMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_preparations', function (Blueprint $table) 
        {
            $table->timestamp('adjustment_date')->nullable();
        });

        Schema::table('material_movements', function (Blueprint $table) 
        {
            $table->string('no_packing_list')->nullable();
            $table->string('no_invoice')->nullable();
        });

        Schema::table('material_movement_lines', function (Blueprint $table) {
            $table->string('c_order_id')->nullable();
            $table->string('c_bpartner_id')->nullable();
            $table->string('warehouse_id')->nullable();
            $table->string('uom_movement')->nullable()->before('qty_movement');
            $table->char('material_stock_id')->nullable();
            $table->foreign('material_stock_id')->references('id')->on('material_stocks')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
