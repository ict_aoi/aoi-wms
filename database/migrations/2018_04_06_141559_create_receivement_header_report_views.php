<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceivementHeaderReportViews extends Migration
{
    public function up(){
        // DB::statement("
        //     CREATE OR REPLACE VIEW public.receivement_header_report AS
        //         SELECT material_arrivals.id,
        //             material_arrivals.document_no,
        //             material_arrivals.category,
        //             material_arrivals.no_packing_list,
        //             material_arrivals.no_invoice,
        //             material_arrivals.supplier_name,
        //             btrim(upper(material_arrivals.item_code::text)) AS item_code,
        //             btrim(upper(material_arrivals.item_desc::text)) AS item_desc,
        //             case
        //                     when conversion.uom_to is null then material_arrivals.uom
        //                     else conversion.uom_to
        //                 end as uom,
        //             material_arrivals.warehouse_id,
        //                 CASE
        //                     WHEN material_arrivals.type_po::text = 2::text THEN 'ACC'::text
        //                     ELSE 'FEBRIC'::text
        //                 END AS type_po,
        //             sum(material_arrivals.qty_carton)::integer AS qty_carton,
        //             carton_in.total AS total_carton_in,
        //             sum(material_arrivals.qty_ordered)::integer as qty_ordered,
        //             coalesce(conversion.dividerate,1)*sum(material_arrivals.qty_upload) AS qty_upload,
        //                 CASE
        //                     WHEN material_arrivals.type_po::text = 2::text THEN 0::double precision
        //                     ELSE roll.total
        //                 END AS total_roll,
        //             material_arrivals.prepared_status
        //         FROM material_arrivals
        //             LEFT JOIN ( SELECT detail_material_arrivals.material_arrival_id,
        //                     count(0) AS total
        //                 FROM detail_material_arrivals
        //                 GROUP BY detail_material_arrivals.material_arrival_id) carton_in ON carton_in.material_arrival_id = material_arrivals.id
        //             LEFT JOIN ( SELECT detail_material_arrivals.material_arrival_id,
        //                     sum(detail_material_arrivals.qty_upload) AS total
        //                 FROM detail_material_arrivals
        //                 GROUP BY detail_material_arrivals.material_arrival_id) roll ON roll.material_arrival_id = material_arrivals.id
        //             LEFT join (
        //                 select uom_conversions.item_code,uom_conversions.category,uom_conversions.uom_from,uom_conversions.uom_to,uom_conversions.dividerate
        //                 from uom_conversions
        //                 group by uom_conversions.item_code,uom_conversions.category,uom_conversions.uom_from,uom_conversions.uom_to,uom_conversions.dividerate
        //             )conversion on conversion.item_code = material_arrivals.item_code and conversion.category = material_arrivals.category and conversion.uom_from = material_arrivals.uom
        //         GROUP BY material_arrivals.id, conversion.uom_to,conversion.dividerate,material_arrivals.qty_ordered,material_arrivals.document_no, material_arrivals.category, material_arrivals.no_packing_list, material_arrivals.no_invoice, material_arrivals.supplier_name, material_arrivals.item_code, material_arrivals.item_desc, material_arrivals.type_po, material_arrivals.qty_carton, carton_in.total, roll.total, material_arrivals.warehouse_id, material_arrivals.prepared_status;
        // ");
    }

    public function down()
    {
        DB::statement('DROP VIEW receivement_header_report CASCADE');
    }
}
