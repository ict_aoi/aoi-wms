<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnHasManyPobuyerColumnInLocatorsTable extends Migration
{
    public function up()
    {
        Schema::table('locators', function (Blueprint $table) {
            $table->boolean('has_many_po_buyer')->default(false);
        });
    }

    public function down()
    {
        Schema::table('locators', function (Blueprint $table) {
           $table->dropColumn('has_many_po_buyer');
        });
    }
}
