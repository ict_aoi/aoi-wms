<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSummaryStockFabricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('summary_stock_fabrics', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('c_bpartner_id')->nullable();
            //$table->string('c_order_id')->nullable();
            //$table->string('no_invoice')->nullable();
            $table->string('document_no')->nullable();
            $table->string('supplier_name')->nullable();
            $table->string('item_id')->nullable();
            $table->string('item_code')->nullable();
            $table->string('color')->nullable();
            $table->string('category_stock')->nullable();
            $table->string('uom')->nullable();
            $table->double('stock',15,8)->nullable();
            $table->double('reserved_qty',15,8)->nullable();
            $table->double('available_qty',15,8)->nullable();
            $table->boolean('is_from_receiving')->default(false);
            $table->boolean('is_from_input_stock')->default(false);
            $table->boolean('is_allocated')->default(false);
            //$table->integer('total_allocated_po_buyer')->default(0);
            $table->integer('user_id')->unsigned();
            $table->string('warehouse_id');
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('summary_stock_fabrics');
    }
}
