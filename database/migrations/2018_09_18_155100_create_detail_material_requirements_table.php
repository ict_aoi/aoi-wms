<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailMaterialRequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_material_requirements', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('po_buyer')->nullable();
            $table->string('item_code')->nullable();
            $table->string('item_desc')->nullable();
            $table->string('job_order')->nullable();
            $table->string('style')->nullable();
            $table->string('uom')->nullable();
            $table->string('part_no')->nullable();
            $table->string('garment_size')->nullable();
            $table->string('category');
            $table->string('article_no')->nullable();
            $table->boolean('is_piping')->nullable();
            $table->date('lc_date')->nullable();
            $table->date('statistical_date')->nullable();
            $table->string('season')->nullable();
            $table->double('qty_required',15,8)->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_material_requirements');
    }
}
