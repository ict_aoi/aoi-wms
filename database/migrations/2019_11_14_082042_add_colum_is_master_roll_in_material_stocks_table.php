<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumIsMasterRollInMaterialStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_stocks', function (Blueprint $table) 
        {
            $table->string('inspect_lot_result')->nullable();
            $table->boolean('is_reject')->default(false);
            $table->boolean('is_master_roll')->default(false);
            $table->timestamp('insert_to_locator_reject_erp_date_from_rma')->nullable()->after('is_reject');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
