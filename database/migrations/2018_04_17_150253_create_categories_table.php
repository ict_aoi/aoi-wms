<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('name')->nullable();
            $table->string('uom')->nullable();
            $table->string('code')->nullable();
            $table->boolean('is_prepared')->nullable();
            $table->timestamps();
            $table->integer('user_id')->nullable()->unsigned();
            $table->datetime('deleted_at')->nullable();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
