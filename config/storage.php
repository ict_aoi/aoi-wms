<?php

return [
	'laporan_receiving' => storage_path() . '/report/laporan_receiving',
	'laporan_perpindahan_barang' => storage_path() . '/app/laporan_perpindahan_barang',
	'laporan_stock' => storage_path() . '/app/laporan_stock',
	'knowledge_base' => storage_path() . '/app/knowledge_base',
	'mrp' => storage_path() . '/app/MRP',
	'report' => storage_path() . '/app/report',
	'auto_allocation' => storage_path() . '/app/update_format/auto_allocation',
	'avatar' => storage_path() . '/app/avatar',
	'email_attachment_qc' => storage_path() . '/app/email_attachment/outstanding_confirmation_qc'
];