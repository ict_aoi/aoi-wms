<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
        ],

        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

       'pgsql' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'absence_aoi' => [
            'driver' => 'pgsql',
            'host' => '192.168.15.56',
            'port' => '5432',
            'database' => 'absensi_aoi',
            'username' => 'absensi',
            'password' => 'Absensi12345',
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            'schema' =>'public',
            'sslmode' => 'prefer',
        ],
        
        'erp' => [
            'driver'    => 'pgsql',
            'host'      => env('ERP_HOST_REMOTE', '192.168.51.123'),
            'port'      => env('ERP_PORT_REMOTE', '5432'),
            'database'  => 'aimsdbc',
            'username'  => 'adempiere',
            'password'  => 'Becarefulwithme',
            'charset'   => 'utf8',
            'prefix'    => '',
            'schema'    => 'adempiere',
            'sslmode'   => 'prefer',
        ],

        'cdms' => [
            'driver'    => 'pgsql',
            'host'      => env('ERP_HOST_REMOTE', '192.168.51.53'),
            'port'      => env('ERP_PORT_REMOTE', '5432'),
            'database'  => 'cdms',
            'username'  => 'postgres',
            'password'  => 'Pass@123',
            'charset'   => 'utf8',
            'prefix'    => '',
            'schema'    => 'public',
            'sslmode'   => 'prefer',
        ],

        'dev_erp' => [
            'driver'    => 'pgsql',
            'host'      => '192.168.51.201',
            'port'      => '5432',
            'database'  => 'aimsdev',
            'username'  => 'adempiere',
            'password'  => 'Becarefulwithme',
            'charset'   => 'utf8',
            'prefix'    => '',
            'schema'    => 'adempiere',
            'sslmode'   => 'prefer',
        ],

        'web_po' => [
            'driver' => 'pgsql',
            'host' => env('WEBPO_HOST_REMOTE', '192.168.51.52'),
            'port' => env('WEBPO_PORT_REMOTE', '5432'),
            'database' => 'import',
            'username' => 'webpo',
            'password' => 'Becarefulwithme',
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'dev_web_po' => [
            'driver' => 'pgsql',
            'host' => '192.168.51.152',
            'port' => '5432',
            'database' => 'import',
            'username' => 'postgres',
            'password' => '',
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],  

        'wms_old' => [
            'driver' => 'pgsql',
            'host' => '192.168.51.202',
            'port' => '5432',
            'database' => 'wms_acc',
            'username' => 'wmsacc',
            'password' => 'Password12345',
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],  

        'dashboard_wms' => [
            'driver' => 'pgsql',
            'host' => '192.168.51.16',
            'port' => '5432',
            'database' => 'dashboard_wms',
            'username' => 'dev',
            'password' => 'Pass@123',
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],  

        'lab' => [
            'driver' => 'pgsql',
            'host' => '192.168.51.15',
            'port' => '5432',
            'database' => 'labdev',
            'username' => 'dev',
            'password' => 'Pass@123',
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ], 
        
        'webpo_new' => [
            'driver' => 'pgsql',
            'host' => '192.168.51.43',
            'port' => '5432',
            'database' => 'wpo',
            'username' => 'postgres',
            'password' => 'Becarefulwithme',
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],  
        'replication_wms_live' => [
            'driver'            => 'pgsql',
            'host'              => '192.168.51.203',
            'port'              => '5432',
            'database'          => 'WMS',
            'username'          => 'postgres',
            'password'          => 'Becarefulwithme',
            'charset'           => 'utf8',
            'prefix'            => '',
            'prefix_indexes'    => true,
            'schema'            => 'public',
            'sslmode'           => 'prefer',
        ],        
        'wms_live' => [
            'driver'            => 'pgsql',
            'host'              => '192.168.51.46',
            'port'              => '5432',
            'database'          => 'WMS',
            'username'          => 'postgres',
            'password'          => 'Becarefulwithme',
            'charset'           => 'utf8',
            'prefix'            => '',
            'prefix_indexes'    => true,
            'schema'            => 'public',
            'sslmode'           => 'prefer',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],

];
