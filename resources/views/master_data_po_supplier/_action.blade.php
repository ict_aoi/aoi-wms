
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($item))
                <li><a href="#" onclick="item('{!! $item !!}')"><i class="icon-stack3"></i> Items</a></li>
            @endif

        </ul>
    </li>
</ul>
