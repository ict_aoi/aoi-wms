
<div id="detailModal" data-backdrop="static" data-keyboard="false"  class="modal fade" style="color:black !important">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<h5 class="modal-title">Uom Conversions</h5>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table datatable-basic table-striped table-hover table-responsive" id="master_data_uom_conversion_table">
						<thead>
							<tr>
								<th>Item Code</th>
								<th>Item Desc</th>
								<th>Uom From</th>
								<th>Uom To</th>
								<th>Devider</th>
								<th>Multiply</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close  <span class="glyphicon glyphicon-remove"></span></button>
			</div>
		</div>
	</div>
</div>
