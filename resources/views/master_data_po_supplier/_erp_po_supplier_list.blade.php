<table class="table datatable-basic table-striped table-hover">
	<thead>
	  <tr>
		<th>Supplier Code</th>
		<th>Supplier Name</th>
		<th>Po Supplier</th>
		<th>Periode Po</th>
		<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>{{ $list->supplier_code }}</td>
				<td>{{ $list->supplier_name }}</td>
				<td>{{ strtoupper($list->documentno) }}</td>
				<td>{{ $list->period_po }}</td>
				<td>
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose"
						type="button" data-orderid="{{ $list->c_order_id }}" 
						data-bpartnerid="{{ $list->c_bpartner_id }}" 
					>Select</button>
				</td>
			</tr>

		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
