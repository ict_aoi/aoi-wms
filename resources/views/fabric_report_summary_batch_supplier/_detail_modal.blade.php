
<div id="detailModal" data-backdrop="static" data-keyboard="false"  class="modal fade" style="color:black !important">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table datatable-basic table-striped table-hover table-responsive" id="fabric_report_summary_batch_supplier_detail_table">
						<thead>
							<tr>
								<th width="50%">Po Supplier</th>
								<th width="50%">Batch Number</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close  <span class="glyphicon glyphicon-remove"></span></button>
			</div>
		</div>
	</div>
</div>
