@extends('layouts.app', ['active' => 'accessories_material_metal_detector'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Metal Detector</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Material Metal Detector</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
		@role(['admin-ict-acc'])
			<div class="panel panel-default border-grey">
				<div class="panel-heading">
					<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
					<div class="heading-elements">
						<ul class="icons-list">
							<li><a data-action="collapse"></a></li>
						</ul>
					</div>
				</div>
				<div class="panel-body">
					@include('form.select', [
						'field' 			=> 'warehouse',
						'label' 			=> 'Warehouse',
						'default'			=> auth::user()->warehouse,
						'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
						'options' 			=> [
							'' 				=> '-- Select Warehouse --',
							'1000002' 		=> 'Warehouse Accessories AOI 1',
							'1000013' 		=> 'Warehouse Accessories AOI 2',
						],
						'class' 			=> 'select-search',
						'attributes' 		=> [
							'id' 			=> 'select_warehouse'
						]
					])
				</div>
			</div>
		@else 
			{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
		@endrole

		<div class="panel panel-default border-grey">
			<div class="panel-body">
				<div class="row">
					@include('form.select', [
						'field' 		=> 'status',
						'label' 		=> 'Status',
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'options' 		=> [
							'QUARANTINE' 	=> 'Quarantine',
							'HOLD' 			=> 'Hold',
							'RELEASE' 		=> 'Release',
							'REJECT' 		=> 'Reject',
							'RANDOM' 		=> 'Random',
						],
						'class' 		=> 'select-search',
						'attributes' 	=> [
							'id' => 'status_all'
						]
					])
				</div>

				<div class="row">
					<div class="form-group text-right">
						<button type="button" class="btn btn-default col-xs-12" id="selectAll">Select All <i class="icon-arrow-right14 position-left"></i></button>
					</div>
				</div>
				
			</div>
		</div>
		
		<div class="panel panel-default border-grey">
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-hover table-responsive">
						<thead>
							<tr>
								<th>#</th>
								<th>Barcode</th>
								<th>Po Supplier</th>
								<th>Po Buyer</th>
								<th>Item</th>
								<th>Uom</th>
								<th>Qty Rcv.</th>
								<th>Qty Reject</th>
								<th>Remark</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="tbody_accessories_material_metal_detector">
						</tbody>
					</table>
				</div>
	{!!
		Form::open([
			'role' 		=> 'form',
			'url' 		=> route('accessoriesMaterialMetalDetector.store'),
			'method' 	=> 'post',
			'enctype'	=> 'multipart/form-data',
			'id'		=> 'form'
		])
	!!}
				{!! Form::hidden('_selectAll',0, array('id' => '_selectAll')) !!}
				{!! Form::hidden('barcodes','[]' , array('id' => 'barcode-header')) !!}
				{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_id')) !!}
				{!! Form::hidden('url_accessories_material_metal_detector', route('accessoriesMaterialMetalDetector.create'), ['id' => 'url_accessories_material_metal_detector']) !!}
				{!! Form::hidden('total_receive','0' , array('id' => 'total_receive')) !!}
				{!! Form::hidden('total_scan','0' , array('id' => 'total_scan')) !!}
					<div class="form-group text-right" style="margin-top:10px">
						<button type="submit" class="btn btn-blue-success col-xs-12 btn-lg">Save <i class="icon-floppy-disk position-left"></i></button>
					</div>
			</div>
		</div>
	{!! Form::close() !!}
	<div class="alert alert-info alert-styled-right alert-bordered" id="alert_checkout">
		<span class="text-semibold">Info for you !</span> <span id="msg"></span>
	</div>
@endsection

@section('page-js')
	@include('accessories_material_metal_detector._item')
	<script src="{{ mix('js/accessories_material_metal_detector.js') }}"></script>
@endsection
