<script type="x-tmpl-mustache" id="auto-allocation-table">
	{% #item %}
		<tr {% #error_upload %} style="background-color:#fab1b1" {% /error_upload %}>
			<td>
				{% no %}
			</td>
			<td>
				{% booking_number %}
			</td>
			<td>
				<!--<p><b>BASED ON STOCK</b> {% type_stock %}</p>-->
				{% type_stock %}
			</td>
			<td>
				<p>{% supplier_name %}</p>
				<p>{% document_no %}</p>
			</td>
			<td>
				<p>{% item_code_source %}</p>
			</td>
			<td>
				<p>{% item_code %}</p>
				<p>{% category %}</p>
				<p>{% uom %}</p>
			</td>
			<td>
				{% po_buyer %}
			</td>
			<td>
				{% qty_allocation %}
			</td>
			<td>
				{% warehouse_name %}
			</td>
			<td>
				{% remark_additional %}
			</td>
			<td>
				{% reason %}
			</td>
			<td>
				{% remark %}
			</td>
			<td>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-icon-anim btn-circle btn-delete-item"><i class="fa icon-trash"></i></button>
			</td>
		</tr>
	{%/item%}

	<tr>
		<td>#</td>
		<td>
			<input type="text" id="booking_number" name="booking_number" class="form-control input-new" placeholder="NOMOR BOOKING"></input>
		</td>
		<td></td>
		<td>
			@include('form.picklist', [
				'field' => 'document_no',
				'name' => 'document_no',
				'placeholder' => 'Silahkan Pilih PO'
			])
			<input type="hidden" id="type_stock" name="type_stock"></input>
			<input type="hidden" id="type_stock_erp" name="type_stock_erp"></input>
			<input type="hidden" id="supplier_name" name="supplier_name"></input>
			<input type="hidden" id="c_bpartner_id" name="c_bpartner_id"></input>
		</td>
		<td>
			@include('form.picklist', [
				'field' => 'item_source',
				'name' => 'item_code_source',
				'placeholder' => 'Silahkan Pilih Item Source'
			])
			<input type="hidden" id="item_source_id" name="item_id" readonly="readonly"></input>
			<input type="hidden" id="item_source_desc" name="item_desc" readonly="readonly"></input>
			<input type="hidden" id="category_source" name="category" readonly="readonly"></input>
			<input type="hidden" id="uom_source" name="uom" readonly="readonly"></input>
		
		</td>
		<td>
			@include('form.picklist', [
				'field' => 'item_code',
				'name' => 'item',
				'placeholder' => 'Silahkan Pilih Item'
			])
			<input type="hidden" id="item_id" name="item_id" readonly="readonly"></input>
			<input type="hidden" id="item_desc" name="item_desc" readonly="readonly"></input>
			<input type="hidden" id="category" name="category" readonly="readonly"></input>
			<input type="hidden" id="uom" name="uom" readonly="readonly"></input>
		
		</td>
		<td>
			@include('form.picklist', [
				'field' => 'po_buyer',
				'name' => 'po_buyer',
				'placeholder' => 'Silahkan Pilih Buyer'
			])
			<input type="hidden" id="promise" name="promise"></input>
			<input type="hidden" id="lc_date" name="lc_date"></input>
			<input type="hidden" id="season" name="season"></input>
			<input type="hidden" id="type_stock_po_buyer" name="type_stock"></input>
			<input type="hidden" id="type_stock_erp_po_buyer" name="type_stock_erp"></input>
		</td>
		<td>
			<input type="text" id="qty_allocation" name="qty_allocation" class="form-control input-number" placeholder="INPUT QTY ALLOCATION"></input>
		</td>
		<td>
			<select id="warehouse" name="source" class="form-control">
				<option value="1000002">Accessories AOI-1</option>
				<option value="1000013">Accessories AOI-2</option>
				<option value="1000011">Fabric AOI-2</option>
				<option value="1000001">Fabric AOI-1</option>
			</select>
		</td>
		<td/> &nbsp </td>
		<td/> &nbsp </td>
		<td/> &nbsp </td>
		<td>
			<button type="button" class="btn btn-yellow-cancel btn-lg" id="AddItemButton" ><i class="icon-add"></i></button>
		</td>

	</tr>
</script>
