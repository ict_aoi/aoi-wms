@extends('layouts.app', ['active' => 'master_data_auto_allocation'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Data Auto Allocation</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data</li>
				<li class="active">Auto Allocation</li>
			</ul>
			<ul class="breadcrumb-elements">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
						<i class="icon-three-bars position-left"></i>
						Actions
						<span class="caret"></span>
					</a>
					
					<ul class="dropdown-menu dropdown-menu-right">
						@role(['admin-ict-acc','admin-ict-fabric','mm-staff-acc','mm-staff'])
							<li><a href="{{ route('masterDataAutoAllocation.create') }}"><i class="icon-pencil6 pull-right"></i> Create</a></li>
							<li><a href="{{ route('masterDataAutoAllocation.exportToExcelPR') }}"><i class="icon-file-download2 pull-right"></i> Export Allocation PR</a></li>
							<li><a href="{{ route('masterDataAutoAllocation.etaDelay') }}"><i class="icon-pencil6 pull-right"></i> Upload ETA Delay & Closing</a></li>
							<li><a href="{{ route('masterDataAutoAllocation.removeFromDashboard') }}"><i class="icon-pencil6 pull-right"></i> Upload Remove From Dashboard</a></li>
							<li><a href="{{ route('masterDataAutoAllocation.deleteFromBulk') }}"><i class="icon-trash pull-right"></i> Delete</a></li>
						@endrole
						@role(['admin-ict-acc','admin-ict-fabric','mm-staff'])
							<li><a href="{{ route('masterDataAutoAllocation.recalculateDashboard') }}"><i class="icon-pencil6 pull-right"></i> Recalculate Dashboard</a></li>
						@endrole
						<li><a href="{{ route('masterDataAutoAllocation.downloadFormBooking') }}"><i class="icon-file-download2 pull-right"></i>Form Booking</a></li>
						<li><a href="{{ route('masterDataAutoAllocation.exportToExcel') }}"><i class="icon-file-download2 pull-right"></i> Export All</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
@endsection


@section('page-content')
	<div class="navbar navbar-default navbar-xs navbar-component no-border-radius-top">
		<ul class="nav navbar-nav visible-xs-block">
			<li class="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
		</ul>

		<div class="navbar-collapse collapse" id="navbar-filter">
			<ul class="nav navbar-nav">
				<li class="active"><a href="#active" data-toggle="tab" onclick="changeTab('active')">Active <i class="icon-stack3 position-left"></i></a></li>
				<li><a href="#additional" data-toggle="tab" onclick="changeTab('additional')">Additional <i class="icon-stack3 position-left"></i></a></li>
				<li><a href="#inactive" data-toggle="tab" onclick="changeTab('inactive')">Inactive <i class=" icon-stack2 position-left"></i></a></li>
			</ul>
		</div>
	</div>

	<div class="tabbable">
		<div class="tab-content">
			<div class="tab-pane fade in active" id="active">
				<div class="panel panel-default border-grey">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table datatable-basic table-striped table-hover table-responsive" id="activeTable">
								<thead>
									<tr>
										<th>ID</th>
										<th>Lc Date</th>
										<th>Date</th>
										<th>Created By</th>
										<th>Updated By</th>
										<th>Status Po Buyer</th>
										<th>Booking Number</th>
										<th>Additonal</th>
										<th>Source Supplier</th>
										<th>Allocation To</th>
										<th>Item</th>
										<th>Quantity</th>
										<th>Warehouse Inventory</th>
										<th>Remark</th>
										<th>Action</th>
										<th>Season</th>
										<th>Po Buyer</th>
										<th>Old Po Buyer</th>
										<th>Document No</th>
										<th>Created At</th>
										<th>Lc Date</th>
										<th>Promise Date</th>
										<th>Item Code</th>
										<th>Category</th>
										<th>Item Code Source</th>
										<th>Qty Outstanding</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>

			</div>

			<div class="tab-pane fade in" id="additional">
				<div class="panel panel-default border-grey">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table datatable-basic table-striped table-hover table-responsive" id="additionalTable">
								<thead>
									<tr>
										<th>ID</th>
										<th>Lc Date</th>
										<th>Date</th>
										<th>Created By</th>
										<th>Updated By</th>
										<th>Status Po Buyer</th>
										<th>Booking Number</th>
										<th>Additonal</th>
										<th>Source Supplier</th>
										<th>Allocation To</th>
										<th>Item</th>
										<th>Quantity</th>
										<th>Warehouse Inventory</th>
										<th>Remark</th>
										<th>Action</th>
										<th>Season</th>
										<th>Po Buyer</th>
										<th>Old Po Buyer</th>
										<th>Document No</th>
										<th>Created At</th>
										<th>Lc Date</th>
										<th>Promise Date</th>
										<th>Item Code</th>
										<th>Category</th>
										<th>Item Code Source</th>
										<th>Qty Outstanding</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>

			</div>

			<div class="tab-pane fade" id="inactive">
				<div class="panel panel-default border-grey">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table datatable-basic table-striped table-hover table-responsive" id="inactiveTable">
								<thead>
									<tr>
									<th>ID</th>
										<th>Lc Date</th>
										<th>Date</th>
										<th>Created By</th>
										<th>Updated By</th>
										<th>Status Po Buyer</th>
										<th>Booking Number</th>
										<th>Additonal</th>
										<th>Source Supplier</th>
										<th>Allocation To</th>
										<th>Item</th>
										<th>Quantity</th>
										<th>Warehouse Inventory</th>
										<th>Remark</th>
										<th>Action</th>
										<th>Season</th>
										<th>Po Buyer</th>
										<th>Old Po Buyer</th>
										<th>Document No</th>
										<th>Created At</th>
										<th>Lc Date</th>
										<th>Promise Date</th>
										<th>Item Code</th>
										<th>Category</th>
										<th>Item Code Source</th>
										<th>Qty Outstanding</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


{!! Form::hidden('page','index' , array('id' => 'page')) !!}
{!! Form::hidden('active_tab',$active_tab , array('id' => 'active_tab')) !!}
{!! Form::hidden('flag_msg',$flag , array('id' => 'flag_msg')) !!}
{!! Form::hidden('url_master_data_auto_allocation_update',route('masterDataAutoAllocation.update') , array('id' => 'url_master_data_auto_allocation_update')) !!}

@endsection

@section('page-modal')
	@include('master_data_auto_allocation._history_modal')
	@include('master_data_auto_allocation._modal_form_booking')
	@include('master_data_auto_allocation._update_modal')
	@include('form.modal_picklist', [
		'name' 			=> 'document_no',
		'title' 		=> 'List Po Supplier',
		'placeholder' 	=> 'Search based on po supplier',
	])

	@include('form.modal_picklist', [
		'name' 			=> 'item_code_source',
		'title' 		=> 'List Item',
		'placeholder' 	=> 'Search based on item code',
	])
@endsection

@section('page-js')
	<script type="text/javascript" src="{{ asset(elixir('js/master_data_auto_allocation.js'))}}"></script>
@endsection
