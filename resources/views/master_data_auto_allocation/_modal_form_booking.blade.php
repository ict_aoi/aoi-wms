<div class="modal fade" id="downloadFormBookingModal" tabindex="-1" role="dialog" aria-labelledby="downloadFormBookingModal">
        <div class="modal-dialog" role="document">
		{{ Form::open(['method' => 'get', 'target' => '_blank', 'class' => 'form-horizontal', 'url' => route('masterDataAutoAllocation.downloadFormBooking')]) }}
		    <div class="modal-content">
                <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">DOWNLOAD FORM BOOKING</h5>
				</div>
				<div class="modal-body">
				   <div class="row">  
					  @include('form.date', [
						'field' => 'lc_date',
						'label' => 'LC DATE',
						'class' => ' datepicker',
						'placeholder' => 'dd/mm/yyyy',
						'label_col' => 'col-md-4',
						'form_col' => 'col-sm-8',
						'attributes' => [
							'id' => 'start_date',
							'autocomplete' => 'off'
						]
					])
					</div>
				</div>
                <div class="modal-footer text-center">
					<button type="submit" class="btn btn-info">Download</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>