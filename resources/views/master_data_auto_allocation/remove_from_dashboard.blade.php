@extends('layouts.app', ['active' => 'master_data_auto_allocation'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Data Auto Allocation</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data</li>
				<li><a href="{{ route('masterDataAutoAllocation.index') }}">Auto Allocation</a></li>
				<li class="active">Remove from Dashboard</li>
			</ul>
		</div>
	</div>
@endsection


@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a class="btn btn-primary btn-icon" href="{{ route('masterDataAutoAllocation.downloadRemoveFromDashboard')}}" data-popup="tooltip" title="download form eta delay" data-placement="bottom" data-original-title="download form eta delay"><i class="icon-download"></i></a>
				<button type="button" class="btn btn-success btn-icon" id="upload_button" data-popup="tooltip" title="upload form eta delay" data-placement="bottom" data-original-title="upload form eta delay"><i class="icon-upload"></i></button>

				{!!
					Form::open([
						'role' => 'form',
						'url' => route('masterDataAutoAllocation.uploadRemoveFromDashboard'),
						'method' => 'POST',
						'id' => 'upload_file_allocation',
						'enctype' => 'multipart/form-data'
					])
				!!}
					<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
					<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
				{!! Form::close() !!}

			</div>
		</div>

		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover table-responsive">
					<thead>
						<tr>
							<th>No</th>
							<th>Po Supplier</th>
							<th>Item Code</th>
							<th>Po Buyer</th>
							<th>Qty</th>
							<th>Is Remove</th>
							<th>Is Closed</th>
							<th>Warehouse Inventory</th>
							<th>Remark Remove</th>
							<th>Upload Result</th>
						</tr>
					</thead>
					<tbody id="tbody-auto-allocation">
					</tbody>
				</table>
			</div>
		</div>
	</div>

{!! Form::hidden('url_master_data_auto_allocation',route('masterDataAutoAllocation.index') , array('id' => 'url_master_data_auto_allocation')) !!}
{!! Form::hidden('flag',null , array('id' => 'flag')) !!}
{!! Form::hidden('new_auto_allocations','[]' , array('id' => 'new_auto_allocations')) !!}
@endsection

@section('page-js')
	@include('master_data_auto_allocation._item_remove_from_dashboard')
	<script type="text/javascript" src="{{ asset(elixir('js/master_data_auto_allocation_remove_from_dashboard.js'))}}"></script>
@endsection
