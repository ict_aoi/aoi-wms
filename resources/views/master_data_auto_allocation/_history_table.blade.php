<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
			<th>No</th>
			<th>Created At</th>
			<th>Updated By</th>

			<th>Old Value</th>
			<th>New Value</th>

			<th>Remark</th>
			
	</tr>
	</thead>
	
	<tbody>
		@foreach ($history_auto_allocations as $key => $history_auto_allocation)
			<tr>
				<td>{{ $key+1 }}</td>
				<td>{{ $history_auto_allocation->created_at->format('d/M/y h:i:s') }}</td>
				<td>{{ $history_auto_allocation->user->name }}</td>
				<td>
					@if($history_auto_allocation->document_no_old && $history_auto_allocation->supplier_name_old && $history_auto_allocation->item_code_old && $history_auto_allocation->po_buyer_old && $history_auto_allocation->uom_old && $history_auto_allocation->qty_allocated_old)
					<p><b>Po Supplier </b> <br/> {{ $history_auto_allocation->document_no_old }}</p>
					<p><b>Supplier Name </b>  <br/> {{ $history_auto_allocation->supplier_name_old }}</p>
					<p><b>Item Code Source </b>  <br/> {{ $history_auto_allocation->item_code_source_old }}</p>
					<p><b>Item Code </b>  <br/> {{ $history_auto_allocation->item_code_old }}</p>
					<p><b>Po Buyer </b>  <br/> {{ $history_auto_allocation->po_buyer_old }}</p>
					<p><b>Uom </b>  <br/> {{ $history_auto_allocation->uom_old }}</p>
					<p><b>Qty Allocation </b>  <br/> {{ $history_auto_allocation->qty_allocated_old }}</p>
					<p><b>Warehouse  <br/> </b> 
						@if($history_auto_allocation->warehouse_id_old == '1000002')
							ACC - AOI 1
						@elseif($history_auto_allocation->warehouse_id_old == '1000001')
							FAB - AOI 1
						@elseif($history_auto_allocation->warehouse_id_old == '1000011')
							FAB - AOI 2
						@elseif($history_auto_allocation->warehouse_id_old == '1000013')
							ACC - AOI 2
						@else 
							
						@endif
					</p>
					@endif
				</td>
				<td>
					<p><b>Po Supplier </b>  <br/> {{ $history_auto_allocation->document_no_new }}</p>
					<p><b>Supplier Name </b>  <br/> {{ $history_auto_allocation->supplier_name_new }}</p>
					<p><b>Item Code Source </b>  <br/> {{ $history_auto_allocation->item_code_source_new }}</p>
					<p><b>Item Code </b>  <br/> {{ $history_auto_allocation->item_code_new }}</p>
					<p><b>Po Buyer </b>  <br/> {{ $history_auto_allocation->po_buyer_new }}</p>
					<p><b>Uom </b>  <br/> {{ $history_auto_allocation->uom_new }}</p>
					<p><b>Qty Allocation </b>  <br/> {{ $history_auto_allocation->qty_allocated_new }}</p>
					<p><b>Warehouse  <br/> </b> 
						@if($history_auto_allocation->warehouse_id_new == '1000002')
							ACC - AOI 1
						@elseif($history_auto_allocation->warehouse_id_new == '1000001')
							FAB - AOI 1
						@elseif($history_auto_allocation->warehouse_id_new == '1000011')
							FAB - AOI 2
						@elseif($history_auto_allocation->warehouse_id_new == '1000013')
							ACC - AOI 2
						@else 
							
						@endif
					</p>
				</td>

				<td>{{ $history_auto_allocation->remark }}</td>
			</tr>
		@endforeach
	</tbody>
</table>
