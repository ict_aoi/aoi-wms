<table class="table">
	<thead>
	  <tr>
		<th>Item Code</th>
		<th>Category</th>
		<th>Uom</th>
		<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ $list->item_code }}
				</td>
				<td>
					{{ $list->category }}
				</td>
				
				<td>
					{{ $list->uom }}
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose"
						type="button" data-id="{{ $list->item_id }}" data-code="{{ $list->item_code }}" data-name="{{ $list->item_desc }}" data-uom="{{ $list->uom }}"
						data-category="{{ $list->category }}">Select</button>
				</td>
			</tr>

		@endforeach
	</tbody>
</table>
{!! $lists->appends(Request::except('page'))->render() !!}
