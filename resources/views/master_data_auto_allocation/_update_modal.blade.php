<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModal">
        <div class="modal-dialog modal-lg" role="document">
		{{ 
			Form::open([
				'method' => 'POST'
				,'id' => 'updateformnya'
				,'class' => 'form-horizontal'
				, 'url' => '#'
			]) 
		}}
		    <div class="modal-content">
                <div class="modal-header bg-blue">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">Edit Auto Allocation</h5>
				</div>
				<div class="modal-body">
					<div class="row">  
						<div class="col-md-6">
							<div class="col-md-12">
								@include('form.select', [
									'label_col'	=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'	=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 	=> 'warehouse_id',
									'label' 	=> 'Warehouse',
									'options' 	=> [
										'1000002'	=>'Accessories aoi 1',
										'1000013'	=>'Accessories aoi 2',
										'1000001'	=>'Fabric aoi 1',
										'1000011'	=>'Fabric aoi 2',
									],
									'class' => 'select-search',
									'attributes' => [
										'id' => 'update_warehouse_id'
									]
								])

								@include('form.select', [
									'label_col'	=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'	=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 	=> 'type_stock',
									'label' 	=> 'Type Stock',
									'options' 	=> [
										'' 	=>	'',
										'1'		=>	'SLT',
										'2'		=>	'REGULAR',
										'3'		=>	'PR/SR',
										'4'		=>	'MTFC',
										'5'		=>	'NB',
										'6'		=>	'ALLOWANCE'
									],
									'class' => 'select-search',
									'attributes' => [
										'id' => 'update_type_stock'
									]
								])

								@include('form.picklist', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> 'document_no',
									'label' 		=> 'Po Supplier',
									'name' 			=> 'document_no',
									'placeholder' 	=> 'Please select po supplier here',
									'attributes' 	=> [
										'id' 		=> 'update_document_no',
										'readonly' 	=> true,
									]
								])

								@include('form.picklist', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> 'item_code_source',
									'label' 		=> 'Item Code Source',
									'name' 			=> 'item_code_source',
									'placeholder' 	=> 'Please select item code source here',
									'attributes' 	=> [
										'id' 		=> 'update_item_code_source',
										'readonly' 	=> true,
									]
								])

								@include('form.text', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> 'booking_number',
									'label' 		=> 'Booking Number',
									'placeholder' 	=> 'Please insert booking number here',
									'attributes' 	=> [
										'id' 		=> 'update_booking_number'
									]
								])
							</div>
						</div>
						<div class="col-md-6">
							<div class="col-md-12">
								@include('form.text', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> 'po_buyer',
									'label' 		=> 'Po Buyer',
									'placeholder' 	=> 'Please insert po buyer here',
									'attributes' 	=> [
										'id' 		=> 'update_po_buyer',
										'readonly' 	=> true,
									]
								])

								@include('form.text', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> 'item_code',
									'label' 		=> 'Item Code Book',
									'placeholder' 	=> 'Please type item code book here',
									'attributes' 	=> [
										'id' 		=> 'update_item_code_book',
										'readonly' 	=> true,
									]
								])

								@include('form.text', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> 'qty_allocation',
									'label' 		=> 'Qty Allocation',
									'placeholder' 	=> 'Please type qty allocation here',
									'attributes' 	=> [
										'id' 			=> 'update_qty_allocation',
										'autocomplete' 	=> 'off'
									]
								])
								@include('form.text', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> 'qty_outstanding',
									'label' 		=> 'Qty Oustanding',
									'attributes'	=> [
										'id' 		=> 'update_qty_outstanding',
										'readonly' 	=> true,
										'autocomplete' 	=> 'off'
									]
								])
								@include('form.text', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> 'qty_allocated',
									'label' 		=> 'Qty Allocated',
									'attributes' 	=> [
										'id'		=> 'update_qty_allocated',
										'readonly' => true
									]
								])
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							@include('form.textarea', [
								'label_col'		=> 'col-md-12 col-lg-12 col-sm-12',
								'form_col'		=> 'col-md-12 col-lg-12 col-sm-12',
								'field' => 'cancel_reason',
								'label' => 'Reason',
								'placeholder' => 'Please type reason here',
								'attributes' => [
									'id' 		=> 'cancel_reason',
									'rows'	 	=> '5',
									'cols' 		=> '15',
									'resize' 	=> 'none'
								]
							])
							{!! Form::hidden('_update_qty_outstanding','', array('id' => '_update_qty_outstanding')) !!}
							{!! Form::hidden('_update_qty_allocation','', array('id' => '_update_qty_allocation')) !!}
							{!! Form::hidden('supplier_name','', array('id' => 'supplier_name')) !!}
							{!! Form::hidden('c_bpartner_id','', array('id' => 'c_bpartner_id')) !!}
							{!! Form::hidden('type_stock_erp','', array('id' => 'type_stock_erp')) !!}
							{!! Form::hidden('type_stock','', array('id' => 'type_stock')) !!}
							{!! Form::hidden('id','', array('id' => 'update_id')) !!}
						</div>
					</div>
				</div>
                <div class="modal-footer text-center">
					<button type="submit" class="btn btn-blue-success"> Save <i class="icon-floppy-disk position-left"></i></button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close <span class="glyphicon glyphicon-remove"></span></button>
                </div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>