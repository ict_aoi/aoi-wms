@extends('layouts.app', ['active' => 'accessories_barcode_material_quality_control'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Barcode Quality Control</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li>Barcode</li>
            <li class="active">Quality Control</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
	<div class="panel-body">
		@role(['admin-ict-acc'])
			@include('form.select', [
				'field' 			=> 'warehouse',
				'label' 			=> 'Warehouse',
				'default'			=> auth::user()->warehouse,
				'label_col' 		=> 'col-sm-12',
				'form_col' 			=> 'col-sm-12',
				'options' 			=> [
					'1000002' 		=> 'Warehouse Accessories AOI 1',
					'1000013' 		=> 'Warehouse Accessories AOI 2',
				],
				'class' 			=> 'select-search',
				'attributes' 		=> [
					'id' 			=> 'select_warehouse'
				]
			])
		@else 
			{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
		@endrole

		<div class="row">
			<div class="col-lg-6">
				{!!
					Form::open(array(
						'class' 	=> 'form-horizontal',
						'role' 		=> 'form',
						'url' 		=> route('accessoriesPrintBarcodeQualityControl.print'),
						'method' 	=> 'get',
						'target' 	=> '_blank'
					))
				!!}
					{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_id')) !!}
				
					<div class="input-group content-group">
						<div class="has-feedback has-feedback-left">
							<input type="text" class="form-control input-xlg barcode" id="barcode" name="barcode" autofocus autocomplete="off" placeholder="Please scan your barcode here">
							<div class="form-control-feedback">
								<i class="icon-search4 text-muted text-size-base"></i>
							</div>
						</div>

						<div class="input-group-btn">
							<button type="submit" class="btn btn-blue-success btn-lg">Search <i class="icon-search4 position-left"></i></button>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="col-lg-6">
				{!!
					Form::open([
						'role' 		=> 'form',
						'url' 		=> route('accessoriesPrintBarcodeQualityControl.print'),
						'method' 	=> 'get',
						'class' 	=> 'form-horizontal',
						'target' 	=> '_blank',
						'id'		=> 'form-search'
					])
				!!}
				
					{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => '_warehouse_id')) !!}
					
					@include('form.picklist', [
						'field' 			=> 'po_buyer',
						'name' 				=> 'po_buyer',
						'placeholder' 		=> 'Please Select PO Buyer',
						'div_class' 		=> 'col-xs-12',
						'form_col' 			=> 'col-xs-12',
						'help' 				=> 'If barcode can\'t be scan, please select po buyer here.'
					])
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection

@section('page-modal')
	@include('accessories_barcode_material_quality_control.modal_picklist', [
		'name' => 'po_buyer',
		'name2' => 'item_code',
		'title' => 'List Po Buyer'
	])
@endsection

@section('page-js')
	<script type="text/javascript" src="{{ asset(elixir('js/accessories_barcode_material_quality_control.js'))}}"></script>
@endsection

