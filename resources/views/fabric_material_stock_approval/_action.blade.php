
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($item_approve))
                <li><a href="#" onclick="approve('{!! $item_approve !!}')"><i class="icon-file-check2"></i> Approve</a></li>
            @endif

            @if (isset($reject))
                <li><a href="#" onclick="reject('{!! $reject !!}')"><i class="icon-x"></i> Reject</a></li>
            @endif

           
        </ul>
    </li>
</ul>
