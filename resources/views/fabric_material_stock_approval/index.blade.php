@extends('layouts.app', ['active' => 'fabric_material_stock_approval'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Stock Approval</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li>Material Stock</li>
			<li class="active">Approval</li>
        </ul>
	</div>
</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>
		{!!
				Form::open([
					'role'          => 'form',
					'url'           => route('FabricMaterialStockApprovalController.approveSelected'),
					'method'        => 'post',
					'class'         => 'form-horizontal',
					'id'            => 'form'
				])
        !!}
		<div class="panel-body">
			@include('form.select', [
				'field' 			=> 'warehouse',
				'label' 			=> 'Warehouse',
				'default' 			=> (auth::user()->department == 'finance & accounting' ? '': auth::user()->warehouse),
				'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
				'options' => [
					'' 		  => 'All Warehouse Fabric',
					'1000001' => 'Warehouse Fabric AOI 1',
					'1000011' => 'Warehouse Fabric AOI 2',
				],
				'class' => 'select-search',
				'attributes' => [
					'id' => 'select_warehouse'
				]
			])
			<a href="{{ route('accessoriesMaterialStockApproval.exportAll') }}" class="btn btn-blue-success col-xs-12">Export All <i class="icon-file-download2 position-left"></i></a>
			@role(['mm-staff-fab', 'finance-account-fab', 'admin-ict-fabric'])
				<button type="submit" class="btn btn-success col-xs-12">Approve<i class="icon-floppy-disk position-left"></i></button>
				{!! Form::hidden('list_history_material_stock_opname','', array('id' => 'list_history_material_stock_opname')) !!}
				{!! Form::hidden('page','index' , array('id' => 'page')) !!}
				{{ Form::close() }}
				{!!
				Form::open([
					'role'          => 'form',
					'url'           => route('FabricMaterialStockApprovalController.approveAll'),
					'method'        => 'post',
					'class'         => 'form-horizontal',
					'id'            => 'form-approve-all'
				])
        		!!}
				{!! Form::hidden('warehouse_id','', array('id' => 'warehouse_id')) !!}
				<button type="submit" class="btn btn-danger col-xs-12">Approve All<i class="icon-floppy-disk position-left"></i></button>
				{{ Form::close() }}
			@endrole
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover " id="materialStockApprovalTable">
					<thead>
						<tr>
							<th>history material stock opname id</th>
							<th>#</th>
							<th>material stock id</th>
							<th>Created At</th>
							<th>Created By</th>
							<th>Barcode</th>
							<th>Po Supplier</th>
							<th>Supplier Name</th>
							<th>No Roll</th>
							<th>Item Code</th>
							<th>Color</th>
							<th>Batch Number</th>
							<th>Actual Lot</th>
							<th>Qty Adjustment</th>
							<th>Note Adjustment</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
		</div>
@endsection

@section('page-js')
	<script src="{{ mix('js/fabric_material_stock_approval.js') }}"></script>
@endsection
