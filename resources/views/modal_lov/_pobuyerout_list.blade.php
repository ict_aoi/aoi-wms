<table class="table">
	<thead>
	  <tr>
			<th>Area</th>
			<th>Rack</th>
			<th>PO Buyer</th>
			<th>Action</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ $list->name }}
				</td>
				<td>
					@if ($list->name != 'GENERAL')
						{{ $list->rack }}.{{ $list->y_row }}.{{ $list->z_column }}
					@endif
				</td>
				<td>
					@if ($list->name != 'GENERAL')
						{{ $list->po_buyer }}
					@endif
				</td>
				<td class="text-center">
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
						type="button" data-id="{{ $list->po_buyer }}" data-destination="{{ $list->to_destination }}" data-name="{{ $list->name }} - {{ $list->rack }}.{{ $list->y_row }}.{{ $list->z_column }}" >Select</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
