<table class="table">
	<thead>
	  <tr>
		<th>Name</th>
		<th>Description</th>
		<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($items as $key => $item)
			<tr class="{{ $item->is_active ?  '' : 'active' }}">
				<td>{{ $item->display_name }}</td>
				<td>{{ $item->description }}</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" type="button"
						data-id="{{ $item->id }}" data-name="{{ $item->display_name }}" data-description = "{{ $item->description }}"
						data-obj="{{ $item->toJson() }}"
					>
						Select
					</button>
				</td>

				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $items->appends(Request::except('page'))->render() !!}
