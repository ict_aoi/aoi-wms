<table class="table">
	<thead>
	  <tr>
		<th>Item Code</th>
		<th>Item Desc</th>
		<th>Action</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ $list->item->item_code }}
				</td>
				<td>
						{{ $list->item->item_desc }}
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
						type="button" data-id="{{ $list->item_id }}" data-name="{{ $list->item->item_desc }}" >Select</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
