<table class="table">
	<thead>
	  <tr>
		<th>Name</th>
		<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($items as $key => $item)
			<tr>
				<td>{{ $item->name }}</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" type="button"
						data-id="{{ $item->id }}" data-name="{{ $item->name }}"
						data-obj="{{ $item->toJson() }}"
					>
						Select
					</button>
				</td>

				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $items->appends(Request::except('page'))->render() !!}
