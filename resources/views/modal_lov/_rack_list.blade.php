<table class="table">
	<thead>
	  <tr>
		<th>AREA</th>
		<th>RAK</th>
		<th>PO BUYER</th>
		<th>NOTE</th>
		<th>ACTION</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ $list->area->name }}
				</td>
				<td>
					@if ($list->area->name != 'GENERAL')
							{{ $list->rack }}.{{ $list->y_row }}.{{ $list->z_column }}
					@endif
				</td>
				<td>
					@if ($list->area->name != 'GENERAL' && $list->has_many_po_buyer == false)
							{{ $list->getActiveBuyer($list->id,$list->has_many_po_buyer) }}
					@endif
				</td>
				<td>
					@if ($list->has_many_po_buyer)
							Rak ini dapat dimasukan lebih dari 1 po buyer
					@else
							Rak ini hanya dapat dimasukan oleh 1 po buyer
					@endif
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
						type="button" data-id="{{ $list->id }}" data-pobuyer="{{ $list->getActiveBuyer($list->id,$list->has_many_po_buyer) }}" data-name="{{ $list->area->name }} - {{ $list->rack }}.{{ $list->y_row }}.{{ $list->z_column }}" data-manypobuyer="{{ $list->has_many_po_buyer }}" >Select</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
