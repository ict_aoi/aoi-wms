<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
			<th>Lc Date</th>
			<th>Promise Date</th>
			<th>Season</th>
			<th>Po Buyer</th>
			<th>Brand</th>
			<th>Type Stock</th>
			<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			@php
				if($list->statistical_date) $promise_date = $list->statistical_date;
				else $promise_date = $list->promise_date;
				
				if($page == 'auto_allocation'){
					$_lc_date = $list->lc_date->format('Y-m-d');
					$_promise_date = $promise_date->format('Y-m-d');
				}else{
					$_lc_date = $list->lc_date;
					$_promise_date = $promise_date;
				}
			@endphp
			<tr>
				<td>{{ $_lc_date}}</td>
				<td>{{ $_promise_date}}</td>
				<td>{{ $list->season }}</td>
				<td>{{ $list->po_buyer }}</td>
				<td>{{ $list->brand }}</td>
				<td>{{ $list->type_stock }}</td>
				<td>
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose" type="button"
						data-id="{{ $list->po_buyer }}" data-name="{{ $list->po_buyer }}"
						data-season="{{ $list->season }}"  data-lc="{{ $_lc_date }}"
						data-brand="{{ $list->brand }}"  data-typestock="{{ $list->type_stock }}" 
						data-typestockerpcode="{{ $list->type_stock_erp_code }}"    
						data-promise="{{ $_promise_date }}"    
					>
						Select
					</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
