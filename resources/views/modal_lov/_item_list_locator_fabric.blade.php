<table class="table">
	<thead>
	  <tr>
		<th>ITEM CODE</th>
		<th>ITEM DESC</th>
		<th>ACTION</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ $list->item_code }}
				</td>
				<td>
					{{ $list->item_desc }}
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose"
						type="button" data-id="{{ $list->item_code }}" data-name="{{ $list->item_desc }}">Select
					</button>

				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
