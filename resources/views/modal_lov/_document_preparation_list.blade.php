<table class="table">
	<thead>
	  <tr>
			<th>NO. PO SUPPLIER</th>
			<th>ACTION</th>
		</tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ $list->document_no }}
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
						type="button" data-id="{{ $list->document_no }}" data-name="{{ $list->document_no }}" >Select</button>
				</td>
				
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
