<table class="table">
	<thead>
	  <tr>
		<th>BATCH NUMBER</th>
		<th>ACTION</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ $list->batch_number }}
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose"
						type="button" data-id="{{ $list->batch_number }}">Select
					</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
