<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
			<th>Po Buyer</th>
			<th>Style</th>
			<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>{{ $list->po_buyer }}</td>
				<td>{{ $list->style }}</td>
				<td>
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose" type="button"
						data-id="{{ $list->po_buyer }}:{{ $list->style }}" data-name="{{ $list->po_buyer }} - {{ $list->style }}" data-style="{{ $list->style }}">
						Select
					</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
