
<div id="detailHandoverModal" data-backdrop="static" data-keyboard="false"  class="modal fade" style="color:black !important">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<h5 class="modal-title">Handover for <span id="header_detail_handover_mrp"></span></h5>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table datatable-basic table-striped table-hover table-responsive" id="report_memo_request_production_detail_handover_tabel">
						<thead>
							<tr>
								<th>Barcode</th>
								<th>Po Supplier</th>
								<th>Supplier Name</th>
								<th>Uom</th>
								<th>Qty On Barcode</th>
								<th>Warehouse Movement</th>
								<th>Status Movement</th>
								<th>Date Movement</th>
								<th>Pic Movement</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>

			{!! Form::hidden('po_buyer','', array('id' => 'detail_handover_po_buyer')) !!}
			{!! Form::hidden('item_code','', array('id' => 'detail_handover_item_code')) !!}
			{!! Form::hidden('style','', array('id' => 'detail_handover_style')) !!}
			{!! Form::hidden('article_no','', array('id' => 'detail_handover_article_no')) !!}
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close  <span class="glyphicon glyphicon-remove"></span></button>
			</div>
		</div>
	</div>
</div>
