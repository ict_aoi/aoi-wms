@extends('layouts.app', ['active' => 'report_memo_request_production'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Memo Requst Production (MRP)</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li class="active">Memo Request Production (MRP)</li>
			</ul>
			<ul class="breadcrumb-elements">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
						<i class="icon-three-bars position-left"></i>
						Actions
						<span class="caret"></span>
					</a>
					
					<ul class="dropdown-menu dropdown-menu-right">
					@role(['admin-ict-acc','admin-ict-fabric'])
						<li><a href="{{ route('reportMemoRequestProduction.import') }}"><i class="icon-file-excel pull-right"></i> Import Closing</a></li>
					@endrole
					</ul>
				</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
	<div class="panel-heading">
		<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
			</ul>
		</div>
	</div>
	<div class="panel-body">
		{!!
			Form::open(array(
				'class' 	=> 'form-horizontal',
				'role' 		=> 'form',
				'url' 		=> route('reportMemoRequestProduction.export'),
				'method' 	=> 'get',
				'target'    => '_blank'
			))
		!!}	
			

			@include('form.picklist', [
				'field' 			=> 'po_buyer',
				'name' 				=> 'po_buyer',
				'placeholder' 		=> 'Select po buyer',
				'div_class' 		=> 'col-xs-12',
				'form_col' 			=> 'col-xs-12',
				'readonly'			=> true
			]) 

			<button type="submit" class="btn btn-default col-xs-12" style="margin-top: 15px">Export <i class="icon-file-excel position-right"></i></button>
		{!! Form::close() !!}

		{!!
			Form::open(array(
				'class' 	=> 'form-horizontal',
				'role' 		=> 'form',
				'url' 		=> route('reportMemoRequestProduction.print'),
				'method' 	=> 'get',
				'target'    => '_blank'
			))
		!!}	
			{!! Form::hidden('_print_po_buyer',null, array('id' => '_print_po_buyer')) !!}		
			@php 
				$akses = 'print-mrp';
				$role_name = \Auth::user()->roles->pluck('name')->toArray();
				$warehouse = Auth::user()->warehouse;
			@endphp
			@if(in_array($akses, $role_name) || $warehouse == '1000002'|| $warehouse == '1000001' || $warehouse == '1000011') 
				<button type="submit" class="btn btn-yellow-cancel col-xs-12">Print Preview <i class="icon-printer position-right"></i></button>
			@endif
		{!! Form::close() !!}
		<a href="{{ route('reportMemoRequestProduction.downloadAll') }}" class="btn btn-blue-success col-xs-12">Download All <i class="icon-file-download2 position-left"></i></a>

		{{-- <a href="#" class="btn btn-blue-success col-xs-12">Download All <i class="icon-file-download2 position-left"></i></a> --}}

	</div>
</div>

<div class="panel panel-default border-grey">
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table datatable-basic table-striped table-hover table-responsive" id="report_mrp_table">
				<thead>
					<tr>
						<th>Season</th>
						<th>PODD Date</th>
						<th>Backlog Status</th>
						<th>Style</th>
						<th>Article No</th>
						<th>Po Buyer</th>
						<th>Category</th>
						<th>Item Code</th>
						<th>Uom</th>
						<th>Qty Need</th>
						<th>Qty Available</th>
						<th>Qty Handover Not Received Yet On Destination</th>
						<th>Qty Supply</th>
						<th>Po Supplier</th>
						<th>Last Status Movement</th>
						<th>Last Warehouse Location</th>
						<th>Last Location</th>
						<th>Last Movement Date</th>
						<th>Last Movement User</th>
						<th>Remark</th>
						<th>System Note</th>
						<th>Action</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
{!! Form::hidden('page','index', array('id' => 'page')) !!}
@endsection

@section('page-modal')
	@include('form.modal_picklist', [
		'name' 			=> 'po_buyer',
		'title' 		=> 'Filter PO Buyer',
		'placeholder' 	=> 'Search based on po buyer'
	])
	@include('report_memo_request_production._closing_modal')
	@include('report_memo_request_production._history_modal')
	@include('report_memo_request_production._detail_handover_modal')
@endsection


@section('page-js')
	<script type="text/javascript" src="{{ asset(elixir('js/report_memo_request_production.js'))}}"></script>
@endsection
