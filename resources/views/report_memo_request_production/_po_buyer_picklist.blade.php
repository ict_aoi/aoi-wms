<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
			<th>Po Buyer</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ $list->po_buyer }}
				</td>
				
				<td class="text-center">
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose" 
						type="button" data-id="{{ $list->po_buyer }}" data-name="{{ $list->po_buyer }}">Select</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
