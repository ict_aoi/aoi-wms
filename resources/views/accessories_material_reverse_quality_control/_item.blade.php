<script type="x-tmpl-mustache" id="accessories_material_reverse_quality_control_table">
	{% #list %}
		<tr id="panel_{% _id %}">
			<td>{% no %}.<input type="checkbox" class="checkbox_item" id="check_{% _idx %}" data-id="{% _idx %}" {% #is_unintegrated %} checked="checked" {% /is_unintegrated %}></td>
			<td>{% po_buyer %}</td>
			<td>{% item_code %}</td>
			<td>{% style %}</td>
			<td>{% article_no %}</td>
			<td>{% uom %}</td>
			<td>{% qty_reject %}</td>
			<td><textarea rows="3" class="form-control input-sm input-item input-edit ict-log" style="resize: none;" id="ict_log_{% _id %}" data-id="{% _id %}">{% ict_log %}</textarea></td>
			<td>
				<input type="number" class="form-control input-sm hidden input-item input-edit" id="currInput_{% _id %}" value="{% qty_movement %}" data-id="{% _id %}" data-row="" onkeydown="javascript: return event.keyCode == 69 ? false : true">
				<input type="number" class="form-control input-sm hidden input-item input-edit" id="tempInput_{% _id %}" value="{% _temp %}" data-id="{% _id %}" data-row="" onkeydown="javascript: return event.keyCode == 69 ? false : true">

				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-icon-anim btn-circle btn-delete-item"><i class="fa icon-trash"></i></button>
			</td>
		</tr>
	{%/list%}
	<tr>
		<td colspan="9">
			<input type="text" id="barcode_value" name="barcode_value" class="form-control input-new barcode_value" placeholder="Please scan your barcode here" autocomplete="off"></input>
		</td>

	</tr>
</script>
