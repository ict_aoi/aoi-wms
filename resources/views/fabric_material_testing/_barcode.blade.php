<script type="x-tmpl-mustache" id="reject-table">
	<tr>
		<td colspan="12">
			<input type="text" id="barcode_value" name="barcode_value" class="barcode_value form-control input-new" placeholder="Please scan your barcode here"></input>
		</td>
	</tr>
	
	{% #item %}
		<tr>
			<td>
				<div class="row">
					<div class="col-xs-3">
						<div class="panel border-left-lg border-left-info">
							<div class="panel-body">
								<ul class="list list-unstyled">
									<li>
										<div class="row">
											<label class="col-lg-3">Document No</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" readonly="readonly" value="{% document_no %}" >
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<label class="col-lg-3">Supplier Name</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" readonly="readonly" value="{% supplier_name %}" >
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<label class="col-lg-3">No Invovice</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" readonly="readonly" value="{% no_invoice %}" >
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<label class="col-lg-3">Item Code</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" readonly="readonly" value="{% item_code %}" >
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<label class="col-lg-3">Color</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" readonly="readonly" value="{% color %}" >
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<label class="col-lg-3">Roll Number</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" readonly="readonly" value="{% nomor_roll %}" >
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<label class="col-lg-3">Batch Number</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" readonly="readonly" value="{% batch_number %}" >
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<label class="col-lg-3">Style</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" readonly="readonly" value="{% style %}" >
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<label class="col-lg-3">Composition</label>
											<div class="col-lg-9">
												<textarea class="form-control" readonly="readonly"> {% composition %} </textarea>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>

					<div class="col-xs-9">
						<div class="table-responsive">
							<table class="table table-striped table-hover" style="background-color:transparent">
								<thead>
									<tr>
										<th rowspan="2">Test Method</th>
										<th rowspan="2">Subtesting</th>
										<th rowspan="2">Result</th>
										<th rowspan="2">Remark</th>
										<th colspan="2"style="text-align: center">Test Status</th>
										<th rowspan="2">Action</th>
									</tr>
									<tr>
										<th>Pass</th>
										<th>Fail</th>
									</tr>
								</thead>
								<tbody>
									{% #detail %}
										<tr>
											<td>
												<input type="text" class="form-control input-edit-point-from input-number" id="point_check_from_{% _id %}_{% _idx %}" data-id="{% _id %}" data-idx="{% _idx %}" value="{% point_check_from %}" readonly="readonly"></input>
											</td>
											<td>
												<input type="text" class="form-control input-edit-point-to input-number" id="point_check_to_{% _id %}_{% _idx %}" data-id="{% _id %}" data-idx="{% _idx %}" value="{% point_check_to %}" readonly="readonly"></input>
											</td>
											<td>
												<textarea class="form-control input-edit-remark" style="resize:none" id="remark_{% _id %}_{% _idx %}" data-id="{% _id %}" data-idx="{% _idx %}">{% remark %}</textarea>
											</td>
											<td>
												<input type="radio" class="input-edit-defect-value" name="defect_code_value_{% _id %}_{% _idx %}" id="defect_code_value_{% _id %}_{% _idx %}_1" value="1" data-id="{% _id %}" data-idx="{% _idx %}" {% #is_selected_1 %}checked="checked"{% /is_selected_1 %}>
											</td>
											<td>
												<input type="radio" class="input-edit-defect-value" name="defect_code_value_{% _id %}_{% _idx %}" id="defect_code_value_{% _id %}_{% _idx %}_2" value="2" data-id="{% _id %}" data-idx="{% _idx %}" {% #is_selected_2 %}checked="checked"{% /is_selected_2 %}>
											</td>
											<td>
												<button type="button" data-id="{% _id %}" data-idx="{% _idx %}" class="btn btn-dafault btn-delete-detail-point"><i class="fa icon-trash"></i></button>
											</td>
										</tr>
									{% /detail %}
									<tr>
										<td>
											<select class="form-control test-method-options" id="status_option_{% _id %}" data-id="{% _id %}">
												{% #status_options %}
													<option value="{% id %}" {% selected %}>{% name %}</option>
												{% /status_options %}
											</select>
										</td>
										<td>
											<select class="form-control subtest-method-options" id="subtest_method_options_{% _id %}" data-id="{% _id %}">
												{% #detail_status_options %}
													<option value="{% id %}" {% selected %}>{% name %}</option>
												{% /detail_status_options %}
											</select>
										</td>
										<td>
											<input type="text" class="form-control input-new input-number" id="point_check_to_{% _id %}" data-id="{% _id %}"></input>
										</td>
										<td>
											<textarea class="form-control input-new input-remark" style="resize:none" id="remark_{% _id %}" data-id="{% _id %}"></textarea>
										</td>
										<td>
											<input type="radio" name="defect_code_value_{% _id %}" id="defect_code_value_{% _id %}_1" value="pass" data-id="{% _id %}">
										</td>
										<td>
											<input type="radio" name="defect_code_value_{% _id %}" id="defect_code_value_{% _id %}_2" value="fail" data-id="{% _id %}">
										</td>
										<td>
											<button type="button" data-id="{% _id %}" class="btn btn-blue-success btn-add-detail-point"><i class="icon-plus-circle2"></i></button>
										</td>
									</tr>
								</tbody>
								<tfoot>
								</tfoot>
							</table>
						</div>
					</div>

				</div>
				<div class="row">
					<div class="form-group text-right">
						<button id="remove_row_{% _id %}" data-id="{% _id %}" class="btn btn-yellow-cancel btn-lg col-xs-12 btn-remove-row">REMOVE <i class="fa icon-trash position-left"></i></button>
					</div>
				</div>
			</td>
		</tr>
	{%/item%}
</script>
