@extends('layouts.app', ['active' => 'user']) 
 
@section('content') 
  <div class="page-header"> 
    <div class="page-header-content"> 
      <div class="page-title" style="padding-top:0px;"> 
        <h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">USER PROFILE</span></h4> 
        <ul class="breadcrumb breadcrumb-caret position-left"> 
          <li><a href="{{ route('dashboard') }}">Account Setting</a></li> 
          <li class="active">Profile</li> 
        </ul> 
        <a class="heading-elements-toggle"><i class="icon-more"></i></a> 
      </div> 
    </div> 
  </div> 
 
  {!! 
    Form::model($user, 
    [ 
      'role' => 'form', 
      'url' => route('user.updatepassword', $user->id), 
      'method' => 'put', 
      'enctype' => 'multipart/form-data', 
      'class' => 'form-horizontal', 
      'id'=> 'form' 
    ]) 
  !!} 
  <div class="panel panel-default border-grey"> 
    <div class="panel-body"> 
      <div class="form-group"> 
        <div class="row"> 
          <div class="col-md-6"> 
            <label>NIK</label> 
            <input type="text" name="nik" value="{{ $user->nik }}" readonly="readonly" class="form-control"> 
          </div> 
 
          <div class="col-md-6"> 
            <label>NAME</label> 
            <input type="text" name="name" value="{{ $user->name }}" readonly="readonly" class="form-control"> 
           
          </div> 
        </div> 
      </div> 

      <div class="form-group"> 
        <div class="row"> 
          <div class="col-md-6"> 
            <label>DIVISION</label> 
            <input type="text" name="divisi" value="{{ $user->division }}" readonly="readonly" class="form-control"> 
          </div> 
 
          <div class="col-md-6"> 
            <label>WAREHOUSE</label> 
            @php 
              if($user->warehouse == '1000002')
                $_warehouse = 'ACCESSORIES AOI 1';
              elseif($user->warehouse == '1000013')
                $_warehouse = 'ACCESSORIES AOI 2';
              elseif($user->warehouse == '1000011')
                $_warehouse = 'FABRIC AOI 2';
              elseif($user->warehouse == '1000001')
                $_warehouse = 'FABRIC AOI 1';
                
            @endphp
            <input type="text" name="name" value="{{ $_warehouse }}" readonly="readonly" class="form-control"> 
           
          </div> 
        </div> 
      </div> 
 
      <div class="form-group"> 
        <div class="row"> 
          <div id="password_new_error" class="col-md-6"> 
            <label>NEW PASSWORD</label> 
            <input type="password" name = "password_new" id="password_new" placeholder="Enter new password" class="form-control"> 
            <span id="password_new_danger" class="help-block text-danger">*Required</span> 
     
          </div> 
 
          <div id="password_confirm_error" class="col-md-6"> 
            <label>REPEAT PASSWORD</label> 
            <input type="password" name = "password_confirm" id="password_confirm" placeholder="Repeat new password" class="form-control"> 
            <span id="password_confirm_danger" class="help-block text-danger">*Required</span> 
          </div> 
        </div> 
      </div> 
    </div> 
  </div> 
  <hr> 
  <div class="text-right"> 
    <button type="submit" class="btn btn-success">SAVE <i class="icon-floppy-disk position-left"></i></button> 
  </div> 
  {!! Form::close() !!} 
@endsection 
 
@section('content-js') 
<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script> 
<script> 
  var url_loading = $('#loading_gif').attr('href'); 
  $('#form').submit(function (event) { 
    event.preventDefault(); 
    bootbox.confirm("Apakah anda yakin akan menyimpan perubahan ini ?", function (result) { 
      if (result) { 
        $.ajax({ 
          type: "PUT", 
          url: $('#form').attr('action'), 
          data: $('#form').serialize(), 
          beforeSend: function () { 
            $.blockUI({ 
              message: "<img src='" + url_loading + "' />", 
              css: { 
                backgroundColor: 'transaparant', 
                border: 'none', 
              } 
            }); 
          }, 
          complete: function () { 
            $.unblockUI(); 
          }, 
          success: function (response) { 
            $("#alert_success").trigger("click", 'Data berhasil disimpan.'); 
          }, 
          error: function (response) { 
            $.unblockUI(); 
            if (response['status'] == 400) { 
              for (i in response['responseJSON']['errors']) { 
                $('#' + i + '_error').addClass('has-error'); 
                $('#' + i + '_danger').text(response['responseJSON']['errors'][i]); 
              } 
            } else if (response['status'] == 422) { 
              $("#alert_error").trigger("click", response['responseJSON']); 
            } else if (response['status'] == 500) { 
              $("#alert_error").trigger("click", 'Please Contact ICT'); 
            } 
          } 
        }) 
        .done(function(response){ 
          $('#password_new').val(''); 
          $('#password_confirm').val(''); 
          $('#password_new_error').removeClass('has-error'); 
          $('#password_new_danger').text('*Required'); 
          $('#password_confirm_error').removeClass('has-error'); 
          $('#password_confirm_danger').text('*Required'); 
        }); 
      } 
    }); 
  }); 
</script> 
@endsection 