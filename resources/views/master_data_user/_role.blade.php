<script type="x-tmpl-mustache" id="role_table">
	{% #item %}
		<tr>
			<td>{% no %}</td>
			<td>{% name %}</td>
			<td>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default  btn-delete-item"><i class="icon-trash"></i></button>
			</td>
		</tr>
	{%/item%}
</script>