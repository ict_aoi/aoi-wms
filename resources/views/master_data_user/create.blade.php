@extends('layouts.app', ['active' => 'master_data_user'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Data User</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data</li>
				<li>User Management</li>
				<li><a href="{{ route('masterDataUser.index') }}" id="url_master_data_user">User</a></li>
				<li class="active">Create</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="row">
		<div class="col-md-5 col-lg-5 col-sm-12">
			<div class="panel panel-flat">
				<div class="panel-body">
					{!!
						Form::open([
							'role' 		=> 'form',
							'url' 		=> route('masterDataUser.store'),
							'method' 	=> 'post',
							'class' 	=> 'form-horizontal',
							'enctype' 	=> 'multipart/form-data',
							'id'		=> 'form'
						])
					!!}
				
						@include('form.select', [
							'field' => 'warehouse',
							'label' => 'Warehouse',
							'mandatory' => '*Required',
							'options' => [
								'' => '-- Select Warehouse --',
								'1000002' => 'Accessories AOI 1',
								'1000013' => 'Accessories AOI 2',
								'1000001' => 'Fabric AOI 1',
								'1000011' => 'Fabric AOI 2',
							],
							'class' => 'select-search',
							'label_col' => 'col-md-2 col-lg-2 col-sm-12',
							'form_col' => 'col-md-10 col-lg-10 col-sm-12',
							'attributes' => [
								'id' => 'warehouse'
							]
						])

						@include('form.picklist', [
							'label_col'		=> 'col-md-2 col-lg-2 col-sm-12',
							'form_col'		=> 'col-md-10 col-lg-10 col-sm-12',
							'field' 		=> 'employee',
							'label' 		=> 'Employee',
							'name' 			=> 'employee',
							'placeholder' 	=> 'Please select employee here',
							'attributes' 	=> [
								'id' 		=> 'employee',
								'readonly' 	=> true,
							]
						])

						@include('form.text', [
							'field' => 'nik',
							'label' => 'Nik',
							'label_col' => 'col-md-2 col-lg-2 col-sm-12',
							'form_col' => 'col-md-10 col-lg-10 col-sm-12',
							'attributes' => [
								'id' => 'nik',
							]
						])

						@include('form.text', [
							'field' => 'name',
							'label' => 'Nama',
							'label_col' => 'col-md-2 col-lg-2 col-sm-12',
							'form_col' => 'col-md-10 col-lg-10 col-sm-12',
							'attributes' => [
								'id' => 'name',
								'readonly' => 'readonly'
							]
						])

						@include('form.text', [
							'field' => 'department',
							'label' => 'Department',
							'label_col' => 'col-md-2 col-lg-2 col-sm-12',
							'form_col' => 'col-md-10 col-lg-10 col-sm-12',
							'attributes' => [
								'id' => 'department',
								'readonly' => 'readonly'
							]
						])

						@include('form.select', [
							'field' => 'sex',
							'label' => 'Sex',
							'options' => [
								'' => '-- Select Sex --',
								'laki' 		=> 'Male',
								'perempuan' => 'Female',
							],
							'class' => 'select-search',
							'label_col' => 'col-md-2 col-lg-2 col-sm-12',
							'form_col' => 'col-md-10 col-lg-10 col-sm-12',
							'attributes' => [
								'id' => 'select_sex'
							]
						])
						
						@include('form.picklist', [
							'label_col'		=> 'col-md-2 col-lg-2 col-sm-12',
							'form_col'		=> 'col-md-10 col-lg-10 col-sm-12',
							'field' 		=> 'role',
							'label' 		=> 'Role',
							'name' 			=> 'role',
							'placeholder' 	=> 'Please select role here',
							'attributes' 	=> [
								'id' 		=> 'role',
								'readonly' 	=> true,
							]
						])
						{!! Form::hidden('roles', '[]', array('id' => 'roles')) !!}
					<div class="text-right">
						<button type="submit" class="btn btn-blue-success col-xs-12">Save <i class="icon-floppy-disk position-left"></i></button>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
		<div class="col-md-7 col-lg-7 col-sm-12">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">User Roles &nbsp;</span></h6>
				</div>
				<div class="panel-body">
					<table class="table datatable-basic table-striped table-hover table-responsive">
						<thead>
							<tr>
								<th>No</th>
								<th>Roles</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="tbody_role"></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	{!! Form::hidden('page','create', array('id' => 'page')) !!}
@endsection

@section('page-modal')
	@include('form.modal_picklist', [
		'name' 			=> 'role',
		'title' 		=> 'List Roles',
		'placeholder' 	=> 'Search based on role',
	])

	@include('form.modal_picklist', [
		'name' 			=> 'employee',
		'title' 		=> 'List Employees',
		'placeholder' 	=> 'Search based on employee',
	])
@endsection

@section('page-js')
	@include('master_data_user._role')
	<script src="{{ mix('js/master_data_user.js') }}"></script>
@endsection
