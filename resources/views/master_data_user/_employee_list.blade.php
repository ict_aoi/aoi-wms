<table class="table table-striped table-hover">
	<thead>
	  <tr>
		<th>Nik</th>
		<th>Name</th>
		<th>Department</th>
		<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>{{ $list->nik }}</td>
				<td>{{ $list->name }}</td>
				<td>{{ $list->department_name }}</td>
				<td>
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose"
						type="button" data-nik="{{ $list->nik }}" 
						data-name="{{ $list->name }}" 
						data-department="{{ $list->department_name }}">Select</button>
				</td>
			</tr>

		@endforeach
	</tbody>
</table>
{!! $lists->appends(Request::except('page'))->render() !!}
