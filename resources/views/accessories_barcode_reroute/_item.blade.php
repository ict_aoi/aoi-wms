<script type="x-tmpl-mustache" id="print-barcode-table">
	{% #item %}
		<tr>
			<td> {%no%} 
				{%#checked%}
					<input type="checkbox" id="check_{%_id%}" class="input-checked" data-id={%_id%} checked="checked">
				{%/checked%}
				{%^checked%}
					<input type="checkbox" id="check_{%_id%}" class="input-checked" data-id={%_id%}>
				{%/checked%}
			</td>
			<td>
				{% document_no %}
			</td>
			<td>
				{% old_po_buyer %}
			</td>
			<td>
				{% new_po_buyer %}
			</td>
			<td>
				{% item_code %}
			</td>
			<td>
				{% style %}
			</td>
			<td>
				{% article_no %}
			</td>
			<td>
				{% qty_conversion %} ({% uom_conversion %})
			</td>
			
		</tr>
	{%/item%}
</script>
