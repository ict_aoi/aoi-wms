<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
			<th>Old Po Buyer</th>
			<th>New Po Buyer</th>
			<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>{{ $list->old_po_buyer }}</td>
				<td>{{ $list->new_po_buyer }}</td>
				<td>
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose" type="button"
						data-id="{{ $list->new_po_buyer }}" 
						data-name="{{ $list->new_po_buyer }}"
						data-old="{{ $list->old_po_buyer }}"
					>
						Select
					</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
