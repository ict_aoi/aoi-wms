@extends('layouts.app', ['active' => 'fabric_material_quality_control'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Quality Control</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Material Quality Control</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	@role(['admin-ict-fabric'])
		<div class="panel panel-default border-grey">
			<div class="panel-body">
				@include('form.select', [
					'field' 			=> 'warehouse',
					'label' 			=> 'Warehouse',
					'default'			=> auth::user()->warehouse,
					'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
					'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
					'options' 			=> [
						'1000001' 		=> 'Warehouse Fabric AOI 1',
						'1000011' 		=> 'Warehouse Fabric AOI 2',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_warehouse'
					]
				])
			</div>
		</div>
	@else 
		{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
	@endrole

	<div class="panel panel-default border-grey panel-collapsed">
		<div class="panel-heading">
			<h5 class="panel-title">Defect Codes<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
				@foreach ($defect_codes as $key => $item)
					@if (($key+1)%11 == 0)
						</div><div class="col-md-6">
					@endif
					<span><b>{{ $item->code}}</b> : {{$item->description}}. <br/></span>
				@endforeach
				</div>
			</div>
			
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">&nbsp;<a class="heading-elements-toggle"><i class="icon-more"></i></a><a class="heading-elements-toggle"><i class="icon-more"></i></a><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<div class="heading-form">
					<div class="heading-btn">
						<i type="button" class="btn btn-default" id="is_release" style="background-color: rgb(252, 252, 252); color: black;">RELEASE</i>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover " id="fabric_report_material_inspect_lab_table">
					<tbody id="tbody-reject">
					</tbody>
				</table>
			</div>

			{!!
				Form::open([
					'role' 		=> 'form',
					'url' 		=> route('fabricMaterialQualityControl.store'),
					'method' 	=> 'post',
					'enctype' 	=> 'multipart/form-data',
					'class' 	=> 'form-horizontal',
					'id'		=> 'form'
				])
			!!}

				{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_id')) !!}
				{!! Form::hidden('_selectAll',0, array('id' => '_selectAll')) !!}
				{!! Form::hidden('barcodes','[]' , array('id' => 'barcode-header')) !!}
				{!! Form::hidden('defect_codes',$defect_codes , array('id' => 'defect_codes')) !!}
				{!! Form::hidden('url_fabric_material_quality_control_create',route('fabricMaterialQualityControl.create') , array('id' => 'url_fabric_material_quality_control_create')) !!}
				{!! Form::hidden('status_release','', array('id' => 'status_release')) !!}
				{!! Form::hidden('_is_release','0', array('id' => '_is_release')) !!}

			<a href="{{ route('temporary.delete') }}" id="url_temporary_delete" class="hidden"></a>
			<div class="form-group text-right" style="margin-top: 10px;">
				<button type="submit" class="btn btn-blue-success col-xs-12 btn-lg">Save <i class="icon-floppy-disk position-left"></i></button>
			</div>
		</div>
	</div>
	{!! Form::hidden('page','index', array('id' => 'page')) !!}
@endsection

@section('page-js')
	<script src="{{ mix('js/fabric_material_quality_control.js') }}"></script>
	@include('fabric_material_quality_control._barcode')
@endsection

