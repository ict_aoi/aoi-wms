@extends('layouts.app', ['active' => 'fabric_report_material_inspect_lab'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Material Inspect Lab</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li>Report</li>
            <li><a href="{{ route('fabricReportMaterialInspectLab.index') }}">Material Inspect Lab</a></li>
            <li class="active">Detail</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Summary Information<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>
		<div class="panel-body">
			<div class="row">
					<div class="col-md-4 col-lg-4 col-sm-12">
						<p>Po Supllier <b>{{ $summary->document_no }}</b></p>
						<p>Item Code <b>{{ $summary->item_code }}</b></p>
						<p>Arrival Date <b>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $summary->receive_date)->format('d/M/Y H:i:s') }}</b></p>
					</div>
					<div class="col-md-4 col-lg-4 col-sm-12">
						<p>Supplier Name :  <b>{{ $summary->supplier_name }}</b></p>
						<p>Color :  <b>{{ $summary->color }}</b></p>
						<p>Total All Roll :  <b>{{ $data_total_roll->total_roll }} </b></p>
						<p>Total Inspect Roll :  <b>{{ ($data_inspect_roll ? $data_inspect_roll->total_roll : '0' )}}</b></p>

					</div>
					<div class="col-md-4 col-lg-4 col-sm-12">
						<p>No Invoice :  <b>{{$summary->no_invoice}}</b></p>
						<p>Style : <b>{{$summary->style}}</b></p> 
						<p>Total Qty All Roll :  <b>{{ $data_total_roll->qty_arrival }} ({{ trim($data_total_roll->uom) }})</b></p>
						<p>Total Qty Inspect Roll :  <b>{{ ($data_inspect_roll ? $data_inspect_roll->qty_arrival :'0') }} ({{ trim($data_total_roll->uom) }})</b></p>
					</div>
				</div>

			{!!
				Form::open(array(
					'class' 	=> 'form-horizontal',
					'role' 		=> 'form',
					'url' 		=> route('fabricReportMaterialInspectLab.exportDetail',$summary->id),
					'method' 	=> 'get'		
				))
			!!}
				<button type="submit" class="btn btn-default col-xs-12">Export All <i class="icon-file-excel position-left"></i></button>
			{!! Form::close() !!}

			{!!
				Form::open(array(
					'class' 	=> 'form-horizontal',
					'role' 		=> 'form',
					'url' 		=> route('fabricReportMaterialInspectLab.updateLot'),
					'method' 	=> 'post',
					'id'		=> 'form'		
				))
			!!}
				{!! Form::hidden('list_lot','[]' , array('id' => 'list_lot')) !!}
				<button type="submit" class="btn btn-blue-success col-xs-12">Update Lot or Result <i class="icon-floppy-disk position-left"></i></button>
			{!! Form::close() !!}
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover " id="fabric_report_material_inspect_lab_detail_table">
					<thead>
						<tr>
							<th>id</th>
							<th>Barcode</th>
							<th>Inspect Lab Date</th>
							<th>No Roll</th>
							<th>Location</th>
							<th>Batch Number</th>
							<th>Actual Lot</th>
							<th>Inspect Lot Result</th>
							<th>Uom</th>
							<th>Qty</th>
							<th>User Inspect</th>
							<th>Remark</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	{!! Form::hidden('url_fabric_material_inspect_lab_detail_data',route('fabricReportMaterialInspectLab.dataDetail',$summary->id) , array('id' => 'url_fabric_material_inspect_lab_detail_data')) !!}
	{!! Form::hidden('page','detail' , array('id' => 'page')) !!}
	
@endsection

@section('page-js')
	<script src="{{ mix('js/fabric_report_material_inspect_lab.js') }}"></script>
@endsection

