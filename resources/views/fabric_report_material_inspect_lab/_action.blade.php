@if(isset($action_view))
    @php $final_result = $model->inspect_lot_result; @endphp
    @if($final_result == 'REJECT' or $final_result == 'RELEASE')
        <select class="form-control status" disabled style="width: 100px;" id="id_{{ $model->id }}">
            <option value="RELEASE" @if($final_result == 'RELEASE') selected @endif >Release</option>
            <option value="HOLD" @if($final_result == 'HOLD') selected @endif >Hold</option>
            <option value="REJECT" @if($final_result == 'REJECT') selected @endif >Reject</option>
        </select>
    @else
        <select class="form-control status" style="width: 100px;" onChange="changelot('{{ $model->id }}')" id="id_{{ $model->id }}">
            <option value="RELEASE" @if($final_result == 'RELEASE') selected @endif >Release</option>
            <option value="HOLD" @if($final_result == 'HOLD') selected @endif >Hold</option>
            <option value="REJECT" @if($final_result == 'REJECT') selected @endif >Reject</option>
        </select>
    @endif
@else 

<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($detail))
                <li><a href="{!! $detail !!}"><i class="icon-search4"></i> Detail</a></li>
            @endif
           
            @if (isset($export))
                <li><a href="{!! $export !!}"><i class="icon-file-excel"></i> Print</a></li>
            @endif
            @if (isset($remark))
                <li><a onclick="remark('{!! $remark !!}')" ><i class="icon-pencil6"></i> Remark</a></li>
            @endif
        </ul>
    </li>
</ul>
@endif
