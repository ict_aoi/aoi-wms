@extends('layouts.app', ['active' => 'accessories_report_material_allocation_cancel_order'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Material Allocation Cancel Order</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li class="active">Material Allocation Cancel Order</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
	<div class="panel-heading">
		<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
			</ul>
		</div>
	</div>
	<div class="panel-body">
	{!!
		Form::open(array(
			'class' => 'form-horizontal',
			'role' => 'form',
			'url' => route('accessoriesReportMaterialAllocationCancelOrder.export'),
			'method' => 'get',
			'target' => '_blank'		
		))
	!!}

	@include('form.date', [
				'field' 		=> 'start_date',
				'label' 		=> 'LC Date From',
				'label_col' 	=> 'col-md-3 col-lg-3 col-sm-12',
				'form_col' 		=> 'col-md-9 col-lg-9 col-sm-12',
				'placeholder' 	=> 'dd/mm/yyyy',
				'default'		=> \Carbon\Carbon::now()->subDays(30)->format('d/m/Y'),
				'class' 		=> 'daterange-single',
				'attributes'	=> [
					'id' 			=> 'start_date',
					'autocomplete' 	=> 'off',
					'readonly' 		=> 'readonly'
				]
				])
			@include('form.date', [
				'field' 		=> 'end_date',
				'label' 		=> 'LC To ',
				'label_col' 	=> 'col-md-3 col-lg-3 col-sm-12',
				'default'		=> \Carbon\Carbon::now()->format('d/m/Y'),
				'form_col' 		=> 'col-md-9 col-lg-9 col-sm-12',
				'placeholder' 	=> 'dd/mm/yyyy',
				'class' 		=> 'daterange-single',
				'attributes' 	=> [
					'id' 			=> 'end_date',
					'readonly' 		=> 'readonly',
					'autocomplete' 	=> 'off'
				]
			])
			<button type="submit" class="btn btn-default col-xs-12">Export All <i class="icon-file-excel position-left"></i></button>
	{!! Form::close() !!}
	</div>
</div>

<div class="panel panel-default border-grey">
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table datatable-basic table-striped table-hover table-responsive" id="report_material_allocation_cancel_order_table">
				<thead>
					<tr>
						<th>Created At</th>
						<th>Po Buyer Old</th>
						<th>Document No</th>
						<th>Item Code</th>
						<th>Stock</th>
						<th>Reserved Qty</th>
						<th>Available Qty</th>
						<th>Warehouse Stock</th>
						<th>Po Buyer New</th>
						<th>LC Date</th>
                        <th>Barcode</th>
						<th>Qty</th>
                        <th>Created Allocation</th>
                        <th>Last tatus Movement</th>
						<th>Warehouse Allocation</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
@endsection

@section('page-js')
	<script type="text/javascript" src="{{ asset(elixir('js/accessories_report_allocation_cancel_order.js'))}}"></script>
@endsection
