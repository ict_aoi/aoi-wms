@if(isset($po_details))
<div class="table-responsive">
	<table class="table table-striped table-hover table-responsive">
		<thead>
			<tr>
				<th>No Invoice</th>
				<th>No Packing List (SJ)</th>
				<th>No Po Supplier</th>
				<th>Supplier Name</th>
				<th>Item Code</th>
				<th>Season</th>
				<th>Qty</th>
				<th>Action</th>
		</thead>
		<tbody>
			@foreach ($po_details as $key => $list)
				<tr id="{{ $list->po_detail_id }}" @if($list->flag_wms)style="background-color:#d9ffde"@endif>
					<td>{{ $list->kst_invoicevendor  }}</td>
					<td>{{ $list->no_packinglist  }}</td>
					<td>{{ $list->documentno  }}</td>
					<td>{{ $list->supplier  }}</td>
					<td>{{ $list->item  }}</td>
					<td>{{ $list->kst_season  }}</td>
					<td>{{ number_format($list->qty_upload,4)  }} ({{ $list->uomsymbol  }})</td>
					<td>
						<button class="btn btn-yellow-cancel btn-xs btn-choose" type="button" id="{{ $list->po_detail_id }}_Select"
							data-id="{{ $list->po_detail_id }}" data-name data-documentno="{{ $list->documentno }}" data-packinglist="{{ $list->no_packinglist }}" data-invoice="{{ $list->kst_invoicevendor }}" data-item="{{ $list->item }}" 
							data-exists="{{ $list->flag_wms }}" data-qty="{{ $list->qty_upload }}" data-uom="{{ $list->uomsymbol }}">Select
						</button>
						<button class="btn btn-default btn-xs btn-unchoose hidden" type="button" id="{{ $list->po_detail_id }}_UnSelect"
							data-id="{{ $list->po_detail_id }}" data-name data-documentno="{{ $list->documentno }}" data-packinglist="{{ $list->no_packinglist }}" data-invoice="{{ $list->kst_invoicevendor }}" data-item="{{ $list->item }}"
							data-exists="{{ $list->flag_wms }}" data-qty="{{ $list->qty_upload }}" data-uom="{{ $list->uomsymbol }}">Un-Select
						</button>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>
<div class="pull-right">
	{!! $po_details->appends(Request::except('page'))->render() !!}
</div>
@else
	<center><h1>Please Search Based On Criteria Above</h1></center>
@endif