
<!DOCTYPE html>
<html>
<head>
    <title>BARCODE :: REPRINT BARCODE SUPPLIER</title>
	<link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/print_barcode_fabric.css')) }}">
</head>
    <body>
		@foreach ($material_stocks as $key_1 => $material_stock)
			@php
				$barcode 	= $material_stock->barcode_supplier;
				$created_at = $material_stock->created_at;
				$is_recycle = $material_stock->getRecycle($material_stock->item_id);
			@endphp
			<div class="outer">
				<div class="title">
					<span style="@if(strlen($material_stock->document_no)>50) font-size: 10px @elseif(strlen($material_stock->document_no)>30 && strlen($material_stock->document_no)<50) font-size: 12px @else font-size: 25px @endif">{{ $material_stock->document_no }}</span>
				</div>
				<div class="isi">
					<div class="isi1">
						<span style="
							@if(strlen($material_stock->item_code)>40) 
								font-size: 12px
						 	@else
								font-size: 20px 
							@endif">{{ $material_stock->item_code.$is_recycle }}</span>
					</div>
					<div class="isi1" style="
						@if(strlen($material_stock->color)>100 && strlen($material_stock->color)<150) 
							font-size: 12px;line-height:0.3cm;
						@elseif(strlen($material_stock->color)>150 && strlen($material_stock->color)<175) 
							height:40px; 
						@elseif(strlen($material_stock->color)>175 && strlen($material_stock->color)<200) 
							height:75px  
						@endif">
						<span style="@if(strlen($material_stock->color)>19) 
							font-size: 7px;word-wrap: break-word;
							@else 
								font-size: 30px 
							@endif">{{ $material_stock->color }}</span>
					</div>
					<div class="isi1">
						<span style="@if(strlen($material_stock->batch_number)>15) font-size: 25px @else font-size: 30px @endif"> {{ $material_stock->batch_number }}</span>
					</div>
					
					<div class="isi2" style="161px;">
						<div class="block-kiri" style="justify-content: center; height: 45px;">
							<span style="font-size: 12px;word-wrap: break-word;">{{ $material_stock->supplier_name }}  </span>
						</div>
						<div class="block-kiri" style="justify-content: center; height: 45px;">
							<span style="font-size: 20px;word-wrap: break-word;">{{ sprintf("%0.4f",$material_stock->available_qty) }} {{ strtoupper($material_stock->uom) }}   </span>
						</div>
						<p style="font-size: 15px;word-wrap: break-word;">NO ROLL : <br/>
						<span style="@if(strlen($material_stock->nomor_roll)>=10 && strlen($material_stock->nomor_roll)<=17) 
							font-size: 25px;
							@elseif(strlen($material_stock->nomor_roll)>=4 && strlen($material_stock->nomor_roll)<10) 
								font-size: 35px 
							@else 
								font-size: 70px 
							@endif">{{ $material_stock->nomor_roll }}</span>
						</p>
					
					</div>
					<div class="isi3" style="232px;">
						<div class="block-kiri" style="height:30px;font-size:25px;">
								{{ $material_stock->jenis_po }}
						</div>
						<div class="block-kiri" style="height: 120px;">
							<img style="height: 2.5cm; width: 4.3cm; margin-top: 10px; margin-left: 2px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$barcode", 'C128') }}" alt="barcode"   />			
							<br/><span style="font-size: 12px;word-wrap: break-word;">{{ $barcode }}</span>
				
						</div>
						<div class="block-kiri">
							No. Invoice {{ $material_stock->no_invoice }}<br/>
							No. Packing List (SJ) {{ $material_stock->no_packing_list }}
						</div>
						<div class="block-kiri">
							Tgl. Receive {{ $created_at->format('d/M/Y H:i:s') }}
						</div>
						
					</div>
				</div>
						
			</div>

			<div class="outer_2">
				<div class="isi_2" style="width:5.3cm;"></div>
				<div class="isi1_2" style="width:5cm;"></div>
			</div>

			<div class="outer_2">
				<div class="isi_2" style="width:4cm;">
					<div class="block-kiri_2">
						<span style="font-size: 10px;word-wrap: break-word;">{{ $material_stock->document_no }}::{{ $material_stock->item_code }}::{{ $material_stock->batch_number }}</span>
						<span style="font-size: 10px;word-wrap: break-word;">{{ $material_stock->nomor_roll }}::{{ number_format($material_stock->available_qty, 2, '.', ',') }} ({{ strtoupper(trim($material_stock->uom)) }})</span>
					</div>
				</div>
				<div class="isi1_2" style="width:6.85cm;">
					<center><span style="font-size: 7px;word-wrap: break-word;">{{ $material_stock->color }}</span></center>
					<img style="height: 0.85cm; width: 5.3cm;	 margin-top: 5px; margin-left: 15px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$barcode", 'C128') }}" alt="barcode"   />			
					<center><span style="font-size: 12px;word-wrap: break-word;">{{ $barcode }}</span></center>
				
				</div>
			</div>
			<div class="outer_2">
				<div class="isi_2" style="width:4cm;">
					<div class="block-kiri_2">
						<span style="font-size: 10px;word-wrap: break-word;">{{ $material_stock->document_no }}::{{ $material_stock->item_code }}::{{ $material_stock->batch_number }}</span>
						<span style="font-size: 10px;word-wrap: break-word;">{{ $material_stock->nomor_roll }}::{{ number_format($material_stock->available_qty, 2, '.', ',') }} ({{ strtoupper(trim($material_stock->uom)) }})</span>
					</div>
				</div>
				<div class="isi1_2" style="width:6.85cm;">
					<center><span style="font-size: 7px;word-wrap: break-word;">{{ $material_stock->color }}</span></center>
					<img style="height: 0.85cm; width: 5.3cm;	 margin-top: 5px; margin-left: 15px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$barcode", 'C128') }}" alt="barcode"   />			
					<center><span style="font-size: 12px;word-wrap: break-word;">{{ $barcode }}</span></center>
				
				</div>
			</div>
		@endforeach
		
    </body>
</html>
