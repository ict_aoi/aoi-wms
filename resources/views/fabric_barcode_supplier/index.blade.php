@extends('layouts.app', ['active' => 'fabric_barcode_supplier'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Reprint Barcode Supplier</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li>Barcode</li>
            <li class="active">Reprint Barcode Supplier</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			@role(['admin-ict-fabric'])
				@include('form.select', [
					'field' 			=> 'warehouse',
					'label' 			=> 'Warehouse',
					'default'			=> auth::user()->warehouse,
					'label_col' 		=> 'col-sm-12',
					'form_col' 			=> 'col-sm-12',
					'options' 			=> [
						'1000001' 		=> 'Warehouse Fabric AOI 1',
						'1000011' 		=> 'Warehouse Fabric AOI 2',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_warehouse'
					]
				])
			@else 
				{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
			@endrole

			<div class="col-xs-6">
				<fieldset>
					<div class="form-group">
						<div class="col-xs-12">
							<div class="input-group">
								<input type="text" id="documentNoSearch"  class="form-control" placeholder="Please type po supplier here" autofocus="true"  autocomplete="off">
								<input type="text" id="itemCodeSearch"  class="form-control" placeholder="Please type item code here" autofocus="true"  autocomplete="off">
								<input type="text" id="suratJalanSearch"  class="form-control" placeholder="Please type no packing list / no delivery here" autofocus="true"  autocomplete="off">
								<input type="text" id="noInvoiceSearch"  class="form-control" placeholder="Please type no invoice here" autofocus="true"  autocomplete="off">
								
								<span class="input-group-btn">
									<button class="btn btn-yellow-cancel" type="button" id="ButtonSrc" style="height: 145px;">Search <span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
								</span>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
			<div class="col-xs-6">
				<div class="row" style="overflow-y: auto; height:150px;">
					<div class="table-responsive">
						<table class="table table-striped table-hover table-responsive">
							<thead>
								<tr>
									<th>No Packing List (SJ)</th>
									<th>No Invoice</th>
									<th>Po Supplier</th>
									<th>Item Code</th>
									<th>Total Qty Arrival</th>
								</tr>
							</thead>
							<tbody id="tbody-summary">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">&nbsp<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements"></div>
		</div>

		<div class="panel-body">
			<div id="item-lists">
				@include('fabric_barcode_supplier.data')
			</div>
			{!!
				Form::open([
					'role' 			=> 'form',
					'url' 			=> route('fabricBarcodeSupplier.store'),
					'method' 		=> 'post',
					'class' 		=> 'form-horizontal',
					#'target' 		=> '_blank',
					'id'			=> 'form'
				])
			!!}
			{!! Form::hidden('warehouse_id',auth::user()->warehouse,array('id' => 'warehouse_id')) !!}
			{!! Form::hidden('data_print','[]',array('id' => 'data_print')) !!}
			{!! Form::hidden('data_summary','[]',array('id' => 'data_summary')) !!}
			<div class="form-group text-right" style="margin-top: 10px;">
				<button type="submit" class="btn btn-blue-success col-xs-12 btn-lg">Print <i class="icon-printer position-left"></i></button>

			</div>
			{!! Form::close() !!}
		</div>
	</div>
	
	

	{!!
		Form::open([
			'role' 		=> 'form',
			'url' 		=> route('fabricBarcodeSupplier.barcode'),
			'method' 	=> 'post',
			'class' 	=> 'form-horizontal',
			'target' 	=> '_blank',
			'id'		=> 'getBarcode'
		])
	!!}
	
		{!! Form::hidden('warehouse_id',auth::user()->warehouse,array('id' => '_warehouse_id')) !!}
		{!! Form::hidden('list_barcodes','[]',array('id' => 'list_barcodes')) !!}
	{!! Form::close() !!}

@endsection

@section('page-js')
	@include('fabric_barcode_supplier._summary')
	<script src="{{ mix('js/fabric_barcode_supplier.js') }}"></script>
@endsection
