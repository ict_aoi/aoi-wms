@extends('dashboard.template')

@section('css')
    <style>
        body {
            margin-top: 10px;
        }
    </style>
@endsection

@section('body')
<div class="container-fluid" id="element">
    <div class="row justify-content-md-center">
        <br>
        <h1 align='center' style="color: white; text-shadow: 2px 0px 5px black;">
            PREPARATION FABRIC {{ $wh }} <br> PCD {{ \Carbon\Carbon::createFromTimestamp(strtotime($planning_date_now))->format('d F Y') }}
        </h1>
        
        <div class="row"
            <div class="col-lg col-md">
                <div class="card-body">
                    <canvas id='myChart'></canvas>
                </div>
            </div>
        </div>    

    </div>
</div>

@endsection

@section('js')

<script>
    var xtarget = [<?= $target ?>];
    var xonprogress = [<?= $progress ?>];
    var xinspect = [<?= $inspect ?>];
    var xsend = [<?= $sent ?>];

    var ctx = document.getElementById('myChart').getContext('2d');
    ctx.canvas.width = 1400;
    ctx.canvas.height= 655;

    var myChart = new Chart(ctx, {
        type: 'bar',
        mode: 'index',
        barPercentage: 1.0,
        aspectRatio: 1,
        plugins: [ChartDataLabels],
        data: {
            labels:
            [<?= $label ?>],
            datasets: [
            {
                type: 'line',
                label: 'Target',
                data: xtarget,
                backgroundColor: '#F1F1F1',
                borderColor: '#F1F1F1',
                pointBorderWidth: 1,
                pointStyle: 'circle',
                fill: false,
                showLine: false,
                tension: 0.1,
                datalabels: {
                    display: true,
                    anchor: 'end',
                    align: 'end',
                    offset: 5,
                    color: '#F1F1F1',
                    backgroundColor: 'black',
                    font: {
                        size: 17,
                        weight: 'bold'
                    }
                }
            }, {
                label: 'Send',
                barPercentage: 1.0,
                data: xsend,
                backgroundColor: [
                    '#06FF00'
                ],
                borderColor: 'black',
                borderWidth: 1
            }, {
                label: 'Onprogress',
                barPercentage: 1.0,
                data: xonprogress,
                backgroundColor: [
                    '#FF8E00'
                ],
                borderColor: 'black',
                borderWidth: 1
            }, {
                label: 'Inspect',
                barPercentage: 1.0,
                data: xinspect,
                backgroundColor: [
                    '#FFE400',
                ],
                borderColor: 'black',
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            layout: {
                padding: {
                  top: 30
                }
            },                       
            interaction: {
                intersect: false,
                mode: 'index'
            },
            plugins: {
                legend: {
                    position: 'bottom',
                    display: true,
                    labels: {
                        usePointStyle: true,
                        pointStyle: 'rectRounded',
                        color: 'white'
                    }
                },
                datalabels: {
                    display: function(context) {
                        return context.dataset.data[context.dataIndex] >= 100; // or >= 1 or ...
                    },
                    formatter: function(value, context) {
                        return context.dataset.data[context.dataIndex].toLocaleString();
                    },
                    color: 'black',
                    anchor: 'center',
                    align: 'center',
                    font: {
                        size: 14,
                        weight: 'bold'
                    }
                }
            },
            scales: {
                x: {
                    stacked: true,
                    ticks: {
                        color: 'white'
                      }            
                },
                y: {
                    stacked: true,
                    grid: {
                        color: 'white'
                    },
                    ticks: {
                        color: 'white'
                      }  
                }
            }
        }
    });

    $(document).ready(function(){
        setTimeout(function() {
            location.reload();
          }, 120000);
     });
</script>

@endsection

