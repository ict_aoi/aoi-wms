<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="{{ asset('css/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
    <title>WMS</title>

    <style>
        .bg {
            /* The image used */
            background-image: url("{{ asset('images/login_cover.png') }}");


            /* Full height */
            width:100%; 
            height:1100px;

            /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        
          }
    </style>

  </head>
  <body class="bg-white bg" scroll="no" style="overflow: hidden">
    <div class="container">
      <br> <br>

        <div class="row">
            <div class="col-sm-6">

            <h4 align='center'>AOI 1</h4>
              <div class="card text-white bg-dark mb-3">
                <div class="card-body">
                  <h5 class="card-title">FABRIC PREPARATION STATUS AOI 1</h5>
                  <p class="card-text">Dashboard preparation fabric untuk AOI 1 perhari ini.</p>
                    <div class="d-grid gap-2">
                        <a href="{{ route('prepare1') }}" class="btn btn-primary" target="_blank">Buka</a>
                    </div>
                </div>
              </div>

            </div>

            <div class="col-sm-6">

            <h4 align='center'>AOI 2</h4>
              <div class="card text-white bg-dark mb-3">
                <div class="card-body">
                  <h5 class="card-title">FABRIC PREPARATION STATUS AOI 2</h5>
                  <p class="card-text">Dashboard preparation fabric untuk AOI 2 perhari ini.</p>
                    <div class="d-grid gap-2">
                        <a href="{{ route('prepare2') }}" class="btn btn-primary" target="_blank">Buka</a>
                    </div>
                </div>
              </div>

            </div>
        </div> {{--end row--}}

        <div class="row">
            <div class="col-sm-6">

              <div class="card text-white bg-success mb-3">
                <div class="card-body">
                  <h5 class="card-title">FABRIC STOCK STATUS AOI 1</h5>
                  <p class="card-text">Dashboard stock fabric untuk AOI 1 perhari ini.</p>
                    <div class="d-grid gap-2">
                        <a href="{{ route('statusfab1') }}" class="btn btn-primary" target="_blank">Buka</a>
                    </div>
                </div>
              </div>

            </div>

            <div class="col-sm-6">

              <div class="card text-white bg-success mb-3">
                <div class="card-body">
                  <h5 class="card-title">FABRIC STOCK STATUS AOI 2</h5>
                  <p class="card-text">Dashboard stock fabric untuk AOI 2 perhari ini.</p>
                    <div class="d-grid gap-2">
                        <a href="{{ route('statusfab2') }}" class="btn btn-primary" target="_blank">Buka</a>
                    </div>
                </div>
              </div>

            </div>
        </div> {{--end row--}}

        <div class="row"> {{-- awal row--}}
            <div class="col-sm-6">

              <div class="card text-white bg-secondary mb-3">
                <div class="card-body">
                  <h5 class="card-title">ACCESSORIES RECEIVING STATUS AOI 1</h5>
                  <p class="card-text">Dashboard receiving accessories untuk AOI 1, hari ini dan 5 hari sebelumnya.</p>
                    <div class="d-grid gap-2">
                        <a href="{{ route('rcv1') }}" class="btn btn-primary" target="_blank">Buka</a>
                    </div>
                </div>
              </div>

            </div>

            <div class="col-sm-6">

              <div class="card text-white bg-secondary mb-3">
                <div class="card-body">
                  <h5 class="card-title">ACCESSORIES RECEIVING STATUS AOI 2</h5>
                  <p class="card-text">Dashboard receiving accessories untuk AOI 2, hari ini dan 5 hari sebelumnya.</p>
                    <div class="d-grid gap-2">
                        <a href="{{ route('rcv2') }}" class="btn btn-primary" target="_blank">Buka</a>
                    </div>
                </div>
              </div>

            </div>
        </div> {{--end row--}}

    </div>

    <script src="{{ asset('js/bootstrap/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap/chart.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap/jquery_v360.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap/datalabelschart.min.js') }}" type="text/javascript"></script>
  </body>
</html>
