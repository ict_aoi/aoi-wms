<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="{{ asset('css/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
    <title>DASHBOARD WMS | WAREHOUSE MANAGEMENT SYSTEM</title>
    @yield('css')

  </head>
  <body style="background-color: #000000;">
      
    @yield('body')

    <script src="{{ asset('js/bootstrap/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap/chart.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap/jquery_v360.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap/datalabelschart.min.js') }}" type="text/javascript"></script>
    @yield('js')
  </body>
</html>
