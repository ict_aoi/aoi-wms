@extends('dashboard.template')

@section('css')
    <style>
        body {
            margin-top: 10px;
        }
    </style>
@endsection

@section('body')
<div class="container-fluid" id="element">
  <h1 align='center' style="color: white; text-shadow: 2px 0px 5px black;"> FABRIC STOCK STATUS {{ $wh }} </h1>

  <div class="row justify-content-md-center">
      
      <div class="col-sm-6">
        <div class="card align-items-center" style="background-color: #323232;">
          <div class="card-body">
            <canvas id="doughnutRunSlow" width="500" height="470"></canvas>
            <br>

            <table class="table table-sm table-dark" style="text-align:center; vertical-align:middle">
              <thead>
                <tr>
                  <th scope="col" colspan="3"> TOTAL CAPACITY </th>
                  <th scope="col"><?= number_format($max) ?></th>
                  <th scope="col">Yards</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row" rowspan="2">USED</th>
                  <td>Slow Moving</td>
                  <td><?= number_format($newSlowMoving, 4, '.', ',')?></td>
                  <td rowspan="2"><?= number_format($usedLeft, 4, '.', ',')  ?></td>
                  <td rowspan="2">Yards</td>
                </tr>
                <tr>
                  <td>Running</td>
                  <td><?= number_format($newRunning, 4, '.', ',') ?></td>
                </tr>
                <tfoot>
                  <tr>
                    <th scope="col" colspan="3"> FREE CAPACITY </th>
                    <th scope="col">
                      <?= number_format($newFreeCapacity) ?>
                    </th>
                    <th scope="col">Yards</th>
                  </tr>
                </tfoot>
              </tbody>
            </table>

          </div>
        </div> <br>
      </div>

      <div class="col-sm-6">
        <div class="card align-items-center" style="background-color: #323232;">
          <div class="card-body">
            <canvas id="doughnutAllo" width="500" height="470"></canvas>
            <br>

            <table class="table table-sm table-dark" style="text-align:center; vertical-align:middle">
              <thead>
                <tr>
                  <th scope="col" colspan="3"> TOTAL CAPACITY </th>
                  <th scope="col"><?= number_format($max) ?></th>
                  <th scope="col">Yards</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row" rowspan="2">USED</th>
                  <td>Allocated</td>
                  <td><?= number_format($allocated, 4, '.', ',') ?></td>
                  <td rowspan="2"><?=  number_format($all, 4, '.', ',')  ?></td>
                  <td rowspan="2">Yards</td>
                </tr>
                <tr>
                  <td>Unallocated</td>
                  <td><?=  number_format($unallocatedAll, 4, '.', ',')  ?></td>
                </tr>
                <tfoot>
                  <tr>
                    <th scope="col" colspan="3"> FREE CAPACITY </th>
                    <th scope="col">
                      <?= number_format($free) ?>
                    </th>
                    <th scope="col">Yards</th>
                  </tr>
                </tfoot>
              </tbody>
            </table>

          </div>
        </div>
      </div>    
         
  </div>

</div>
@endsection

@section('js')

<script>
  
  var FreeCapacity = [<?= $newFreeCapacity ?>];
  var SlowMoving   = [<?= $newSlowMoving ?>];
  var Running      = [<?= $newRunning ?>];
  var usedLeft     = [<?= $usedLeft?>];
  if(FreeCapacity < 0){
    FreeCapacity = 0;
  }

  
  var Allocated    = [<?= $allocated ?>];
  var Unallocated  = [<?= $unallocatedAll ?>];

  var donnut1 = document.getElementById("doughnutRunSlow");
  var donatRunSlow = new Chart(donnut1, {
    type: 'doughnut',
    plugins: [ChartDataLabels],
    data: {
      labels: ["Running", "Slow Moving", "Free Capacity"],
      datasets: [{
        data: [Running, SlowMoving, FreeCapacity],
        backgroundColor: ['rgba(65, 129, 223, 1)', 'rgba(225, 84, 157, 1)', 'rgba(84, 85, 84, 1)'],
        borderWidth: 0
      }]
    },
    options: {
      responsive: false,
      interaction: {
        intersect: false,
        mode: 'index'
      },
      plugins: {
        legend: {
          position: 'bottom',
          display: true,
          labels: {
            usePointStyle: true,
            pointStyle: 'rectRounded',
            color: 'white'
          }
        },
        datalabels: {
          font: {
              size: 20
          },
          formatter: (value, ctx) => {
            if(value > 0) {
              let sum = 0;
              let dataArr = ctx.chart.data.datasets[0].data;
              // console.log(ctx.dataset.data); 
              dataArr.map(data => {
                  sum += Number(data);
                 
              });
              // console.log(value);
              let percentage = ((value / sum)  * 100).toFixed(2) + '%';
              return percentage;
            } else {
              return '';
            }
          },
          color: 'white',
          backgroundColor: 'black',
          font: {
            size: 20,
            weight: 'bold'
          },
        }
      }
    }
  });

  var donnut2 = document.getElementById("doughnutAllo");
  var donatAllo = new Chart(donnut2, {
    type: 'doughnut',
    plugins: [ChartDataLabels],
    data: {
      labels: ["Allocated", "Unallocated", "Free Capacity"],
      datasets: [{
        data: [Allocated, Unallocated, FreeCapacity],
        backgroundColor: ['rgba(65, 129, 223, 1)', 'rgba(225, 84, 157, 1)', 'rgba(84, 85, 84, 1)'],
        borderWidth: 0
      }]
    },
    options: {
      responsive: false,
      interaction: {
        intersect: false,
        mode: 'index'
      },
      plugins: {
        legend: {
          position: 'bottom',
          display: true,
          labels: {
            usePointStyle: true,
            pointStyle: 'rectRounded',
            color: 'white'
          }
        },
        datalabels: {
          font: {
              size: 20
          },
          formatter: (value, ctx) => {
            if(value > 0) {
              let sum = 0;
              let dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                  sum += Number(data);
              });

              let percentage = (value * 100 / sum).toFixed(2) + '%';
              return percentage;
            } else {
              return '';
            }
          },
          color: 'white',
          backgroundColor: 'black',
          font: {
            size: 20,
            weight: 'bold'
          },
        }
      }
    }
  });


  
    $(document).ready(function(){
        setTimeout(function() {
            location.reload();
          }, 60000);
     });
  
</script>
  
@endsection