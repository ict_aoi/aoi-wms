@extends('layouts.app', ['active' => 'criteria'])

@section('content')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title" style="padding-top:0px;">
				<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">CRITERIA</span></h4>

				<ul class="breadcrumb breadcrumb-caret position-left">
					<li><a href="{{ route('dashboard') }}">Master Data</a></li>
					<li class="active">Criteria</li>
					<a href="{{ route('criteria.update') }}" id="url_criteriapdate" class="hidden"></a>
					
				</ul>
				<a class="heading-elements-toggle"><i class="icon-more"></i></a>
			</div>

			<div class="heading-elements">
				<div class="heading-btn-group">
					<button type="button" class="btn btn-info" data-toggle="modal" data-target="#insertModal"><i class="icon-add position-left"></i> ADD NEW</button>
        	</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-basic']) !!}
			</div>
		</div>
@endsection

@section('content-modal')
	@include('criteria._insert_modal')
	@include('criteria._update_modal')
@endsection

@section('content-js')
	{!! $html->scripts() !!}
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/criteria.js'))}}"></script>
@endsection
