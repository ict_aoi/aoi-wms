
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($cancel))
                <li><a href="#" onclick="cancel('{!! $cancel !!}')"><i class=" icon-x"></i> Cancel</a></li>
            @endif
        </ul>
    </li>
</ul>
