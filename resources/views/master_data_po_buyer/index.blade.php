@extends('layouts.app', ['active' => 'master_data_po_buyer'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Data Po Buyer</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data</li>
				<li class="active">Po Buyer</li>
			</ul>

	
			<ul class="breadcrumb-elements">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
						<i class="icon-three-bars position-left"></i>
						Actions
						<span class="caret"></span>
					</a>
					
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a data-toggle="modal" data-target="#erpItemPicklistModal"><i class="icon-sync pull-right"></i> Sync</a></li>
						<li><a href="{{ route('masterDataPoBuyer.cancelBulk') }}"><i class="icon-x pull-right"></i> Cancel Bulk</a></li>
						<li><a href="{{ route('masterDataPoBuyer.uploadPSD') }}"><i class="icon-x pull-right"></i> Upload PSD</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover " id="master_data_po_buyer_table">
					<thead>
						<tr>
							<th>Lc Date</th>
							<th>Statistical Date</th>
							<th>Promise Date</th>
							<th>Po Buyer</th>
							<th>Brand</th>
							<th>Type Stock</th>
							<th>Cancel Date</th>
							<th>Cancel Reason</th>
							<th>Cancel By</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>

	{!! Form::hidden('page','index', array('id' => 'page')) !!}
@endsection

@section('page-js')
	<script src="{{ mix('js/master_data_po_buyer.js') }}"></script>
@endsection
