<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8">
</head>
<body>
    <h3>Daftar Outstanding Fabric Reject di WMS</h3>

    <div>
        <p style="margin: 1em 0;"><b>Mohon perhantian dan kerja samanya</b><br/>
        <br>Dibawah ini adalah material yang belum di move ke locator reject di wms <br/>
        <b>(Total yang belum terkonfirmasi {{ $total_outstanding }} baris.</b>
        </p>
        
        <p style="margin: 1em 0 1em 15px;">
            <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nomor Roll</th>
						<th>Po Supplier</th>
						<th>Supplier Name</th>
                        <th>Item Code</th>
						<th>Item Desc</th>
                        <th>Batch Number</th>
						<th>Color</th>
						<th>No Packing List</th>
						<th>Invoice</th>
                        <th>Warehouse</th>
                        <th>Qty</th>
                        <th>Remark</th>
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $key => $datum)
                        @if($key < 100)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $datum->nomor_roll }}</td>
                                <td>{{ $datum->document_no }}</td>
                                <td>{{ $datum->supplier_name }}</td>
                                <td>{{ $datum->item_code }}</td>
                                <td>{{ $datum->item_desc }}</td>
                                <td>{{ $datum->batch_number }}</td>
                                <td>{{ $datum->color }}</td>
                                <td>{{ $datum->no_packing_list }}</td>
                                <td>{{ $datum->no_invoice}}</td>
                                <td>{{ $datum->warehouse_name}}</td>
                                <td>{{ $datum->qty_reject }}</td>
                                <td>{{ $datum->remark }}</td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </p>
    </div>

    <div style="font-size: 15px; margin-top: 10px">
        <h2> Silahkan cek attachement untuk melihat keseluruhan data</h2>
        Email ini dikirimkan otomatis melalui aplikasi WMS. diharapkan untuk tidak membalas email ini.
        <br/>
        <b>Jangan Lupa untuk melakukan scan reject di WMS supaya material berpindah ke locator reject</b> 
        <p style="margin: 1em 0;" style="margin-top: 5px">Demikan yang dapat diinfokan, <br/>
        Terima kasih,<br/>
        WMS - Autobot</p>
    </div>
    <br>

    <hr>

</body>
</html>