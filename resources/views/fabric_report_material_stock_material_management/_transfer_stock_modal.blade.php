
<div id="transFerStockModal" class="modal fade" data-backdrop="static" data-keyboard="false"  style="color:black !important">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		{{ 
			Form::open(['method' => 'POST'
				,'id' => 'transFerStockForm'
				,'class' => 'form-horizontal'
				,'url' => route('accessoriesReportMaterialStock.storeTransferStock')
			]) 
		}}
			<div class="modal-header bg-blue">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Transfer stock for <span id="transfer_header_stock"></span></h5>

			</div>

			<div class="modal-body">
				@include('form.text', [
						'label_col' 	=> 'col-lg-3 col-md-3 col-sm-12',
						'form_col' 		=> 'col-lg-9 col-md-9 col-sm-12',
						'field' 		=> 'uom',
						'label' 		=> 'Uom',
						'attributes' 	=> [
							'id' 		=> 'transfer_uom',
							'readonly' 	=> 'readonly'
						]
					])

					@include('form.text', [
						'label_col' 	=> 'col-lg-3 col-md-3 col-sm-12',
						'form_col' 		=> 'col-lg-9 col-md-9 col-sm-12',
						'field' 		=> 'qty_available',
						'label' 		=> 'Qty available',
						'attributes' 	=> [
							'id' 		=> 'tranfer_qty_available',
							'readonly' 	=> 'readonly'
						]
					])

					@include('form.text', [
						'label_col' 	=> 'col-lg-3 col-md-3 col-sm-12',
						'form_col' 		=> 'col-lg-9 col-md-9 col-sm-12',
						'field' 		=> 'qty_transfer',
						'label' 		=> 'Qty transfer',
						'placeholder' 	=> 'Please input qty transfer here',
						'attributes' 	=> [
							'id' 		=> 'qty_tranfer'
						],
						'help' => 'Qty transfer must be less than qty available'
					])
					
					{!! Form::hidden('id','', array('id' => 'transfer_id')) !!}
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close  <span class="glyphicon glyphicon-remove"></span></button>
				<button type="submit" class="btn btn-blue-success">Save <i class="icon-floppy-disk position-left"></i></button>
			</div>
		</div>
		{{ Form::close() }}
	</div>
</div>
