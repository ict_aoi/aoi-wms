@extends('layouts.app', ['active' => 'fabric_report_material_stock_material_management'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Material Stock Material Management</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li class="active">Material Stock Stock Material Management</li>
			</ul>
			<ul class="breadcrumb-elements">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
						<i class="icon-three-bars position-left"></i>
						Actions
						<span class="caret"></span>
					</a>
					
					<ul class="dropdown-menu dropdown-menu-right">
					    <li><a href="{{ route('fabricReportMaterialStockMaterialManagement.changeTypeBulk') }}""><i class="icon-copy3 pull-right"></i> Change Type Bulk</a></li>
						<li><a href="{{ route('fabricReportMaterialStockMaterialManagement.deleteBulk') }}"><i class="icon-x pull-right"></i> Delete Bulk</a></li>
                        <li><a href="{{ route('fabricReportMaterialStockMaterialManagement.editBulk') }}"><i class="icon-pencil6 pull-right"></i> Edit Bulk</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>

		<div class="panel-body">
			@include('form.select', [
				'field' => 'warehouse',
				'label' => 'Warehouse',
				'default' => $warehouse_id,
				'label_col' => 'col-md-2 col-lg-2 col-sm-12',
				'form_col' => 'col-md-10 col-lg-10 col-sm-12',
				'options' => [
					'' => '-- Select Warehouse --',
					'1000001' => 'Warehouse Fabric AOI 1',
					'1000011' => 'Warehouse Fabric AOI 2',
				],
				'class' => 'select-search',
				'attributes' => [
					'id' => 'select_warehouse'
				]
			])

			@include('form.select', [
				'field' => 'type_stock',
				'label' => 'Type Stock',
				'label_col' => 'col-md-2 col-lg-2 col-sm-12',
				'form_col' => 'col-md-10 col-lg-10 col-sm-12',
				'options' => [
					'' => '-- Select Type Stock --',
					'-' => '-',
					'1' => 'SLT',
					'2' => 'REGULER',
					'3' => 'PR/SR',
					'4' => 'MTFC',
					'5' => 'NB',
				],
				'class' => 'select-search',
				'attributes' => [
					'id' => 'select_type_stock'
				]
			])
			
			{!!
				Form::open(array(
					'class' => 'form-horizontal',
					'role' => 'form',
					'url' => route('fabricReportMaterialStockMaterialManagement.export'),
					'method' => 'get',
					'target' => '_blank'		
				))
			!!}
            {!! Form::hidden('warehouse_id',$warehouse_id , array('id' => '_warehouse_stock_id')) !!}
			<button type="submit" class="btn btn-default col-xs-12">Export Stock All <i class="icon-file-excel position-left"></i></button>
			{!! Form::close() !!}

			{!!
				Form::open(array(
						'class' => 'heading-form',
						'role' => 'form',
						'url' => route('fabricReportMaterialStockMaterialManagement.exportAllocation'),
						'method' => 'get',
						'target' => '_blank'		
				))
			!!}
            {!! Form::hidden('warehouse_id',$warehouse_id , array('id' => '_warehouse_allocation_id')) !!}
			<button type="submit" class="btn btn-default col-xs-12">Export Allocation All<i class="icon-file-excel position-left"></i></button>
			{!! Form::close() !!}
		</div>
	</div>

	<div class="navbar navbar-default navbar-xs navbar-component no-border-radius-top">
		<ul class="nav navbar-nav visible-xs-block">
			<li class="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
		</ul>

		<div class="navbar-collapse collapse" id="navbar-filter">
			<ul class="nav navbar-nav">
				<li class="@if($active_tab == 'active') active @endif"><a href="#active" data-toggle="tab" onclick="changeTab('active')">Active <i class="icon-stack3 position-left"></i></a></li>
				<li class="@if($active_tab == 'inactive') active @endif"><a href="#inactive" data-toggle="tab" onclick="changeTab('inactive')">Inactive <i class="icon-stack2 position-left"></i></a></li>
			</ul>
		</div>
	</div>

	<div class="tabbable">
		<div class="tab-content">
			<div class="tab-pane fade in active" id="active">
				<div class="panel panel-default border-grey">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table datatable-basic table-striped table-hover " id="activeTable">
								<thead>
									<tr>
										<th>ID</th>
										<th>supplier_code</th>
										<th>item_code</th>
										<th>po_buyer</th>
										<th>warehouse_id</th>
										<th>available_qty</th>
										<th>Warehouse Stock</th>
										<th>Source</th>
										<th>Supplier</th>
										<th>Document No</th>
										<th>Type Stock</th>
										<th>Item</th>
										<th>Uom</th>
										<th>Stock</th>
										<th>Reserved</th>
										<th>Available</th>
										<th>Action</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div class="tab-pane fade" id="inactive">
				<div class="panel panel-default border-grey">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table datatable-basic table-striped table-hover table-responsive" id="inactiveTable">
								<thead>
									<tr>
										<th>ID</th>
										<th>supplier_code</th>
										<th>item_code</th>
										<th>po_buyer</th>
										<th>warehouse_id</th>
										<th>available_qty</th>
										<th>Warehouse Stock</th>
										<th>Source</th>
										<th>Supplier</th>
										<th>Document No</th>
										<th>Type Stock</th>
										<th>Item</th>
										<th>Uom</th>
										<th>Stock</th>
										<th>Reserved</th>
										<th>Available</th>
										<th>Action</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	{!! Form::hidden('active_tab',$active_tab , array('id' => 'active_tab')) !!}
@endsection

@section('page-modal')
	@include('fabric_report_material_stock_material_management._modal')
	@include('fabric_report_material_stock_material_management._change_type_modal')
	@include('fabric_report_material_stock_material_management._transfer_stock_modal')
	@include('fabric_report_material_stock_material_management._update_stock_modal')
	@include('fabric_report_material_stock_material_management._delete_stock_modal')
@endsection

@section('page-js')
    <script type="text/javascript" src="{{ asset(elixir('js/fabric_report_material_stock_material_management.js'))}}"></script>
@endsection
