@extends('layouts.app', ['active' => 'master_data_permission'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Data Permission</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data</li>
				<li>User Management</li>
				<li><a href="{{ route('masterDataPermission.index') }}" id="url_master_data_permission">Permission</a></li>
				<li class="active">Create</li>
			</ul>

			
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			{{ 
				Form::open([
				'method' 	=> 'POST',
				'id' 		=> 'form',
				'class' 	=> 'form-horizontal',
				'url' 		=> route('masterDataPermission.store')]) 
			}}
				
			@include('form.text', [
				'field' 		=> 'name',
				'label' 		=> 'Name',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'placeholder' 	=> 'Please insert permission name here',
				'attributes' 	=> [
					'id' 		=> 'name'
				]
			])
			@include('form.textarea', [
				'field' 		=> 'description',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'label' 		=> 'Description',
				'placeholder' 	=> 'Please insert description name here',
			])

			<button type="submit" class="btn btn-blue-success col-xs-12" style="margin-top: 15px">Save <i class="icon-floppy-disk position-right"></i></button>
		{{ Form::close() }}
		</div>
	</div>
	{!! Form::hidden('page','create', array('id' => 'page')) !!}
@endsection

@section('page-js')
	<script src="{{ mix('js/master_data_permission.js') }}"></script>
@endsection
