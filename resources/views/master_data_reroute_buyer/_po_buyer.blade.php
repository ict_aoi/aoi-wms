<table class="table">
    <thead>
        <tr>
            <th>PO BUYER</th>
            <th>SELECT</th>
        </tr>
    </thead>
    
    <tbody>
        @foreach ($lists as $key => $list)
            <tr>
                <td>
                    {{ $list->po_buyer }}
                </td>
                <td>
                    <button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" type="button" data-buyer="{{ $list->po_buyer }}" >Select</button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
    
    