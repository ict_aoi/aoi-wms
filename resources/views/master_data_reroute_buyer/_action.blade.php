
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($refresh))
                <li><a href="#" onclick="refresh('{!! $refresh !!}')"><i class="icon-database-refresh"></i> Refersh</a></li>
            @endif
            
        </ul>
    </li>
</ul>
