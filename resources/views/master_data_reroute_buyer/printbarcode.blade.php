<!DOCTYPE html>
<html>
<head>
    <title>BARCODE :: PREPERATION</title>
    <link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/printbarcodewebpo.css')) }}">

</head>
    <body>
		@php $flag = 0 ; @endphp
		@foreach ($items as $key => $value)
			@for ($i = 0; $i < $value->total_carton ; $i++)
				@if (($flag+1) % 3 === 0)
					<p style="page-break-after: always;">&nbsp;</p>
				@endif

				<div class="outer">
					<div class="title">BARCODE PREPERATION</div>
					
					<div class="isi">
						<div class="isi1">
							NO CARTON : <span style="font-size: 24px">{{ $i+1 }}</span> / {{ $value->total_carton }}
						</div>
						<div class="isi1">
							<span style="@if(strlen($value->item_code)>40) font-size: 12px @endif">{{ $value->item_code }}</span>
						</div>
						<div class="isi1">
							<span style="@if(strlen($value->item_desc)>40) font-size: 12px @endif">{{ $value->item_desc }}</span>
						</div>

						<div class="isi2">
							<div class="block-kiri" style="justify-content: center; height: 45px;">
								@php 
									$conversion = $value->conversion($value->item_code,$value->category,$value->uom_conversion);
									if($conversion){
										$multiplyrate = $conversion->multiplyrate;
										$uom_reconversion = $conversion->uom_from;
									}else{
										$multiplyrate = 1;
										$uom_reconversion = $value->uom_conversion;
									}
									$qty_reconversion = $multiplyrate * $value->qty_conversion; 
								
								@endphp
								<span style="font-size: 12px; word-wrap: break-word;">{{ number_format($value->qty_conversion, 2, '.', ',') }} {{ strtoupper($value->uom_conversion) }} <br/> ({{ number_format($qty_reconversion, 2, '.', ',') }} {{ strtoupper($uom_reconversion) }})</span>
							</div>
							<!--<div class="block-kiri" style="height:35px;line-height: 35px">
								<span style="font-size: px; word-wrap: break-word;">PO BUYER: {{ $value->po_buyer }}  </span>
							</div-->
							<div class="block-kiri" style="height: 39px;">
								<span style="font-size: 10px; word-wrap: break-word;">
									{{ $value->document_no }}
									<br/>{{ $value->supplier_name }}	
								</span>
							</div>
							<div class="block-kiri" style="margin-top: 7px">
								<span style="font-size: 9px; word-wrap: break-word;">{{ $value->job_order }}<br/>{{ $value->style }}</span>
							</div>
						</div>
						<div class="isi3">
							<div class="block-kiri" style="height: 120px;">
								@if ($value->total_carton == 1)
									<img style="height: 2.5cm; width: 4.3cm;	 margin-top: 10px; margin-left: 2px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$value->barcode", 'C128') }}" alt="barcode"   />			
									<br/> 
									<span style="font: 10px monospace;" text-anchor="middle" x="65" y="62">{{ $value->barcode }}</span>
								@else
									@php
										$_barcode = $value->barcode.'-'.($i+1);
									@endphp

									<img style="height: 2.5cm; width: 4.3cm;	 margin-top: 10px; margin-left: 2px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$_barcode", 'C128') }}" alt="barcode"   />			
									<br/> 
									<span style="font: 10px monospace;" text-anchor="middle" x="65" y="62">{{ $_barcode }}</span>
								@endif
							</div>
							<div class="block-kiri" style="margin-top: 6px;" >
								PO BUYER: <span style="font-size: 16px">
								{{ $value->po_buyer }} @if($value->is_additional)*@endif
								</span>
							</div>
						</div>
					</div>		
				</div>
				@php $flag++;@endphp
				
			@endfor
		@endforeach
		
    </body>
</html>