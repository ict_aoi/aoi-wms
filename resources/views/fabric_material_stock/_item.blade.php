<script type="x-tmpl-mustache" id="material-stock-fabric-table">
	{% #item %}
		<tr {% #is_error %} style="background-color:#fab1b1" {% /is_error %} {% #is_warning %} style="background-color:#fffed9" {% /is_warning %}>
			<td>
				{% no %}
			</td>
			<td>
				{% type_stock %}
			</td>
			<td>
				{% supplier_name %}
			</td>
			<td>
				{% document_no %}
			</td>
			<td>
				{% item_code %} ({% category %})
			</td>
			<td>
				{% uom %}
			</td>
			<td>
				{% roll_number %}
			</td>
			<td>
				{% batch_number %}
			</td>
			<td>
				{% actual_width %}
			</td>
			<td>
				{% actual_lot %}
			</td>
			<td>
				{% qty %}
			</td>
			<td>
				{% #is_error %} {% source %} {% /is_error %}
				{% ^is_error %}
					<textarea class="form-control input-sm input-source-edit" rows="5" style="resize:none" id="sourceInput_{% _id %}" data-id="{% _id %}" placeholder="Please insert note here">{% source %}</textarea>
				{% /is_error %}
			</td>
			<td>
				{% remark %}
			</td>
			<td>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-icon-anim btn-circle btn-delete-item"><i class="fa icon-trash"></i></button>
			</td>
		</tr>
	{%/item%}

	<tr>
		<td>#</td>
		<td>
			<select id="type_stock" name="type_stock" class="form-control">
				<option value="0"> </option>
				<option value="1">SLT</option>
				<option value="2">REGULER</option>
				<option value="3">PR/SR</option>
				<option value="4">MTFC</option>
			</select>
			<span class="help-block text-info">Optional</span>
		</td>
		<td>
			<div style="width:100%">
				@include('form.picklist', [
					'field' 		=> 'supplier_fabric',
					'name' 			=> 'supplier_fabric',
					'placeholder' 	=> 'Select supplier here'
				])
				<span class="help-block text-info">*Optional</span>
				<input type="hidden" id="c_bpartner_id" name="c_bpartner_id""></input>
				<input type="hidden" id="supplier_code" name="supplier_code""></input>
			</div>
		</td>
		<td>
			FREE STOCK
		</td>
		<td>
			<div style="width:100%">
				@include('fabric_material_stock.item_picklist', [
					'field' 		=> 'item_out_fabric',
					'name' 			=> 'item_out_fabric',
					'placeholder' 	=> 'Select item code here'
				])
				<span class="help-block text-danger">*Required</span>
			</div>
		</td>
		<td>
			<div style="width:100%">
				<input type="hidden" id="item_desc" name="item_desc"></input>
				<input type="hidden" id="category" name="category"></input>
				<input type="hidden" id="color" name="category"></input>
				<input type="hidden" id="upc" name="category"></input>
				<input type="text" id="uom" name="uom" class="form-control input-new" readonly="readonly"></input>
			</div>
		</td>
		<td>
			<input type="text" id="nomor_roll" name="nomor_roll" class="form-control input-new"></input>
		</td>
		<td>
			<input type="text" id="batch_number" name="batch_number" class="form-control input-new"></input>
		</td>
		</td>
		<td>
			<input type="text" id="actual_width" name="actual_width" class="form-control input-new input-number"></input>
		</td>
		<td>
			<input type="text" id="actual_lot" name="actual_lot" class="form-control input-new"></input>
		</td>
		<td>
			<input type="text" id="qty" name="qty" class="form-control input-new input-number"></input>
			<span class="help-block text-danger">*Required</span>
		</td>
		<td>
			<textarea class="form-control source" style="resize:none" rows="5"  id="source"></textarea>
		</td>
		<td></td>
		<td>
			<button type="button" class="btn btn-yellow-cancel btn-lg" id="AddItemButton" ><i class="icon-add"></i></button>
		</td>

	</tr>
</script>
