<table class="table datatable-basic table-striped table-hover">
	<thead>
	  <tr>
		<th>Item Code</th>
		<th>Item Desc</th>
		<th>Composition</th>
		<th>Color</th>
		<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td> {{ $list->item_code }}</td>
				<td> {{ $list->item_desc }}</td>
				<td> {{ $list->composition }}</td>
				<td> {{ $list->color }}</td>
				<td>
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose"
						type="button" 
						data-id="{{ $list->item_id }}" 
						data-code="{{ $list->item_code }}" 
						data-name="{{ $list->item_desc }}" 
						data-color="{{ $list->color }}"
						data-upc="{{ $list->upc }}"
						>Select</button>
				</td>
			</tr>

		@endforeach
	</tbody>
</table>
{!! $lists->appends(Request::except('page'))->render() !!}
