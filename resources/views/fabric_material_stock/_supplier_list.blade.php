<table class="table datatable-basic table-striped table-hover">
	<thead>
	  <tr>
		<th>Supplier Code</th>
		<th>Supplier Name</th>
		<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>{{ $list->supplier_code }}</td>
				<td>{{ $list->supplier_name }}</td>
				<td>
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose"
						type="button" data-id="null" 
						data-name="null" 
						data-supplier="{{ $list->supplier_name }}" 
						data-bpartner="{{ $list->c_bpartner_id }}"
						data-suppliercode="{{$list->supplier_code}}"
					>Select</button>
				</td>
			</tr>

		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
