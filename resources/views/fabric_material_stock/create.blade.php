@extends('layouts.app', ['active' => 'fabric_material_stock'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Stock</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="{{ route('fabricMaterialStock.index') }}">Material Stock</a></li>
			<li class="active">Create</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	@role(['admin-ict-fabric'])
		<div class="panel panel-default border-grey">
			<div class="panel-body">
				@include('form.select', [
					'field' 			=> 'warehouse',
					'label' 			=> 'Warehouse',
					'default'			=> auth::user()->warehouse,
					'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
					'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
					'options' 			=> [
						'1000001' 		=> 'Warehouse Fabric AOI 1',
						'1000011' 		=> 'Warehouse Fabric AOI 2',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_warehouse'
					]
				])
			</div>
		</div>
	@else 
		{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
	@endrole

	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				{!!
					Form::open([
						'role' 			=> 'form',
						'class'			=>'heading-form',
						'url' 			=> route('fabricMaterialStock.import'),
						'method' 		=> 'POST',
						'id' 			=> 'upload_file_allocation',
						'enctype' 		=> 'multipart/form-data'
					])
				!!}
					<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
					<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
				{!! Form::close() !!}
				<div class="heading-btn">
					@role(['admin-ict-fabric'])
						{!!
							Form::open([
								'role' 			=> 'form',
								'class'			=>'heading-form',
								'url' 			=> route('fabricMaterialStock.importFormSplitRoll'),
								'method' 		=> 'post',
								'target' 		=> '_blank',
								'id' 			=> 'upload_form_split_roll',
								'enctype' 		=> 'multipart/form-data'
							])
						!!}
							<input type="file" class="hidden" id="upload_file_split_roll" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
							<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
						{!! Form::close() !!}
						
						<a class="btn btn-default btn-icon" href="{{ route('fabricMaterialStock.exportFormSplitRoll')}}" data-popup="tooltip" title="download form import split roll" data-placement="bottom" data-original-title="download form import split roll"><i class="icon-download"></i></a>
						<button type="button" class="btn btn-default btn-icon" id="upload_button_split_roll" data-popup="tooltip" title="upload form import split roll" data-placement="bottom" data-original-title="upload form import split roll"><i class="icon-upload"></i></button>
					@endrole

					<a class="btn btn-primary btn-icon" href="{{ route('fabricMaterialStock.export')}}" data-popup="tooltip" title="download form import" data-placement="bottom" data-original-title="download form import"><i class="icon-download"></i></a>
					<button type="button" class="btn btn-success btn-icon" id="upload_button" data-popup="tooltip" title="upload form import" data-placement="bottom" data-original-title="upload form import"><i class="icon-upload"></i></button>
					
					
				
				</div>
				
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover ">
					<thead>
						<tr>
							<th>No</th>
							<th>Type Stock</th>
							<th>Supplier Name</th>
							<th>Po Supplier</th>
							<th>Item Code</th>
							<th>Uom</th>
							<th>No Roll</th>
							<th>Batch Number</th>
							<th>Actual Width</th>
							<th>Actual Lot</th>
							<th>Qty</th>
							<th>Source</th>
							<th>System Note</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody id="tbody-material-stock-fabric"></tbody>
				</table>
			</div>
			{!!
				Form::open([
					'role' 		=> 'form',
					'url' 		=> route('fabricMaterialStock.store'),
					'method' 	=> 'post',
					'enctype' 	=> 'multipart/form-data',
					'class' 	=> 'form-horizontal',
					'id'		=> 'form'
				])
			!!}
				<div class="form-group text-right" style="margin-top: 10px;">
					{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_id')) !!}
					{!! Form::hidden('materials','[]' , array('id' => 'materials')) !!}
					{!! Form::hidden('mapping_stocks',$mapping_stocks , array('id' => 'mapping_stocks')) !!}
					{!! Form::hidden('url_fabric_material_stock',route('fabricMaterialStock.index') , array('id' => 'url_fabric_material_stock')) !!}
					<button type="submit" class="btn col-xs-12 btn-blue-success">Save <i class="icon-floppy-disk position-left"></i></button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>

	{!! Form::hidden('page','create' , array('id' => 'page')) !!}
@endsection


@section('page-modal')
	@include('form.modal_picklist', [
		'name' 			=> 'item_out_fabric',
		'title' 		=> 'List Item',
		'placeholder' 	=> 'Search based on code / name',
	])

	@include('form.modal_picklist', [
		'name' 			=> 'supplier_fabric',
		'title' 		=> 'List Supplier',
		'placeholder' 	=> 'Search based on code / name',
	])
@endsection

@section('page-js')
	@include('fabric_material_stock._item')
	<script src="{{ mix('js/fabric_material_stock.js') }}"></script>
@endsection
