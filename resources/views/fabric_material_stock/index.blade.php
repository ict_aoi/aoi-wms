@extends('layouts.app', ['active' => 'fabric_material_stock_other'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Stock Other</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Material Stock Other</li>
        </ul>
		<ul class="breadcrumb-elements">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
					<i class="icon-three-bars position-left"></i>
					Actions
					<span class="caret"></span>
				</a>
				
				<ul class="dropdown-menu dropdown-menu-right">
					<li><a href="{{ route('fabricMaterialStockOther.create') }}"><i class="icon-plus2 pull-right"></i> Create</a></li>
					<li><a href="{{ route('fabricMaterialStockOther.export') }}" target="_blank"><i class="icon-file-excel pull-right"></i> Export</a></li>
				</ul>
			</li>
		</ul>
    </div>
</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover " id="material_stock_other_table">
					<thead>
						<tr>
							<th>id</th>
							<th>Created At</th>
							<th>Created By</th>
							<th>Type Stock</th>
							<th>Po Supplier</th>
							<th>Supplier Name</th>
							<th>No Roll</th>
							<th>Item Code</th>
							<th>Color</th>
							<th>Batch Number</th>
							<th>Actual Lot</th>
							<th>Uom</th>
							<th>Qty Upload</th>
							<th>Qty Available</th>
							<th>Source</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>

	{!! Form::hidden('flag_msg',$flag , array('id' => 'flag_msg')) !!}
	{!! Form::hidden('page','index' , array('id' => 'page')) !!}
@endsection

@section('page-js')
	<script src="{{ mix('js/fabric_material_stock_other.js') }}"></script>
@endsection
