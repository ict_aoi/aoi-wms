
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($kepalaproduksi))
                <li><a href="{!! $kepalaproduksi !!}" target="_blank"><i class="icon-search4"></i> Kepala Produksi</a></li>
            @endif
            @if (isset($material_pendukung))
                <li><a href="{!! $material_pendukung !!}" target="_blank"><i class="icon-database-insert"></i> Pendukung</a></li>
            @endif
            @if (isset($detail_kontrak))
                <li><a href="{!! $detail_kontrak !!}" target="_blank"><i class="icon-search4"></i> Detail Kontrak</a></li>
            @endif
            @if (isset($print_doc))
                <li><a href="{!! $print_doc !!}" target="_blank"><i class="icon-file-word"></i> Export Doc</a></li>
                <li class="divider"></li>
            @endif

            @if (isset($detail))
                <li><a href="{!! $detail !!}" target="_blank"><i class="icon-search4"></i> Detail</a></li>
            @endif

            @if (isset($history))
                <li><a href="{!! $history !!}" target="_blank"><i class="icon-history"></i> History</a></li>
            @endif

            @if (isset($historyModal))
                <li><a href="#" onclick="history('{!! $historyModal !!}')" ><i class="icon-history"></i> History</a></li>
            @endif
            
            @if (isset($show))
                <li><a href="{!! $show !!}"><i class="icon-search4"></i> Details</a></li>
            @endif

            @if (isset($show_modal))
                <li><a href="#" onclick="show('{!! $show_modal !!}')" ><i class="icon-search4"></i> Detail</a></li>
            @endif

            @if (isset($edit))
                <li><a href="{!! $edit !!}"><i class="icon-pencil6"></i> Edit</a></li>
            @endif

            @if (isset($edit_modal))
                <li><a href="#" onclick="edit('{!! $edit_modal !!}')" ><i class="icon-pencil6"></i> Edit</a></li>
            @endif
            @if (isset($remark))
                <li><a href="#" onclick="remark('{!! $remark !!}')" ><i class="icon-pencil6"></i> Remark</a></li>
            @endif
            @if (isset($delete))
                <li><a href="#" onclick="hapus('{!! $delete !!}')"><i class="fa icon-trash"></i> Delete</a></li>
            @endif
            
            @if (isset($unlocked))
                <li><a href="#" onclick="unlocked('{!! $unlocked !!}')"><i class="icon-unlocked"></i> Unlock</a></li>
            @endif

            @if (isset($approve))
                <li><a href="#" onclick="approve('{!! $approve !!}')"><i class="icon-file-check2"></i> Approve</a></li>
            @endif

            @if (isset($confirm))
                <li><a href="#" onclick="confirm('{!! $confirm !!}')"><i class="icon-file-check2"></i> Confirm</a></li>
            @endif

            @if (isset($reject))
                <li><a href="#" onclick="reject('{!! $reject !!}')"><i class=" icon-x"></i> Reject</a></li>
            @endif

            @if (isset($closeMrp))
                <li><a onclick="closeMrp('{!! $closeMrp !!}')"><i class=" icon-x"></i> Close</a></li>
            @endif

            @if (isset($cancelAutoAllocation))
                <li><a href="#" onclick="cancel('{!! $cancelAutoAllocation !!}')"><i class=" icon-x"></i> Cancel Auto Allocation</a></li>
            @endif

            @if (isset($refresh))
                <li><a href="#" onclick="reroute('{!! $refresh !!}')"><i class="icon-database-refresh"></i> Reroute</a></li>
            @endif

            @if (isset($sync))
                <li><a href="#" onclick="sync('{!! $sync !!}')"><i class="icon-sync"></i> Sync</a></li>
            @endif

            @if (isset($recalculate))
                <li><a href="#" onclick="recalculate('{!! $recalculate !!}')"><i class="icon-calculator3"></i> Recalculate</a></li>
            @endif

            @if (isset($allocatingStock))
                <li><a href="#" onclick="allocating('{!! $allocatingStock !!}')"><i class=" icon-pie-chart"></i> Allocating</a></li>
            @endif

           

             @if (isset($print))
                <li><a href="{!! $print !!}" target="_blank"><i class="icon-printer2"></i> Print</a></li>
            @endif

            @if (isset($plan))
                <li><a href="{!! $plan !!}"><i class="icon-file-plus"></i> Make a Plan</a></li>
            @endif

           

            @if (isset($cancel))
                <li><a href="#" onclick="cancel('{!! $cancel !!}')"><i class="icon-x"></i> Cancel</a></li>
            @endif

            @if (isset($move))
                <li><a href="#" onclick="move('{!! $move !!}')"><i class="icon-transmission"></i> Move Locator </a></li>
            @endif
            
            @if (isset($transfer))
                <li><a href="#" onclick="transfer('{!! $transfer !!}')"><i class="icon-exit"></i> Transfer Stock </a></li>
            @endif

            @if (isset($change_type))
                <li><a href="#" onclick="changeType('{!! $change_type !!}')"><i class="icon-copy3"></i> Change Type</a></li>
            @endif

        </ul>
    </li>
</ul>
