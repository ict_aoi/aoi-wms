<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
		<th>Area</th>
		<th>Locator</th>
		<th>Po Buyer</th>
		<th>Note</th>
		<th>Action</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			@php
				if(!$list->has_many_po_buyer) $active_pobuyer = $list->last_po_buyer;
				else $active_pobuyer = null;
			@endphp
			<tr>
				<td>
					{{ $list->area->name }}
				</td>
				<td>
					@if ($list->area->name != 'GENERAL')
							{{ $list->rack }}.{{ $list->y_row }}.{{ $list->z_column }}
					@endif
				</td>
				<td>
					@if ($list->area->name != 'GENERAL' && $list->has_many_po_buyer == false)
							{{ $active_pobuyer }}
					@endif
				</td>
				<td>
					@if ($list->has_many_po_buyer)
							Rak ini dapat dimasukan lebih dari 1 po buyer
					@else
							Rak ini hanya dapat dimasukan oleh 1 po buyer
					@endif
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose" 
						type="button" data-id="{{ $list->id }}" data-pobuyer="{{ $active_pobuyer }}" data-name="{{ $list->area->name }} - {{ $list->rack }}.{{ $list->y_row }}.{{ $list->z_column }}" data-manypobuyer="{{ $list->has_many_po_buyer }}" >Select</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
