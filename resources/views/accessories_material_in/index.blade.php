@extends('layouts.app', ['active' => 'accessories_material_in'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material In</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Material In</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	@role(['admin-ict-acc'])
		<div class="panel panel-default border-grey">
			<div class="panel-heading">
				<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
					</ul>
				</div>
			</div>
			<div class="panel-body">
				@include('form.select', [
					'field' 			=> 'warehouse',
					'label' 			=> 'Warehouse',
					'default'			=> auth::user()->warehouse,
					'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
					'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
					'options' 			=> [
						'1000002' 		=> 'Warehouse Accessories AOI 1',
						'1000013' 		=> 'Warehouse Accessories AOI 2',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_warehouse'
					]
				])
			</div>
		</div>
	@else 
		{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
	@endrole

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover table-responsive">
					<thead>
						<tr>
							<th>#</th>
							<th>Statistical Date</th>
							<th>Season</th>
							<th>Source</th>
							<th>Po Buyer</th>
							<th>Item</th>
							<th>Total Package</th>
							<th>Style</th>
							<th>Uom</th>
							<th>Need</th>
							<th>Qty On Barcode</th>
							<th>Error Note</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody id="tbody_accessories_material_in">
					</tbody>
				</table>
			</div>
					
				
		{!!
			Form::open([
				'role' 		=> 'form',
				'url' 		=> route('accessoriesMaterialIn.store'),
				'method' 	=> 'post',
				'enctype'	=> 'multipart/form-data',
				'id'		=> 'form'
			])
		!!}	
				
				{!! Form::hidden('__po_buyer','', array('id' => '__po_buyer')) !!}
				{!! Form::hidden('barcode_products',$return_array , array('id' => 'barcode_products')) !!}
				{!! Form::hidden('rack_po_buyer',null, array('id' => 'rack_po_buyer')) !!}
				{!! Form::hidden('has_many_po_buyer',null, array('id' => 'has_many_po_buyer')) !!}
				{!! Form::hidden('_from_rackin_id', isset($hidden_value) ? $hidden_value : null, ['id' => '_from_rackin_id']) !!}
				{!! Form::hidden('url_accessories_material_in_create',route('accessoriesMaterialIn.create'), ['id' => 'url_accessories_material_in_create']) !!}
				{!! Form::hidden('url_temporary_reset',route('temporary.resetAll'), ['id' => 'url_temporary_reset']) !!}
				{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_id')) !!}
				
				@include('form.picklist', [
					'field' 		=> 'from_rackin',
					'name' 			=> 'from_rackin',
					'label' 		=> 'Locator',
					'readonly' 		=> true,
					'placeholder' 	=> 'Please select locator',
					'title' 		=> 'Please select locator',
					'label_col' 	=> 'col-lg-3 col-md-3 col-sm-12',
					'form_col' 		=> 'col-lg-9 col-md-9 col-sm-12',
				])

			<div class="form-group text-right" style="margin-top:10px">
				<button type="button" class="btn btn-yellow-cancel col-xs-6 btn-lg" id="reset_all_checkin" >Reset  <span class="glyphicon glyphicon-remove"></span></button>
				<button type="submit" class="btn btn-blue-success col-xs-6 btn-lg">Save <i class="icon-floppy-disk position-left"></i></button>
			</div>
		{!! Form::close() !!}
				
				
		</div>
			
	</div>

	<div class="alert alert-info alert-styled-right alert-bordered hidden" id="alert_locator">
		<span class="text-semibold">Info for you !</span> <span id="msg_alert"></span><b id="list_rack"></b>
	</div>

	<div class="alert alert-info alert-styled-right alert-bordered hidden" id="alert_out">
		<span class="text-semibold">Info for you !</span> <span id="msg_out"></span>
	</div>
@endsection

@section('page-modal')
	@include('form.modal_picklist', [
		'name' => 'from_rackin',
		'title' => 'List Locator',
		'placeholder' => 'Search based on name / code',
	])
@endsection

@section('page-js')
	@include('accessories_material_in._item')
	<script src="{{ mix('js/accessories_material_in.js') }}"></script>
@endsection
