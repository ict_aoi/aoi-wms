@extends('layouts.app', ['active' => 'search'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Search</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li class="active">Search</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-flat border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
		</div>

		<div class="panel-body">
			<div class="row">
				<div class="col-xs-6">
					{!!
						Form::open(array(
							'class' => 'main-search',
							'role' => 'form',
							'url' => route('search.detail'),
							'method' => 'get',
							'id' => 'form'
						))
					!!}
						<div class="input-group content-group">
							<div class="has-feedback has-feedback-left">
								<input type="text" class="form-control input-xlg barcode" id="barcode" name="barcode" autofocus autocomplete="off" placeholder="Please scan your barcode here">
								<div class="form-control-feedback">
									<i class="icon-search4 text-muted text-size-base"></i>
								</div>
							</div>

							<div class="input-group-btn">
								<button type="submit" class="btn btn-blue-success btn-xlg">Search <i class="icon-search4 position-right"></i></button>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
				<div class="col-xs-6">
					{!!
						Form::open([
							'role' 		=> 'form',
							'url'		=> route('search.detail'),
							'method' 	=> 'get',
							'enctype' 	=> 'multipart/form-data',
							'class' 	=> 'form-horizontal',
							'id'		=> 'form-search'
						])
					!!}
					@include('form.picklist', [
						'field' 			=> 'po_buyer',
						'name' 				=> 'po_buyer',
						'div_class' 		=> 'col-xs-12',
						'form_col' 			=> 'col-xs-12',
						'placeholder' 		=> 'Please Select PO Buyer',
						'help'				=> 'If barcode can\'t be scan, please search po buyer here.'
					])
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
	<div class="row hidden" id="result">
		<div class="col-lg-8">
			<div class="panel panel-body">
				<p class="text-muted text-size-small">Movement Histories</p>
				<hr>
				<ul class="media-list search-results-list content-group" id="result_history">
				</ul>
			</div>
		</div>

		<div class="col-lg-4">
			<div class="panel panel-body">
				<h5 class="text-semibold" id="item_code"></h5>
				<p class="content-group" id="item_desc"></p>

				<ul class="list content-group">
					<li><span class="text-semibold">No. PO Supplier:</span> <span id="document_no" style="color:#1e88e5"><span></li>
					<li><span class="text-semibold">No. PO Buyer:</span> <span id="po_buyer" style="color:#1e88e5"><span></li>
					<li><span class="text-semibold">Job Order:</span> <span id="job_order" style="color:#1e88e5"><span></li>
					<li><span class="text-semibold">Style:</span> <span id="style" style="color:#1e88e5"><span></li>
					<li><span class="text-semibold">Article No:</span> <span id="article_no" style="color:#1e88e5"><span></li>
					<li><span class="text-semibold">Qc Last Status:</span> <span id="qc_status" style="color:#1e88e5"><span></li>
					<li><span class="text-semibold">Qc Check Last Date:</span> <span id="qc_last_date_check" style="color:#1e88e5"><span></li>
				</ul>
			</div>

			@role(['admin-ict-acc','supervisor','admin-ict-fabric','supervisor-acc'])
				{{ Form::open(['method' => 'get', 'id' => 'form_print_barcode', 'class' => 'form-horizontal', 'url' => "#",'target' => '_blank']) }}
					<button type="submit" id="btn_reprint" class="btn btn-yellow-cancel col-xs-12">Reprint Barcode</button>
					<button type="button" id="btn_disabled_reprint" class="btn btn-default col-xs-12 hidden" disabled="disabled">Reprint Barcode</button>
				{{ Form::close() }}
			@endrole

		</div>
	</div>
	{!! Form::hidden('movement_history','[]' , array('id' => 'movement_history')) !!}
@endsection

@section('page-modal')
	@include('search.modal_picklist', [
		'name' 			=> 'po_buyer',
		'name2' 		=> 'item_code',
		'title' 		=> 'List Of Po Buyer',
		'placeholder' 	=> 'Search based on po buyer',
		'placeholder2' 	=> 'Search based on item code',
	])
@endsection

@section('page-js')
	@include('search._movement_history')
	<script type="text/javascript" src="{{ asset(elixir('js/search.js'))}}"></script>
@endsection
