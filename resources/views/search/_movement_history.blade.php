<script type="x-tmpl-mustache" id="_draw">
	{% #items %}
		<li class="media">
			<div class="media-body">
				<h6 class="media-heading"><a href="#"><strong>{% status %}</strong></a></h6>
				<p>FROM LOCATION : {% from_location %}</p>
				<p>TO LOCATION : {% to_destination %}</p>
				<p>USER : {% user_name %}</p>
				<p>MOVEMENT DATE : {% movement_date %}</p>
			</div>
		</li>
	{%/items%}
</script>