@extends('layouts.app', ['active' => 'library'])

@section('content')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title" style="padding-top:0px;">
				<h4><a href="{{ route('knowledgebase.library') }}"></a><span class="text-semibold">KNOWLEDGE BASE</span></h4>
				<ul class="breadcrumb breadcrumb-caret position-left">
					<li>Knowledge Base</li>
					<li class="active">data</li>
				</ul>
				<a class="heading-elements-toggle"><i class="icon-more"></i></a>
			</div>
		</div>
	</div>
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">BROWSE WORK INSTRUCTION(S)<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
					<li><a data-action="close"></a></li>
				</ul>
			</div>
		</div>

		<div class="panel-body">
			<div class="row">
				<div class="col-md-4">
					<div class="content-group">
						<h6 class="text-semibold heading-divided"><i class="icon-folder6 position-left"></i> MATERIAL RECEIVE</h6>
						<div class="list-group no-border">
							<a href="{{ route('knowledgebase.exportPdf','material-receive-scan') }}" target="_blank" class="list-group-item">
								<i class="icon-file-text2"></i> Scan
							</a>

							<a href="{{ route('knowledgebase.exportPdf','material-receive-printbarcode') }}" target="_blank" class="list-group-item">
								<i class="icon-file-text2"></i> Print Barcode
							</a>

							<a href="{{ route('knowledgebase.exportPdf','material-receive-reject') }}" target="_blank" class="list-group-item">
								<i class="icon-file-text2"></i> Reject
							</a>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="content-group">
						<h6 class="text-semibold heading-divided"><i class="icon-folder6 position-left"></i> MATERIAL IN</h6>
						<div class="list-group no-border">
							<a href="{{ route('knowledgebase.exportPdf','material-in-checkin') }}" target="_blank" class="list-group-item">
								<i class="icon-file-text2"></i> Check in
							</a>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="content-group">
						<h6 class="text-semibold heading-divided"><i class="icon-folder6 position-left"></i> MATERIAL OUT</h6>
						<div class="list-group no-border">
							<a href="{{ route('knowledgebase.exportPdf','material-out-checkout') }}" target="_blank" class="list-group-item">
								<i class="icon-file-text2"></i> Check out
							</a>

							<a href="{{ route('knowledgebase.exportPdf','material-out-partial') }}" target="_blank" class="list-group-item">
								<i class="icon-file-text2"></i> Partial
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="content-group">
						<h6 class="text-semibold heading-divided"><i class="icon-folder6 position-left"></i> MISCELLANEOUS ITEM</h6>
						<div class="list-group no-border">
							<a href="{{ route('knowledgebase.exportPdf','miscellaneous-item-data') }}" target="_blank" class="list-group-item">
								<i class="icon-file-text2"></i> Data
							</a>

							<a href="{{ route('knowledgebase.exportPdf','miscellaneous-item-data') }}" target="_blank" class="list-group-item">
								<i class="icon-file-text2"></i> Approval
							</a>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="content-group">
						<h6 class="text-semibold heading-divided"><i class="icon-folder6 position-left"></i> ALLOCATION</h6>
						<div class="list-group no-border">
							<a href="{{ route('knowledgebase.exportPdf','allocation-data') }}" target="_blank"  class="list-group-item">
								<i class="icon-file-text2"></i> Data
							</a>

							<a href="{{ route('knowledgebase.exportPdf','allocation-approval') }}" target="_blank"  class="list-group-item">
								<i class="icon-file-text2"></i> Approval
							</a>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="content-group">
						<h6 class="text-semibold heading-divided"><i class="icon-folder6 position-left"></i> LOCATOR</h6>
						<div class="list-group no-border">
							<a href="{{ route('knowledgebase.exportPdf','locator-info') }}" target="_blank" class="list-group-item">
								<i class="icon-file-text2"></i> Info
							</a>

							<a href="{{ route('knowledgebase.exportPdf','locator-barcode') }}" target="_blank" class="list-group-item">
								<i class="icon-file-text2"></i> Barcode
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="content-group">
						<h6 class="text-semibold heading-divided"><i class="icon-folder6 position-left"></i> REPORT</h6>
						<div class="list-group no-border">
							<a href="{{ route('knowledgebase.exportPdf','report-receivement') }}" target="_blank" class="list-group-item">
								<i class="icon-file-text2"></i> Receivement
							</a>

							<a href="{{ route('knowledgebase.exportPdf','report-movement') }}" target="_blank"  class="list-group-item">
								<i class="icon-file-text2"></i> Movement
							</a>
							
							<a href="{{ route('knowledgebase.exportPdf','report-stock') }}" target="_blank" class="list-group-item">
								<i class="icon-file-text2"></i> Stock
							</a>

							<a href="{{ route('knowledgebase.exportPdf','report-miscellanoue-item') }}" target="_blank"  class="list-group-item">
								<i class="icon-file-text2"></i> Miscellaneous Item
							</a>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="content-group">
						<h6 class="text-semibold heading-divided"><i class="icon-folder6 position-left"></i> MASTER DATA</h6>
						<div class="list-group no-border">
							<a href="{{ route('knowledgebase.exportPdf','master-area-locator') }}" target="_blank"  class="list-group-item">
								<i class="icon-file-text2"></i> Area & Locator
							</a>

							<a href="{{ route('knowledgebase.exportPdf','master-destination') }}" target="_blank" class="list-group-item">
								<i class="icon-file-text2"></i> Destination
							</a>

							<a href="{{ route('knowledgebase.exportPdf','master-criteria') }}" target="_blank" class="list-group-item">
								<i class="icon-file-text2"></i> Criteria
							</a>

							<a href="{{ route('knowledgebase.exportPdf','master-permission') }}" target="_blank" class="list-group-item">
								<i class="icon-file-text2"></i> Permission
							</a>

							<a href="{{ route('knowledgebase.exportPdf','master-role') }}" target="_blank" class="list-group-item">
								<i class="icon-file-text2"></i> Role
							</a>

							<a href="{{ route('knowledgebase.exportPdf','master-user') }}" target="_blank" class="list-group-item">
								<i class="icon-file-text2"></i> User
							</a>
						</div>
					</div>
				</div>
				{!! Form::hidden('flag_msg',(Session::has('flag')) ? Session::get('flag'):null , array('id' => 'flag_msg')) !!}
			
				
			</div>
		</div>
	</div>
@endsection

@section('content-modal')
@endsection

@section('content-js')
<script>
	$(function () {
		var flag_msg = $('#flag_msg').val();
		if (flag_msg == 'error') {
			$("#alert_error").trigger("click", 'File tidak ditemukan.');
		}
	});
</script>
@endsection
