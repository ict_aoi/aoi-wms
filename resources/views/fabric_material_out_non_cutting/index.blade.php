@extends('layouts.app', ['active' => 'fabric_material_out_non_cutting'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Out Non Cutting</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Material Out Non Cutting</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	@role(['admin-ict-fabric'])
		<div class="panel panel-default border-grey">
			<div class="panel-body">
				@include('form.select', [
					'field' 			=> 'warehouse',
					'label' 			=> 'Warehouse',
					'default'			=> auth::user()->warehouse,
					'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
					'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
					'options' 			=> [
						'1000001' 		=> 'Warehouse Fabric AOI 1',
						'1000011' 		=> 'Warehouse Fabric AOI 2',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_warehouse'
					]
				])
			</div>
		</div>
	@else 
		{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
	@endrole


	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover table-responsive">
					<thead>
						<tr>
							<th>#</th>
							<th>Locator Source</th>
							<th>Po Supplier</th>
							<th>Supplier Name</th>
							<th>Item Code</th>
							<th>Nomor Roll</th>
							<th>Batch Number</th>
							<th>Actual Lot</th>
							<th>Available Qty</th>
							<th>Reserved Qty</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody id="tbody-material-out-fabric">
					</tbody>
				</table>
			</div>

			<div class="row">
				<div class="alert alert-default alert-bordered  hidden" id="selected_div">
					<center><b> <span id="selected_document_no"> </span> :: <span id="selected_item_code"> </span>:: <span id="selected_qty"> </span> /  <span id="selected_qty_curr"> </span></p></b></center>
				</div>
			</div>
			
			{!!
				Form::open([
					'role' => 'form',
					'url' => route('fabricMaterialOutNonCutting.store'),
					'method' => 'post',
					'enctype' => 'multipart/form-data',
					'class' => 'form-horizontal',
					'id'=> 'form'
				])
			!!}
			
				{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_id')) !!}
				{!! Form::hidden('barcode_products','[]' , array('id' => 'barcode_products')) !!}
				{!! Form::hidden('data_allocation','[]' , array('id' => 'data_allocation')) !!}
				{!! Form::hidden('temporary_data_handover', '[]' , array('id' => 'temporary_data_handover')) !!}

				<a href="{{ route('fabricMaterialOutNonCutting.create') }}" id="url_get_data" class="hidden"></a>
				<a href="{{ route('fabricMaterialOutNonCutting.removeLastUser') }}" id="url_remove_last_used" class="hidden"></a>
						
				<div class="form-group text-right" style="margin-top: 10px;">
					<button type="submit" class="btn btn-blue-success col-xs-12 btn-lg">Save <i class="icon-floppy-disk position-left"></i></button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>

	{!!
		Form::open([
			'role' 		=> 'form',
			'url' 		=> route('fabricMaterialOutNonCutting.printBarcode'),
			'method' 	=> 'get',
			'class' 	=> 'form-horizontal',
			'id'		=> 'getBarcode',
			'target' 	=> '_blank'
		])
	!!}
		{!! Form::hidden('list_barcodes','[]' , array('id' => 'list_barcodes')) !!}
		<div class="form-group text-right" style="margin-top: 10px;">
			<button type="submit" class="btn hidden">Print <i class="icon-printer position-left"></i></button>
		</div>
	{!! Form::close() !!}
@endsection


@section('page-modal')
	@include('fabric_material_out_non_cutting._confirmation_handover_modal')
	@include('fabric_material_out_non_cutting._confirmation_move_order_modal')
	@include('fabric_material_out_non_cutting.modal_picklist', [
		'title' => 'Searching Criteria',
	])	
@endsection

@section('page-js')
	@include('fabric_material_out_non_cutting._item')
	<script src="{{ mix('js/fabric_material_out_non_cutting.js') }}"></script>
@endsection
