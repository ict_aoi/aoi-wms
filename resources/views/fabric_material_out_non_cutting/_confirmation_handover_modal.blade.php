<div class="modal fade" id="confirmationHandoverModal" tabindex="-1" role="dialog" data-toggle="modal" data-backdrop="static" data-keyboard="false" aria-labelledby="confirmationModalLabel">
	<div class="modal-dialog" role="document">
		{{ 
			Form::open([
				'method'	=> 'post',
				'id' 		=> 'formSummaryHandover',
				'class' 	=> 'form-horizontal',
				'url' 		=> route('fabricMaterialOutNonCutting.storeSummaryHandoverMaterial')
			]) 
		}}
		    
			<div class="modal-content">
                <div class="modal-header bg-blue">
					<h5 class="modal-title"><p>Confirmation Modal For <span id="_supplier_name"></span></p><p><span id="_po_supplier"></span> <b>-</b> <span id="_item_code"></span></p> </h5>
					<small>Please input nomor "Pindah Tangan" (PT) and Qty out here. </small> 
				</div>

				<div class="modal-body">
				  	<div class="row">
						<div class="col-md-12">
						   	@include('form.text', [
								'field' 		=> 'no_pt_input',
								'label_col' 	=> 'col-lg-2 col-md-2 col-sm-12',
								'form_col' 		=> 'col-lg-10 col-md-10 col-sm-12',
								'label' 		=> 'Input No. PT',
								'placeholder' 	=> 'Please input nomor PT here',
								'attributes' 	=> [
									'id' 			=> 'input_no_pt',
									'autocomplete'	=> 'off'
								]
							])
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
						   	@include('form.text', [
								'field' 		=> 'input_qty_handover',
								'label_col' 	=> 'col-lg-2 col-md-2 col-sm-12',
								'form_col'		=> 'col-lg-10 col-md-10 col-sm-12',
								'label' 		=> 'Qty Move',
								'placeholder' 	=> 'Please input total qty out here',
								'attributes' 	=> [
									'id' 			=> 'input_qty_handover',
									'autocomplete'	=> 'off'
								]
							])
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
						   	@include('form.text', [
								'field' 		=> 'no_bc',
								'label_col' 	=> 'col-lg-2 col-md-2 col-sm-12',
								'form_col' 		=> 'col-lg-10 col-md-10 col-sm-12',
								'label' 		=> 'No BC',
								'placeholder' 	=> 'Please input no BC here',
								'attributes' 	=> [
									'id' 			=> 'input_bc_confirmation',
									'autocomplete'	=> 'off'
								]
							])
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							@include('form.text', [
								'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
								'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
								'field' 		=> 'no_surat_jalan',
								'label' 		=> 'No Surat Jalan',
								'placeholder' 	=> 'Please input no surat jalan here',
								'attributes' 	=> [
									'id' 		=> 'update_no_surat_jalan'
								]
                            ])
							</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							@include('form.date', [
								'field' 		=> 'tanggal_kirim',
								'label' 		=> 'Tanggal Kirim',
								'label_col' 	=> 'col-md-3 col-lg-3 col-sm-12',
								'form_col' 		=> 'col-md-9 col-lg-9 col-sm-12',
								'placeholder' 	=> 'dd/mm/yyyy',
								'class' 		=> 'daterange-single',
								'attributes' 	=> [
									'id' 			=> 'tanggal_kirim',
									'readonly' 		=> 'readonly',
									'autocomplete' 	=> 'off'
								]
							])
						</div>
					</div>
				</div>

				{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_handover_id')) !!}
				{!! Form::hidden('summary_handover_id', null , array('id' => '__summary_handover_id')) !!}
				{!! Form::hidden('supplier_name', null , array('id' => '__supplier_name')) !!}
				{!! Form::hidden('c_bpartner_id', null , array('id' => '__c_bpartner_id')) !!}
				{!! Form::hidden('item_code', null, array('id' => '__item_code')) !!}
				{!! Form::hidden('document_no', null , array('id' => '__document_no')) !!}
				{!! Form::hidden('c_order_id', null , array('id' => '__c_order_id')) !!}
				{!! Form::hidden('item_id', null , array('id' => '__item_id')) !!}
				{!! Form::hidden('is_move_order', false , array('id' => '__check_move_order_handover')) !!}
				

                <div class="modal-footer">
					<div class="row">
						<button type="button" class="btn btn-default" onClick="dismisModal()" >Cancel</button>
						<button type="submit" class="btn btn-blue-success">Save</button>
					</div>
			    </div>
                
			</div>
		{{ Form::close() }}
	</div>
</div>