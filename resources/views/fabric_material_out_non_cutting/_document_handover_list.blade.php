<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
			<th>No PT</th>
			<th>Po Supplier</th>
			<th>Item Code</th>
			<th>Total Qty Handover ( In Yds )</th>
			<th>Qty Outstanding ( In Yds )</th>
			<th>Qty Supply ( In Yds )</th>
			<th>Complate Date</th>
			<th>Action</th>
		</tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ $list->handover_document_no }}
				</td>
				<td>
					{{ $list->document_no }}
				</td>
				<td>
					{{ $list->item_code }}
				</td>
				<td>
					{{  sprintf('%0.4f',$list->total_qty_handover) }} 
				</td>
				<td>
					{{  sprintf('%0.4f',$list->total_qty_outstanding) }}
				</td>
				<td>
					{{  sprintf('%0.4f',$list->total_qty_supply) }}
				</td>
				<td>
					{{ ($list->complete_date ? $list->complete_date->format('d/M/y h:i:s') : null) }}
				</td>
				<td>
					@if(!$list->complete_date )
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose" 
						type="button" 
						data-id="{{ $list->id }}" 
						data-name="{{ $list->handover_document_no }}" 
						data-total="{{ $list->total_qty_handover }}" 
						data-type="{{ $type }}" 
						data-qty="{{ $list->total_qty_outstanding }}" >Select</button>
					@endif
				</td>
				
			</tr>
		@endforeach
	</tbody>
</table>

