
<!DOCTYPE html>
<html>
<head>
    <title>BARCODE :: PRINT BARCODE HANDOVER</title>
	<link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/print_barcode_fabric.css')) }}">
</head>
    <body>
		@foreach ($material_handovers as $key_1 => $material_handover)
			<div class="outer">
					<div class="title">
						<span style="@if(strlen($material_handover->document_no)>40) font-size: 12px @else font-size: 34px @endif">{{ $material_handover->document_no }}</span>
					</div>
					<div class="isi">
						<div class="isi1">
							<span style="@if(strlen($material_handover->item_code)>20) font-size: 20px @else font-size: 30px @endif">{{ $material_handover->item_code }}</span>
						</div>
						<div class="isi1">
							<span style="@if(strlen($material_handover->color)>15) font-size: 10px @else font-size: 30px @endif">{{ $material_handover->color }}</span>
						</div>
						<div class="isi1">
							<span style="@if(strlen($material_handover->batch_number)>15) font-size: 25px @else font-size: 30px @endif"> {{ $material_handover->batch_number }}</span>
						</div>
						
						<div class="isi2">
							<div class="block-kiri" style="justify-content: center; height: 45px;">
								<span style="font-size: 12px;word-wrap: break-word;">{{ $material_handover->supplier_name }}  </span>
							</div>
							<div class="block-kiri" style="justify-content: center; height: 45px;">
								<span style="font-size: 20px;word-wrap: break-word;">{{ sprintf("%0.4f",$material_handover->qty_handover) }} {{ strtoupper($material_handover->uom) }}   </span>
							</div>
							<p style="font-size: 15px;word-wrap: break-word;">NO ROLL : <br/><span style="@if(strlen($material_handover->nomor_roll)>=4) font-size: 35px @else font-size: 70px @endif">{{ $material_handover->nomor_roll }}</p>
						
						</div>
						<div class="isi3">
							<div class="block-kiri" style="height: 150px;">
								<img style="height: 2.5cm; width: 4.3cm; margin-top: 35px; margin-left: 2px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$material_handover->barcode", 'C128') }}" alt="barcode"   />			
								<br/><span style="font-size: 12px;word-wrap: break-word;">{{ $material_handover->barcode }}</span>
					
							</div>
							<div class="block-kiri">
								No. Invoice {{ $material_handover->no_invoice }}
							</div>
							<div class="block-kiri">
								Tgl. Handover {{ $material_handover->created_at }}
							</div>
							
						</div>
					</div>
							
				</div>
				<br/>
				<div class="outer_2">
					<div class="isi_2">
						<div class="block-kiri_2">
							<span style="font-size: 10px;word-wrap: break-word;">{{ $material_handover->document_no }}::{{ $material_handover->item_code }}::{{ $material_handover->color }}::</span>
							<span style="font-size: 10px;word-wrap: break-word;">{{ $material_handover->nomor_roll }}::{{ number_format($material_handover->qty_handover, 2, '.', ',') }} ({{ strtoupper(trim($material_handover->uom)) }})</span>
						</div>
					</div>
					<div class="isi1_2">
						<img style="height: 1cm; width: 5.3cm;	 margin-top: 18px; margin-left: 15px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$material_handover->barcode", 'C128') }}" alt="barcode"   />			
						<center><span style="font-size: 12px;word-wrap: break-word;">{{ $material_handover->barcode }}</span></center>
					
					</div>
				</div>
			
			
		@endforeach
		
    </body>
</html>