@extends('layouts.app', ['active' => 'accessories_locator'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Locator</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Locator</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>

		<div class="panel-body">
			{!!
				Form::open(array(
					'class' 	=> 'horizontal-form',
					'role' 		=> 'form',
					'url' 		=> route('accessoriesLocator.index'),
					'method' 	=> 'get',
				))
			!!}

				@role(['admin-ict-acc'])
					@include('form.select', [
						'field' 			=> 'warehouse',
						'label' 			=> 'Warehouse',
						'default'			=> auth::user()->warehouse,
						'label_col' 		=> 'col-sm-12',
						'form_col' 			=> 'col-sm-12',
						'options' 			=> [
							'1000002' 		=> 'Warehouse Accessories AOI 1',
							'1000013' 		=> 'Warehouse Accessories AOI 2',
						],
						'class' 			=> 'select-search',
						'attributes' 		=> [
							'id' 			=> 'select_warehouse'
						]
					])
				@else 
					{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
				@endrole

				<div class="col-md-6">
					@include('form.picklist', [
						'field' 		=> 'po_buyer_locator',
						'name' 			=> 'po_buyer_locator',
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'placeholder' 	=> 'Please Select Po Buyer',
						'delete' 		=> 'true'
					])
				</div>
				<div class="col-md-6">
					@include('form.text', [
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'field' 		=> 'item_code',
						'label' 		=> 'Item Code',
						'placeholder' 	=> 'Please input item code here',
						'attributes' 	=> [
							'id' 		=> 'item_code'
						]
					])
				</div>

				
			<button type="submit" class="btn btn-blue-success col-xs-12">Filter<i class="icon-filter3 position-left"></i></button>
			{!! Form::close() !!}
		</div>
	</div>

	@foreach ($areas as $key => $area)
		<div class="col-xs-12" style="@if($area->name == 'FREE STOCK') background-color: pink;@endif">
			<h6 class="content-group text-semibold">
				{{ $area->name }} AREA
			</h6>
			@foreach ($area->locators()->select('rack','has_many_po_buyer')->where('is_active',true)->groupby('rack','has_many_po_buyer')->orderby('rack')->get() as $key_rack => $rack)
				<div class="panel @if($rack->has_many_po_buyer == false) panel-default border-grey @else panel-blue panel-bordered @endif">
					<div class="panel-heading">
						<h6 class="panel-title"> {{ $rack->rack }}<a class="heading-elements-toggle"><i class="icon-more"></i></a><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-hover table-responsive">
								<tbody>
									<tr>
										@php 
											$baris = 0;
										@endphp
										@foreach ($area->getLocatorsAttribute($area->id,$rack->rack,$rack->has_many_po_buyer,$filtering_locator) as $key_locator => $locator)
											@if ($baris != $locator->y_row)
												</tr><tr>
											@endif

											@php
												if($rack->has_many_po_buyer) $po_buyer = 'Has Many Po Buyer';
												else $po_buyer = $locator->po_buyer;
												
											@endphp

											@if ( $locator->counter_in > 0)
												<td style="background-color:#d9ffde;">
													<a class="btn-show-item" data-toggle="modal" data-target="#detilRackModal" data-header="{{ $key }}" data-rack="{{ $key_rack }}"  data-id="{{ $key_locator }}"  data-popup="tooltip" title="{{ $po_buyer }}" data-placement="bottom" data-original-title="{{ $po_buyer }}">
														<input type="hidden" id="areaName_{{ $key }}_{{ $key_rack }}_{{ $key_locator }}" value="{{ $area->name }}" data-header="{{ $key }}" data-rack="{{ $key_rack }}" data-id="{{ $key_locator }}"></input>
														<input type="hidden" id="rack_{{ $key }}_{{ $key_rack }}_{{ $key_locator }}" value="{{ $rack->rack }}.{{ $locator->y_row }}.{{ $locator->z_column }}" data-header="{{ $key }}" data-rack="{{ $key_rack }}" data-id="{{ $key_locator }}"></input>
														<input type="hidden" id="locatorId_{{ $key }}_{{ $key_rack }}_{{ $key_locator }}" value="{{ $locator->id }}" data-header="{{ $key }}" data-rack="{{ $key_rack }}" data-id="{{ $key_locator }}"></input>
														<input type="hidden" id="poBuyer_{{ $key }}_{{ $key_rack }}_{{ $key_locator }}" value="{{ $po_buyer }}" data-header="{{ $key }}" data-rack="{{ $key_rack }}" data-id="{{ $key_locator }}"></input>
														<input type="hidden" id="counterIn_{{ $key }}_{{ $key_rack }}_{{ $key_locator }}" value="{{ $locator->counter_in }}" data-header="{{ $key }}" data-rack="{{ $key_rack }}" data-id="{{ $key_locator }}"></input>
														
														<span style="color:black">
															{{ $locator->rack }}.{{ $locator->y_row }}.{{ $locator->z_column }}
														</span>
													</a>
												</td>
											@else
												<td>
												<a class="btn-show-item" data-toggle="modal" data-target="#detilRackModal" data-header="{{ $key }}" data-rack="{{ $key_rack }}"  data-id="{{ $key_locator }}"  data-popup="tooltip" title="available" data-placement="bottom" data-original-title="available">
														<input type="hidden" id="areaName_{{ $key }}_{{ $key_rack }}_{{ $key_locator }}" value="{{ $area->name }}" data-header="{{ $key }}" data-rack="{{ $key_rack }}" data-id="{{ $key_locator }}"></input>
														<input type="hidden" id="rack_{{ $key }}_{{ $key_rack }}_{{ $key_locator }}" value="{{ $rack->rack }}.{{ $locator->y_row }}.{{ $locator->z_column }}" data-header="{{ $key }}" data-rack="{{ $key_rack }}" data-id="{{ $key_locator }}"></input>
														<input type="hidden" id="locatorId_{{ $key }}_{{ $key_rack }}_{{ $key_locator }}" value="{{ $locator->id }}" data-header="{{ $key }}" data-rack="{{ $key_rack }}" data-id="{{ $key_locator }}"></input>
														<input type="hidden" id="poBuyer_{{ $key }}_{{ $key_rack }}_{{ $key_locator }}" value="{{ $po_buyer }}" data-header="{{ $key }}" data-rack="{{ $key_rack }}" data-id="{{ $key_locator }}"></input>
														<input type="hidden" id="counterIn_{{ $key }}_{{ $key_rack }}_{{ $key_locator }}" value="{{ $locator->counter_in }}" data-header="{{ $key }}" data-rack="{{ $key_rack }}" data-id="{{ $key_locator }}"></input>
														
														
														<span style="color:black">
															{{ $locator->rack }}.{{ $locator->y_row }}.{{ $locator->z_column }}
														</span>
													</a>
												</td>
											@endif
											
											@php 
												$baris = $locator->y_row;
											@endphp
										@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			@endforeach
			
		</div>
	@endforeach
@endsection

@section('page-modal')
	@include('accessories_locator._detail_free_stock_area')
	@include('accessories_locator._detail_non_free_stock_area')

	@include('accessories_locator.modal_picklist', [
		'name' 			=> 'po_buyer_locator',
		'title' 		=> 'List Po Buyer',
		'placeholder' 	=> 'Search based on buyer',
	])
@endsection

@section('page-js')
	<script src="{{ mix('js/accessories_locator.js') }}"></script>
@endsection
