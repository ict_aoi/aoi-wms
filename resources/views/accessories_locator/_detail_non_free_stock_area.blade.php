<div id="detilNonFreeStockModal" data-backdrop="static" data-keyboard="false" class="modal fade" style="color:black !important">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<h5 class="modal-title">Detail Locator <span id="detail_header_non_free_stock_area"></span></h5>
			</div>

			<div class="modal-body">
				<div class="table-responsive">
					<table class="table datatable-basic table-striped table-hover table-responsive" id="detail_non_free_stock_table">
						<thead>
							<tr>
								<th>Barcode</th>
								<th>Po Supplier</th>
								<th>Item Code</th>
								<th>Po Buyer</th>
								<th>Style</th>
								<th>Article No</th>
								<th>Uom</th>
								<th>Qty On Barcode</th>
								<th>Qty Need</th>
								<th>Qty Receive</th>
							</tr>
						</thead>
					</table>
				</div>
				
			</div>
			
			{!! Form::hidden('locator_non_free_stock_id',null, array('id' => 'locator_non_free_stock_id')) !!}
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="glyphicon glyphicon-remove"></i> </button>
			</div>
		</div>
	</div>
</div>
