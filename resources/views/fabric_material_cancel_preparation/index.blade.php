@extends('layouts.app', ['active' => 'fabric_material_cancel_preparation'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Cancel Preparation</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li>Material Cancel</li>
            <li class="active">Material Preparation</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	@role(['admin-ict-fabric'])
		<div class="panel panel-default border-grey">
			<div class="panel-body">
				@include('form.select', [
					'field' 			=> 'warehouse',
					'label' 			=> 'Warehouse',
					'default'			=> auth::user()->warehouse,
					'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
					'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
					'options' 			=> [
						'1000001' 		=> 'Warehouse Fabric AOI 1',
						'1000011' 		=> 'Warehouse Fabric AOI 2',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_warehouse'
					]
				])
			</div>
		</div>
	@else 
		{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
	@endrole

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover table-responsive">
					<thead>
						<tr>
							<th>#</th>
							<th>Source</th>
							<th>Actual Lot</th>
							<th>Top Width</th>
							<th>Middle Width</th>
							<th>Bottom Width</th>
							<th>Actual Width</th>
							<th>Total Qty Booking</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody id="tbody-material-cancel-fabric">
					</tbody>
				</table>
			</div>

			{!!
				Form::open([
					'role' 		=> 'form',
					'url' 		=> route('fabricMaterialCancelPreparation.store'),
					'method' 	=> 'post',
					'enctype'	=> 'multipart/form-data',
					'class' 	=> 'form-horizontal',
					'id'		=> 'form'
				])
			!!}
			
			{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_id')) !!}
			{!! Form::hidden('items', '[]' , array('id' => 'items')) !!}
			{!! Form::hidden('url_fabric_material_cancel_preparation_create',route('fabricMaterialCancelPreparation.create'), ['id' => 'url_fabric_material_cancel_preparation_create']) !!}
			<div class="form-group text-right" style="margin-top: 10px;">
				<button type="submit" class="btn btn-blue-success col-xs-12 btn-lg">Save <i class="icon-floppy-disk position-left"></i></button>
			</div>
			
			{!! Form::close() !!}
			
		</div>
		
	</div>
@endsection

@section('page-js')
	@include('fabric_material_cancel_preparation._item')
	<script src="{{ mix('js/fabric_material_cancel_preparation.js') }}"></script>
@endsection
