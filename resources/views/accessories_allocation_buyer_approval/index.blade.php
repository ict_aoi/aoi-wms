@extends('layouts.app', ['active' => 'accessories_allocation_buyer_approval'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Allocation Approval</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Allocation</li>
				<li class="active">Approval</li>
			</ul>
		</div>
	</div>
@endsection


@section('page-content')
<div class="panel panel-default border-grey">
	<div class="panel-heading">
		<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
			</ul>
		</div>
	</div>

	<div class="panel-body">
		@include('form.select', [
			'field' 		=> 'warehouse',
			'label' 		=> 'Warehouse',
			'default' 		=> auth::user()->warehouse,
			'form_col' 		=> 'col-lg-6 col-md-6 col-sm-12',
			'options' 		=> [
				'' 			=> '-- Select Warehouse --',
				'1000002' 	=> 'Warehouse Accessories AOI 1',
				'1000013' 	=> 'Warehouse Accessories AOI 2',
			],
			'class' 		=> 'select-search',
			'attributes' 	=> [
				'id' 		=> 'select_warehouse'
			]
		])
	</div>
</div>

<div class="navbar navbar-default navbar-xs navbar-component no-border-radius-top">
	<ul class="nav navbar-nav visible-xs-block">
		<li class="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
	</ul>

	<div class="navbar-collapse collapse" id="navbar-filter">
		<ul class="nav navbar-nav">
			<li class="active"><a href="#waiting_for_approve" data-toggle="tab" onclick="changeTab('waiting')">Waiting for approved <i class="icon-file-eye position-left"></i></a></li>
			<li><a href="#approved" data-toggle="tab" onclick="changeTab('approved')">Approved <i class="icon-file-check position-left"></i></a></li>
		</ul>
	</div>
</div>

<div class="tabbable">
	<div class="tab-content">
		<div class="tab-pane fade in active" id="waiting_for_approve">
			<div class="panel panel-default border-grey">
				<div class="panel-heading">
					<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
					<div class="heading-elements">
						<ul class="icons-list">
							<li><a data-action="collapse"></a></li>
						</ul>
					</div>
				</div>

				<div class="panel-body">
					{!!
						Form::open(array(
							'class' 	=> 'form-horizontal',
							'role' 		=> 'form',
							'url' 		=> route('accessoriesAllocationBuyerApproval.approveAll'),
							'method' 	=> 'get',
							'enctype' 	=> 'multipart/form-data',
							'id'		=> 'approval_all_from'
						))
					!!}
						@include('form.select', [
							'field' 		=> 'creted_by',
							'label' 		=> 'Created by',
							'default' 		=> auth::user()->id,
							'form_col' 		=> 'col-lg-6 col-md-6 col-sm-12',
							'options' 		=> [
								'' 			=> '-- Select Created by --',
							]+$list_users,
							'class' 		=> 'select-search',
							'attributes' 	=> [
								'id' 		=> 'select_user_created'
							]
						])
						
						<br/>
						<div class="heading-btn">
							<button type="submit" class="btn btn-default col-xs-12">Approve All <i class="icon-checkmark position-left"></i></button>
						</div>
					{!! Form::close() !!}
				</div>
			</div>

			<div class="panel panel-default border-grey">
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table datatable-basic table-striped table-hover" id="waitingTable">
							<thead>
								<tr>
									<th>ID</th>
									<th>Created At</th>
									<th>Warehouse Allocation</th>
									<th>Approved At</th>
									<th>Username</th>
									<th>Type Stock Material</th>
									<th>Supplier Code</th>
									<th>Supplier Name</th>
									<th>Po Supplier</th>
									<th>Item Code</th>
									<th>Category</th>
									<th>Type Stock Po Buyer</th>
									<th>New Po Buyer</th>
									<th>Old Po Buyer</th>
									<th>Uom</th>
									<th>Style</th>
									<th>Qty Allocated</th>
									<th>Is Additional</th>
									<th>Status</th>
									<th>Status Barcode</th>
									<th>Remark</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>

		</div>

		<div class="tab-pane fade" id="approved">
			<div class="panel panel-default border-grey">
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table datatable-basic table-striped table-hover table-responsive" id="approvedTable">
							<thead>
								<tr>
									<th>ID</th>
									<th>Created At</th>
									<th>Warehouse Allocation</th>
									<th>Approved At</th>
									<th>Username</th>
									<th>Type Stock Material</th>
									<th>Supplier Code</th>
									<th>Supplier Name</th>
									<th>Po Supplier</th>
									<th>Item Code</th>
									<th>Category</th>
									<th>Type Stock Po Buyer</th>
									<th>New Po Buyer</th>
									<th>Old Po Buyer</th>
									<th>Uom</th>
									<th>Style</th>
									<th>Qty Allocated</th>
									<th>Is Additional</th>
									<th>Status</th>
									<th>Status Barcode</th>
									<th>Remark</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

{!! Form::hidden('active_tab',$active_tab , array('id' => 'active_tab')) !!}
@endsection

@section('page-js')
	<script src="{{ mix('js/accessories_allocation_buyer_approval.js') }}"></script>
@endsection