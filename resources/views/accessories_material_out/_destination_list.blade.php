<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
		<th>Code</th>
		<th>Action</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					@if($list->area->name == 'SUBCONT')
						@if($list->rack == 'YUNI' || $list->rack == 'AOI')
							{{ $list->rack }} {{ $list->y_row }}
						@else
							{{ $list->code }} 
						@endif
					@else
						@if($list->rack != 'LINE')
							{{ $list->rack }} 
						@else
							{{ $list->code }}
						@endif
					@endif
					
				</td>
				<td>
					@php
						if($list->rack == 'YUNI' || $list->rack == 'AOI')
							$_code = $list->rack.' '.$list->y_row;
						else
							$_code = $list->code

						//$list->rack == 'AOI 2' || $list->rack == 'AOI 1';
					@endphp
					
					@if($is_cancel)
						@if($list->rack == 'CANCEL ORDER' || $list->rack == 'CANCEL ITEM' ) 
							<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose" type="button" data-id="{{ $list->id }}" data-name="{{ $_code }}">
								Select 
							</button>
						@else
							PILIHAN TIDAK TERSEDIA
						@endif
					@else 
						<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose" type="button" data-id="{{ $list->id }}" data-name="{{ $_code }}">
							Select
						</button>
					@endif
				</td>
			</tr>
		@endforeach
	</tbody>
</table>


{!! $lists->appends(Request::except('page'))->render() !!}
