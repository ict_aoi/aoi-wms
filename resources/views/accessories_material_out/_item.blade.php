<script type="x-tmpl-mustache" id="accessories_material_out_table">
var test = '{{Auth::user()->division}}';
	{% #list %}
		<tr id="panel_{% _id %}" {% #is_error %} style="background-color:#fab1b1" {% /is_error %}>
			<td>{% no %}</td>
			<td>{% statistical_date %}</td>
			<td>{% season %}</td>
			<td>{% po_buyer %}</td>
			<td>{% item_code %}</td>
			<td>{% counter %}/{% total_carton %}</td>
			<td>{% style %}</td>
			<td>{% article_no %}</td>
			<td>{% uom_conversion %}</td>
			<td>{% qty_required %}</td>
			<td>{% qty_input %}</td>
			<td><input type="text" class="form-control note" id="note_{%_id%}" data-id="{% _id %}" value="-" ></td>
			{% #user_division %}
				<td><input type="text" class="form-control ict-log" id="ict_log_{%_id%}" data-id="{% _id %}" value="{%ict_log%}" ></td>
			{% /user_division %}
			<td>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-delete-item"><i class="fa icon-trash"></i></button>
			</td>
		</tr>
	{%/list%}
	<tr>
		<td colspan="13">
			<input type="text" id="barcode_value" name="barcode_value" class="form-control input-new barcode_value" placeholder="Please scan your barcode here" autocomplete="off"></input>
		</td>

	</tr>
</script>
