@extends('layouts.app', ['active' => 'accessories_material_out'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Out</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Material Out</li>
        </ul>

			<ul class="breadcrumb-elements">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
						<i class="icon-three-bars position-left"></i>
						Actions
						<span class="caret"></span>
					</a>
					
					<ul class="dropdown-menu dropdown-menu-right">
					@role(['admin-ict-acc'])
						<li><a href="{{ route('accessoriesMaterialOut.import') }}"><i class="icon-upload pull-right"></i>Import </a></li>
						<li><a href="{{ route('accessoriesMaterialOut.importCarton') }}"><i class="icon-upload pull-right"></i>Import Out Carton</a></li>
						<li><a href="{{ route('accessoriesMaterialOut.importOut') }}"><i class="icon-upload pull-right"></i>Import Out</a></li>
					@else
						<li><a href="{{ route('accessoriesMaterialOut.importCarton') }}"><i class="icon-upload pull-right"></i>Import Out Carton</a></li>
					@endrole

					@role(['import-out-acc'])
						<li><a href="{{ route('accessoriesMaterialOut.importOut') }}"><i class="icon-upload pull-right"></i>Import Out</a></li>
					@endrole
					</ul>
				</li>
			</ul>
    </div>
</div>
@endsection

@section('page-content')
		@role(['admin-ict-acc'])
			<div class="panel panel-default border-grey">
				<div class="panel-heading">
					<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
					<div class="heading-elements">
						<ul class="icons-list">
							<li><a data-action="collapse"></a></li>
						</ul>
					</div>
				</div>
				<div class="panel-body">
					@include('form.select', [
						'field' 			=> 'warehouse',
						'label' 			=> 'Warehouse',
						'default'			=> auth::user()->warehouse,
						'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
						'options' 			=> [
							'' 				=> '-- Select Warehouse --',
							'1000002' 		=> 'Warehouse Accessories AOI 1',
							'1000013' 		=> 'Warehouse Accessories AOI 2',
						],
						'class' 			=> 'select-search',
						'attributes' 		=> [
							'id' 			=> 'select_warehouse'
						]
					])
				</div>
			</div>
		@else 
			{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
		@endrole

		<div class="panel panel-default border-grey">
			<div class="panel-body">
				@role(['admin-ict-acc'])
					<div class="row">
						@include('form.text', [
							'label_col' 	=> 'col-lg-3 col-md-3 col-sm-12',
							'form_col' 		=> 'col-lg-9 col-md-9 col-sm-12',
							'field' 		=> '_ict_log',
							'label' 		=> 'Ict log',
							'placeholder' 	=> 'Please type log here',
							'attributes' 	=> [
								'id' 		=> '_ict_log'
							]
						])
					</div>
				@endrole
				
				<div class="table-responsive">
					<table class="table table-striped table-hover table-responsive">
						<thead>
							<tr>
								<th>#</th>
								<th>Statistical Date</th>
								<th>Season</th>
								<th>Po Buyer</th>
								<th>Item</th>
								<th>Total Package</th>
								<th>Style</th>
								<th>Article No</th>
								<th>Uom</th>
								<th>Need</th>
								<th>Qty On Barcode</th>
								<th>Note</th>   {{--hanya untuk input no kk --}}
								@role(['admin-ict-acc'])
									<th>Ict log</th>
								@endrole
								
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="tbody_accessories_material_out">
						</tbody>
					</table>
				</div>

	{!!
		Form::open([
			'role' 		=> 'form',
			'url' 		=> route('accessoriesMaterialOut.store'),
			'method' 	=> 'post',
			'enctype'	=> 'multipart/form-data',
			'id'		=> 'form'
		])
	!!}
				
				{!! Form::hidden('po_buyer',null, array('id' => 'po_buyer')) !!}
				{!! Form::hidden('is_cancel_order',null, array('id' => 'is_cancel_order')) !!}
				{!! Form::hidden('barcode_products',$return_array , array('id' => 'barcode_products')) !!}
				{!! Form::hidden('_to_destination_id', isset($hidden_value) ? $hidden_value : null, ['id' => '_to_destinationId']) !!}
				{!! Form::hidden('url_accessories_material_out_create', route('accessoriesMaterialOut.create'), ['id' => 'url_accessories_material_out_create']) !!}
				{!! Form::hidden('url_temporary_delete', route('temporary.delete'), ['id' => 'url_temporary_delete']) !!}
				{!! Form::hidden('url_temporary_reset', route('temporary.resetAll'), ['id' => 'url_temporary_reset']) !!}
				{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_id')) !!}

				@include('form.picklist', [
					'field' 		=> 'to_destination',
					'name' 			=> 'to_destination',
					'label' 		=> 'Destination',
					'readonly' 		=> true,
					'placeholder' 	=> 'Please select destination',
					'title' 		=> 'Please select destination',
					'label_col' 	=> 'col-lg-3 col-md-3 col-sm-12',
					'form_col' 		=> 'col-lg-9 col-md-9 col-sm-12',
				])

				<div class="form-group text-right" style="margin-top:10px">
					<button type="button" class="btn btn-yellow-cancel col-xs-6 btn-lg" id="reset_all_checkout" >Reset  <span class="glyphicon glyphicon-remove"></span></button>
					<button type="submit" class="btn btn-blue-success col-xs-6 btn-lg">Save <i class="icon-floppy-disk position-left"></i></button>
				</div>
			</div>
		</div>

	{!! Form::close() !!}
	<div class="alert alert-info alert-styled-right alert-bordered hidden" id="alert_checkout">
		<span class="text-semibold">Info for you !</span> <span id="msg_checkout"></span>
	</div>
@endsection

@section('page-modal')
	@include('form.modal_picklist', [
		'name' => 'to_destination',
		'title' => 'List Destination',
		'placeholder' => 'Search based on name',
	])
@endsection

@section('page-js')
	@include('accessories_material_out._item')
	<script src="{{ mix('js/accessories_material_out.js') }}"></script>
@endsection
