@extends('layouts.app', ['active' => 'accessories_material_out'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Out</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li><a href="{{ route('accessoriesMaterialOut.index') }}">Material Out</a></li>
				<li class="active">Import Carton</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<div class="heading-btn">
					<a class="btn btn-primary btn-icon" href="{{ route('accessoriesMaterialOut.downloadFormUploadCarton')}}" data-popup="tooltip" title="download form import" data-placement="bottom" data-original-title="download form import"><i class="icon-download"></i></a>
					<button id="upload_button" class="btn btn-success" data-popup="tooltip" title="upload form import" data-placement="bottom" data-original-title="upload form import"><i class="icon-upload"></i></button>
					{!!
						Form::open([
							'role'		=> 'form',
							'url' 		=> route('accessoriesMaterialOut.uploadCarton'),
							'method' 	=> 'post',
							'id' 		=> 'upload_data',
							'enctype' 	=> 'multipart/form-data'
						])
						!!}
							<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
							<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
						{!! Form::close() !!}
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover table-responsive">
					<thead>
						<tr>
							<td>No</td>
							<td>Barcode</td>
							<td>Po Supplier</td>
							<td>Po Buyer</td>
							<td>Item Code</td>
							<td>Uom</td>
							<td>Qty</td>
							<td>Status</td>
						</tr>
					</thead>
					<tbody id="tbody_list">
					</tbody>
				</table>
			</div>
		</div>
	</div>
	{!! Form::hidden('page','import', array('id' => 'page')) !!}
	{!! Form::hidden('list','[]',array('id' => 'list')) !!}
@endsection

@section('page-js')
	@include('accessories_material_out._item_import_carton')
	<script src="{{ mix('js/accessories_import_material_out_carton.js') }}"></script>
@endsection
