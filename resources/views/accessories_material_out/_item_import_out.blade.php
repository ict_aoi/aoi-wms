<script type="x-tmpl-mustache" id="list_table">
	{% #item %}
		<tr id="panel_{% _id %}" {% #is_error %} style="background-color:#fab1b1" {% /is_error %}>
			<td>{% no %}</td>
			<td>{% barcode %}</td>
			<td>{% document_no %}</td>
			<td>{% po_buyer %}</td>
			<td>{% item_code %}</td>
			<td>{% uom %}</td>
			<td>{% qty %}</td>
			<td>{% status %}</td>
		</tr>
	{%/item%}
</script>
