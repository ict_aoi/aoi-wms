@extends('layouts.app', ['active' => 'accessories_material_stock_data'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Stock</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="{{ route('accessoriesMaterialStock.index') }}" id="url_materialother">Material Stock</a></li>
            <li class="active">Create</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	@role(['admin-ict-acc'])
		<div class="panel panel-default border-grey">
			<div class="panel-body">
				@include('form.select', [
					'field' 			=> 'warehouse',
					'label' 			=> 'Warehouse',
					'default'			=> auth::user()->warehouse,
					'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
					'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
					'options' 			=> [
						'1000002' 		=> 'Warehouse Accessories AOI 1',
						'1000013' 		=> 'Warehouse Accessories AOI 2',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_warehouse'
					]
				])
			</div>
		</div>
	@else 
		{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
	@endrole

	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				{!!
					Form::open(array(
							'class' => 'heading-form',
							'role' => 'form',
							'url' => route('accessoriesMaterialStock.exportSupplier'),
							'method' => 'get',
							
						))
				!!}
					<div class="from-group text-right">
						<button type="submit" class="btn btn-default">Export Master Supplier <i class="icon-file-excel position-left"></i></button>
					</div>
				{!! Form::close() !!}
					&nbsp
					<a class="btn btn-primary btn-icon" href="{{ route('accessoriesMaterialStock.export')}}" data-popup="tooltip" title="download form import" data-placement="bottom" data-original-title="download form import"><i class="icon-download"></i></a>
					<button type="button" class="btn btn-success btn-icon" id="upload_button" data-popup="tooltip" title="upload form import" data-placement="bottom" data-original-title="upload form import"><i class="icon-upload"></i></button>

				{!!
					Form::open([
						'role' => 'form',
						'url' => route('accessoriesMaterialStock.import'),
						'method' => 'POST',
						'id' => 'upload_file_allocation',
						'enctype' => 'multipart/form-data'
					])
				!!}
					<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
					<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
				{!! Form::close() !!}

				</div>
		</div>

		<div class="panel-body">
			{!!
			    Form::open([
			      	'role' => 'form',
			      	'url' => route('accessoriesMaterialStock.store'),
			      	'method' => 'post',
			      	'enctype' => 'multipart/form-data',
					'class' => 'form-horizontal',
					'id'=> 'form'
			    ])
			!!}
				{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_id')) !!}
				{!! Form::hidden('flag',null , array('id' => 'flag')) !!}
			<div class="table-responsive">
				<table class="tabletable-striped table-hover table-responsive">
					<thead>
						<tr>
							<th>#</th>
							<th>Type Stock</th>
							<th>Supplier Name</th>
							<th>Po Supplier</th>
							<th>Po Buyer</th>
							<th>Locator</th>
							<th>Item</th>
							<th>Uom</th>
							<th>Qty</th>
							<th>Source</th>
							<th>Remark</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody id="tbody-material-other">
					</tbody>
				</table>
			</div>

			<div class="form-group text-right" style="margin-top: 10px;">
				{!! Form::hidden('materials','[]' , array('id' => 'materials')) !!}
				<button type="submit" class="btn  col-xs-12 btn-blue-success">Save <i class="icon-floppy-disk position-left"></i></button>
			</div>
		</div>
	</div>
	
	
	{!! Form::close() !!}
	{!! Form::hidden('mapping_stocks',$mapping_stocks , array('id' => 'mapping_stocks')) !!}
	{!! Form::hidden('suppliers',$suppliers , array('id' => 'suppliers')) !!}
@endsection

@section('page-modal')
	@include('form.modal_picklist', [
		'name' => 'item',
		'title' => 'List Item',
		'placeholder' => 'Search based on name / code',
	])

	@include('form.modal_picklist', [
		'name' => 'document_no',
		'title' => 'List Po Supplier',
		'placeholder' => 'Search based on Nomor PO',
	])
	
	@include('form.modal_picklist', [
		'name' => 'supplier_name',
		'title' => 'List Supplier',
		'placeholder' => 'Search based on name supplier',
	])

	@include('form.modal_picklist', [
		'name' => 'locator',
		'title' => 'List Locator',
		'placeholder' => 'Search based on name / code',
	])
@endsection

@section('page-js')
	@include('accessories_material_stock._item')
	<script src="{{ mix('js/accessories_material_stock.js') }}"></script>
@endsection
