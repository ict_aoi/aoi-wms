<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
			<th>Locator Code</th>
			<th>Action</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>{{ $list->code }}</td>
				<td>
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose" 
						type="button" data-id="{{ $list->id }}" 
						data-name="{{ $list->code }}">Select</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
