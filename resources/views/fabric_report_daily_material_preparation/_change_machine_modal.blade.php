<div id="machineModal" class="modal fade">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class="modal-header bg-blue">
					<h5 class="modal-title">Preparation for <span id="preparation_header"></span></h5>
				</div>
			</div>
				{!!
					Form::open([
						'role' 		=> 'form',
						'url' 		=> route('fabricReportDailyMaterialPreparation.updateMachine'),
						'method' 	=>'PUT',
						'class' 	=> 'form-horizontal',
						'id' 		=> 'machine_form'
					])
				!!}
				<div class="modal-body">
				<div class="row"> 
                    @include('form.select', [
                        'field' => 'machine',
                        'label' => 'Machine',
                        'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                        'options' => [
                            'MANUAL' => 'MANUAL',
                            'STEAM' => 'MACHINE',
                        ],
                        'class' => 'select-search',
                        'attributes' => [
                            'id' => 'select_machine'
                        ]
                    ])

					@include('form.textarea', [
						'field' 		=> 'remark',
						'label' 		=> 'Remark',
						'label_col' 	=> 'col-xs-12',
						'form_col' 		=> 'col-xs-12',
						'placeholder' 	=> 'Note',
						'attributes' 	=> [
							'id' 		=> 'remark',
							'rows' 		=> 3,
							'resize' 	=> false
						]
					])

					{!! Form::hidden('id','', array('id' => 'preparation_fabric_id')) !!}
				</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close <span class="glyphicon glyphicon-remove"></span></button>
					<button type="submit" class="btn btn-blue-success">Save <i class="icon-floppy-disk position-left"></i></button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>