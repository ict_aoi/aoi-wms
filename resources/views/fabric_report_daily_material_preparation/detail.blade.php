@extends('layouts.app', ['active' => 'fabric_report_daily_material_preparation'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Daily Material Preparation</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li><a href="{{ route('fabricReportDailyMaterialPreparation.index') }}">Daily Material Preparation</a></li>
				<li class="active">Detail</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
	<div class="panel-heading">
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
			</ul>
		</div>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6 col-lg-6 col-sm-12">
				<p>Po Supllier <b>{{ $material_preparation->document_no }}</b></p>
				<p>Supplier Name <b>{{ $material_preparation->supplier_name }}</b></p>
				<p>Warehouse Preparation <b>{{ ($material_preparation->warehouse_id == '1000001' ? 'Warehouse fabric AOI 1' : 'Warehouse fabric AOI 2') }}</b></p>
				<p>Planning Date <b>{{ ($material_preparation->planning_date) ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $material_preparation->planning_date)->format('d/M/Y') : $material_preparation->planning_date }}</b></p>
				<p>Article No <b>{{ $material_preparation->article_no }}</b></p>
				<p>Style <b>{{ $material_preparation->_style }}</b></p>
			</div>
			<div class="col-md-6 col-lg-6 col-sm-12">
				<p>Piping : @if($material_preparation->is_piping) <span class="label label-info">Piping</span>
									@else <span class="label label-default">Non Piping</span>
									@endif</p>
				<p>Item Code :  <b>{{ $material_preparation->item_code }}</b></p>
				<p>Item Code Source :  <b>{{ $material_preparation->item_code_source }}</b></p>
				<p>Total Preparation :  <b>{{ number_format($material_preparation->total_reserved_qty, 4, '.', ',') }} ({{ $material_preparation->uom }})</b></p>
				<p>Total Relax :  <b><span id='text_total_qty_rilex'>{{ number_format($material_preparation->total_qty_rilex, 4, '.', ',') }} </span>({{ $material_preparation->uom }})</b></p>

			</div>
		</div>

		{!!
			Form::open(array(
				'class' 	=> 'heading-form',
				'role' 		=> 'form',
				'url' 		=> route('fabricReportDailyMaterialPreparation.exportDetail',$material_preparation->id),
				'method' 	=> 'get',
				'target' 	=> '_blank'		
			))
		!!}
		<button type="submit" class="btn btn-default col-xs-12">Export Detail All <i class="icon-file-excel position-left"></i></button>
	{!! Form::close() !!}
	</div>
</div>

<div class="panel panel-flat panel-collapsed">
        <div class="panel-heading">
            <h5 class="panel-title">Move Preparation   <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
			{!!
				Form::open(array(
					'class' 		=> 'form-horizontal',
					'role' 			=> 'form',
					'url' 			=> route('fabricReportDailyMaterialPreparation.storeChangePlan',$material_preparation->id),
					'method' 		=> 'post',
					'id'			=> 'formChangePlan'	
				))
			!!}

				<div class="row">
					<div class="col-md-6 col-lg-6 col-sm-12">
						@include('form.select', [
							'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
							'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
							'field' 		=> 'is_piping',
							'label' 		=> 'Piping',
							'options'		=> [
								''	=>'Please Select',
								'0'	=>'Bukan Piping',
								'1'	=>'Piping'
							],
							'attributes' => [
								'id' => 'is_piping'
							]
						])
						
						@include('form.date', [
							'field' 		=> 'start_date',
							'label' 		=> 'Planning Date',
							'class' 		=> 'start_date',
							'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
							'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
							'placeholder' 	=> 'dd/mm/yyyy',
							'attributes' 	=> [
								'id' 			=> 'start_date',
								'autocomplete' 	=> 'off',
								'readonly' 		=> 'readonly'
							]
						])

						@include('form.select', [
							'field' => 'article',
							'label' => 'Article No',
							'label_col' => 'col-md-2 col-lg-2 col-sm-12',
							'form_col' => 'col-md-10 col-lg-10 col-sm-12',
							'options' => [
								'' => '-- Select Article --',
							],
							'class' => 'select-search',
							'attributes' => [
								'id' => 'select_article'
							]
						])

						@include('form.select', [
							'field' => 'style',
							'label' => 'Style',
							'label_col' => 'col-md-2 col-lg-2 col-sm-12',
							'form_col' => 'col-md-10 col-lg-10 col-sm-12',
							'options' => [
								'' => '-- Select Style --',
							],
							'class' => 'select-search',
							'attributes' => [
								'id' => 'select_style'
							]
						])
					</div>

					<div class="col-md-6 col-lg-6 col-sm-12">
						@include('form.text', [
							'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
							'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
							'field' 			=> 'qty_preparation',
							'label' 			=> 'Qty Preparation (YDS)',
							'placeholder' 		=> 'Silahkan masukan Total Qty yang dipersiapkan',
							'attributes' 		=> [
								'id' 			=> 'qty_preparation',
								'readonly' 		=> 'readonly',
							]
						])
						
						@include('form.text', [
							'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
							'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
							'field' 		=> 'qty_relax',
							'label' 		=> 'Qty Relax (YDS)',
							'attributes' 	=> [
								'id' 		=> 'qty_relax',
								'readonly' => 'readonly',
							]
						])
						
						@include('form.text', [
							'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
							'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
							'field' 		=> 'qty_outstanding',
							'label' 		=> 'Qty Outstanding (YDS)',
							'attributes' 	=> [
								'id' 		=> 'qty_outstanding',
								'readonly' => 'readonly',
							]
						])
					</div>
				</div>
				

				{!! Form::hidden('url_get_planning_per_article', route('fabricReportDailyMaterialPreparation.getPlanningPerArticle'), array('id' => 'url_get_planning_per_article')) !!}
				{!! Form::hidden('url_get_planning_per_style', route('fabricReportDailyMaterialPreparation.getPlanningPerStyle'), array('id' => 'url_get_planning_per_style')) !!}
				{!! Form::hidden('url_get_planning_per_date', route('fabricReportDailyMaterialPreparation.getPlanningPerDate'), array('id' => 'url_get_planning_per_date')) !!}
				

				{!! Form::hidden('total_reserved_qty',	$material_preparation->total_reserved_qty, array('id' => 'total_reserved_qty')) !!}
				{!! Form::hidden('total_qty_rilex',	$material_preparation->total_qty_rilex, array('id' => 'total_qty_rilex')) !!}
				{!! Form::hidden('saldo_qty_reserved',	$material_preparation->total_qty_rilex, array('id' => 'saldo_qty_reserved')) !!}
				{!! Form::hidden('_planning_date',	null, array('id' => '_planning_date')) !!}
				{!! Form::hidden('new_material_preparation_fabric_id','', array('id' => 'new_material_preparation_fabric_id')) !!}
				{!! Form::hidden('material_preparation_fabric_id',	$material_preparation->id, array('id' => 'material_preparation_fabric_id')) !!}
				{!! Form::hidden('warehouse_id',	$material_preparation->warehouse_id, array('id' => 'warehouse_id')) !!}
				
				{!! Form::hidden('list_preparation_material',$details, array('id' => 'list_preparation_materials')) !!}
				{!! Form::hidden('list_material','[]', array('id' => 'list_materials')) !!}
				<button type="button" onClick="checkedAll()" class="btn btn-yellow-cancel col-xs-12">Check | Uncheck All <i class="icon-checkbox-checked position-left"></i></button>
				<button type="submit" class="btn btn-blue-success col-xs-12">Save Move Plan <i class="icon-floppy-disk position-left"></i></button>
				
			{!! Form::close() !!}
        </div>
    </div>

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover table-responsive" id="daily_detail_material_preparation_table">
					<thead>
						<tr>
							<th>ID</th>
							<th>#</th>
							<th>Preparation Date</th>
							<th>Preparation By</th>
							<th>Movement Out Date</th>
							<th>Barcode</th>
							<th>No Roll</th>
							<th>Batch Number</th>
							<th>Begin Width</th>
							<th>Middle Width</th>
							<th>End Width</th>
							<th>Actual Width</th>
							<th>Actual Length</th>
							<th>Actual Lot</th>
							<th>Qty Prepare (In Yard)</th>
							<th>Action</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
	{!! Form::hidden('page','detail', array('id' => 'page')) !!}
	{!! Form::hidden('url_fabric_report_daily_material_preparation_detail',route('fabricReportDailyMaterialPreparation.dataDetail',$material_preparation->id), array('id' => 'url_fabric_report_daily_material_preparation_detail')) !!}

	{!!
		Form::open([
			'role' 			=> 'form',
			'url' 			=> route('fabricReportDailyMaterialPreparation.printBarcodeMovePrepare'),
			'method' 		=> 'post',
			'id'			=> 'getBarcode',
			'target' 		=> '_blank'
		])
	!!}
		{!! Form::hidden('list_barcodes','[]' , array('id' => 'list_barcodes')) !!}
		<div class="form-group text-right" style="margin-top: 10px;">
			<button type="submit" class="btn hidden">Print <i class="icon-printer position-left"></i></button>
		</div>
	{!! Form::close() !!}

@endsection

@section('page-js')
	<script src="{{ mix('js/fabric_report_daily_material_preparation.js') }}"></script>
@endsection
