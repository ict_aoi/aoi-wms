
<!DOCTYPE html>
<html>
<head>
    <title>BARCODE :: REPRINT BARCODE PREPARATION </title>
	<link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/print_barcode_fabric.css')) }}">
</head>
    <body>
		@foreach($barcode_preparations as $barcode_preparation)
			<div class="outer">
						<div class="title">
							<span style="@if(strlen($barcode_preparation->materialStock->document_no)>50) font-size: 10px @elseif(strlen($barcode_preparation->materialStock->document_no)>30 && strlen($barcode_preparation->materialStock->document_no)<50) font-size: 12px @else font-size: 25px @endif">{{ $barcode_preparation->materialStock->document_no }}</span>
						</div>
						<div class="isi">
							<div class="isi1">
								<span style="@if(strlen($barcode_preparation->materialStock->item_code)>=50) font-size: 12px @elseif(strlen($barcode_preparation->materialStock->item_code)>20 && strlen($barcode_preparation->materialStock->item_code)<50) font-size: 20px @else font-size: 30px @endif">{{ $barcode_preparation->materialStock->item_code }}</span>
							</div>
							<div class="isi1" style="
								@if(strlen($barcode_preparation->materialStock->color)>100 && strlen($barcode_preparation->materialStock->color)<150) font-size: 12px;line-height:0.3cm;
								@elseif(strlen($barcode_preparation->materialStock->color)>150 && strlen($barcode_preparation->materialStock->color)<175) height:40px; 
								@elseif(strlen($barcode_preparation->materialStock->color)>175 && strlen($barcode_preparation->materialStock->color)<200) height:75px  
								@endif">
								<span style="@if(strlen($barcode_preparation->materialStock->color)>15) font-size: 10px @else font-size: 30px @endif">{{ $barcode_preparation->materialStock->color }}</span>
							</div>
							<div class="isi1">
								<span style="@if(strlen($barcode_preparation->materialStock->batch_number)>15) font-size: 25px @else font-size: 30px @endif"> {{ $barcode_preparation->materialStock->batch_number }}</span>
							</div>
							
							<div class="isi2">
								<div class="block-kiri" style="justify-content: center; height: 45px;">
									<span style="font-size: 12px;word-wrap: break-word;">{{ $barcode_preparation->materialStock->supplier_name }}  </span>
								</div>
								<div class="block-kiri" style="justify-content: center; height: 45px;">
									<span style="font-size: 20px;word-wrap: break-word;">{{ number_format($barcode_preparation->reserved_qty, 2, '.', ',') }} {{ strtoupper($barcode_preparation->materialStock->uom) }}   </span>
								</div>
								<p style="font-size: 15px;word-wrap: break-word;">NO ROLL : <br/><span style="
									@if(strlen($barcode_preparation->materialStock->nomor_roll)>=10 && strlen($barcode_preparation->materialStock->nomor_roll)<15) font-size: 25px; 
									@elseif(strlen($barcode_preparation->materialStock->nomor_roll)>=4 && strlen($barcode_preparation->materialStock->nomor_roll)<10) font-size: 35px 
									@else font-size: 70px @endif">{{ $barcode_preparation->materialStock->nomor_roll }}
								</p>
							
							</div>
							<div class="isi3">
								<div class="block-kiri" style="height:30px;font-size:25px;">
									@if($barcode_preparation->materialStock->jenis_po) {{ $barcode_preparation->materialStock->jenis_po}} @else - @endif
								</div>
								<div class="block-kiri" style="height: 120px;">
									<img style="height: 2.5cm; width: 4.3cm; margin-top: 10px; margin-left: 2px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$barcode_preparation->barcode", 'C128') }}" alt="barcode"   />			
									<br/><span style="font-size: 12px;word-wrap: break-word;">{{ $barcode_preparation->barcode }}</span>
						
								</div>
								<div class="block-kiri">
									No. Invoice {{ $barcode_preparation->materialStock->no_invoice }}
								</div>
								<div class="block-kiri">
									@if($barcode_preparation->materialPreparationFabric->planning_date)
										Tgl. Planning {{ $barcode_preparation->materialPreparationFabric->planning_date->format('d/M/Y') }}
									@endif
										<br/>Tgl. Prepare {{ $barcode_preparation->created_at->format('d/M/Y h:i:s')  }}
									<br/>

								</div>
								
							</div>
						</div>
								
			</div>
		@endforeach
    </body>
</html>