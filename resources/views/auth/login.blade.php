<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>WMS | Warehouse Management System</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <!-- Global stylesheets -->

	    <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	    <link href="{{ mix('css/backend.css') }}" rel="stylesheet" type="text/css">
       
    </head>
    <body class="login-container login-cover">
        <div class="navbar-inverse-login bg-transparent">
            <div class="navbar-header">
                <ul class="nav navbar-nav pull-right visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile" class="legitRipple"><i class="icon-menu7"></i></a></li>
                </ul>
            </div>

            <div class="navbar-collapse collapse" id="navbar-mobile">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#" class="legitRipple">
                        <i class="icon-database"></i> Material Requirement
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('memoRequestProduction.index' )}}" class="legitRipple">
                            <i class="icon-archive"></i>  Memo Request Production (MRP) 
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('dashboard1' )}}" class="legitRipple" target="_blank">
                            <i class="icon-chart"></i> Dashboard 
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        
        <div class="page-container">
            <div class="page-content">
                <div class="content-wrapper">
                    <div class="content">
                        <form  method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <div class="panel panel-body login-form">
                                <div class="text-center">
                                    <div><img src="{{ asset('images/icon_wms.png') }}" width="100px" height="100px"></div>
                                    <h5 class="content-group-lg">Login to your account <small class="display-block">Enter your credentials</small></h5>
                                </div>

                                <div class="form-group has-feedback has-feedback-left">
                                    <input id="nik" type="texboxt" placeholder="Input your nik here" class="form-control" name="nik" value="{{ old('nik') }}" required autofocus autocomplete="off">
                                    <div class="form-control-feedback">
                                        <i class="icon-user text-muted"></i>
                                    </div>
                                </div>

                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="password" class="form-control" placeholder="Input your password here" name="password" required>
                                    <div class="form-control-feedback">
                                        <i class="icon-lock2 text-muted"></i>
                                    </div>
                                </div>

                                @if ($errors->has('employee_code'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('employee_code') }}</strong>
                                    </span>
                                @elseif ($errors->has('email'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @elseif (session()->has('flash_notification.message'))
                                    <span class="help-block text-danger">
                                        <strong>{!! session()->get('flash_notification.message') !!}</strong>
                                    </span>   
                                @endif
                                <div class="form-group">
                                    <button type="submit" class="btn btn-yellow-cancel btn-block">Login <i class="icon-circle-right2 position-right"></i></button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ mix('js/backend.js') }}"></script>
	</body>
</html>
