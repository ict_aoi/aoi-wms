@extends('layouts.app', ['active' => 'handover_material_report'])

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('report.summaryMaterialStockFabric') }}"></a><span class="text-semibold">MATERIAL HANDOVER</span></h4>
						<ul class="breadcrumb breadcrumb-caret position-left">
							<li><a href="{{ route('report.handoverMaterial') }}">Report</a></li>
                            <li><a href="{{ route('report.handoverMaterial') }}">Material Handover</a></li>
                            <li class="active">Detail</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini digunakan untuk melihat detail laporan Material yang perlu di pindah tangankan.
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-basic','id'=>'handover-datatables']) !!}
			</div>
		</div>
	</div>
@endsection

@section('content-js')
	{!! $html->scripts() !!}
	<script type="text/javascript" src="{{ asset(elixir('js/report_handover_fabric.js'))}}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
@endsection
