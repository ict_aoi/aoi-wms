@extends('layouts.app', ['active' => 'actual_width_fabric'])

@section('content')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title" style="padding-top:0px">
				<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">ACTUAL WIDTH REPORT</span></h4>

				<ul class="breadcrumb breadcrumb-caret position-left">
					<li><a href="{{ route('dashboard') }}">Report</a></li>
					<li class="active">Actual Width</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			{!!
				Form::open(array(
					'class' => 'form-signin',
					'role' => 'form',
					'url' => route('report.actualWidthExcel'),
					'method' => 'get',
					'id' => 'form',
					'target' => '_blank'
				))
			!!}

			@include('form.date', [
				'field' => 'start_date',
				'label' => 'Start PCD (00:00)',
				'class' => ' datepicker',
				'placeholder' => 'dd/mm/yyyy',
				'label_col' => 'col-md-4',
				'form_col' => 'col-sm-8',
				'attributes' => [
					'id' => 'start_date',
					'autocomplete' => 'off'
				]
			])

			@include('form.date', [
				'field' => 'end_date',
				'label' => 'End PCD (23:59)',
				'class' => ' datepicker',
				'placeholder' => 'dd/mm/yyyy',
				'label_col' => 'col-md-4',
				'form_col' => 'col-sm-8',
				'attributes' => [
					'id' => 'end_date',
					'autocomplete' => 'off'
				]
			])
			<div class="from-group text-right">
				<button type="submit" class="btn btn-info btn-md" id="exportExcel" style="margin-top:15px">EXPORT EXCEL <i class="icon-file-excel position-left"></i></button>
			</div>
			{!! Form::close() !!}
		</div>
@endsection

@section('content-js')
@endsection
