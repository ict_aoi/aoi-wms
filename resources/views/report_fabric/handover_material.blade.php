@extends('layouts.app', ['active' => 'handover_material_report'])

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('report.summaryMaterialStockFabric') }}"></a><span class="text-semibold">MATERIAL HANDOVER</span></h4>
						<ul class="breadcrumb breadcrumb-caret position-left">
							<li><a href="{{ route('report.handoverMaterial') }}">Report</a></li>
							<li class="active">Material Handover</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini digunakan untuk melihat laporan Material yang perlu di pindah tangankan.
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				@if(Auth::user()->hasRole(['mm-staff','admin-ict-fabric']))
					{!!
						Form::open(array(
							'class' => 'heading-form',
							'id'=>'form-warehouse',
							'role' => 'form',
							'url' => route('report.handoverMaterial'),
							'method' => 'get',
						))
					!!}
					@include('form.select', [
						'field' => 'warehouse',
						'default' => $warehouse,
						'options' => [
							''=>'SELECT WAREHOUSE',
							'1000001'=>'AOI 1',
							'1000011'=>'AOI 2',
						],
						'attributes' => [
							'id' => 'warehouse'
						]
					])
					{!! Form::close() !!}
				@endif
				<a target="_blank" href="{{route('report.handoverMaterialExcel',[
					'warehouse'=>$warehouse
				])}}" class="btn btn-success" id="btn_export"><i class="icon-file-excel"></i> EXPORT</a>
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-basic','id'=>'handover-datatables']) !!}
			</div>
		</div>
	</div>
@endsection

@section('content-js')
	{!! $html->scripts() !!}
	<script type="text/javascript" src="{{ asset(elixir('js/report_handover_fabric.js'))}}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
@endsection
