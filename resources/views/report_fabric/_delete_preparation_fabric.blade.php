<div id="deletePreparationFabricModal" class="modal fade">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">DELETE PREPARATION FABRIC</h5>
			</div>
				{!!
					Form::open([
						'role' => 'form',
						'url' => '#',
						'method' => 'post',
						'class' => 'form-horizontal',
						'id'=> 'delete_preparation_fabric_form'
					])
				!!}
				<div class="modal-body">
				<div class="row">  
						<div class="col-md-12">
                    @include('form.text', [
                            'field' => 'document_no',
							'label' => 'Document No',
							'label_col' => 'col-xs-4',
							'form_col' => 'col-xs-8',
                            'attributes' => [
								'id' => 'delete_document_no',
								'readonly' => true
                            ]
                        ])

					@include('form.text', [
						'field' => 'item_code',
						'label' => 'Item Code',
						'label_col' => 'col-xs-4',
						'form_col' => 'col-xs-8',    
						'attributes' => [
							'id' => 'delete_item_code',
							'readonly' => true
						]
					])
                    
                    @include('form.text', [
						'field' => 'planning_date',
						'label' => 'Planning',
						'label_col' => 'col-xs-4',
						'form_col' => 'col-xs-8',    
						'attributes' => [
							'id' => 'delete_planning_date',
							'readonly' => true
						]
					])
						</div>
						<div class="col-md-12">
					@include('form.textarea', [
						'field' => 'delete_reason',
						'label' => 'Alasan',
						'label_col' => 'col-xs-12',
						'form_col' => 'col-xs-12',
						'placeholder' => 'Tulis alasan anda',
						'attributes' => [
							'id' => 'delete_reason',
							'rows' => 3,
						]
					])
						</div>

					

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-danger">Delete</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>