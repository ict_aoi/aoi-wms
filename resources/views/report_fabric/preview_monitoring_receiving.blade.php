<html>
<head>
	<title>PRINT :: MONITORING RECEIVING</title>
	<link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/print_mrp.css')) }}">

</head>
<body>
	<div class="outer">
		<div class="header">
			<div class="header-tengah">LAPORAN MONITORING RECEIVING - APPAREL WAREHOUSE MANAGEMENT SYSTEM</div>
		</div>
		<div class="isi">
			<table border="1px" cellspacing="0" style="width: 100%">
				<thead style="background-color: #2196F3;color:black">
					<tr>
						<th class="text-center">#</th>
						<th>TANGGAL TERIMA</th>
						<th>NO. INVOICE</th>
						<th>NO. PO SUPPLIER</th>
						<th>NAMA SUPPLIER</th>
						<th>KODE ITEM</th>
						<th>WARNA</th>
						<th>TOTAL ROLL</th>
						<th>TOTAL YARD</th>
						<th>GUDANG PENERIMA</th>
						<th>PENERIMA</th>
					</tr>
				</thead>
				<tbody>
					@foreach($monitor_receivements as $key => $data)
						@php
							if($data->warehouse_id == '1000001')
								$_warehouse = 'Fabric AOI 1';
							elseif($data->warehouse_id == '1000011')
								$_warehouse = 'Fabric AOI 2';

						@endphp

						<tr>
							<td>{{ $key+1}}</td>
							<td>{{ $data->arrival_date }}</td>
							<td>{{ $data->no_invoice }}</td>
							<td>{{ $data->document_no }}</td>
							<td>{{ $data->supplier_name }}</td>
							<td>{{ $data->item_code }}</td>
							<td>{{ $data->color }}</td>
							<td>{{ $data->total_roll }}</td>
							<td>{{ $data->total_yard }}</td>
							<td>{{ $_warehouse }}</td>
							<td>{{ $data->user_receive }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>		
	</div>
</body>
</html>