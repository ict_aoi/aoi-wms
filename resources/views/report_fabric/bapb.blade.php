@extends('layouts.app', ['side' => 'allocation-bapb'])

@section('content')
<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">BAPB</span></h4>

						<ul class="breadcrumb breadcrumb-caret position-left">
							<a href="#" id="url_index" class="hidden"></a>
							<li><a href="{{ route('dashboard') }}">Report</a></li>
							<li class="active">BAPB</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-heading">
		<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
		<div class="heading-elements">
		</div>
		
	</div>
		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-basic']) !!}
			</div>
		</div>
	</div>
@endsection

@section('content-js')
{!! $html->scripts() !!}
@endsection