@extends('layouts.app', ['active' => 'report_inspect_qc'])

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('report.inspectLab') }}"></a><span class="text-semibold">DETAIL INSPECT</span></h4>
						<ul class="breadcrumb breadcrumb-caret position-left">
							<li>Report</li>
							<li><a href="{{ route('report.fabric.inspectQc') }}">Inspect QC</a></li>
							<li class="active">Detail</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini digunakan oleh bagian Inspect QC, untuk melihat detail laporan inspect qc.
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
		</div>

		{{csrf_field()}}
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th rowspan="2" style="text-align: center">START POINT CHECK</th>
							<th rowspan="2" style="text-align: center">END POINT CHECK</th>
							<th rowspan="2" style="text-align: center">DEFECT CODE</th>
							<th colspan="4" style="text-align: center">DEFECT CODE VALUE</th>
						</tr>
						<tr>
							<th style="text-align: center">1</th>
							<th style="text-align: center">2</th>
							<th style="text-align: center">3</th>
							<th style="text-align: center">4</th>
						</tr>
					</thead>
					<tbody>
						@foreach($data as $i)
						<tr style="text-align: center">
							<td>{{$i->start_point_check}}</td>
							<td>{{$i->end_point_check}}</td>
							<td>{{$i->defect_code}}</td>
							@if($i->is_selected_1)
								<td><i class="icon-check"></i></td>
							@else
								<td></td>
							@endif

							@if($i->is_selected_2)
								<td><i class="icon-check"></i></td>
							@else
								<td></td>
							@endif

							@if($i->is_selected_3)
								<td><i class="icon-check"></i></td>
							@else
								<td></td>
							@endif

							@if($i->is_selected_4)
								<td><i class="icon-check"></i></td>
							@else
								<td></td>
							@endif

						</tr>
						@endforeach
					</tbody>
					<tfoot>
						<tr style="text-align: center">
							<td></td>
							<td></td>
							<td>Point</td>
							<td>{{$header->point_1}}</td>
							<td>{{$header->point_2}}</td>
							<td>{{$header->point_3}}</td>
							<td>{{$header->point_4}}</td>
						</tr>
						<tr style="text-align: center">
							<td></td>
							<td></td>
							<td>Percentage</td>
							<td>{{$header->percentage}}</td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="text-align: center">
							<td></td>
							<td></td>
							<td>Total Point</td>
							<td>{{$header->total_point}}</td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('content-js')
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
@endsection
