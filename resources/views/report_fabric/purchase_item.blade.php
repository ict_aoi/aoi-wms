@extends('layouts.app', ['active' => 'report-purchase-fabric'])

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">REPORT PURCHASE ITEM</span></h4>

						<ul class="breadcrumb breadcrumb-caret position-left">
							<a href="{{ route('materialqcfabric.index') }}" id="url_index" class="hidden"></a>
							<li><a href="{{ route('dashboard') }}">Report</a></li>
							<li class="active">Purchase Item</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini digunakan oleh bagian MM, untuk melihat data alokasi yang beralas dari pembelian.
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-heading">
		<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
		<div class="heading-elements">
			<div class="heading-btn">
				<button type="button" id="btn-cancel" class="btn btn-danger">CANCEL</button>
			</div>
		</div>
		
	</div>
		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-basic','id'=>'purchase-datatables']) !!}
			</div>
		</div>
	</div>
	<a href="{{route('report.purchaseItemFabric.cancelSelectedAllocationPurchase')}}" id="route_cancel" class="hidden"></a>
@endsection

@section('content-js')
	{!! $html->scripts() !!}
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/monitoring_purchase_item_fabric.js'))}}"></script>
@endsection
