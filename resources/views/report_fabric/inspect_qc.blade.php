@extends('layouts.app', ['active' => 'report_inspect_qc'])

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">REPORT INSPECT QC</span></h4>

						<ul class="breadcrumb breadcrumb-caret position-left">
							<a href="{{ route('materialqcfabric.index') }}" id="url_index" class="hidden"></a>
							<li><a href="{{ route('dashboard') }}">Report</a></li>
							<li class="active">Inspect QC</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini digunakan oleh bagian Inspect QC, untuk melihat data material yang mereka scan untuk di jadikan bahan sampling.
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="tabbable">
			<div class="panel-heading">
				<ul class="nav nav-tabs">
					<li class="@if($active_tab == 'detail') active @endif"><a href="#basic-tab1" data-toggle="tab" aria-expanded="false" onclick="">Detail</a></li>
					<li class="@if($active_tab == 'summary') active @endif"><a href="#basic-tab2" data-toggle="tab" aria-expanded="true" onclick="summaryDataTables()">Summary</a></li>
				</ul>
			</div>
			<div class="panel-body">
				<div class="tab-content">
					<div class="tab-pane @if($active_tab == 'detail') active @endif" id="basic-tab1">
						<div class="panel-heading">
							<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
							<div class="heading-elements">
								<div class="from-group text-right">
									{!!
										Form::open(array(
											'class' => 'heading-form',
											'role' => 'form',
											'url' => route('report.fabric.inspectQc'),
											'method' => 'get',
											'id'=>'filter_tanggal'
										))
									!!}
									{!! Form::hidden('active_tab_detail',true, array('id' => 'active_tab_detail')) !!}
									<div class="form-group">
										@include('form.date', [
											'field' => 'start_date',
											'label' => 'FROM (00:00)',
											'class' => ' datepicker',
											'default'=> $start,
											'placeholder' => 'dd/mm/yyyy',
											'attributes' => [
												'id' => 'start_date',
												'autocomplete' => 'off'
											]
										])
									</div>
									<div class="form-group">
										@include('form.date', [
											'field' => 'end_date',
											'label' => 'TO (23:59)',
											'class' => ' datepicker',
											'default'=> $end,
											'placeholder' => 'dd/mm/yyyy',
											'attributes' => [
												'id' => 'end_date',
												'autocomplete' => 'off'
											]
										])
									</div>
									<div class="heading-btn">
										{{-- <button type="submit" class="btn btn-default">FILTER</button> --}}
									</div>
								{!! Form::close() !!}
								{!!
									Form::open(array(
											'class' => 'heading-form',
											'role' => 'form',
											'url' => route('report.fabric.inspectQcExportToExcel'),
											'method' => 'get',
											//'id' => 'form'
										))
								!!}
									{!! Form::hidden('__start_date',$start, array('id' => '__start_date')) !!}
									{!! Form::hidden('__end_date',$end, array('id' => '__end_date')) !!}
									<div class="from-group text-right">
										<button type="submit" class="btn btn-success" id="exportExcel">Export <i class="icon-file-excel position-left"></i></button>
									</div>
								{!! Form::close() !!}
								</div>
							</div>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								{!! $html->table(['class'=>'table datatable-basic','id'=>'inspect-lab-datatables']) !!}
							</div>
						</div>
					</div>
					<div class="tab-pane @if($active_tab == 'summary') active @else fade @endif" id="basic-tab2">
							<div class="panel-heading">
								<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
									
								<div class="heading-elements">
										{!!
											Form::open(array(
												'class' => 'heading-form',
												'role' => 'form',
												'id'=>'form_daily_filter',
												'url' => route('report.fabric.inspectQc'),
												'method' => 'get',
											))
										!!}
										<div class="form-group">
											@include('form.date', [
												'field' => 'daily_date',
												'label' => 'TGL (00:00)',
												'class' => ' datepicker',
												'default'=> $daily_date,
												'placeholder' => 'dd/mm/yyyy',
												'attributes' => [
													'id' => 'daily_date',
													'autocomplete' => 'off'
												]
											])
										</div>
										{!! Form::hidden('active_tab_daily','true', array('id' => 'active_tab2')) !!}
										<div class="heading-btn">
										</div>
									{!! Form::close() !!}
									{!!
										Form::open(array(
												'class' => 'heading-form',
												'role' => 'form',
												'url' => route('report.dailyPreparationFabricExcel'),
												'method' => 'get',
												'id' => 'form_daily'
											))
									!!}
										{!! Form::hidden('__daily_date',$daily_date, array('id' => '__daily_date')) !!}
										<div class="from-group text-right">
											<button type="submit" class="btn btn-success" id="exportExcel">Export <i class="icon-file-excel position-left"></i></button>
										</div>
									{!! Form::close() !!}
								</div>
							</div>
						<br/>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-basic table-condensed" id="dailyInspect">
									<thead>
									<tr>
										<th>USER</th>
										<th>TOTAL ROLL</th>
										<th>TOTAL YARD</th>
										<th>ACTION</th>
									</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('content-js')
	{!! $html->scripts() !!}
	<script type="text/javascript" src="{{ asset(elixir('js/report_inspect_lab_qc_fabric.js'))}}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
	<script>
	$(document).ready(function() {
		active_tab = '{{$active_tab}}';
		
		console.log(active_tab);
		//$('#active_tab').val('daily');
		if(active_tab == 'summary'){
			summaryDataTables();
		}
	});
	</script>
@endsection
