@extends('layouts.app', ['active' => 'preparation_fabric_report'])

@section('content')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title" style="padding-top:0px">
				<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">PREPARATION FABRIC REPORT</span></h4>

				<ul class="breadcrumb breadcrumb-caret position-left">
					<li><a href="{{ route('dashboard') }}">Report</a></li>
					<li class="active">Preparation Fabric</li>
				</ul>
			</div>
		</div>
	</div>
	
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="tabbable tab-content-bordered">
				<ul class="nav nav-tabs">
					<li class="@if($active_tab == 'reguler') active @endif"><a href="#reguler" data-toggle="tab" aria-expanded="true" onclick="changeTab('reguler')">Reguler</a></li>
					<li class="@if($active_tab == 'additional') active @endif"><a href="#additional" data-toggle="tab" aria-expanded="true" onclick="changeTab('additional')">Additional</a></li>
					<li class="@if($active_tab == 'daily') active @endif"><a href="#daily" data-toggle="tab" aria-expanded="false" onclick="changeTab('daily')">Daily</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane @if($active_tab == 'reguler') active @endif" id="reguler">
						<div class="panel-heading">
							<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
							<div class="heading-elements">
								{!!
									Form::open(array(
										'class' => 'heading-form',
										'role' => 'form',
										'id'=>'form_filter',
										'url' => route('report.preparationFabric'),
										'method' => 'get',
									))
								!!}
									<div class="form-group">
										@include('form.date', [
											'field' => 'start_date',
											'label' => 'FROM (00:00)',
											'class' => ' datepicker',
											'default'=> $start_date,
											'placeholder' => 'dd/mm/yyyy',
											'attributes' => [
												'id' => 'start_reguler_date',
												'autocomplete' => 'off'
											]
										])
									</div>
									<div class="form-group">
										@include('form.date', [
											'field' => 'end_date',
											'label' => 'TO (23:59)',
											'class' => ' datepicker',
											'default'=> $end_date,
											'placeholder' => 'dd/mm/yyyy',
											'attributes' => [
												'id' => 'end_reguler_date',
												'autocomplete' => 'off'
											]
										])
									</div>
								{!! Form::close() !!}

								{!!
									Form::open(array(
											'class' => 'heading-form',
											'role' => 'form',
											'url' => route('report.preparationNonAdditionalFabricExcel'),
											'method' => 'get'		
									))
								!!}
								{!! Form::hidden('_start_date',$start_date , array('id' => '_start_reguler_date')) !!}
								{!! Form::hidden('_end_date',$end_date , array('id' => '_end_reguler_date')) !!}
								{!! Form::hidden('_type','reguler' , array('id' => '_type')) !!}
									<button type="submit" class="btn btn-default">Export All<i class="icon-file-excel position-left"></i></button>
								{!! Form::close() !!}

								
							</div>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-basic table-condensed" id="regulerTable">
									<thead>
										<tr>
											<th>ID</th>
											<th>PIPING</th>
											<th>PLANNING DATE</th>
											<th>SUPPLIER NAME</th>
											<th>DOCUMENT NO</th>
											<th>ITEM CODE</th>
											<th>QTY PRAPARE</th>
											<th>QTY RELAX</th>
											<th>QTY OUTSTANDING</th>
											<th>REMARK</th>
											<th>ACTION</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
					<div class="tab-pane @if($active_tab == 'additional') active @else fade @endif" id="additional">
						<div class="panel-heading">
							<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
							<div class="heading-elements">
								{!!
									Form::open(array(
											'class' => 'heading-form',
											'role' => 'form',
											'url' => route('report.preparationAdditionalFabricExcel'),
											'method' => 'get',
											'target' => '_blank'		
									))
								!!}
								<button type="submit" class="btn btn-default">Export All<i class="icon-file-excel position-left"></i></button>
								{!! Form::close() !!}

								
							</div>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-basic table-condensed" id="additionalTable">
									<thead>
										<tr>
											<th>ID</th>
											<th>PIPING</th>
											<th>SUPPLIER NAME</th>
											<th>DOCUMENT NO</th>
											<th>ITEM CODE</th>
											<th>QTY PRAPARE</th>
											<th>QTY RELAX</th>
											<th>QTY OUTSTANDING</th>
											<th>REMARK</th>
											<th>ACTION</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
					<div class="tab-pane @if($active_tab == 'daily') active @else fade @endif" id="daily">
						<div class="panel-heading">
							<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
							<div class="heading-elements">
								{!!
									Form::open(array(
										'class' => 'heading-form',
										'role' => 'form',
										'id'=>'form_filter',
										'url' => route('report.preparationFabric'),
										'method' => 'get',
									))
								!!}
									<div class="form-group">
										@include('form.date', [
											'field' => 'start_date',
											'label' => 'TODAY',
											'class' => ' datepicker',
											'default'=> $daily_date,
											'placeholder' => 'dd/mm/yyyy',
											'attributes' => [
												'id' => 'start_date',
												'autocomplete' => 'off'
											]
										])
									</div>
								{!! Form::close() !!}

								{!!
									Form::open(array(
											'class' => 'heading-form',
											'role' => 'form',
											'url' => route('report.preparationNonAdditionalFabricExcel'),
											'method' => 'get',	
									))
								!!}
								{!! Form::hidden('_start_date',$daily_date , array('id' => '_start_daily_date')) !!}
								{!! Form::hidden('_end_date',$daily_date , array('id' => '_end_daily_date')) !!}
								{!! Form::hidden('_type','daily' , array('id' => '_type')) !!}
								<button type="submit" class="btn btn-default">Export All <i class="icon-file-excel position-left"></i></button>
								{!! Form::close() !!}

								
							</div>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-basic table-condensed" id="dailyTable">
									<thead>
										<tr>
											<th>ID</th>
											<th>PIPING</th>
											<th>PLANNING DATE</th>
											<th>SUPPLIER NAME</th>
											<th>DOCUMENT NO</th>
											<th>ITEM CODE</th>
											<th>QTY PRAPARE</th>
											<th>QTY RELAX</th>
											<th>QTY OUTSTANDING</th>
											<th>REMARK</th>
											<th>ACTION</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	{!! Form::hidden('active_tab',$active_tab , array('id' => 'active_tab')) !!}
@endsection
@section('content-modal')
	@include('report_fabric._delete_preparation_fabric')
@endsection
@section('content-js')
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/report_preparation_fabric.js'))}}"></script>
@endsection
