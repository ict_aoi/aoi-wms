@extends('layouts.app', ['active' => 'cutting_instruction_fabric'])

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><span class="text-semibold">DETAIL CUTTING INSTRUCTION</span></h4>
						<ul class="breadcrumb breadcrumb-caret position-left">
							<li><a href="{{ route('dashboard') }}">Report</a></li>
							<li><a href="{{ route('report.cuttingInstruction') }}">Cutting Instruction</a></li>
							<li class="active">Detail</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini digunakan untuk melihat detail laporan CI
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-4">
					<p>Tanggal Planning : <b>{{ $header->planning_date }}</b></p>
					<p>Article :  <b>{{ $header->article_no }}</b></p>
					<p>Style :  <b>{{ $header->style }}</b></p>
				</div>
				<div class="col-md-4">
					<p>Po Buyer :  <b>{{ $header->po_buyer }}</b></p>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">Data<a class="heading-elements-toggle"><i class="icon-more"></i></a><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				
				<div class="heading-btn">
						<a
						href="{{route('report.cuttingInstructionExcel',[
							'article_no'=>$header->article_no,
							'planning_date' => $header->planning_date,
							'style'=>$header->style,
							'warehouse_id' => $header->warehouse_id,
							'po_buyer' => $header->po_buyer,
							'item_code' => $header->item_code,
						]) }}"
						class="btn btn-default">
						Excel <i class="icon-file-excel position-left"></i>
						</a>
						@if(auth::user()->hasRole('admin-ict-fabric'))
						<button type="button" id="btn-receive" class="btn btn-primary">RECEIVE <i class="icon-check position-left"></i></button>
						@endif
				</div>
			</div>
		</div>

		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-basic','id' => 'detail-cutting-datatables']) !!}
			</div>
			
		</div>
	</div>
	<a href="{{route('report.cuttingInstructionDetailReceive')}}" id="route_receive" class="hidden"></a>
	
@endsection

@section('content-js')
{!! $html->scripts() !!}
	<script>
	var url_loading = $('#loading_gif').attr('href');
	list_receive_cutting = [];
	url_route_receive = $('#route_receive').attr('href');
	$(document).ready(function(){
		var dtable = $('#detail-cutting-datatables').dataTable().api();
		dtable.page.len(100);
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtable.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtable.search("").draw();
				}
				return;
			});
		dtable.draw();
	});
	
	function checkbox(id){
		var status = $('#is_checked_'+id).attr('data-status');
		if(status == '0'){
			var find=false;
			for( var i = 0; i < list_receive_cutting.length; i++){ 
				if ( list_receive_cutting[i] === id) {
					find=true;
					break;
				}
			}
			if(find==false){
				list_receive_cutting.push(id);
			}
			$('#is_checked_'+id).attr('data-status','1');
		}else{
			for( var i = 0; i < list_receive_cutting.length; i++){ 
				if ( list_receive_cutting[i] === id) {
					list_receive_cutting.splice(i, 1); 
				}
			}
			$('#is_checked_'+id).attr('data-status','0');
		}
	}

	$('#btn-receive').on('click',receive_item);
	function receive_item(){
		if(list_receive_cutting.length<=0){
			$("#alert_info_2").trigger("click", "Silahkan checklist material yang akan di terima terlebih dahulu !");
		}else{
			console.log(list_receive_cutting);
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				type: "put",
				url: url_route_receive,
				data: {id:list_receive_cutting},
				beforeSend: function () {
					swal.close();
					$.blockUI({
						message: "<img src='" + url_loading + "' />",
						css: {
							backgroundColor: 'transaparant',
							border: 'none',
						}
					});
				},
				success: function () {
					$.unblockUI();
				},
				error: function (response) {
					$.unblockUI();
					if (response['status'] == 422)
						$("#alert_info").trigger("click", response['responseJSON']);
				}
			})
			.done(function ($result) {
				$('#detail-cutting-datatables').DataTable().ajax.reload();
				$("#alert_success").trigger("click", 'Material berhasil di terima.');
				report_allocation=[];
			});
		}
	}
	</script>
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
@endsection
