@extends('layouts.app', ['active' => 'inspect_lab_fabric'])

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('report.inspectLab') }}"></a><span class="text-semibold">INSPECT LAB</span></h4>
						<ul class="breadcrumb breadcrumb-caret position-left">
							<li >Report</li>
							<li class="active">Inspect Lab</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini digunakan oleh bagian Lab, untuk melihat laporan dari hasil inspect lab.
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
			<div class="panel-heading">
				<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
				<div class="heading-elements">
						<div class="from-group text-right">
							{!!
								Form::open(array(
									'class' => 'heading-form',
									'role' => 'form',
									'url' => route('report.inspectLab'),
									'method' => 'get',
									'id'=>'filter_date'
								))
							!!}
							<div class="form-group">
								@include('form.date', [
									'field' => 'start_date',
									'label' => 'FROM (00:00)',
									'class' => ' datepicker',
									'default'=> $start,
									'placeholder' => 'dd/mm/yyyy',
									'attributes' => [
										'id' => 'start_date',
										'autocomplete' => 'off'
									]
								])
							</div>
							<div class="form-group">
								@include('form.date', [
									'field' => 'end_date',
									'label' => 'TO (23:59)',
									'class' => ' datepicker',
									'default'=> $end,
									'placeholder' => 'dd/mm/yyyy',
									'attributes' => [
										'id' => 'end_date',
										'autocomplete' => 'off'
									]
								])
							</div>


							<div class="heading-btn">
								{{-- <button type="submit" class="btn btn-default">FILTER</button> --}}
							</div>
						{!! Form::close() !!}
						{!!
							Form::open(array(
									'class' => 'heading-form',
									'role' => 'form',
									'url' => route('report.inspectLabExport'),
									'method' => 'get',
									//'id' => 'form'
								))
						!!}
							{!! Form::hidden('__start_date',$start, array('id' => '__start_date')) !!}
							{!! Form::hidden('__end_date',$end, array('id' => '__end_date')) !!}
							<div class="from-group text-right">
								<button type="submit" class="btn btn-success" id="exportExcel">Export <i class="icon-file-excel position-left"></i></button>
							</div>
						{!! Form::close() !!}
						</div>
				</div>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					{!! $html->table(['class'=>'table datatable-basic','id'=>'inspect-lab-datatables']) !!}
				</div>
			</div>
		</div>
@endsection

@section('content-js')
	{!! $html->scripts() !!}
	<script type="text/javascript" src="{{ asset(elixir('js/inspect_lab_fabric.js'))}}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
@endsection
