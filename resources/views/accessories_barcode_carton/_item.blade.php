<script type="x-tmpl-mustache" id="accessories_barcode_carton_table">
	{% #item %}
		<tr id="panel_{% _id %}">
			<td>
				<p id="no_{% _id %}">{% no %}</p>
			</td>
			<td>{% season %}</td>
			<td>{% last_locator_code %}</td>
			<td>{% po_buyer %}</td>
			<td>{% item_code %} ({% category %})</td>
			<td>{% style %}</td>
			<td>{% article_no %}</td>
			<td>{% qty_receive %} ({% uom_conversion %})</td>
			<td><input type="text" class="form-control input-edit-qty-carton input-number" id="qtyCartonInput_{% _id %}" value="{% total_carton %}" data-id="{% _id %}" placeholder="Input Qty Reject"  data-row="" ></td>
			<td>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-icon-anim btn-circle btn-delete-item"><i class="fa icon-trash"></i></button>
			</td>
		</tr>
	{%/item%}
	<tr>
		<td colspan="13">
			<input type="text" id="barcode_value" name="barcode_value" class="form-control input-new barcode_value" autocomplete="off" placeholder="Please scan your barcode here"></input>
			<span class="text-danger hidden" id="errorInput"></span>
		</td>
		
	</tr>
</script>