<!DOCTYPE html>
<html>
<head>
    <title>BARCODE :: TRF</title>
    <link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/barcode_rack_fabric.css')) }}">
</head>
<body>
    {{-- @foreach ($master_testings as $master_testing) --}}
       <div class="outer-mini text-uppercase">
		<div class="title">WAREHOUSE MANAGEMENT SYSTEM - FABRIC</div>
		<div class="company">PT. APPAREL ONE INDONESIA</div><br>
		{{-- <div class="qrcode">
           <img style="height: 4cm; width: 8cm;	 margin-top: 18px; margin-left: 15px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$master_testings->no_trf", 'C128') }}" alt="barcode"   />	
        </div> --}}
        <br/>
		<div class="footer">			
			<p style="font-size:20px">{{ $master_testings->no_trf }}</p> 
		</div>
	</div>	
    {{-- @endforeach --}}
</body>
</html>
