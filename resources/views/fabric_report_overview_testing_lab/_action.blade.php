
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($print_notrf))
            <li><a href="{!! $print_notrf !!}"><i class="icon-search4"></i> Print No TRF</a></li>
        @endif
            @if (isset($detail))
                <li><a href="{!! $detail !!}"><i class="icon-search4"></i> Print Detail</a></li>
            @endif

            @if (isset($edit))
                <li><a href="{!! $edit !!}"><i class="icon-pencil6"></i> Edit</a></li>
            @endif

            @if (isset($delete))
                <li><a href="#" onclick="hapus('{!! $delete !!}')"><i class="icon-x"></i> Delete</a></li> 
            @endif
        </ul>
    </li>
</ul>
