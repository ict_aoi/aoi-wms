@extends('layouts.app', ['active' => 'area'])

@section('content')
	<div class="page-title" style="padding-top:0px">
		<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">Master Data</span></h4>

		<ul class="breadcrumb breadcrumb-caret position-left">
			<li><a href="{{ route('dashboard') }}">Master Data</a></li>
			<li><a href="{{ route('area.index') }}">Area & Locator</a></li>
			<li class="active">Show</li>
		</ul>

	</div>

	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Area Details</h5>
		</div>
		<div class="panel-body panel-details">
			<div class="form-group row">
				<label for="vendor" class="control-label col-lg-3 text-semibold">
					Area Name
				</label>
				<div class="col-lg-9">
					<p>{{ $area->name }}</p>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">List Of Locator(s)</h5>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-basic']) !!}
			</div
		</div>
	</div>
@endsection

@section('content-modal')
@endsection

@section('content-js')
{!! $html->scripts() !!}
@endsection
