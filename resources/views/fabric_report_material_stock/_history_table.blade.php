<table class="table table-striped table-hover">
	<thead>
	  <tr>
			<th>No</th>
			<th>Movement Date</th>
			<th>Pic Movement</th>
			<th>Status</th>
			<th>From</th>
			<th>To</th>
			<th>Qty Movement</th>
			<th>Note</th>
			
	</tr>
	</thead>
	
	<tbody>
		@foreach ($movement_histories as $key => $movement_history)
			@php
				if($movement_history->user_id) $user_name = strtoupper($movement_history->user->name);
				else $user_name = null;

				if($movement_history->from_location) $from_locator = strtoupper($movement_history->fromLocator->code);
				else $from_locator = null;

				if($movement_history->to_destination) $destination = strtoupper($movement_history->destinationLocator->code);
				else $destination = null;
			@endphp
			<tr>
				<td>{{ $key+1 }}</td>
				<td>{{ $movement_history->created_at->format('d/M/Y H:i:s') }}</td>
				<td>{{ $user_name }}</td>
				<td>{{ strtoupper($movement_history->status) }}</td>
				<td>{{ $from_locator }}</td>
				<td>{{ $destination }}</td>
				<td>{{ $movement_history->qty }}</td>
				<td>{{ $movement_history->note }}</td>
			</tr>
		@endforeach
	</tbody>
</table>
