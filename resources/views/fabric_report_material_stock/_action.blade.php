
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($detail))
                <li><a href="{!! $detail !!}"><i class="icon-search4"></i> Detail</a></li>
            @endif

            @if (isset($historyModal))
                <li><a href="#" onclick="history('{!! $historyModal !!}')" ><i class="icon-history"></i> History</a></li>
            @endif
            
            @if (isset($unlocked))
                <li><a href="#" onclick="unlocked('{!! $unlocked !!}')"><i class="icon-unlocked"></i> Unlock</a></li>
            @endif

            @if (isset($recalculate))
                <li><a href="#" onclick="recalculate('{!! $recalculate !!}')"><i class="icon-calculator3"></i> Recalculate</a></li>
            @endif

            @if (isset($print))
                <li><a href="{!! $print !!}" target="_blank"><i class="icon-printer2"></i> Print</a></li>
            @endif
        </ul>
    </li>
</ul>
