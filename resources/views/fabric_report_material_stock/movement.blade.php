@extends('layouts.app', ['active' => 'report_stock_fabric'])

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('report.inspectLab') }}"></a><span class="text-semibold">MOVEMENT HISTORY</span></h4>
						<ul class="breadcrumb breadcrumb-caret position-left">
							<li>Report</li>
							<li><a href="{{ route('report.summaryMaterialStockFabric') }}">Material Stock</a></li>
							<li><a href="{{ route('report.materialStockFabric') }}?id={{$summary_stock_fabric_id}}">Detail</a></li>
							<li class="active">Movement Stock</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini digunakan oleh untuk melihat laporan Movement History.
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
			<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>

		</div>
			<div class="panel-body">
				<div class="table-responsive">
					{!! $html->table(['class'=>'table datatable-basic']) !!}
				</div>
			</div>
		</div>
@endsection

@section('content-js')
	{!! $html->scripts() !!}
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
@endsection
