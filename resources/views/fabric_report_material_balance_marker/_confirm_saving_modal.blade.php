<div class="modal fade" id="confirmSavingModal" role="dialog" aria-labelledby="confirmSavingModal">
        <div class="modal-dialog modal-lg" role="document">
		{{ 
			Form::open([
				'method' => 'POST',
				'id'     => 'confirmStoreSaving',
				'url'    => '#',
				'class'  => 'form-horizontal',
			]) 
		}}
		    <div class="modal-content">
                <div class="modal-header bg-blue">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">Confirm Saving</h5>
				</div>
				<div class="modal-body">
					<div class="row">  
						<div class="col-md-6">
							<div class="col-md-12">
								@include('form.select', [
									'label_col'	=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'	=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 	=> '_warehouse_id',
									'label' 	=> 'Warehouse',
									'options' 	=> [
										'1000011'	=>'Fabric aoi 2',
										'1000001'	=>'Fabric aoi 1',
									],
									'class' => 'select-search',
									'attributes' => [
										'id' => '_warehouse_id'
									]
								])

								@include('form.text', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> '_planning_date',
									'label' 		=> 'Planning Date',
									'attributes' 	=> [
										'id' 		=> '_planning_date',
										'readonly' 	=> true,
									]
								])
								@include('form.text', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> '_style',
									'label' 		=> 'Style',
									'attributes' 	=> [
										'id' 		=> '_style',
										'readonly' 	=> true,
									]
								])

								@include('form.text', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> '_article_no',
									'label' 		=> 'Article No',
									'attributes' 	=> [
										'id' 		=> '_article_no',
										'readonly' 	=> true,
									]
								])
                                
                                @include('form.select', [
									'label_col'	=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'	=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 	=> '_nomor_roll',
									'label' 	=> 'Nomor Roll',
									'options' 	=> [
									],
									'class' => 'select-search',
									'attributes' => [
										'id' => 'select_nomor_roll'
									]
								])
							</div>
						</div>
						<div class="col-md-6">
							<div class="col-md-12">
							@include('form.select', [
									'label_col'	=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'	=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 	=> '_document_no',
									'label' 	=> 'Document No',
									'options' 	=> [
									],
									'class' => 'select-search',
									'attributes' => [
										'id' => 'select_document_no'
									]
								])

								@include('form.text', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> '_po_buyer',
									'label' 		=> 'Po Buyer',
									'attributes' 	=> [
										'id' 		=> '_po_buyer',
										'readonly' 	=> true,
									]
								])

								@include('form.text', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> '_item_code_source',
									'label' 		=> 'Item Code Source',
									'placeholder' 	=> 'Please type item code book here',
									'attributes' 	=> [
										'id' 		=> '_item_code',
									]
								])

								@include('form.text', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> '_item_code',
									'label' 		=> 'Item Code Book',
									'placeholder' 	=> 'Please type item code book here',
									'attributes' 	=> [
										'id' 		=> '_item_code_book',
										'readonly' 	=> true,
									]
								])

								@include('form.text', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> '_qty_saving',
									'label' 		=> 'Qty Saving',
									'placeholder' 	=> 'Please type qty allocation here',
									'attributes' 	=> [
										'id' 			=> '_qty_allocation',
										'autocomplete' 	=> 'off',
                                        'readonly' 	=> true,
									]
								])
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							@include('form.textarea', [
								'label_col'		=> 'col-md-12 col-lg-12 col-sm-12',
								'form_col'		=> 'col-md-12 col-lg-12 col-sm-12',
								'field' => '_remark',
								'label' => 'Remark',
								'attributes' => [
									'id' 		=> '_remark',
									'rows'	 	=> '5',
									'cols' 		=> '15',
									'resize' 	=> 'none'
								]
							])

							{!! Form::hidden('_lc_date','', array('id' => '_lc_date')) !!}
							{!! Form::hidden('_promise_date','', array('id' => '_promise_date')) !!}
							{!! Form::hidden('_item_id_book','', array('id' => '_item_id_book')) !!}
							{!! Form::hidden('_item_id_source','', array('id' => '_item_id_source')) !!}
							{!! Form::hidden('_supplier_name','', array('id' => '_supplier_name')) !!}
							{!! Form::hidden('_c_bpartner_id','', array('id' => '_c_bpartner_id')) !!}
							{!! Form::hidden('_type_stock_erp','', array('id' => '_type_stock_erp')) !!}
							{!! Form::hidden('_type_stock','', array('id' => '_type_stock')) !!}
							{!! Form::hidden('_marker_release_detail_id','', array('id' => '_marker_release_detail_id')) !!}
						</div>
					</div>
				</div>
                <div class="modal-footer text-center">
					<button type="submit" class="btn btn-blue-success"> Save <i class="icon-floppy-disk position-left"></i></button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close <span class="glyphicon glyphicon-remove"></span></button>
                </div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>