@extends('layouts.app', ['active' => 'fabric_report_material_balance_marker'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Material Balance Marker</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data</li>
				<li><a href="{{ route('fabricReportMaterialBalanceMarker.index') }}">Report Material Balance Marker</a></li>
				<li class="active">Create Allocation</li>
			</ul>
		</div>
	</div>
@endsection


@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a class="btn btn-primary btn-icon" href="{{ route('fabricReportMaterialBalanceMarker.downloadCreateAllocation')}}" data-popup="tooltip" title="download form eta delay" data-placement="bottom" data-original-title="download form eta delay"><i class="icon-download"></i></a>
				<button type="button" class="btn btn-success btn-icon" id="upload_button" data-popup="tooltip" title="upload form eta delay" data-placement="bottom" data-original-title="upload form eta delay"><i class="icon-upload"></i></button>

				{!!
					Form::open([
						'role' => 'form',
						'url' => route('fabricReportMaterialBalanceMarker.uploadCreateAllocation'),
						'method' => 'POST',
						'id' => 'upload_file_allocation',
						'enctype' => 'multipart/form-data'
					])
				!!}
					<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
					<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
				{!! Form::close() !!}

			</div>
		</div>

		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover table-responsive">
					<thead>
						<tr>
                            <th>No</th>
                            <TH>Planning Date</TH>
							<th>Po Supplier</th>
							<th>Item Code</th>
                            <th>Po Buyer</th>
                            <th>Style</th>
                            <th>Article No</th>
                            <th>Qty</th>
                            <th>Is Balance Marker</th>
							<th>Warehouse Inventory</th>
							<th>Remark</th>
							<th>Upload Result</th>
						</tr>
					</thead>
					<tbody id="tbody-auto-allocation">
					</tbody>
				</table>
			</div>
		</div>
    </div>
    
@endsection

@section('page-modal')
    @include('fabric_report_material_balance_marker._item')
@endsection

@section('page-js')
	<script type="text/javascript" src="{{ asset(elixir('js/fabric_report_material_balance_marker_create_allocation.js'))}}"></script>
@endsection
