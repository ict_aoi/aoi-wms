
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($confirm))
                <li><a href="#" onclick="confirm('{!! $confirm !!}')"><i class="icon-pencil6"></i> Confirm</a></li>
            @endif
            @if (isset($reject))
                <li><a href="#" onclick="reject('{!! $reject !!}')"><i class="icon-trash"></i> Reject</a></li>
            @endif
            @if (isset($confirm_save))
                <li><a href="#" onclick="confirmSave('{!! $confirm_save !!}')"><i class="icon-pencil6"></i> Confirm Saving</a></li>
            @endif
        </ul>
    </li>
</ul>
