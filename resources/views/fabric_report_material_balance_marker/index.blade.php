@extends('layouts.app', ['active' => 'fabric_report_material_balance_marker'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Material Balance Marker</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li class="active">Report Material Balance Marker</li>
			</ul>
			<ul class="breadcrumb-elements">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
						<i class="icon-three-bars position-left"></i>
						Actions
						<span class="caret"></span>
					</a>
					
					<ul class="dropdown-menu dropdown-menu-right">
						@role(['admin-ict-acc','admin-ict-fabric','mm-staff-acc','mm-staff'])
							<li><a href="{{ route('fabricReportMaterialBalanceMarker.createAllocation') }}"><i class="icon-pencil6 pull-right"></i> Create</a></li>
						@endrole
					</ul>
				</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
	<div class="panel-heading">
		<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
			</ul>
		</div>
	</div>
	<div class="panel-body">
	{!!
		Form::open(array(
			'class' => 'form-horizontal',
			'role' => 'form',
			'url' => route('fabricReportMaterialBalanceMarker.export'),
			'method' => 'get',
			'target' => '_blank'		
		))
	!!}
	@include('form.date', [
		'field' 		=> 'start_date',
		'label' 		=> 'Planning Date From (00:00)',
		'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
		'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
		'default'		=> \Carbon\Carbon::now()->subDays(7)->format('d/m/Y'),
		'placeholder' 	=> 'dd/mm/yyyy',
		'class' 		=> 'daterange-single',
		'attributes'	=> [
			'id' 			=> 'start_date',
			'autocomplete' 	=> 'off',
			'readonly' 		=> 'readonly'
		]
	])
	
	@include('form.date', [
		'field' 		=> 'end_date',
		'label' 		=> 'Planning Date To (23:59)',
		'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
		'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
		'default'		=> \Carbon\Carbon::now()->addDays(7)->format('d/m/Y'),
		'placeholder' 	=> 'dd/mm/yyyy',
		'class' 		=> 'daterange-single',
		'attributes' 	=> [
			'id' 			=> 'end_date',
			'readonly' 		=> 'readonly',
			'autocomplete' 	=> 'off'
		]
	])

	@include('form.select', [
		'label_col' => 'col-md-2 col-lg-2 col-sm-12',
		'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
		'field'     => 'factory_id',
		'label'     => 'Warehouse',
		'options'   => [
			''  => 'All Warehouse',
			'1' => 'AOI 1',
			'2' => 'AOI 2',
		],
		'class'      => 'select-search',
		'attributes' => [
			'id' => 'factory_id'
		]
	])
	{{-- @include('form.date', [
				'field' 		=> 'cutting_date',
				'label' 		=> 'Cutting Date',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'default'		=> \Carbon\Carbon::now()->format('d/m/Y'),
				'placeholder' 	=> 'dd/mm/yyyy',
				'class' 		=> 'daterange-single',
				'attributes' 	=> [
					'id' 			=> 'cutting_date',
					'readonly' 		=> 'readonly',
					'autocomplete' 	=> 'off'
				]
			]) --}}


		<button type="submit" class="btn btn-default col-xs-12">Export All <i class="icon-file-excel position-left"></i></button>
	{!! Form::close() !!}
	</div>
</div>

<div class="panel panel-default border-grey">
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table datatable-basic table-striped table-hover table-responsive" id="fabric_report_material_balance_marker">
				<thead>
					<tr>
						<th>Warehouse</th>
						<th>Planning Date</th>
						<th>Warehouse</th>
                        <th>Style</th>
						<th>Article</th>
						<th>Po Buyer</th>
						<th>Item Code Source</th>
                        <th>Item Code Book</th>
						<th>Actual Marker</th>
                        <th>CSI</th>
						<th>Balance</th>
						<th>Status</th>
                        <th>% Prepared</th>
                        <th>Remark</th>
                        <th>Action</th>
					</tr>
				</thead>
				<tbody>
            	</tbody>
			</table>
		</div>
	</div>
</div>

{!! Form::hidden('user_id',auth::user()->id , array('id' => 'user_id')) !!}
{!! Form::hidden('url_store_balance_marker',route('fabricReportMaterialBalanceMarker.confirmStore') , array('id' => 'url_store_balance_marker')) !!}
{!! Form::hidden('url_store_saving',route('fabricReportMaterialBalanceMarker.confirmSavingStore') , array('id' => 'url_store_saving')) !!}
@endsection

@section('page-modal')
	@include('fabric_report_material_balance_marker._confirm_balance_marker_modal')
	@include('fabric_report_material_balance_marker._confirm_saving_modal')
	@include('form.modal_picklist', [
		'name' 			=> 'document_no',
		'title' 		=> 'List Po Supplier',
		'placeholder' 	=> 'Search based on po supplier',
	])

	@include('form.modal_picklist', [
		'name' 			=> 'item_code_source',
		'title' 		=> 'List Item',
		'placeholder' 	=> 'Search based on item code',
	])
@endsection

@section('page-js')
	<script src="{{ mix('js/fabric_report_material_balance_marker.js') }}"></script>
@endsection
