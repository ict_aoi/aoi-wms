<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal">
        <div class="modal-dialog modal-lg" role="document">
		{{ 
			Form::open([
				'method' => 'POST',
				'id'     => 'confirmStore',
				'url'    => '#',
				'class'  => 'form-horizontal',
			]) 
		}}
		    <div class="modal-content">
                <div class="modal-header bg-blue">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">Confirm Balance Marker</h5>
				</div>
				<div class="modal-body">
					<div class="row">  
						<div class="col-md-6">
							<div class="col-md-12">
								@include('form.select', [
									'label_col'	=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'	=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 	=> 'warehouse_id',
									'label' 	=> 'Warehouse',
									'options' 	=> [
										'1000011'	=>'Fabric aoi 2',
										'1000001'	=>'Fabric aoi 1',
									],
									'class' => 'select-search',
									'attributes' => [
										'id' => 'warehouse_id'
									]
								])

								@include('form.text', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> 'planning_date',
									'label' 		=> 'Planning Date',
									'attributes' 	=> [
										'id' 		=> 'planning_date',
										'readonly' 	=> true,
									]
								])
								@include('form.text', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> 'style',
									'label' 		=> 'Style',
									'attributes' 	=> [
										'id' 		=> 'style',
										'readonly' 	=> true,
									]
								])

								@include('form.text', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> 'article_no',
									'label' 		=> 'Article No',
									'attributes' 	=> [
										'id' 		=> 'article_no',
										'readonly' 	=> true,
									]
								])

								@include('form.text', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> 'booking_number',
									'label' 		=> 'Booking Number',
									'placeholder' 	=> 'Please insert booking number here',
									'attributes' 	=> [
										'id' 		=> 'booking_number'
									]
								])
							</div>
						</div>
						<div class="col-md-6">
							<div class="col-md-12">
							@include('form.picklist', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> 'document_no',
									'label' 		=> 'Po Supplier',
									'name' 			=> 'document_no',
									'placeholder' 	=> 'Please select po supplier here',
									'attributes' 	=> [
										'id' 		=> 'document_no',
										'readonly' 	=> true,
									]
								])

								@include('form.text', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> 'po_buyer',
									'label' 		=> 'Po Buyer',
									'attributes' 	=> [
										'id' 		=> 'po_buyer',
										'readonly' 	=> true,
									]
								])

								@include('form.text', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> 'item_code_source',
									'label' 		=> 'Item Code Source',
									'placeholder' 	=> 'Please type item code book here',
									'attributes' 	=> [
										'id' 		=> 'item_code',
									]
								])

								@include('form.text', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> 'item_code',
									'label' 		=> 'Item Code Book',
									'placeholder' 	=> 'Please type item code book here',
									'attributes' 	=> [
										'id' 		=> 'item_code_book',
										'readonly' 	=> true,
									]
								])

								@include('form.text', [
									'label_col'		=> 'col-md-3 col-lg-3 col-sm-12',
									'form_col'		=> 'col-md-9 col-lg-9 col-sm-12',
									'field' 		=> 'qty_allocation',
									'label' 		=> 'Qty Allocation',
									'placeholder' 	=> 'Please type qty allocation here',
									'attributes' 	=> [
										'id' 			=> 'qty_allocation',
										'autocomplete' 	=> 'off'
									]
								])
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							@include('form.textarea', [
								'label_col'		=> 'col-md-12 col-lg-12 col-sm-12',
								'form_col'		=> 'col-md-12 col-lg-12 col-sm-12',
								'field' => 'remark',
								'label' => 'Remark',
								'attributes' => [
									'id' 		=> 'remark',
									'rows'	 	=> '5',
									'cols' 		=> '15',
									'resize' 	=> 'none'
								]
							])

							{!! Form::hidden('lc_date','', array('id' => 'lc_date')) !!}
							{!! Form::hidden('promise_date','', array('id' => 'promise_date')) !!}
							{!! Form::hidden('item_id_book','', array('id' => 'item_id_book')) !!}
							{!! Form::hidden('item_id_source','', array('id' => 'item_id_source')) !!}
							{!! Form::hidden('supplier_name','', array('id' => 'supplier_name')) !!}
							{!! Form::hidden('c_bpartner_id','', array('id' => 'c_bpartner_id')) !!}
							{!! Form::hidden('type_stock_erp','', array('id' => 'type_stock_erp')) !!}
							{!! Form::hidden('type_stock','', array('id' => 'type_stock')) !!}
							{!! Form::hidden('marker_release_detail_id','', array('id' => 'marker_release_detail_id')) !!}
						</div>
					</div>
				</div>
                <div class="modal-footer text-center">
					<button type="submit" class="btn btn-blue-success"> Save <i class="icon-floppy-disk position-left"></i></button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close <span class="glyphicon glyphicon-remove"></span></button>
                </div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>