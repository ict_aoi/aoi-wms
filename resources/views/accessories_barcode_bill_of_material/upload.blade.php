@extends('layouts.app', ['active' => 'accessories_barcode_bill_of_material'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Bill Of Material</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li>Barcode</li>
            <li><a href="{{ route('accessoriesBarcodeBillOfMaterial.index') }}">Bill Of Material</a></li>
            <li class="active">Upload</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

	@role(['admin-ict-acc'])
		<div class="panel panel-default border-grey">
			<div class="panel-heading">
				<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
					</ul>
				</div>
			</div>
			<div class="panel-body">
				@include('form.select', [
					'field' 			=> 'warehouse',
					'label' 			=> 'Warehouse',
					'default'			=> auth::user()->warehouse,
					'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
					'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
					'options' 			=> [
						'1000002' 		=> 'Warehouse Accessories AOI 1',
						'1000013' 		=> 'Warehouse Accessories AOI 2',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_warehouse'
					]
				])
			</div>
		</div>
	@else 
		{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
	@endrole

	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp </h6>
			<div class="heading-elements">
				<div class="heading-btn">
					<a class="btn btn-primary btn-icon" href="{{ route('accessoriesBarcodeBillOfMaterial.exportFileUpload') }}" data-popup="tooltip" title="download form import" data-placement="bottom" data-original-title="download form import"><i class="icon-download"></i></a>
					<button type="button" class="btn btn-info btn-icon" id="upload_button" data-popup="tooltip" title="upload form import" data-placement="bottom" data-original-title="upload form import"><i class="icon-upload"></i></button>
				</div>
			</div>
		</div>

		<div class="panel-body">
			
			<div class="table-responsive">
				<table class="table table-striped table-hover table-responsive">
					<thead>
						<tr>
							<th>NO</th>
							<th>Po Buyer</th>
							<th>Item</th>
							<th>Style</th>
							<th>Uom</th>
							<th>Qty</th>
							<th>Upload Result</th>
						</tr>
					</thead>
					<tbody id="tbody-upload-bom">
					</tbody>
				</table>
			</div>

			{!!
				Form::open([
					'role' 			=> 'form',
					'url' 			=> route('accessoriesBarcodeBillOfMaterial.printoutAll'),
					'method' 		=> 'post',
					'enctype' 		=> 'multipart/form-data',
					'class' 		=> 'form-horizontal',
					'id' 			=> 'form_print_out',
					'target' 		=> '_blank'
				])
			!!}
				{!! Form::hidden('bom','[]',array('id' => 'bom')) !!}
				{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_id')) !!}
				<div class="form-group text-right" style="margin-top: 10px;">
					<button type="button" id="print_out_btn" class="btn btn-blue-success col-xs-12 btn-lg">Print <i class="icon-printer position-left"></i></button>
				</div>
			{!! Form::close() !!}	

			{!!
			Form::open([
				'role' 				=> 'form',
				'url' 				=> route('accessoriesBarcodeBillOfMaterial.import'),
				'method' 			=> 'post',
				'id' 				=> 'upload_file_allocation',
				'enctype' 			=> 'multipart/form-data'
			])
			!!}
				<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
				<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
			{!! Form::close() !!}
		</div>
	</div>
@endsection

@section('page-js')
	@include('accessories_barcode_bill_of_material._item')
	<script src="{{ mix('js/accessories_barcode_bill_of_material_import.js') }}"></script>
@endsection