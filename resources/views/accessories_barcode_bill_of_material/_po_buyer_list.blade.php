<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
			<th>Po Buyer</th>
			<th>Item</th>
			<th>Style</th>
			<th>Article</th>
			<th>Uom</th>
			<th>Qty</th>
			<th>System Log</th>
			<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($data as $key => $list)
			@php
				$check_prepared = $list->checkIlaPrepared($list->po_buyer,$list->item_id,$list->style,$list->article_no);
				$created_at 	= ($check_prepared) ? $check_prepared->created_at : null ;
				$username 		= ($check_prepared) ? $check_prepared->username : null;
				$warehouse 		= ($check_prepared) ? $check_prepared->warehouse : null;
			@endphp

			<tr>
				<td>
					{{ $list->po_buyer }}
				</td>
				<td>
					{{ $list->item_code }}
				</td>
				<td>
					{{ $list->style }}
				</td>
				<td>
					{{ $list->article_no }}
				</td>
				<td>
					{{ $list->uom }}
				</td>
				<td>
					{{ $list->qty_required }}
				</td>
				@if ($check_prepared)
				<td>
					SUDAH DICETAK, {{ $created_at }} oleh {{ $username }} {{ $warehouse }}
				</td>
				<td></td>
				@else
				<td>
					{{ $check_prepared }}
					SIAP CETAK
				</td>
				<td class="text-center">
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose"
						type="button" data-buyer="{{ $list->po_buyer }}" 
						data-item="{{ $list->item_code }}" 
						data-style="{{ $list->style }}"
						data-article ="{{ $list->article_no }}" 
						data-job = "{{ $list->job_order }}" 
						data-category ="{{ $list->category }}"
						data-desc="{{ $list->item_desc }}"  
						data-uom="{{ $list->uom }}" 
						data-need="{{ $list->qty_required }}">Select</button>
				</td>
				@endif
			</tr>
		@endforeach
	</tbody>
</table>

{!! $data->appends(Request::except('page'))->render() !!}
