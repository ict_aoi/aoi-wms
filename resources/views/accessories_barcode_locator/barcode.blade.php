<!DOCTYPE html>
<html>
<head>
    <title>Barcode :: @if($rack == 'null') Area & Locator @else Area @endif</title>
    <link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/barcode_area.css')) }}">
</head>
<body>
    @foreach ($locators as $key => $locator)
       <div class="outer">
            <div class="isi">
                <div class="block-kiri">WMS - ACC</div>
                <div class="block-kiri" style="height: 50px; line-height: 50px;">
                    <span style="font-size: 35px;">{{ $locator->rack }}.{{ $locator->y_row }}.{{ $locator->z_column }}</span>
                </div>
                <div class="block-kiri" style="font-size: 12px">{{ $locator->area->name }}</div>
            </div>
            <div class="isi1">
                <img style="height: 2cm; width: 5.3cm;	 margin-top: 18px; margin-left: 15px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$locator->barcode", 'C128') }}" alt="barcode"   />			
                <center><span style="font-size: 12px;word-wrap: break-word;">{{ $locator->barcode }}</span></center>
            </div>
        </div>
    @endforeach
</body>
</html>