@extends('layouts.app', ['active' => 'accessories_barcode_locator'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Locator</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li>Barcode</li>
            <li class="active">Locator</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	@foreach ($areas as $area)
		<div class="panel panel-default border-grey">
			<div class="panel-heading">
				<h6 class="panel-title"> Area {{ ucwords($area->name) }} <a class="heading-elements-toggle"><i class="icon-more"></i></a><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
				<div class="heading-elements">
					<div class="heading-btn">
						<a href="{{ route('accessoriesBarcodeLocator.printOut', ['area'=> $area->id , 'rack' => 'null']) }}" class="btnPrint btn btn-blue-success" target="_blank"><i class="icon-printer"></i></a>
					</div>
				</div>
			</div>

			<div class="panel-body">
				@if ($area->Locators()->select('rack')->where('is_active',true)->groupBy('rack')->count()!=0)
				<div class="table-responsive">
					@foreach ($area->Locators()->select('rack')->where('is_active',true)->groupBy('rack')->orderBy('rack')->get() as $item)
						<table class="table table-striped table-hover table-responsive">
							<tbody>
								<tr>
									<td style="width: 100%;">
										{{ trim($item->rack) }}
									</td>
									<td>
										<a href="{{ route('accessoriesBarcodeLocator.printOut', ['area'=> $area->id , 'rack' => $item->rack]) }}" class="btnPrint" target="_blank"><button class="btn-yellow-cancel" type="button"><i class="icon-printer"></i></button></a>
									</td>
								</tr>
							</tbody>
						</table>
					@endforeach
				</div>
				@else
					<p>Locator not found</p>
				@endif
			</div>
		</div>
	@endforeach
	
@endsection
