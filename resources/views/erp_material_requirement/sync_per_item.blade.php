@extends('layouts.app', ['active' => 'erp_material_requiremet'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Requirement</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Material Requirement</li>
				<li class="active">Sync Per Item</li>
			</ul>
		</div>
	</div>
@endsection


@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a class="btn btn-primary btn-icon" href="{{ route('erpMaterialRequirement.exportSyncPerItem')}}" data-popup="tooltip" title="download form eta delay" data-placement="bottom" data-original-title="download form eta delay"><i class="icon-download"></i></a>
				<button type="button" class="btn btn-success btn-icon" id="upload_button" data-popup="tooltip" title="upload form eta delay" data-placement="bottom" data-original-title="upload form eta delay"><i class="icon-upload"></i></button>

				{!!
					Form::open([
						'role' => 'form',
						'url' => route('erpMaterialRequirement.importsyncPerItem'),
						'method' => 'POST',
						'id' => 'upload_file_sync',
						'enctype' => 'multipart/form-data'
					])
				!!}
					<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
					<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
				{!! Form::close() !!}

			</div>
		</div>

		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover table-responsive">
					<thead>
						<tr>
							<th>Lc Date</th>
							<th>Statistical Date</th>
							<th>Season</th>
							<th>Po Buyer</th>
							<th>Item Code</th>
							<th>Style</th>
							<th>Article No</th>
							<th>UOM</th>
							<th>Warehouse Packing</th>
							<th>Qty Required</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody id="tbody-sync-per-item">
					</tbody>
				</table>
			</div>
		</div>
	</div>

{!! Form::hidden('flag',null , array('id' => 'flag')) !!}
{!! Form::hidden('sync_per_items','[]' , array('id' => 'sync_per_items')) !!}
@endsection

@section('page-js')
	@include('erp_material_requirement._item_sync_per_item')
	<script type="text/javascript" src="{{ asset(elixir('js/sync_per_item.js'))}}"></script>
@endsection
