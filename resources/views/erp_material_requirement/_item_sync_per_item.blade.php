<script type="x-tmpl-mustache" id="sync-per-item-table">
	{% #item %}
		<tr {% #error_upload %} style="background-color:#fab1b1" {% /error_upload %}>
			<td>{% lc_date %}</td>
			<td>{% statistical_date %}</td>
			<td>{% season %}</td>
			<td>{% po_buyer %}</td>
			<td>{% item_code %} ({% uom %})</td>
			<td>{% style %}</td>
			<td>{% article_no %}</td>
			<td>{% uom %}</td>
			<td>{% warehouse_packing %}</td>
			<td>{% qty_required %}</td>
			<td>{% remark %}</td>
		</tr>
	{%/item%}
</script>
