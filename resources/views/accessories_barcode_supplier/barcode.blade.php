<!DOCTYPE html>
<html>
<head>
    <title>BARCODE :: REPRINT BARCODE SUPPLIER</title>
    <link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/print_barcode_wpo.css')) }}">

</head>
    <body>
		@if (count($data_web_po) == 0)
				{{ $document_no }} belum di tarik dierp, silahkan hubungi ict.
		@else

			@php $flag = 0 ; @endphp
			@foreach ($data_web_po as $key => $web_po)
				@for ($i = 0; $i < $web_po->qty_carton ; $i++)
					<!-- @if (($flag+1) % 3 === 0)
						<p style="page-break-after: always;">&nbsp;</p>
					@endif -->

					@php
						$check_flag_wms = $web_po->checkFlagWms($web_po->po_detail_id);
						$check_recycle = $web_po->getRecycle($web_po->item);
						$check_sp = $web_po->getSewingPlace($web_po->m_product_id, $web_po->pobuyer);
					@endphp

					<div class="outer">
						<div class="title">BARCODE SYSTEM - AOI</div>
						
						<div class="isi">
							<div class="isi1" style="padding-top:0;">
								<div class="isi3" style="height:31px;float:left;">
										NO CARTON : <span style="font-size: 24px">{{ $i+1 }}</span> / {{ $web_po->qty_carton }}
								</div>
								<div class="isi2" style="height:31px;width:40.37%;display:block;">
								<span style="font-size: 12px">SEASON 
									@php
									$has_dash = strpos($web_po->pobuyer, ",");
									if($has_dash==false||$web_po->pobuyer!=''||$web_po->pobuyer!=null)
									{
										echo $web_po->getSeason($web_po->pobuyer);
									}
									@endphp
								</span> 
								</div>
							</div>
							<div class="isi1" style="padding-top:4px;">
							<span style="@if(strlen($web_po->item)>40) font-size: 12px @endif">{{ $web_po->item.$check_recycle }}</span>
							</div>
							<div class="isi1">
								ORDER: {{ number_format($web_po->qtyordered, 2, '.', ',') }} || DLVRD: {{ number_format($web_po->qty_upload, 2, '.', ',') }} {{ strtoupper($web_po->uomsymbol) }}       			
							</div>

							<div class="isi2">
								<div class="block-kiri" style="justify-content: center; height: 45px;">
									<span style="font-size: 10px;word-wrap: break-word;">{{ $web_po->desc_product }}</span>
								</div>
								<div class="block-kiri" style="height:35px;line-height: 35px">
									<span style="font-size: 12px;word-wrap: break-word;">{{ $web_po->documentno }}</span>
								</div>
								<div class="block-kiri" style="height: 38px;">
									<span style="font-size: 10px;word-wrap: break-word;">{{ $web_po->supplier }} PL:{{ $web_po->no_packinglist }}</span>
								</div>
								<div class="block-kiri" style="height: 50px; font-size:0.8em;">
									<span style="word-wrap: break-word;">
										@php
											$replace_po_buyer = trim(str_replace('-S', '',$web_po->pobuyer));
											$list_po_buyer = explode(', ', $replace_po_buyer);
											$po_buyer      = array_unique($list_po_buyer);
											$po_buyer      = implode(',', $po_buyer);
											$has_dash = strpos($po_buyer, ",");
										@endphp
										@if($web_po->pobuyer == '' || $web_po->pobuyer == null)
											BARCODE ALLOCATION
										@else
											@if($web_po->uomsymbol == 'YDS' || $web_po->uomsymbol == 'M' || $web_po->uomsymbol == 'CNS')
												HAS MANY JOB ORDER
											@else
												@if ($has_dash == false)
													@php
														$data_bom = $web_po->getBom($po_buyer,$web_po->item);
														foreach ($data_bom as $key => $value) {
															$article_no = $data_bom->article_no;
															$style 		= $data_bom->style;
															$crd 		= $data_bom->promise_date;
														}
													@endphp
													Article No : {{ $article_no }}<br/>
													Style :  {{ $style }}<br/>
													CRD :  {{ $crd }}
												@else
													HAS MANY JOB ORDER
												@endif
											@endif
											
										@endif
										
									</span>
							
								</div>
							</div>
							<div class="isi3">
								@if ($check_flag_wms == true)
									@if (Auth::User()->hasRole('admin-ict-acc') ||  Auth::User()->hasRole('supervisor'))
										<div class="block-kiri" style="height: 120px;">
											@if ($web_po->qty_carton == 1)
												<img style="height: 2.5cm; width: 4.3cm;	 margin-top: 10px; margin-left: 2px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$web_po->po_detail_id", 'C128') }}" alt="barcode"   />
												<br/>
												<span style="font: 10px monospace;" text-anchor="middle" x="65" y="62">{{ $web_po->po_detail_id }}</span>			
											@else
												@php
													$_barcode = $web_po->po_detail_id.'-'.($i+1);
												@endphp

												<img style="height: 2.5cm; width: 4.3cm;	 margin-top: 10px; margin-left: 2px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$_barcode", 'C128') }}" alt="barcode"   />	
												<br/>
												<span style="font: 10px monospace;" text-anchor="middle" x="65" y="62">{{ $_barcode }}</span>			
											@endif
										</div>

										<div class="block-kiri" style="margin-top: 6px;" >
											PO BUYER: <span style="font-size: 1em">
											@if($web_po->pobuyer == '' || $web_po->pobuyer == null)
												BARCODE ALLOCATION
											@else
												@php
													$replace_po_buyer = trim(str_replace('-S', '',$web_po->pobuyer));
													$list_po_buyer = explode(', ', $replace_po_buyer);
													$po_buyer      = array_unique($list_po_buyer);
													$po_buyer      = implode(',', $po_buyer);
													$has_dash = strpos($po_buyer, ",");
													$uom_need_prepare = false;

													if ($web_po->uomsymbol == 'YDS' || $web_po->uomsymbol == 'M' || $web_po->uomsymbol == 'CNS')
													{
														$uom_need_prepare = true;
													}
													else
													{
														$uom_need_prepare = false;
													}
												@endphp
												
												@php
												if($web_po->m_warehouse_id == '1000002')
												{
													$warehouse_name ='-AOI1';
												}
												else
												{
													$warehouse_name ='-AOI2';
												}
													
												@endphp

												@if ($has_dash == false && $uom_need_prepare == false)
													{{ $po_buyer.$warehouse_name }}
												@else
													HAS MANY PO BUYER
												@endif
											@endif
											
											</span>
											<span>
												<br>Sewing Place : {{ $check_sp}}
											</span>
										</div>
									@else
										</br>
										ITEM SUDAH PERNAH DITERIMA, SILAHKAN HUBUNGI ICT / SUPERVISOR UNTUK REPRINT BARCODE.
									@endif
								@else
									<div class="block-kiri" style="height: 120px;">
										@if ($web_po->qty_carton == 1)
											<img style="height: 2.5cm; width: 4.3cm;	 margin-top: 10px; margin-left: 2px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$web_po->po_detail_id", 'C128') }}" alt="barcode"   />			
											<br/> 
											<span style="font: 10px monospace;" text-anchor="middle" x="65" y="62">{{ $web_po->po_detail_id }}</span>
								
										@else
											@php
												$_barcode = $web_po->po_detail_id.'-'.($i+1);
											@endphp

											<img style="height: 2.5cm; width: 4.3cm;	 margin-top: 10px; margin-left: 2px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$_barcode", 'C128') }}" alt="barcode"   />			
											<br/> 
											<span style="font: 10px monospace;" text-anchor="middle" x="65" y="62">{{ $_barcode }}</span>
										@endif
									</div>
									
									<div class="block-kiri" style="margin-top: 6px;" >
										PO BUYER: <span style="font-size: 16px">
										@if($web_po->pobuyer == '' || $web_po->pobuyer == null)
											BARCODE ALLOCATION
										@else
										@php
											$replace_po_buyer = trim(str_replace('-S', '',$web_po->pobuyer));
											$list_po_buyer = explode(', ', $replace_po_buyer);
											$po_buyer      = array_unique($list_po_buyer);
											$po_buyer      = implode(',', $po_buyer);
											$has_dash = strpos($po_buyer, ",");
											$uom_need_prepare = false;

											if ($web_po->uomsymbol == 'YDS' || $web_po->uomsymbol == 'M' || $web_po->uomsymbol == 'CNS')
											{
												$uom_need_prepare = true;
											}
											else
											{
												$uom_need_prepare = false;
											}
										@endphp

											@if ($has_dash == false && $uom_need_prepare == false)
												{{ $po_buyer }}
											@else
												HAS MANY PO BUYER
											@endif
										@endif
											
										</span>
									</div>
								@endif
							</div>
						</div>		
					</div>
					@php $flag++;@endphp
					
				@endfor
			
			@endforeach
		@endif
    </body>
</html>