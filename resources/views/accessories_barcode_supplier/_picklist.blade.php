<div id="{{ $name }}Modal" class="modal fade" style="color:black !important">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">{{ $title }}</h5>
			</div>

			<div class="modal-body">
				<fieldset>
					<div class="form-group">
						<div class="col-xs-12">
							<div class="input-group">
								<input type="text" id="{{ $name }}Search"  class="form-control" placeholder="Silahkan masukan PO Supplier." autofocus="true">
								<input type="text" id="{{ $name5 }}Search5"  class="form-control" placeholder="Silahkan masukan PO Buyer." autofocus="true">
								<input type="text" id="{{ $name2 }}Search2"  class="form-control" placeholder="Silahkan masukan Kode Item." autofocus="true">
								<input type="text" id="{{ $name3 }}Search3"  class="form-control" placeholder="Silahkan masukan No Packing List." autofocus="true">
								<input type="text" id="{{ $name4 }}Search4"  class="form-control" placeholder="Silahkan masukan No Invoice." autofocus="true">
								
								<span class="input-group-btn">
									<button class="btn bg-teal" type="button" id="ButtonSrc" style="height: 180px;">Search</button>
								</span>
							</div>
						</div>
					</div>
				</fieldset>
				</br>
				<div class="shade-screen" style="text-align: center;">
					<div class="shade-screen hidden">
						<img src="/img/ajax-loader.gif">
					</div>
				</div>
				<div class="table-responsive" id="{{ $name }}Table"></div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
