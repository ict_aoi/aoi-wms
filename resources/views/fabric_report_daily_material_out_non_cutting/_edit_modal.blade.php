<div class="modal fade" id="editModal" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="updateReportStockModal">
        <div class="modal-dialog" role="document">
		{{ 
			Form::open(['method' 	=> 'POST'
			,'id' 					=> 'updateForm'
			,'class' 				=> 'form-horizontal'
			,'url' 					=> route('fabricReportDailyMaterialOutNonCutting.update')])
		}}
		
		    <div class="modal-content">
                <div class="modal-header bg-blue">
					<h5 class="modal-title">Edit <span id="update_header"></span></h5>
				</div>
				<div class="modal-body">
					<div class="row">  
						<div class="col-md-12">
						    
							@include('form.text', [
								'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
								'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
								'field' 		=> 'no_pt',
								'label' 		=> 'No PT',
								'placeholder' 	=> 'Please input no pt here',
								'attributes' 	=> [
									'id' 		=> 'update_no_pt'
								]
                            ])

							@include('form.text', [
								'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
								'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
								'field' 		=> 'no_bc',
								'label' 		=> 'No BC',
								'placeholder' 	=> 'Please input no bc here',
								'attributes' 	=> [
									'id' 		=> 'update_no_bc'
								]
                            ])

                            @include('form.text', [
								'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
								'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
								'field' 		=> 'uom',
								'label' 		=> 'Uom',
								'attributes' 	=> [
									'id' 		=> 'update_uom',
									'readonly' 	=> true
								]
                            ])

							@include('form.text', [
								'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
								'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
								'field' 		=> 'total_qty_handover',
								'label' 		=> 'Total Qty Handover',
								'placeholder' 	=> 'Please input total qty handover here',
								'help'			=> 'Total qty handover cannot be less than total supplied',
								'attributes' 	=> [
									'id' 		=> 'update_total_qty_handover',
								]
                            ])

							@include('form.text', [
								'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
								'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
								'field' 		=> 'total_qty_supplied',
								'label' 		=> 'Total Qty Supplied',
								'attributes' 	=> [
									'id' 		=> 'update_total_qty_supplied',
									'readonly' 	=> true
								]
                            ])

                            {!! Form::hidden('id','', array('id' => 'update_id')) !!}
                            {!! Form::hidden('update_old_total_qty_handover','', array('id' => 'update_old_total_qty_handover')) !!}
						</div>
					</div>
				</div>
                <div class="modal-footer text-center">
					<button type="submit" class="btn btn-blue-success"> Save <i class="icon-floppy-disk position-left"></i></button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close <span class="glyphicon glyphicon-remove"></span></button>
                </div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>