@extends('layouts.app', ['active' => 'fabric_report_daily_material_out_non_cutting'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Daily Material Out Non Cutting</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li><a href="{{ route('fabricReportDailyMaterialOutNonCutting.index') }}">Daily Material Out Non Cutting</a></li>
				<li class="active">Detail</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
	<div class="panel-heading">
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
			</ul>
		</div>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6 col-lg-6 col-sm-12">
				<p>Po Supllier <b>{{ $daily_handover->document_no }}</b></p>
				<p>Supplier Name <b>{{ $daily_handover->supplier_name }}</b></p>
				<p>Created Date <b>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $daily_handover->created_at)->format('d/M/Y H:i:s') }}</b></p>
				<p>Complete Date <b>{{ ( $daily_handover->complete_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $daily_handover->complete_date)->format('d/M/Y H:i:s') : '-') }}</b></p>
				<p>NO PT <b>{{ $daily_handover->no_pt }}</b></p>
			</div>
			<div class="col-md-6 col-lg-6 col-sm-12">
				<p>Item Code :  <b>{{ $daily_handover->item_code }}</b></p>
				<p>Color :  <b>{{ $daily_handover->color }}</b></p>
				<p>Total Handover :  <b>{{ $daily_handover->total_qty_handover }} ({{ $daily_handover->uom }})</b></p>
				<p>Total Supplied :  <b>{{ $daily_handover->total_qty_supply }} ({{ $daily_handover->uom }})</b></p>

			</div>
		</div>
	</div>
</div>

<div class="panel panel-default border-grey">
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table datatable-basic table-striped table-hover table-responsive" id="daily_data_material_handover_table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Created At</th>
						<th>Created By</th>
						<th>Barcode</th>
						<th>No Roll</th>
						<th>Batch Number</th>
						<th>Uom</th>
						<th>Qty Handover</th>
						<th>Warehouse Destination</th>
						<th>User Receive</th>
						<th>Date Receive</th>
						<th>Action</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
{!! Form::hidden('page','detail', array('id' => 'page')) !!}
{!! Form::hidden('url_fabric_report_daily_material_handover',route('fabricReportDailyMaterialOutNonCutting.dataDetail',$daily_handover->id), array('id' => 'url_fabric_report_daily_material_handover')) !!}
@endsection

@section('page-js')
	<script src="{{ mix('js/fabric_report_daily_material_out_non_cutting.js') }}"></script>
@endsection
