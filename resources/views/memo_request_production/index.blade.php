<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>WMS | Warehouse Management System</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <!-- Global stylesheets -->

	    <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	    <link href="{{ mix('css/backend.css') }}" rel="stylesheet" type="text/css">
       
    </head>
    
	<body class="login-cover">
		<div class="navbar-inverse-login bg-transparent">
            <div class="navbar-header">
                <ul class="nav navbar-nav pull-right visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile" class="legitRipple"><i class="icon-menu7"></i></a></li>
                </ul>
            </div>

            <div class="navbar-collapse collapse" id="navbar-mobile">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#" class="legitRipple">
                        <i class="icon-database"></i> Material Requirement
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('memoRequestProduction.index' )}}" class="legitRipple">
                            <i class="icon-archive"></i>  Memo Request Production (MRP) 
                        </a>
                    </li>
                </ul>
            </div>
        </div>

		<div class="page-header">
			<div class="page-header-content">
				<div class="page-title">
					<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Material Movement</span></h4>
				</div>
			</div>
			<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
				<ul class="breadcrumb">
					<li><a href="{{ route('login') }}"><i class="icon-home2 position-left"></i> Home</a></li>
					<li class="active">Material Movement</li>
				</ul>
			</div>
		</div>
		
		<div class="page-container">
			<div class="page-content">
				<div class="panel panel-default border-grey">
					<div class="panel-heading">
						<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
						<div class="heading-elements">
							<ul class="icons-list">
								<li><a data-action="collapse"></a></li>
							</ul>
						</div>
					</div>
					<div class="panel-body">
						{!!
							Form::open(array(
								'class' 	=> 'form-horizontal',
								'role' 		=> 'form',
								'url' 		=> route('memoRequestProduction.export'),
								'method' 	=> 'get',
								'target'    => '_blank'
							))
						!!}	
							

							@include('form.picklist', [
								'field' 			=> 'po_buyer',
								'name' 				=> 'po_buyer',
								'placeholder' 		=> 'Select po buyer',
								'div_class' 		=> 'col-xs-12',
								'form_col' 			=> 'col-xs-12',
								'readonly'			=> true
							]) 

							<button type="submit" class="btn btn-default col-xs-12" style="margin-top: 15px">Export <i class="icon-file-excel position-right"></i></button>
						{!! Form::close() !!}

						{!!
							Form::open(array(
								'class' 	=> 'form-horizontal',
								'role' 		=> 'form',
								'url' 		=> route('memoRequestProduction.print'),
								'method' 	=> 'get',
								'target'    => '_blank'
							))
						!!}	
							{!! Form::hidden('_print_po_buyer',null, array('id' => '_print_po_buyer')) !!}	
							@php 
								$akses = 'print-mrp';
								$role_name = \Auth::user()->roles->pluck('name')->toArray();
								$warehouse = Auth::user()->warehouse;
							@endphp
							@if(in_array($akses, $role_name) || $warehouse == '1000002'|| $warehouse == '1000001' || $warehouse == '1000011') 
								<button type="submit" class="btn btn-yellow-cancel col-xs-12">Print Preview <i class="icon-printer position-right"></i></button>
							@endif
						{!! Form::close() !!}
						<a href="{{ route('memoRequestProduction.downloadAll') }}" class="btn btn-blue-success col-xs-12">Download All <i class="icon-file-download2 position-left"></i></a>

					</div>
				</div>

				<div class="panel panel-default border-grey">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table datatable-basic table-striped table-hover table-responsive" id="report_mrp_table">
								<thead>
									<tr>
										<th>Season</th>
										<th>PODD Date</th>
										<th>Backlog Status</th>
										<th>Style</th>
										<th>Article No</th>
										<th>Po Buyer</th>
										<th>Category</th>
										<th>Item Code</th>
										<th>Uom</th>
										<th>Qty Need</th>
										<th>Qty Available</th>
										<th>Qty Supply</th>
										<th>Po Supplier</th>
										<th>Last Status Movement</th>
										<th>Last Warehouse Location</th>
										<th>Last Location</th>
										<th>Last Movement Date</th>
										<th>Last Movement User</th>
										<th>Remark</th>
										<th>System Note</th>
										<th>Action</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>

				@include('form.modal_picklist', [
					'name' 			=> 'po_buyer',
					'title' 		=> 'Filter PO Buyer',
					'placeholder' 	=> 'Search based on po buyer'
				])

				@include('memo_request_production._history_modal')
            </div>
        </div>
        <div class="footer text-muted">
            &copy; 2018. <a href="#" style="color:black">WMS | Warehouse Management System</a>
        </div>
        <!-- /footer -->

        <script src="{{ mix('js/backend.js') }}"></script>
        <script type="text/javascript" src="{{ asset(elixir('js/memo_request_production.js'))}}"></script>

    </body>
</html>
