@extends('layouts.app', ['active' => 'accessories_report_summary_material_stock'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Material Stock Per Item (BC)</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li class="active">Material Stock Per Item (BC)</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>

		<div class="panel-body">
			@include('form.select', [
				'field' 		=> 'warehouse',
				'label' 		=> 'Warehouse',
				'default' 		=> auth::user()->warehouse,
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'options' 		=> [
					'' 			=> '-- All Warehouse --',
					'1000013' 	=> 'Warehouse Accessories AOI 2',
					'1000002' 	=> 'Warehouse Accessories AOI 1',
				],
				'class' 		=> 'select-search',
				'attributes' 	=> [
					'id' 		=> 'select_warehouse'
				]
			])
			
			{!!
				Form::open(array(
					'class' 	=> 'heading-form',
					'role' 		=> 'form',
					'url' 		=> route('accessoriesReportMaterialSummaryStock.export'),
					'method' 	=> 'get',
					'target' 	=> '_blank'		
				))
			!!}
			{!! Form::hidden('_warehouse_id',auth::user()->warehouse, array('id' => '_warehouse_id')) !!}
			<button type="submit" class="btn btn-default col-xs-12">Export Stock All<i class="icon-file-excel position-left"></i></button>
			{!! Form::close() !!}
			{!!
				Form::open(array(
					'class' 	=> 'heading-form',
					'role' 		=> 'form',
					'url' 		=> route('AccessoriesReportMaterialSummaryStock.exportDetail'),
					'method' 	=> 'get',
					'target' 	=> '_blank'		
				))
			!!}
			{!! Form::hidden('_warehouse_id',auth::user()->warehouse, array('id' => '_detail_warehouse_id')) !!}
			<button type="submit" class="btn btn-default col-xs-12">Export Detail Stock All<i class="icon-file-excel position-left"></i></button>
			{!! Form::close() !!}
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover " id="summary_stock_table">
					<thead>
						<tr>
							<th>No </th>
							<th>Warehouse </th>
							<th>Item Id</th>
							<th>Item Code</th>
							<th>Category</th>
							<th>Uom</th>
							<th>Total Qty</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>

	{!! Form::hidden('page','index', array('id' => 'page')) !!}
@endsection

@section('page-js')
	<script src="{{ mix('js/accessories_report_summary_material_stock.js') }}"></script>
@endsection
