@extends('layouts.app', ['active' => 'fabric_report_material_quality_control'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Material Quality Control (FIR)</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li>Report</li>
            <li><a href="{{ route('fabricReportMaterialQualityControl.index') }}">Material Quality Control (FIR)</a></li>
            <li class="active">Detail</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Header<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-4">
					<p>Document No : <b>{{$header->document_no}}</b></p>
					<p>Item Code :  <b>{{$header->item_code}}</b></p>
					<p>Inspect Date :  <b>{{ ($header->inspection_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $header->inspection_date)->format('d/M/Y H:i:s') : '')}}</b></p>
					<p>Received Date :  <b>{{ ($header->receiving_date ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $header->receiving_date)->format('d/M/Y H:i:s') : '') }}</b></p>
				</div>
				<div class="col-md-4">
					<p>Supplier Name :  <b>{{$header->supplier_name}}</b></p>
					<p>Color :  <b>{{$header->color}}</b></p>
					<p>Total Batch Number :  <b>{{$header->total_batch_number}}</b></p>
					<p>Total Inspected Batch Number :  <b>{{$header->total_inspected_batch_number}}</b></p>

				</div>
				<div class="col-md-4">
					<p>No Invoice :  <b>{{$header->no_invoice}}</b></p>
					<p>Status : <b>{{$header->status}}</b></p> 
					<p>Style : <b>{{( $style ? $style->style:null ) }}</b></p>
					<p>Composition : <b>{{$header->composition}}</b></p>  
				</div>
			</div>

			{!!
				Form::open(array(
					'class' => 'form-horizontal',
					'role' => 'form',
					'url' => route('fabricReportMaterialQualityControl.exportDetail',$header->id),
					'method' => 'get'		
				))
			!!}
				<button type="submit" class="btn btn-default col-xs-12">Export All <i class="icon-file-excel position-left"></i></button>
			{!! Form::close() !!}

			{!!
				Form::open(array(
					'class' 	=> 'form-horizontal',
					'role' 		=> 'form',
					'url' 		=> route('fabricReportMaterialQualityControl.confirmFinalResult'),
					'method' 	=> 'post',
					'id'		=> 'form'		
				))
			!!}
				{!! Form::hidden('list_confirm','[]' , array('id' => 'list_confirm')) !!}
				<button type="submit" class="btn btn-blue-success col-xs-12">Confirm Final Result <i class="icon-floppy-disk position-left"></i></button>
			{!! Form::close() !!}
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Quality Control Inspect<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover " id="fabric_report_material_quality_detail_table">
					<thead>
						<tr>
							<th rowspan="2">id</th>
							<th rowspan="2">Date Of</th>
							<th rowspan="2">No Roll</th>
							<th rowspan="2">Batch Number</th>
							<th rowspan="2">Actual Lot</th>
							<th rowspan="2">N.W (Kg s)</th>
							<th rowspan="2">Sticker Yds Qty</th>
							<th rowspan="2">Width On Barcode</th>
							<th rowspan="2">Yards Actual</th>
							<th rowspan="2">Yds Diff</th>
							<th rowspan="2">Cuttable Width</th>
							<th colspan="20" style="text-align: center">Defective Code</th>
							<th colspan="2" style="text-align: center">Total</th>
							<th colspan="2" style="text-align: center">Fabric Replacement Summary(Yards)</th>
							<th rowspan="2" style="text-align: center">Created By</th>
							<th rowspan="2" style="text-align: center">Confirm By</th>
							<th rowspan="2">Remark</th>
							<th rowspan="2">Confirm Final Result</th>
							<th rowspan="2">Note</th>
							<th rowspan="2">Action</th>
						</tr>
						<tr>
							<th>A</th>
							<th>B</th>
							<th>C</th>
							<th>D</th>
							<th>E</th>
							<th>F</th>
							<th>G</th>
							<th>H</th>
							<th>I</th>
							<th>J</th>
							<th>K</th>
							<th>L</th>
							<th>M</th>
							<th>N</th>
							<th>O</th>
							<th>P</th>
							<th>Q</th>
							<th>R</th>
							<th>S</th>
							<th>T</th>
							<th>Linear Point</th>
							<th>100SQ/YD</th>
							<th style="text-align: center">Formula 1</th>
							<th style="text-align: center">Formula 2</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Preparation Inspect<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover " id="fabric_report_material_quality_detail_other_table">
					<thead>
						<tr>
							<th>id</th>
							<th>Date Of</th>
							<th>No Roll</th>
							<th>Batch Number</th>
							<th>Sticker Yds Qty</th>
							<th>Yds Diff</th>
							<th>Cuttable Width</th>
							<th>Actual Yds</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	{!! Form::hidden('flag_msg',$flag , array('id' => 'flag_msg')) !!}
	{!! Form::hidden('page','detail', array('id' => 'page')) !!}
	{!! Form::hidden('url_fabric_material_quality_control_detail_data_other',route('fabricReportMaterialQualityControl.dataDetailOther',$header->id), array('id' => 'url_fabric_material_quality_control_detail_data_other')) !!}
	{!! Form::hidden('url_fabric_material_quality_control_detail_data',route('fabricReportMaterialQualityControl.dataDetail',$header->id), array('id' => 'url_fabric_material_quality_control_detail_data')) !!}
@endsection



@section('page-modal')
	@include('fabric_report_material_quality_control._qc_issue_modal')
	@include('fabric_report_material_quality_control._update_actual_width_modal')
@endsection

@section('page-js')
	
	<script src="{{ mix('js/fabric_report_material_quality_control.js') }}"></script>
@endsection

