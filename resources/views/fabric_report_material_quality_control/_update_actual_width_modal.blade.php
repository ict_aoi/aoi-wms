<div class="modal fade" id="updateActualWidhModal" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="updateActualWidhModal">
        <div class="modal-dialog" role="document">
		{{ 
			Form::open([
				'method' 	=> 'POST',
				'id' 		=> 'updateActualWidthform',
				'class' 	=> 'form-horizontal',
				'url' 		=> route('fabricReportMaterialQualityControl.updateActualWidth')])
		}}
		
		    <div class="modal-content">
                <div class="modal-header bg-blue">
					<h5 class="modal-title">Edit Actual Width for <span id="update_header"></span></h5>
				</div>
				<div class="modal-body">
					@include('form.text', [
						'label_col' 	=> 'col-xs-3',
						'form_col' 		=> 'col-xs-9',
						'field' 		=> 'top_width',
						'label' 		=> 'Top Width',
						'class'			=> 'width',
						'placeholder' 	=> 'Insert Top Width',
						'attributes' 	=> [
							'id' 		=> 'top_width'
						]
					])

					@include('form.text', [
						'label_col' 	=> 'col-xs-3',
						'form_col' 		=> 'col-xs-9',
						'field' 		=> 'middle_width',
						'class'			=> 'width',
						'label' 		=> 'Middle Width',
						'placeholder' 	=> 'Insert Middle Width',
						'attributes' 	=> [
							'id' 		=> 'middle_width'
						]
					])

					@include('form.text', [
						'label_col' 	=> 'col-xs-3',
						'form_col' 		=> 'col-xs-9',
						'field' 		=> 'bottom_width',
						'class'			=> 'width',
						'label' 		=> 'Bottom Width',
						'placeholder' 	=> 'Insert Bottom Width',
						'attributes' 	=> [
							'id' 		=> 'bottom_width'
						]
					])

					@include('form.text', [
						'label_col' 	=> 'col-xs-3',
						'form_col' 		=> 'col-xs-9',
						'field' 		=> 'actual_width',
						'label' 		=> 'Actual Width',
						'attributes' 	=> [
							'id' 		=> 'actual_width',
							'readonly'	=> 'readonly'
						]
					])

					{!! Form::hidden('_top_width','', array('id' => '_top_width')) !!}
					{!! Form::hidden('_middle_width','', array('id' => '_middle_width')) !!}
					{!! Form::hidden('_bottom_width','', array('id' => '_bottom_width')) !!}
					{!! Form::hidden('_actual_width','', array('id' => '_actual_width')) !!}
					{!! Form::hidden('id','', array('id' => '_id')) !!}
				</div>
                <div class="modal-footer text-center">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close <span class="glyphicon glyphicon-remove"></span></button>
                	<button type="submit" class="btn btn-blue-success"> Save <i class="icon-floppy-disk position-left"></i></button>
				</div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>