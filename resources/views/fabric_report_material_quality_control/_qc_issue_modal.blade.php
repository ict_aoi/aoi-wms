<div class="modal fade" id="issueModalModal" tabindex="-1" data-backdrop="static" data-keyboard="false" style="color:black !important">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Issue Qc For <span id="issue_title"></span></h5>
			</div>
					
			<div class="modal-body">
				@include('form.text', [
					'field' 		=> 'qty_arrival',
					'label_col' 	=> 'col-xs-12',
					'form_col' 		=> 'col-xs-12',
					'label' 		=> 'Qty Arrival ( Sticker Yds Qty )',
					'attributes' => [
						'id' 		=> 'issue_qty_arrival',
						'readonly' 	=> 'true',

					]
				])

				@include('form.text', [
					'field' 		=> 'formula_1',
					'label_col' 	=> 'col-xs-12',
					'form_col' 		=> 'col-xs-12',
					'label' 		=> 'Formula 1',
					'attributes' => [
						'id' 		=> 'issue_formula_1',
						'readonly' 	=> 'true',

					]
				])
				@include('form.text', [
					'field' 		=> 'formula_2',
					'label_col' 	=> 'col-xs-12',
					'form_col' 		=> 'col-xs-12',
					'label' 		=> 'Formula 2',
					'attributes' => [
						'id' 		=> 'issue_formula_2',
						'readonly' 	=> 'true',

					]
				])

				@include('form.text', [
					'label_col' 	=> 'col-xs-12',
					'form_col' 		=> 'col-xs-12',
					'field' 		=> 'qty_issue',
					'label'			=> 'Qty Issue',
					'placeholder' 	=> 'Qty Issue',
					'mandatory' 	=> '*Required',
					'attributes' 	=> [
						'id' => 'qty_issue',
					]
				])

				{!! Form::hidden('roll_id','', array('id' => 'roll_id')) !!}
			</div>

			<div class="modal-footer text-right">
				<button type="button" class="btn btn-default hidden" data-dismiss="modal" id="btnCloseModal">Close <span class="glyphicon glyphicon-remove"></span></button>
				<button type="button" class="btn btn-blue-success col-xs-12" onClick="saveQtyIssue()">Save <i class="icon-floppy-disk position-left"></i></button>
			</div>
		</div>
	</div>
</div>