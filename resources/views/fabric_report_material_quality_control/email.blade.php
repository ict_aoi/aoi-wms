<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8">
</head>
<body>
    <h3>Daftar Outstanding FIR di WMS</h3>

    <div>
        <p style="margin: 1em 0;"><b>Mohon perhantian dan kerja samanya</b><br/>
        <br>Dibawah ini adalah material yang belum dilakukan konfirmasi status qc final di wms <br/>
        <b>(Total yang belum terkonfirmasi {{ $total_outstanding }} baris.</b>
        </p>
        
        <p style="margin: 1em 0 1em 15px;">
            <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>DATE FABRIC RECEIVED</th>
                        <th>DATE INSPECTED</th>
                        <th>INSPECTOR NAME</th>
                        <th>WAREHOUSE NAME</th>
                        <th>DOCUMENT NO</th>
                        <th>SUPPLIER NAME</th>
                        <th>ITEM CODE</th>
                        <th>NO INVOICE</th>
                        <th>NOMOR ROLL</th>
                        <th>BATCH NUMBER</th>
                        <th>LINEAR POINT</th>
                        <th>100 SQ YDS</th>
                        <th>FORMULA 1</th>
                        <th>FORMULA 2</th>
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $key => $datum)
                        @if($key < 100)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $datum->date_fabric_received }}</td>
                                <td>{{ $datum->date_inspected }}</td>
                                <td>{{ $datum->inspector_name }}</td>
                                <td>{{ $datum->warehouse_name }}</td>
                                <td>{{ $datum->document_no }}</td>
                                <td>{{ $datum->supplier_name }}</td>
                                <td>{{ $datum->item_color_width }}</td>
                                <td>{{ $datum->no_invoice }}</td>
                                <td>{{ $datum->nomor_roll }}</td>
                                <td>{{ $datum->batch_number }}</td>
                                <td>{{ $datum->linear_point }}</td>
                                <td>{{ $datum->sq_yard }}</td>
                                <td>{{ $datum->formula_1 }}</td>
                                <td>{{ $datum->formula_2 }}</td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </p>
    </div>

    <div style="font-size: 15px; margin-top: 10px">
        <h2> Silahkan cek attachement untuk melihat keseluruhan data</h2>
        Email ini dikirimkan otomatis melalui aplikasi WMS. diharapkan untuk tidak membalas email ini.
        <br/>
        <b>Jangan Lupa untuk melakukan konfirmasi di WMS supaya material dapat digunakan untuk proses relax</b> 
        <p style="margin: 1em 0;" style="margin-top: 5px">Demikan yang dapat diinfokan, <br/>
        Terima kasih,<br/>
        WMS - Autobot</p>
    </div>
    <br>

    <hr>

</body>
</html>