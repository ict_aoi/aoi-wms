@extends('layouts.app', ['active' => 'fabric_report_material_quality_control'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Material Quality Control (FIR)</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li>Report</li>
            <li><a href="{{ route('fabricReportMaterialQualityControl.index') }}">Material Quality Control (FIR)</a></li>
            <li><a href="{{ route('fabricReportMaterialQualityControl.detail',$header_id) }}">Detail</a></li>
            <li class="active">Point</li>
        </ul>
    </div>
</div>

@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Header<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-4">
					<p>Inspect Date :  <b>{{ ($material_check->created_at ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $material_check->created_at)->format('d/M/Y H:i:s') : '')}}</b></p>
					<p>Received Date :  <b>{{ ($material_check->materialStock->created_at ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $material_check->materialStock->created_at)->format('d/M/Y H:i:s') : '') }}</b></p>
					<p>No Roll : <b> {{$material_check->materialStock->nomor_roll}}</b></p>
					<p>Batch Number : <b> {{$material_check->materialStock->batch_number}}</b></p>
					<p>Sticker Yds Qty : <b> {{$material_check->qty_on_barcode}}</b></p>
					<p>witdh On Barcode : <b> {{$material_check->width_on_barcode}}</b></p>
				</div>
				<div class="col-md-4">
					<p>Yards Actual Qty : <b> {{$material_check->actual_length}}</b></p>
					<p>Yards Diffreren : <b> {{$material_check->different_yard}}</b></p>
					<p>Cuttable Width : <b> {{$material_check->actual_width}}</b></p>
					<p>Create by : <b> {{$material_check->user->name}}</b></p>
					<p>Confirm By : <b> {{ ($material_check->confirm_user_id ? $material_check->confirmUser->name.' at '.$material_check->confirm_date->format('d/M/Y H:i:s') : '')}}</b></p>
				</div>
				<div class="col-md-4">
					<p>Remark : <b> {{$material_check->final_result}}</b></p>
					<p>Note : <b> {{$material_check->note}}</b></p>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover " id="fabric_report_material_quality_table">
					<thead>
						<tr>
							<th rowspan="2" style="text-align: center">Start Point Check</th>
							<th rowspan="2" style="text-align: center">End Point Check</th>
							<th rowspan="2" style="text-align: center">Defect Code</th>
							<th rowspan="2" style="text-align: center">Remark</th>
							<th colspan="4" style="text-align: center">Defect Code Value</th>
						</tr>
						<tr>
							<th style="text-align: center">1</th>
							<th style="text-align: center">2</th>
							<th style="text-align: center">3</th>
							<th style="text-align: center">4</th>
						</tr>
					</thead>
					<tbody id="tbody-detail-fir">
					</tbody>
					<tfoot>
						<tr style="text-align: center">
							<td></td>
							<td></td>
							<td></td>
							<td>Point</td>
							<td><input type="text" class="form-control" id="point_1" name="point_1" value="{{$obj->point_1}}" readonly></td>
							<td><input type="text" class="form-control" id="point_2" name="point_2" value="{{$obj->point_2}}" readonly></td>
							<td><input type="text" class="form-control" id="point_3" name="point_3" value="{{$obj->point_3}}" readonly></td>
							<td><input type="text" class="form-control" id="point_4" name="point_4" value="{{($obj->point_4)}}" readonly></td>
						</tr>
						<tr style="text-align: center">
							<td></td>
							<td></td>
							<td></td>
							<td>Percentage</td>
							<td><input type="text" class="form-control" id="percentage" name="percentage" value="{{($obj->percentage)?$obj->percentage:0}}" readonly></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr style="text-align: center">
							<td></td>
							<td></td>
							<td></td>
							<td>Total Point</td>
							<td><input type="text" class="form-control" id="total_point" name="total_point" value="{{$obj->total_point ?$obj->total_point:0}}" readonly></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</tfoot>
				</table>
			</div>
			{!!
				Form::open([
					'role' 			=> 'form',
					'url' 			=> route('fabricReportMaterialQualityControl.updatePoint',[$header_id,$material_check->id]),
					'method' 		=> 'post',
					'enctype' 		=> 'multipart/form-data',
					'class' 		=> 'form-horizontal',
					'id'			=> 'form'
				])
			!!}
				
				{!! Form::hidden('detail_fir',$detail , array('id' => 'detail_fir')) !!}
				{!! Form::hidden('header',json_encode($obj), array('id' => 'header')) !!}
				{!! Form::hidden('list_deleted','[]' , array('id' => 'list_deleted')) !!}
				{!! Form::hidden('defect_codes',$defect_codes , array('id' => 'defect_codes')) !!}
				
				<div class="form-group text-right" style="margin-top: 10px;">
					@if($material_check->confirm_user_id && $material_check->final_result =='REJECT' )
						<b>POINT CANNOT BE UPDATED DUE FINAL RESULT IS REJECT AND ALREADY CONFIRM BY QC</b>
					@else
						<button type="submit" class="btn btn-blue-success btn-lg">Save <i class="icon-floppy-disk position-left"></i></button>
					@endif
				</div>
			{!! Form::close() !!}
			
			
		</div>
	</div>
	{!! Form::hidden('page','index', array('id' => 'page')) !!}
	{!! Form::hidden('url_fabric_material_quality_control_detail',route('fabricReportMaterialQualityControl.detail',$header_id), array('id' => 'url_fabric_material_quality_control_detail')) !!}
@endsection

@section('page-js')
	@include('fabric_report_material_quality_control._defect_code')
	<script src="{{ mix('js/fabric_report_material_quality_control_detail_points.js') }}"></script>
@endsection

