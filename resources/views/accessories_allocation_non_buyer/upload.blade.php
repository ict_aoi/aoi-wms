@extends('layouts.app', ['active' => 'accessories_barcode_non_buyer'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Allocation Non Buyer</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Allocation</li>
				<li><a href="{{ route('accessoriesAllocationNonBuyer.index') }}">Non Buyer</a></li>
				<li class="active">Upload</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	@role(['admin-ict-acc'])
		<div class="panel panel-default border-grey">
			<div class="panel-heading">
				<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
					</ul>
				</div>
			</div>
			<div class="panel-body">
				@include('form.select', [
					'field' 			=> 'warehouse',
					'label' 			=> 'Warehouse',
					'default'			=> auth::user()->warehouse,
					'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
					'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
					'options' 			=> [
						'1000002' 		=> 'Warehouse Accessories AOI 1',
						'1000013' 		=> 'Warehouse Accessories AOI 2',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_warehouse'
					]
				])
			</div>
		</div>
	@else 
		{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
	@endrole
	
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp </h6>
			<div class="heading-elements">
				<div class="heading-btn">
					<a class="btn btn-primary btn-icon" href="{{ route('accessoriesAllocationNonBuyer.exportFileUpload') }}" data-popup="tooltip" title="download form import" data-placement="bottom" data-original-title="download form import"><i class="icon-download"></i></a>
					<button type="button" class="btn btn-info btn-icon" id="upload_button" data-popup="tooltip" title="upload form import" data-placement="bottom" data-original-title="upload form import"><i class="icon-upload"></i></button>
				</div>
			</div>
		</div>

		<div class="panel-body">
			{!! Form::hidden('allocation_non_po_buyers','[]',array('id' => 'allocation_non_po_buyers')) !!}
			<div class="table-responsive">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th>Source Stock</th>
							<th>Item</th>
							<th>Uom</th>
							<th>Qty Allocation</th>
							<th>Note</th>
							<th>Result</th>
						</tr>
					</thead>
					<tbody id="tbody-upload-allocation-non-buyer">
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<hr>
	

	{!!
		Form::open([
			'role' 			=> 'form',
			'url' 			=> route('accessoriesAllocationNonBuyer.import'),
			'method' 		=> 'post',
			'id' 			=> 'upload_file_allocation',
			'enctype' 		=> 'multipart/form-data'
		])
	!!}
		{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_id')) !!}
		<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
		<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
	{!! Form::close() !!}
@endsection

@section('page-js')
	@include('accessories_allocation_non_buyer._item')
	<script src="{{ mix('js/accessories_allocation_non_buyer_import.js') }}"></script>
@endsection