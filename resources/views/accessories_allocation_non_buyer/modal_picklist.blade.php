<div id="{{ $name }}Modal" class="modal fade" style="color:black !important">
	<div class="modal-dialog modal-lg" style="width: 75%;">
		<div class="modal-content">
			<div class="modal-header btn-blue-success">
				<h5 class="modal-title">{{ $title }}</h5>
			</div>

			<div class="modal-body">
				<fieldset>
					<div class="form-group">
						<div class="col-xs-12">
							<div class="input-group">
								<input type="text" id="{{ $name }}Search"  class="form-control" placeholder="Please type po supplier here" autofocus="true">
								<input type="text" id="{{ $name2 }}Search2"  class="form-control" placeholder="Please type po buyer here." autofocus="true">
								<input type="text" id="{{ $name3 }}Search3"  class="form-control" placeholder="Please type item code here." autofocus="true">
								
								<span class="input-group-btn">
									<button class="btn btn-yellow-cancel legitRipple" type="button" id="ButtonSrc" style="height: 110px;">Search <span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
								</span>
							</div>
						</div>
					</div>
				</fieldset>
				</br>
				<div class="shade-screen" style="text-align: center;">
					<div class="shade-screen hidden">
						<img src="/images/ajax-loader.gif">
					</div>
				</div>
				<div class="table-responsive" id="{{ $name }}Table"></div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
