@extends('layouts.app', ['active' => 'master_data_defect'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Data Fabric Testing</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data</li>
				<li><a href="{{ route('masterDataFabricTesting.index') }}" id="url_master_data_fabric_testing_index">Fabric Testing</a></li>
				<li class="active">Create</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			{{ Form::open([
				'method' => 'POST',
				'id' => 'form',
				'class' => 'form-horizontal',
				'url' => route('masterDataFabricTesting.storeDetail')]) 
			}}
				
			@include('form.text', [
				'field' 		=> 'subtesting_name',
				'label' 		=> 'Subtesting Name',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'placeholder' 	=> 'Please type subtesting here',
				'attributes' 	=> [
					'style' 	=> 'resize:none',
					'id' => 'subtesting_name'
				]
			])

			@include('form.text', [
				'field' 		=> 'require',
				'label' 		=> 'Require',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'placeholder' 	=> 'Please type require here',
				'attributes' 	=> [
					'style' 	=> 'resize:none',
					'id' => 'require'
				]
			])

			{!! Form::hidden('url_detail_fabric_testing',route('masterDataFabricTesting.dataDetail',$master_data_fabric_testing->id), array('id' => 'url_detail_fabric_testing')) !!}
			{!! Form::hidden('master_data_fabric_testing_id',$master_data_fabric_testing->id, array('id' => 'master_data_fabric_testing_id')) !!}
			<button type="submit" class="btn btn-blue-success col-xs-12" style="margin-top: 15px">Save <i class="icon-floppy-disk position-right"></i></button>
		{{ Form::close() }}
		</div>
	</div>
@endsection

@section('page-js')
	<script src="{{ mix('js/detail_master_data_fabric_testing.js') }}"></script>
@endsection
