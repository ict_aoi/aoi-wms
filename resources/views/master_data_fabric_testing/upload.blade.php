@extends('layouts.app', ['active' => 'erp_material_requiremet'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Data Fabric Testing</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data Fabric Testing</li>
				<li class="active">Upload</li>
			</ul>
		</div>
	</div>
@endsection


@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a class="btn btn-primary btn-icon" href="{{ route('masterDataFabricTesting.export')}}" data-popup="tooltip" title="download form upload" data-placement="bottom" data-original-title="download form upload"><i class="icon-download"></i></a>
				<button type="button" class="btn btn-success btn-icon" id="upload_button" data-popup="tooltip" title="upload form upload" data-placement="bottom" data-original-title="upload form upload"><i class="icon-upload"></i></button>

				{!!
					Form::open([
						'role' => 'form',
						'url' => route('masterDataFabricTesting.import'),
						'method' => 'POST',
						'id' => 'upload_fabric_testing',
						'enctype' => 'multipart/form-data'
					])
				!!}
					<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
					<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
				{!! Form::close() !!}

			</div>
		</div>

		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover table-responsive">
					<thead>
						<tr>
							<th>Testing Classification</th>
							<th>Method</th>
							<th>Testing Name</th>
							<th>Testing code</th>
							<th>Requirement</th>
							<th>Calculation Method</th>
							<th>Min</th>
							<th>Max</th>
							<th>Operator</th>
							<th>Detail Requirement</th>
							<th>Remark</th>
						</tr>
					</thead>
					<tbody id="tbody-upload-fabric-testing">
					</tbody>
				</table>
			</div>
		</div>
	</div>

{!! Form::hidden('flag',null , array('id' => 'flag')) !!}
{!! Form::hidden('upload_fabric_testings','[]' , array('id' => 'upload_fabric_testings')) !!}
@endsection

@section('page-js')
	@include('master_data_fabric_testing._item')
	<script type="text/javascript" src="{{ asset(elixir('js/upload_fabric_testing.js'))}}"></script>
@endsection
