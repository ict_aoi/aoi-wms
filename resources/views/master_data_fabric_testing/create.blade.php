@extends('layouts.app', ['active' => 'master_data_defect'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Data Fabric Testing</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data</li>
				<li><a href="{{ route('masterDataFabricTesting.index') }}" id="url_master_data_fabric_testing_index">Fabric Testing</a></li>
				<li class="active">Create</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<form action="{{ route('masterDataFabricTesting.save') }}" id="form-create" method="POST">
				{{ csrf_field() }}

				<div class="row">
					<div class="col-lg-6">
						{{-- <div class="col-md-12">
							<div class="row">
								<label class="display-block text-semibold">No TRF</label>
								<input type="text" name="trf_number" id="trf_number" class="form-control" required="" style="text-transform: uppercase;">
							</div>
						</div> --}}
						<div class="col-md-12">
							<div class="row">
								<label class="display-block text-semibold">Buyer</label>
								<select name="buyer" id="buyer" class="form-control select-search">
									<option>== Select Buyer ==</option>
									@foreach ($buyers as $by)
									<option value="{{ $by->buyer}}">{{ $by->buyer}} </option>
									@endforeach 
								</select>
								{{-- <input type="text" name="other_buyer" id="other_buyer" class="form-control" placeholder="Input buyer name "> --}}
							</div>
						</div>
						
						

						<div class="col-md-12">
							<div class="row">
								<label class="display-block text-semibold">Category Spesimen</label>
								<select name="category_specimen" id="category_specimen" class="form-control select-search">
									<option value="">== Select Category Specimen ==</option>
									
								</select>
								<input type="hidden" name="url_categoryspecimen" id="url_categoryspecimen" value ="{{ route('masterDataFabricTesting.getTypeSpecimen') }}" class="form-control" required="" style="text-transform: uppercase;">
								<input type="hidden" name="url_save" id="url_save" value ="{{ route('masterDataFabricTesting.save') }}" class="form-control" required="" style="text-transform: uppercase;">
								<input type="hidden" name="url_getpo" id="url_getpo" value ="{{ route('masterDataFabricTesting.getPoreference') }}" class="form-control" required="" style="text-transform: uppercase;">
								<input type="hidden" name="url_style" id="url_style" value ="{{ route('masterDataFabricTesting.getStyle') }}" class="form-control" required="" style="text-transform: uppercase;">
								<input type="hidden" name="url_get_requirement" id="url_get_requirement" value ="{{ route('masterDataFabricTesting.getRequirements') }}" class="form-control" required="" >
								<a href="{{ route('masterDataFabricTesting.getCategory') }}" id="url_typespecimen"></a>
							</div>
						</div>

						<!-- <div class="col-md-12">
							<div class="row">
								<label class="display-block text-semibold">Type Spesimen</label>
								<select name="type_specimen" id="type_specimen" class="form-control select-search">
									<option>== Select Type Specimen ==</option>
									
								</select>
								{{-- <input type="text" name="requirement_id" id="requirement_id" class="form-control" required="" style="text-transform: uppercase;"> --}}
							</div>
						</div> -->
						<div class="col-md-12">
							<div class="row">
								<label class="display-block text-semibold">Category</label>
								<select name="category" id="category" class="form-control select-search">
									<option>== Select Category ==</option>
									{{-- @foreach ($category as $cs)
									<option value="{{ $cs->category}}">{{ $cs->category}}</option>
									@endforeach --}}
								</select>
								{{-- <input type="text" name="requirement_id" id="requirement_id" class="form-control" required="" style="text-transform: uppercase;"> --}}
							</div>
						</div>

							
						<div class="col-lg-12">
							<div class="row">
								<label class="display-block text-semibold">Part yang akan di test</label>
								<div class="form-group">
									<textarea rows="3" cols="3" class="form-control elastic" placeholder="Part yang akan di test" id="part_akan_dites"></textarea>
								</div>
							</div>
						</div>

						
						<div class="col-md-12">
							<div class="row">
								<label class="display-block text-semibold">Return Test Sample</label>
								<select name="return_sample" id="return_sample" class="form-control select-search">
									<option>== Choose return Sample ==</option>
									<option value="true">Yes</option>
									<option value="false">No</option>
								</select>
							</div>
						</div>

					
					
						

						
						
					</div>
					<div class=" col-lg-6">
						<div class="col-md-12">
							<div class="row">
								<label class="display-block text-semibold">Lab Location</label>
								<select name="factory" id="factory" class="form-control select-search">
					
									<option value="AOI1">AOI 1</option>
									<option value="AOI2">AOI 2</option>
									{{-- <option value="BBIS">BBIS</option> --}}
								</select>
								{{-- <input type="text" name="requirement_id" id="requirement_id" class="form-control" required="" style="text-transform: uppercase;"> --}}
							</div>
						</div>

						<div class="col-md-12">
							<div class="row">
								<label class="display-block text-semibold">Test Required</label>
								
								
								<div class = "col-md-3" id="bulkTesting">
									<label class="display-block">1 Bulk Testing</label>
									<div class="radio">
										<label>
											<input type="radio" id="test_required" name="test_required" class="styled" value = "bulktesting_m">
											M (Model Level)
										</label>
									</div>

									<div class="radio">
										<label>
											<input type="radio" id="test_required" name="test_required" class="styled" value = "bulktesting_a">
											A (Article Level)
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" id="test_required" name="test_required" class="styled" value = "bulktesting_selective">
											Selective
										</label>
									</div>
								</div>

								<div class = "col-md-3" id="reorderTesting">
									<label class="display-block">Re-Order Testing</label>
									<div class="radio">
										<label>
											<input type="radio" id="test_required" name="test_required" class="styled" value = "reordertesting_m">
											M (Model Level)
										</label>
									</div>
	
									<div class="radio">
										<label>
											<input type="radio" id="test_required" name="test_required" class="styled" value = "reordertesting_a">
											A (Article Level)
										</label>
									</div>

									<div class="radio">
										<label>
											<input type="radio" id="test_required" name="test_required" class="styled" value = "reordertesting_selective">
											Selective
										</label>
									</div>
								</div>

								<div class = "col-md-3" id="reTest">
									<label class="display-block">Re-Test</label>
								<div class="radio">
									<label>
										<input type="radio" id="test_required" name="test_required" class="styled" checked="checked" value = "retest">
										Re-Test
									</label>
								</div>

								<div >
										<input type="text" name="no_trf" id="no_trf" class="form-control"  placeholder="Input previous no trf">
								</div>
								</div>
								
								{{-- <input type="text" name="requirement_id" id="requirement_id" class="form-control" required="" style="text-transform: uppercase;"> --}}
							</div>
						</div>

						<div class="col-md-12">
							<div class="row">
								<label class="display-block text-semibold">Date Information</label>
								
								<div class = "col-md-6">
			
									<div class="radio_date">
										<label>
											<input type="radio" name="radio_date" class="styled" id = "radio_date" value = "buy_ready" checked="checked">
											Buy ready (special for development testing)
										</label>
									</div>
								</div>
	
								<div class = "col-md-3">
			
									<div class="radio_date">
										<label>
											<input type="radio" name="radio_date" class="styled" id = "radio_date" value = "podd">
											PODD
										</label>
									</div>
								</div>
								<div class = "col-md-3">
			
									<div class="radio_date">
										<label>
											<input type="radio" name="radio_date" class="styled" id = "radio_date" value = "output_sewing">
											1st output sewing
										</label>
									</div>
								</div>
							

								
								<input type="date" name="testing_date" id="testing_date" class="form-control" required="" style="text-transform: uppercase;">
							</div>
						</div>

					
					
					
				
					</div>
				</div>

				<br><br>
				<div class="row">
					<div class="col-lg-6">
						<div class="col-md-12">
							<div class="row" id="row_barcode_fabric">
								<label class="display-block text-semibold">Barcode Fabric</label>
								<input type="text" name="barcode_fabric" id="barcode_fabric" class="form-control" style="text-transform: uppercase;">
								<input type="hidden" name="urlbarcode_fabric" id="urlbarcode_fabric" class="form-control" value = "{{ route('masterDataFabricTesting.getBarcodeFabric') }}" style="text-transform: uppercase;">
						
							</div>
						</div>
						<div class="col-md-12">
							<div class="row" id="row_document_no">
								<label class="display-block text-semibold">PO Supplier</label>
								<input type="text" name="document_no" id="document_no" class="form-control"  style="text-transform: uppercase;" readonly>
							</div>
						</div>
						<div class="col-md-12">
							<div class="row" id="row_supplier_name">
								<label class="display-block text-semibold">Supplier Name</label>
								<input type="text" name="supplier_name" id="supplier_name" class="form-control"  style="text-transform: uppercase;" readonly>
							</div>
						</div>
						<div class="col-md-12">
							<div class="row" id="row_item_code">
								<label class="display-block text-semibold">Item Code</label>
								<input type="text" name="item_code" id="item_code" class="form-control" style="text-transform: uppercase;" readonly>
							</div>
						</div>
						
						<div class="col-md-12">
							<div class="row" id="row_nomor_roll">
								<label class="display-block text-semibold">No Roll</label>
								<input type="text" name="nomor_roll" id="nomor_roll" class="form-control" style="text-transform: uppercase;" readonly>
							</div>
						</div>
						<div class="col-md-12">
							<div class="row" id="row_batch">
								<label class="display-block text-semibold">Batch</label>
								<input type="text" name="batch" id="batch" class="form-control"  style="text-transform: uppercase;" readonly>
							</div>
						</div>
						<div class="col-md-12">
							<div class="row" id="row_po_mo">
								<label class="display-block text-semibold">PO Buyer</label>
								<input type="text" name="po_mo" id="po_mo" class="form-control" style="text-transform: uppercase;">
							</div>
						</div>
						
						<div class="col-md-12">
							<div class="row"  id="row_item">
								<label class="display-block text-semibold">Item</label>
								<select name="item" id="item" class="form-control select-search">
									<option>== Select Item ==</option>
									</select>
								{{-- <input type="text" name="item" id="item" class="form-control" required="" style="text-transform: uppercase;"> --}}
							</div>
						</div> 

						<div class="col-md-12">
							<div class="row"  id="row_size">
								<label class="display-block text-semibold">Size</label>
									<select name="size" id="size" class="form-control select-search" >
									<option>== Select Size ==</option>
									</select>
								<input type="hidden" name="urlsize" id="urlsize" class="form-control" value = "{{ route('masterDataFabricTesting.getSize') }}" style="text-transform: uppercase;">
							</div>
						</div>

						<div class="col-md-12">
							<div class="row"  id="row_style">
								<label class="display-block text-semibold">Style</label>
								<input type="text" name="style" id="style" class="form-control" required="" style="text-transform: uppercase;" readonly>
							</div>
						</div>

						<div class="col-md-12">
							<div class="row" id="row_article">
								<label class="display-block text-semibold">Article</label>
								<input type="text" name="article" id="article" class="form-control" required="" style="text-transform: uppercase;" readonly>
							</div>
						</div>

						<div class="col-md-12">
							<div class="row"  id="row_color">
								<label class="display-block text-semibold">Color</label>
								<input type="text" name="color" id="color" class="form-control" required="" style="text-transform: uppercase;" readonly>
							</div>
						</div>
						<div class="col-md-12">
							<div class="row"  id="row_destination">
								<label class="display-block text-semibold">Destination</label>
								<input type="text" name="destination" id="destination" class="form-control" style="text-transform: uppercase;" readonly>
							</div>
						</div>
						
						<div class="col-md-12">
							<div class="row"  id="row_composition">
								<label class="display-block text-semibold">Composition</label>
								<input type="text" name="composition" id="composition" class="form-control"  readonly>
							</div>
						</div>
						
					</div>

				

					
				</div>
				<br><br>

				
			
			

				<div class="row">
					<div class="col-lg-12">
						<div class="col-md-12">
							<div class="row">
								<label class="display-block text-semibold">Remarks</label>
								<label>Reporting maximum of 4 days. Status received specimen & status finished specimen</label>
							</div>
						</div>

						
					</div>

				
				</div>

			
            
				<button type="submit" class="btn btn-blue-success col-xs-12" style="margin-top: 15px">Save <i class="icon-floppy-disk position-right"></i></button>
			</form>
		</div>
	</div>
@endsection

@section('page-js')
	<script src="{{ mix('js/master_data_fabric_testing.js') }}"></script>
@endsection
