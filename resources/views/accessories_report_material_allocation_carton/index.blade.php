@extends('layouts.app', ['active' => 'accessories_report_material_allocation_carton'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Material Allocation Carton</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li class="active">Material Allocation Carton</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
	<div class="panel-heading">
		<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
			</ul>
		</div>
	</div>
	<div class="panel-body">
	{!!
		Form::open(array(
			'class' => 'form-horizontal',
			'role' => 'form',
			'url' => route('accessoriesReportMaterialAllocationCarton.export'),
			'method' => 'get',
			'target' => '_blank'		
		))
	!!}
			<button type="submit" class="btn btn-default col-xs-12">Export All <i class="icon-file-excel position-left"></i></button>
	{!! Form::close() !!}
	</div>
</div>

<div class="panel panel-default border-grey">
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table datatable-basic table-striped table-hover table-responsive" id="report_material_allocation_carton_table">
				<thead>
					<tr>
						<th>Po Buyer</th>
						<th>Brand</th>
						<th>Destination</th>
						<th>Lc Date</th>
						<th>Item Code</th>
						<th>Style</th>
						<th>Article</th>
						<th>Qty Required</th>
						<th>Qty Allocation</th>
						<th>Qty Outstanding</th>
						<th>Season</th>
						<th>PSD</th>
						<th>Promise Date</th>
						<th>Batch Code</th>
						<th>Updated At</th>
						<th>Warehouse Packing</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
@endsection

@section('page-js')
	<script type="text/javascript" src="{{ asset(elixir('js/accessories_report_allocation_carton.js'))}}"></script>
@endsection
