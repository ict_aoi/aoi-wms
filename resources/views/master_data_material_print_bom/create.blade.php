@extends('layouts.app', ['active' => 'master_data_material_print_bom'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Data Material Print Bom</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data</li>
				<li><a href="{{ route('masterDataMaterialPrintBom.index') }}" id="url_master_data_material_print_bom_index">Material Print Bom</a></li>
				<li class="active">Create</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			{{ 
				Form::open([
				'method' 	=> 'POST',
				'id' 		=> 'form',
				'class' 	=> 'form-horizontal',
				'url' 		=> route('masterDataMaterialPrintBom.store')]) 
			}}
				
			@include('form.text', [
				'field' 		=> 'item_code',
				'label' 		=> 'Item Code',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'placeholder' 	=> 'Please type code here',
				'attributes' 	=> [
					'id' 			=> 'item_code',
					'autocomplete'	=> 'off'
				]
			])

			@include('form.checkbox', [
				'field' 		=> 'is_ila',
				'label' 		=> 'Is Ila',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'style_checkbox' => 'checkbox checkbox-switchery',
				'class' 		=> 'switchery',
				'attributes' 	=> [
					'id' 		=> 'checkbox_is_ila'
				]
			])

			@include('form.checkbox', [
				'field' 		=> 'is_from_buyer',
				'label' 		=> 'Is From Buyer',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'style_checkbox' => 'checkbox checkbox-switchery',
				'class' 		=> 'switchery',
				'attributes' 	=> [
					'id' 		=> 'checkbox_is_from_buyer'
				]
			])

			@include('form.checkbox', [
				'field' 		=> 'is_paxar',
				'label' 		=> 'Is Paxar',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'style_checkbox' => 'checkbox checkbox-switchery',
				'class' 		=> 'switchery',
				'attributes' 	=> [
					'id' 		=> 'checkbox_is_paxar'
				]
			])

			<button type="submit" class="btn btn-blue-success col-xs-12" style="margin-top: 15px">Save <i class="icon-floppy-disk position-right"></i></button>
		{{ Form::close() }}
		</div>
	</div>
	{!! Form::hidden('page','create', array('id' => 'page')) !!}
@endsection

@section('page-js')
	<script src="{{ mix('js/switch.js') }}"></script>
	<script src="{{ mix('js/master_data_material_print_bom.js') }}"></script>
@endsection
