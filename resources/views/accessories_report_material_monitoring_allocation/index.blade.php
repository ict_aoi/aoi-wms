@extends('layouts.app', ['active' => 'fabric_report_daily_material_out'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Material Monitoring Allocation</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li class="active">Material Monitoring Allocation</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
		{!!
				Form::open(array(
						'class' => 'heading-form',
						'role' => 'form',
						'url' => route('accessoriesReportMaterialMonitoringAllocation.export'),
						'method' => 'post',
						'target' => '_blank'		
				))
		!!}
			<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>
		<div class="panel-body">
			@include('form.select', [
				'field' 			=> 'warehouse',
				'label' 			=> 'Warehouse',
				'default' 			=> auth::user()->warehouse,
				'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
				'options' => [
					'' => '-- Select Warehouse --',
					'1000002' => 'Warehouse Acc AOI 1',
					'1000013' => 'Warehouse Acc AOI 2',
				],
				'class' => 'select-search',
				'attributes' => [
					'id' => 'select_warehouse'
				]
			])

			@include('form.date', [
				'field' 		=> 'start_date',
				'label' 		=> 'LC Date From (00:00)',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'default'		=> \Carbon\Carbon::now()->subDays(7)->format('d/m/Y'),
				'placeholder' 	=> 'dd/mm/yyyy',
				'class' 		=> 'daterange-single',
				'attributes'	=> [
					'id' 			=> 'start_date',
					'autocomplete' 	=> 'off',
					'readonly' 		=> 'readonly'
				]
			])
			
			@include('form.date', [
				'field' 		=> 'end_date',
				'label' 		=> 'LC Date To (23:59)',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'default'		=> \Carbon\Carbon::now()->addDays(30)->format('d/m/Y'),
				'placeholder' 	=> 'dd/mm/yyyy',
				'class' 		=> 'daterange-single',
				'attributes' 	=> [
					'id' 			=> 'end_date',
					'readonly' 		=> 'readonly',
					'autocomplete' 	=> 'off'
				]
			])

			<button type="submit" class="btn btn-default col-xs-12">Export All <i class="icon-file-excel position-left"></i></button>
		{!! Form::close() !!}
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover table-responsive" id="material_monitoring_allocation">
					<thead>
						<tr>
							<th>No</th>
							<th>LC Date</th>
							<th>Barcode</th>
							<th>Document No</th>
							<th>Supplier</th>
							<th>PO Buyer</th>
							<th>Item Code</th>
							<th>Category</th>
							<th>Style</th>
							<th>Article</th>
							<th>UOM</th>
							<th>Qty</th>
							<th>PIC</th>
							<th>Last Movement Status</th>
							<th>Last Movement Date</th>
							<th>Locator</th>
							<th>Warehouse</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>

	{!! Form::hidden('page','index' , array('id' => 'page')) !!}
	{!! Form::hidden('user_id',auth::user()->id , array('id' => 'user_id')) !!}
@endsection	

@section('page-modal')
    @include('accessories_report_material_monitoring_allocation._detail')
@endsection

@section('page-js')
	<script src="{{ mix('js/accessories_report_material_monitoring_allocation.js') }}"></script>
@endsection
