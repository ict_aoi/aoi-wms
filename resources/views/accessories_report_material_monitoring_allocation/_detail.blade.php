<div id="detailMaterialMonitoringModal" data-backdrop="static" data-keyboard="false" class="modal fade" style="color:black !important">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<h5 class="modal-title">Detail Material Monitoring</h5>
			</div>

			<div class="modal-body">
				<div class="table-responsive">
					<table class="table datatable-basic table-striped table-hover table-responsive" id="detail_material_monitoring">
						<thead>
							<tr>
								<th>Barcode</th>
								<th>PIC</th>
								<th>Status Awal</th>
								<th>Status Akhir</th>
								<th>From Locator</th>
								<th>To Locator</th>
								<th>Qty</th>
							</tr>
						</thead>
					</table>
				</div>
				
			</div>
			
			{!! Form::hidden('material_preparation_id',null, array('id' => 'material_preparation_id')) !!}
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="glyphicon glyphicon-remove"></i> </button>
			</div>
		</div>
	</div>
</div>
