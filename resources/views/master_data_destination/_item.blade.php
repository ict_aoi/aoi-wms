<script type="x-tmpl-mustache" id="destination-table">
	{% #item %}
		<tr>
			<td>{% no %}</td>
			<td>
				<div id="rack_{% _id %}" style="text-align:center;">{%rack%}</div>
				<input type="text" style="text-align:center;" class="form-control input-sm hidden input-item input-edit" id="rackInput_{% _id %}" value="{%rack%}" data-id="{% _id %}" data-row="">
			</td>
			<td>
				<div id="yrow_{% _id %}" style="text-align:center;">{% y_row %}</div>
				<input type="text" style="text-align:center;" class="form-control input-sm hidden input-item input-edit input-number" id="yrowInput_{% _id %}" value="{% y_row %}" data-id="{% _id %}" data-row="">
			</td>
			<td>
				<div id="zcolumn_{% _id %}" style="text-align:center;">{% z_column %}</div>
				<input type="text" style="text-align:center;" class="form-control input-sm hidden input-item input-edit input-number" id="zcolumnInput_{% _id %}" value="{%z_column%}" data-id="{% _id %}" data-row="">
			</td>
			<td>
				<button type="button" id="edit_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-edit-item"><i class="icon-pencil"></i></button>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-delete-item"><i class="fa icon-trash"></i></button>
				<button type="button" id="simpan_{% _id %}" data-id="{% _id %}" class="btn btn-yellow-cancel btn-icon-anim btn-circle btn-save-item hidden"><i class="icon-floppy-disk"></i></button>
				<button type="button" id="cancel_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-icon-anim btn-circle btn-cancel-item hidden"><i class="icon-x""></i></button>
			</td>
		</tr>
	{%/item%}
	<tr>
		<td>
			#
		</td>
		<td>
			<input type="text" id="rack" name="rack" class="form-control input-new" placeholder="Please type detial name"></input>
		</td>
		<td>
			<input type="text" id="y_row" name="y_row" class="form-control input-new" placeholder="Please type order number"></input>
		</td>
		<td>
			<input type="text" id="name_id" name="name_id" class="form-control input-new" placeholder="Please type erp code id"></input>
		</td>
		<td>
			<button type="button" class="btn btn-default btn-lg" id="AddItemButton" ><i class="icon-add"></i></button>
		</td>
		
	</tr>
</script>