<div class="modal fade" id="restoreReportStockModal" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="updateReportStockModal">
        <div class="modal-dialog" role="document">
		{{ 
			Form::open(['method' => 'POST'
			,'id' => 'restoreStockform'
			,'class' => 'form-horizontal'
			,'url' => route('accessoriesReportMaterialStock.storeRestoreStock')])
		}}
		
		    <div class="modal-content">
                <div class="modal-header bg-blue">
					<h5 class="modal-title">Restore stock for <span id="restore_header_stock"></span></h5>
				</div>
				<div class="modal-body">
					@include('form.text', [
						'label_col' 	=> 'col-xs-3',
						'form_col' 		=> 'col-xs-9',
						'field' 		=> 'restore_uom',
						'label' 		=> 'Uom',
						'placeholder' 	=> 'UOM',
						'attributes' 	=> [
							'id' 		=> 'restore_uom',
							'readonly' 	=> true
						]
					])
					@include('form.text', [
						'label_col' 	=> 'col-xs-3',
						'form_col' 		=> 'col-xs-9',
						'field' 		=> 'restore_available_qty_stock',
						'label'		 	=> 'Qty Restore',
						'placeholder' 	=> 'Please type qty restore',
						'attributes' => [
							'id' => 'restore_qty'
						]
					])

					@include('form.textarea', [
						'label_col' 	=> 'col-xs-3',
						'form_col' 		=> 'col-xs-9',
						'field' 		=> 'restore_reason',
						'label' 		=> 'Reason',
						'placeholder' 	=> 'Please insert you reason here',
						'attributes' 	=> [
							'id' => 'restore_reason',
							'rows' => '5',
							'cols' => '15'
						]
					])

					{!! Form::hidden('id','', array('id' => 'restore_stock_id')) !!}
				</div>
                <div class="modal-footer text-center">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close <span class="glyphicon glyphicon-remove"></span></button>
                	<button type="submit" class="btn btn-blue-success"> Save <i class="icon-floppy-disk position-left"></i></button>
				</div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>