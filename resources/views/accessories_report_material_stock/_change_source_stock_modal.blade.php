<div class="modal fade" id="changeSourceReportStockModal" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="updateReportStockModal">
        <div class="modal-dialog" role="document">
		{{ 
			Form::open([
				'method' => 'POST',
				'id' => 'changeSourceStockform',
				'class' => 'form-horizontal',
				'url' => route('accessoriesReportMaterialStock.storeChangeSourceStock')])
		}}
		
		    <div class="modal-content">
                <div class="modal-header bg-blue">
					<h5 class="modal-title">Change Source stock for <span id="change_source_stock_header_stock"></span></h5>
				</div>
				<div class="modal-body">
					@include('form.text', [
						'label_col' 	=> 'col-xs-3',
						'form_col' 		=> 'col-xs-9',
						'field' 		=> 'restore_uom',
						'label' 		=> 'Uom',
						'placeholder' 	=> 'UOM',
						'attributes' 	=> [
							'id' 		=> 'change_source_stock_uom',
							'readonly' 	=> true
						]
					])

					@include('form.text', [
						'label_col' 	=> 'col-xs-3',
						'form_col' 		=> 'col-xs-9',
						'field' 		=> 'change_source_stock_available',
						'label'		 	=> 'Qty Available',
						'attributes' => [
							'id' 		=> 'change_source_stock_available',
							'readonly' 	=> true
						]
					])

					@include('form.text', [
						'label_col' 	=> 'col-xs-3',
						'form_col' 		=> 'col-xs-9',
						'field' 		=> 'change_source_stock_actual',
						'label'		 	=> 'Qty Actual',
						'placeholder' 	=> 'Please type qty change',
						'attributes' => [
							'id' => 'change_source_stock_actual'
						]
					])

					@include('form.textarea', [
						'label_col' 	=> 'col-xs-3',
						'form_col' 		=> 'col-xs-9',
						'field' 		=> 'change_source_stock_reason',
						'label' 		=> 'Reason',
						'placeholder' 	=> 'Please insert you reason here',
						'attributes' 	=> [
							'id' 	=> 'change_source_stock_reason',
							'rows' 	=> '5',
							'cols' 	=> '15'
						]
					])

					{!! Form::hidden('id','', array('id' => 'change_source_stock_id')) !!}
				</div>
                <div class="modal-footer text-center">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close <span class="glyphicon glyphicon-remove"></span></button>
                	<button type="submit" class="btn btn-blue-success"> Save <i class="icon-floppy-disk position-left"></i></button>
				</div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>