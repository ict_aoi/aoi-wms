
<div id="moveLocatorModal" class="modal fade" data-backdrop="static" data-keyboard="false" style="color:black !important">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		{{ 
			Form::open(['method' => 'POST'
			,'id' => 'moveLocatorform'
			,'class' => 'form-horizontal'
			,'url' => route('accessoriesReportMaterialStock.storeMoveLocator')
			]) 
		}}
			<div class="modal-header bg-blue">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Move locator for <span id="header_stock"></span></h5>

			</div>

			<div class="modal-body">
				@include('form.picklist', [
					'label_col' 	=> 'col-lg-3 col-md-3 col-sm-12',
					'form_col' 		=> 'col-lg-9 col-md-9 col-sm-12',
					'field' 		=> 'locator',
					'label' 		=> 'Locator',
					'name' 			=> 'locator',
					'readonly' 		=> true,
					'placeholder' 	=> 'Please select locator',
					'attributes' 	=> [
						'id' 		=> 'locator',
						'readonly' 	=> true,
					]
				])
				
				@include('form.text', [
					'label_col' 	=> 'col-lg-3 col-md-3 col-sm-12',
					'form_col' 		=> 'col-lg-9 col-md-9 col-sm-12',
					'field' 		=> 'uom',
					'label' 		=> 'Uom',
					'placeholder' 	=> 'Uom',
					'attributes' 	=> [
						'id' 		=> 'uom',
						'readonly' 	=> 'readonly'
					]
				])

				@include('form.text', [
					'label_col' 	=> 'col-lg-3 col-md-3 col-sm-12',
					'form_col' 		=> 'col-lg-9 col-md-9 col-sm-12',
					'field' 		=> 'qty_available',
					'label' 		=> 'Qty available',
					'attributes' 	=> [
						'id' 		=> 'qty_available',
						'readonly' 	=> 'readonly'
					]
				])

				@include('form.text', [
					'label_col' 	=> 'col-lg-3 col-md-3 col-sm-12',
					'form_col' 		=> 'col-lg-9 col-md-9 col-sm-12',
					'field' 		=> 'qty_move',
					'label' 		=> 'Qty move',
					'placeholder' 	=> 'Please input qty move here',
					'attributes' 	=> [
						'id' 		=> 'qty_move',
					],
					'help' 			=> 'Qty move must be less than qty available'
				])
				
				{!! Form::hidden('old_locator_id','', array('id' => 'old_locator_id')) !!}
				{!! Form::hidden('id','', array('id' => 'id')) !!}
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close  <span class="glyphicon glyphicon-remove"></span></button>
				<button type="submit" class="btn btn-blue-success">Save <i class="icon-floppy-disk position-left"></i></button>
			</div>
		</div>
		{{ Form::close() }}
	</div>
</div>
