<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
			<th>No</th>
			<th>Remark</th>
			<th>Uom</th>
			<th>Qty</th>
			<th>Created At</th>
			<th>Created By</th>
	</tr>
	</thead>
	
	<tbody>
		@php $total = 0; @endphp
		@foreach ($detail_stock as $key => $item)
			<tr>
				<td>
					{{ $key+1 }}
				</td>
				<td>
					{{ strtoupper($item->remark) }}
				</td>
				<td>
					{{ $item->uom }}
				</td>
				
				<td>
					{{ number_format($item->qty, 4, '.', ',') }}
				</td>
				<td>
					{{ $item->created_at->format('d/M/Y H:i:s') }}
				</td>
				<td>
					{{ $item->user->name }}
				</td>
			</tr>
			@php $total += $item->qty; @endphp
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4"></td>
			<td><b>TOTAL</b></td>
			<td>{{ number_format($total, 4, '.', ',') }}</td>
		</tr>
	</tfoot>
</table>

