<div class="modal fade" id="updateReportStockModal" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="updateReportStockModal">
        <div class="modal-dialog" role="document">
		{{ 
			Form::open(['method' => 'POST'
			,'id' => 'updateStockform'
			,'class' => 'form-horizontal'
			,'url' => route('accessoriesReportMaterialStock.storeEditStock')])
		}}
		
		    <div class="modal-content">
                <div class="modal-header bg-blue">
					<h5 class="modal-title">Edit stock for <span id="update_header_stock"></span></h5>
				</div>
				<div class="modal-body">
					@include('form.text', [
						'label_col' => 'col-xs-3',
						'form_col' => 'col-xs-9',
						'field' => 'update_uom',
						'label' => 'Uom',
						'placeholder' => 'UOM',
						'attributes' => [
							'id' => 'update_uom',
							'readonly' => true
						]
					])
					@include('form.text', [
						'label_col' => 'col-xs-3',
						'form_col' => 'col-xs-9',
						'field' => 'new_available_qty_stock',
						'label' => 'New Available Qty',
						'placeholder' => 'Please type new available qty',
						'attributes' => [
							'id' => 'new_available_qty_stock'
						]
					])

					@include('form.textarea', [
						'label_col' 	=> 'col-xs-3',
						'form_col' 		=> 'col-xs-9',
						'field' 		=> 'update_reason',
						'label' 		=> 'Reason',
						'placeholder' 	=> 'Please insert you reason here',
						'attributes' 	=> [
							'id' 		=> 'update_reason',
							'rows' 		=> '5',
							'cols' 		=> '15',
							'style' 	=> 'resize:none'
						]
					])

					{!! Form::hidden('id','', array('id' => 'update_stock_id')) !!}
				</div>
                <div class="modal-footer text-center">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close <span class="glyphicon glyphicon-remove"></span></button>
                	<button type="submit" class="btn btn-blue-success"> Save <i class="icon-floppy-disk position-left"></i></button>
				</div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>