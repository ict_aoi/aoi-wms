<!DOCTYPE html>
<html>
<head>
    <title>BARCODE :: TRANSFER</title>
    <link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/printbarcodewebpo.css')) }}">

</head>
    <body>
			@foreach ($material_stock as $key => $value)
			<div class="outer">
						<div class="title">BARCODE TRANSFER</div>
						
						<div class="isi">
							<div class="isi1" style="padding-top:0;">
								<div class="isi3" style="height:31px;float:left;">
										NO CARTON : 1
								</div>
								<div class="isi2" style="height:31px;width:40.37%;display:block;">
								<span style="font-size: 12px">SEASON {{$value->getSeason($value->po_buyer)}}</span> 
								</div>
							</div>
							<div class="isi1">
								<span style="@if(strlen($value->item_code)>40) font-size: 12px @endif">{{ $value->item_code }}</span>
							</div>
							<div class="isi1">
								<span style="@if(strlen($value->item_desc)>40) font-size: 12px @endif">{{ $value->item_desc }}</span>
							</div>

							<div class="isi2">
								<div class="block-kiri" style="justify-content: center; height: 45px;">
									@php 
										$conversion = $value->conversion($value->item_code,$value->uom_conversion);
										if($conversion){
											$multiplyrate = $conversion->multiplyrate;
											$uom_reconversion = $conversion->uom_from;
										}else{
											$multiplyrate = 1;
											$uom_reconversion = $value->uom;
										}
										$qty_reconversion = $multiplyrate * $value->available_qty; 
									
									@endphp
									<span style="font-size: 12px; word-wrap: break-word;">{{ number_format($value->available_qty, 2, '.', ',') }} {{ strtoupper($value->uom) }} <br/> ({{ number_format($qty_reconversion, 2, '.', ',') }} {{ strtoupper($uom_reconversion) }})</span>
								</div>
								<!--<div class="block-kiri" style="height:35px;line-height: 35px">
									<span style="font-size: px; word-wrap: break-word;">PO BUYER: {{ $value->po_buyer }}  </span>
								</div-->
								<div class="block-kiri" style="height: 39px;">
									<span style="font-size: 10px; word-wrap: break-word;">
										{{ $value->document_no }}
										<br/>{{ $value->supplier_name }}	
									</span>
								</div>
								<div class="block-kiri" style="margin-top: 7px">
									<p style="font-size:9px">BARCODE INI SEBAGAI BUKTI SERAH TERIMA</p>
								</div>
							</div>
							<div class="isi3">
								<div class="block-kiri" style="height: 120px;">
										<p style="font-size:9px">BARCODE INI DITEMPELKAN KEBARANG YANG AKAN DI KIRIM, DAN TIDAK PERLU DI LAKUKAN PENERIMAAN DI WH TUJUAN.</p>
										<p style="font-size:9px">LANGUNG DIBERIKAN KE ADM STOCK, SUPAYA STOCK DAPAT DIAPPROVED.</p>
								</div>
								<div class="block-kiri" style="margin-top: 6px;" >
									PO BUYER: {{ $value->po_buyer }} 
								</div>
							</div>
						</div>		
					</div>
			@endforeach
		
    </body>
</html>