<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
			<th>No</th>
			<th>Created By</th>
			<th>Created Time</th>
			<th>Remark</th>
			<th>Uom</th>
			<th>Qty Booking</th>
	</tr>
	</thead>
	
	<tbody>
		@php $total = 0; @endphp
		@foreach ($non_allocation as $key => $item)
			<tr>
				<td>
					{{ $key+1 }}
				</td>
				<td>
					{{ strtoupper($item->user->name) }}
				</td>
				<td>
					{{ $item->created_at->format('d/M/Y H:i:s') }}
				</td>
				<td>
					{{ strtoupper($item->remark) }}
				</td>
				<td>
					{{ $item->uom }}
				</td>
				<td>
					{{ number_format($item->qty_booking, 2, '.', ',') }}
				</td>
			</tr>
			@php $total += $item->qty_booking; @endphp
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4"></td>
			<td><b>TOTAL</b></td>
			<td>{{ number_format($total, 4, '.', ',') }}</td>
		</tr>
	</tfoot>
</table>

