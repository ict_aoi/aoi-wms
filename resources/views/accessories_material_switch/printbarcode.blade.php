<!DOCTYPE html>
<html>
<head>
	<title>BARCODE :: ALLOCATION</title>
	<link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/printbarcodereceive.css')) }}">
</head>
    <body>
        @foreach ($allocation_items as $key => $allocation_item)
            <div class="outer">
                <div class="title">BARCODE SYSTEM - AOI</div>
                <div class="isi">
                    <div class="isi1">
                        <span style="word-wrap: break-word;@if(strlen($allocation_item->item_code)>40) font-size: 10px @endif">{{ $allocation_item->item_code }}</span>
                    </div>
                    <div class="isi1">
                        <span style="word-wrap: break-word;@if(strlen($allocation_item->itemInformation($allocation_item->item_code)->item_desc)>40) font-size: 10px @endif">{{ $allocation_item->itemInformation($allocation_item->item_code)->item_desc }}</span>
					</div>
                    <div class="isi1">
                        BOOK: {{ number_format($allocation_item->qty_booking, 2, '.', ',') }} {{ strtoupper($allocation_item->uom) }}         			
                    </div>

                    <div class="isi2">
                        <div class="block-kiri" style="height: 45px;">
                            <span style="font-size: 12px;word-wrap: break-word;">
                                {{ $allocation_item->job }} 
                            </span>
                        </div>
                        <div class="block-kiri" style="height: 45px;">
                            <span style="font-size: 12px;word-wrap: break-word;">
                                {{ $allocation_item->style }} 
                            </span>
                        </div>
                        <div class="block-kiri" style="">
                            <span style="font-size: 12px;word-wrap: break-word;">
                                {{ $allocation_item->no_po_supplier }} 
                            </span>
                        </div>
                    </div>
                    <div class="isi3">
                        <div class="block-kiri" style="height: 120px;">
                            <img style="height: 2.5cm; width: 4.3cm;	 margin-top: 10px; margin-left: 2px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("NEW.$allocation_item->barcode", 'C128') }}" alt="barcode"   />			
                        </div>
                        <div class="block-kiri" style="margin-top: 6px;" >
                            PO BUYER: <span style="font-size: 16px">
                            {{ $allocation_item->po_buyer }} 
                            </span>
                        </div>
                    </div>
                </div>		
            </div>

             @if (($key+1) % 3 === 0)
                <p style="page-break-after: always;">&nbsp;</p>
            @endif
        @endforeach
    </body>
</html>