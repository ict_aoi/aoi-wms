
<div id="approvalModal" class="modal fade" style="color:black !important">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Approve this data?</h5>
			</div>

			<div class="modal-body">
				<fieldset>
					<div class="form-group">
						<div class="col-xs-12">
							<div class="input-group">
								<input type="hidden" id="flagApproval"  class="form-control">
								<input type="text" id="approvalSearch"  class="form-control" placeholder="Input Keyword..." autofocus>
								<span class="input-group-btn">
									<button class="btn bg-teal" type="button" id="approvalButtonSrc">Search</button>
								</span>
							</div>
							<span class="help-block">Cari berdasarkan Kode Item/Po Buyer/Kode PO Supplier</span>
								
						</div>
					</div>
				</fieldset>
				</br>
				<div class="shade-screen" style="text-align: center;">
					<div class="shade-screen hidden">
						<img src="/img/ajax-loader.gif">
					</div>
				</div>
				<div class="table-responsive" id="approvalTable"></div>
			</div>

			{{ Form::open(['method' => 'POST', 'id' => 'approvalformnya', 'class' => 'form-horizontal', 'url' => "#"]) }}
				<div class="modal-footer">
					<button type="submit" class="btn btn-success">APPROVE ALL <i class="icon-checkmark position-left"></i></button>
					<button type="button" class="btn btn-warning" data-dismiss="modal">CLOSE  <span class="glyphicon glyphicon-remove"></span></button>
				</div>
			{{ Form::close() }}
		</div>
	</div>
</div>
