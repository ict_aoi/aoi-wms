<script type="x-tmpl-mustache" id="print-barcode-switch-accessories-table">
	{% #item %}
		<tr id="panel_{% _id %}">
			<td>{% nox %}</td>
			<td>{% document_no %}</td>
			<td>{% po_buyer %}</td>
			<td>{% item_code %} ({% category %})</td>
			<td>{% uom %}</td>
			<td>{% qty_switch %}</td>
			<td>{% note %}</td>
			<td>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-icon-anim btn-circle btn-delete-item"><i class="fa icon-trash"></i></button>
			</td>
		</tr>
	{%/item%}
	<tr>
	<td colspan="8">
		<input type="text" id="barcode_value" name="barcode_value" class="form-control input-new barcode_value" autocomplete="off" placeholder="SILAHKAN SCAN BARCODE DISINI"></input>
	</td>
		
	</tr>
</script>