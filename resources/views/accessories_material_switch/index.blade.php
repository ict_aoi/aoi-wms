@extends('layouts.app', ['active' => 'switch-allocation-accessories'])

@section('page-content')
	<a href="{{ route('accessoriesMaterialSwitch.index') }}" id="url_allocation_index" class="hidden"></a>
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">SWITCH ALLOCATION</span></h4>
						<ul class="breadcrumb breadcrumb-caret position-left">
							<li><a href="{{ route('dashboard') }}">Allocation</a></li>
							<li class="active">Switch</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini digunakan untuk menukar alokasi yg ada di wms.
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
				
			<div class="heading-elements">
				{!!
					Form::open(array(
							'class' => 'heading-form',
							'role' => 'form',
							'url' => route('accessoriesMaterialSwitch.index'),
							'method' => 'get',
							'id'=>'form-status'
							
						))
				!!}
					<div class="from-group text-right">
						@include('form.select', [
							'field' => 'status',
							'label' => '',
							'default'=>$status,
							'options' => [
								'' => 'PILIH STATUS',
								'MATERIAL BELUM DITUKAR' => 'MATERIAL BELUM DITUKAR',
								'MATERIAL SUDAH DITUKAR' => 'MATERIAL SUDAH DITUKAR',
								'MATERIAL SUDAH DI GUNAKAN UNTUK PO BUYER BARU'=>'MATERIAL SUDAH DI GUNAKAN UNTUK PO BUYER BARU',
								'MATERIAL SUDAH DI KEMBALIKAN UNTUK PO BUYER LAMA'=>'MATERIAL SUDAH DI KEMBALIKAN UNTUK PO BUYER LAMA',
							],
							'attributes' => [
								'id' => 'status'
							]
						])
					</div>
				{!! Form::close() !!}
				{!!
					Form::open(array(
							'class' => 'heading-form',
							'role' => 'form',
							'url' => route('accessoriesMaterialSwitch.export'),
							'method' => 'get',
							'target' => '_blank'
							
						))
				!!}
					<div class="from-group text-right">
						<button type="submit" class="btn btn-default">Export <i class="icon-file-excel position-left"></i></button>
					</div>
				{!! Form::close() !!}
				&nbsp;
				@if(Auth::user()->hasRole('admin-ict-acc') || Auth::user()->hasRole('mm-staff-acc'))
					<a href="{{ route('accessoriesMaterialSwitch.create') }}" class="btn btn-info">ADD NEW <i class="icon-add position-right"></i></a>
				@endif
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				{!! Form::hidden('allocations','[]',array('id' => 'allocations')) !!}
				<a href="{{ route('accessoriesMaterialSwitch.update') }}" id="url_switch_edit" class="hidden"></a>
				{!! $html->table(['class'=>'table datatable-basic','id' => 'switch-accessories-datatables']) !!}
			</div>
		</div>
	</div>
@endsection


@section('page-modal')
	@include('accessories_material_switch._update_modal')
@endsection

@section('page-js')
	{!! $html->scripts() !!}
	<script type="text/javascript" src="{{ asset(elixir('js/switch_allocation_accessories.js'))}}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
@endsection
