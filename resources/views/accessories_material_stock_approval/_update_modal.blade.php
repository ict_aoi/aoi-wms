<div class="modal fade" id="updateModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="updateModal">
        <div class="modal-dialog modal-lg" role="document">
		{{ Form::open(['method' => 'POST', 'id' => 'updateformnya', 'class' => 'form-horizontal', 'url' => route('accessoriesMaterialStockApproval.update')]) }}
		    <div class="modal-content">
                <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">EDIT APPROVAL STOCK</h5>
				</div>
				<div class="modal-body">
				   <div class="row">  
						<div class="col-md-6">
							@include('form.text', [
								'label_col' 	=> 'col-sm-12',
								'form_col' 		=> 'col-sm-12',
								'field' 		=> 'supplier_name',
								'label' 		=> 'SUPPLIER NAME',
								'placeholder' 	=> 'SUPPLIER NAME',
								'attributes' 	=> [
									'id' 		=> 'supplier_name',
									'readonly' 	=> true
								]
							])

							@include('form.text', [
								'label_col' 	=> 'col-sm-12',
								'form_col' 		=> 'col-sm-12',
								'field' 		=> 'document_no',
								'label' 		=> 'NO. PO SUPPLIER',
								'placeholder' 	=> 'NO. PO SUPPLIER',
								'attributes' 	=> [
									'id' 		=> 'document_no',
									'readonly' 	=> true
								]
							])

							@include('form.text', [
								'label_col' 	=> 'col-sm-12',
								'form_col' 		=> 'col-sm-12',
								'field' 		=> 'item_code',
								'label' 		=> 'ITEM CODE',
								'placeholder' 	=> 'ITEM CODE',
								'attributes' 	=> [
									'id' 		=> 'item_code',
									'readonly' 	=> true
								]
							])

							@include('form.text', [
								'label_col' 	=> 'col-sm-12',
								'form_col' 		=> 'col-sm-12',
								'field' 		=> 'uom',
								'label' 		=> 'UOM',
								'placeholder' 	=> 'UOM',
								'attributes' 	=> [
									'id' 		=> 'uom',
									'readonly' 	=> true
								]
							])

							@include('form.textarea', [
								'field' 		=> 'note',
								'label_col' 	=> 'col-sm-12',
								'form_col' 		=> 'col-sm-12',
								'label' 		=> 'Note Adjustment',
								'placeholder' 	=> 'Please insert remark name here',
								'attributes' 	=> [
									'id' 		=> 'note',
									'style' 	=> 'resize:none',
									'readonly' 	=> true,
									'rows' 		=> 2
								]
							])
						</div>
						<div class="col-md-6">
							@include('form.picklist', [
								'label_col' 	=> 'col-sm-12',
								'form_col' 		=> 'col-sm-12',
								'field' 		=> 'locator',
								'label' 		=> 'Locator',
								'name' 			=> 'locator',
								'readonly' 		=> true,
								'placeholder' 	=> 'Please select locator',
								'attributes' 	=> [
									'id' 		=> 'locator',
									'readonly' 	=> true,
								]
							])
							
							@include('form.text', [
								'label_col' 	=> 'col-sm-12',
								'form_col' 		=> 'col-sm-12',
								'field' 		=> 'qty_adjustment',
								'class' 		=> 'input-number',
								'label' 		=> 'Qty Adjustment',
								'placeholder' 	=> 'Qty Adjustment',
								'attributes' 	=> [
									'id' 		=> 'qty_adjustment'
									
								]
							])

							@include('form.textarea', [
								'field' 		=> 'remark_update',
								'label_col' 	=> 'col-sm-12',
								'form_col' 		=> 'col-sm-12',
								'label' 		=> 'Remark Update',
								'placeholder' 	=> 'Please insert remark update here',
								'attributes' 	=> [
									'id' 		=> 'remark_update',
									'style' 	=> 'resize:none',
									'rows' 		=> 2
								]
							])
						</div>
					</div>
				</div>
				{!! Form::hidden('table_id','', array('id' => 'table_id')) !!}
				{!! Form::hidden('table_name','', array('id' => 'table_name')) !!}
                <div class="modal-footer text-center">
					<button type="submit" class="btn btn-blue-success">Save  <i class="icon-floppy-disk position-left"></i></button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>