<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
			<th>Type Stock</th>
			<th>Supplier Name</th>
			<th>Po Supplier</th>
			<th>Locator</th>
			<th>Po Buyer Source</th>
			<th>Item</th>
			<th>Uom</th>
			<th>Available Qty</th>
			<th>Source</th>
			<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>{{ $list->type_stock }}</td>
				<td>{{ $list->supplier_name }}</td>
				<td>{{ $list->document_no }}</td>
				<td>
					@if($list->locator)
						{{ $list->locator->code }}
					@endif

				</td>
				<td>{{ $list->po_buyer }}</td>
				<td>{{ $list->item_code }} ({{ $list->category }})</td>
				<td>{{ $list->uom }}</td>
				<td>{{ number_format($list->available_qty, 4, '.', ',') }}</td>
				<td>{{ $list->source }}</td>
				<td>
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose" type="button"
						data-id="{{ $list->id }}" data-item="{{ $list->item_code }}" data-documentno="{{ $list->document_no }}" data-name="{{ $list->item_code }}#{{ $list->item_desc }}" data-category="{{ $list->category }}" 
						data-buyer="{{ $list->po_buyer }}" data-qty="{{ $list->available_qty }}" data-qtycurrency="{{ number_format($list->available_qty, 2, '.', ',') }}" data-uom="{{ $list->uom }}" data-typestock="{{ $list->type_stock }}"
					>
						Select
					</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
