<script type="x-tmpl-mustache" id="allocation-upload-table">
	{% #item %}
		<tr {% #is_error %} style="background-color:#fab1b1" {% /is_error %}>
			<td>{% no %}</td>
			<td>
				<p>Supplier Code <b>{% supplier_code %}</b></p>
				<p>Po. Supplier <b>{% document_no %}</b></p>
				<p>Po. Buyer Source <b>{% po_buyer_source %}</b></p>
			</td>
			<td>
				{% item_code %}
			</td>
			<td>
				{% po_buyer %}
			</td>
			<td>
				{% uom %}
			</td>
			<td>
				{% qty_booking %}
			</td>
			<td>
				{% style %}
			</td>
			<td>
				{% result %}
			</td>
			
		</tr>
	{%/item%}
</script>