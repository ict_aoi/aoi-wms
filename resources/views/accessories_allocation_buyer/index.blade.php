@extends('layouts.app', ['active' => 'accessories_allocation_buyer'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Allocation Buyer</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Allocation</li>
				<li class="active">Buyer</li>
			</ul>
			@role(['admin-ict-acc','supervisor','free-stock', 'mm-staff-acc'])
				<ul class="breadcrumb-elements">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
							<i class="icon-three-bars position-left"></i>
							Actions
							<span class="caret"></span>
						</a>
						
						<ul class="dropdown-menu dropdown-menu-right">
							<li><a href="{{ route('accessoriesAllocationBuyer.upload') }}"><i class="icon-file-excel pull-right"></i>Upload</a></li>
						</ul>
					</li>
				</ul>
			@endrole
		</div>
	</div>
@endsection

@section('page-content')
	<a href="{{ route('accessoriesAllocationBuyer.index') }}" id="url_allocation_index" class="hidden"></a>
	
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			@role(['admin-ict-acc'])
				@include('form.select', [
					'field' 			=> 'warehouse',
					'label' 			=> 'Warehouse',
					'default'			=> auth::user()->warehouse,
					'label_col' 		=> 'col-sm-12',
					'form_col' 			=> 'col-sm-12',
					'options' 			=> [
						'1000002' 		=> 'Warehouse Accessories AOI 1',
						'1000013' 		=> 'Warehouse Accessories AOI 2',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_warehouse'
					]
				])
			@else 
				@include('form.select', [
					'field' 			=> 'warehouse',
					'label' 			=> 'Warehouse',
					'default'			=> auth::user()->warehouse,
					'label_col' 		=> 'col-sm-12',
					'form_col' 			=> 'col-sm-12',
					'options' 			=> [
						'1000002' 		=> 'Warehouse Accessories AOI 1',
						'1000013' 		=> 'Warehouse Accessories AOI 2',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_export_warehouse'
					]
				])
				{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
			@endrole
			
			@role(['admin-ict-acc','supervisor','free-stock'])
				{!!
					Form::open([
						'role' 			=> 'form',
						'url' 			=> route('accessoriesAllocationBuyer.store'),
						'method' 		=> 'post',
						'enctype'		=> 'multipart/form-data',
						'class' 		=> 'form-horizontal',
						'id'			=> 'form'
					])
				!!}
					<div class="col-md-6">
						@include('form.picklist', [
							'label' 			=> 'Stock',
							'field' 			=> 'item_allocation',
							'name' 				=> 'item_allocation',
							'placeholder' 		=> 'Searching Stock',
							'form_col' 			=> 'col-xs-12',
							'mandatory' 		=> '*Required',
							'readonly' 			=> 'true',

						])

						<p id="type_stock" class="hidden"><b>Type Stock: </b> <span id="_type_stock"></span></p>
						<p id="document_no" class="hidden"><b>Po Supplier: </b> <span id="_document_no"></span></p>
						<p id="category" class="hidden"><b>Category: </b> <span id="_category"></span></p>
						<!--<p id="locator" class="hidden"><b>LOCATOR: </b> <span id="_locator"></span></p>-->
						<p id="uom" class="hidden"><b>Uom: </b> <span id="_uom"></span></p>
						<p id="qty_stock" class="hidden"><b>Available Qty: </b> <span id="_qty_stock"></span></p>
						{!! Form::hidden('__document_no','', array('id' => '__document_no')) !!}
						{!! Form::hidden('__uom','', array('id' => '__uom')) !!}
						{!! Form::hidden('__category','', array('id' => '__category')) !!}
						{!! Form::hidden('_temp_qty','', array('id' => '_temp_qty')) !!}
						{!! Form::hidden('locator_id','', array('id' => '___locator')) !!}
						{!! Form::hidden('item_code','', array('id' => '___item_code')) !!}
					</div>
					<div class="col-md-6">
						@include('form.picklist', [
							'label' 			=> 'Po Buyer',
							'field' 			=> 'po_buyer_allocation',
							'name' 				=> 'po_buyer_allocation',
							'placeholder' 		=> 'Please Select PO Buyer',
							'label_col' 		=> 'col-sm-12',
							'form_col' 			=> 'col-sm-12',
							'mandatory' 		=> '*Required'
						])

						<div class="row">
							<div class="col-xs-6">
								<p id="style" class="hidden"><b>Style: </b> <span id="_style"></span></p>
								<p id="job_order" class="hidden"><b>Article: </b> <span id="_job_order"></span></p>
								<p id="uom_po_buyer" class="hidden"><b>Uom: </b> <span id="_uom_po_buyer"></span></p>
								<p id="qty_booking" class="hidden"><b>Qty Booking: </b> {!! Form::text('qty_booking','', array('id' => '_qty_booking')) !!}</p>
								{!! Form::hidden('__qty_booking','', array('id' => '__qty_booking')) !!}
								{!! Form::hidden('__style','', array('id' => '__style')) !!}
								{!! Form::hidden('__article','', array('id' => '__article')) !!}
							</div>
							<div id="is_additional_div" class="col-xs-6 hidden">
								<!-- @include('form.checkbox', [
									'field' 		=> 'is_additional',
									'label' 		=> 'Is Additional',
									'label_col' 	=> 'col-md-4 col-lg-4 col-sm-12',
									'form_col' 		=> 'col-md-8 col-lg-8 col-sm-12',
									'style_checkbox' => 'checkbox checkbox-switchery',
									'class' 		=> 'switchery',
									'attributes' 	=> [
										'id' 		=> 'checkbox_is_additional'
									]
								])

								@include('form.textarea', [
									'field' 		=> 'remark_additional',
									'label_col' 	=> 'col-sm-12',
									'form_col' 		=> 'col-sm-12',
									'label' 		=> 'Remark Additional',
									'placeholder' 	=> 'Please insert remark name here',
									'attributes' 	=> [
										'id' 		=> 'remark_additional'
									]
								]) -->
								
								<span class="help-block">Please turn this on when allocation is additional</span>
								
							</div>
						</div>

					</div>
					{!! Form::hidden('warehouse_id','', array('id' => 'warehouse_id')) !!}
					<button type="submit" class="btn btn-blue-success col-xs-12" >Save <i class="icon-floppy-disk position-left"></i></button>
				{!! Form::close() !!}
			@endrole
			{!!
				Form::open(array(
						'class' 	=> 'heading-form',
						'role' 		=> 'form',
						'url' 		=> route('accessoriesAllocationBuyer.export'),
						'method' 	=> 'get',
						'target' 	=> '_blank'

					))
			!!}
				{!! Form::hidden('export_warehouse',auth::user()->warehouse , array('id' => 'export_warehouse')) !!}
				<button type="submit" class="btn btn-default col-xs-12">Export All<i class="icon-file-excel position-left"></i></button>
			{!! Form::close() !!}
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover " id="allocationTable">
					<thead>
						<tr>
							<th>id</th>
							<th>Created At</th>
							<th>Warehouse Allocation</th>
							<th>Username</th>
							<th>Type Stock Material</th>
							<th>Supplier Code</th>
							<th>Supplier Name</th>
							<th>Po. Supplier</th>
							<th>Item Code</th>
							<th>Category</th>
							<th>Type Stock Buyer</th>
							<th>New Po Buyer</th>
							<th>Old Po Buyer</th>
							<th>Uom</th>
							<th>Article</th>
							<th>Style</th>
							<th>Qty Allocated</th>
							<th>Status Barcode</th>
							<th>Is Additional</th>
							<th>Remark Additional</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('page-modal')
	@include('accessories_allocation_buyer.modal_picklist', [
		'name' => 'item_allocation',
		'name2' => 'document_no_allocation',
		'name3' => 'po_buyer_allocation',
		'title' => 'Filter Criteria',
	])

	@include('form.modal_picklist', [
		'name' 			=> 'po_buyer_allocation',
		'title' 		=> 'List Of PO Buyer',
		'placeholder' 	=> 'Cari Berdasarkan PO Buyer',
	])
@endsection

@section('page-js')
	<script src="{{ mix('js/switch.js') }}"></script>
	<script src="{{ mix('js/accessories_allocation_buyer.js') }}"></script>
@endsection
