@extends('layouts.app', ['active' => 'master_data_purchase_item'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Data Purchase Item</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data</li>
				<li class="active">Purchase Item</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>
		<div class="panel-body">
			@include('form.select', [
				'field' => 'warehouse',
				'label' => 'Warehouse Purchase',
				'default' => auth::user()->warehouse,
				'label_col' => 'col-md-2 col-lg-2 col-sm-12',
				'form_col' => 'col-md-10 col-lg-10 col-sm-12',
				'options' => [
					'' => '-- Select Warehouse --',
					'1000002' => 'Warehouse Accessories AOI 1',
					'1000013' => 'Warehouse Accessories AOI 2',
					'1000001' => 'Warehouse Fabric AOI 1',
					'1000011' => 'Warehouse Fabric AOI 2',
				],
				'class' => 'select-search',
				'attributes' => [
					'id' => 'select_warehouse'
				]
			])
			
			@include('form.select', [
				'field' => 'month_order',
				'label' => 'Month Purchase Order',
				'default' => \Carbon\Carbon::now()->format('m'),
				'label_col' => 'col-md-2 col-lg-2 col-sm-12',
				'form_col' => 'col-md-10 col-lg-10 col-sm-12',
				'options' => $months,
				'class' => 'select-search',
				'attributes' => [
					'id' => 'select_month'
				]
			])
			
			@include('form.select', [
				'field' => 'year_order',
				'label' => 'Year Purchase Order',
				'default' => \Carbon\Carbon::now()->format('Y'),
				'label_col' => 'col-md-2 col-lg-2 col-sm-12',
				'form_col' => 'col-md-10 col-lg-10 col-sm-12',
				'options' => $years,
				'class' => 'select-search',
				'attributes' => [
					'id' => 'select_year'
				]
			])
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover table-responsive" id="master_data_purchase_order_table">
					<thead>
						<tr>
							<th>Promise Date</th>
							<th>Status Po Buyer</th>
							<th>Warehouse Purchase</th>
							<th>Supplier Name</th>
							<th>Po Supplier</th>
							<th>Po Buyer</th>
							<th>Item Code</th>
							<th>Category</th>
							<th>Uom</th>
							<th>Qty Order</th>
							<th>Qty PR</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('page-js') 
	<script type="text/javascript" src="{{ asset(elixir('js/master_data_purchase_item.js'))}}"></script>
@endsection
