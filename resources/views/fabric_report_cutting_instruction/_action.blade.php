
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($detail))
                <li><a href="{!! $detail !!}"><i class="icon-search4"></i> Detail</a></li>
            @endif
            @if (isset($export))
                <li><a href="{!! $export !!}" target="_blank"><i class="icon-file-excel"></i> Export</a></li>
            @endif
        </ul>
    </li>
</ul>
