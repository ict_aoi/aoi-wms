@extends('layouts.app', ['active' => 'fabric_locator'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Locator</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Locator</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>

		<div class="panel-body">
			{!!
				Form::open(array(
					'class' 	=> 'horizontal-form',
					'role' 		=> 'form',
					'url' 		=> route('fabricLocator.index'),
					'method' 	=> 'get',
				))
			!!}
				
				@role(['admin-ict-fabric'])
					@include('form.select', [
						'field' 			=> 'warehouse',
						'label' 			=> 'Warehouse',
						'default'			=> auth::user()->warehouse,
						'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
						'options' 			=> [
							'1000001' 		=> 'Warehouse Fabric AOI 1',
							'1000011' 		=> 'Warehouse Fabric AOI 2',
						],
						'class' 			=> 'select-search',
						'attributes' 		=> [
							'id' 			=> 'select_warehouse'
						]
					])
				@else 
					{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
				@endrole
					
				@include('form.text', [
					'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
					'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
					'field' 		=> 'item_code',
					'label' 		=> 'Item Code',
					'placeholder' 	=> 'Please input item code here',
					'attributes' 	=> [
						'id' 		=> 'item_code'
					]
				])
			<button type="submit" class="btn btn-blue-success col-xs-12">Fitler<i class="icon-filter3 position-left"></i></button>
			{!! Form::close() !!}
		</div>
	</div>

	@foreach ($areas as $key => $area)
		<div class="col-xs-12">
			<h6 class="content-group text-semibold">
				{{ ($area->name == 'RELAX' ? 'RELAX' : $area->name ) }} AREA
			</h6>
			@foreach ($area->locators()->select('rack')->where('is_active',true)->groupby('rack')->orderby('rack')->get() as $key_rack => $rack)
				
				<div class="panel panel-default border-greyf">
					<div class="panel-heading">
						<h6 class="panel-title"> {{ ($rack->rack == 'RELAX' ? 'RELAX' : $rack->rack ) }} <a class="heading-elements-toggle"><i class="icon-more"></i></a><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
						<div class="heading-elements"> </div>
					</div>
					
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-hover table-responsive">
								<tbody>
									<tr>
										@php
											$baris = 0;
										@endphp
										@foreach ($area->getLocatorsAttribute($area->id,$rack->rack,false,$filtering_locator) as $key_locator => $locator)
											@if ($baris != $locator->y_row)
												</tr><tr>
											@endif

											<td style="{{ ( $locator->counter_in > 0 ? ($locator->po_buyer == 'RLX' ? 'background-color:#fffeba' : 'background-color:#d9ffde') : '') }};">
												<a class="btn-show-item" data-toggle="modal" data-target="#detilRackModal" data-header="{{ $key }}" data-rack="{{ $key_rack }}"  data-id="{{ $key_locator }}"  data-popup="tooltip" title="" data-placement="bottom">
													<input type="hidden" id="areaName_{{ $key }}_{{ $key_rack }}_{{ $key_locator }}" value="{{ $area->name }}" data-header="{{ $key }}" data-rack="{{ $key_rack }}" data-id="{{ $key_locator }}"></input>
													<input type="hidden" id="rack_{{ $key }}_{{ $key_rack }}_{{ $key_locator }}" value="{{ $rack->rack }}.{{ $locator->y_row }}.{{ $locator->z_column }}" data-header="{{ $key }}" data-rack="{{ $key_rack }}" data-id="{{ $key_locator }}"></input>
													<input type="hidden" id="locatorId_{{ $key }}_{{ $key_rack }}_{{ $key_locator }}" value="{{ $locator->id }}" data-header="{{ $key }}" data-rack="{{ $key_rack }}" data-id="{{ $key_locator }}"></input>
													<input type="hidden" id="counterIn_{{ $key }}_{{ $key_rack }}_{{ $key_locator }}" value="{{ $locator->counter_in }}" data-header="{{ $key }}" data-rack="{{ $key_rack }}" data-id="{{ $key_locator }}"></input>

													<span style="color:black">
														{{ ($locator->rack == 'RELAX' ? 'RELAX' : $locator->rack ) }}.{{ $locator->y_row }}.{{ $locator->z_column }}
													</span>
												</a>
											</td>

											@php
												$baris = $locator->y_row;
											@endphp
										@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			@endforeach

		</div>
	@endforeach
@endsection

@section('page-modal')
	@include('fabric_locator._detail_modal')
@endsection

@section('page-js')
	<script src="{{ mix('js/fabric_locator.js') }}"></script>
@endsection
