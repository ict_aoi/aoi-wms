<div id="detilModal" data-backdrop="static" data-keyboard="false" class="modal fade" style="color:black !important">
	<div class="modal-dialog modal-lg" style="width:75%" ">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<h5 class="modal-title">Detail Locator <span id="detail_header"></span></h5>
			</div>

			<div class="modal-body">
				<div class="tabbable tab-content-bordered" id=>
					<ul class="nav nav-tabs">
						<li class="active"><a href="#summary" data-toggle="tab" aria-expanded="false" onclick="changeTab('summary')">Summary</a></li>
						<li><a href="#detailPerPoSupplier" data-toggle="tab" aria-expanded="true" onclick="changeTab('detailPerPoSupplier')">Detail Per Po Supplier</a></li>
						<li><a href="#detail" data-toggle="tab" aria-expanded="true" onclick="changeTab('detail')">Detail</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="summary">
							<div class="table-responsive">
								<table class="table datatable-basic table-striped table-hover table-responsive" id="summary_table">
									<thead>
										<tr>
											<th>Upc Item</th>
											<th>Total Roll</th>
											<th>Total Yard(In Yds)</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
						<div class="tab-pane" id="detailPerPoSupplier">
							<div class="table-responsive">
								<table class="table datatable-basic table-striped table-hover table-responsive" id="detail_per_po_supplier_table">
									<thead>
										<tr>
											<th>Upc</th>
											<th>Po Supplier</th>
											<th>Item Code</th>
											<th>Item Desc</th>
											<th>Total Roll</th>
											<th>Total Yard (In Yard)</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
						<div class="tab-pane" id="detail">
							<div class="table-responsive">
								<table class="table datatable-basic table-striped table-hover table-responsive" id="detail_table">
									<thead>
										<tr>
											<th>Barcode</th>
											<th>Po Supplier</th>
											<th>Item Code</th>
											<th>Item Desc</th>
											<th>Batch Number</th>
											<th>No Roll</th>
											<th>Uom</th>
											<th>Actual Lot</th>
											<th>Qty On Barcode</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			
			{!! Form::hidden('locator_id',null, array('id' => 'locator_id')) !!}
			{!! Form::hidden('active_tab','summary', array('id' => 'active_tab')) !!}
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="glyphicon glyphicon-remove"></i> </button>
			</div>
		</div>
	</div>
</div>
