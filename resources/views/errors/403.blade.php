@extends('layouts.app')

@section('content')
	<div class="text-center content-group">
        <h1 class="error-title">403</h1>
        <h4>OOOPS, ANDA TIDAK DAPAT MENGAKSES MENU INI. FORBIDDEN AREA !!</h4>
        <span>Jika anda bersikeras mau membuka menu ini, silahkan hubungi ICT untuk diberikan akses ke menu ini, oke !? </span>
    </div>

    <div class="row">
        <div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
            <div class="row">
                <div class="col-xs-12">
                    <a href="{{ route('dashboard') }}" class="btn btn-primary btn-block content-group"><i class="icon-circle-left2 position-left"></i> Go to home</a>
                </div>
                </div>
        </div>
    </div>
@endsection




