<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>WMS</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/app.css')) }}">
        <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
       <link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/app.css')) }}">
	
    </head>
    <body>
        
    <!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Error title -->
				<div class="text-center content-group">
					<h1 class="error-title">404</h1>
                    <h5>HALAMAN YANG ANDA CARI TIDAK DITEMUKAN!</h5>
                    <span>Salah URL tuh, typo kali mas / mbanya.</span>
				</div>
				<!-- /error title -->


				<!-- Error content -->
				<div class="row">
					<div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
						<div class="row">
                            <div class="col-xs-12">
                                <a href="{{ route('dashboard') }}" class="btn btn-primary btn-block content-group"><i class="icon-circle-left2 position-left"></i> Go to home</a>
                            </div>
                         </div>
					</div>
				</div>
				<!-- /error wrapper -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
    </body>
</html>