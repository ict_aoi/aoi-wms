@extends('layouts.app')

@section('page-content')
	<div class="text-center content-group">
        <h1 class="error-title">MAINTENANCE</h1>
        <span>Maaf halaman ini sedang dalam perbaikan</span>
    </div>

    <div class="row">
        <div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
            <div class="row">
                <div class="col-xs-12">
                    <a href="{{ route('dashboard') }}" class="btn btn-primary btn-block content-group"><i class="icon-circle-left2 position-left"></i> Go to home</a>
                </div>
                </div>
        </div>
    </div>
@endsection




