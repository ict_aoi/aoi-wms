<div class="modal fade" id="insertModal" tabindex="-1" role="dialog" aria-labelledby="insertModalLabel">
        <div class="modal-dialog" role="document">
		{{ Form::open(['method' => 'POST', 'id' => 'insertformnya', 'class' => 'form-horizontal', 'url' => route('materialbacklog.store')]) }}
		    <div class="modal-content">
                <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">MATERIAL BACKLOG</h5>
				</div>
				<div class="modal-body">
				   <div class="row">
					   <div class="col-md-12">
               @include('form.text', [
               'field' => 'po_buyer',
               'label' => 'PO BUYER',
               'placeholder' => 'PO BUYER',
               'attributes' => [
               'id' => 'po_buyer'
               ]
               ])
						    @include('form.text', [
								'field' => 'item_code',
								'label' => 'ITEM CODE',
								'placeholder' => 'ITEM CODE',
								'attributes' => [
									'id' => 'item_code'
								]
							])
						</div>
					</div>
				</div>
                <div class="modal-footer text-center">
					<button type="submit" class="btn btn-success">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

			</div>
		{{ Form::close() }}
        </div>
	</div>
