@extends('layouts.app', ['active' => 'master-material-backlog'])

@section('content')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title" style="padding-top:0px;">
				<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">MATERIAL BACKLOG</span></h4>

				<ul class="breadcrumb breadcrumb-caret position-left">
					<li><a href="{{ route('dashboard') }}">Master Data</a></li>
					<li class="active">Material Backlog</li>
				</ul>
				<a href="{{ route('master.materialmoq.update') }}" id="url_materialbacklogupdate" class="hidden"></a>
				<a class="heading-elements-toggle"><i class="icon-more"></i></a>
			</div>

			<div class="heading-elements">
					<button type="button" class="btn btn-info" data-toggle="modal" data-target="#insertModal"><i class="icon-add position-left"></i> ADD NEW</button>
			</div>
		</div>
	</div>
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>

			<div class="heading-elements">
				<a class="btn btn-success btn-icon" href="{{ route('materialbacklog.import')}}">Import/Export <i class="icon-file-excel"></i></a>
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-basic']) !!}
			</div>
		</div>
@endsection

@section('content-modal')
	@include('material_backlog._insert_modal')
@endsection

@section('content-js')
	{!! $html->scripts() !!}
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
<script type="text/javascript" src="{{ asset(elixir('js/material_backlog.js'))}}"></script>
@endsection
