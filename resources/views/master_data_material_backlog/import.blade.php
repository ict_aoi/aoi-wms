@extends('layouts.app', ['active' => 'master-material-backlog'])

@section('content')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title" style="padding-top:0px;">
				<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">UPLOAD</span></h4>

				<ul class="breadcrumb breadcrumb-caret position-left">
					<li><a href="{{ route('materialbacklog.index') }}">Material Backlog</a></li>
					<li class="active">Upload </li>
				</ul>
				</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp </h6>
			<div class="heading-elements">
				<div class="heading-btn">
					<a class="btn btn-primary btn-icon" href="{{ route('materialbacklog.exportToExcel') }}" data-popup="tooltip" title="download form import" data-placement="bottom" data-original-title="download form import"><i class="icon-download"></i></a>
					<button type="button" class="btn btn-info btn-icon" id="upload_button" data-popup="tooltip" title="upload form import" data-placement="bottom" data-original-title="upload form import"><i class="icon-upload"></i></button>
				</div>
			</div>
		</div>

		<div class="panel-body">
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>NO</th>
              <th>PO BUYER</th>
              <th>ITEM CODE</th>
							<th>INSERT RESULT</th>
						</tr>
					</thead>
					<tbody id="tbody-upload">
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<hr>
	{!!
    Form::open([
        'role' => 'form',
        'url' => route('materialbacklog.importFromExcel'),
        'method' => 'post',
        'id' => 'upload_file_material_backlog',
        'enctype' => 'multipart/form-data'
    ])
	!!}
		<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
		<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
	{!! Form::close() !!}
@endsection

@section('content-js')
	<script type="text/javascript" src="{{ asset(elixir('js/material_backlog.js'))}}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
@endsection
