@extends('layouts.app', ['active' => 'master_data_role'])

@section('page-content')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title" style="padding-top:0px;">
				<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">CREATE ROLE</span></h4>

				<ul class="breadcrumb breadcrumb-caret position-left">
					<li><a href="{{ route('dashboard') }}">Master Data</a></li>
					<li><a href="{{ route('dashboard') }}">Authentication</a></li>
					<li><a href="{{ route('masterDataRole.index') }}" id="url_role">Role</a></li>
					<li class="active">Create</li>
				</ul>
				<a class="heading-elements-toggle"><i class="icon-more"></i></a>
			</div>
		</div>
	</div>

	{!!
		Form::open([
			'role' => 'form',
			'url' => route('masterDataRole.store'),
			'method' => 'post',
			'enctype' => 'multipart/form-data',
			'class' => 'form-horizontal',
			'id'=> 'form'
		])
	!!}
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-12">
			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-default border-grey">
						<div class="panel-heading">
							<h6 class="panel-title">ROLE FORM<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
							<div class="heading-elements">
								<ul class="icons-list">
									<li><a data-action="collapse"></a></li>
								</ul>
							</div>
						</div>

						<div class="panel-body">
							@include('form.text', [
								'field' => 'name',
								'label' => 'NAME',
								'label_col' => 'col-xs-12',
								'form_col' => 'col-xs-12',
								'placeholder' => 'Role Name',
								'attributes' => [
									'id' => 'name'
								]
							])
							
							@include('form.textarea', [
								'field' => 'description',
								'label' => 'DESCRIPTION',
								'label_col' => 'col-xs-12',
								'form_col' => 'col-xs-12',
								'placeholder' => 'Description',
								'attributes' => [
									'rows' => '3'
								]
							])
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-9 col-md-9 col-sm-12">
			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-default border-grey">
						<div class="panel-heading">
							<h6 class="panel-title">ROLE - PERMISSION <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
							<span class="label label-info heading-text">Mapping Role dengan Permission.</span>
								
							<div class="heading-elements">
								
								<ul class="icons-list">
									<li><a data-action="collapse"></a></li>
								</ul>
							</div>
						</div>

						<div class="panel-body">
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
											<th>NO</th>
											<th>NAME</th>
											<th>DESCRIPTION</th>
											<th>ACTION</th>
										</tr>
									</thead>
									<tbody id="tbody-permission">
									</tbody>
								</table>
								{!! Form::hidden('permissions', '[]', array('id' => 'permissions')) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<hr>
	<div class="form-group text-right" style="margin-top: 10px;">
		<button type="submit" class="btn btn-success btn-lg">SAVE <i class="icon-floppy-disk position-left"></i></button>
	</div>
	{!! Form::close() !!}
@endsection

@section('page-modal')
	@include('form.modal_picklist', [
		'name' => 'permission',
		'title' => 'List Of Permission',
		'placeholder' => 'Cari Berdasarkan Nama',
	])	
@endsection

@section('page-js')
	@include('master_data_role._permission')
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/master_data_role.js'))}}"></script>
@endsection