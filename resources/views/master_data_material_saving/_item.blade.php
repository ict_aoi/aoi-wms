<script type="x-tmpl-mustache" id="material-saving-table">
	{% #item %}
		<tr {% #is_error %} style="background-color:#fab1b1;" {% /is_error %}>
			<td>
				{% pcd %}
			</td>
			<td>
				{% po_supplier %}
			</td>
			<td>
				{% item_code %}
			</td>
			<td>
				{% style %}
			</td>
			<td>
				{% article %}
			</td>
			<td>
				{% uom %}
			</td>
			<td>
				{% qty_saving %}
			</td>
			<td>
				{% warehouse %}
			</td>
			<td>
				{% status %}
			</td>
		</tr>
	{%/item%}
</script>
