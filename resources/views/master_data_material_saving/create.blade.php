@extends('layouts.app', ['active' => 'master_data_material_saving'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Data Material Saving</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data</li>
				<li><a href="{{ route('masterDataMaterialSaving.index') }}">Material Saving</a></li>
				<li class="active">Create</li>
			</ul>
	</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<div class="heading-btn">
					<a href="{{route('masterDataMaterialSaving.exportFileUpload')}}" class="btn btn-primary"><i class="icon-download"></i></a>
					<button id="upload_button" class="btn btn-success"><i class="icon-upload"></i></button>
					{!!
						Form::open([
						'role' 		=> 'form',
						'url' 		=> route('masterDataMaterialSaving.importFileExcel'),
						'method' 	=> 'post',
						'id' 		=> 'upload_file_material_saving',
						'enctype' 	=> 'multipart/form-data'
					])
					!!}
						<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
						<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
					{!! Form::close() !!}
				</div>
			</div>
		</div>



		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover ">
					<thead>
						<tr>
							<td>Planning Date</td>
							<td>Po Supplier</td>
							<td>Item Code</td>
							<td>Style</td>
							<td>Article No</td>
							<td>Uom</td>
							<td>Qty Saving</td>
							<td>Warehouse Preparation</td>
							<td>Result</td>
						</tr>
					</thead>
					<tbody id="tbody-upload-material-saving"></tbody>
				</table>
			</div>
		</div>
	</div>

	{!! Form::hidden('material-saving','[]',array('id' => 'material-saving')) !!}
	{!! Form::hidden('page','create', array('id' => 'page')) !!}
@endsection

@section('page-js')
	@include('master_data_material_saving._item')
	<script src="{{ mix('js/master_data_material_saving.js') }}"></script>
@endsection
