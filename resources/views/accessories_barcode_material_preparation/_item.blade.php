<script type="x-tmpl-mustache" id="preparation-table">
	{% #item %}
		<tr id="panel_{% _id %}" data-id="{% _id %}">
			<td>
				{% no %}
				{% #show_selected %}
					<input type="checkbox" class="checkbox_item" id="check_{% _id %}" data-id="{% _id %}" {% #selected %} checked="checked" {% /selected %}>
				{% /show_selected %}
			</td>

			<td>{% document_no %}</td>
			<td>{% show_po_buyer %}</td>
			<td>{% item_code %}</td>
			<td>{% style %}</td>
			<td>{% article_no %}</td>
			<td>{% uom_conversion %}</td>
			<td>{% qty_need %}</td>
			<td>{% qty_conversion %}</td>
			<td>{% note %}</td>
		</tr>
	{%/item%}
</script>