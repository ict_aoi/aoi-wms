<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
			<th>Supplier Name</th>
			<th>Po. Supplier</th>
			<th>Item</th>
			<th>Prepared Status</th>
			<th>Action</th>
		</tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ $list->supplier_name }}
				</td>
				<td>
					{{ $list->document_no }}
				</td>
				<td>
					{{ $list->item_code }}
				</td>
				<td>
					{{ $list->prepared_status }}
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose" 
						type="button" data-partner="{{ $list->c_bpartner_id }}" data-document="{{ $list->document_no }}" data-item="{{ $list->item_code }}" data-status="{{ $list->prepared_status }}">Select</button>
				</td>
				
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
