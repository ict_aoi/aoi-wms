@extends('layouts.app', ['active' => 'fabric_material_in'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material In</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Material In</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	@role(['admin-ict-fabric'])
		<div class="panel panel-default border-grey">
			<div class="panel-body">
				@include('form.select', [
					'field' 			=> 'warehouse',
					'label' 			=> 'Warehouse',
					'default'			=> auth::user()->warehouse,
					'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
					'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
					'options' 			=> [
						'1000001' 		=> 'Warehouse Fabric AOI 1',
						'1000011' 		=> 'Warehouse Fabric AOI 2',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_warehouse'
					]
				])
			</div>
		</div>
	@else 
		{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
	@endrole

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover table-responsive">
					<thead>
						<tr>
							<th>#</th>
							<th>Scan</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody id="tbody_fabric_material_in">
					</tbody>
				</table>
			</div>

			{!!
				Form::open([
					'role' 		=> 'form',
					'url' 		=> route('fabricMaterialIn.store'),
					'method' 	=> 'post',
					'enctype'	=> 'multipart/form-data',
					'class' 	=> 'form-horizontal',
					'id'		=> 'form'
				])
			!!}
			
			{!! Form::hidden('last_locator_used',null , array('id' => 'last_locator_used')) !!}
			{!! Form::hidden('barcode_products','[]' , array('id' => 'barcode_products')) !!}
			{!! Form::hidden('url_fabric_material_in_create',route('fabricMaterialIn.create'), ['id' => 'url_fabric_material_in_create']) !!}
			{!! Form::hidden('url_temporary_reset',route('temporary.resetAll'), ['id' => 'url_temporary_reset']) !!}
			{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_id')) !!}
			
			<div class="alert alert-info alert-styled-right alert-bordered hidden" id="alert_locator">
				<span class="text-semibold"><center><p id="msg_locator"></p></center>
			</div>
			

			@include('form.picklist', [
				'field' 		=> 'locator_in_fabric',
				'name' 			=> 'locator_in_fabric',
				'label' 		=> 'Locator',
				'readonly' 		=> true,
				'placeholder' 	=> 'Please select locator',
				'title' 		=> 'Please select locator',
				'label_col' 	=> 'col-lg-3 col-md-3 col-sm-12',
				'form_col' 		=> 'col-lg-9 col-md-9 col-sm-12',
			])

			
			
			<div class="form-group text-right">
				<!--<button type="button" class="btn btn-yellow-cancel col-xs-6 btn-lg" id="reset_all_checkin" >Reset  <span class="glyphicon glyphicon-remove"></span></button-->
				<button type="submit" class="btn btn-blue-success col-xs-12 btn-lg">Save <i class="icon-floppy-disk position-left"></i></button>
			</div>
			
			{!! Form::close() !!}
			
		</div>
		
	</div>
@endsection

@section('page-modal')
	@include('form.modal_picklist', [
		'name' 			=> 'locator_in_fabric',
		'title' 		=> 'List Locator',
		'placeholder' 	=> 'Search based on name / Code',
	])
@endsection

@section('page-js')
	@include('fabric_material_in._item')
	<script src="{{ mix('js/fabric_material_in.js') }}"></script>
@endsection
