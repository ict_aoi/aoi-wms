<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
			<th>Area</th>
			<th>Location</th>
			<th>Status</th>
			<th>Action</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ $list->area->name }}
				</td>
				<td>
					{{ $list->rack }}.{{ $list->y_row }}.{{ $list->z_column }}
				</td>
				<td>
					@if($list->last_po_buyer == 'INV')
						Rak ini digunakan hanya untuk roll yg <b>BELUM</b> di <b>RELAX</b>.
					@elseif($list->last_po_buyer == 'RLX')
						Rak ini digunakan hanya untuk roll yg <b>SUDAH</b> di <b>RELAX</b>.
					@else
						Rak ini belum <b>BERTUAN</b>.
					@endif
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose" 
						type="button" 
						data-id="{{ $list->id }}" 
						data-lastused="{{ $list->last_po_buyer }}" 
						data-name="{{ $list->area->name }} - {{ $list->rack }}.{{ $list->y_row }}.{{ $list->z_column }}" >Select</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
