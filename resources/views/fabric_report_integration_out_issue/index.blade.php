@extends('layouts.app', ['active' => 'fabric_report_material_bapb'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Material Bapb</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li class="active">Material Bapb</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
	<div class="panel-heading">
		<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
			</ul>
		</div>
	</div>
	<div class="panel-body">
	{!!
		Form::open(array(
			'class' => 'form-horizontal',
			'role' => 'form',
			'url' => route('fabricReportMaterialBapb.export'),
			'method' => 'get',
			'target' => '_blank'		
		))
	!!}
		@include('form.select', [
			'field' 			=> 'warehouse',
			'label' 			=> 'Warehouse',
			'default' 			=> auth::user()->warehouse,
			'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
			'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
			'options' => [
				'' => '-- Select Warehouse --',
				'1000001' => 'Warehouse Fabric AOI 1',
				'1000011' => 'Warehouse Fabric AOI 2',
			],
			'class' => 'select-search',
			'attributes' => [
				'id' => 'select_warehouse'
			]
		])

		<button type="submit" class="btn btn-default col-xs-12">Export All <i class="icon-file-excel position-left"></i></button>
	{!! Form::close() !!}
	</div>
</div>

<div class="panel panel-default border-grey">
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table datatable-basic table-striped table-hover table-responsive" id="fabric_report_material_bapb_table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Bapb Date</th>
						<th>Bapb By</th>
						<th>Warehouse Location</th>
						<th>Po Supplier</th>
						<th>Supplier Code</th>
						<th>Supplier Name</th>
						<th>No Invoice</th>
						<th>No Roll</th>
						<th>Item Code</th>
						<th>Color</th>
						<th>Uom</th>
						<th>Bapb Qty</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>

{!! Form::hidden('user_id',auth::user()->id , array('id' => 'user_id')) !!}
@endsection

@section('page-js')
	<script src="{{ mix('js/fabric_report_material_bapb.js') }}"></script>
@endsection
