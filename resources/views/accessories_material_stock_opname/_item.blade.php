<script type="x-tmpl-mustache" id="accessories_material_stock_opname_table">
	{% #list %}
		<tr>
			<td>{% no %}</td>
			<td>{% locator %}</td>
			<td>{% statistical_date %}</td>
			<td>{% season %}</td>
			<td>{% po_buyer %}</td>
			<td>{% document_no %}</td>
			<td>{% item_code %}</td>
			<td>{% qty_conversion %} ({% uom_conversion %})</td>
		</tr>
	{%/list%}
	<tr>
		<td colspan="9">
			<input type="text" id="barcode_value" name="barcode_value" class="form-control input-new barcode_value" autocomplete="off" placeholder="Please scan your barcode here"></input>
		</td>
	</tr>
</script>