@extends('layouts.app', ['active' => 'fabric_print_surat_jalan'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Print Surat Jalan</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Barcode</li>
				<li class="active">Print Surat Jalan</li>
			</ul>           
		</div>
	</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
	<div class="panel-heading">
		<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
			</ul>
		</div>
	</div>
	<div class="panel-body">
		@include('form.select', [
			'field' => 'warehouse',
			'label' => 'Warehouse',
			'div_class'	=> 'hidden',
			'default' => auth::user()->warehouse,
			'label_col' => 'col-md-2 col-lg-2 col-sm-12',
			'form_col' => 'col-md-10 col-lg-10 col-sm-12',
			'options' => [
				'' => '-- Select Warehouse --',
				'1000001' => 'Warehouse Fabric AOI 1',
				'1000011' => 'Warehouse Fabric AOI 2',
			],
			'class' => 'select-search',
			'attributes' => [
				'id' => 'select_warehouse'
			]
		])
		
		@include('form.date', [
			'field' 		=> 'start_date',
			'label' 		=> 'Tanggal Kirim Awal',
			'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
			'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
			'default'		=> \Carbon\Carbon::now()->subDays(30)->format('d/m/Y'),
			'placeholder' 	=> 'dd/mm/yyyy',
			'class' 		=> 'daterange-single',
			'attributes'	=> [
				'id' 			=> 'start_date',
				'autocomplete' 	=> 'off',
				'readonly' 		=> 'readonly'
			]
		])
		
		@include('form.date', [
			'field' 		=> 'end_date',
			'label' 		=> 'Tanggal Kirim Akhir',
			'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
			'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
			'default'		=> \Carbon\Carbon::now()->format('d/m/Y'),
			'placeholder' 	=> 'dd/mm/yyyy',
			'class' 		=> 'daterange-single',
			'attributes' 	=> [
				'id' 			=> 'end_date',
				'readonly' 		=> 'readonly',
				'autocomplete' 	=> 'off'
			]
		])
	</div>
</div>

<div class="panel panel-default border-grey">
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table datatable-basic table-striped table-hover table-responsive" id="print_surat_jalan_table">
				<thead>
					<tr>
                        <th>Warehouse Location</th>
                        <th>Tanggal Kirim</th>
                        <th>No Surat Jalan</th>
                        <th>No PT</th>
                        <th>Total Handover</th>
                        <th>Action</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>

{!! Form::hidden('user_id',auth::user()->id , array('id' => 'user_id')) !!}
@endsection

@section('page-js')
	<script type="text/javascript" src="{{ asset(elixir('js/fabric_print_surat_jalan.js'))}}"></script>
@endsection
