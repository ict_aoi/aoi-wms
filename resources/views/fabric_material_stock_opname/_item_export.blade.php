<script type="x-tmpl-mustache" id="stock-opname-table">
	{% #item %}
		<tr>
			<td>
				{% po_supplier %}
			</td>
			<td>
				{% item_code %}
			</td>
			<td>
				{% uom %}
			</td>
			<td>
				{% available_qty %}
			</td>
			<td>
				{% actual_qty %}
			</td>
			<td>
				{% status %}
			</td>
		</tr>
	{%/item%}
</script>
