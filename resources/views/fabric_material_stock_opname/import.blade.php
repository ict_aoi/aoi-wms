@extends('layouts.app', ['active' => 'sto-fabric'])

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('report.inspectLab') }}"></a><span class="text-semibold">Material STO</span></h4>
						<ul class="breadcrumb breadcrumb-caret position-left">
							<li><a href="{{route('materialStockOpnameFabric.index')}}">Material STO</a></li>
							<li class="active">Import</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini digunakan oleh tim untuk melakukan stock opname untuk setiap material dan dapat digunakan juga untuk melihat isi dalam rak. 
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
			<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<div class="heading-btn">
						<a href="{{route('materialStockOpnameFabric.exportToExcel')}}" class="btn btn-primary" data-popup="tooltip" title="download form import" data-placement="bottom" data-original-title="download form import"><i class="icon-download"></i></a>
						<button id="upload_button" class="btn btn-success" data-popup="tooltip" title="upload form import" data-placement="bottom" data-original-title="upload form import"><i class="icon-upload"></i></button>
				</div>
			</div>
		</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<td>PO SUPPLIER</td>
								<td>ITEM CODE</td>
								<td>NO ROLL</td>
								<td>BATCH</td>
								<td>LOT ACTUAL</td>
								<td>UOM</td>
								<td>AVAILABLE QTY</td>
								<td>ACTUAL YARD</td>
								<td>RESULT</td>
							</tr>
						</thead>
						<tbody id="tbody-upload-stock-opname">

						</tbody>
					</table>
				</div>
			</div>
		</div>
		{!! Form::hidden('stock-opname','[]',array('id' => 'stock-opname')) !!}
		{!!
	    Form::open([
	        'role' => 'form',
	        'url' => route('materialStockOpnameFabric.importFromExcel'),
	        'method' => 'post',
	        'id' => 'upload_file_stock_opname',
	        'enctype' => 'multipart/form-data'
	    ])
		!!}
			<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
			<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
		{!! Form::close() !!}
@endsection
@include('material_sto_fabric._item_export')

@section('content-js')
	<script type="text/javascript">
	var url_loading = $('#loading_gif').attr('href');
	list_stock_opname = JSON.parse($('#stock-opname').val());

	function render() {
		$('#stock-opname').val(JSON.stringify(list_stock_opname));

		var tmpl = $('#stock-opname-table').html();
		Mustache.parse(tmpl);
		var data = { item: list_stock_opname };
		var html = Mustache.render(tmpl, data);
		$('#tbody-upload-stock-opname').html(html);
	}

	$('#upload_button').on('click', function () {
		$('#upload_file').trigger('click');
	});


	$('#upload_file').on('change', function () {
		$.ajax({
			type: "post",
			url: $('#upload_file_stock_opname').attr('action'),
			data: new FormData(document.getElementById("upload_file_stock_opname")),
			processData: false,
			contentType: false,
			beforeSend: function () {
				$.blockUI({
					message: "<img src='" + url_loading + "' />",
					css: {
						backgroundColor: 'transaparant',
						border: 'none',
					}
				});
			},
			complete: function () {
				$.unblockUI();
			},
			success: function (response) {
				$("#alert_info_2").trigger("click", 'upload berhasil, silahkan cek kembali.');

				for (idx in response) {
					var data = response[idx];
					var input = {
						'po_supplier': data.po_supplier,
						'item_code': data.item_code,
						'no_roll': data.no_roll,
						'batch': data.batch,
						'lot_actual': data.lot_actual,
						'uom': data.uom,
						'available_qty': data.available_qty,
						'actual_yard': data.actual_yard,
						'status': data.status,
					};
					list_stock_opname.push(input);
				}

			},
			error: function (response) {
				$.unblockUI();
				$('#upload_file_stock_opname').trigger('reset');
				if (response['status'] == 500)
					$("#alert_error").trigger("click", 'Please Contact ICT.');

				if (response['status'] == 422)
					$("#alert_error").trigger("click", response.responseJSON);

			}
		})
		.done(function () {
			$('#upload_file_stock_opname').trigger('reset');
			render();
		});

	})
	</script>
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
@endsection
