@extends('layouts.app', ['active' => 'fabric_material_preparation'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Preparation</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Material Preparation</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			@role(['admin-ict-fabric'])
				@include('form.select', [
					'field' 			=> 'warehouse',
					'label' 			=> 'Warehouse',
					'default'			=> auth::user()->warehouse,
					'label_col' 		=> 'col-md-3 col-lg-3 col-sm-12',
					'form_col' 			=> 'col-md-9 col-lg-9 col-sm-12',
					'options' 			=> [
						'1000001' 		=> 'Warehouse Fabric AOI 1',
						'1000011' 		=> 'Warehouse Fabric AOI 2',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_warehouse'
					]
				])
			
			@else 
				{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}

			@endrole

			<div class="row" style="overflow-y: auto; height:150px;">
				<div class="table-responsive">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th colspan="10"><center>Material Planning</center></th>
							</tr>
							<tr>
								<th>Planning Date</th>
								<th>Supplier Name</th>
								<th>Po Supplier</th>
								<th>Item Code</th>
								<th>Item Code Source</th>
								<th>Article No</th>
								<th>Qty Need Prepare</th>
								<th>Piping</th>
								<th>Additional</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="tbody-material-planning-fabric">
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th colspan="13"><center>Detail Roll</center></th>
						</tr>
						<tr>
							<th>#</th>
							<th>Roll Number</th>
							<th>Batch Number</th>
							<th>Actual lot</th>
							<th>Uom</th>
							<th>Top Width</th>
							<th>Middle Width</th>
							<th>Bottom Width</th>
							<th>Actual Width</th>
							<th>Actual Yds</th>
							<th>Available Qty</th>
							<th>Total Qty Booking</th>
							<th>Action</th>

						</tr>
					</thead>
					<tbody id="tbody-material-preparation-fabric">
					</tbody>
				</table>
			</div>

			{!!
				Form::open([
					'role' 		=> 'form',
					'url' 		=> route('fabricMaterialPreparation.store'),
					'method' 	=> 'post',
					'enctype' 	=> 'multipart/form-data',
					'class' 	=> 'form-horizontal',
					'id'		=> 'form'
				])
			!!}
			
			@include('form.picklist', [
				'field' 		=> 'locator_in_fabric',
				'name' 			=> 'locator_in_fabric',
				'label' 		=> 'Locator',
				'readonly' 		=> true,
				'placeholder' 	=> 'Please select locator',
				'title' 		=> 'Please select locator',
				'label_col' 	=> 'col-lg-3 col-md-3 col-sm-12',
				'form_col' 		=> 'col-lg-9 col-md-9 col-sm-12',
			])

			<div class="row">
			<div class="col-sm-12">
			@include('form.select', [
				'field' 		=> 'machine',
				'label' 		=> 'Machine',
				'label_col' 	=> 'col-lg-3 col-md-3 col-sm-12',
				'form_col' 		=> 'col-lg-9 col-md-9 col-sm-12',
				'options' 		=> [
					''			=>'Please Select Machine',
					'MANUAL1' 	=> 'Manual 1',
					'MANUAL2' 	=> 'Manual 2',
					'STEAM' 	=> 'Steam',
					'C-TEX'		=> 'C-TEX'
				],
				'class'      => 'select-search',
				'attributes' => [
					'id' 		=> 'select_machine'
				]
			])
				</div>
				</div>

			<div class="form-group text-right" style="margin-top: 10px;">
				{!! Form::hidden('url_remove_status_preparation', route('fabricMaterialPreparation.removeStatusPreparation'), array('id' => 'url_remove_status_preparation')) !!}
				{!! Form::hidden('url_remove_last_used', route('fabricMaterialPreparation.removeLastUser'), array('id' => 'url_remove_last_used')) !!}
				{!! Form::hidden('url_get_roll', route('fabricMaterialPreparation.getRoll'), array('id' => 'url_get_roll')) !!}
				{!! Form::hidden('url_get_planning_per_article', route('fabricMaterialPreparation.getPlanningPerArticle'), array('id' => 'url_get_planning_per_article')) !!}
				{!! Form::hidden('url_get_planning_per_style', route('fabricMaterialPreparation.getPlanningPerStyle'), array('id' => 'url_get_planning_per_style')) !!}
				{!! Form::hidden('url_get_planning_per_date', route('fabricMaterialPreparation.getPlanningPerDate'), array('id' => 'url_get_planning_per_date')) !!}
				{!! Form::hidden('active_tab', 'reguler' , array('id' => 'active_tab')) !!}

				{!! Form::hidden('url_get_planning_per_date_additional', route('fabricMaterialPreparation.getPlanningPerDateAdditional'), array('id' => 'url_get_planning_per_date_additional')) !!}
				{!! Form::hidden('url_get_planning_per_no_booking_additional', route('fabricMaterialPreparation.getPlanningPerNoBookingAdditional'), array('id' => 'url_get_planning_per_no_booking_additional')) !!}
				<!-- {!! Form::hidden('url_get_planning_per_date_additional', route('fabricMaterialPreparation.getPlanningPerDateAdditional'), array('id' => 'url_get_planning_per_date_additional')) !!} -->

				{!! Form::hidden('url_get_planning_per_article_balance_marker', route('fabricMaterialPreparation.getPlanningPerArticleBalanceMarker'), array('id' => 'url_get_planning_per_article_balance_marker')) !!}
				{!! Form::hidden('url_get_planning_per_style_balance_marker', route('fabricMaterialPreparation.getPlanningPerStyleBalanceMarker'), array('id' => 'url_get_planning_per_style_balance_marker')) !!}
				{!! Form::hidden('url_get_planning_per_date_balance_marker', route('fabricMaterialPreparation.getPlanningPerDateBalanceMarker'), array('id' => 'url_get_planning_per_date_balance_marker')) !!}

				{!! Form::hidden('url_get_planning_per_po_buyer', route('fabricMaterialPreparation.getPlanningPerPoBuyer'), array('id' => 'url_get_planning_per_po_buyer')) !!}
				{!! Form::hidden('data_planning', '[]' , array('id' => 'data_planning')) !!}
				{!! Form::hidden('data_prepare', '[]' , array('id' => 'data_prepare')) !!}
				{!! Form::hidden('warehouse_id', auth::user()->warehouse , array('id' => 'warehouse_id')) !!}
				{!! Form::hidden('machine', null , array('id' => 'machine')) !!}
				{!! Form::hidden('temporary_data_prepare', '[]' , array('id' => 'temporary_data_prepare')) !!}

				<button type="submit" class="btn btn-blue-success col-xs-12 btn-lg">Save <i class="icon-floppy-disk position-left"></i></button>
			</div>
			{!! Form::close() !!}	
		</div>
	</div>	

	{!!
		Form::open([
			'role' 		=> 'form',
			'url' 		=> route('fabricMaterialPreparation.printBarcode'),
			'method' 	=> 'get',
			'class' 	=> 'form-horizontal',
			'id'		=> 'getBarcode',
			'target' 	=> '_blank'
		])
	!!}
		{!! Form::hidden('list_barcodes','[]' , array('id' => 'list_barcodes')) !!}
		<div class="form-group text-right" style="margin-top: 10px;">
			<button type="submit" class="btn hidden">Print <i class="icon-printer position-left"></i></button>
		</div>
	{!! Form::close() !!}
@endsection

@section('page-modal')
	@include('fabric_material_preparation._confirmation_modal')
	@include('form.modal_picklist', [
		'name' 			=> 'locator_in_fabric',
		'title' 		=> 'List Locator',
		'placeholder' 	=> 'Search based on name / Code',
	])
@endsection

@section('page-js')
	@include('fabric_material_preparation._item_planning')
	@include('fabric_material_preparation._item_preparation')
	<script src="{{ mix('js/fabric_material_preparation.js') }}"></script>
@endsection
