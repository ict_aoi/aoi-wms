@extends('layouts.app', ['active' => 'monitor-handover'])

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">MONITORING HANDOVER</span></h4>

						<ul class="breadcrumb breadcrumb-caret position-left">
							<li>Report</li>
							<li class="active">Monitoring Handover</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini digunakan untuk memonitor perpindahan tangan dari AOI 1 KE AOI 2 atau sebaliknya.
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				{!!
					Form::open(array(
						'class' => 'heading-form',
						'role' => 'form',
						'id'=>'form_filter',
						'url' => route('report.monitoringHandover'),
						'method' => 'get',
					))
				!!}
				<div class="form-group">
					@include('form.date', [
						'field' => 'start_date',
						'label' => 'FROM (00:00)',
						'class' => ' datepicker',
						'default'=> $start,
						'placeholder' => 'dd/mm/yyyy',
						'attributes' => [
							'id' => 'start_date',
							'autocomplete' => 'off'
						]
					])
				</div>
				<div class="form-group">
					@include('form.date', [
						'field' => 'end_date',
						'label' => 'TO (23:59)',
						'class' => ' datepicker',
						'default'=> $end,
						'placeholder' => 'dd/mm/yyyy',
						'attributes' => [
							'id' => 'end_date',
							'autocomplete' => 'off'
						]
					])
				</div>

				<div class="heading-btn">
					{{-- <button type="submit" class="btn btn-default">FILTER</button> --}}
				</div>
			{!! Form::close() !!}
			</div>
		</div>

		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-basic','id'=> 'handover-datatable']) !!}
			</div>
		</div>
	</div>

@endsection

@section('content-js')
	{!! $html->scripts() !!}
	<script type="text/javascript" src="{{ asset(elixir('js/monitoring_handover.js'))}}"></script>
@endsection
