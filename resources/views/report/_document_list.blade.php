<table class="table">
	<thead>
	  <tr>
			<th>NO. PO SUPPLIER</th>
			<th>ACTION</th>
		</tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>{{ $list->documentno  }}</td>
				<td>
					<button class="btn btn-info btn-xs btn-choose" type="button"
						data-dismiss="modal" data-invoice="{{ $list->documentno }}">Select
					</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
