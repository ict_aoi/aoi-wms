@extends('layouts.app', ['active' => 'report_other_material-allocation'])

@section('content')
	<a href="{{ route('report.materialstock') }}" id="url_material_stock_index" class="hidden"></a>

	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title" style="padding-top:0px">
				<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">DELETE BULK STOCK</span></h4>

				<ul class="breadcrumb breadcrumb-caret position-left">
					<li><a href="{{ route('dashboard') }}">Report</a></li>
					<li><a href="{{ route('report.materialstock') }}"> Material Stock </a></li>
					<li>Active</li>
					<li class="active">Delete Bulk</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a class="btn btn-primary btn-icon" href="{{ route('report.materialStockExportFormDeleteInput')}}" data-popup="tooltip" title="download form delete" data-placement="bottom" data-original-title="download form delete"><i class="icon-download"></i></a>
				<button type="button" class="btn btn-success btn-icon" id="upload_button" data-popup="tooltip" title="upload form delete" data-placement="bottom" data-original-title="upload form delete"><i class="icon-upload"></i></button>

				{!!
					Form::open([
						'role' => 'form',
						'url' => route('report.materialStockstoreDeleteBulk'),
						'method' => 'POST',
						'id' => 'upload_file_allocation',
						'enctype' => 'multipart/form-data'
					])
				!!}
					<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
					<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
				{!! Form::close() !!}

			</div>
		</div>

		<div class="panel-body">
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>NO</th>
							<th>SUPPLIER NAME</th>
							<th>NO. PO SUPPLIER</th>
							<th>KODE ITEM</th>
							<th>QTY</th>
							<th>REASON</th>
							<th>WAREHOUSE INVENTORY</th>
							<th>UPLOAD RESULT</th>
						</tr>
					</thead>
					<tbody id="tbody-delete-bulk">
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<hr>
	<div class="form-group text-right" style="margin-top: 10px;">
		{!! Form::hidden('delete_bulk_items','[]' , array('id' => 'delete_bulk_items')) !!}
		<a href="{{ route('report.materialstock') }}" class="btn btn-info">BACK <i class="icon-arrow-left13 position-left"></i></a>
	</div>
@endsection

@section('content-js')
	@include('report._material_stock_delete_item')
	<script type="text/javascript" src="{{ asset(elixir('js/delete_bulk_material_stock.js'))}}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
@endsection
