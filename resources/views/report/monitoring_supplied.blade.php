@extends('layouts.app', ['active' => 'monitor-checkout'])

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">MONITORING CHECKOUT</span></h4>

						<ul class="breadcrumb breadcrumb-caret position-left">
							<li>Report</li>
							<li class="active">Monitoring Checkout</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini digunakan untuk memonitor item yang keluar.
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				{!!
					Form::open(array(
						'class' => 'heading-form',
						'role' => 'form',
						'id'=>'form_filter',
						'url' => route('report.monitoringSupplied'),
						'method' => 'get',
					))
				!!}
				<div class="form-group">
					@include('form.date', [
						'field' => 'start_date',
						'label' => 'FROM (00:00)',
						'class' => ' datepicker',
						'default'=> $start,
						'placeholder' => 'dd/mm/yyyy',
						'attributes' => [
							'id' => 'start_date',
							'autocomplete' => 'off'
						]
					])
				</div>
				<div class="form-group">
					@include('form.date', [
						'field' => 'end_date',
						'label' => 'TO (23:59)',
						'class' => ' datepicker',
						'default'=> $end,
						'placeholder' => 'dd/mm/yyyy',
						'attributes' => [
							'id' => 'end_date',
							'autocomplete' => 'off'
						]
					])
				</div>

				<div class="heading-btn">
					{{-- <button type="submit" class="btn btn-default">FILTER</button> --}}
				</div>
			{!! Form::close() !!}
			</div>
		</div>

		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-basic','id'=> 'handover-datatable']) !!}
			</div>
		</div>
	</div>

@endsection

@section('content-js')
	{!! $html->scripts() !!}
    <script>
    $('#start_date').on('change',submitFilter);
    $('#end_date').on('change',submitFilter);

    function submitFilter(){
        $('#form_filter').submit();
    }
    $(function(){
        var dtable = $('#handover-datatable').dataTable().api();
        dtable.page.len(100);
        dtable.draw();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
    })
    </script>
@endsection
