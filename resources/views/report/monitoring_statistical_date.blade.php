@extends('layouts.app', ['active' => 'monitor-statistical-date'])

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">MONITORING STATISTICAL DATE</span></h4>
						
						<ul class="breadcrumb breadcrumb-caret position-left">
							<li class="active"><a href="{{ route('dashboard') }}">Monitoring Statistical Date</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini untuk memonitor statistical date per po buyer untuk setiap invoice.
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="panel panel-default border-grey">
			<div class="panel-heading">
				<h6 class="panel-title">FILTER CRITERIA<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
					</ul>
				</div>
			</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-xs-6">
						@include('form.picklist', [
							'field' => 'no_invoice',
							'name' => 'no_invoice',
							'placeholder' => 'Pilih Nomor Invoice',
							'title' => 'Pilih Nomor Invoice'
						])
					</div>
					<div class="col-xs-6">
						@include('form.picklist', [
							'field' => 'document_no',
							'name' => 'document_no',
							'placeholder' => 'Pilih Nomor PO Supplier',
							'title' => 'Pilih Nomor PO Supplier'
						])
					</div>
				</div>
				<br/>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="panel panel-default border-grey">
			<div class="panel-heading">
				<h6 class="panel-title"> &NonBreakingSpace; <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
				<!--<span class="label label-info heading-text">Quantity sisa akan di masukan ke dalam Free Stock.</span>-->
					
				<div class="heading-elements">
					<div class="heading-btn">
						{!!
							Form::open([
								'role' => 'form',
								'url' => route('report.exportBuyer'),
								'method' => 'post',
								'enctype' => 'multipart/form-data',
								'class' => 'form-horizontal'
							])
						!!}
							<button type="submit" id="btn_export" class="btn btn-default"> Excel<i class="icon-file-excel position-left"></i></button>
							{!! Form::hidden('material_statistical_dates','[]' , array('id' => 'material_statistical_dates')) !!}
						{!! Form::close() !!}	
					</div>
				</div>
			</div>

			<div class="panel-body">
				<a href="{{ route('report.buyerOnInvoice') }}" id="url_get_buyer" class="hidden"></a>
							
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>PO SUPPLIER</th>
								<th>ITEM</th>
								<th>PO BUYER</th>
								<th>STATISTICAL DATE</th>
							</tr>
						</thead>
						<tbody id="tbody-monitoring-statistical-date">
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
@endsection

@section('content-modal')
	@include('form.modal_picklist', [
		'name' => 'no_invoice',
		'title' => 'List No Invoice',
		'placeholder' => 'Silahkan cari berdasarkan No Invoice'
	])	

	@include('form.modal_picklist', [
		'name' => 'document_no',
		'title' => 'List PO Supplier',
		'placeholder' => 'Silahkan cari berdasarkan PO Supplier'
	])
@endsection

@section('content-js')
	@include('report._monitoring_statistical_date')
	<script type="text/javascript" src="{{ asset(elixir('js/monitoring_statistical_date.js'))}}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
@endsection
