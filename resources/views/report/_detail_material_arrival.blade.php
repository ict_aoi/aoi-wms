<table class="table">
	<thead>
	  <tr>
			<th>NO</th>
			<th>BARCODE SUPPLIER</th>
			<th>RECEIVE DATE</th>
			<th>RECEIVE USER</th>
	</tr>
	</thead>
	
	<tbody>
		@foreach ($items as $key => $item)
			<tr>
				<td>
					{{ $key+1 }}
				</td>
				<td>
					{{ $item->barcode_supplier }}
				</td>
				<td>
						{{ $item->created_at->format('d/m/Y H:m:s') }}
				</td>
				<td>
					{{ $item->user->name }}
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

