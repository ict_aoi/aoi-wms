@extends('layouts.app', ['active' => 'mrp'])

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">MRP</span></h4>

						<ul class="breadcrumb breadcrumb-caret position-left">
							<li>Report</li>
							<li class="active">MRP</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini digunakan untuk melihat laporan item yang dibutuhkan dari suatu po buyer.
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
				
			<div class="heading-elements">
					{!!
						Form::open(array(
							'class' => 'heading-form',
							'role' => 'form',
							'url' => route('report.mrp'),
							'method' => 'get',
						))
					!!}
					<div class="form-group">
						<input type="text" value="{{ $po_buyer }}" class="form-control" name="po_buyer" id="po_buyer" placeholder="INPUT PO BUYER">
					</div>

					
					<div class="heading-btn">
						<button type="submit" class="btn btn-default">FILTER</button>
					</div>
				{!! Form::close() !!}
				{!!
					Form::open(array(
							'class' => 'heading-form',
							'role' => 'form',
							'url' => route('report.exportMrpReport'),
							'method' => 'get',
							#'id' => 'form'
						))
				!!}
					{!! Form::hidden('_po_buyer',$po_buyer, array('id' => '_po_buyer')) !!}
					<div class="from-group text-right">
						<button type="submit" class="btn btn-success" id="exportExcel">Export <i class="icon-file-excel position-left"></i></button>
					</div>
				{!! Form::close() !!}
				{!!
					Form::open(array(
							'class' => 'heading-form',
							'role' => 'form',
							'url' => route('report.printMrpReport'),
							'method' => 'get',
							'target' => '_blank'
							//'id' => 'form'
						))
				!!}
					{!! Form::hidden('_print_po_buyer',$po_buyer, array('id' => '_print_po_buyer')) !!}
					<div class="from-group text-right">
						<button type="submit" class="btn btn-info" id="print">Print <i class="icon-printer position-left"></i></button>
					</div>
				{!! Form::close() !!}
				{!!
					Form::open(array(
							'class' => 'heading-form',
							'role' => 'form',
							'url' => route('report.exportAllMrpReport'),
							'method' => 'get',
							#'id' => 'form2'
						))
				!!}
					<div class="from-group text-right">
						<button type="submit" class="btn btn-warning">Download All <i class="icon-file-download2 position-left"></i></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<a href="{{ route('report.mrpClosing') }}" id="url_close_mrp" class="hidden"></a>
				{!! $html->table(['class'=>'table datatable-basic','id'=> 'mrp-datatable']) !!}
			</div>
		</div>
	</div>

@endsection

@section('content-modal')
	@include('report._closing_mrp_modal')
@endsection

@section('content-js')
	{!! $html->scripts() !!}
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/monitoring_mrp.js'))}}"></script>
@endsection
