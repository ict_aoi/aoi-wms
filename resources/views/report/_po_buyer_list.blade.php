<table class="table">
	<thead>
	  <tr>
			<th>NO. PO BUYER</th>
			<th>ACTION</th>
		</tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>{{ $list->po_buyer  }}</td>
				<td>
					<button class="btn btn-info btn-xs btn-choose" type="button"
						data-dismiss="modal" data-po-buyer="{{ $list->po_buyer }}">Select
					</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
