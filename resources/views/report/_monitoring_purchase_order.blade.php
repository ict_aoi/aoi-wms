<script type="x-tmpl-mustache" id="monitoring-purchase-order-table">
	{% #item %}
		<tr>
			<td>{% document_no %}</td>
			<td>{% item_code %}</td>
			<td>{% po_buyer %}</td>
			<td>{% warehouse_name %}</td>
			<td>{% category %}</td>
			<td>{% uom %}</td>
			<td>{% qty %}</td>
		</tr>
	{%/item%}
</script>