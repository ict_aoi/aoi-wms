$(function () {
    $('#select_warehouse').trigger('change');
    $('#accessories_report_material_stock_opname_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength: 100,
        scroller: true,
        deferRender: true,
        ajax: {
            type: 'GET',
            url: '/accessories/report/material-stock-opname/data',
            data: function (d) {
                return $.extend({}, d, {
                    "warehouse": $('#select_warehouse').val(),
                    "start_date": $('#start_date').val(),
                    "end_date": $('#end_date').val(),
                });
            }
        },
        columns: [{
                data: 'id',
                name: 'id',
                searchable: true,
                visible: false,
                orderable: false
            },
            {
                data: 'sto_date',
                name: 'sto_date',
                searchable: false,
                visible: true,
                orderable: true
            },
            {
                data: 'name',
                name: 'name',
                searchable: false,
                visible: true,
                orderable: true
            },
            {
                data: 'locator',
                name: 'locator',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'warehouse_name',
                name: 'warehouse_name',
                searchable: false,
                visible: true,
                orderable: true
            },
            {
                data: 'document_no',
                name: 'document_no',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'c_bpartner_id',
                name: 'c_bpartner_id',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'supplier_name',
                name: 'supplier_name',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'item_code',
                name: 'item_code',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'uom_conversion',
                name: 'uom_conversion',
                searchable: false,
                visible: true,
                orderable: false
            },
            {
                data: 'qty_conversion',
                name: 'qty_conversion',
                searchable: false,
                visible: true,
                orderable: false
            },
        ]
    });

    var dtable = $('#accessories_report_material_stock_opname_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
        });
    dtable.draw();

    $('#select_warehouse').on('change', function () {
        dtable.draw();
    });
    $('#start_date').on('change', function () {
        dtable.draw();
    });

    $('#end_date').on('change', function () {
        dtable.draw();
    });
});