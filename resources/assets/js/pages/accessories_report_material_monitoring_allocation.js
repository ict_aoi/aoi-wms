$(function () 
{
    // var page = $('#page').val();

    // if(page == 'index')
    // {
        var material_monitoring_allocation =  $('#material_monitoring_allocation').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength: 100,
            scroller: true,
            deferRender: true,
            ajax: {
                type: 'GET',
                url: '/accessories/report/material-monitoring-allocation/data',
                data: function (d) {
                    return $.extend({}, d, {
                        "warehouse": $('#select_warehouse').val(),
                        "start_date": $('#start_date').val(),
                        "end_date": $('#end_date').val(),
                    });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = material_monitoring_allocation.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'lc_date', name: 'lc_date',searchable:true,visible:true,orderable:true},
                {data: 'barcode', name: 'barcode',searchable:true,visible:true,orderable:true},
                {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
                {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
                {data: 'po_buyer', name: 'po_buyer',searchable:true,visible:true,orderable:true},
                {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
                {data: 'category', name: 'category',searchable:true,visible:true,orderable:true},
                {data: 'style', name: 'style',searchable:true,visible:true,orderable:true},
                {data: 'article_no', name: 'article_no',searchable:false,visible:true,orderable:true},
                {data: 'uom_conversion', name: 'uom_conversion',searchable:false,visible:true,orderable:true},
                {data: 'qty_conversion', name: 'qty_conversion',searchable:true,visible:true,orderable:true},
                {data: 'pic', name: 'pic',searchable:false,visible:true,orderable:true},
                {data: 'last_status_movement', name: 'last_status_movement',searchable:false,visible:true,orderable:true},
                {data: 'last_movement_date', name: 'last_movement_date',searchable:false,visible:true,orderable:true},
                {data: 'locator', name: 'locator',searchable:false,visible:true,orderable:true},
                {data: 'warehouse', name: 'warehouse',searchable:false,visible:true,orderable:false},
                {data: 'action', name: 'action',searchable:false,visible:true,orderable:true},
            ]
        });
    
        var dtable = $('#material_monitoring_allocation').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
            });
        dtable.draw();
    
        $('#select_warehouse').on('change', function () {
            dtable.draw();
        });
    
        $('#start_date').on('change', function () {
            dtable.draw();
        });
    
        $('#end_date').on('change', function () {
            dtable.draw();
        });
    // }else
    // {
        // var detail_material_monitoring_allocation =  $('#detail_material_monitoring_allocation').DataTable({
        //     dom: 'Bfrtip',
        //     processing: true,
        //     serverSide: true,
        //     pageLength: 100,
        //     scroller: true,
        //     deferRender: true,
        //     ajax: {
        //         type: 'GET',
        //         url: '/fabric/report/daily-material-out/detail-data',
        //         data: function (d) {
        //             return $.extend({}, d, {
        //                 "planning_date"     : $('#planning_date').val(),
        //                 "warehouse_id"      : $('#warehouse_id').val(),
        //                 "style"             : $('#style').val(),
        //                 "article_no"        : $('#article_no').val(),
        //                 "po_buyer"          : $('#po_buyer').val(),
        //                 "is_piping"         : $('#is_piping').val(),
        //                 "c_order_id"        : $('#c_order_id').val(),
        //                 "item_id_book"      : $('#item_id_book').val(),
        //                 "item_id_source"    : $('#item_id_source').val(),
        //             });
        //         }
        //     },
        //     fnCreatedRow: function (row, data, index) {
        //         var info = detail_material_monitoring_allocation.page.info();
        //         var value = index+1+info.start;
        //         $('td', row).eq(0).html(value);
        //     },
        //     columns: [
        //         {data: null, sortable: false, orderable: false, searchable: false},
        //         {data: 'barcode', name: 'barcode',searchable:true,visible:true,orderable:false},
        //         {data: 'nomor_roll', name: 'nomor_roll',searchable:true,visible:true,orderable:false},
        //         {data: 'begin_width', name: 'begin_width',searchable:false,visible:true,orderable:true},
        //         {data: 'middle_width', name: 'middle_width',searchable:false,visible:true,orderable:true},
        //         {data: 'end_width', name: 'end_width',searchable:false,visible:true,orderable:true},
        //         {data: 'actual_width', name: 'actual_width',searchable:false,visible:true,orderable:true},
        //         {data: 'actual_lot', name: 'actual_lot',searchable:false,visible:true,orderable:true},
        //         {data: 'last_status_movement', name: 'last_status_movement',searchable:false,visible:true,orderable:true},
        //         {data: 'total_qty', name: 'total_qty',searchable:false,visible:true,orderable:true},
        //     ]
        // });
    
        // var dtable = $('#material_monitoring_allocation').dataTable().api();
        // $(".dataTables_filter input")
        //     .unbind() // Unbind previous default bindings
        //     .bind("keyup", function (e) { // Bind our desired behavior
        //         // If the user pressed ENTER, search
        //         if (e.keyCode == 13) {
        //             // Call the API search function
        //             dtable.search(this.value).draw();
        //         }
        //         // Ensure we clear the search if they backspace far enough
        //         if (this.value == "") {
        //             dtable.search("").draw();
        //         }
        //         return;
        //     });
        // dtable.draw();
    // }
    
});



function detail(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();

        }
    })
    .done(function (response) {
        // $('#update_invoice').attr('action', response.url_update);
        // $('#update_nama').val(response.nama);
        // $('#update_alamat').val(response.alamat);
        // $('#update_telepon').val(response.telepon);
        // $('#update_no_izin').val(response.no_izin);
        $('#material_preparation_id').val(response.id);
        $('#detailMaterialMonitoringModal').modal();
       detailTable();

    });
}

function detailTable(){
    $('#detail_material_monitoring').DataTable().destroy();
    $('#detail_material_monitoring tbody').empty();
    var detail_material_monitoring =  $('#detail_material_monitoring').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength: 100,
        scroller: true,
        deferRender: true,
        ajax: {
            type: 'GET',
            url: '/accessories/report/material-monitoring-allocation/detail-data',
            data: function (d) {
                return $.extend({}, d, {
                    "material_preparation_id": $('#material_preparation_id').val(),
                });
            }
        },
        columns: [
            {data: 'barcode', name: 'barcode',searchable:true,visible:true,orderable:true},
            {data: 'name', name: 'name',searchable:true,visible:true,orderable:true},
            {data: 'status_awal', name: 'status_awal',searchable:true,visible:true,orderable:true},
            {data: 'status_akhir', name: 'status_akhir',searchable:true,visible:true,orderable:true},
            {data: 'from_locator', name: 'from_locator',searchable:true,visible:true,orderable:true},
            {data: 'to_locator', name: 'to_locator',searchable:true,visible:true,orderable:true},
            {data: 'qty', name: 'qty',searchable:true,visible:true,orderable:true},
        ]
    });

    var dtable2 = $('#detail_material_monitoring').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable2.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable2.search("").draw();
            }
            return;
        });
    dtable2.draw();
}