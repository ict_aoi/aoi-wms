edit_bulk_items = JSON.parse($('#edit_bulk_items').val());

$(function()
{
	render();
	

});

$('#upload_button').on('click', function () {
	$('#upload_file').trigger('click');
});

$('#upload_file').on('change', function () {
	//$('#upload_file_allocation').submit();
	$.ajax({
		type: "post",
		url: $('#upload_file_allocation').attr('action'),
		data: new FormData(document.getElementById("upload_file_allocation")),
		processData: false,
		contentType: false,
		beforeSend: function () {
			edit_bulk_items = [];
			$.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) {
			$("#alert_info").trigger("click", 'Upload successfully.');
			for(idx in response){
				var input = response[idx];
				edit_bulk_items.push(input);
			}
		},
		error: function (response) {
			$.unblockUI();
			$('#upload_file_allocation').trigger('reset');
			if (response['status'] == 500)
				$("#alert_error").trigger("click", 'Please Contact ICT.');

			if (response['status'] == 422)
				$("#alert_error").trigger("click", response.responseJSON);

		}
	})
	.done(function () {
		$('#upload_file_allocation').trigger('reset');
		render();
	});

})



function render() 
{
	getIndex();
	$('#edit_bulk_items').val(JSON.stringify(edit_bulk_items));
	var tmpl = $('#edit-bulk-table').html();
	Mustache.parse(tmpl);
	var data = { item: edit_bulk_items };
	var html = Mustache.render(tmpl, data);
	$('#tbody-edit-bulk').html(html);
}

function getIndex() 
{
	for (idx in edit_bulk_items) {
		edit_bulk_items[idx]['_id'] = idx;
		edit_bulk_items[idx]['no'] = parseInt(idx) + 1;
	}
}