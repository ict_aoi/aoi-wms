list_material_print_barcode_switch = JSON.parse($('#material_print_barcode_switch').val());

$(function () {
    $('.barcode_value').focus();

    function render() {
        getIndex();
        $('#material_print_barcode_switch').val(JSON.stringify(list_material_print_barcode_switch));
        var tmpl = $('#print-barcode-switch-accessories-table').html();
        Mustache.parse(tmpl);
        var data = { item: list_material_print_barcode_switch };
        var html = Mustache.render(tmpl, data);
        $('#tbody-print-barcode-switch').html(html);
        $('.barcode_value').focus();
        bind();
    }

    function bind() {
        $('#barcode_value').on('change', tambahItem);
		var delay = (function () {
			var timer = 0;
			return function (callback, ms) {
				clearTimeout(timer);
				timer = setTimeout(callback, ms);
			};
		})();

        $('.btn-delete-item').on('click', deleteItem);
		/*$("#barcode_value").on("input", function () {
			delay(function () {
				if ($("#barcode_value").val().length < 12) {
					$("#barcode_value").val("");
				}
			}, 20);
		});*/
    }

    function deleteItem() {
        var i = $(this).data('id');
        list_material_print_barcode_switch.splice(i, 1);
        render();
    }

    function getIndex() {
        for (idx in list_material_print_barcode_switch) {
            list_material_print_barcode_switch[idx]['_idx'] = idx;
            list_material_print_barcode_switch[idx]['nox'] = parseInt(idx) + 1;
        }
    }

    function checkItem(barcode){
        for (idx in list_material_print_barcode_switch) {
            var data = list_material_print_barcode_switch[idx];

            if (data.barcode == barcode) return false;
        }

        return true;
    }

    function tambahItem() {
        var barcode_val = $('#barcode_value').val();
        var url_get_print_barcode_switch_accessories = $('#url_get_print_barcode_switch_accessories').attr('href');
        var diff = checkItem(barcode_val);

        if (!diff) {
            $("#barcode_value").val("");
            $(".barcode_value").focus();
            $("#alert_error").trigger("click", 'Item Sudah di scan.');
            return;
        } 
        
            $.ajax({
                type: "GET",
                url: url_get_print_barcode_switch_accessories,
                data: {
                    barcode: barcode_val
                },
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    list_material_print_barcode_switch.push(response);
                },
                error: function (response) {
                    $.unblockUI();
                    $('#barcode_value').val('');
                    $('.barcode_value').focus();

                    if (response['status'] == 500)
                        $("#alert_error").trigger("click", 'Please Contact ICT');

                    if (response['status'] == 422)
                        $("#alert_error").trigger("click", response.responseJSON);

                    list_material_print_barcode_switch = [];
                    render();
                }
            })
            .done(function () {
                
                $('#barcode_value').val('');
                $('.barcode_value').focus();
                render();
            });;
        


    }

    function setFocusToTextBox() {
        $('#barcode_value').focus();
    }
    setFocusToTextBox();
    render();

    $('#form').submit(function (event) {
        event.preventDefault();

        bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        $("#alert_success").trigger("click", 'Material Berhasil di Ubah');
                    },
                    error: function (response) {
                        $.unblockUI();
                        $('#barcode_value').val('');
                        $('.barcode_value').focus();

                        if (response['status'] == 500)
                            $("#alert_error").trigger("click", 'Please Contact ICT');

                        if (response['status'] == 422)
                            $("#alert_error").trigger("click", response.responseJSON);

                        if (response['status'] == 504)
                            $("#alert_error").trigger("click", 'request timeout');

                    }
                })
                .done(function (response) {
                    $('#barcode_value').val('');
                    $('.barcode_value').focus();
                    list_material_print_barcode_switch = [];
                    $('#list_barcodes').val(JSON.stringify(response));
                    $('#getBarcode').submit();
                    $('#list_barcodes').val('');
                    render();
                });
            }
        });
    });
});

