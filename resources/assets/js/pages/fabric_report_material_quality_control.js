$(function()
{
    var page = $('#page').val();
    if(page == 'index')
    {
        $('#fabric_report_material_quality_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            ajax: {
                type: 'GET',
                url: '/fabric/report/material-quality-control/data',
                data: function(d) {
                    return $.extend({}, d, {
                        "warehouse"     : $('#select_warehouse').val(),
                        "start_date"    : $('#start_date').val(),
                        "end_date"      : $('#end_date').val(),
                        "status"        : $('#select_status').val(),
                    });
               }
            },
            columns: [
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'po_detail_id', name: 'po_detail_id',searchable:false,visible:true,orderable:true},
                {data: 'no_pt', name: 'no_pt',searchable:false,visible:true,orderable:true},
                {data: 'is_handover', name: 'is_handover',searchable:false,visible:true,orderable:true},
                {data: 'warehouse_id', name: 'warehouse_id',searchable:false,visible:true,orderable:true},
                {data: 'receiving_date', name: 'receiving_date',searchable:true,visible:true,orderable:true},
                {data: 'inspection_date', name: 'inspection_date',searchable:true,visible:true,orderable:true},
                {data: 'name', name: 'name',searchable:false,visible:true,orderable:false},
                {data: 'supplier_name', name: 'supplier_name',searchable:false,visible:true,orderable:true},
                {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
                {data: 'no_invoice', name: 'no_invoice',searchable:true,visible:true,orderable:true},
                {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
                {data: 'color', name: 'color',searchable:false,visible:true,orderable:true},
                {data: 'total_batch_number', name: 'total_batch_number',searchable:false,visible:true,orderable:true},
                {data: 'total_inspected_batch_number', name: 'total_inspected_batch_number',searchable:false,visible:true,orderable:true},
                {data: 'total_all_roll', name: 'total_all_roll',searchable:false,visible:true,orderable:true},
                {data: 'total_all_yds', name: 'total_all_yds',searchable:false,visible:true,orderable:true},
                {data: 'action', name: 'action',searchable:false,visible:true,orderable:true},
            ]
        });
    
        var dtable = $('#fabric_report_material_quality_table').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();
        
        $('#select_warehouse').on('change',function(){
            dtable.draw();
        });
    
        $('#start_date').on('change',function(){
            dtable.draw();
        });
    
        $('#end_date').on('change',function(){
            dtable.draw();
        });
        $('#select_status').on('change', function(){
            dtable.draw();
        });
    }else if(page == 'import')
    {
        list_confirmation = JSON.parse($('#list_confirmation').val());
		render();
    }else
    {
        list_confirm = JSON.parse($('#list_confirm').val());
        var flag_msg = $('#flag_msg').val();
        if(flag_msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved');
        
        var url_fabric_material_quality_control_detail_data         = $('#url_fabric_material_quality_control_detail_data').val();
        var url_fabric_material_quality_control_detail_data_other   = $('#url_fabric_material_quality_control_detail_data_other').val();
        
        $('#fabric_report_material_quality_detail_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            ajax: {
                type: 'GET',
                url: url_fabric_material_quality_control_detail_data
            },
            columns: [
                {data: 'material_check_id', name: 'material_check_id',searchable:true,visible:false,orderable:false},
                {data: 'created_at', name: 'created_at',searchable:true,visible:true,orderable:true},
                {data: 'nomor_roll', name: 'nomor_roll',searchable:true,visible:true,orderable:true},
                {data: 'batch_number', name: 'batch_number',searchable:true,visible:true,orderable:true},
                {data: 'load_actual', name: 'load_actual',searchable:false,visible:true,orderable:false},
                {data: 'kg', name: 'kg',searchable:true,visible:true,orderable:true},
                {data: 'qty_on_barcode', name: 'qty_on_barcode',searchable:false,visible:true,orderable:false},
                {data: 'width_on_barcode', name: 'width_on_barcode',searchable:false,visible:true,orderable:false},
                {data: 'actual_length', name: 'actual_length',searchable:false,visible:true,orderable:false},
                {data: 'different_yard', name: 'different_yard',searchable:false,visible:true,orderable:false},
                {data: 'actual_width', name: 'actual_width',searchable:false,visible:true,orderable:false},
                {data: 'jumlah_defect_a', name: 'jumlah_defect_a',searchable:false,visible:true,orderable:false},
                {data: 'jumlah_defect_b', name: 'jumlah_defect_b',searchable:false,visible:true,orderable:false},
                {data: 'jumlah_defect_c', name: 'jumlah_defect_c',searchable:false,visible:true,orderable:false},
                {data: 'jumlah_defect_d', name: 'jumlah_defect_d',searchable:false,visible:true,orderable:false},
                {data: 'jumlah_defect_e', name: 'jumlah_defect_e',searchable:false,visible:true,orderable:false},
                {data: 'jumlah_defect_f', name: 'jumlah_defect_f',searchable:false,visible:true,orderable:false},
                {data: 'jumlah_defect_g', name: 'jumlah_defect_g',searchable:false,visible:true,orderable:false},
                {data: 'jumlah_defect_h', name: 'jumlah_defect_h',searchable:false,visible:true,orderable:false},
                {data: 'jumlah_defect_i', name: 'jumlah_defect_i',searchable:false,visible:true,orderable:false},
                {data: 'jumlah_defect_j', name: 'jumlah_defect_j',searchable:false,visible:true,orderable:false},
                {data: 'jumlah_defect_k', name: 'jumlah_defect_k',searchable:false,visible:true,orderable:false},
                {data: 'jumlah_defect_l', name: 'jumlah_defect_l',searchable:false,visible:true,orderable:false},
                {data: 'jumlah_defect_m', name: 'jumlah_defect_m',searchable:false,visible:true,orderable:false},
                {data: 'jumlah_defect_n', name: 'jumlah_defect_n',searchable:false,visible:true,orderable:false},
                {data: 'jumlah_defect_o', name: 'jumlah_defect_o',searchable:false,visible:true,orderable:false},
                {data: 'jumlah_defect_p', name: 'jumlah_defect_p',searchable:false,visible:true,orderable:false},
                {data: 'jumlah_defect_q', name: 'jumlah_defect_q',searchable:false,visible:true,orderable:false},
                {data: 'jumlah_defect_r', name: 'jumlah_defect_r',searchable:false,visible:true,orderable:false},
                {data: 'jumlah_defect_s', name: 'jumlah_defect_s',searchable:false,visible:true,orderable:false},
                {data: 'jumlah_defect_t', name: 'jumlah_defect_t',searchable:false,visible:true,orderable:false},
                {data: 'total_linear_point', name: 'total_linear_point',searchable:false,visible:true,orderable:false},
                {data: 'total_yds', name: 'total_yds',searchable:false,visible:true,orderable:false},
                {data: 'total_formula_1', name: 'total_formula_1',searchable:false,visible:true,orderable:false},
                {data: 'total_formula_2', name: 'total_formula_2',searchable:false,visible:true,orderable:false},
                {data: 'pic_qc', name: 'pic_qc',searchable:false,visible:true,orderable:false},
                {data: 'pic_confirm_qc', name: 'pic_confirm_qc',searchable:false,visible:true,orderable:false},
                {data: 'remark', name: 'remark',searchable:false,visible:true,orderable:false},
                {data: 'final_result', name: 'final_result',searchable:false,visible:true,orderable:false},
                {data: 'note', name: 'note',searchable:false,visible:true,orderable:false},
                {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
            ]
        });
    
        var dtable = $('#fabric_report_material_quality_detail_table').dataTable().api();
        $("#fabric_report_material_quality_detail_table.dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();

        $('#fabric_report_material_quality_detail_other_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            ajax: {
                type: 'GET',
                url: url_fabric_material_quality_control_detail_data_other
            },
            columns: [
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'created_at', name: 'created_at',searchable:true,visible:true,orderable:true},
                {data: 'nomor_roll', name: 'nomor_roll',searchable:false,visible:true,orderable:true},
                {data: 'batch_number', name: 'batch_number',searchable:true,visible:true,orderable:true},
                {data: 'qty_order', name: 'qty_order',searchable:false,visible:true,orderable:false},
                {data: 'different_yard', name: 'different_yard',searchable:false,visible:true,orderable:true},
                {data: 'actual_width', name: 'actual_width',searchable:false,visible:true,orderable:true},
                {data: 'actual_length', name: 'actual_length',searchable:false,visible:true,orderable:true},
            ]
        });
    
        var dtableOther = $('#fabric_report_material_quality_detail_other_table').dataTable().api();
        $("#fabric_report_material_quality_detail_other_table.dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtableOther.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtableOther.search("").draw();
                }
                return;
        });
        dtableOther.draw();

        renderConfirmationFinalResult();

        $('#form').submit(function (event) 
        {
            event.preventDefault();
            
            if (list_confirm.length == 0) 
            {
                $("#alert_warning").trigger("click", 'There no data to need update');
                return false;
            }


            bootbox.confirm("Are you sure want to update the result ?.", function (result) 
            {
                if (result) 
                {
                    $.ajax({
                        type: "POST",
                        url: $('#form').attr('action'),
                        data: $('#form').serialize(),
                        beforeSend: function () 
                        {
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function () 
                        {
                            $.unblockUI();
                        },
                        success: function () 
                        {
                            list_confirm = [];
                            renderConfirmationFinalResult();
                        },
                        error: function (response) 
                        {
                            $.unblockUI();
                            $('#barcode_value').val('');
                            $('.barcode_value').focus();

                            if (response['status'] == 500)
                                $("#alert_error").trigger("click", 'Please Contact ICT');


                            if (response['status'] == 422)
                                $("#alert_warning").trigger("click", response.responseJSON);

                        }
                    })
                    .done(function (response){
                        $("#alert_success").trigger("click", 'Update final result successfully.');
                        $('#fabric_report_material_quality_detail_table').DataTable().ajax.reload();
                    });
                }
            });



        });

        $('#updateActualWidthform').submit(function (event) 
        {
            event.preventDefault();
            $('#updateActualWidhModal').modal('hide');
            bootbox.confirm("Are you sure want to update this data ?.", function (result) 
            {
                if (result) 
                {
                    $.ajax({
                        type: "POST",
                        url: $('#updateActualWidthform').attr('action'),
                        data: $('#updateActualWidthform').serialize(),
                        beforeSend: function () 
                        {
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function () 
                        {
                            $.unblockUI();
                        }
                        ,error: function (response) 
                        {
                            $.unblockUI();
                            $('#updateActualWidhModal').modal('show');
                            if (response['status'] == 500)  $("#alert_error").trigger("click", 'Please Contact ICT');
                            if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);
                        }
                    })
                    .done(function (){
                        $("#alert_success").trigger("click", 'Update cuttable width successfully.');
                        $('#fabric_report_material_quality_detail_table').DataTable().ajax.reload();
                    });
                }
            });



        });
    }
});

$('#qty_issue').keypress(function (e) {

    if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
        return false;
    }
    return true;
});

$('#qty_issue').change(function()
{
   var qty_issue        = parseFloat($(this).val());
   var qty_available    = parseFloat($('#issue_qty_arrival').val());
   var _temp            = qty_available - qty_issue;

   if(_temp < 0)
   {
        $("#alert_info").trigger("click", 'Qty issue should be less than qty on barcode.');
        $(this).val('');
        return false;
   }
});

$('#upload_button').on('click', function () 
{
	$('#upload_file').trigger('click');
});

$('#upload_file').on('change', function () 
{
	$.ajax({
		type: "post",
		url: $('#upload_file_confirmation').attr('action'),
		data: new FormData(document.getElementById("upload_file_confirmation")),
		processData: false,
		contentType: false,
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) 
		{
			$("#alert_info").trigger("click", 'Upload successfully.');

            for (idx in response) 
            {
				var data = response[idx];
				var input = 
				{
					'supplier_name'		: data.supplier_name,
					'no_invoice'		: data.no_invoice,
					'document_no'		: data.document_no,
					'item_code'		    : data.item_code,
					'category'		    : data.category,
					'no_roll'		    : data.no_roll,
					'batch_number'	    : data.batch_number,
					'final_result'		: data.final_result,
					'confirm_by'		: data.confirm_by,
					'confirm_date'		: data.confirm_date,
					'is_error'		    : data.is_error,
					'status'		    : data.status,
				};
				list_confirmation.push(input);
			}

		},
		error: function (response) 
		{
			$.unblockUI();
			$('#upload_file_confirmation').trigger('reset');
			if (response['status'] == 500)
				$("#alert_error").trigger("click", 'Please Contact ICT.');

			if (response['status'] == 422)
				$("#alert_error").trigger("click", response.responseJSON);

		}
	})
	.done(function () {
		$('#upload_file_confirmation').trigger('reset');
		render();
	});

});

$('.width').keypress(function (e) {

    if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
        return false;
    }
    return true;
});

$('#top_width').change(function()
{
    var value = $(this).val();

    if(!value)
    {
        var old_value = $('#_top_width').val();
        $(this).val(old_value);
        changeActualWidth();
        $("#alert_warning").trigger("click", 'Top width cannot be empty');
       
        return false;
    }
    $(this).val(value);

    changeActualWidth();
});

$('#middle_width').change(function()
{
   
    var value = $(this).val();

    if(!value)
    {
        var old_value = $('#_middle_width').val();
        $(this).val(old_value);
        changeActualWidth();
        $("#alert_warning").trigger("click", 'Middle width cannot be empty');
        
        return false;
    }

    $(this).val(value);
    changeActualWidth();
});

$('#bottom_width').change(function()
{
    var value = $(this).val();

    if(!value)
    {
        var old_value = $('#_bottom_width').val();
        $(this).val(old_value);
        changeActualWidth();
        $("#alert_warning").trigger("click", 'Bottom width cannot be empty');

        return false;

    }

    changeActualWidth();
});

function checkDataExists(id,selected_value,qty_issue) 
{
	for (var i in list_confirm) 
	{
		var data = list_confirm[i];

		if (data.id == id) 
		{

            data.selected_value = selected_value;
            
            if(qty_issue) data.qty_issue    = qty_issue;
            else if(selected_value !='SELEKSI PANEL' || selected_value !='SPESIAL MARKER')  data.qty_issue = '0';
            
            return true;
		}

	}

	return false;
}

function selectStatus(id)
{
    var selected_value = $('#id_'+id).val();
    var qty_on_roll = $('#qty_on_roll_'+id).val();
    var formula_1   = $('#formula_1_'+id).val();
    var formula_2   = $('#formula_2_'+id).val();
    var qty_issue   = '0';

   
    if(selected_value)
    {
        if(selected_value =='SELEKSI PANEL' || selected_value =='SPESIAL MARKER')
        {
            if(formula_1 != 0) qty_issue = formula_1;
            else if(formula_2 != 0) qty_issue = formula_2;
            else qty_issue = qty_on_roll;
            /*var qty_on_roll = $('#qty_on_roll_'+id).val();
            var formula_1   = $('#formula_1_'+id).val();
            var formula_2   = $('#formula_2_'+id).val();

            $('#issue_qty_arrival').val(qty_on_roll);
            $('#issue_formula_1').val(formula_1);
            $('#issue_formula_2').val(formula_2);
            $('#qty_issue').val('');
            $('#roll_id').val(id);
            $('#issue_title').text(selected_value);

            if(selected_value =='SPESIAL MARKER')
            {
                $('#btnCloseModal').removeClass('hidden');
            }else
            {
                $('#btnCloseModal').addClass('hidden');
            }

            $('#issueModalModal').modal({
                backdrop: 'static',
                keyboard: false  // to prevent closing with Esc button (if you want this too)
            });*/
        }else
        {
            qty_issue = '0';
        }

        var checkItem = checkDataExists(id,selected_value,qty_issue);

        if(!checkItem)
        {
            var input = {
                'id'                : id,
                'selected_value'    : selected_value,
                'qty_issue'         : qty_issue,
            };
    
            //issueModalModal
            list_confirm.push(input);
        }
        
        renderConfirmationFinalResult();
    }
}

function saveQtyIssue()
{
    var qty_issue       = $('#qty_issue').val();
    var roll_id         = $('#roll_id').val();
    var selected_value  = $('#id_'+roll_id).val();

    if(!qty_issue)
    {
        $("#alert_info").trigger("click", 'Please insert Qty issue first.');
        return false;
    }

    checkDataExists(roll_id,selected_value,qty_issue);
    renderConfirmationFinalResult();

    $('#issueModalModal').modal('toggle');

}

function note(url)
{
    swal({
		title: "Add Note",
		text: "Please type note :",
		type: "input",
		showCancelButton: true,
		confirmButtonColor: "#2196F3",
		closeOnConfirm: false,
		animation: "slide-from-top",
		inputPlaceholder: "Please type note here"
	},
	function (inputValue) 
	{
		if (inputValue === false) return false;
        if (inputValue === "") 
        {
			swal.showInputError("Bro / Sist, please write it first.");
			return false
		}
		
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
			type: "put",
			url: url,
			data: {
				'note': inputValue
			},
			beforeSend: function () {
				swal.close();
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function () {
				$.unblockUI();
			},
			error: function (response) {
				$.unblockUI();
				if (response['status'] == 422)
					$("#alert_error").trigger("click", response['responseJSON']);
			}
		})
		.done(function ($result) {
			$("#alert_success").trigger("click", 'Data successfully updated.');
			$('#fabric_report_material_quality_detail_table').DataTable().ajax.reload();
		});
	});
}

function renderConfirmationFinalResult() 
{
    $('#list_confirm').val(JSON.stringify(list_confirm));
    console.log(list_confirm);
}

function render() 
{
    getIndex();
	$('#list_confirmation').val(JSON.stringify(list_confirmation));
    var tmpl = $('#list_confirmation_table').html();
	Mustache.parse(tmpl);
	var data = { item: list_confirmation };
	var html = Mustache.render(tmpl, data);
	$('#tbody_list_confirmation').html(html);
}

function getIndex() 
{
	for (id in list_confirmation) {
		list_confirmation[id]['_id'] = id;
		list_confirmation[id]['no'] = parseInt(id) + 1;
	}
}

function showWidth(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });

            $('#_id').val('');
            $('#top_width').val('');
            $('#middle_width').val('');
            $('#bottom_width').text('');
            $('#actual_width').text('');
            $('#_top_width').text('');
            $('#_middle_width').text('');
            $('#_bottom_width').text('');
            $('#_actual_width').text('');
            $('#update_header').text('');
        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            if (response['status'] == 422) $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
    .done(function (response) 
    {
       
        $('#_id').val(response.id);
        $('#top_width').val(response.begin_width);
        $('#_top_width').val(response.begin_width);
        $('#middle_width').val(response.middle_width);
        $('#_middle_width').val(response.middle_width);
        $('#bottom_width').val(response.end_width);
        $('#_bottom_width').val(response.end_width);
        $('#actual_width').val(response.actual_width);
        $('#_actual_width').val(response.actual_width);
        $('#update_header').text(response.supplier_name+' '+response.document_no+' '+response.item_code+' '+response.nomor_roll );
        $('#updateActualWidhModal').modal({
            backdrop: 'static',
            keyboard: false  // to prevent closing with Esc button (if you want this too)
        });
    });
}

function changeActualWidth() 
{
	var begin_width     = $('#top_width').val();
	var middle_width    = $('#middle_width').val();
	var end_width       = $('#bottom_width').val();
    var actual_width    = null
    
    if (begin_width != null && middle_width != null && end_width != null) 
    {
		width = [begin_width, middle_width, end_width];
		Array.min = function (array) {
			return Math.min.apply(Math, array);
		};
		actual_width = Array.min(width);
    }
    
	$('#actual_width').val(actual_width);
}
