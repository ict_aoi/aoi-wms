$(function () 
{
    var page = $('#page').val();

    if(page == 'index')
    {
        var daily_material_out_table =  $('#daily_material_out_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength: 100,
            scroller: true,
            deferRender: true,
            ajax: {
                type: 'GET',
                url: '/fabric/report/daily-material-out/data',
                data: function (d) {
                    return $.extend({}, d, {
                        "warehouse": $('#select_warehouse').val(),
                        "start_date": $('#start_date').val(),
                        "end_date": $('#end_date').val(),
                    });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = daily_material_out_table.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'planning_date', name: 'planning_date',searchable:true,visible:true,orderable:true},
                {data: 'warehouse_id', name: 'warehouse_id',searchable:false,visible:true,orderable:false},
                {data: 'style', name: 'style',searchable:true,visible:true,orderable:true},
                {data: 'po_buyer', name: 'po_buyer',searchable:true,visible:true,orderable:true},
                {data: 'destination', name: 'destination',searchable:true,visible:true,orderable:true},
                {data: 'article_no', name: 'article_no',searchable:true,visible:true,orderable:true},
                {data: 'item_code_book', name: 'item_code_book',searchable:true,visible:true,orderable:true},
                {data: 'item_code_source', name: 'item_code_source',searchable:true,visible:true,orderable:true},
                {data: 'color', name: 'color',searchable:true,visible:true,orderable:true},
                {data: 'is_piping', name: 'is_piping',searchable:false,visible:true,orderable:true},
                {data: 'qty_bom', name: 'qty_bom',searchable:false,visible:true,orderable:true},
                {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
                {data: 'actual_lot', name: 'actual_lot',searchable:false,visible:true,orderable:true},
                {data: 'total_roll', name: 'total_roll',searchable:false,visible:true,orderable:true},
                {data: 'total_prepare', name: 'total_prepare',searchable:false,visible:true,orderable:true},
                {data: 'total_supply', name: 'total_supply',searchable:false,visible:true,orderable:true},
                {data: 'total_outstanding', name: 'total_outstanding',searchable:false,visible:true,orderable:true},
                {data: 'balance', name: 'balance',searchable:false,visible:true,orderable:true},
                {data: 'last_scan_prepare_date', name: 'last_scan_prepare_date',searchable:true,visible:true,orderable:true},
                {data: 'last_movement_out_date', name: 'last_movement_out_date',searchable:true,visible:true,orderable:true},
                {data: 'action', name: 'action',searchable:false,visible:true,orderable:true},
            ]
        });
    
        var dtable = $('#daily_material_out_table').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
            });
        dtable.draw();
    
        $('#select_warehouse').on('change', function () {
            dtable.draw();
        });
    
        $('#start_date').on('change', function () {
            dtable.draw();
        });
    
        $('#end_date').on('change', function () {
            dtable.draw();
        });
    }else
    {
        var detail_daily_material_out_table =  $('#detail_daily_material_out_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength: 100,
            scroller: true,
            deferRender: true,
            ajax: {
                type: 'GET',
                url: '/fabric/report/daily-material-out/detail-data',
                data: function (d) {
                    return $.extend({}, d, {
                        "planning_date"     : $('#planning_date').val(),
                        "warehouse_id"      : $('#warehouse_id').val(),
                        "style"             : $('#style').val(),
                        "article_no"        : $('#article_no').val(),
                        "po_buyer"          : $('#po_buyer').val(),
                        "is_piping"         : $('#is_piping').val(),
                        "c_order_id"        : $('#c_order_id').val(),
                        "item_id_book"      : $('#item_id_book').val(),
                        "item_id_source"    : $('#item_id_source').val(),
                    });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = detail_daily_material_out_table.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'barcode', name: 'barcode',searchable:true,visible:true,orderable:false},
                {data: 'nomor_roll', name: 'nomor_roll',searchable:true,visible:true,orderable:false},
                {data: 'begin_width', name: 'begin_width',searchable:false,visible:true,orderable:true},
                {data: 'middle_width', name: 'middle_width',searchable:false,visible:true,orderable:true},
                {data: 'end_width', name: 'end_width',searchable:false,visible:true,orderable:true},
                {data: 'actual_width', name: 'actual_width',searchable:false,visible:true,orderable:true},
                {data: 'actual_lot', name: 'actual_lot',searchable:false,visible:true,orderable:true},
                {data: 'last_status_movement', name: 'last_status_movement',searchable:false,visible:true,orderable:true},
                {data: 'total_qty', name: 'total_qty',searchable:false,visible:true,orderable:true},
            ]
        });
    
        var dtable = $('#daily_material_out_table').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
            });
        dtable.draw();
    }
});