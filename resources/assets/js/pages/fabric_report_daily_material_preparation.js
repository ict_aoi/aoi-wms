$(function()
{
	var active_tab = $('#page').val();

	if(active_tab =='index')
	{
		var active_tab = $('#active_tab').val();
		if(active_tab == 'reguler')
		{
			$('#active_tab_export').val('reguler')
			regulerTables();

			var dtablRegulerTable = $('#reguler_table').dataTable().api();
			$(".dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtablRegulerTable.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtablRegulerTable.search("").draw();
					}
					return;
			});
			dtablRegulerTable.draw();
			$('#select_warehouse').on('change',function(){
				dtablRegulerTable.draw();
			});
			
			$('#start_date').on('change',function(){
				dtablRegulerTable.draw();
			});
			
			$('#end_date').on('change',function(){
				dtablRegulerTable.draw();
			});

		}else if( active_tab == 'additional')
		{
			$('#active_tab_export').val('additional')
			additionalTables();

			var dtableAdditional = $('#additional_table').dataTable().api();
			$(".dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtableAdditional.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtableAdditional.search("").draw();
					}
					return;
			});
			dtableAdditional.draw();
			$('#select_warehouse').on('change',function(){
				dtableAdditional.draw();
			});
			
			$('#start_date').on('change',function(){
				dtableAdditional.draw();
			});
			
			$('#end_date').on('change',function(){
				dtableAdditional.draw();
			});
		}
		else if( active_tab == 'balance_marker')
		{
			$('#active_tab_export').val('balance_marker')
			balanceMarkerTables();

			var dtableBalanceMarker = $('#balance_marker_table').dataTable().api();
			$(".dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtableBalanceMarker.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtableBalanceMarker.search("").draw();
					}
					return;
			});
			dtableBalanceMarker.draw();
			$('#select_warehouse').on('change',function(){
				dtableBalanceMarker.draw();
			});
			
			$('#start_date').on('change',function(){
				dtableBalanceMarker.draw();
			});
			
			$('#end_date').on('change',function(){
				dtableBalanceMarker.draw();
			});
		}

	}else
	{
		list_preparation_materials 	= JSON.parse($('#list_preparation_materials').val());
		list_materials 				= JSON.parse($('#list_materials').val());

		var url_fabric_report_daily_material_preparation_detail = $('#url_fabric_report_daily_material_preparation_detail').val();
		$('#daily_detail_material_preparation_table').DataTable({
			dom: 'Bfrtip',
			processing: true,
			ordering: false,
			serverSide: true,
			pageLength:-1,
			ajax: {
				type: 'GET',
				url: url_fabric_report_daily_material_preparation_detail
			},
			footerCallback: function ( row, data, start, end, display ) {
				var api = this.api(), data;
	
				// converting to interger to find total
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
	
				// computing column Total of the complete result
				var total = api
					.column( 14 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );
	
				$( api.column( 13 ).footer() ).html('Total');
				$( api.column( 14 ).footer() ).html(total.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
			},
			columns: [
				{data: 'id', name: 'id',visible:false},
				{data: 'checkbox', name: 'checkbox',visible:true,searchable:false},
				{data: 'created_at', name: 'created_at',searchable:false},
				{data: 'name', name: 'name',searchable:false},
				{data: 'movement_out_date', name: 'movement_out_date',searchable:false},
				{data: 'barcode', name: 'barcode'},
				{data: 'nomor_roll', name: 'nomor_roll'},
				{data: 'batch_number', name: 'batch_number',orderable:false,searchable:false},
				{data: 'begin_width', name: 'begin_width',orderable:false,searchable:false},
				{data: 'middle_width', name: 'middle_width',orderable:false,searchable:false},
				{data: 'end_width', name: 'end_width',orderable:false,searchable:false},
				{data: 'actual_width', name: 'actual_width',orderable:false,searchable:false},
				{data: 'actual_length', name: 'actual_length',orderable:false,searchable:false},
				{data: 'load_actual', name: 'load_actual',orderable:false,searchable:false},
				{data: 'reserved_qty', name: 'reserved_qty',orderable:false,searchable:false},
				{data: 'action', name: 'action',searchable:false,orderable:false},
			]
		});

		var dtabl = $('#daily_detail_material_preparation_table').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtabl.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtabl.search("").draw();
				}
				return;
		});
		dtabl.draw();

		$('#formChangePlan').submit(function (event) 
		{
			event.preventDefault();
		
			if(list_materials.length == '')
			{
				$("#alert_warning").trigger("click", 'Please select material first');
				return false;
			}


			bootbox.confirm("Are you sure want to move this preparation ?.", function (result) 
			{
				if (result) 
				{
					$.ajax({
						type: "POST",
						cache:false,
						url: $('#formChangePlan').attr('action'),
						data: $('#formChangePlan').serialize(),
						beforeSend: function () {
							$.blockUI({
								message: '<i class="icon-spinner4 spinner"></i>',
								overlayCSS: {
									backgroundColor: '#fff',
									opacity: 0.8,
									cursor: 'wait'
								},
								css: {
									border: 0,
									padding: 0,
									backgroundColor: 'transparent'
								}
							});
						},
						complete: function () {
							$.unblockUI();
						},
						success: function () 
						{
							
						},
						error: function (response) 
						{
							$.unblockUI();
							
							if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
							if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);
						}
					})
					.done(function (response)
					{

						$("#alert_success").trigger("click", 'Data successfully saved.');
						if(response != -1)
						{
							console.log(response);
							$('#list_barcodes').val(JSON.stringify(response));
							$('#getBarcode').submit();
							$('#list_barcodes').val('');
						}
						
						location.reload();
					});
				}
			});
		});
	}
});

$('#active_tab').on('change',function()
{
	var active_tab = $('#active_tab').val();
	if(active_tab == 'reguler')
	{
		$('#active_tab_export').val('reguler')
		regulerTables();

		var dtablRegulerTable = $('#reguler_table').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtablRegulerTable.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtablRegulerTable.search("").draw();
				}
				return;
		});
		dtablRegulerTable.draw();
		$('#select_warehouse').on('change',function(){
            dtablRegulerTable.draw();
		});
		
		$('#start_date').on('change',function(){
            dtablRegulerTable.draw();
		});
		
		$('#end_date').on('change',function(){
            dtablRegulerTable.draw();
        });

		
	}else if( active_tab == 'additional')
	{
		$('#active_tab_export').val('additional');
		additionalTables();
		var dtableAdditional = $('#additional_table').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtableAdditional.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtableAdditional.search("").draw();
				}
				return;
		});
		dtableAdditional.draw();
		$('#select_warehouse').on('change',function(){
            dtableAdditional.draw();
		});
		
		$('#start_date').on('change',function(){
            dtableAdditional.draw();
		});
		
		$('#end_date').on('change',function(){
            dtableAdditional.draw();
        });
	}
	else if( active_tab == 'balance_marker')
	{
		$('#active_tab_export').val('balance_marker');
		balanceMarkerTables();
		var dtableBalanceMarker = $('#balance_marker_table').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtableBalanceMarker.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtableBalanceMarker.search("").draw();
				}
				return;
		});
		dtableBalanceMarker.draw();
		$('#select_warehouse').on('change',function(){
            dtableBalanceMarker.draw();
		});
		
		$('#start_date').on('change',function(){
            dtableBalanceMarker.draw();
		});
		
		$('#end_date').on('change',function(){
            dtableBalanceMarker.draw();
        });
	}
	 
});

$('.start_date').datepicker({
	format: "dd/mm/yyyy",
	autoclose: true,
	todayHighlight: true
})
.on("change", function () 
{
	var warehouse_id 					= $('#warehouse_id').val();
	var planning_date 					= $('#_planning_date').val();
	var start_date 						= $('#start_date').val();
	var _planning_date 					= $('#_planning_date').val();
	var is_piping 						= $('#is_piping').val();
	var material_preparation_fabric_id	= $('#material_preparation_fabric_id').val();

	if((is_piping == null || !is_piping) && start_date)
	{
		$("#alert_warning").trigger("click", 'Please select piping type first');
		$('#start_date').val('');
	}else
	{
		if (!_planning_date) 
		{
			getPlanningPerArticle(material_preparation_fabric_id,warehouse_id,is_piping, this.value);
		}else 
		{
			if (_planning_date != this.value) getPlanningPerArticle(material_preparation_fabric_id,warehouse_id,is_piping, this.value);
		}
	}
});

$('#select_article').on('change',function()
{
	var warehouse_id 		= $('#warehouse_id').val();
	var is_piping 			= $('#is_piping').val();
	var value 				= $(this).val();    
	var planning_date 		= $('#_planning_date').val();  
	
	if(value)
    {
        var url_get_planning_per_style = $('#url_get_planning_per_style').val();

		$.ajax({
			type: "get",
			url: url_get_planning_per_style,
			data: {
				planning_date	: planning_date,
				article_no		: value,
				warehouse_id	: warehouse_id,
				is_piping		: is_piping
			},
			beforeSend: function () 
			{
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			complete: function () 
			{
				$.unblockUI();
			},
			success: function (response) 
			{
				var styles 	= response.styles;
				var total 	= response.total;

				$("#select_style").empty();
				
				$.each(styles,function(id,name){
					$("#select_style").append('<option value="'+id+'">'+name+'</option>');
				});

				if(total > 0) $('#select_style').trigger('change');
			},
			error: function (response) {
				$.unblockUI();

				if (response['status'] == 500)
					$("#alert_error").trigger("click", 'Please Contact ICT');

				if (response['status'] == 422)
				{
					$("#alert_warning").trigger("click", response.responseJSON);
					$("#select_style").empty();
					$("#select_style").append('<option value="">-- Select Style --</option>');
				
				}
			}
		});
    }else
    {
        $("#select_style").empty();
        $("#select_style").append('<option value="">-- select Style --</option>');

    }
});

$('#select_style').on('change',function()
{
	var value 								= $(this).val();    
	var article 							= $('#select_article').val(); 
	var warehouse_id 						= $('#warehouse_id').val(); 
	var material_preparation_fabric_id 		= $('#material_preparation_fabric_id').val();  
	var planning_date 						= $('#_planning_date').val();  
	var is_piping 							= $('#is_piping').val();  
	
	if(value)
    {
        var url_get_planning_per_date = $('#url_get_planning_per_date').val();

		$.ajax({
			type: "get",
			url: url_get_planning_per_date,
			data: {
				id				: material_preparation_fabric_id,
				planning_date	: planning_date,
				article_no		: article,
				warehouse_id	: warehouse_id,
				style			: value,
				is_piping		: is_piping
			},
			beforeSend: function () 
			{
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			complete: function () 
			{
				$.unblockUI();
			},
			success: function (response) 
			{
				if (response != -1) 
				{
					uncheckedAll();
					var material_preparation_fabric_id 	= response.id;
					var qty_outstanding 				= response.total_qty_outstanding;
					var qty_relax 						= response.total_qty_rilex;
					var total_reserved_qty				= response.total_reserved_qty;
					
					$('#qty_preparation').val(total_reserved_qty);
					$('#qty_outstanding').val(qty_outstanding);
					$('#qty_relax').val(qty_relax);
					$('#new_material_preparation_fabric_id').val(material_preparation_fabric_id);
				
				} else 
				{
					$('#material_preparation_fabric_id').val('');
					$('#qty_preparation').val('');
					$('#qty_outstanding').val('0');
					$('#qty_relax').val('0');
				}
			},
			error: function (response) {
				$.unblockUI();

				if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
				if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);
			}
		});
    }
});

function getPlanningPerArticle( id, warehouse_id,is_piping, selected_date) 
{
	if (selected_date) 
	{
		var url_get_planning_per_article = $('#url_get_planning_per_article').val();

		$.ajax({
			type: "get",
			url: url_get_planning_per_article,
			data: {
				id				: id,
				warehouse_id	: warehouse_id,
				planning_date	: selected_date,
				is_piping		: is_piping,
			},
			beforeSend: function () 
			{
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			complete: function () 
			{
				$.unblockUI();
			},
			success: function (response) 
			{
				var articles = response.articles;
				var total = response.total;

				$("#select_article").empty();
				$.each(articles,function(id,name){
					$("#select_article").append('<option value="'+id+'">'+name+'</option>');
				});

				if(total > 0) $('#select_article').trigger('change');
			},
			error: function (response) 
			{
				$.unblockUI();

			
				if (response['status'] == 500)
					$("#alert_error").trigger("click", 'Please Contact ICT');

				if (response['status'] == 422)
				{
					$("#alert_warning").trigger("click", response.responseJSON);
					$("#select_article").empty();
					$("#select_article").append('<option value="">-- Select Article --</option>');

					$("#select_style").empty();
					$("#select_style").append('<option value="">-- Select Style --</option>');

					$('#qty_preparation').val('0');
					$('#qty_relax').val('0');
					$('#qty_outstanding').val('0');
				
				}
					


			}
		});
	}

	$('#_planning_date').val(selected_date);
}

function getPlanning( id, selected_date) 
{
	if (selected_date) 
	{
		var url_get_planning = $('#url_get_planning').val();

		$.ajax({
			type: "get",
			url: url_get_planning,
			data: {
				id				: id,
				planning_date	: selected_date,
			},
			beforeSend: function () 
			{
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			complete: function () 
			{
				$.unblockUI();
			},
			success: function (response) 
			{
				var data 	= response.data;
				var total 	= response.total;
				if(total == 0)
				{
					$("#alert_warning").trigger("click", 'Planning not found');
					$("#select_style").empty();
					$("#select_style").append('<option value="">-- Select Style --</option>');

					$("#select_article").empty();
					$("#select_article").append('<option value="">-- Select Article --</option>');

					$('#qty_preparation').val('0');
					$('#qty_relax').val('0');
					$('#qty_outstanding').val('0');
				} 
				else
				{
					$('#qty_preparation').val(data.total_qty_outstanding);
					$('#qty_relax').val(data.total_qty_rilex);
					$('#qty_outstanding').val(data.total_qty_outstanding);
				}
			},
			error: function (response) 
			{
				$.unblockUI();
				
				if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
				if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);
			}
		});
	}

	$('#_planning_date').val(selected_date);
}

function regulerTables()
{
	$('#reguler_table').DataTable().destroy();
	$('#reguler_table tbody').empty();
	
	var regulerTable =  $('#reguler_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        ajax: {
            type: 'GET',
			url: '/fabric/report/daily-material-preparation/data-reguler',
			data: function(d) {
                return $.extend({}, d, {
                    "warehouse" 	: $('#select_warehouse').val(),
                    "start_date" 	: $('#start_date').val(),
                    "end_date" 		: $('#end_date').val(),
                });
            }
		},
		
		fnCreatedRow: function (row, data, index) {
			var info = regulerTable.page.info();
			var value = index+1+info.start;
			$('td', row).eq(0).html(value);
		},
        columns: [
			{data: null, sortable: false, orderable: false, searchable: false},
			{data: 'id', name: 'id',visible:false},
			{data: 'warehouse_id', name: 'warehouse_id'},
			{data: 'planning_date', name: 'planning_date',searchable:true},
			{data: 'is_piping', name: 'is_piping',searchable:false},
			{data: 'document_no', name: 'document_no',orderable:true},
			{data: 'supplier_name', name: 'supplier_name'},
			{data: 'item_code', name: 'item_code',orderable:true},
			{data: 'item_code_source', name: 'item_code_source'},
			{data: 'po_buyer', name: 'po_buyer'},
			{data: 'article_no', name: 'article_no',orderable:true,searchable:true},
			{data: '_style', name: '_style',orderable:true,searchable:true},
			{data: 'uom', name: 'uom'},
			{data: 'total_reserved_qty', name: 'total_reserved_qty',orderable:true,searchable:false},
			{data: 'total_qty_outstanding', name: 'total_qty_outstanding',orderable:true,searchable:false},
			{data: 'total_qty_rilex', name: 'total_qty_rilex',searchable:false,orderable:true},
			{data: 'actual_lot', name: 'actual_lot',searchable:false,orderable:true},
			{data: 'machine', name: 'machine',searchable:false,orderable:true},
			{data: 'status', name: 'status',searchable:false,orderable:true},
			{data: 'last_locator_code', name: 'last_locator_code',searchable:true,orderable:true},
			{data: 'remark', name: 'remark',searchable:false},
			{data: 'remark_planning', name: 'remark_planning',searchable:false},
			{data: 'action', name: 'action',searchable:false,orderable:false},
		]
    });
}

function balanceMarkerTables()
{
	$('#balance_marker_table').DataTable().destroy();
	$('#balance_marker_table tbody').empty();
	
	var dtableBalanceMarker =  $('#balance_marker_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        ajax: {
            type: 'GET',
			url: '/fabric/report/daily-material-preparation/data-balance-marker',
			data: function(d) {
                return $.extend({}, d, {
                    "warehouse" 	: $('#select_warehouse').val(),
                    "start_date" 	: $('#start_date').val(),
                    "end_date" 		: $('#end_date').val(),
                });
            }
		},
		
		fnCreatedRow: function (row, data, index) {
			var info = dtableBalanceMarker.page.info();
			var value = index+1+info.start;
			$('td', row).eq(0).html(value);
		},
        columns: [
			{data: null, sortable: false, orderable: false, searchable: false},
			{data: 'id', name: 'id',visible:false},
			{data: 'auto_allocation_id', name: 'auto_allocation_id', orderable:true, searchable:true},
			{data: 'warehouse_id', name: 'warehouse_id'},
			{data: 'planning_date', name: 'planning_date',searchable:true},
			{data: 'is_piping', name: 'is_piping',searchable:false},
			{data: 'document_no', name: 'document_no',orderable:true},
			{data: 'supplier_name', name: 'supplier_name'},
			{data: 'item_code', name: 'item_code',orderable:true},
			{data: 'item_code_source', name: 'item_code_source'},
			{data: 'po_buyer', name: 'po_buyer'},
			{data: 'article_no', name: 'article_no',orderable:true,searchable:true},
			{data: '_style', name: '_style',orderable:true,searchable:true},
			{data: 'uom', name: 'uom'},
			{data: 'total_reserved_qty', name: 'total_reserved_qty',orderable:true,searchable:false},
			{data: 'total_qty_outstanding', name: 'total_qty_outstanding',orderable:true,searchable:false},
			{data: 'total_qty_rilex', name: 'total_qty_rilex',searchable:false,orderable:true},
			{data: 'machine', name: 'machine',searchable:false,orderable:true},
			{data: 'remark', name: 'remark',searchable:false},
			{data: 'remark_planning', name: 'remark_planning',searchable:false},
			{data: 'action', name: 'action',searchable:false,orderable:false},
		]
    });
}

function additionalTables()
{
	$('#additional_table').DataTable().destroy();
	$('#additional_table tbody').empty();
	
	var additionalTable =  $('#additional_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        ajax: {
            type: 'GET',
			url: '/fabric/report/daily-material-preparation/data-additional',
			data: function(d) {
                return $.extend({}, d, {
                    "warehouse" 	: $('#select_warehouse').val(),
                    "start_date" 	: $('#start_date').val(),
                    "end_date" 		: $('#end_date').val(),
                });
            }
		},
		fnCreatedRow: function (row, data, index) {
			var info = additionalTable.page.info();
			var value = index+1+info.start;
			$('td', row).eq(0).html(value);
		},
		columns: [
			{data: null, sortable: false, orderable: false, searchable: false},
			{data: 'id', name: 'id',visible:false},
			{data: 'auto_allocation_id', name: 'auto_allocation_id', orderable:true, searchable:true},
			{data: 'created_at', name: 'created_at', orderable:true, searchable:true},
			{data: 'warehouse_id', name: 'warehouse_id'},
			{data: 'is_piping', name: 'is_piping',searchable:false},
			{data: 'document_no', name: 'document_no', searchable:true},
			{data: 'supplier_name', name: 'supplier_name'},
			{data: 'item_code', name: 'item_code' ,searchable:true},
			{data: 'uom', name: 'uom'},
			{data: 'total_reserved_qty', name: 'total_reserved_qty',orderable:false,searchable:false},
			{data: 'total_qty_outstanding', name: 'total_qty_outstanding',orderable:false,searchable:false},
			{data: 'total_qty_rilex', name: 'total_qty_rilex',searchable:false},
			{data: 'additional_note', name: 'additional_note',searchable:true},
			{data: 'action', name: 'action',searchable:false,orderable:false},
		]
    });
}

function changeTab(status)
{
	$('#active_tab').val(status).trigger('change');
}

function remark(url)
{
    swal({
		title: "Add Remark",
		text: "Please type remark :",
		type: "input",
		showCancelButton: true,
		confirmButtonColor: "#2196F3",
		closeOnConfirm: false,
		animation: "slide-from-top",
		inputPlaceholder: "Please type remark here"
	},
	function (inputValue) 
	{
		if (inputValue === false) return false;
        if (inputValue === "") 
        {
			swal.showInputError("Bro / Sist, please write it first.");
			return false
		}
		
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
			type: "put",
			url: url,
			data: {
				'remark': inputValue
			},
			beforeSend: function () {
				swal.close();
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function () {
				$.unblockUI();
			},
			error: function (response) {
				$.unblockUI();
				if (response['status'] == 422) $("#alert_error").trigger("click", response['responseJSON']);
				if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
			}
		})
		.done(function () {
			$("#alert_success").trigger("click", 'Data successfully updated.');
			var active_tab = $('#active_tab').val();
			if(active_tab == 'reguler') $('#reguler_table').DataTable().ajax.reload();
			else $('#additional_table').DataTable().ajax.reload();
		});
	});
}

function changeMachine(url)
{
	$.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            if (response['status'] == 422) $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
    .done(function (response) {
    	$('#material_preparation_fabric_id').val(response.id);
        $('#preparation_header').text(response.planning+' '+response.document_no+' '+response.item_code+' '+response.po_buyer );
        $('#machineModal').modal({
            backdrop: 'static',
            keyboard: false  // to prevent closing with Esc button (if you want this too)
        });
    });
}

$('#machine_form').submit(function (event) 
{
	event.preventDefault();
	
	bootbox.confirm("Are you sure wan to save this item ?.", function (result) {
		if (result) {
			$.ajax({
				type: "PUT",
				url: $('#form').attr('action'),
				data: $('#form').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				complete: function () {
					$.unblockUI();
				},
				success: function () {
					$("#alert_success").trigger("click", 'Data successfully received');
				},
				error: function (response) {
					$.unblockUI();
					if (response['status'] == 500 ) $("#alert_error").trigger("click", 'Please Contact ICT');
					if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);

				}
			})
			.done(function () 
			{
				$('#barcode_value').val('');
				$('.barcode_value').focus();
				list_material_receive_piping__fabric = [];
				render();
			});
		}
	});
});


function closePrepare(url)
{
    swal({
		title: "Add Reason",
		text: "Please type reason :",
		type: "input",
		showCancelButton: true,
		confirmButtonColor: "#2196F3",
		closeOnConfirm: false,
		animation: "slide-from-top",
		inputPlaceholder: "Please type reason here"
	},
	function (inputValue) 
	{
		if (inputValue === false) return false;
        if (inputValue === "") 
        {
			swal.showInputError("Bro / Sist, please write it first.");
			return false
		}
		
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
			type: "put",
			url: url,
			data: {
				'remark': inputValue
			},
			beforeSend: function () {
				swal.close();
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function () {
				$.unblockUI();
			},
			error: function (response) {
				$.unblockUI();
				if (response['status'] == 422) $("#alert_warning").trigger("click", response['responseJSON']);
				if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
			}
		})
		.done(function () {
			$("#alert_success").trigger("click", 'Data successfully updated.');
			var active_tab = $('#active_tab').val();
			if(active_tab == 'reguler') $('#reguler_table').DataTable().ajax.reload();
			else $('#additional_table').DataTable().ajax.reload();
		});
	});
}

function cancel(url)
{
	bootbox.confirm("Are you sure want cancel this preparation ?.", function (result) 
	{
		if (result) 
		{
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
	
			$.ajax({
				type: "post",
				url: url,
				beforeSend: function () {
					swal.close();
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function () {
					$.unblockUI();
				},
				error: function (response) {
					$.unblockUI();
					if (response['status'] == 422) $("#alert_warning").trigger("click", response['responseJSON']);
					if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
				}
			})
			.done(function () {
				$("#alert_success").trigger("click", 'Data successfully updated.');
				var active_tab = $('#active_tab').val();
				if(active_tab == 'reguler') $('#reguler_table').DataTable().ajax.reload();
				else $('#additional_table').DataTable().ajax.reload();
			});
		}
	});
}

function destroy(url)
{
    swal({
		title: "Add Reason",
		text: "Please type Reason :",
		type: "input",
		showCancelButton: true,
		confirmButtonColor: "#2196F3",
		closeOnConfirm: false,
		animation: "slide-from-top",
		inputPlaceholder: "Please type reason here"
	},
	function (inputValue) 
	{
		if (inputValue === false) return false;
        if (inputValue === "") 
        {
			swal.showInputError("Bro / Sist, please write it first.");
			return false
		}
		
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
			type: "post",
			url: url,
			data: {
				'reason': inputValue
			},
			beforeSend: function () {
				swal.close();
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function () {
				$.unblockUI();
			},
			error: function (response) {
				$.unblockUI();
				if (response['status'] == 422) $("#alert_warning").trigger("click", response['responseJSON']);
				if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
			}
		})
		.done(function () {
			$("#alert_success").trigger("click", 'Data successfully updated.');
			var active_tab = $('#active_tab').val();
			if(active_tab == 'reguler') $('#reguler_table').DataTable().ajax.reload();
			else $('#additional_table').DataTable().ajax.reload();
		});
	});
}

function deleteClosing(url)
{
    $.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$.ajax({
		type: "post",
		url: url,
		beforeSend: function () {
			swal.close();
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		success: function () {
			$.unblockUI();
		},
		error: function (response) {
			$.unblockUI();
			if (response['status'] == 422) $("#alert_warning").trigger("click", response['responseJSON']);
			if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
		}
	})
	.done(function () {
		$("#alert_success").trigger("click", 'Data successfully updated.');
		var active_tab = $('#active_tab').val();
		if(active_tab == 'reguler') $('#reguler_table').DataTable().ajax.reload();
		else $('#additional_table').DataTable().ajax.reload();
	});
}

function history(url)
{
    $('#historyModal').modal();
    $('#fabric_report_daily_material_preparation_history_table').DataTable().destroy();
    $('#fabric_report_daily_material_preparation_history_table tbody').empty();
    
    $('#fabric_report_daily_material_preparation_history_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        ajax: {
            type: 'GET',
            url: url
        },
        columns: [
            {data: 'created_at', name: 'created_at',searchable:false,visible:true,orderable:true,width:'100%'},
            {data: 'remark', name: 'remark',searchable:false,visible:true,orderable:true,width:'100%'},
            {data: 'qty_before', name: 'qty_before',searchable:true,visible:true,orderable:true,width:'100%'},
            {data: 'qty_after', name: 'qty_after',searchable:true,visible:true,orderable:true,width:'100%'}
        ]
    });

   var dtable = $('#fabric_report_daily_material_preparation_history_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
	});
}

function reprepare(url)
{
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$.ajax({
		type: "post",
		url: url,
		beforeSend: function () {
			swal.close();
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		success: function () {
			$.unblockUI();
		},
		error: function (response) {
			$.unblockUI();
			if (response['status'] == 422) $("#alert_warning").trigger("click", response['responseJSON']);
			if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
		}
	})
	.done(function () {
		$("#alert_success").trigger("click", 'Data successfully updated.');
		var active_tab = $('#active_tab').val();
		if(active_tab == 'reguler') $('#reguler_table').DataTable().ajax.reload();
		else $('#additional_table').DataTable().ajax.reload();
	});
}

function closeModal()
{
	var active_tab = $('#active_tab').val();
	if(active_tab == 'reguler')
	{
		var dtablRegulerTable = $('#reguler_table').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtablRegulerTable.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtablRegulerTable.search("").draw();
				}
				return;
		});
		dtablRegulerTable.draw();
		$('#select_warehouse').on('change',function(){
            dtablRegulerTable.draw();
		});
		
		$('#start_date').on('change',function(){
            dtablRegulerTable.draw();
		});
		
		$('#end_date').on('change',function(){
            dtablRegulerTable.draw();
        });

		
	}else if( active_tab == 'additional')
	{
		var dtableAdditional = $('#additional_table').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtableAdditional.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtableAdditional.search("").draw();
				}
				return;
		});
		dtableAdditional.draw();
		$('#select_warehouse').on('change',function(){
            dtableAdditional.draw();
		});
		
		$('#start_date').on('change',function(){
            dtableAdditional.draw();
		});
		
		$('#end_date').on('change',function(){
            dtableAdditional.draw();
        });
	} 
}

function unlock(url) 
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
            type: "PUT",
            url: url,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
            },
        })
        .done(function (response) {
            $("#alert_success").trigger("click", 'Data successfully unlocked');
        });
}

function recalculate(url) 
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
            type: "PUT",
            url: url,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function () {
                $.unblockUI();
            },
        })
        .done(function () {
			$("#alert_success").trigger("click", 'Data successfully recalculate');
			var active_tab = $('#active_tab').val();
			if(active_tab == 'reguler') $('#reguler_table').DataTable().ajax.reload();
			else $('#additional_table').DataTable().ajax.reload();
        });
}

function selectedMoveRoll(id)
{
	if (document.getElementById(id).checked) 
	{
		var new_material_preparation_fabric_id = $('#new_material_preparation_fabric_id').val();
		
		if(!new_material_preparation_fabric_id)
		{
			//
			$("#alert_warning").trigger("click", 'Please select new date planning first');
			$("#"+id).removeAttr("checked");
			return false;
		}

		var data = searchOldRoll(id);

		var total_reserved_qty 		= parseFloat($('#total_reserved_qty').val());
		var total_qty_rilex 		= parseFloat($('#total_qty_rilex').val());
		var new_qty_relax 			= parseFloat($('#qty_relax').val());
		var new_qty_outstanding 	= parseFloat($('#qty_outstanding').val());
		var qty_prepare_per_roll	= parseFloat(data.reserved_qty);

		if(parseFloat(new_qty_outstanding) > 0.000)
		{
			if(parseFloat(total_reserved_qty) == 0.000)
			{
				if ((qty_prepare_per_roll / parseFloat(new_qty_outstanding)) >= 1) var reserved_qty = new_qty_outstanding;
				else var reserved_qty = qty_prepare_per_roll;

				var new_total_qty_rilex = parseFloat(total_qty_rilex) - parseFloat(reserved_qty);
				

				
				var input = {
					'old_id' 							: data.id,
					'material_preparation_fabric_id' 	: data.material_preparation_fabric_id,
					'reserved_qty' 						: reserved_qty,
					
				}

				data.reserved_qty = parseFloat(qty_prepare_per_roll - reserved_qty);

				list_materials.push(input);

				var _new_new_qty_outstanding 	= parseFloat(new_qty_outstanding - reserved_qty);
				var _new_qty_relax 				= parseFloat(new_qty_relax + reserved_qty);

				$('#qty_outstanding').val(_new_new_qty_outstanding.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&'));
				$('#qty_relax').val(_new_qty_relax.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&'));
				
				$('#total_qty_rilex').val(new_total_qty_rilex.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&'));
				$('#text_total_qty_rilex').text(new_total_qty_rilex.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,')+' ');
			
				renderNewPrepare();
				renderOldPrepare();
			}else
			{
				var total_roll		= list_preparation_materials.length;
				//console.log(total_roll);
				//console.log(total_qty_rilex);
				//console.log(total_reserved_qty);
				/*if(total_qty_rilex > total_reserved_qty && total_roll > 1)
				{
					var new_qty_prepare_per_roll = parseFloat(qty_prepare_per_roll);
				}else
				{
					
				}*/

				var saldo_qty_reserved 		= parseFloat($('#saldo_qty_reserved').val());
	
				if(saldo_qty_reserved > 0)
				{
					if (saldo_qty_reserved/qty_prepare_per_roll >= 1)
					{
						var saldo_qty = parseFloat(saldo_qty_reserved);
					}
					else
					{
						var saldo_qty = parseFloat(qty_prepare_per_roll);
						$("#alert_warning").trigger("click", 'Roll with barcode '+data.barcode+' cannot move all qty because this plan has qty preparation too');
						var new_qty_prepare_per_roll = parseFloat(qty_prepare_per_roll - saldo_qty);
					}
				}else
				{
					var new_qty_prepare_per_roll = parseFloat(qty_prepare_per_roll);
				}
				

				if(parseFloat(new_qty_prepare_per_roll) > 0)
				{
					if (new_qty_prepare_per_roll / new_qty_outstanding >= 1) var reserved_qty = parseFloat(new_qty_outstanding);
					else var reserved_qty = parseFloat(new_qty_prepare_per_roll);

					//data.reserved_qty = reserved_qty;
					//console.log(reserved_qty);
					data.reserved_qty = parseFloat(qty_prepare_per_roll - reserved_qty).toFixed(4);
					var input = {
						'old_id' 							: data.id,
						'material_preparation_fabric_id' 	: data.material_preparation_fabric_id,
						'reserved_qty' 						: reserved_qty//parseFloat(qty_prepare_per_roll - reserved_qty).toFixed(4),
						
					}

					list_materials.push(input);

					//var _new_new_qty_outstanding 	= parseFloat(new_qty_outstanding - (qty_prepare_per_roll - reserved_qty).toFixed(4));
					var _new_new_qty_outstanding 	= parseFloat(new_qty_outstanding - reserved_qty);
					//var _new_qty_relax 				= parseFloat(new_qty_relax + (qty_prepare_per_roll - reserved_qty).toFixed(4));
					var _new_qty_relax 				= parseFloat(new_qty_relax + reserved_qty);

					$('#qty_outstanding').val(_new_new_qty_outstanding.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&'));
					$('#qty_relax').val(_new_qty_relax.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&'));
					$('#total_qty_rilex').val(parseFloat(total_qty_rilex - reserved_qty).toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&'));
					$('#text_total_qty_rilex').text(parseFloat(total_qty_rilex - reserved_qty).toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,')+' ');
					
					renderNewPrepare();
					renderOldPrepare();
				}else
				{
					//return false;
					
					//$("#alert_error").trigger("click", 'hubungi mas charis, ini masuk ke kondisi yg perlu di cek, cuma belum ada case jadi belum bisa di cek ');
	
					/*
					bingung ini buat apa
					data.reserved_qty = 0;
					var input = {
						'old_id' 							: data.id,
						'material_preparation_fabric_id' 	: data.material_preparation_fabric_id,
						'reserved_qty' 						: qty_prepare_per_roll,
						
					}

					list_materials.push(input);

					var _new_new_qty_outstanding 	= parseFloat(new_qty_outstanding - qty_prepare_per_roll);
					var _new_qty_relax 				= parseFloat(new_qty_relax + qty_prepare_per_roll);

					$('#qty_outstanding').val(_new_new_qty_outstanding.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&'));
					$('#qty_relax').val(_new_qty_relax.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&'));
					
					$('#total_qty_rilex').val(_new_new_qty_outstanding.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&'));
					$('#text_total_qty_rilex').text(new_total_qty_rilex.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,')+' ');*/
				}

				saldo_qty_reserved -= saldo_qty;
				$('#saldo_qty_reserved').val(saldo_qty_reserved);

				renderNewPrepare();
				renderOldPrepare();
				
			}
		}else
		{
			$("#alert_warning").trigger("click", 'there is no outstanding for this plan');
			$("#"+id).removeAttr("checked");
			return false;
		}
		
	} else 
	{
		var data_new 					= searchNewRoll(id);
		var data_old 					= searchOldRoll(id);
		var total_reserved_qty 			= parseFloat($('#total_reserved_qty').val());
		var total_qty_rilex 			= parseFloat($('#total_qty_rilex').val());
		var new_qty_relax 				= parseFloat($('#qty_relax').val());
		var new_qty_outstanding 		= parseFloat($('#qty_outstanding').val());

		if(data_new)
		{
			var qty_prepare_per_roll		= parseFloat(data_new.reserved_qty);

			var _new_new_qty_outstanding 	= parseFloat(new_qty_outstanding + qty_prepare_per_roll);
			var _new_qty_relax 				= parseFloat(new_qty_relax - qty_prepare_per_roll);
			var new_total_qty_rilex 		= parseFloat(total_qty_rilex + qty_prepare_per_roll);
	
			$('#qty_outstanding').val(_new_new_qty_outstanding.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&'));
			$('#qty_relax').val(_new_qty_relax.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&'));
			
			$('#total_qty_rilex').val(_new_new_qty_outstanding.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&'));
			$('#text_total_qty_rilex').text(_new_new_qty_outstanding.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,')+' ');
			
			list_materials.splice(data_new._id, 1);
			data_old.reserved_qty = parseFloat(data_new.reserved_qty + data_old.reserved_qty);
	
			renderNewPrepare();
			renderOldPrepare();
		}
		
	}
}

function searchOldRoll(id)
{
	for (var i in list_preparation_materials) 
	{
		var data = list_preparation_materials[i];
		if(data.id == id) return data;
	}
}

function searchNewRoll(id)
{
	for (var i in list_materials) 
	{
		var data = list_materials[i];
		if(data.old_id == id) return data;
	}
}

function renderOldPrepare() 
{
	getIndexOldPrepare();
	$('#list_preparation_materials').val(JSON.stringify(list_preparation_materials));
	console.log(list_preparation_materials);
}

function getIndexOldPrepare() 
{
	for (id in list_preparation_materials) 
	{
		list_preparation_materials[id]['_id'] 	= id;
		list_preparation_materials[id]['no'] 	= parseInt(id) + 1;
	}
}

function renderNewPrepare() 
{
	getIndexNewPrepare();
	$('#list_materials').val(JSON.stringify(list_materials));
	console.log(list_materials);
}

function getIndexNewPrepare() 
{
	for (id in list_materials) 
	{
		list_materials[id]['_id'] 	= id;
		list_materials[id]['no'] 	= parseInt(id) + 1;
	}
}

function checkedAll()
{
	for (i in list_preparation_materials) 
	{
		var data = list_preparation_materials[i];
		if(!data.is_closing)
		{
			$("#"+data.id).trigger("click");
		}
		
	}
}

function uncheckedAll()
{
	for (i in list_preparation_materials) 
	{
		var data = list_preparation_materials[i];
		if(data)
		{
			if(!data.is_closing)
			{
				if (document.getElementById(data.id).checked) 
				{
					$("#"+data.id).trigger("click");
				} 
			}
		}
		
		
		
	}
}

