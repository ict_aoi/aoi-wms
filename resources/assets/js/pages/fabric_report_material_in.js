$(function()
{
		
			regulerTables();

			var dtablRegulerTable = $('#in_table').dataTable().api();
			$(".dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtablRegulerTable.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtablRegulerTable.search("").draw();
					}
					return;
			});
			dtablRegulerTable.draw();
			$('#select_warehouse').on('change',function(){
				dtablRegulerTable.draw();
                
			});
			
			$('#start_date').on('change',function(){
				dtablRegulerTable.draw();
			});
			
			$('#end_date').on('change',function(){
				dtablRegulerTable.draw();
			});

		

	
});



function regulerTables()
{
	$('#in_table').DataTable().destroy();
	$('#in_table tbody').empty();
	
	var regulerTable =  $('#in_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        ajax: {
            type: 'GET',
			url: '/fabric/report/report-material-in/data',
			data: function(d) {
                return $.extend({}, d, {
                    "warehouse" 	: $('#select_warehouse').val(),
                    "start_date" 	: $('#start_date').val(),
                    "end_date" 		: $('#end_date').val(),
                });
            }
		},
		
		fnCreatedRow: function (row, data, index) {
			var info = regulerTable.page.info();
			var value = index+1+info.start;
			$('td', row).eq(0).html(value);
		},
        columns: [
			{data: null, sortable: false, orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at',searchable:true},
			{data: 'warehouse', name: 'warehouse'},
			{data: 'document_no', name: 'document_no',orderable:true},
			{data: 'item_code', name: 'item_code',orderable:true},
            {data: 'no_invoice', name: 'no_invoice',orderable:true},
            {data: 'no_packing_list', name: 'no_packing_list',orderable:true},
            {data: 'qty', name: 'qty',searchable:false},
			{data: 'uom', name: 'uom'},
            {data: 'source', name: 'source',orderable:true},
			
		]
    });
}

$('#export').click(function(){
	var warehouse 	= $('#select_warehouse').val();
	var start_date 	= $('#start_date').val();
	var end_date 	= $('#end_date').val();
	var url			= '/fabric/report/report-material-in/export';

	window.location.href = url + '?warehouse=' + warehouse + '&start_date=' + start_date + '&end_date=' + end_date;


})
