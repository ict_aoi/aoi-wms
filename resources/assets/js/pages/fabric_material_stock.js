$(function()
{
    var page = $('#page').val();
   
    if(page == 'index')
    {
        var flag_msg = $('#flag_msg').val();
        if (flag_msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');


        $('#material_stock_other_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            scroller:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/fabric/material-stock-other/data'
            },
            columns: [
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'created_at', name: 'created_at',searchable:false,visible:true,orderable:true},
                {data: 'user_id', name: 'user_id',searchable:false,visible:true,orderable:true},
                {data: 'type_stock', name: 'type_stock',searchable:false,visible:true,orderable:true},
                {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
                {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
                {data: 'nomor_roll', name: 'nomor_roll',searchable:true,visible:true,orderable:true},
                {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
                {data: 'color', name: 'color',searchable:false,visible:true,orderable:true},
                {data: 'batch_number', name: 'batch_number',searchable:false,visible:true,orderable:true},
                {data: 'load_actual', name: 'load_actual',searchable:false,visible:true,orderable:true},
                {data: 'uom', name: 'uom',searchable:false,visible:true,orderable:false},
                {data: 'qty_order', name: 'qty_order',searchable:false,visible:true,orderable:false},
                {data: 'available_qty', name: 'available_qty',searchable:false,visible:true,orderable:false},
                {data: 'source', name: 'source',searchable:false,visible:true,orderable:false},
                {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
            ]
        });
    
        var dtable = $('#material_stock_other_table').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();
    }else
    {
        list_materials 	= JSON.parse($('#materials').val());
        mapping_stocks 	= JSON.parse($('#mapping_stocks').val());
        
        //POSupplierLov('document_no_fabric', '/fabric/material-stock-other/create/supplier-picklist?');
        itemLov('item_out_fabric','/fabric/material-stock/create/item-picklist?')
        POSupplierLov('supplier_fabric', '/fabric/material-stock/create/supplier-picklist?');
        render();

        $('#form').submit(function (event) 
        {
            event.preventDefault();
            render();
            var url_fabric_material_stock = $('#url_fabric_material_stock').val();

            if(checkHasError())
            {
                $("#alert_error").trigger("click", 'Please check item first');
                return alert_warning;
            }

            if(checkHasSource()){
                $("#alert_warning").trigger("click", 'Please input remark first.');
                return false;
            }

            bootbox.confirm("Are you sure want to save this data ?", function (result) {
                if (result) {
                    $.ajax({
                        type: "POST",
                        url: $('#form').attr('action'),
                        data: $('#form').serialize(),
                        beforeSend: function () {
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        error: function (response) {
                            $.unblockUI();

                            if(response['status'] == 500)
                                $("#alert_error").trigger("click", 'Please contact ict');

                            if(response['status'] == 422)
                                $("#alert_error").trigger("click", response.responseJSON);

                        }
                    })
                    .done(function(response)
                    {
                        $.unblockUI();
                        $('#materials').val('[]');
                        list_materials = [];
                        render();

                        $("#alert_success").trigger("click", 'Data successfully saved.');

                        var arrStr = encodeURIComponent(JSON.stringify(response));
                        window.open('/fabric/material-stock/show-barcode?id=' + arrStr);
                        //document.location.href = url_fabric_material_stock;
                    });
                }
            });

        });

    }
});

$('#upload_button').on('click', function () 
{
    $('#upload_file').trigger('click');
});

$('#upload_file').on('change', function () 
{
    $.ajax({
        type: "post",
        url: $('#upload_file_allocation').attr('action'),
        data: new FormData(document.getElementById("upload_file_allocation")),
        processData: false,
        contentType: false,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            $("#alert_info").trigger("click", 'Upload success, please check again before save.');
            for (idx in response) {
                var data = response[idx];
                if (data.roll_number) 
                {
                    var _roll_number = data.roll_number;
                }else 
                {
                    var _roll_number = getRollNumber(data.document_no, data.item_code);
                }

                var input = {
                    'c_bpartner_id'	        : data.c_bpartner_id,
                    'supplier_code'	        : data.supplier_code,
                    'supplier_name'	        : data.supplier_name,
                    'c_order_id'	        : data.c_order_id,
                    'document_no'	        : data.document_no,
                    'item_id'		        : data.item_id,
                    'item_code'		        : data.item_code,
                    'item_desc'		        : data.item_desc,
                    'uom'			        : data.uom,
                    'category'		        : data.category,
                    'color'		            : data.color,
                    'upc'		            : data.upc,
                    'qty'			        : data.qty,
                    'batch_number' 	        : data.batch_number,
                    'roll_number'	        : _roll_number,
                    'actual_width'	        : data.actual_width,
                    'actual_lot'	        : data.actual_lot,
                    'source'		        : data.source,
                    'type_stock'	        : data.type_stock,
                    'mapping_stock_id'      : data.mapping_stock_id,
                    'type_stock_erp_code'   : data.type_stock_erp_code,
                    'is_error'              : data.is_error,
                    'remark'                : data.remark
                };

                list_materials.push(input);
            }
            
            //list_materials = response;
        },
        error: function (response) {
            $.unblockUI();
            $('#upload_file_allocation').trigger('reset');
            if (response['status'] == 500)
                $("#alert_error").trigger("click", 'Please Contact ICT.');

            if (response['status'] == 422)
                $("#alert_error").trigger("click", response.responseJSON);

        }
    })
    .done(function () {
        $('#upload_file_allocation').trigger('reset');
        render();
    });

});


$('#upload_button_split_roll').on('click', function () 
{
    $('#upload_file_split_roll').trigger('click');
});

$('#upload_file_split_roll').on('change', function () 
{
    $('#upload_form_split_roll').submit();
});

$('#select_warehouse').on('change',function(){
	var warehouse_id = $(this).val();
	$('#warehouse_id').val(warehouse_id);
});

function checkHasSource()
{
	var result	= false;
	for(var i in list_materials)
	{
		var material = list_materials[i];

		if(!material.source && !material.is_error)
		{
			material.is_warning = true;
			result = true;
			break;
		}
	}

	render();
	return result
}

function POSupplierLov(name, url) 
{
    var search      = '#' + name + 'Search';
    var list        = '#' + name + 'List';
    var item_id     = '#' + name + 'Id';
    var item_name   = '#' + name + 'Name';
    var modal       = '#' + name + 'Modal';
    var table       = '#' + name + 'Table';
    var buttonSrc   = '#' + name + 'ButtonSrc';
    var buttonDel   = '#' + name + 'ButtonDel';

    function itemAjax() 
    {
        var q = $(search).val();
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
            url: url + '&q=' + q
        })
        .done(function (data) {
            $(table).html(data);
            pagination(name);
            $(search).focus();
            $(search).val('');
            $(table).removeClass('hidden');
            $(modal).find('.shade-screen').addClass('hidden');
            $(modal).find('.form-search').removeClass('hidden');

            $(table).find('.btn-choose').on('click', chooseItem);
        });
    }

    function chooseItem() 
    {
        var id              = $(this).data('id');
        var name            = $(this).data('name');
        var supplier        = $(this).data('supplier');
        var bpartner        = $(this).data('bpartner');
        var supplier_code   = $(this).data('suppliercode');

        $(item_id).val(bpartner);
        $(item_name).val(supplier);
        $('#supplier_name').val(supplier);
        $('#c_bpartner_id').val(bpartner);
        $('#supplier_code').val(supplier_code);
    }

    function pagination() 
    {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(search).val("");
    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) 
    {
        if (e.keyCode == 13)
            itemAjax();
    });

    $(buttonDel).on('click', function () 
    {
        $(item_id).val('');
        $(item_name).val('');

    });

    itemAjax();
}

function showModalItemPickList()
{
    /*var document_no = $('#document_no_fabricName').val();
    if(document_no)
    {
        $('#item_out_fabricModal').modal();
    }else 
    {
        $("#alert_warning").trigger("click", 'Please select po supplier first.');
        return false;
    }*/

    $('#item_out_fabricModal').modal();
    
}

function itemLov(name, url)
{
    
    var search      = '#' + name + 'Search';
    var list        = '#' + name + 'List';
    var item_id     = '#' + name + 'Id';
    var item_name   = '#' + name + 'Name';
    var modal       = '#' + name + 'Modal';
    var table       = '#' + name + 'Table';
    var buttonSrc   = '#' + name + 'ButtonSrc';
    var buttonDel   = '#' + name + 'ButtonDel';

    function itemAjax() 
    {
        var q = $(search).val();
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
            url: url + '&q=' + q
        })
        .done(function (data) {
            $(table).html(data);
            pagination(name);
            $(search).focus();
            $(search).val('');
            $(table).removeClass('hidden');
            $(modal).find('.shade-screen').addClass('hidden');
            $(modal).find('.form-search').removeClass('hidden');

            $(table).find('.btn-choose').on('click', chooseItem);
        });
    }

    function chooseItem() 
    {
        var id      = $(this).data('id');
        var code    = $(this).data('code');
        var name    = $(this).data('name');
        var color   = $(this).data('color');
        var upc     = $(this).data('upc');

        $(item_id).val(id);
        $(item_name).val(code);
        $('#item_desc').val(name);
        $('#color').val(color);
        $('#upc').val(upc);
        $('#category').val('FB');
        $('#uom').val('YDS');
    }

    function pagination() 
    {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(search).val("");
    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) 
    {
        if (e.keyCode == 13)
            itemAjax();
    });

    $(buttonDel).on('click', function () 
    {
        $(item_id).val('');
        $(item_name).val('');

    });

    itemAjax();
}

function getRollNumber(document_no, item_code)
{
    if (list_materials.length == 0)return 1;
        
    let _nomor_roll = 1;

    for (idx in list_materials) 
    {
        let data = list_materials[idx];
        if (data.document_no == document_no && data.item_code == item_code)
        {
            _nomor_roll =  parseInt(data.roll_number) + 1;
        }
    }
    return _nomor_roll;
}

function render() 
{
    getIndex();
    $('#materials').val(JSON.stringify(list_materials));

    var tmpl = $('#material-stock-fabric-table').html();
    Mustache.parse(tmpl);
    var data = { item: list_materials };
    var html = Mustache.render(tmpl, data);
    $('#tbody-material-stock-fabric').html(html);

    console.log(data);
    bind();
}

function getIndex() 
{
    for (idx in list_materials) 
    {
        list_materials[idx]['_id'] 	= idx;
        list_materials[idx]['no'] 	= parseInt(idx) + 1;
    }
}

function bind() 
{
    $('#AddItemButton').on('click', tambahItem);
    $('.btn-delete-item').on('click', deleteItem);
    $('.input-source-edit').on('change', changeSource);
    $('#qty').on('change', tambahQty);
    $('#item_out_fabricButtonLookup').on('click',showModalItemPickList);
    $('#item_out_fabricName').on('click',showModalItemPickList);

    $('.input-new').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            tambahItem();
        }
    });

    $('.input-edit').keypress(function (e) {
        var i = $(this).data('id');

        if (e.keyCode == 13) {
            e.preventDefault();
            $('#simpan_' + i).click();
        }
    });

    $('.input-number').keypress(function (e) {
        if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
            return false;
        }
        return true;
    });
    
}

function tambahQty() 
{
    var qty = $('#qty').val();
    if(parseFloat(qty) < 1) 
    {
        $("#alert_warning").trigger("click", 'Qty cannot be less than 1.');
        $('#qty').val('');
        return false;
    }
    
    $('#qty').val(parseFloat(qty).toFixed(2));
    return;
}

function changeSource()
{
	var i 		= $(this).data('id');
	var data 	= list_materials[i];

	var value	= $('#sourceInput_'+i).val();
    data.source = value.trim();
    if(value) data.is_warning = false;
	$('#materials').val(JSON.stringify(list_materials));
}


function tambahItem() 
{
    let c_bpartner_id   	= 'FREE STOCK';//$('#c_bpartner_id').val();
    let supplier_name 	    = 'FREE STOCK';//$('#supplier_name').val();
    let c_order_id 		    = 'FREE STOCK';//$('#document_no_fabricId').val();
    let document_no 	    = 'FREE STOCK';//$('#document_no_fabricName').val();
 
    let item_id 		    = $('#item_out_fabricId').val();
    let item_code 		    = $('#item_out_fabricName').val();
    let item_desc 		    = $('#item_desc').val();
    let uom 			    = $('#uom').val();
    let color 			    = $('#color').val();
    let upc 			    = $('#upc').val();
    let qty 			    = $('#qty').val();
  
    let supplier_code 		= $('#supplier_code').val();
    let category 		    = $('#category').val();
    let batch_number 	    = $('#batch_number').val();
    let roll_number 	    = $('#nomor_roll').val();
    let actual_width 	    = $('#actual_width').val();
    let actual_lot 		    = $('#actual_lot').val();
    let source 			    = $('#source').val();
    let _roll_number 	    = roll_number;
    let type_stocks		    = $('#type_stock').val();
    let type_stock		    = null;
    let mapping_stock_id    = null;
    let type_stock_erp_code =null;

    
    if (!roll_number) _roll_number = getRollNumber(document_no, item_code);
    
    
    if (!item_id)
    {
        $("#alert_warning").trigger("click", 'Please select item first.');
        return false;
    }

    if (!qty) 
    {
        $("#alert_warning").trigger("click", 'Please type qty first.');
        return false;
    }

    if(parseFloat(qty) < 1) 
    {
        $("#alert_warning").trigger("click", 'Qty cannot be less than 1.');
        return;
    }

    //get selected value from combo box type stock
    if(type_stocks == 0)
    {
        if(document_no == 'FREE STOCK')
        {
            type_stock 			= 'REGULER';
            type_stock_erp_code = '2';
        }else
        {
            let get_4_digit_from_beginning  = document_no.substr(0,4);
            let mapping_stock               = getMappingStock(get_4_digit_from_beginning);

            if(mapping_stock!=null)
            {
                type_stock			        = mapping_stock.type_stock;
                type_stock_erp_code         = mapping_stock.type_stock_erp_code;
            }else
            {
                let get_5_digit_from_beginning = document_no.substr(0,5);
                mapping_stock 				   = getMappingStock(get_5_digit_from_beginning);
                if(mapping_stock)
                {
                    type_stock			        = mapping_stock.type_stock;
                    type_stock_erp_code         = mapping_stock.type_stock_erp_code;
                }else
                {
                    type_stock 			        = 'REGULER';
                    type_stock_erp_code         = '2';
                }
            }
        }
    }else
    {
        _type_stock 		 = $('#type_stock option:selected').text();
        _type_stock_erp_code = type_stocks;
        type_stock 			 = _type_stock;
        type_stock_erp_code  = _type_stock_erp_code;
    }

    var input = {
        'c_bpartner_id'	        : c_bpartner_id,
        'supplier_code'	        : supplier_code,
        'supplier_name'	        : supplier_name,
        'c_order_id'	        : c_order_id,
        'document_no'	        : document_no,
        'item_id'		        : item_id,
        'item_code'		        : item_code,
        'item_desc'		        : item_desc,
        'uom'			        : uom,
        'category'		        : category,
        'color'		            : color,
        'upc'		            : upc,
        'qty'			        : qty,
        'batch_number' 	        : batch_number,
        'roll_number'	        : _roll_number,
        'actual_width'	        : actual_width,
        'actual_lot'	        : actual_lot,
        'source'		        : source,
        'type_stock'	        : type_stock,
        'mapping_stock_id'      : mapping_stock_id,
        'type_stock_erp_code'   : type_stock_erp_code,
        'is_error'              : false,
        'is_warning'            : false,
        'remark'                : 'ready to save'
    };

    list_materials.push(input);

    render();
}

function deleteItem() 
{
    var i = $(this).data('id');

    list_materials.splice(i, 1);
    render();
}

function getMappingStock(document_no)
{
	let findResult = null
    mapping_stocks.forEach(i => 
    {
		let _document_no = i.document_number;
        if(document_no == _document_no)
        {
			findResult = i;
		}
	});

	return findResult;
}

function checkHasError()
{
	let findResult = false
	list_materials.forEach(i => {
		if(i.is_error == true){
			findResult = true;
			//break loop
			return false;
		}
	});

	return findResult;
}
