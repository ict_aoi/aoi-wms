$(function()
{
    var page = $('#page').val();
    if(page == 'index')
    {
        $('#fabric_report_material_inspect_lab_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            ajax: {
                type: 'GET',
                url: '/fabric/report/material-inspect-lab/data',
                data: function(d) {
                    return $.extend({}, d, {
                        "warehouse"     : $('#select_warehouse').val(),
                        "start_date"    : $('#start_date').val(),
                        "end_date"      : $('#end_date').val(),
                        "status"        : $('#select_status').val(),
                    });
               }
            },
            columns: [
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'warehouse_id', name: 'warehouse_id',searchable:false,visible:true,orderable:true},
                {data: 'receive_date', name: 'receive_date',searchable:true,visible:true,orderable:true},
                {data: 'no_invoice', name: 'no_invoice',searchable:true,visible:true,orderable:true},
                {data: 'is_handover', name: 'is_handover',searchable:true,visible:true,orderable:true},
                {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
                {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
                {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
                {data: 'style', name: 'style',searchable:true,visible:true,orderable:true},
                {data: 'color', name: 'color',searchable:true,visible:true,orderable:true},
                {data: 'total_roll', name: 'total_roll',searchable:false,visible:true,orderable:true},
                {data: 'total_yard', name: 'total_yard',searchable:false,visible:true,orderable:true},
                {data: 'total_inspect_lab_roll', name: 'total_inspect_lab_roll',searchable:false,visible:true,orderable:true},
                {data: 'jumlah_batch_number', name: 'jumlah_batch_number',searchable:true,visible:true,orderable:true},
                {data: 'jumlah_lot_actual', name: 'jumlah_lot_actual',searchable:true,visible:true,orderable:true},
                {data: 'batch_number', name: 'batch_number',searchable:true,visible:true,orderable:true},
                {data: 'lot_actual', name: 'lot_actual',searchable:true,visible:true,orderable:true},
                {data: 'action', name: 'action',searchable:false,visible:true,orderable:true},
            ]
        });
    
        var dtable = $('#fabric_report_material_inspect_lab_table').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();
        
        $('#select_warehouse').on('change',function(){
            dtable.draw();
        });
    
        $('#start_date').on('change',function(){
            dtable.draw();
        });
    
        $('#end_date').on('change',function(){
            dtable.draw();
        });

        $('#select_status').on('change', function(){
            dtable.draw();
        });
    }else
    {
        list_lot = JSON.parse($('#list_lot').val());

        var url_fabric_material_inspect_lab_detail_data = $('#url_fabric_material_inspect_lab_detail_data').val();
        $('#fabric_report_material_inspect_lab_detail_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:-1,
            ajax: {
                type: 'GET',
                url: url_fabric_material_inspect_lab_detail_data
            },
            columns: [
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'barcode_supplier', name: 'barcode_supplier',searchable:true,visible:true,orderable:true},
                {data: 'inspect_lab_date', name: 'inspect_lab_date',searchable:false,visible:true,orderable:true},
                {data: 'nomor_roll', name: 'nomor_roll',searchable:true,visible:true,orderable:true},
                {data: 'locator_id', name: 'locator_id',searchable:false,visible:true,orderable:false},
                {data: 'batch_number', name: 'batch_number',searchable:true,visible:true,orderable:true},
                {data: 'load_actual', name: 'load_actual',searchable:true,visible:true,orderable:true},
                {data: 'inspect_lot_result', name: 'inspect_lot_result',searchable:true,visible:true,orderable:true},
                {data: 'uom', name: 'uom',searchable:false,visible:true,orderable:false},
                {data: 'qty_arrival', name: 'qty_arrival',searchable:false,visible:true,orderable:false},
                {data: 'user_lab_id', name: 'user_lab_id',searchable:false,visible:true,orderable:false},
                {data: 'inspect_lab_remark', name: 'inspect_lab_remark',searchable:false,visible:true,orderable:false},
                
            ]
        });
    
        var dtable = $('#fabric_report_material_inspect_lab_detail_table').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();

        render();
    }


    $('#form').submit(function (event) 
	{
		event.preventDefault();
        
        if (list_lot.length == 0) 
		{
			$("#alert_warning").trigger("click", 'There no data to need update');
			return false;
		}


		bootbox.confirm("Are you sure want to update the lot ?.", function (result) 
		{
			if (result) 
			{
				$.ajax({
					type: "POST",
					url: $('#form').attr('action'),
					data: $('#form').serialize(),
					beforeSend: function () 
					{
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () 
					{
						$.unblockUI();
					},
					success: function () 
					{
						list_lot = [];
						render();
					},
					error: function (response) 
					{
						$.unblockUI();
						$('#barcode_value').val('');
						$('.barcode_value').focus();

						if (response['status'] == 500)
							$("#alert_error").trigger("click", 'Please Contact ICT');


						if (response['status'] == 422)
							$("#alert_warning").trigger("click", response.responseJSON);

					}
				})
				.done(function (response){
                    $("#alert_success").trigger("click", 'Update lot successfully.');
                    $('#fabric_report_material_inspect_lab_detail_table').DataTable().ajax.reload();
				});
			}
		});



	});
});

function render() 
{
    $('#list_lot').val(JSON.stringify(list_lot));
}

function checkDataExists(id,lot_actual,result, inspect_lab_remark) 
{
	for (var i in list_lot) 
	{
		var data = list_lot[i];

		if (data.id == id) 
		{

            data.lot_actual = lot_actual;
            data.result     = result;
            data.inspect_lab_remark = inspect_lab_remark;
			return true;
		}

	}

	return false;
}

function changelot(id)
{
    var lot_actual              = $('#load_'+id).val();
    var select_status           = $('#id_'+id).val();
    var hidden_load             = $('#hidden_load_'+id).val();
    var inspect_lab_remark      = $('#remark_'+id).val();
    console.log(inspect_lab_remark);

    if(lot_actual)
    {
        var checkItem = checkDataExists(id,lot_actual,select_status,inspect_lab_remark);

        if(!checkItem)
        {
            var input = {
                'id'            : id,
                'lot_actual'    : lot_actual,
                'result'        : select_status,
                'inspect_lab_remark' : inspect_lab_remark,
            };
    
            list_lot.push(input);
        }
        
        render();
    }else
    {
        $("#alert_warning").trigger("click",'Lot cannot be empthy');
        $('#load_'+id).val(hidden_load);
    }
}

// function changeremark(id)
// {
//     var remark      = $('#load_'+id).val();
//     var hidden_load     = $('#hidden_load_'+id).val();

//     if(lot_actual)
//     {
//         var checkItem = checkDataExists(id,remark,select_status);

//         if(!checkItem)
//         {
//             var input = {
//                 'id'            : id,
//                 'remark'    : remark,
//                 'result'        : select_status,
//             };
    
//             list_lot.push(input);
//         }
        
//         render();
//     }else
//     {
//         $("#alert_warning").trigger("click",'Lot cannot be empthy');
//         $('#load_'+id).val(hidden_load);
//     }
// }
