$(function()
{
    $('#daily_material_arrival_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/fabric/report/daily-material-arrival/data',
            data: function(d) {
                return $.extend({}, d, {
                    "warehouse"     : $('#select_warehouse').val(),
                    "start_date"    : $('#start_date').val(),
                    "end_date"      : $('#end_date').val(),
                });
           }
        },
        columns: [
            {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
            {data: 'arrival_date', name: 'arrival_date',searchable:false,visible:true,orderable:true},
            {data: 'warehouse_name', name: 'warehouse_name',searchable:false,visible:true,orderable:true},
            {data: 'user_receive', name: 'user_receive',searchable:true,visible:true,orderable:true},
            {data: 'no_invoice', name: 'no_invoice',searchable:true,visible:true,orderable:true},
            {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
            {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'color', name: 'color',searchable:false,visible:true,orderable:true},
            {data: 'total_roll', name: 'total_roll',searchable:false,visible:true,orderable:false},
            {data: 'total_yard', name: 'total_yard',searchable:false,visible:true,orderable:false},
            {data: 'jenis_po', name: 'jenis_po',searchable:false,visible:true,orderable:false},
            {data: 'source', name: 'source',searchable:false,visible:true,orderable:false},
        ]
    });

    var dtable = $('#daily_material_arrival_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();
    
    $('#select_warehouse').on('change',function(){
        dtable.draw();
    });

    $('#start_date').on('change',function(){
        dtable.draw();
    });

    $('#end_date').on('change',function(){
        dtable.draw();
    });
})