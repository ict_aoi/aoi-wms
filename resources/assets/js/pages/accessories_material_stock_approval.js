$(function()
{
	$('.input-number').keypress(function (e) {
		if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
			return false;
		}
		return true;
	});

	var warehouse_id = $('#select_warehouse').val();
	$('#warehouse_id').val(warehouse_id);
	locatorPicklist('locator',warehouse_id,'/accessories/material-stock-approval/locator-picklist?');

	var approvalStockTable =  $('#accessories_material_stock_approval_table').DataTable({
		dom: 'Bfrtip',
		processing: true,
		serverSide: true,
		pageLength:-1,
		ajax: {
			type: 'GET',
			url: '/accessories/material-stock-approval/data',
			data: function(d) {
				return $.extend({}, d, 
					{
					"warehouse"     : $('#select_warehouse').val(),
				});
			}
		},
		fnCreatedRow: function (row, data, index) {
            var info = approvalStockTable.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
		columns: [
			{data: null, sortable: false, orderable: false, searchable: false},
			{data: 'checkbox', name: 'checkbox',searchable:false,orderable:false},
			{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
			{data: 'material_stock_id', name: 'material_stock_id',searchable:true,visible:false,orderable:false},
			{data: 'created_at', name: 'created_at',searchable:true,visible:true,orderable:true},
			{data: 'created_by', name: 'created_by',searchable:true,visible:true,orderable:true},
			{data: 'warehouse_name', name: 'warehouse_name',searchable:true,visible:true,orderable:true},
			{data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
			{data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
			{data: 'po_buyer', name: 'po_buyer',searchable:true,visible:true,orderable:true},
			{data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
			{data: 'item_code_source', name: 'item_code_source',searchable:true,visible:true,orderable:true},
			{data: 'category', name: 'category',searchable:true,visible:true,orderable:true},
			{data: 'source', name: 'source',searchable:true,visible:true,orderable:true},
			{data: 'qty', name: 'qty',searchable:false,visible:true,orderable:false},
			{data: 'remark', name: 'remark',searchable:true,visible:true,orderable:false},
			{data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
		]
	});
	
	var dtable = $('#accessories_material_stock_approval_table').dataTable().api();
	$(".dataTables_filter input")
		.unbind() // Unbind previous default bindings
		.bind("keyup", function (e) { // Bind our desired behavior
			// If the user pressed ENTER, search
			if (e.keyCode == 13) {
				// Call the API search function
				dtable.search(this.value).draw();
			}
			// Ensure we clear the search if they backspace far enough
			if (this.value == "") {
				dtable.search("").draw();
			}
			return;
	});
	dtable.draw();

	$('#select_warehouse').on('change',function(){
		warehouse_id = $('#select_warehouse').val();
		$('#warehouse_id').val(warehouse_id);
        dtable.draw();
	});
	
	$('#form').submit(function (event) 
	{
		event.preventDefault();
		selectedApproval()
		var list_detail_material_stock = $('#list_detail_material_stock').val();

		if (!list_detail_material_stock) {
		$("#alert_warning").trigger("click", 'Tidak ada item yang dipilih');
		return false;
		}

		bootbox.confirm("Are you sure want to approve this material ?.", function (result) 
		{
			if (result) {
				$.ajax({
					type: "POST",
					url: $('#form').attr('action'),
					data: $('#form').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function () {
						$.unblockUI();
						$('#form').trigger('reset');
						$("#alert_success").trigger("click", 'Data successfully approved');
					},
					error: function (response) {
						$.unblockUI();
						if (response['status'] == 500)
							$("#alert_error").trigger("click", 'Please contact ICT');


						if (response['status'] == 422 || response['status'] == 400)
							$("#alert_warning").trigger("click", response.responseJSON);
					}
				})
				.done(function () 
				{
					$('#accessories_material_stock_approval_table').DataTable().ajax.reload();
				});
			}
		});

	});

	$('#form-approve-all').submit(function (event) 
	{
		event.preventDefault();

		bootbox.confirm("Are you sure want to approve all ?.", function (result) 
		{
			if (result) {
				$.ajax({
					type: "POST",
					url: $('#form-approve-all').attr('action'),
					data: $('#form-approve-all').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function () {
						$.unblockUI();
						$('#form-approve-all').trigger('reset');
						$("#alert_success").trigger("click", 'Data successfully approved');
					},
					error: function (response) {
						$.unblockUI();
						if (response['status'] == 500)
							$("#alert_error").trigger("click", 'Please contact ICT');


						if (response['status'] == 422 || response['status'] == 400)
							$("#alert_warning").trigger("click", response.responseJSON);
					}
				})
				.done(function () 
				{
					$('#accessories_material_stock_approval_table').DataTable().ajax.reload();
				});
			}
		});

	});

});

$('#select_warehouse').change(function () 
{
	var warehouse_id = $(this).val();
	locatorPicklist('from_rackin',warehouse_id, '/accessories/material-stock-approval/rack-picklist?');
});


function locatorPicklist(name,warehouse_id, url) 
{
	var search 		= '#' + name + 'Search';
	var item_id 	= '#' + name + 'Id';
	var item_name 	= '#' + name + 'Name';
	var modal 		= '#' + name + 'Modal';
	var table 		= '#' + name + 'Table';
	var buttonSrc 	= '#' + name + 'ButtonSrc';
	var buttonDel 	= '#' + name + 'ButtonDel';

	function itemAjax(){
		var q = $(search).val();
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&q=' + q +'&warehouse_id=' + warehouse_id
		})
		.done(function (data) {
			$(table).html(data);
			pagination(name);
			$(search).focus();
			$(search).val('');
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem()
	{
		var id 			= $(this).data('id');
		var name 		= $(this).data('name');
		
		$(item_id).val(id);
		$(item_name).val(name);
	}

	function pagination()
	{
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	$(buttonDel).on('click', function () {
		$(item_id).val('');
		$(item_name).val('');

	});

	itemAjax();
}

/*$('#approvalall_from').submit(function (event) 
{
	event.preventDefault();
	bootbox.confirm("Are you sure want to approve all data ?", function (result) {
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				type: "POST",
				url: $('#approvalall_from').attr('action'),
				data: {
					'status':'all'
				},
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function () {
					$.unblockUI();
					$("#alert_success").trigger("click", 'Data successfully approved.');

				},
				error: function (response) {
					$.unblockUI();
					if (response['status'] == 500)
						$("#alert_error").trigger("click", 'Please Contact ICT');
					
					if(response['status'] == 422)
						$("#alert_error").trigger("click", response.responseJSON);
				}
			})
			.done(function () {
				$('#accessories_material_stock_approval_table').DataTable().ajax.reload();
			});
		}
	});
});*/

$('#updateformnya').submit(function (event)
{
	var remark_update	= $('#remark_update').val();
	var qty_adjustment 	= $('#qty_adjustment').val();

	if(!qty_adjustment){
		$("#alert_warning").trigger("click", 'Qty can\'t be empty ');
		return false;
	}

	if(!remark_update){
		$("#alert_warning").trigger("click", 'Please insert remark update first ');
		return false;
	}

	event.preventDefault();
	bootbox.confirm("Are you sure want to edit and approve this data?", function (result) {
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				type: "post",
				url: $('#updateformnya').attr('action'),
				data:  $('#updateformnya').serialize(),
				beforeSend: function () {
					$('#updateModal').modal('hide');
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function () {
					$.unblockUI();
					$('#updateformnya').trigger('reset');
					$("#alert_success").trigger("click", 'Data successfully approved.');

				},
				error: function (response) {
					$('#updateModal').modal();
					$.unblockUI();
					
					if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
					if(response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);
				}
			})
			.done(function () {
				$('#accessories_material_stock_approval_table').DataTable().ajax.reload();
			});
		}
	});
});

function approve(url) 
{
	bootbox.confirm("Are you sure want to approve this data ?", function (result) 
	{
		if (result) 
		{
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: "PUT",
				url: url,
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function (response) {
					$.unblockUI();
				},
				error: function (response) {
					$.unblockUI();
					if(response['status'] == 422) $("#alert_error").trigger("click", response.responseJSON);
					if (response['status'] == 400) {
						for (i in response['responseJSON']['errors']) {
							$('#' + i + '_error').addClass('has-error');
							$('#' + i + '_danger').text(response['responseJSON']['errors'][i]);
						}
					} if (response['status'] == 500) {
						$("#alert_error").trigger("click", 'Please contact ICT');
					}
				}
			})
			.done(function ($result) {
				$("#alert_success").trigger("click", 'Data successfully approved.');
				$('#accessories_material_stock_approval_table').DataTable().ajax.reload();
			});
		}
	});
}

function reject(url) 
{
	bootbox.confirm("Are you sure want to reject this data ?", function (result) 
	{
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: "PUT",
				url: url,
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function (response) {
					$.unblockUI();
				},
				error: function (response) {
					$.unblockUI();
					if (response['status'] == 400) {
						for (i in response['responseJSON']['errors']) {
							$('#' + i + '_error').addClass('has-error');
							$('#' + i + '_danger').text(response['responseJSON']['errors'][i]);
						}
					} if (response['status'] == 500) {
						$("#alert_error").trigger("click", 'Please contact ICT');
					}
				}
			})
			.done(function ($result) {
				$("#alert_success").trigger("click", 'Data successfully rejected');
				$('#accessories_material_stock_approval_table').DataTable().ajax.reload();
			});
		}
	});
}

function edit(url)
{
	$.ajax({
		url: url,
		beforeSend: function () {
			$.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
		},
		success: function () {
			$.unblockUI();
		},
		error: function (response) 
		{
			$.unblockUI();
			
			if (response['status'] == 500) $("#alert_error").trigger('click','Please contact ICT');
			if (response['status'] == 422) $("#alert_warning").trigger('click', response.responseJSON);
		}
	})
	.done(function (data) {
		$('#supplier_name').val(data.supplier_name);
		$('#document_no').val(data.document_no);
		$('#item_code').val(data.item_code);
		$('#uom').val(data.uom);
		$('#note').text(data.remark);
		$('#qty_adjustment').val(data.qty);
		$('#table_id').val(data.table_id);
		$('#table_name').val(data.table_name);
		$('#locatorId').val('');
		$('#locatorName').val('');
		
		$('#updateModal').modal();
	});
}

function selectedApproval()
{
	var list_detail_material_stock = new Array();
	$("#accessories_material_stock_approval_table input[type=checkbox]:checked").each(function (){
		list_detail_material_stock.push(this.id);
	});
	$('#list_detail_material_stock').val(list_detail_material_stock);
}


