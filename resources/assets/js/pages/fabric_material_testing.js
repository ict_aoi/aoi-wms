list_barcode_header = JSON.parse($('#barcode-header').val());

$(function () 
{
    render();

    $('#form').submit(function (event) 
	{
		event.preventDefault();

		if (list_barcode_header.length == 0) {
			$("#alert_warning").trigger("click", 'Please scan barcode first.');
			return false;
		}

		bootbox.confirm("Are you sure want to save this ?.", function (result) 
		{
			if (result) {
				$.ajax({
						type: "POST",
						url: $('#form').attr('action'),
						data: $('#form').serialize(),
						beforeSend: function () {
							$.blockUI({
								message: '<i class="icon-spinner4 spinner"></i>',
								overlayCSS: {
									backgroundColor: '#fff',
									opacity: 0.8,
									cursor: 'wait'
								},
								css: {
									border: 0,
									padding: 0,
									backgroundColor: 'transparent'
								}
							});
						},
						complete: function () {
							$.unblockUI();
						},
						success: function (response) {
							$('#barcode_value').val('');
							$('.barcode_value').focus();
						},
						error: function (response) {
							$.unblockUI();
							$('#barcode_value').val('');
							$('.barcode_value').focus();

							if (response['return'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
							if (response['return'] == 422) $("#alert_warning").trigger("click", response.responseJSON);

						}
					})
					.done(function (response) {
						$("#alert_success").trigger("click", 'Data successfully saved.');
						list_barcode_header = [];
						render();
						renderDefect();
						setFocusToTextBox();
					});
			}
		});
	});
});

function render() 
{
	getIndex();
	$('#barcode-header').val(JSON.stringify(list_barcode_header));
	var tmpl = $('#reject-table').html();
	Mustache.parse(tmpl);
	var data = {
		item: list_barcode_header
	};
	var html = Mustache.render(tmpl, data);
	$('#tbody-reject').html(html);
	bind();
}

function tambahItem() 
{
	var warehouse_id 	= $('#select_warehouse').val();
	var barcode 		= $('#barcode_value').val();

	var url_fabric_material_fabric_testing_create 	= $('#url_fabric_material_fabric_testing_create').val();
	var diff 										= checkItem(barcode);

	if (!diff) 
	{
		$("#alert_warning").trigger("click", 'Barcode already scanned.');
		$("#barcode_value").val("");
		$(".barcode_value").focus();
		return;
	}

	$.ajax({
			type: "GET",
			url: url_fabric_material_fabric_testing_create,
			data: {
				barcode			: barcode,
				warehouse_id	: warehouse_id
			},
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			complete: function () {
				$.unblockUI();
			},
			success: function (response) {
				list_barcode_header.push(response);
				$('#barcode_value').val('');
				$('.barcode_value').focus();
			},
			error: function (response) {
				$.unblockUI();
				$('#barcode_value').val('');
				$('.barcode_value').focus();

				if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
				if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);
			}
		})
		.done(function () {
			render();
			$('.barcode_value').focus();

		});;
}

function checkItem(barcode_val, index) {
	let n = barcode_val.includes("-");

	if (n)
		var _temp = barcode_val.split("-")[0];
	else
		var _temp = barcode_val;
	for (var i in list_barcode_header) {
		var barcode = list_barcode_header[i];
		if (i == index)
			continue;

		if (barcode.barcode == _temp) {
			return false;
		}

	}
	return true;
}

function bind() 
{
	$('#barcode_value').on('change', tambahItem);
	$('.btn-remove-row').on('click', removeRow);
	$('.test-method-options').on('change', selectMethod);
	$('.subtest-method-options').on('change', selectSubMethod);
	$('.input-number').keypress(function (e) 
	{
		let charCode = (e.which) ? e.which : e.keyCode;
		if ((charCode >= 46 && charCode <= 57) || (charCode >= 37 && charCode <= 40) || charCode == 9 || charCode == 8 || charCode == 46) {
			return true;
		}
		return false;
	});

	$('.input-edit').keypress(function (e) 
	{
		let i = $(this).data('id');

		if (e.keyCode == 13) {
			e.preventDefault();
			$('#simpan_' + i).click();
		}
	});
}

function removeRow() 
{
	let id = $(this).attr('data-id');
	for (let i = list_barcode_header.length - 1; i >= 0; i--) 
	{
		if (i == id) {
			list_barcode_header.splice(i, 1);
		}
	}
	render();
}

function getIndex() 
{
	for (id in list_barcode_header) {
		list_barcode_header[id]['_id'] = id;
		list_barcode_header[id]['no'] = parseInt(id) + 1;


		for (let idy in list_barcode_header[id]['status_options']) {
			if (list_barcode_header[id]['status'] == list_barcode_header[id]['status_options'][idy]['id']) {
				list_barcode_header[id]['status_options'][idy]['selected'] = 'selected';
			} else {
				list_barcode_header[id]['status_options'][idy]['selected'] = '';
			}

		}

		let details = list_barcode_header[id]['detail'];

		for (let idx in details) {
			details[idx]['_idx'] = idx;
			details[idx]['nox'] = parseInt(idx) + 1;
		}
	}
}

function selectMethod()
{
	let i = $(this).data('id');
	let selected_item = $('#status_option_' + i).val();
	console.log(selected_item);
	$.ajax({
		type: "GET",
		url: 'material-fabric-testing/get-subtesting/'+selected_item,
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) {
			list_barcode_header.push(response);
			$('#barcode_value').val('');
			$('.barcode_value').focus();
		},
		error: function (response) {
			$.unblockUI();
			$('#barcode_value').val('');
			$('.barcode_value').focus();

			if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
			if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);
		}
	})
	.done(function () {
		render();
		$('.barcode_value').focus();

	});;
	
}

function selectSubMethod()
{

}