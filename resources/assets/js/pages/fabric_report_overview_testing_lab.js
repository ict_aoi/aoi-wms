
$(function()
{
    $('#developTesting , #bulkTesting , #reorderTesting , #reTest ').hide();
    var flag_msg = $('#flag_msg').val();
	if (flag_msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');
	else if (flag_msg == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated.');

    
    
    var url   = $('#url_data').val();
    $('#master_data_fabric_testing_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        deferRender:true,
        scrollX:true,
        ajax: {
            type: 'GET',
            url: url,
        },
        
        columns: [
            {data: 'created_at', name: 'created_at',searchable:false,visible:true,orderable:true},
            {data: 'test_required', name: 'test_required',searchable:false,visible:true,orderable:true},
            {data: 'date_information_remark', name: 'date_information_remark',searchable:false,visible:true,orderable:true},
            {data: 'buyer', name: 'buyer',searchable:false,visible:true,orderable:true},
            {data: 'lab_location', name: 'lab_location',searchable:false,visible:true,orderable:true},
            {data: 'trf_id', name: 'trf_id',searchable:true,visible:true,orderable:true},
            {data: 'nik', name: 'nik',searchable:false,visible:true,orderable:true},
            {data: 'orderno', name: 'orderno',searchable:true,visible:true,orderable:true},
            {data: 'category', name: 'category',searchable:false,visible:true,orderable:true},
            {data: 'category_specimen', name: 'category_specimen',searchable:false,visible:true,orderable:true},
            {data: 'type_specimen', name: 'type_specimen',searchable:false,visible:true,orderable:true},
            {data: 'return_test_sample', name: 'return_test_sample',searchable:false,visible:true,orderable:true},
            {data: 'status', name: 'status',searchable:true,visible:true,orderable:true},
            {data: 'verified_lab_date', name: 'verified_lab_date',searchable:true,visible:true,orderable:true},
            {data: 't1_result', name: 't1_result',searchable:true,visible:true,orderable:true},
            {data: 'result_t2', name: 'result_t2',searchable:true,visible:true,orderable:true},
            {data: 'remarks', name: 'remarks',searchable:true,visible:true,orderable:true},
            // {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
	    ]
    });

    var dtable = $('#master_data_fabric_testing_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();



    
})


