$(function()
{
    $('#report_material_ila_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/accessories/report/material-ila/data',
            data: function(d) {
                return $.extend({}, d, {
                    "po_buyer"     : $('#po_buyerName').val(),
                });
           }
        //     data: function(d) {
        //         return $.extend({}, d, {
        //             "warehouse"     : $('#select_warehouse').val(),
        //             "start_date"    : $('#start_date').val(),
        //             "end_date"      : $('#end_date').val(),
        //         });
        //    }
        },
        columns: [
            {data: 'last_movement_date', name: 'created_at',searchable:true,visible:true,orderable:false},
            {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
            {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
            {data: 'po_buyer', name: 'po_buyer',searchable:true,visible:true,orderable:true},
            {data: 'style', name: 'style',searchable:true,visible:true,orderable:true},
            {data: 'article_no', name: 'article_no',searchable:true,visible:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'item_desc', name: 'item_desc',searchable:true,visible:true,orderable:true},
            {data: 'category', name: 'category',searchable:true,visible:true,orderable:true},
            {data: 'uom_conversion', name: 'uom_conversion',searchable:false,visible:true,orderable:false},
            {data: 'qty_need', name: 'qty_need',searchable:false,visible:true,orderable:false},
            {data: 'qty_movement', name: 'qty_movement',searchable:false,visible:true,orderable:false},
            {data: 'last_status_movement', name: 'last_status_movement',searchable:true,visible:true,orderable:false},
            {data: 'warehouse', name: 'warehouse',searchable:false,visible:true,orderable:false}
	    ]
    });

    var dtable = $('#report_material_ila_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();
    
    // $('#select_warehouse').on('change',function(){
    //     dtable.draw();
    // });

    // $('#start_date').on('change',function(){
    //     dtable.draw();
    // });

    // $('#end_date').on('change',function(){
    //     dtable.draw();
    // });

    buyerLov('po_buyer', 'material-ila/po-buyer-picklist?');
    
    function buyerLov(name, url) 
    {
        var search = '#' + name + 'Search';
        var item_id = '#' + name + 'Id';
        var item_name = '#' + name + 'Name';
        var modal = '#' + name + 'Modal';
        var table = '#' + name + 'Table';
        var buttonSrc = '#ButtonSrc';
        var buttonDel = '#' + name + 'ButtonDel';
        var is_cancel = $('#is_cancel_order').val();

        function itemAjax() 
        {
            var q = $(search).val();
            
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');
            $(modal).find('.form-search').addClass('hidden');

            $.ajax({
                url: url + '&q=' + q
            })
            .done(function (data) 
            {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');

                $(table).find('.btn-choose').on('click', chooseItem);
            });
        }

        function chooseItem() 
        {
            var id      = $(this).data('id');
            var name    = $(this).data('name');
            
            $(item_id).val(id);
            $(item_name).val(name);
            dtable.draw();
        }

        function pagination() 
        {
            $(modal).find('.pagination a').on('click', function (e) {
                var params = $(this).attr('href').split('?')[1];
                url = $(this).attr('href') + (params == undefined ? '?' : '');

                e.preventDefault();
                itemAjax();
            });
        }

        //$(search).val("");
        $(buttonSrc).unbind();
        $(search).unbind();

        $(buttonSrc).on('click', itemAjax);

        $(search).on('keypress', function (e) 
        {
            if (e.keyCode == 13)
                itemAjax();
        });

        $(buttonDel).on('click', function () 
        {
            $(item_id).val('');
            $(item_name).val('');

        });

        itemAjax();
    }

});