
$(function()
{
	list_sync_per_items = JSON.parse($('#sync_per_items').val());
	render();
});

$('#upload_button').on('click', function () 
{
	$('#upload_file').trigger('click');
});

$('#upload_file').on('change', function () 
{
	$.ajax({
		type: "post",
		url: $('#upload_file_sync').attr('action'),
		data: new FormData(document.getElementById("upload_file_sync")),
		processData: false,
		contentType: false,
		beforeSend: function () {
			list_sync_per_items = [];
			$.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) {
			$("#alert_info").trigger("click", 'Upload Remove From Dashboard successfully.');
			for(idx in response){
				var input = response[idx];
				list_sync_per_items.push(input);
			}
		},
		error: function (response) {
			$.unblockUI();
			$('#upload_file_sync').trigger('reset');
			if (response['status'] == 500)
				$("#alert_error").trigger("click", 'Please Contact ICT.');

			if (response['status'] == 422)
				$("#alert_error").trigger("click", response.responseJSON);

		}
	})
	.done(function () {
		$('#upload_file_sync').trigger('reset');
		render();
	});

})

function render() 
{
	getIndex();
	$('#sync_per_items').val(JSON.stringify(list_sync_per_items));
	var tmpl = $('#sync-per-item-table').html();
	Mustache.parse(tmpl);
	var data = { item: list_sync_per_items };
	var html = Mustache.render(tmpl, data);
	$('#tbody-sync-per-item').html(html);
}

function getIndex() 
{
	for (idx in list_sync_per_items) {
		list_sync_per_items[idx]['_id'] = idx;
		list_sync_per_items[idx]['no'] = parseInt(idx) + 1;
	}
}
