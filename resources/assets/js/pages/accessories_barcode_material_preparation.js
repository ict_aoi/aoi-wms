list_material_preparation = JSON.parse($('#material_preparations').val());

$(function()
{
	$('#document_material_preparationName').click(function () {
		var warehouse_id = $('#select_warehouse').val();
		PreparationLov('document_material_preparation','item_material_preparation',warehouse_id, '/accessories/barcode/preparation/critria-picklist?');
	});

	$('#document_material_preparationButton').click(function () {
		var warehouse_id = $('#select_warehouse').val();
		PreparationLov('document_material_preparation', 'item_material_preparation',warehouse_id, '/accessories/barcode/preparation/critria-picklist?');
	});

	$('#btn_check_all').click(function(){
		var total = list_material_preparation.length;
		if (total == 0){
			$("#alert_info_2").trigger("click", 'Data is empty');
			return false;
		}else
		{
			for (id in list_material_preparation) 
			{
				list_material_preparation[id].selected = true;
			}
			render();
		}


	});

	$('#btn_uncheck_all').click(function () 
	{
		var total = list_material_preparation.length;
		if (total == 0) 
		{
			$("#alert_info_2").trigger("click", 'Data is empty');
			return false;
		}else
		{
			for (id in list_material_preparation) {
				list_material_preparation[id].selected = false;
			}
			render();
		}
	});

	function PreparationLov(name, name2,warehouse_id, url) 
	{
		var search 			= '#' + name + 'Search';
		var search2 		= '#' + name2 + 'Search2';
		var list 			= '#' + name + 'List';
		var item_id 		= '#' + name + 'Id';
		var item_name 		= '#' + name + 'Name';
		var modal 			= '#' + name + 'Modal';
		var table 			= '#' + name + 'Table';
		var buttonSrc 		= '#ButtonSrc';
		var buttonDel 		= '#' + name + 'ButtonDel';

		function itemAjax() 
		{
			var q = $(search).val();
			var q2 = $(search2).val();

			$(table).addClass('hidden');
			$(modal).find('.shade-screen').removeClass('hidden');
			$(modal).find('.form-search').addClass('hidden');

			$.ajax({
				url: url + '&warehouse_id=' + warehouse_id + '&document_no=' + q + '&item_code=' + q2
			})
			.done(function (data) {
				$(table).html(data);
				pagination(name);
				$(search).focus();
				$(table).removeClass('hidden');
				$(modal).find('.shade-screen').addClass('hidden');
				$(modal).find('.form-search').removeClass('hidden');

				$(table).find('.btn-choose').on('click', chooseItem);
			});
		}

		function chooseItem() 
		{
			var c_bpartner_id 	= $(this).data('partner');
			var document_no 	= $(this).data('document');
			var item_code 		= $(this).data('item');
			var status 			= $(this).data('status');

			$('#c_bpartner_id').val(c_bpartner_id);
			$('#document_no').val(document_no);
			$('#item_code').val(item_code);
			$('#status').val(status);
			$('#form_store_barcode').submit();
		}

		function pagination() {
			$(modal).find('.pagination a').on('click', function (e) {
				var params = $(this).attr('href').split('?')[1];
				url = $(this).attr('href') + (params == undefined ? '?' : '');

				e.preventDefault();
				itemAjax();
			});
		}

		//$(search).val("");
		$(buttonSrc).unbind();
		$(search).unbind();
		$(search2).unbind();

		$(buttonSrc).on('click', itemAjax);

		$(search).on('keypress', function (e) {
			if (e.keyCode == 13)
				itemAjax();
		});

		$(search2).on('keypress', function (e) {
			if (e.keyCode == 13)
				itemAjax();
		});

		$(buttonDel).on('click', function () {
			$(item_id).val('');
			$(item_name).val('');

		});

		itemAjax();
	}

	function render()
	{
		getIndex();
		$('#material_preparations').val(JSON.stringify(list_material_preparation));
		var tmpl = $('#preparation-table').html();
		Mustache.parse(tmpl);
		var data = { item: list_material_preparation };
		var html = Mustache.render(tmpl, data);
		$('#tbody-preparation').html(html);
		isOtherBuyerSupplied();
		bind();
	}

	function getIndex()
	{
		for (id in list_material_preparation)
		{
			list_material_preparation[id]['_id'] = id;
			list_material_preparation[id]['no'] = parseInt(id)+1;
		}
	}

	function isOtherBuyerSupplied() 
	{
		for (var id in list_material_preparation) {
			var data = list_material_preparation[id];
			if(data.is_handover){
				var barckground_color = '#34eba5';
				var color = 'white';
			}else if (data.is_other_buyer_checkout) {
				var barckground_color = '#333333';
				var color = 'white';
			}else if(data.is_backlog){
				var barckground_color = '#ffff99';
				var color = 'black';
			}else if(data.is_remaining){
				var barckground_color = '#3498db';
				var color = 'white';
			}else if(data.is_error || data.is_po_buyer_cancel){
				var barckground_color = '#f54336';
				var color = 'white';
			}else{
				var barckground_color = 'white';
				var color = 'black';
			}

			$('#panel_'+ id).css('background-color', barckground_color);
			$('#panel_'+ id).css('color', color);

		}
	}

	function bind() 
	{
		$('.checkbox_item').on('click', checkedItem);
	}

	function checkedItem() 
	{
		var id = $(this).data('id');
		
		if ($('#check_'+id).is(':checked')) var checked = true;
		else var checked = false;
		
		list_material_preparation[id].selected = checked;
		
		render();
	}

	function checkItem(document_no, item_code)
	{
		for (var i in list_material_preparation) {
			var data = list_material_preparation[i];
			if ((document_no != '' && document_no != null)  && (item_code == null || item_code == '')){
				if (data.document_no == document_no) {
					return false;
				}
			}else{
				if (data.document_no == document_no && data.item_code == item_code) {
					return false;
				}
			}
		}
		return true;
	}
	
	$('#form_store_barcode').submit(function (event) 
	{
		event.preventDefault();
		var item_code 						= $('#item_code').val()
		var document_no 					= $('#document_no').val()
		
		var diff = checkItem(document_no, item_code);

		if (!diff)
		{
			$("#alert_warning").trigger("click", 'Data already exists');
			return false;
		}

		$.ajax({
			type: "POST",
			url: $('#form_store_barcode').attr('action'),
			data: $('#form_store_barcode').serialize(),
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			complete: function () 
			{
				$.unblockUI();
			},
			success: function (response) 
			{
				for (var i in response) 
				{
					var input = response[i];
					list_material_preparation.push(input);
				}
			},
			error: function (response) 
			{
				$.unblockUI();
				$('#c_bpartner_id').val('');
				$('#item_code').val('');
				$('#c_bpartner_id').val('');
				$('#status').val('');
				
				if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');

				if (response['status'] == 422)
					$("#alert_error").trigger("click", response['responseJSON'])
			}
		})
		.done(function () 
		{
			$('#po_buyer').val('');
			$('#item_code').val('');
			$('#document_no').val('');
			$('#status').val('');
			render();
		});
	});

	$('#form').submit(function (event) 
	{
		event.preventDefault();
		bootbox.confirm("Are you sure want to print this barcode ?.", function (result) 
		{
			if (result) {
				$.ajax({
					type: "POST",
					url: $('#form').attr('action'),
					data: $('#form').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function (response) 
					{
						if (response['flag'] == 1)
							$("#alert_success").trigger("click", 'Material Berhasil di Receive');
						else if (response['flag'] == 0)
							$("#alert_error").trigger("click", response['message']);

						$('#document_material_preparationName').val('');
						$('#document_material_preparationId').val('');
						$('#item_material_preparationName').val('');
						$('#item_material_preparationId').val('');
					},
					error: function (response) {
						$.unblockUI();
						if (response['status'] == 500 )
							$("#alert_error").trigger("click", 'Please Contact ICT');

						if(response['status'] == 422){
							$("#alert_error").trigger("click", response.responseJSON)
						}
					}
				})
				.done(function (response) {
					list_material_preparation = [];
					render();
					
					$('#list_barcodes').val(JSON.stringify(response));
					$('#getBarcode').submit();
					$('#list_barcodes').val('');
					
				});
			}
		});
	});
	render();

	
});

$('#select_warehouse').on('change',function(){
	$('#warehouse_id').val($(this).val());
});

