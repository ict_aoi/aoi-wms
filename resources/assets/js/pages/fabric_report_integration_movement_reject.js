$(function()
{
    $('#select_warehouse').trigger('change');
    var fabric_report_material_integration_movement_reject_table = $('#fabric_report_material_integration_movement_reject_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/fabric/report/integration-movement-reject/data',
            data: function(d) {
                return $.extend({}, d, {
                    "warehouse"     : $('#select_warehouse').val(),
                });
           }
        },
        fnCreatedRow: function (row, data, index) {
            var info = fabric_report_material_integration_movement_reject_table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'material_movement_id', name: 'material_movement_id',searchable:true,visible:true,orderable:false},
            {data: 'material_movement_line_id', name: 'material_movement_line_id',searchable:true,visible:false,orderable:false},
            {data: 'warehouse_id', name: 'warehouse_id',searchable:false,visible:true,orderable:true},
            {data: 'created_at', name: 'created_at',searchable:false,visible:true,orderable:true},
            {data: 'integration_date', name: 'integration_date',searchable:false,visible:true,orderable:false},
            {data: 'no_packing_list', name: 'no_packing_list',searchable:true,visible:true,orderable:true},
            {data: 'no_invoice', name: 'no_invoice',searchable:true,visible:true,orderable:true},
            {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'nomor_roll', name: 'nomor_roll',searchable:true,visible:true,orderable:true},
            {data: 'qty', name: 'qty',searchable:false,visible:true,orderable:false},
            {data: 'note', name: 'note',searchable:false,visible:true,orderable:false},
        ]
    });

    var dtable = $('#fabric_report_material_integration_movement_reject_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();
    
    $('#select_warehouse').on('change',function(){
        console.log('adasd');
        dtable.draw();
    });

    $('#form').submit(function (event) 
	{
        event.preventDefault();
        var document_movement_id = $('#document_movement_id').val();

        if(!document_movement_id)
        {
            $("#alert_warning").trigger("click", 'Please insert document movement id first');
            return false;
        }

        $('#reintegrateModal').modal('hide');
		bootbox.confirm("Are you sure want to re-integrate this movement "+document_movement_id+" ?.", function (result) 
		{
			if (result) 
			{
				$.ajax({
					type: "POST",
					url: $('#form').attr('action'),
					data: $('#form').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					error: function (response) 
					{
						$.unblockUI();
						
						if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
                        if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);
                    }
				})
				.done(function ()
				{
                    $("#alert_success").trigger("click", 'Movement Successfully Re-integrated');
                    $('#form').trigger("reset");
                    $('#fabric_report_material_integration_movement_reject_table').DataTable().ajax.reload();
				});
			}
		});
	});
});