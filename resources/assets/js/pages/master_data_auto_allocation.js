$(function()
{
	POSupplierLov('document_no', '/master-data/auto-allocation/get-po-supplier-picklist?');
	itemLov('item', '/master-data/auto-allocation/get-item-picklist?');
	itemLov('item_code_source', '/master-data/auto-allocation/get-item-picklist?');
	var page	 = $('#page').val();

	if(page == 'index')
	{
		var flag_msg = $('#flag_msg').val();
		if (flag_msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');

		$('#active_tab').on('change',function(){
			var active_tab = $('#active_tab').val();
			if(active_tab == 'inactive')
			{
				inactiveDataTables();
	
				var dtable = $('#inactiveTable').dataTable().api();
				$(".dataTables_filter input")
					.unbind() // Unbind previous default bindings
					.bind("keyup", function (e) { // Bind our desired behavior
						// If the user pressed ENTER, search
						if (e.keyCode == 13) {
							// Call the API search function
							dtable.search(this.value).draw();
						}
						// Ensure we clear the search if they backspace far enough
						if (this.value == "") {
							dtable.search("").draw();
						}
						return;
				});
				dtable.draw();
	
				
			}else if( active_tab == 'active')
			{
				activeDataTables();
	
				var dtable = $('#activeTable').dataTable().api();
				$(".dataTables_filter input")
					.unbind() // Unbind previous default bindings
					.bind("keyup", function (e) { // Bind our desired behavior
						// If the user pressed ENTER, search
						if (e.keyCode == 13) {
							// Call the API search function
							dtable.search(this.value).draw();
						}
						// Ensure we clear the search if they backspace far enough
						if (this.value == "") {
							dtable.search("").draw();
						}
						return;
				});
				dtable.draw();
			}else if(active_tab == 'additional') 
			{
				additionalDataTables();
	
				var dtable = $('#additionalTable').dataTable().api();
				$(".dataTables_filter input")
					.unbind() // Unbind previous default bindings
					.bind("keyup", function (e) { // Bind our desired behavior
						// If the user pressed ENTER, search
						if (e.keyCode == 13) {
							// Call the API search function
							dtable.search(this.value).draw();
						}
						// Ensure we clear the search if they backspace far enough
						if (this.value == "") {
							dtable.search("").draw();
						}
						return;
				});
				dtable.draw();
			}
		});
	
		var active_tab = $('#active_tab').val();
		if(active_tab == 'inactive')
		{
			inactiveDataTables();
	
			var dtable = $('#inactiveTable').dataTable().api();
			$(".dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtable.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtable.search("").draw();
					}
					return;
			});
			dtable.draw();
	
			
		}else if( active_tab == 'active')
		{
			activeDataTables();
	
			var dtable = $('#activeTable').dataTable().api();
			$(".dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtable.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtable.search("").draw();
					}
					return;
			});
			dtable.draw();
		}else if(active_tab == 'additional')
		{
			additionalDataTables();
	
			var dtable = $('#additionalTable').dataTable().api();
			$(".dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtable.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtable.search("").draw();
					}
					return;
			});
			dtable.draw();
		}
	}else
	{
		list_new_auto_allocations = JSON.parse($('#new_auto_allocations').val());
		render();
	}
});

$('#upload_button').on('click', function () 
{
	$('#upload_file').trigger('click');
});

$('#upload_file').on('change', function () 
{
	//$('#upload_file_allocation').submit();
	$.ajax({
		type: "post",
		url: $('#upload_file_allocation').attr('action'),
		data: new FormData(document.getElementById("upload_file_allocation")),
		processData: false,
		contentType: false,
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) {
			$("#alert_info_2").trigger("click", 'Upload successfully, please check again before save.');
			for(idx in response){
				var input = response[idx];

				var check = checkIsExists(input.document_no,input.c_bpartner_id,input.item_code,input.po_buyer,input.qty_allocation,input.warehouse);
				if(check == 0){
					list_new_auto_allocations.push(input);
				}


				
			}
			
		},
		error: function (response) {
			$.unblockUI();
			$('#upload_file_allocation').trigger('reset');
			if (response['status'] == 500)
				$("#alert_error").trigger("click", 'Please Contact ICT.');

			if (response['status'] == 422)
				$("#alert_error").trigger("click", response.responseJSON);

		}
	})
	.done(function () {
		$('#upload_file_allocation').trigger('reset');
		render();
	});

})

$('#updateformnya').submit(function (event) 
{
	event.preventDefault();
	var update_qty_allocation 	= $('#update_qty_allocation').val();
	var update_qty_allocated 	= $('#update_qty_allocated').val();
	var update_qty_outstanding 	= $('#update_qty_outstanding').val();
	var cancel_reason 			= $('#cancel_reason').val();

	if(!update_qty_allocation || !update_qty_allocated || !update_qty_outstanding)
	{
		$("#alert_warning").trigger("click", 'Qty cant be empty.');
		return false;
	}

	if(!cancel_reason)
	{
		$("#alert_warning").trigger("click", 'Please insert reason first.');
		return false;
	}

	$('#updateModal').modal('hide');
	bootbox.confirm("Are you sure want to save this data ?", function (result) 
	{
		if (result) {
			$.ajax({
				type: "POST",
				url: $('#updateformnya').attr('action'),
				data: $('#updateformnya').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				complete: function () {
					$.unblockUI();
				},
				success: function (response) {
					var data = response.response;
					console.log(data);
					var notif = response.data;
					console.log(notif);
					if(notif.status == '200')
					{
						$('#updateformnya').trigger('reset');
						$("#alert_success").trigger("click", 'Data successfully updated');
						
						var active_tab = $('#active_tab').val();
						if(active_tab == 'active') $('#activeTable').DataTable().ajax.reload();
						else if(active_tab == 'additional') $('#additionalTable').DataTable().ajax.reload();
	
					}else{
						$.unblockUI();
						$("#alert_error").trigger("click", 'Please Contact ICT');
						$('#updateModal').modal();
					}
				
				},
				error: function (response) {
					var notif = response.data;
					$.unblockUI();
					$("#alert_error").trigger("click", 'Please Contact ICT');
					$('#updateModal').modal();
				}
			});
		}
	});
});

$('#form').submit(function (event) {
	event.preventDefault();
	var url_auto_allocation_index = $('#url_auto_allocation_index').attr('href');

	bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?", function (result) {
		if (result) {
			$.ajax({
				type: "POST",
				url: $('#form').attr('action'),
				data: $('#form').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function () {
					//$.unblockUI();
					//$('#form').trigger('reset');
					//list_new_auto_allocations = [];
					//render();
					document.location.href = url_auto_allocation_index;
				},
				error: function (response) {
					$.unblockUI();
					
					if(response['status'] == 500)
						$("#alert_error").trigger("click", 'Please Contact ICT');
					
					if(response['status'] == 422)
						$("#alert_error").trigger("click", response.responseJSON);
						
				}
			});
		}
	});
	
});

$('#update_qty_allocation').on('change',changeOutstanding);


function render() 
{
	getIndex();
	$('#new_auto_allocations').val(JSON.stringify(list_new_auto_allocations));
	var tmpl = $('#auto-allocation-table').html();
	Mustache.parse(tmpl);
	var data = { item: list_new_auto_allocations };
	var html = Mustache.render(tmpl, data);
	$('#tbody-auto-allocation').html(html);

	bind();
}

function getIndex() 
{
	for (idx in list_new_auto_allocations) {
		list_new_auto_allocations[idx]['_id'] = idx;
		list_new_auto_allocations[idx]['no'] = parseInt(idx) + 1;
	}
}

function bind() 
{
	$('#AddItemButton').on('click', tambahItem);
	$('.btn-delete-item').on('click', deleteItem);

	$('.input-new').keypress(function (e) {
		if (e.keyCode == 13) {
			e.preventDefault();
			tambahItem();
		}
	});

	$('.input-edit').keypress(function (e) {
		var i = $(this).data('id');

		if (e.keyCode == 13) {
			e.preventDefault();
			$('#simpan_' + i).click();
		}
	});

	$('.input-number').keypress(function (e) {
		if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
			return false;
		}
		return true;
	});

	$('#po_buyerName').click(function () {
		var supplier = $('#c_bpartner_id').val();
		var item_code = $('#itemName').val();
		var type_stock = $('#type_stock').val();

		if(!supplier)
		{
			$("#alert_warning").trigger("click", 'Silahkan pilih supplier terlebih dahulu.');
			return false;
		}

		if(!item_code){
			$("#alert_warning").trigger("click", 'Silahkan pilih item terlebih dahulu.');
			return false;
		}
		
		PoBuyerLov('po_buyer',item_code,type_stock,'/master-data/auto-allocation/get-po-buyer-picklist?');
	});
	
	$('#po_buyerButtonLookup').click(function () {
		var supplier = $('#c_bpartner_id').val();
		var item_code = $('#itemName').val();
		var type_stock = $('#type_stock').val();

		if(!supplier)
		{
			$("#alert_warning").trigger("click", 'Silahkan pilih supplier terlebih dahulu.');
			return false;
		}

		if(!item_code){
			$("#alert_warning").trigger("click", 'Silahkan pilih item terlebih dahulu.');
			return false;
		}
		
		PoBuyerLov('po_buyer',item_code,type_stock,'/master-data/auto-allocation/get-po-buyer-picklist?');
	});
}

function checkIsExists(document_no,c_bpartner_id,item_code,po_buyer,qty_allocation,warehouse_id)
{
	var flag = 0;
	for (idx in list_new_auto_allocations) {
		var data = list_new_auto_allocations[idx];
		if(document_no == data.document_no && c_bpartner_id == data.c_bpartner_id && item_code == data.item_code && po_buyer == data.po_buyer && qty_allocation == data.qty_allocation && warehouse_id == data.warehouse) flag++;
	}

	return flag;
}

function tambahItem()
{
	var booking_number 		= $('#booking_number').val();
	var document_no 		= $('#document_noName').val();
	var supplier_name 		= $('#supplier_name').val();
	var c_bpartner_id 		= $('#c_bpartner_id').val();
	var promise_date 		= $('#promise').val();
	var c_order_id 			= $('#document_noId').val();
	var item_id 			= $('#itemId').val();
	var item_code 			= $('#itemName').val();
	var item_code_source 	= $('#item_code_sourceName').val();
	var item_desc 			= $('#item_desc').val();
	var category 			= $('#category').val();
	var po_buyer 			= $('#po_buyerName').val();
	var type_stock 			= $('#type_stock').val();
	var type_stock_erp_code = $('#type_stock_erp').val();
	var uom 				= $('#uom').val();
	var season 				= $('#season').val();
	var lc_date 			= $('#lc_date').val();
	var qty_allocation 		= $('#qty_allocation').val();
	var warehouse 			= $('#warehouse').val();
	var type_stock_po_buyer = $('#type_stock_po_buyer').val();
	var type_stock_erp_po_buyer = $('#type_stock_erp_po_buyer').val();

	if(warehouse == '1000002') var _warehouse_name = 'Accessories AOI-1';
	else if(warehouse == '1000013') var _warehouse_name = 'Accessories AOI-2';
	else if(warehouse == '1000011') var _warehouse_name = 'Fabric AOI-2';
	else if(warehouse == '1000001') var _warehouse_name = 'Fabric AOI-1';

	if(!document_no || !supplier_name || !c_bpartner_id || !item_code || !item_desc  || !category  || !po_buyer  || !uom || !qty_allocation || !warehouse){
		$("#alert_warning").trigger("click", 'All field must be filled.');
		return false;
	}

	if(item_code_source) var _item_code_source = item_code_source;
	else var _item_code_source = item_code;

	var check = checkIsExists(document_no,c_bpartner_id,item_code,po_buyer,qty_allocation,warehouse);
	if(check == 0){
		var input = 
		{
			id 					: null,
			c_order_id 			: c_order_id,
			type_stock 			: type_stock_po_buyer,
			type_stock_erp_code : type_stock_erp_po_buyer,
			booking_number 		: booking_number,
			lc_date 			: lc_date,
			season 				: season,
			article_no 			: null,
			promise_date 		: promise_date,
			c_bpartner_id 		: c_bpartner_id,
			supplier_name 		: supplier_name,
			document_no 		: document_no,
			item_id 			: item_id,
			item_code_source 	: _item_code_source,
			item_code 			: item_code,
			item_desc 			: item_desc,
			category 			: category,
			po_buyer 			: po_buyer,
			old_po_buyer 		: po_buyer,
			uom 				: uom,
			qty_allocation 		: qty_allocation,
			qty_outstanding 	: 0,
			warehouse_name 		: _warehouse_name,
			warehouse 			: warehouse,
			is_additional 		: false,
			remark_additional 	: null,
			error_upload 		: false,
			reason 				: null,
			order_by 			: 2,
			remark 			: 'READY TO INSERT',
			
			
		}

		list_new_auto_allocations.push(input);
		render();
	}
	
}

function deleteItem()
{
	var i = $(this).data('id');
	list_new_auto_allocations.splice(i, 1);
	render();
}

function edit(url) 
{
	$.ajax({
		url: url,
		beforeSend: function () {
			$.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
		},
		success: function (response) {
			$.unblockUI();
		},
	})
	.done(function (data) {
		var url_master_data_auto_allocation_update = $('#url_master_data_auto_allocation_update').val();

		if(data['qty_outstanding'] == 0)
		{
			$("#alert_warning").trigger("click", 'This data cant be edit because there is nothing outstanding, if yout want to edit please do cancel auto allocation first.');
			return false;
		}

		$('#updateformnya').attr('action', url_master_data_auto_allocation_update);
		$('#update_id').val(data['id']);
		$('#update_booking_number').val(data['document_allocation_number']);
		$("#update_type_stock").val(data['type_stock_erp_code']).trigger('change');
		$("#update_warehouse_id").val(data['warehouse_id']).trigger('change');
		$('#update_po_buyer').val(data['po_buyer']);
		$('#document_noName').val(data['document_no']);
		$('#_update_qty_allocation').val(data['qty_allocation']);
		$('#_update_qty_outstanding').val(data['qty_outstanding']);
		$('#update_qty_allocation').val(data['qty_allocation']);
		$('#update_qty_allocated').val(data['qty_allocated']);
		$('#item_code_sourceName').val(data['item_code_source']);
		$('#update_item_code_book').val(data['item_code']);
		$('#update_qty_outstanding').val(data['qty_outstanding']);
		if(data['document_no'] == 'FREE STOCK') $('#c_bpartner_id').val('FREE STOCK');
		else $('#c_bpartner_id').val(data['c_bpartner_id']);
		$('#supplier_name').val(data['supplier_name']);

		$('#updateModal').modal();
	});
}

function recalculate(url) 
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "put",
        url: url,
        beforeSend: function () {
            swal.close();
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            if (response['status'] == 422)
                $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
        .done(function () {
            $("#alert_success").trigger("click", 'Recalculate successfully.');
			var active_tab = $('#active_tab').val();
			if(active_tab == 'active') $('#activeTable').DataTable().ajax.reload();
			else if(active_tab == 'additional') $('#additionalTable').DataTable().ajax.reload();
        });
}

function changeOutstanding()
{
	var qty_allocated = $('#update_qty_allocated').val();
	var qty_allocation = $('#update_qty_allocation').val();
	var qty_outstanding = qty_allocation - qty_allocated;

	if(qty_outstanding >= 0){
		$('#update_qty_outstanding').val(qty_outstanding);
	} else{
		var _update_qty_allocation = $('#_update_qty_allocation').val();
		var _update_qty_outstanding = $('#_update_qty_outstanding').val();
		$('#update_qty_allocation').val(_update_qty_allocation);
		$('#update_qty_outstanding').val(_update_qty_outstanding);
		$("#alert_warning").trigger("click", 'QTY Tidak bisa diubah menjadi '+qty_allocation+' dikerenakan menghasilkan data minus');
		return false;
	}
}

function POSupplierLov(name, url) 
{
	var search = '#' + name + 'Search';
	var list = '#' + name + 'List';
	var item_id = '#' + name + 'Id';
	var item_name = '#' + name + 'Name';
	var modal = '#' + name + 'Modal';
	var table = '#' + name + 'Table';
	var buttonSrc = '#' + name + 'ButtonSrc';
	var buttonDel = '#' + name + 'ButtonDel';

	function itemAjax() {
		var q = $(search).val();
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&q=' + q
		})
		.done(function (data) {
			$(table).html(data);
			pagination(name);
			$(search).focus();
			//$(search).val('');
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem() {
		var id = $(this).data('id');
		var name = $(this).data('name');
		var supplier = $(this).data('supplier');
		var bpartner = $(this).data('bpartner');
		var typestockerp = $(this).data('typestockerp');
		var typestock = $(this).data('typestock');
		
		$(item_id).val(id);
		$(item_name).val(name);
		$('#supplier_name').val(supplier);
		$('#c_bpartner_id').val(bpartner);
		$('#type_stock').val(typestock);
		$('#type_stock_erp').val(typestockerp);
		


	}

	function pagination() {
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	//$(search).val("");
	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	$(buttonDel).on('click', function () {
		$(item_id).val('');
		$(item_name).val('');

	});

	itemAjax();
}

function itemLov(name, url) {
	var search = '#' + name + 'Search';
	var list = '#' + name + 'List';
	var item_id = '#' + name + 'Id';
	var item_name = '#' + name + 'Name';
	var modal = '#' + name + 'Modal';
	var table = '#' + name + 'Table';
	var buttonSrc = '#' + name + 'ButtonSrc';
	var buttonDel = '#' + name + 'ButtonDel';
	
	function itemAjax() {
		var q = $(search).val();
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&q=' + q
		})
		.done(function (data) {
			$(table).html(data);
			pagination(name);
			$(search).focus();
			$(search).val('');
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem() {
		var id = $(this).data('id');
		var code = $(this).data('code');
		var item_desc_name = $(this).data('name');
		var uom = $(this).data('uom');
		var category = $(this).data('category');
		
		$(item_id).val(id);
		$(item_name).val(code);
		if(name == 'item_code_source'){
			$('#item_source_id').val(id);
			$('#item_source_desc').val(item_desc_name);
			$('#category_source').val(category);
			$('#uom_source').val(uom);
		}else{	
			$('#item_id').val(id);
			$('#item_desc').val(item_desc_name);
			$('#category').val(category);
			$('#uom').val(uom);
		}
		
		
	}

	function pagination() {
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	$(search).val("");
	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	$(buttonDel).on('click', function () {
		$(item_id).val('');
		$(item_name).val('');

	});

	itemAjax();
}

function PoBuyerLov(name,item_code,type_stock, url){
	var search = '#' + name + 'Search';
	var list = '#' + name + 'List';
	var item_id = '#' + name + 'Id';
	var item_name = '#' + name + 'Name';
	var modal = '#' + name + 'Modal';
	var table = '#' + name + 'Table';
	var buttonSrc = '#' + name + 'ButtonSrc';
	var buttonDel = '#' + name + 'ButtonDel';
	
	function itemAjax() {
		var q = $(search).val();
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&q=' + q + '&item_code=' + item_code + '&type_stock=' + type_stock
		})
		.done(function (data) {
			$(table).html(data);
			pagination(name);
			$(search).focus();
			$(search).val('');
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem() {
		var id = $(this).data('id');
		var name = $(this).data('name');
		var lc_date = $(this).data('lc');
		var season = $(this).data('season');
		var type_stock = $(this).data('typestock');
		var type_stock_erp_code = $(this).data('typestockerpcode');
		var promise = $(this).data('promise');
		
		$(item_id).val(id);
		$(item_name).val(name);

		$('#promise').val(promise);
		$('#lc_date').val(lc_date);
		$('#season').val(season);
		$('#type_stock_po_buyer').val(type_stock);
		$('#type_stock_erp_po_buyer').val(type_stock_erp_code);
		
	}

	function pagination() {
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	$(search).val("");
	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	$(buttonDel).on('click', function () {
		$(item_id).val('');
		$(item_name).val('');

	});

	itemAjax();
}

function hapus(url) {
	swal({
		title: "Apakah anda yakin akan menghapus alokasi ini ?",
		text: "Tulisankan alasan anda:",
		type: "input",
		showCancelButton: true,
		confirmButtonColor: "#2196F3",
		closeOnConfirm: false,
		animation: "slide-from-top",
		inputPlaceholder: "Silahkan tuliskan alasan anda"
	},
	function (inputValue) {
		if (inputValue === false) return false;
		if (inputValue === "") {
			swal.showInputError("Bro dan Sist belum menuliskan alasannya, tulis dlu laah !");
			return false
		}
		
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
			type: "post",
			url: url,
			data: {
				'cancel_reason': inputValue
			},
			beforeSend: function () {
				swal.close();
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function () {
				$.unblockUI();
			},
			error: function (response) {
				$.unblockUI();
				if (response['status'] == 422) $("#alert_info_2").trigger("click", response.responseJSON);
			}
		})
		.done(function () {
			$("#alert_success").trigger("click", 'Data successfully cancelled.');
			var active_tab = $('#active_tab').val();
			if(active_tab == 'active') $('#activeTable').DataTable().ajax.reload();
			else if(active_tab == 'additional') $('#additionalTable').DataTable().ajax.reload();
		});
	});
}

function allocating(url){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$.ajax({
		type: "post",
		url: url,
		beforeSend: function () {
			swal.close();
			$.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
		},
		success: function () {
			$.unblockUI();
		},
		error: function (response) {
			$.unblockUI();
			if (response['status'] == 422) $("#alert_info_2").trigger("click", response.responseJSON);
		}
	})
	.done(function () {
		$("#alert_success").trigger("click", 'Data successfully allocated.');
		var active_tab = $('#active_tab').val();
		if(active_tab == 'active') $('#activeTable').DataTable().ajax.reload();
		else if(active_tab == 'additional') $('#additionalTable').DataTable().ajax.reload();
	});
}

function cancel(url){
	swal({
		title: "Are you sure want to cancel this allocation ?",
		text: "Reason:",
		type: "input",
		showCancelButton: true,
		confirmButtonColor: "#2196F3",
		closeOnConfirm: false,
		animation: "slide-from-top",
		inputPlaceholder: "Please type reason here"
	},
	function (inputValue) {
		if (inputValue === false) return false;
		if (inputValue === "") {
			swal.showInputError("Please type reason here !");
			return false
		}
		
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
			type: "post",
			url: url,
			data: {
				'cancel_reason': inputValue
			},
			beforeSend: function () {
				swal.close();
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function () {
				$.unblockUI();
			},
			error: function (response) {
				$.unblockUI();
				if (response['status'] == 422) $("#alert_error").trigger("click", response.responseJSON);
			}
		})
		.done(function () {
			$("#alert_success").trigger("click", 'Data successfully canceled.');
			var active_tab = $('#active_tab').val();
			if(active_tab == 'active') $('#activeTable').DataTable().ajax.reload();
			else if(active_tab == 'additional') $('#additionalTable').DataTable().ajax.reload();
		});
	});
}

function history(url){
	$('#historyModal').modal();
    function itemAjax(){
        $('#historyTable').addClass('hidden');
        $('#historyModal').find('.shade-screen').removeClass('hidden');
    
        $.ajax({
            url: url,
        })
        .done(function (data) {
            $('#historyTable').html(data);
            $('#historyTable').removeClass('hidden');
            $('#historyModal').find('.shade-screen').addClass('hidden');
            pagination('detail');
        });
    }

    function pagination() {
        $('#detailModal').find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }
    itemAjax();
}

function changeTab(status){
	$('#active_tab').val(status).trigger('change');
}

function activeDataTables()
{
	$('#activeTable').DataTable().destroy();
	$('#activeTable tbody').empty();

	$('#activeTable').DataTable({
		dom: 'Bfrtip',
		processing: true,
		serverSide: true,
		pageLength:100,
		ajax: {
			type: 'GET',
			url: '/master-data/auto-allocation/active',
		},
		columns: [
			{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
			{data: 'lc_date', name: 'lc_date',searchable:false,orderable:false},
			{data: 'date', name: 'date',searchable:false,orderable:false},
			{data: 'user_pic', name: 'user_pic',searchable:false,orderable:false},
			{data: 'update_user', name: 'update_user',searchable:false,orderable:false},
			{data: 'status_po_buyer', name: 'status_po_buyer',searchable:true,orderable:false},
			{data: 'booking_number', name: 'booking_number',searchable:true,orderable:false},
			{data: 'additional', name: 'additional',searchable:false,orderable:false},
			{data: 'source_supplier', name: 'source_supplier',searchable:false,visible:true,orderable:false},
			{data: 'allocating_to', name: 'allocating_to',searchable:false,visible:true,orderable:false},
			{data: 'item', name: 'item',searchable:false,visible:true,orderable:false},
			{data: 'quantity', name: 'quantity', searchable:false,orderable:false},
			{data: 'warehouse_name', name: 'warehouse_name',searchable:true,visible:true},
			{data: 'remark_update', name: 'remark_update',searchable:true,visible:true},
			{data: 'action', name: 'action',width:'50px',searchable:false,visible:true},
			{data: 'season', name: 'season',searchable:true, visible:false},
			{data: 'po_buyer', name: 'po_buyer',searchable:true, visible:false},
			{data: 'old_po_buyer', name: 'old_po_buyer',searchable:true, visible:false},
			{data: 'document_no', name: 'document_no',searchable:true, visible:false},
			{data: 'created_at', name: 'created_at',searchable:true, visible:false},
			{data: 'lc_date', name: 'lc_date',searchable:true,visible:false},
			{data: 'promise_date', name: 'promise_date',searchable:true,visible:false},
			{data: 'item_code', name: 'item_code',searchable:true,visible:false},
			{data: 'category', name: 'category',searchable:true,visible:false},
			{data: 'item_code_source', name: 'item_code_source',searchable:true,visible:false},
			{data: 'qty_outstanding', name: 'qty_outstanding',searchable:true,visible:false}
		]
	});
}

function additionalDataTables()
{
	$('#additionalTable').DataTable().destroy();
	$('#additionalTable tbody').empty();

	$('#additionalTable').DataTable({
		dom: 'Bfrtip',
		processing: true,
		serverSide: true,
		pageLength:100,
		ajax: {
			type: 'GET',
			url: '/master-data/auto-allocation/additional',
		},
		columns: [
			{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
			{data: 'lc_date', name: 'lc_date',searchable:false,orderable:false},
			{data: 'date', name: 'date',searchable:false,orderable:false},
			{data: 'user_pic', name: 'user_pic',searchable:false,orderable:false},
			{data: 'update_user', name: 'update_user',searchable:false,orderable:false},
			{data: 'status_po_buyer', name: 'status_po_buyer',searchable:true,orderable:false},
			{data: 'booking_number', name: 'booking_number',searchable:true,orderable:false},
			{data: 'additional', name: 'additional',searchable:false,orderable:false},
			{data: 'source_supplier', name: 'source_supplier',searchable:false,visible:true,orderable:false},
			{data: 'allocating_to', name: 'allocating_to',searchable:false,visible:true,orderable:false},
			{data: 'item', name: 'item',searchable:false,visible:true,orderable:false},
			{data: 'quantity', name: 'quantity', searchable:false,orderable:false},
			{data: 'warehouse_name', name: 'warehouse_name',searchable:true,visible:true},
			{data: 'remark_update', name: 'remark_update',searchable:true,visible:true},
			{data: 'action', name: 'action',width:'50px',searchable:false,visible:true},
			{data: 'season', name: 'season',searchable:true, visible:false},
			{data: 'po_buyer', name: 'po_buyer',searchable:true, visible:false},
			{data: 'old_po_buyer', name: 'old_po_buyer',searchable:true, visible:false},
			{data: 'document_no', name: 'document_no',searchable:true, visible:false},
			{data: 'created_at', name: 'created_at',searchable:true, visible:false},
			{data: 'lc_date', name: 'lc_date',searchable:true,visible:false},
			{data: 'promise_date', name: 'promise_date',searchable:true,visible:false},
			{data: 'item_code', name: 'item_code',searchable:true,visible:false},
			{data: 'category', name: 'category',searchable:true,visible:false},
			{data: 'item_code_source', name: 'item_code_source',searchable:true,visible:false},
			{data: 'qty_outstanding', name: 'qty_outstanding',searchable:true,visible:false}
		]
	});
}

function inactiveDataTables()
{
	$('#inactiveTable').DataTable().destroy();
	$('#inactiveTable tbody').empty();

	$('#inactiveTable').DataTable({
		dom: 'Bfrtip',
		processing: true,
		serverSide: true,
		pageLength:100,
		ajax: {
			type: 'GET',
			url: '/master-data/auto-allocation/inactive',
		},
		columns: [
			{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
			{data: 'lc_date', name: 'lc_date',searchable:false,orderable:false},
			{data: 'date', name: 'date',searchable:false,orderable:false},
			{data: 'user_pic', name: 'user_pic',searchable:false,orderable:false},
			{data: 'update_user', name: 'update_user',searchable:false,orderable:false},
			{data: 'status_po_buyer', name: 'status_po_buyer',searchable:true,orderable:false},
			{data: 'booking_number', name: 'booking_number',searchable:true,orderable:false},
			{data: 'additional', name: 'additional',searchable:false,orderable:false},
			{data: 'source_supplier', name: 'source_supplier',searchable:false,visible:true,orderable:false},
			{data: 'allocating_to', name: 'allocating_to',searchable:false,visible:true,orderable:false},
			{data: 'item', name: 'item',searchable:false,visible:true,orderable:false},
			{data: 'quantity', name: 'quantity', searchable:false,orderable:false},
			{data: 'warehouse_name', name: 'warehouse_name',searchable:true,visible:true},
			{data: 'remark_update', name: 'remark_update',searchable:true,visible:true},
			{data: 'action', name: 'action',width:'50px',searchable:false,visible:true},
			{data: 'season', name: 'season',searchable:true, visible:false},
			{data: 'po_buyer', name: 'po_buyer',searchable:true, visible:false},
			{data: 'old_po_buyer', name: 'old_po_buyer',searchable:true, visible:false},
			{data: 'document_no', name: 'document_no',searchable:true, visible:false},
			{data: 'created_at', name: 'created_at',searchable:true, visible:false},
			{data: 'lc_date', name: 'lc_date',searchable:true,visible:false},
			{data: 'promise_date', name: 'promise_date',searchable:true,visible:false},
			{data: 'item_code', name: 'item_code',searchable:true,visible:false},
			{data: 'category', name: 'category',searchable:true,visible:false},
			{data: 'item_code_source', name: 'item_code_source',searchable:true,visible:false},
			{data: 'qty_outstanding', name: 'qty_outstanding',searchable:true,visible:false}
		]
	});
}