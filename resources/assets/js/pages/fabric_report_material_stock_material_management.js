$(function()
{
    $('#active_tab').on('change',function()
    {
		var active_tab = $('#active_tab').val();
        if(active_tab == 'inactive')
        {
			inactiveDataTables();

			var dtableInactive = $('#inactiveTable').dataTable().api();
			$(".dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtableInactive.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtableInactive.search("").draw();
					}
					return;
			});
			dtableInactive.draw();
            $('#select_warehouse').on('change',function(){
                dtableInactive.draw();
            });

            $('#select_type_stock').on('change',function(){
                dtableInactive.draw();
            });
			
        }else if( active_tab == 'active')
        {
			activeDataTables();

			var dtableActive = $('#activeTable').dataTable().api();
			$(".dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtableActive.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtableActive.search("").draw();
					}
					return;
			});
            dtableActive.draw();
            
            $('#select_warehouse').on('change',function(){
                dtableActive.draw();
            });

            $('#select_type_stock').on('change',function(){
                dtableActive.draw();
            });
		} 
	});

    var active_tab = $('#active_tab').val();
    //console.log(active_tab);
    if(active_tab == 'inactive')
    {
		inactiveDataTables();

		var dtableInactive = $('#inactiveTable').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtableInactive.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtableInactive.search("").draw();
				}
				return;
		});
        dtableInactive.draw();
        
        $('#select_warehouse').on('change',function(){
            dtableInactive.draw();
        });

        $('#select_type_stock').on('change',function(){
            dtableInactive.draw();
        });

		
    }else if( active_tab == 'active')
    {
		activeDataTables();

		var dtableActive = $('#activeTable').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtableActive.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtableActive.search("").draw();
				}
				return;
		});
        dtableActive.draw();
        
        $('#select_warehouse').on('change',function(){
            dtableActive.draw();
        });

        $('#select_type_stock').on('change',function(){
            dtableActive.draw();
        });
	} 
})

function changeTab(status){
	$('#active_tab').val(status).trigger('change');
}

$('#select_warehouse').on('change',function()
{
    var value = $(this).val();
    console.log(value);
    $('#_warehouse_stock_id').val(value);
    $('#_warehouse_allocation_id').val(value);
});

function activeDataTables()
{
    $('#activeTable').DataTable().destroy();
    $('#activeTable tbody').empty();

    $('#activeTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        ajax: {
            type: 'GET',
            url: '/fabric/report/material-stock-material-management/active',
            data: function(d) {
                return $.extend({}, d, {
                    "warehouse" : $('#select_warehouse').val(),
                    "type_stock_erp_code" : $('#select_type_stock').val(),
                });
           }
        },
        columns: [
            {data: 'material_stock_id', name: 'material_stock_id',searchable:true,visible:false,orderable:false},
            {data: 'supplier_code', name: 'supplier_code',searchable:true, visible:false,orderable:false},
            {data: 'item_code', name: 'item_code',searchable:true,visible:false,orderable:false},
            {data: 'po_buyer', name: 'po_buyer',searchable:true, visible:false,orderable:false},
            {data: 'warehouse_id', name: 'warehouse_id',searchable:true, visible:false,orderable:false},
            {data: 'available_qty', name: 'available_qty',searchable:false, visible:false,orderable:false},
            {data: 'warehouse_name', name: 'warehouse_name',searchable:false,visible:true,orderable:false},
            {data: 'source', name: 'source',searchable:false,orderable:false,width:'45px'},
            {data: 'supplier', name: 'supplier',searchable:false,orderable:false,width:'45px'},
            {data: 'document_no', name: 'document_no',searchable:true, visible:true,orderable:false},
            {data: 'type_stock', name: 'type_stock',searchable:false,visible:true,orderable:true},
            {data: 'item', name: 'item',searchable:false,orderable:false,width:'45px'},
            {data: 'uom', name: 'uom',searchable:false,orderable:false},
            {data: 'stock', name: 'stock',searchable:false,orderable:false},
            {data: 'reserved_qty', name: 'reserved_qty',searchable:false, visible:true,orderable:true},
            {data: '_available_qty', name: '_available_qty',searchable:false, visible:true,orderable:true},
            {data: 'action', name: 'action',width:'50px',searchable:false,visible:true},
        ]
    });
}

function inactiveDataTables()
{
  	$('#inactiveTable').DataTable().destroy();
    $('#inactiveTable tbody').empty();

    $('#inactiveTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        ajax: {
            type: 'GET',
            url: '/fabric/report/material-stock-material-management/inactive',
            data: function(d) {
                return $.extend({}, d, {
                    "warehouse" : $('#select_warehouse').val(),
                    "type_stock_erp_code" : $('#select_type_stock').val(),
                });
            }
        },
        columns: [
            {data: 'material_stock_id', name: 'material_stock_id',searchable:true,visible:false,orderable:false},
            {data: 'supplier_code', name: 'supplier_code',searchable:true, visible:false,orderable:false},
            {data: 'item_code', name: 'item_code',searchable:true,visible:false,orderable:false},
            {data: 'po_buyer', name: 'po_buyer',searchable:true, visible:false,orderable:false},
            {data: 'warehouse_id', name: 'warehouse_id',searchable:true, visible:false,orderable:false},
            {data: 'available_qty', name: 'available_qty',searchable:false, visible:false,orderable:false},
            {data: 'warehouse_name', name: 'warehouse_name',searchable:false,visible:true,orderable:false},
            {data: 'source', name: 'source',searchable:false,orderable:false,width:'45px'},
            {data: 'supplier', name: 'supplier',searchable:false,orderable:false,width:'45px'},
            {data: 'document_no', name: 'document_no',searchable:true, visible:true,orderable:false},
            {data: 'type_stock', name: 'type_stock',searchable:false,visible:true,orderable:true},
            {data: 'item', name: 'item',searchable:false,orderable:false,width:'45px'},
            {data: 'uom', name: 'uom',searchable:false,orderable:false},
            {data: 'stock', name: 'stock',searchable:false,orderable:false},
            {data: 'reserved_qty', name: 'reserved_qty',searchable:false, visible:true,orderable:true},
            {data: '_available_qty', name: '_available_qty',searchable:false, visible:true,orderable:true},
            {data: 'action', name: 'action',width:'50px',searchable:false,visible:true},
        ]
    });
}

function allocating(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "post",
        url: url,
        beforeSend: function () {
            swal.close();
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            if (response['status'] == 422)
                $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
        .done(function () {
            $("#alert_success").trigger("click", 'Success.');
            $('#activeTable').DataTable().ajax.reload();
        });
}

$('#qty_move').keypress(function (e) {

    if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
        return false;
    }
    return true;
});

$('#delete_qty').keypress(function (e) {

    if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
        return false;
    }
    return true;
});

$('#qty_tranfer').keypress(function (e) {

    if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
        return false;
    }
    return true;
});

$('#new_available_qty_stock').keypress(function (e) {

    if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
        return false;
    }
    return true;
});

$('#qty_change_stock').keypress(function (e) {

    if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
        return false;
    }
    return true;
});

$('#qty_move').change(function()
{
   var qty_move         = parseFloat($(this).val());
   var qty_available    = parseFloat($('#qty_available').val());

   var _temp = qty_available - qty_move;

   if(_temp < 0)
   {
        $("#alert_info").trigger("click", 'Qty move should be less than qty available.');
        $(this).val('');
        return false;
   }
});

$('#delete_qty').change(function()
{
    var qty_delete      = parseFloat($(this).val());
    var qty_available   = parseFloat($('#delete_qty_available').val());
    var _temp           = qty_available - qty_delete;
 
    if(_temp < 0)
    {
        $("#alert_info").trigger("click", 'Qty delete should be less than qty available');
        $(this).val('');
        return false;
    }
    
});

$('#qty_tranfer').change(function()
{
    var qty_tranfer     = parseFloat($(this).val());
    var qty_available   = parseFloat($('#tranfer_qty_available').val());
    var _temp           = qty_available - qty_tranfer;
 
    if(_temp < 0)
    {
        $("#alert_info").trigger("click", 'Qty transfer should be less than qty available');
        $(this).val('');
        return false;
    }
});

$('#qty_change_stock').change(function()
{
    var qty_tranfer     = parseFloat($(this).val());
    var qty_available   = parseFloat($('#qty_available_change_stock').val());
    var _temp           = qty_available - qty_tranfer;
 
    if(_temp < 0)
    {
        $("#alert_info").trigger("click", 'Qty change type stock should be less than qty available');
        $(this).val('');
        return false;
    }
});

function move(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () 
        {
            $.unblockUI();
        },
        error: function (response) 
        {
            $.unblockUI();
            if (response['status'] == 422) $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
    .done(function (response) 
    {
        if(response.available_qty <= 0)
        {
            $("#alert_info").trigger("click", 'This data doesn\'t have qty.');
            return false;
        }

        var _supplier_name = (response.supplier_name)? response.supplier_name : '';
        var _po_buyer = (response.po_buyer)? response.po_buyer : '';

        $('#id').val(response.id);
        $('#uom').val(response.uom);
        $('#qty_available').val(response.available_qty);
        $('#locatorId').val(response.locator_id);
        $('#old_locator_id').val(response.locator_id);
        $('#locatorName').val(response.locator_name);
        $('#header_stock').text(_supplier_name+' '+response.document_no+' '+response.item_code +' '+response.locator_name+' '+_po_buyer);
        $('#moveLocatorModal').modal({
            backdrop: 'static',
            keyboard: false  // to prevent closing with Esc button (if you want this too)
        });
    });
}

function cancel(url) 
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "post",
        url: url,
        beforeSend: function () {
            swal.close();
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            if (response['status'] == 422)
                $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
    .done(function () {
        $("#alert_success").trigger("click", 'Success.');
        $('#activeTable').DataTable().ajax.reload();
    });
}

function recalculate(url) 
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "put",
        url: url,
        beforeSend: function () {
            swal.close();
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            if (response['status'] == 422)
                $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
        .done(function () {
            $("#alert_success").trigger("click", 'Success.');
            $('#activeTable').DataTable().ajax.reload();
        });
}

function showDetail(url){
    $('#detailModal').modal();
    function itemAjax(){
        $('#detailTable').addClass('hidden');
        $('#detailModal').find('.shade-screen').removeClass('hidden');
    
        $.ajax({
            url: url,
        })
        .done(function (data) {
            $('#detailTable').html(data);
            $('#detailTable').removeClass('hidden');
            $('#detailModal').find('.shade-screen').addClass('hidden');
        });
    }

    itemAjax();
}

function history(url)
{
    $('#histori_stock').html('');
    $('#detailModal').modal();

    function itemAjax(){
        $('#tab').addClass('hidden');
        $('#detailModal').find('.shade-screen').removeClass('hidden');
    
        $.ajax({
            url: url,
        })
        .done(function (data) 
        {
            console.log(data);
            //var material_stock = data.material_stock;
            //$('#histori_stock').text(material_stock.supplier_name+' '+material_stock.document_no+' '+material_stock.item_code+' '+material_stock.po_buyer+' '+material_stock.locator);
            $('#sourceStockTable').html(data.view_detail_stock);
            $('#detailTable').html(data.view_allocation_per_pobuyer);
            $('#detailNonPoTable').html(data.view_allocation_non_per_pobuyer);
            $('#detailNonAllocartinTable').html(data.view_non_allocation);
            $('#tab').removeClass('hidden');
            $('#detailModal').find('.shade-screen').addClass('hidden');
        });
    }

    function searchPoBuyer(){
        var q = $('#detailTableSearch').val();
        $('#detailTable').addClass('hidden');
        $('#detailModal').find('.shade-screen-2').removeClass('hidden');
        $('#detailModal').find('.form-search').addClass('hidden');
        
        $.ajax({
            url: url + '?q=' + q,
        })
        .done(function (data) {
            var material_stock = data.material_stock;
            $('#histori_stock').text(material_stock.supplier_name+' '+material_stock.document_no+' '+material_stock.item_code+' '+material_stock.po_buyer+' '+material_stock.locator);
            $('#detailTable').html(data.view_allocation_per_pobuyer);
            $('#detailTableSearch').focus();
			$('#detailTableSearch').val('');
            $('#detailTable').removeClass('hidden');
            $('#detailModal').find('.shade-screen-2').addClass('hidden');
            $('#detailModal').find('.form-search').removeClass('hidden');
            
        });
    }

    $('#detailTableSearch').val("");
	$('#detailTableButtonSrc').unbind();
    $('#detailTableSearch').unbind();
    
    $('#detailTableButtonSrc').on('click', searchPoBuyer);

	$('#detailTableSearch').on('keypress', function (e) {
		if (e.keyCode == 13)
        searchPoBuyer();
	});

   
    itemAjax();
}

function edit(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });

            $('#update_stock_id').val('');
            $('#update_uom').val('');
            $('#update_qty_available_stock').val('');
            $('#update_header_stock').text('');
       
            

        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            if (response['status'] == 422) $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
    .done(function (response) {
        var _supplier_name = (response.supplier_name)? response.supplier_name : '';
        var _po_buyer = (response.po_buyer)? response.po_buyer : '';

        $('#update_stock_id').val(response.id);
        $('#update_uom').val(response.uom);
        $('#update_qty_available_stock').val(response.available_qty);
        $('#update_header_stock').text(_supplier_name+' '+response.document_no+' '+response.item_code+' '+_po_buyer );
        $('#updateReportStockModal').modal({
            backdrop: 'static',
            keyboard: false  // to prevent closing with Esc button (if you want this too)
        });
    });
}

function hapus(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            if (response['status'] == 422) $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
    .done(function (response) {
        var _supplier_name = (response.supplier_name)? response.supplier_name : '';
        var _po_buyer = (response.po_buyer)? response.po_buyer : '';

        $('#delete_id').val(response.id);
        $('#delete_uom').val(response.uom);
        $('#delete_qty_available').val(response.available_qty);
        $('#delete_header_stock').text(_supplier_name+' '+response.document_no+' '+response.item_code+' '+response.locator_name+' '+_po_buyer );
        $('#deleteStockModal').modal({
            backdrop: 'static',
            keyboard: false  // to prevent closing with Esc button (if you want this too)
        });
    });
}

function transfer(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            if (response['status'] == 422) $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
    .done(function (response) {
        var _supplier_name = (response.supplier_name)? response.supplier_name : '';
        var _po_buyer = (response.po_buyer)? response.po_buyer : '';

        $('#transfer_id').val(response.id);
        $('#transfer_uom').val(response.uom);
        $('#tranfer_qty_available').val(response.available_qty);
        $('#transfer_header_stock').text(_supplier_name+' '+response.document_no+' '+response.item_code+' '+_po_buyer );
        $('#transFerStockModal').modal({
            backdrop: 'static',
            keyboard: false  // to prevent closing with Esc button (if you want this too)
        });
    });
}

function changeType(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            if (response['status'] == 422) $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
    .done(function (response) 
    {
        var update = response;
        
        if(update.available_qty <= 0){
            $("#alert_info_2").trigger("click", 'Type stock cannot because doesnt have qty left.');
            return false;
        }

        var _supplier_name = (update.supplier_name)? update.supplier_name : '';
        var _po_buyer = (update.po_buyer)? update.po_buyer : '';

        $('#curr_type_stock').val(update.type_stock);
        $('#change_type_stock_id').val(update.id);
        $('#uom_change_type_stock').val(update.uom);
        $('#qty_available_change_stock').val(update.available_qty);
        $('#header_change_type_stock').text(_supplier_name+' '+update.document_no+' '+update.item_code+' '+_po_buyer );
        $("#mapping_type_stock_option").val(update.type_stock).trigger('change');

        $('#changeTypeStockModal').modal({
            backdrop: 'static',
            keyboard: false  // to prevent closing with Esc button (if you want this too)
        });
    });
}



$('#changeTypeStockForm').submit(function (event) 
{
    event.preventDefault();

    var qty_move                  = $('#qty_change_stock').val();
    var curr_type_stock           = $('#curr_type_stock').val();
    var mapping_type_stock_option = $('#mapping_type_stock_option').val();

    if(curr_type_stock == mapping_type_stock_option)
    {
        $("#alert_warning").trigger("click", 'Type stock stil same with the previous, nothing changed.');
        return false;
    }

    if(qty_move <= 0)
    {
        $("#alert_warning").trigger("click", 'Please insert qty move first.');
        return false;
    }

    $('#changeTypeStockModal').modal('hide');
    bootbox.confirm("Are you sure want to change this to "+mapping_type_stock_option+" ?.", function (result) {
        if (result) {
            $.ajax({
                type: "POST",
                url: $('#changeTypeStockForm').attr('action'),
                data: $('#changeTypeStockForm').serialize(),
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function () {
                    $.unblockUI();
                    $('#changeTypeStockForm').trigger('reset');
                    $('#changeTypeStockModal').modal('hide');
                },
                error: function (response) {
                    $.unblockUI();
                    if(response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
                    $('#changeTypeStockModal').modal();
                }
                
            })
            .done(function(){
                $('#activeTable').DataTable().ajax.reload();
            });
        }
    });
});


$('#updateStockform').submit(function (event) {
    event.preventDefault();

    var new_qty_available = $('#new_available_qty_stock').val();
    var update_reason = $('#update_reason').val();
    
    if(!new_qty_available){
        $("#alert_info").trigger("click", 'silahkan isi qty terlebih dahulu');
        return false;
    }

    if(!update_reason){
        $("#alert_info").trigger("click", 'silahkan isi alasannya dulu bro dan sist');
        return false;
    }

    $('#updateReportStockModal').modal('hide');
    bootbox.confirm("Apakah anda yakin akan mengubah stock ini ?.", function (result) {
        if (result) {
            $.ajax({
                type: "POST",
                url: $('#updateStockform').attr('action'),
                data: $('#updateStockform').serialize(),
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function () {
                    $.unblockUI();
                    $('#updateStockform').trigger('reset');
                    $('#updateReportStockModal').modal('hide');
                },
                error: function (response) {
                    $.unblockUI();
                    if(response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
                    $('#updateReportStockModal').modal();
                }
                
            })
            .done(function(){
                $('#activeTable').DataTable().ajax.reload();
            });
        }
    });
});

$('#transFerStockForm').submit(function (event) {
    event.preventDefault();

    var qty_tranfer = $('#qty_tranfer').val();
    
    if(!qty_tranfer){
        $("#alert_info").trigger("click", 'silahkan isi qty terlebih dahulu');
        return false;
    }

    $('#transFerStockModal').modal('hide');
    bootbox.confirm("Apakah anda yakin akan mentrasfer stock ini ?.", function (result) {
        if (result) {
            $.ajax({
                type: "POST",
                url: $('#transFerStockForm').attr('action'),
                data: $('#transFerStockForm').serialize(),
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function () {
                    $.unblockUI();
                    $('#transFerStockForm').trigger('reset');
                    $('#transFerStockModal').modal('hide');
                },
                error: function (response) {
                    $.unblockUI();
                    if(response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
                    $('#transFerStockModal').modal();
                }
                
            })
            .done(function(response){
                $('#activeTable').DataTable().ajax.reload();

                if (response != 'no_need_print'){
                    var arrStr = encodeURIComponent(JSON.stringify(response));
                    window.open('/report/accessories/material-stock/transfer-stock/print-barcode?id=' + arrStr);
                }
            });
        }
    });
});

$('#deleteStockform').submit(function (event) 
{
    event.preventDefault();

    var delete_qty      = $('#delete_qty').val();
    var delete_reason   = $('#delete_reason').val();
    
    if(delete_qty <= 0)
    {
        $("#alert_info").trigger("click", 'Please fill qty delete first.');
        return false;
    }

    if(!delete_reason)
    {
        $("#alert_warning").trigger("click", 'Please insert reason first.');
        return false;
    }

    $('#deleteStockModal').modal('hide');
    bootbox.confirm("Are you sure want to delete this stock ?.", function (result) 
    {
        if (result) {
            $.ajax({
                type: "POST",
                url: $('#deleteStockform').attr('action'),
                data: $('#deleteStockform').serialize(),
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function () {
                    $.unblockUI();
                    $('#deleteStockform').trigger('reset');
                    $('#deleteStockModal').modal('hide');
                },
                error: function (response) {
                    $.unblockUI();
                    if(response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ict');
                    $('#deleteStockModal').modal();
                }
                
            })
            .done(function(){
                $('#activeTable').DataTable().ajax.reload();
            });
        }
    });
});

