$(function()
{
    $('#report_material_metal_detector_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/accessories/report/material-metal-detector/data',
            data: function(d) {
                return $.extend({}, d, {
                    "warehouse"     : $('#select_warehouse').val(),
                    "start_date"    : $('#start_date').val(),
                    "end_date"      : $('#end_date').val(),
                    "status"        : $('#select_status').val(),
                });
           }
        },
        columns: [
            {data: 'warehouse', name: 'warehouse',searchable:true,visible:true,orderable:false},
            {data: 'arrival_date', name: 'arrival_date',searchable:false,visible:true,orderable:true},
            {data: 'inspection_date', name: 'inspection_date',searchable:false,visible:true,orderable:true},
            {data: 'inspector_name', name: 'inspector_name',searchable:true,visible:true,orderable:true},
            {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
            {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
            {data: 'po_buyer', name: 'po_buyer',searchable:true,visible:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'item_desc', name: 'item_desc',searchable:false,visible:true,orderable:false},
            {data: 'category', name: 'category',searchable:true,visible:true,orderable:true},
            {data: 'style', name: 'style',searchable:true,visible:true,orderable:true},
            {data: 'article_no', name: 'article_no',searchable:true,visible:true,orderable:true},
            {data: 'status', name: 'status',searchable:true,visible:true,orderable:false},
            {data: 'uom_conversion', name: 'uom_conversion',searchable:false,visible:true,orderable:false},
            {data: 'percentage', name: 'percentage',searchable:false,visible:true,orderable:false},
            {data: 'qty_order', name: 'qty_order',searchable:false,visible:true,orderable:false},
            {data: 'qty_reject', name: 'qty_reject',searchable:false,visible:true,orderable:false},
            {data: 'qty_release', name: 'qty_release',searchable:false,visible:true,orderable:false},
            {data: 'remark', name: 'remark',searchable:false,visible:true,orderable:false},
	    ]
    });

    var dtable = $('#report_material_metal_detector_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();
    
    $('#select_warehouse').on('change',function(){
        dtable.draw();
    });

    $('#select_status').on('change',function(){
        dtable.draw();
    });

    $('#start_date').on('change',function(){
        dtable.draw();
    });

    $('#end_date').on('change',function(){
        dtable.draw();
    });;
});