$(function () {
	var dtable = $('#items-datatable').dataTable().api();
	dtable.page.len(100);
	$(".dataTables_filter input")
		.unbind() // Unbind previous default bindings
		.bind("keyup", function (e) { // Bind our desired behavior
			// If the user pressed ENTER, search
			if (e.keyCode == 13) {
				// Call the API search function
				dtable.search(this.value).draw();
			}
			// Ensure we clear the search if they backspace far enough
			if (this.value == "") {
				dtable.search("").draw();
			}
			return;
		});
	dtable.draw();
});

$('#sync_item').on('click',syncItem);
function syncItem(){
	url_sync = $('#route_sync').attr('href');
	console.log(url_sync);
	$.ajax({
		type: "GET",
		url: url_sync,
		beforeSend: function () {
			$.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
		},
		success: function (response) {
			$.unblockUI();
			$("#alert_info").trigger("click", response);
		},
		error: function (response) {
			$.unblockUI();
			if (response['status'] == 500){
				$("#alert_error").trigger("click", 'Please Contact ICT');
			}
		}
	})
	.done(function ($result) {
		// $("#alert_success").trigger("click", 'Data berhasil disinkron.');
		$('#dataTableBuilder').DataTable().ajax.reload();
	});
}