$(function () 
{
	$('#active_tab').on('change', function () 
	{
		var active_tab = $('#active_tab').val();
		if (active_tab == 'reguler') 
		{
			regulerDataTables();

			var dtable = $('#reguler_table').dataTable().api();
			$(".dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtable.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtable.search("").draw();
					}
					return;
				});
			dtable.draw();

			$('#start_date').on('change',function(){
				dtable.draw();
			});
		
			$('#end_date').on('change',function(){
				dtable.draw();
			});
		}else if (active_tab == 'additional') 
		{
			additionalDataTables();

			var dtable = $('#additional_table').dataTable().api();
			$(".dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtable.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtable.search("").draw();
					}
					return;
				});
			dtable.draw();

			$('#start_date').on('change',function(){
				dtable.draw();
			});
		
			$('#end_date').on('change',function(){
				dtable.draw();
			});
		}
	});

	var active_tab = $('#active_tab').val();
	if (active_tab == 'reguler') 
	{
		regulerDataTables();

		var dtable = $('#reguler_table').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtable.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtable.search("").draw();
				}
				return;
			});
		dtable.draw();

		$('#start_date').on('change',function(){
			dtable.draw();
		});
	
		$('#end_date').on('change',function(){
			dtable.draw();
		});
	}else if (active_tab == 'additional') 
	{
		additionalDataTables();

		var dtable = $('#additional_table').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtable.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtable.search("").draw();
				}
				return;
			});
		dtable.draw();

		$('#start_date').on('change',function(){
			dtable.draw();
		});
	
		$('#end_date').on('change',function(){
			dtable.draw();
		});
	}
});

function changeTab(status) 
{
	$('#active_tab').val(status).trigger('change');
}

function regulerDataTables() 
{
	$('#reguler_table').DataTable().destroy();
	$('#reguler_table tbody').empty();

	$('#reguler_table').DataTable({
		dom: 'Bfrtip',
		processing: true,
		serverSide: true,
		pageLength: 100,
		scroller: true,
		deferRender: true,
		ajax: {
			type: 'GET',
			url: '/fabric/allocation/data-reguler',
			data: function(d) {
                return $.extend({}, d, {
                    "start_date"    : $('#start_date').val(),
                    "end_date"      : $('#end_date').val(),
                });
           }
		},
		columns: [
			{
				data		: 'allocation_item_id',
				name		: 'allocation_item_id',
				searchable 	: true,
				visible		: false,
				orderable	: false
			},
			{
				data		: 'auto_allocation_id',
				name		: 'auto_allocation_id',
				searchable 	: true,
				visible		: false,
				orderable	: false
			},
			{
				data		: 'lc_date',
				name		: 'lc_date',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'style',
				name		: 'style',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'article_no',
				name		: 'article_no',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'planning_date',
				name		: 'planning_date',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'po_buyer',
				name		: 'po_buyer',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'warehouse_inventory_name',
				name		: 'warehouse_inventory_name',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'item_code',
				name		: 'item_code',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'item_code_source',
				name		: 'item_code_source',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'uom',
				name		: 'uom',
				searchable 	: false,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'qty_need',
				name		: 'qty_need',
				searchable 	: false,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'qty_booking',
				name		: 'qty_booking',
				searchable 	: false,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'qty_plan',
				name		: 'qty_plan',
				searchable 	: false,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'actual_lot',
				name		: 'actual_lot',
				searchable 	: false,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'document_no',
				name		: 'document_no',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'supplier_name',
				name		: 'supplier_name',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'item_desc',
				name		: 'item_desc',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'category',
				name		: 'category',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'pic',
				name		: 'pic',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'warehouse_inventory',
				name		: 'warehouse_inventory',
				searchable 	: true,
				visible		: false,
				orderable	: false
			},
			{
				data		: 'created_at',
				name		: 'created_at',
				searchable 	: false,
				visible		: true,
				orderable	: false
			},
			{
				data		: 'remark',
				name		: 'remark',
				searchable 	: false,
				visible		: true,
				orderable	: false
			},
			{
				data		: 'remark_additional',
				name		: 'remark_additional',
				searchable 	: false,
				visible		: true,
				orderable	: false
			}
		]
	});
}

function additionalDataTables() 
{
	$('#additional_table').DataTable().destroy();
	$('#additional_table tbody').empty();

	$('#additional_table').DataTable({
		dom: 'Bfrtip',
		processing: true,
		serverSide: true,
		pageLength: 100,
		scroller: true,
		deferRender: true,
		ajax: {
			type: 'GET',
			url: '/fabric/allocation/data-additional',
			data: function(d) {
                return $.extend({}, d, {
                    "start_date"    : $('#start_date').val(),
                    "end_date"      : $('#end_date').val(),
                });
           }
		},
		columns: [
			{
				data		: 'allocation_item_id',
				name		: 'allocation_item_id',
				searchable 	: true,
				visible		: false,
				orderable	: false
			},
			{
				data		: 'auto_allocation_id',
				name		: 'auto_allocation_id',
				searchable 	: true,
				visible		: false,
				orderable	: false
			},
			{
				data		: 'lc_date',
				name		: 'lc_date',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'style',
				name		: 'style',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'article_no',
				name		: 'article_no',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'planning_date',
				name		: 'planning_date',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'po_buyer',
				name		: 'po_buyer',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'warehouse_inventory_name',
				name		: 'warehouse_inventory_name',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'item_code',
				name		: 'item_code',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'item_code_source',
				name		: 'item_code_source',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'uom',
				name		: 'uom',
				searchable 	: false,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'qty_need',
				name		: 'qty_need',
				searchable 	: false,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'qty_booking',
				name		: 'qty_booking',
				searchable 	: false,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'qty_plan',
				name		: 'qty_plan',
				searchable 	: false,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'actual_lot',
				name		: 'actual_lot',
				searchable 	: false,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'document_no',
				name		: 'document_no',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'supplier_name',
				name		: 'supplier_name',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'item_desc',
				name		: 'item_desc',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'category',
				name		: 'category',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'pic',
				name		: 'pic',
				searchable 	: true,
				visible		: true,
				orderable	: true
			},
			{
				data		: 'warehouse_inventory',
				name		: 'warehouse_inventory',
				searchable 	: true,
				visible		: false,
				orderable	: false
			},
			{
				data		: 'created_at',
				name		: 'created_at',
				searchable 	: false,
				visible		: true,
				orderable	: false
			},
			{
				data		: 'remark',
				name		: 'remark',
				searchable 	: false,
				visible		: true,
				orderable	: false
			},
			{
				data		: 'remark_additional',
				name		: 'remark_additional',
				searchable 	: false,
				visible		: true,
				orderable	: false
			}
		],
		order: [
			[0, 'desc'],
			[7, 'desc']
		],
	});
}