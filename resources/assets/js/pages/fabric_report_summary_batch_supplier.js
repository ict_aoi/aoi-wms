$(function()
{
    $('#fabric_report_summary_batch_supplier_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/fabric/report/summary-batch-supplier/data',
            data: function(d) {
                return $.extend({}, d, {
                    "year"      : $('#select_year').val(),
                    "month"     : $('#select_month').val(),
                });
           }
        },
        columns: [
            {data: 'year_receive', name: 'year_receive',searchable:false,visible:true,orderable:true},
            {data: 'month_receive_name', name: 'month_receive_name',searchable:false,visible:true,orderable:true},
            {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
            {data: 'total_batch', name: 'total_batch',searchable:false,visible:true,orderable:false},
            {data: 'action', name: 'qty',searchable:false,visible:true,orderable:false},
        ]
    });

    var dtable = $('#fabric_report_summary_batch_supplier_table').dataTable().api();
    $("#fabric_report_summary_batch_supplier_table.dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();
    
    $('#select_year').on('change',function(){
        dtable.draw();
    });

    $('#select_month').on('change',function(){
        dtable.draw();
    });
});

function detail(url)
{
    $('#detailModal').modal();
    $('#fabric_report_summary_batch_supplier_detail_table').DataTable().destroy();
    $('#fabric_report_summary_batch_supplier_detail_table tbody').empty();
    
    $('#fabric_report_summary_batch_supplier_detail_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        ajax: {
            type: 'GET',
            url: url
        },
        columns: [
            {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true,width:'50%'},
            {data: 'batch_number', name: 'batch_number',searchable:true,visible:true,orderable:true,width:'50%'}
        ]
    });

    var dtable = $('#fabric_report_summary_batch_supplier_detail_table').dataTable().api();
    $("#fabric_report_summary_batch_supplier_detail_table.dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
}