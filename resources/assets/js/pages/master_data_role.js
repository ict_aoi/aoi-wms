$(function () {
	var list_permissions = JSON.parse($('#permissions').val());
	var flag_msg = $('#flag_msg').val();
	if (flag_msg == 'success') {
		$("#alert_success").trigger("click", 'Data berhasil disimpan.');
	} else if (flag_msg == 'success_2') {
		$("#alert_success").trigger("click", 'Data berhasil diubah.');
	}

	CreatePicklist('permission', '/master-data/user-management/role/permission/list?');

	function render() {
		getIndex();
		$('#permissions').val(JSON.stringify(list_permissions));

		var tmpl = $('#permission-table').html();


		Mustache.parse(tmpl);
		var data = {
			item: list_permissions
		};
		var html = Mustache.render(tmpl, data);
		$('#tbody-permission').html(html);
		bind();

		//CreatePicklist('item', '/production/item/list?');
	}

	function bind() {
		$('#AddItemButton').on('click', tambahItem);
		$('.btn-delete-item').on('click', deleteItem);

		$('.input-new').keypress(function (e) {
			if (e.keyCode == 13) {
				e.preventDefault();
				tambahItem();
			}
		});

		$('.input-edit').keypress(function (e) {
			var i = $(this).data('id');

			if (e.keyCode == 13) {
				e.preventDefault();
				$('#simpan_' + i).click();
			}
		});

	}

	function checkItem(permission_id, index) {
		for (var i in list_permissions) {
			var permission = list_permissions[i];
			if (i == index)
				continue;

			if (permission.id == permission_id)
				return false;
		}

		return true;
	}

	function getIndex() {
		for (idx in list_permissions) {
			list_permissions[idx]['_id'] = idx;
			list_permissions[idx]['no'] = parseInt(idx) + 1;
		}
	}

	function tambahItem() {
		var name = $('#permissionName').val();
		var id = $('#permissionId').val();
		var description = $('#description').val();

		var input = {
			'id': id,
			'name': name,
			'description': description
		};

		var diff = checkItem(id);
		if (!diff) {
			$('#errorInput').removeClass('hidden');
			return;
		}

		if (name)
			list_permissions.push(input);

		render();
	}

	function deleteItem() {
		var i = parseInt($(this).data('id'), 10);

		list_permissions.splice(i, 1);
		render();
	}

	render();
});

function CreatePicklist(name, url) {
	var search = '#' + name + 'Search';
	var list = '#' + name + 'List';
	var item_id = '#' + name + 'Id';
	var item_name = '#' + name + 'Name';
	var modal = '#' + name + 'Modal';
	var table = '#' + name + 'Table';
	var buttonSrc = '#' + name + 'ButtonSrc';
	var buttonDel = '#' + name + 'ButtonDel';

	function itemAjax() {
		var q = $(search).val();

		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
				url: url + '&q=' + q,
			})
			.done(function (data) {
				$(table).html(data);
				pagination(name);

				$(table).removeClass('hidden');
				$(modal).find('.shade-screen').addClass('hidden');
				$(modal).find('.form-search').removeClass('hidden');

				$(table).find('.btn-choose').on('click', chooseItem);
			});
	}

	function chooseItem() {
		var id = $(this).data('id');
		var name = $(this).data('name');
		var description = $(this).data('description');
		var obj = $(this).data('obj');
		$(item_id).val(id);
		$(item_name).val(name);
		$('#description').val(description);

		if (!obj || obj.length == 0)
			obj = [{}];

		$(item_id).trigger('change', obj);
	}

	function pagination() {
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	$(search).val("");
	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	$(buttonDel).on('click', function () {
		$(item_id).val('');
		$(item_name).val('');
		$(description).val('');
	});

	itemAjax();
}

function hapus(url) {
	bootbox.confirm("Apakah anda yakin akan menghapus data ini ?.", function (result) {
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
					type: "DELETE",
					url: url,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					success: function (response) {
						$.unblockUI();
					},
				})
				.done(function ($result) {
					$("#alert_success").trigger("click", 'Data berhasil dihapus.');
					$('#dataTableBuilder').DataTable().ajax.reload();
				});
		}
	});
}

var url = $('#url_role').attr('href');
$('#form').submit(function (event) {
	event.preventDefault();

	var role_name = $('#name').val();
	if (!role_name) {
		$("#alert_error").trigger("click", 'Silahkan isi role name terlebih dahulu.');
		return false;
	}

	bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
		if (result) {
			$.ajax({
				type: "POST",
				url: $('#form').attr('action'),
				data: $('#form').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function (response) {
					document.location.href = url;
				},
				error: function (response) {
					$.unblockUI();
					if (response['status'] == 500) {
						$("#alert_error").trigger("click", 'Please Contact ICT');
					} else {
						for (i in response.responseJSON) {
							$("#alert_error").trigger("click", response.responseJSON[i]);
						}
					}
				}
			});
		}
	});
});