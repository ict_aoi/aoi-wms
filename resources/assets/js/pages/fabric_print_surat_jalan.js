$(function()
{
    $('#select_warehouse').trigger('change');
    $('#print_surat_jalan_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/fabric/barcode/print-surat-jalan/data',
            data: function(d) {
                return $.extend({}, d, {
                    "warehouse"     : $('#select_warehouse').val(),
                    "start_date"    : $('#start_date').val(),
                    "end_date"      : $('#end_date').val()
                });
           }
        },
        columns: [
            {data: 'warehouse_id', name: 'warehouse_id',searchable:true,visible:true,orderable:true},
            {data: 'tanggal_kirim', name: 'tanggal_kirim',searchable:true,visible:true,orderable:true},
            {data: 'no_surat_jalan', name: 'no_surat_jalan',searchable:true,visible:true,orderable:false},
            {data: 'no_kk', name: 'no_kk',searchable:false,visible:true,orderable:true},
            {data: 'total_qty_handover', name: 'total_qty_handover',searchable:false,visible:true,orderable:true},
            {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
        ]
    });

    var dtable = $('#print_surat_jalan_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();

    $('#select_warehouse').on('change',function(){
        dtable.draw();
    });

    $('#start_date').on('change',function(){
        dtable.draw();
    });

    $('#end_date').on('change',function(){
        dtable.draw();
    });
})

