$(function () 
{
	var page = $('#page').val();

	if(page == 'index')
	{
		var flag_msg = $('#flag_msg').val();
		if (flag_msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');
		else if (flag_msg == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated.');

		$('#master_data_destination_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            scroller:true,
            deferRender:true,
            ajax: {
                type: 'GET',
				url: '/master-data/destination/data',
				data: function(d) {
                    return $.extend({}, d, {
                        "warehouse"       : $('#select_warehouse').val()
                    });
               }
            },
            columns: [
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'name', name: 'name',searchable:true,visible:true,orderable:true},
				{data: 'action', name: 'action',searchable:false,visible:true,orderable:true},
            ]
        });
    
        var dtable = $('#master_data_destination_table').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
		dtable.draw();

		$('#select_warehouse').on('change',function(){
			dtable.draw();
		});
	}
	else
	{
		list_destinations = JSON.parse($('#destination').val());
		render();
	}
	


});

$('#form').submit(function (event) 
{
	event.preventDefault();

	var warehouse 		= $('#warehouse').val();
	var name 			= $('#name').val();
	var total_detail 	= list_destinations.length;

	if (!warehouse) 
	{
		$("#alert_warning").trigger("click", 'Please select warehouse first.');
		return false;
	}

	if (!name) 
	{
		$("#alert_warning").trigger("click", 'Please insert name first.');
		return false;
	}

	if (total_detail == 0) 
	{
		$("#alert_warning").trigger("click", 'Please insert detial at least 1.');
		return false;
	}

	bootbox.confirm("Are you sure want to save this data ?.", function (result) 
	{
		if (result) {
			$.ajax({
				type: "POST",
				url: $('#form').attr('action'),
				data: $('#form').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function () {
					var url = $('#url_destination_index').val(); 
					document.location.href = url;
				},
				error: function (response) {
					$.unblockUI();
					if(response.status == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
					else if(response.status == 422) $("#alert_error").trigger("click", response.responseJSON.message);
				}
			});
		}
	});
	
});

function render() 
{
	getIndex();
	$('#destination').val(JSON.stringify(list_destinations));

	var tmpl = $('#destination-table').html();
	Mustache.parse(tmpl);
	var data = { item: list_destinations };
	var html = Mustache.render(tmpl, data);
	$('#tbody-destination').html(html);
	bind();
}

function bind() 
{
	$('#AddItemButton').on('click', tambahItem);
	$('.btn-delete-item').on('click', deleteItem);
	$('.btn-edit-item').on('click', editItem);
	$('.btn-save-item').on('click', saveItem);
	$('.btn-cancel-item').on('click', cancelEdit);


	$('.input-new').keypress(function (e) {
		if (e.keyCode == 13) {
			e.preventDefault();
			tambahItem();
		}
	});

	$('.input-edit').keypress(function (e) {
		var i = $(this).data('id');

		if (e.keyCode == 13) {
			e.preventDefault();
			$('#simpan_' + i).click();
		}
	});

	$('.input-number').keypress(function (e) {
		if (e.keyCode > 31 && (e.keyCode < 47 || e.keyCode > 57)) {
			return false;
		}
		return true;
	});
}

function getIndex() 
{
	for (idx in list_destinations) {
		list_destinations[idx]['_id'] = idx;
		list_destinations[idx]['no'] = parseInt(idx) + 1;
	}
}

function checkItem(name,urutan, index) 
{
	for (var i in list_destinations) {
		var rack = list_destinations[i];
		if (i == index)
			continue;

		if (rack.rack == name && rack.y_row == urutan)
			return false;
	}

	return true;
}

function tambahItem() 
{
	var name 		= $('#rack').val();
	var name_id 	= $('#name_id').val();
	var urutan 		= $('#y_row').val();

	var input = 
	{
		'rack'		: name,
		'z_column'	: name_id,
		'y_row'		: urutan,
		'area_id' 	: -1,
		'locator_id': -1
	};
	var diff = checkItem(name, urutan);

	if (!diff) 
	{
		$("#alert_warning").trigger("click", 'Data already exists.');
		return false;
	}

	list_destinations.push(input);
	render();
}

function editItem() 
{
	var i = $(this).data('id');

	var area_id = $('#areaIdInput_' + i).val();

	$('#rack_' + i).addClass('hidden');
	$('#yrow_' + i).addClass('hidden');
	$('#zcolumn_' + i).addClass('hidden');

	$('#rackInput_' + i).removeClass('hidden');
	$('#yrowInput_' + i).removeClass('hidden');	
	$('#zcolumnInput_' + i).removeClass('hidden');
	

	$('#edit_' + i).addClass('hidden');
	$('#delete_' + i).addClass('hidden');

	//remove hidden textbox
	

	$('#simpan_' + i).removeClass('hidden');
	$('#cancel_' + i).removeClass('hidden');
}

function cancelEdit() 
{
	var i = $(this).data('id');

	//remove hidden div
	$('#rack_' + i).removeClass('hidden');
	$('#zcolumn_' + i).removeClass('hidden');
	$('#yrow_' + i).removeClass('hidden');

	$('#edit_' + i).removeClass('hidden');
	$('#delete_' + i).removeClass('hidden');

	//add hidden textbox
	$('#rackInput_' + i).addClass('hidden');
	$('#zcolumnInput_' + i).addClass('hidden');
	$('#yrowInput_' + i).addClass('hidden');

	$('#simpan_' + i).addClass('hidden');
	$('#cancel_' + i).addClass('hidden');
	$('#errorInput_' + i).addClass('hidden');
}

function saveItem() 
{
	var i = $(this).data('id');

	var name 		= $('#rackInput_' + i).val();
	var name_id 	= $('#zcolumnInput_' + i).val();
	var area_id 	= $('#areaIdInput_' + i).val();
	var urutan 		= $('#yrowInput_' + i).val();
	
	var diff = checkItem(name, urutan, i);

	if (!diff) {
		$('#errorInput_' + i).removeClass('hidden');
		return;
	}

	list_destinations[i].rack = name;
	list_destinations[i].z_column = name_id;
	list_destinations[i].y_row = urutan;
	render();
}

function deleteItem() 
{
	var i 		= $(this).data('id');
	var data 	= list_destinations[i];

	if (data.locator_id == -1) list_destinations.splice(i, 1);
	else $("#alert_error").trigger("click", 'belum ada fungsi untuk menghapus data yang sudah tersimpan di database');

	render();
}

