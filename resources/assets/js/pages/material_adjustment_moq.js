list_material_moqs = JSON.parse($('#material_moqs').val());
list_material_adjustment_moqs = JSON.parse($('#material_adjustment_moqs').val());

$(function () {
    $('.barcode_value').focus();

    function renderHeader() {
		getIndexHeader();
		$('#material_moqs').val(JSON.stringify(list_material_moqs));
		var tmpl = $('#material-moq-table').html();
		Mustache.parse(tmpl);
        var data = { list: list_material_moqs };
        var html = Mustache.render(tmpl, data);
		$('#tbody-material-moq').html(html);
		bindHeader();
	}

	function renderDetail() {
		getIndexDetail();
		$('#material_adjustment_moqs').val(JSON.stringify(list_material_adjustment_moqs));
		var tmpl = $('#material-adjustment-moq-table').html();
		Mustache.parse(tmpl);
		var data = { list: list_material_adjustment_moqs };
        var html = Mustache.render(tmpl, data);
        $('#tbody-material-adjustment-moq').html(html);
		bindDetail();
	}
    
    function sortList(array, key) {
		return array.sort(function (a, b) {
			var x = a[key]; var y = b[key];
			return ((x < y) ? -1 : ((x > y) ? 1 : 0));
		});
    }
    

    function getIndexHeader() {
		for (id in list_material_moqs) {
			list_material_moqs[id]['_id'] = id;
            list_material_moqs[id]['no'] = parseInt(id) + 1;
        }
    }
    
    function getIndexDetail() {
        for (idx in list_material_adjustment_moqs) {
            list_material_adjustment_moqs[idx]['_id'] = idx;
            list_material_adjustment_moqs[idx]['nox'] = parseInt(idx) + 1;
        }
    }

    function bindHeader(){
		$('.btn-show-detail').on('click', showDetail);
    }
    
    function bindDetail() {
        $('#barcode_value').on('change', tambahItem);
		$('.btn-delete-item').on('click', deleteItem);
    }

    function deleteItem() {
        var i = $(this).data('id');
        var detail = list_material_adjustment_moqs[i];
        var purchase_item_id = detail.purchase_item_id;
        var qty_adjustment = parseFloat(detail.qty_adjustment);


		for (idx in list_material_moqs) {
			var data = list_material_moqs[idx];
			if (data.id == detail.purchase_item_id) {
				var qty_moq = parseFloat(data.qty_moq);
				data.qty_moq = parseFloat(qty_moq + qty_adjustment).toFixed(4);
			}
        }
        
        list_material_adjustment_moqs.splice(i, 1);
        removeHeader(purchase_item_id);
        renderDetail();
        renderHeader();
        setFocusToTextBox();
    }

    function removeHeader(purchase_item_id){
		for (id in list_material_moqs) {
            var data = list_material_moqs[id];
           if (data.id == purchase_item_id)
			{

				var temp = 0;
				for (idx in list_material_adjustment_moqs) {
					var data_2 = list_material_adjustment_moqs[idx];
					if (data_2.purchase_item_id == purchase_item_id) temp++;
				}
                
               if (temp == 0) list_material_moqs.splice(id, 1);
			}
		}
	}

    function showDetail(){
		var id = $(this).data('id');
		var data = list_material_moqs[id];
		var purchase_item_id = data.id;
		
		setSelected(purchase_item_id);
		$('#material_moqs').val(JSON.stringify(sortList(list_material_moqs, 'sorting')));
		renderHeader();
		renderDetail();
    }
    
    function checkHeaderExists(document_no,item_code,c_bpartner_id,po_buyer,warehouse){
		var flag = 0;
		for (var idx in list_material_moqs) {
			var data = list_material_moqs[idx];
            if (data.document_no == document_no 
                && data.item_code == item_code 
                && data.c_bpartner_id == c_bpartner_id 
                && data.po_buyer == po_buyer 
                && data.warehouse == warehouse) flag++;
		}

		return flag;
    }
    
    function setSelected(purchase_item_id){
		// selected planning
		var planning_date = null;
		for (idx in list_material_moqs) {
			var data = list_material_moqs[idx];
			if (data.id == purchase_item_id ){
				data.sorting = 0;
				data.is_selected = true;

			}else{
				data.sorting = 1;
				data.is_selected = false;
			}
		}

		// selected preparation
		for (idx in list_material_adjustment_moqs) {
			var data = list_material_adjustment_moqs[idx];
			if (data.purchase_item_id == purchase_item_id ){
				data.sorting = 0;
				data.is_selected = true;
			}else{
				data.sorting = 1;
				data.is_selected = false;
			}
		}
    }
    
    function selectedHeader(purchase_item_id,qty_conversion,row_detail){
		var qty_conversion = parseFloat(qty_conversion);
		var qty_booking = parseFloat(0);
		var is_complate = 0;
		//var detail = list_material_adjustment_moqs[row_detail];

        for (idx in list_material_moqs) {
			var data = list_material_moqs[idx];

            if (data.id == purchase_item_id){
                if (parseFloat(data.qty_moq) <= 0.00) {
                    $("#alert_info_2").trigger("click", 'kebutuhan untuk item ' + data.item_code + ' asal dari ' + data.document_no + ' sudah terpenuhi semua');
                    list_material_adjustment_moqs.splice(row_detail, 1);
                    removeHeader(data.id);
                    is_complate++;
                    break;
                }

                    
                if ((qty_conversion / parseFloat(data.qty_moq)) >= 1) var reserved_qty = parseFloat(data.qty_moq);
                else var reserved_qty = parseFloat(qty_conversion);

                qty_conversion -= parseFloat(reserved_qty);
                data.qty_moq = parseFloat(parseFloat(data.qty_moq) - parseFloat(reserved_qty)).toFixed(4);
                qty_booking += parseFloat(reserved_qty);

                if (qty_conversion == 0)
                    break;
                
            }
				
		}

		if (is_complate == 0){
			var qty_adjustment = parseFloat(list_material_adjustment_moqs[row_detail].qty_adjustment);
			list_material_adjustment_moqs[row_detail].qty_adjustment = parseFloat(qty_adjustment + qty_booking).toFixed(4);
            list_material_adjustment_moqs[row_detail].qty_conversion = parseFloat(qty_conversion).toFixed(4);
            if(qty_conversion <= 0) list_material_adjustment_moqs[row_detail].note = 'BARCODE SUDAH TIDAK DIGUNAKAN KARNA QTY DI PAKAI UNTUK MOQ';
		}
		

		$('#material_moqs').val(JSON.stringify(sortList(list_material_moqs, 'sorting')));

	}

    function tambahItem(){
        var barcode_val = $('#barcode_value').val();
        var preparation_url = $('#url_preperation_picklist').attr('href');
        /*var diff = checkItem(barcode_val);

        if (!diff) {
            $("#barcode_value").val("");
            $(".barcode_value").focus();
            $("#alert_error").trigger("click", 'Item Sudah di scan.');
            return;
        }*/ 
        
        $.ajax({
            type: "GET",
            url: preparation_url,
            data: {
                barcode: barcode_val
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                var header = response.header;
                var detail = response.detail;

                var id = header.id;
                var supplier_name = header.supplier_name;
				var c_bpartner_id = header.c_bpartner_id;
				var item_code = header.item_code;
				var document_no = header.document_no;
				var warehouse = header.warehouse;
				var po_buyer = header.po_buyer;
				var uom = header.uom;
				var qty_moq = header.credit_moq;

                var flag = checkHeaderExists(document_no,item_code,c_bpartner_id,po_buyer,warehouse);
                if(flag == 0){
                    var input = {
                        'id' :id,
                        'supplier_name' :supplier_name,
                        'c_bpartner_id' :c_bpartner_id,
                        'item_code' :item_code,
                        'document_no' :document_no,
                        'warehouse' :warehouse,
                        'po_buyer' :po_buyer,
                        'uom' :uom,
                        '_qty_moq' :qty_moq,
                        'qty_moq' :qty_moq,
                        'is_selected' :false,
                    };
                    list_material_moqs.push(input);
                }

                list_material_adjustment_moqs.push(detail);
                setSelected(detail.purchase_item_id);
                selectedHeader(detail.purchase_item_id,detail.qty_conversion, list_material_adjustment_moqs.length - 1);
                $('#material_moqs').val(JSON.stringify(sortList(list_material_moqs, 'sorting')));
            },
            error: function (response) {
                $.unblockUI();
                $('#barcode_value').val('');
                $('.barcode_value').focus();

                if (response['status'] == 500)
                    $("#alert_error").trigger("click", 'Please Contact ICT');

                if (response['status'] == 422)
                    $("#alert_error").trigger("click", response.responseJSON);

                list_material_adjustment_moqs = [];
                renderHeader();
                renderDetail();
            }
        })
        .done(function () {
            
            $('#barcode_value').val('');
            $('.barcode_value').focus();
            renderHeader();
            renderDetail();
            setFocusToTextBox();
        });;
    }

    function setFocusToTextBox() {
        $('#barcode_value').focus();
    }

    
    renderDetail();
    renderHeader();
    setFocusToTextBox();

    $('#form').submit(function (event) {
        event.preventDefault();

        bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        $("#alert_success").trigger("click", 'Material Berhasil di Ubah');
                    },
                    error: function (response) {
                        $.unblockUI();
                        $('#barcode_value').val('');
                        $('.barcode_value').focus();

                        if (response['status'] == 500)
                            $("#alert_error").trigger("click", 'Please Contact ICT');

                        if (response['status'] == 422)
                            $("#alert_error").trigger("click", response.responseJSON);

                        if (response['status'] == 504)
                            $("#alert_error").trigger("click", 'request timeout');

                    }
                })
                .done(function (response) {
                    $('#barcode_value').val('');
                    $('.barcode_value').focus();
                    list_material_adjustment_moqs = [];
                    list_material_moqs = [];
                    renderDetail();
                    renderHeader();

                    
                    var arrStr = encodeURIComponent(JSON.stringify(response['data_print']));
                    if (arrStr != 'false') window.open('/barcode/accessories/print-barcode-adjustment-moq/printout?id=' + arrStr);

                });
            }
        });
    });
});

