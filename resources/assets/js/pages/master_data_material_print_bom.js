
$(function()
{
    var page = $('#page').val();
    
	if(page == 'index')
	{
		var flag_msg = $('#flag_msg').val();
		if (flag_msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');
		else if (flag_msg == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated.');

		$('#master_data_material_print_bom_table').DataTable({
			dom: 'Bfrtip',
			processing: true,
			serverSide: true,
			pageLength:100,
			deferRender:true,
			ajax: {
				type: 'GET',
				url: '/master-data/material-print-bom/data',
			},
			columns: [
				{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
				{data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
				{data: 'category', name: 'category',searchable:true,visible:true,orderable:true,width:'20%'},
				{data: 'is_ila', name: 'is_ila',searchable:false,visible:true,orderable:true,width:'20%'},
				{data: 'is_paxar', name: 'is_paxar',searchable:false,visible:true,orderable:true,width:'20%'},
				{data: 'is_from_buyer', name: 'is_from_buyer',searchable:false,visible:true,orderable:true,width:'20%'},
				{data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
			]
		});
	
		var dtable = $('#master_data_material_print_bom_table').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtable.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtable.search("").draw();
				}
				return;
		});
		dtable.draw();
	}else if(page == 'edit')
	{
		var is_ila 			= $('#is_ila').val();
		var is_paxar 		= $('#is_paxar').val();
		var is_from_buyer 	= $('#is_from_buyer').val();

		if(is_ila == 1) $("#checkbox_is_ila").parent().find(".switchery").prop('checked', true).trigger("click");
		if(is_paxar== 1) $("#checkbox_is_paxar").parent().find(".switchery").prop('checked', true).trigger("click");
		if(is_from_buyer ==1) $("#checkbox_is_from_buyer").parent().find(".switchery").prop('checked', true).trigger("click");
	}else if(page == 'import')
	{
		list_material_exclude = JSON.parse($('#material-exclude').val());
		render();
	} 
});

function hapus(url) 
{
    bootbox.confirm("Are you sure want to delete this data ?.", function (result) 
    {
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: "delete",
				url: url,
                beforeSend: function () 
                {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function (response) {
					$.unblockUI();
				},
			}).done(function ($result) {
				$("#alert_success").trigger("click", 'Data successfully deleted.');
				$('#master_data_material_print_bom_table').DataTable().ajax.reload();
			});
		}
	});
}

$('#form').submit(function (event) 
{
  event.preventDefault();
  var item_code		= $('#item_code').val();
  var is_ila  		= ($('#checkbox_is_ila').is(":checked") ? 1 : 0) ;
  var is_buyer  	= ($('#checkbox_is_from_buyer').is(":checked") ? 1 : 0) ;
  var is_paxar  	= ($('#checkbox_is_paxar').is(":checked") ? 1 : 0) ;
  var total_chacked	= parseInt(is_ila)+parseInt(is_paxar)+parseInt(is_buyer);

  if (!item_code) 
  {
    $("#alert_warning").trigger("click", 'Please insert item code first.');
    return false;
  }

  if (total_chacked == 0) 
  {
    $("#alert_warning").trigger("click", 'Please checked one of it.');
    return false;
  }
   
  bootbox.confirm("Are you sure want to save this data ?.", function (result) 
  {
    if (result) 
    {
        $.ajax({
            type: "POST",
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
            },
            error: function (response) 
            {
              $.unblockUI();
              if (response.status == 500) $("#alert_error").trigger("click", 'Please contact ICT');
              if (response.status == 422) $("#alert_error").trigger("click", response.responseJSON.message);
            }
        })
        .done(function () 
        {
            var url_master_data_material_print_bom_index	= $('#url_master_data_material_print_bom_index').attr('href');
            document.location.href  						= url_master_data_material_print_bom_index;
        });
    }
  });
});


function render() 
{
	$('#material_exclude').val(JSON.stringify(list_material_exclude));

	var tmpl = $('#material-exclude-table').html();
	Mustache.parse(tmpl);
	var data = { item: list_material_exclude };
	var html = Mustache.render(tmpl, data);
	$('#tbody-upload-material-exclude').html(html);
}

$('#upload_button').on('click', function () 
{
	$('#upload_file').trigger('click');
});

$('#upload_file').on('change', function () 
{
	$.ajax({
		type: "post",
		url: $('#upload_file_material_exclude').attr('action'),
		data: new FormData(document.getElementById("upload_file_material_exclude")),
		processData: false,
		contentType: false,
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) 
		{
			$("#alert_info_2").trigger("click", 'Upload successfully.');

			for (idx in response) {
				var data = response[idx];
				var input = 
				{
					'status'		: data.status,
					'item_code'		: data.item_code,
					'category'		: data.category,
					'is_ila'		: data.is_ila,
					'is_from_buyer'	: data.is_from_buyer,
					'is_paxar'		: data.is_paxar,
					'is_error'		: data.is_error,
				};
				list_material_exclude.push(input);
			}

		},
		error: function (response) 
		{
			$.unblockUI();
			$('#upload_file_material_exclude').trigger('reset');
			if (response['status'] == 500)
				$("#alert_error").trigger("click", 'Please Contact ICT.');

			if (response['status'] == 422)
				$("#alert_error").trigger("click", response.responseJSON);

		}
	})
	.done(function () {
		$('#upload_file_material_exclude').trigger('reset');
		render();
	});

})
