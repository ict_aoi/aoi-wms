<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/soal-internal-use', ['uses' => 'HomeController@soapInternalUse'])->name('home.soapInternalUse');
Route::get('/soap-movement', ['uses' => 'HomeController@soapMovement'])->name('home.soapMovement');
Route::get('/soap-complete-mrc', ['uses' => 'HomeController@soapCompleteMRC'])->name('home.soapCompleteMRC');
Route::get('/soap-out-issue', ['uses' => 'HomeController@soapOutIssue'])->name('home.soapOutIssue');
