<?php namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\User;
use App\Models\Item;
use App\Models\Locator;
use App\Models\Scheduler;
use App\Models\MaterialMovement;
use App\Models\MaterialMovementLine;

class UpdateVarianceStockFab extends Command
{
    protected $signature    = 'varianceStockFab:update';
    protected $description  = 'Sync variance stock fab';

    public function __construct()
    {
        parent::__construct();
    }

    
    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','VARIANCE_STOCK_FAB')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going){
            $new_scheduler = Scheduler::create([
                'job'       => 'VARIANCE_STOCK_FAB',
                'status'    => 'ongoing'
            ]);
            $this->info('SYNC VARIANCE STOCK FAB START JOB AT '.carbon::now());
            $this->setStartJob($new_scheduler);
            $this->doJob();
            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
            $this->info('SYNC VARIANCE STOCK FAB END JOB AT '.carbon::now());
        }else{
            $this->info('SYNC VARIANCE STOCK FAB SEDANG BERJALAN');
        }  
    }

    private function doJob()
    {
        DB::select(db::raw("SELECT * FROM update_integration_to_free_stock_fab();"));
        $movement_date  = Carbon::now();
        $data           = DB::table('variance_stock_fab_wms_vs_erp_v')->get();

        if(count($data) > 0)
        {
            foreach ($data as $key => $value) 
            {
                try 
                {
                    DB::beginTransaction();

                    $item_id        = $value->item_id;
                    $warehouse_id   = $value->warehouse_id;
                    $uom            = $value->uom_erp;
                    $operator       = sprintf("%.5f",$value->operator);
                    //$operator       = $value->operator;
                    $item           = Item::where('item_id',$item_id)->first();

                    $system = User::where([
                        ['name','system'],
                        ['warehouse',$warehouse_id]
                    ])
                    ->first();
                    
                    $inventory_erp = Locator::with('area')
                    ->where([
                        ['is_active',true],
                        ['rack','ERP INVENTORY'],
                    ])
                    ->whereHas('area',function ($query) use($warehouse_id)
                    {
                        $query->where('is_active',true);
                        $query->where('warehouse',$warehouse_id);
                        $query->where('name','ERP INVENTORY');
                    })
                    ->first();

                    $is_movement_integration_exists = MaterialMovement::where([
                        ['from_location',$inventory_erp->id],
                        ['to_destination',$inventory_erp->id],
                        ['from_locator_erp_id',$inventory_erp->area->erp_id],
                        ['to_locator_erp_id',$inventory_erp->area->erp_id],
                        ['po_buyer','-'],
                        ['is_integrate',false],
                        ['is_active',true],
                        ['no_packing_list','-'],
                        ['no_invoice','-'],
                        ['status','integration-to-inventory-erp'],
                    ])
                    ->first();
                    
                    if(!$is_movement_integration_exists)
                    {
                        $movement_integration = MaterialMovement::firstOrCreate([
                            'from_location'         => $inventory_erp->id,
                            'to_destination'        => $inventory_erp->id,
                            'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                            'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                            'po_buyer'              => '-',
                            'is_integrate'          => false,
                            'is_active'             => true,
                            'no_packing_list'       => '-',
                            'no_invoice'            => '-',
                            'status'                => 'integration-to-inventory-erp',
                            'created_at'            => $movement_date,
                            'updated_at'            => $movement_date,
                        ]);

                        $movement_integration_id = $movement_integration->id;
                    }else
                    {
                        $is_movement_integration_exists->updated_at = $movement_date;
                        $is_movement_integration_exists->save();

                        $movement_integration_id = $is_movement_integration_exists->id;
                    }

                    $is_material_movement_line_integration_exists = MaterialMovementLine::whereNull('material_preparation_id')
                    ->whereNull('material_stock_id')
                    ->where([
                        ['material_movement_id',$movement_integration_id],
                        ['qty_movement',$operator],
                        ['item_id',$item_id],
                        ['warehouse_id',$warehouse_id],
                        ['is_integrate',false],
                        ['is_active',true],
                        ['date_movement',$movement_date],
                    ])
                    ->exists();

                    if(!$is_material_movement_line_integration_exists)
                    {
                        MaterialMovementLine::Create([
                            'material_movement_id'          => $movement_integration_id,
                            'item_code'                     => $item->item_code,
                            'item_id'                       => $item_id,
                            'item_id_source'                => null,
                            'c_order_id'                    => '-',
                            'c_orderline_id'                => '-',
                            'c_bpartner_id'                 => '-',
                            'supplier_name'                 => '-',
                            'type_po'                       => 1,
                            'is_integrate'                  => false,
                            'is_active'                     => true,
                            'uom_movement'                  => $uom,
                            'qty_movement'                  => $operator,
                            'date_movement'                 => $movement_date,
                            'created_at'                    => $movement_date,
                            'updated_at'                    => $movement_date,
                            'date_receive_on_destination'   => $movement_date,
                            'nomor_roll'                    => '-',
                            'warehouse_id'                  => $warehouse_id,
                            'document_no'                   => '-',
                            'note'                          => 'ADJ VARIANCE WMS FAB',
                            'is_active'                     => true,
                            'user_id'                       => $system->id,
                        ]);
                    }

                    DB::commit();

                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }
        }
        
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler)
    {
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
