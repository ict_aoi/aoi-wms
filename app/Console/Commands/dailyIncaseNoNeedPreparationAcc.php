<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Models\Locator;
use App\Models\PurchaseItem;
use App\Models\MaterialReadyPreparation;
use App\Models\UomConversion;
use App\Models\MaterialPreparation;
use App\Models\DetailMaterialPreparation;
use App\Models\MaterialRequirement;
use App\Models\MaterialMovement;
use App\Models\MaterialMovementLine;
use App\Models\Temporary;

class dailyIncaseNoNeedPreparationAcc extends Command
{
    protected $signature    = 'dailyIncaseNoNeedPreparationAcc:insert';
    protected $description  = 'Insert Preparation when data not inserted';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $data           =  DB::table('incase_no_need_prepared_acc')->get();
        $concatenate    = '';


        try
        {
            db::beginTransaction();
            foreach ($data as $key => $value) 
            {
                $po_detail_id           = $value->po_detail_id;
                $supplier_name          = $value->supplier_name;
                $warehouse_id           = $value->warehouse_id;
                $material_subcont_id    = $value->material_subcont_id;
                $po_detail_id           = $value->po_detail_id;
                $is_backlog             = false;
                $item_desc              = $value->item_desc;
                $item_code              = $value->item_code;
                $c_order_id             = $value->c_order_id;
                $document_no            = $value->document_no;
                $po_buyer               = $value->po_buyer;
                $category               = $value->category;
                $item_id                = $value->item_id;
                $c_bpartner_id          = $value->c_bpartner_id;
                $uom                    = $value->uom;
                $movement_date          = $value->movement_date;
                $user_id                = $value->user_id;
                $qty_carton             = $value->qty_carton;
                $is_need_to_be_switch   = false;

                $receiving_location = Locator::with('area')
                ->whereHas('area',function ($query) use($warehouse_id) {
                    $query->where('is_destination',false);
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','RECEIVING');
                })
                ->first();

                $handover_location = Locator::with('area')
                ->whereHas('area',function ($query) use($warehouse_id){
                    $query->where('is_destination',false);
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','HANDOVER');
                })
                ->first();

                $supplier_location = Locator::with('area')
                ->whereHas('area',function ($query) use($warehouse_id){
                    $query->where('is_destination',false);
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','SUPPLIER');
                })
                ->first();

                $bom_location = Locator::with('area')
                ->whereHas('area',function ($query) use($warehouse_id){
                    $query->where('is_destination',false);
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','BOM');
                })
                ->first();

                $material_ready_prepare = MaterialReadyPreparation::with('materialArrival')
                ->where([
                    ['item_code',$item_code],
                    ['document_no',$document_no],
                    ['po_buyer',$po_buyer],
                ])
                ->whereHas('materialArrival',function ($query) use ($po_detail_id,$warehouse_id){
                    $query->where('warehouse_id',$warehouse_id);
                    $query->where('po_detail_id',$po_detail_id);
                })
                ->first();

                $is_exists = MaterialPreparation::where([
                    ['barcode',$po_detail_id],
                    ['warehouse',$warehouse_id]
                ])
                ->first();

                if(!$is_exists)
                {
                    if($is_backlog)
                    {
                        $conversion = UomConversion::where([
                            ['item_code',$item_code],
                            ['category',$category],
                            ['uom_to',$uom]
                        ])
                        ->first();

                        if($conversion) $multiplyrate = $conversion->multiplyrate;
                        else $multiplyrate = 1;
                            
                        $dividerate     = 1;
                        $uom_conversion = $uom;
                    }else
                    {
                        $conversion = UomConversion::where([
                            ['item_code',$item_code],
                            ['category',$category],
                            ['uom_from',$uom]
                        ])
                        ->first();

                        if($conversion)
                        {
                            $dividerate     = $conversion->dividerate;
                            $multiplyrate   = $conversion->multiplyrate;
                            $uom_conversion = $conversion->uom_to;
                        }else
                        {
                            $dividerate     = 1;
                            $multiplyrate   = 1;
                            $uom_conversion = $uom;
                        }
                    }
                    
                    if($material_subcont_id)
                    {
                        $copy_preparation = MaterialPreparation::whereIn('id',function($query) use($material_subcont_id) 
                        {
                            $query->select('material_preparation_id')
                            ->from('material_movement_lines')
                            ->whereIn('id',function($query2) use ($material_subcont_id){
                                $query2->select('material_movement_line_id')
                                ->from('material_subconts')
                                ->where('id',$material_subcont_id);
                            })
                            ->groupby('material_preparation_id');
                        })
                        ->first();

                        $is_exists = MaterialPreparation::where([
                            'item_id'               => $copy_preparation->item_id,
                            'c_bpartner_id'         => $copy_preparation->c_bpartner_id,
                            'barcode'               => $copy_preparation->barcode,
                            'document_no'           => $copy_preparation->document_no,
                            'po_buyer'              => $copy_preparation->po_buyer,
                            'qty_conversion'        => $copy_preparation->qty_conversion,
                            'style'                 => $copy_preparation->style,
                            'warehouse'             => $warehouse_id,
                            'item_code'             => $copy_preparation->item_code,
                            'total_carton'          => $copy_preparation->total_carton,
                            'type_po'               => 2,
                            'last_status_movement'  => 'receiving',
                            'last_locator_id'       => $receiving_location->id,
                            'last_movement_date'    => $movement_date,
                            'last_user_movement_id' => $user_id
                        ])
                        ->exists();

                        if(!$is_exists)
                        {

                            $material_preparation = MaterialPreparation::Create([
                                'item_id'               => $copy_preparation->item_id,
                                'c_order_id'            => $copy_preparation->c_order_id,
                                'c_bpartner_id'         => $copy_preparation->c_bpartner_id,
                                'supplier_name'         => $copy_preparation->supplier_name,
                                'barcode'               => $copy_preparation->barcode,
                                'document_no'           => $copy_preparation->document_no,
                                'po_buyer'              => $copy_preparation->po_buyer,
                                'is_moq'                => $copy_preparation->is_moq,
                                'is_backlog'            => $copy_preparation->is_backlog,
                                'uom_conversion'        => $copy_preparation->uom_conversion,
                                'qty_conversion'        => $copy_preparation->qty_conversion,
                                'qty_reconversion'      => $copy_preparation->qty_reconversion,
                                'job_order'             => $copy_preparation->job_order,
                                'style'                 => $copy_preparation->style,
                                'article_no'            => $copy_preparation->article_no,
                                'warehouse'             => $warehouse_id,
                                'item_code'             => $copy_preparation->item_code,
                                'item_desc'             => $copy_preparation->item_desc,
                                'category'              => $copy_preparation->category,
                                'qc_status'             => $copy_preparation->qc_status,
                                'total_carton'          => $copy_preparation->total_carton,
                                'is_allocation'         => $copy_preparation->is_allocation,
                                '_style'                => $copy_preparation->_style,
                                'referral_code'         => $copy_preparation->referral_code,
                                'sequence'              => $copy_preparation->sequence,
                                'type_po'               => 2,
                                'is_from_handover'      => true,
                                'is_need_to_be_switch'  => $is_need_to_be_switch,
                                'user_id'               => $user_id,
                                'last_status_movement'  => 'receiving',
                                'last_locator_id'       => $receiving_location->id,
                                'last_movement_date'    => $movement_date,
                                'last_user_movement_id' => $user_id
                            ]);

                            //insert detail
                            DetailMaterialPreparation::Create([
                                'material_preparation_id'   => $material_preparation->id,
                                'material_subcont_id'       => $material_subcont_id,
                                'user_id'                   => $user_id
                            ]);
                            
                            $material_movement = MaterialMovement::firstorcreate([
                                'from_location'         => $handover_location->id,
                                'to_destination'        => $receiving_location->id,
                                'is_active'             => true,
                                'po_buyer'              => $po_buyer,
                                'status'                => 'receive'
                            ]);
                            
                            MaterialMovementLine::create([
                                'material_movement_id'      => $material_movement->id,
                                'material_preparation_id'   => $material_preparation->id,
                                'item_code'                 => $material_preparation->item_code,
                                'type_po'                   => $material_preparation->type_po,
                                'qty_movement'              => $material_preparation->qty_conversion,
                                'date_movement'             => $movement_date,
                                'created_at'                => $movement_date,
                                'updated_at'                => $movement_date,
                                'is_active'                 => false,
                                'user_id'                   => $user_id
                            ]);

                            $concatenate .= "'" .$material_preparation->barcode."',";
                        }

                    }else
                    {
                        $material_requirement = MaterialRequirement::where([
                            ['item_code',$item_code],
                            ['category',$category],
                            ['po_buyer',$po_buyer]
                        ])
                        ->first();

                        $qty_upload             = $value->qty_upload;
                        $material_arrival_id    = $value->id;
                        $qty_conversion         = ($qty_upload) * $dividerate;

                        if($material_requirement)
                        {
                            $article_no         = $material_requirement->article_no;
                            $job_order          = $material_requirement->job_order;
                            $style              = $material_requirement->style;
                        }else
                        {
                            $article_no         = null;
                            $job_order          = null;
                            $style              = null;
                        }

                        $is_exists = MaterialPreparation::where([
                            'item_id'               => $item_id,
                            'c_bpartner_id'         => $c_bpartner_id,
                            'barcode'               => $po_detail_id,
                            'document_no'           => $document_no,
                            'po_buyer'              => $po_buyer,
                            'qty_conversion'        => $qty_conversion,
                            'style'                 => $style,
                            'warehouse'             => $warehouse_id,
                            'item_code'             => $item_code,
                            'type_po'               => 2,
                            'user_id'               => $user_id,
                            'last_status_movement'  => 'receiving',
                            'last_locator_id'       => $receiving_location->id,
                            'last_movement_date'    => $movement_date,
                            'last_user_movement_id' => $user_id
                        ])
                        ->exists();

                        if(!$is_exists)
                        {
                            $is_stock_already_created  = PurchaseItem::where([
                                [db::raw('upper(document_no)'),strtoupper($document_no)],
                                ['po_buyer',$po_buyer],
                                ['c_bpartner_id',$c_bpartner_id],
                                ['warehouse',$warehouse_id],
                                [db::raw('upper(item_code)'),strtoupper($item_code)],
                                ['is_stock_already_created',true]
                            ])
                            ->exists();

                            $material_preparation = MaterialPreparation::Create([
                                'item_id'                   => $item_id,
                                'c_order_id'                => $c_order_id,
                                'c_bpartner_id'             => $c_bpartner_id,
                                'supplier_name'             => $supplier_name,
                                'barcode'                   => $po_detail_id,
                                'document_no'               => $document_no,
                                'po_buyer'                  => $po_buyer,
                                'is_moq'                    => false,
                                'is_backlog'                => false,
                                'uom_conversion'            => $uom_conversion,
                                'qty_conversion'            => $qty_conversion,
                                'qty_reconversion'          => $qty_conversion * $multiplyrate,
                                'job_order'                 => $job_order,
                                'style'                     => $style,
                                '_style'                    =>  explode('::',$job_order)[0],
                                'article_no'                => $article_no,
                                'warehouse'                 => $warehouse_id,
                                'item_code'                 => $item_code,
                                'item_desc'                 => $item_desc,
                                'category'                  => $category,
                                'total_carton'              => intval($qty_carton),
                                'type_po'                   => 2,
                                'is_stock_already_created'  => $is_stock_already_created,
                                'is_need_to_be_switch'      => $is_need_to_be_switch,
                                'user_id'                   => $user_id,
                                'last_status_movement'      => 'receiving',
                                'last_locator_id'           => $receiving_location->id,
                                'last_movement_date'        => $movement_date,
                                'last_user_movement_id'     => $user_id
                            ]);

                            //insert detail
                            DetailMaterialPreparation::Create([
                                'material_preparation_id'   => $material_preparation->id,
                                'material_arrival_id'       => $material_arrival_id,
                                'user_id'                   => $user_id
                            ]);

                            $material_movement = MaterialMovement::firstorcreate([
                                'from_location'     => $supplier_location->id,
                                'to_destination'    => $receiving_location->id,
                                'is_active'         => true,
                                'po_buyer'          => $po_buyer,
                                'status'            => 'receive'
                            ]);


                            MaterialMovementLine::create([
                                'material_movement_id'      => $material_movement->id,
                                'material_preparation_id'   => $material_preparation->id,
                                'item_code'                 => $material_preparation->item_code,
                                'type_po'                   => $material_preparation->type_po,
                                'qty_movement'              => $material_preparation->qty_conversion,
                                'date_movement'             => $movement_date,
                                'created_at'                => $movement_date,
                                'updated_at'                => $movement_date,
                                'is_active'                 => false,
                                'user_id'                   => $user_id
                            ]);

                            $concatenate .= "'" .$material_preparation->barcode."',";
                        }
                    }
                    
                    $temporary = Temporary::Create([
                        'barcode'   => $po_buyer,
                        'status'    => 'mrp',
                        'user_id'   => $user_id
                    ]);
                }
            }

            $concatenate = substr_replace($concatenate, '', -1);
            if($concatenate !='')
            {
                DB::select(db::raw("SELECT * FROM delete_duplicate_preparation_acc(array[".$concatenate."]);"));
                DB::select(db::raw("SELECT * FROM delete_duplicate_movement(array[".$concatenate."]);"));
            }
            db::commit();
        }catch (Exception $ex){
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
    }
}
