<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\MovementOutController;

class IncaseSubcont extends Command
{
    protected $signature = 'incasesubconts:insert';
    protected $description = 'inscase insert subcont';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        MovementOutController::insertSubcontSchedular();
        $this->info('Incase INSERT SUBCONT has been successfully');
    }
}
