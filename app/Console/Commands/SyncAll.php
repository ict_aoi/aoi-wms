<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SyncAll extends Command
{
    protected $signature = 'sync:all';
    protected $description = 'Melakukan sinkronisasi semua';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->call('sync:item');
        $this->call('sync:uomConversion');
        $this->call('sync:supplier');
    }
}
