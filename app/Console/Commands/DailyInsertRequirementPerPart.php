<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ErpMaterialRequirementController;

class DailyInsertRequirementPerPart extends Command
{
    protected $signature = 'dailyRequirementPerPart:insert';
    protected $description = 'Insert po buyer from LC for requirement material daily';
    
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(){
        ErpMaterialRequirementController::daily_cron_insert_per_part();
        $this->info('Daily Insert has been successfully');
    }
}
