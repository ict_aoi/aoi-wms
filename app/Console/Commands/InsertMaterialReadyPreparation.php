<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\MaterialArrivalController;

class InsertMaterialReadyPreparation extends Command
{
    protected $signature = 'incasematerialreadypreparations:insert';
    protected $description = 'inscase insert MATERIAL READY PREPARATION';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        MaterialArrivalController::insert_material_ready_preparations();
        $this->info('Incase INSERT MATERIAL READY PREPARATION has been successfully');
    }
}
