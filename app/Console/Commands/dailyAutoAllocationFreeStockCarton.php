<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class dailyAutoAllocationFreeStockCarton extends Command
{
    protected $signature = 'dailyAutoAllocationFreeStockCarton:insert';
    protected $description = 'update stock reserve acc';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','SYNC_AUTO_ALLOCATION_CARTON')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job' => 'SYNC_AUTO_ALLOCATION_CARTON',
                'status' => 'ongoing'
            ]);
            $this->setStartJob($new_scheduler);
            
            $this->info('SYNC AUTO ALLOCATION CARTON START JOB AT '.carbon::now());
            $this->doJob();
            $this->info('SYNC AUTO ALLOCATION CARTON  END JOB AT '.carbon::now());

            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
        }else{
            $this->info('SYNC AUTO ALLOCATION CARTON SEDANG BERJALAN');
        } 
    }

    static function doJob()
    {
        try 
        {
            DB::beginTransaction();
                AccessoriesBarcodeSupplierController::autoAllocationFreeStockCarton();
            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

    } 

    private function setStatus($scheduler,$status)
    {
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler)
    {
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler)
    {
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}

