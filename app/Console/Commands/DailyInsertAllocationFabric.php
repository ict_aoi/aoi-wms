<?php namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Scheduler;
use App\Http\Controllers\MasterDataAutoAllocationController;

class DailyInsertAllocationFabric extends Command
{
    protected $signature = 'InsertAllocationFabric:daily';
    protected $description = 'insert allocation fabric daily';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','SYNC_INSERT_ALLOCATION_FAB')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job'       => 'SYNC_INSERT_ALLOCATION_FAB',
                'status'    => 'ongoing'
            ]);
            $this->info('SYNC INSERT ALLOCATION FABRIC JOB AT '.carbon::now());
            $this->setStartJob($new_scheduler);
            MasterDataAutoAllocationController::scheduleInsertAllocationFabric();
            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
            $this->info('DONE INSERT ALLOCATION FABRIC JOB AT '.carbon::now());
        }else{
            $this->info('SYNC INSERT ALLOCATION FABRIC IS RUNNING');
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
