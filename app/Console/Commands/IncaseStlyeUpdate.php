<?php namespace App\Console\Commands;
use Illuminate\Console\Command;
use App\Http\Controllers\ErpMaterialRequirementController;

class IncaseStlyeUpdate extends Command
{
    protected $signature = 'incasepobuyer:update';
    protected $description = 'update po buyer incase';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        ErpMaterialRequirementController::dailyUpdatePobuyer();
        $this->info('Incase Update has been successfully');
    }
}
