<?php namespace App\Console\Commands;

use Carbon\Carbon;
use App\Http\Controllers\ERPController;
use Illuminate\Console\Command;

class DailyInsertMasterErp extends Command
{
    protected $signature = 'dailymastererp:insert';
    protected $description = 'Insert to get latest data master from ERP';

    public function __construct(){
        parent::__construct();
    }

    public function handle(){
        $this->info('START SCHEDULE INSERT MASTER UOM AT '.carbon::now());
        ERPController::uomCronInsert();
        $this->info('SCHEDULE INSERT MASTER UOM HAS BEEN SUCCESSFULLY AT '.carbon::now());

        $this->info('START SCHEDULE INSERT MASTER ITEM AT '.carbon::now());
        ERPController::itemCronInsert();
        $this->info('SCHEDULE INSERT MASTER ITEM HAS BEEN SUCCESSFULLY AT '.carbon::now());

        $this->info('START SCHEDULE INSERT MASTER SUPPLIER AT '.carbon::now());
        ERPController::supplierCronInsert();
        $this->info('SCHEDULE INSERT MASTER SUPPLIER HAS BEEN SUCCESSFULLY AT '.carbon::now());

        $this->info('START SCHEDULE INSERT PO PO SUPPLIER AT '.carbon::now());
        ERPController::poSupplierCronInsert();
        $this->info('SCHEDULE INSERT MASTER PO SUPPLIER HAS BEEN SUCCESSFULLY AT '.carbon::now());

        $this->info('START SCHEDULE INSERT PURCHASE TO ALLOCATION FABRIC AT '.carbon::now());
        ERPController::dailyInsertPurchaseAllocationItems();
        $this->info('SCHEDULE INSERT PURCHASE TO ALLOCATION FABRIC HAS BEEN SUCCESSFULLY AT '.carbon::now());
    }
}
