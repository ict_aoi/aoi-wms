<?php

namespace App\Console\Commands;
use DB;
use Carbon\Carbon;
use App\Models\Temporary;
use App\Models\Scheduler;
use App\Models\AutoAllocation;
use App\Models\HistoryAutoAllocations;
use App\Http\Controllers\AccessoriesReportMaterialStockController as ReportStock;
use App\Models\MaterialStock;

use Illuminate\Console\Command;
use App\Http\Controllers\AccessoriesBarcodeSupplierController;

class DailyAutoAllocationCarton extends Command
{
    protected $signature = 'dailyAutoAllocationCarton:insert';
    protected $description = 'update stock reserve acc';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','SYNC_AUTO_ALLOCATION_CARTON')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job' => 'SYNC_AUTO_ALLOCATION_CARTON',
                'status' => 'ongoing'
            ]);
            $this->setStartJob($new_scheduler);
            
            $this->info('SYNC AUTO ALLOCATION CARTON START JOB AT '.carbon::now());
            $this->doJob();
            $this->info('SYNC AUTO ALLOCATION CARTON  END JOB AT '.carbon::now());

            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
        }else{
            $this->info('SYNC AUTO ALLOCATION CARTON SEDANG BERJALAN');
        } 
    }

    static function doJob()
    {

        $refresh = DB::select(db::raw("
        refresh materialized view material_requirement_cartons_mv;"
        ));


        $items = db::table('material_requirement_carton_per_item_v')
        ->get();

        foreach($items as $key => $item)
        {

            $item_id      = $item->item_id;
            $warehouse_id = $item->warehouse_id;
            //$total_qty_need     = $item->qty_outstanding;

            $material_stocks = MaterialStock::select('id',
            'item_id',
            'item_code',
            'c_order_id',
            'warehouse_id',
            'type_stock',
            'type_stock_erp_code',
            'c_bpartner_id',
            'supplier_name',
            'document_no',
            'item_desc',
            'category',
            'uom',
            'available_qty')
            ->where('item_id',$item_id)
            ->where('warehouse_id',$warehouse_id)
            ->where('deleted_at',null)
            ->where('available_qty','>','0')
            ->where('is_running_stock', false)
            ->where('is_closing_balance', false)
            ->where('is_allocated', false)
            ->where('is_active', true)
            ->where('is_stock_on_the_fly', false)
            ->get();

            //dd($material_stocks);

            foreach($material_stocks as $key2 => $material_stock)
            {
                $material_stock_id      = $material_stock->id;
                $c_order_id             = $material_stock->c_order_id;
                $type_stock             = $material_stock->type_stock;
                $type_stock_erp_code    = $material_stock->type_stock_erp_code;
                $c_bpartner_id          = $material_stock->c_bpartner_id;
                $supplier_name          = $material_stock->supplier_name;
                $document_no            = $material_stock->document_no;
                $item_id                = $material_stock->item_id;
                $item_code_source       = $material_stock->item_code;
                $item_code_book         = $material_stock->item_code;
                $item_desc              = $material_stock->item_desc;
                $category               = $material_stock->category;
                $uom                    = $material_stock->uom;
                $warehouse_name         = $material_stock->warehouse_id;
                $warehouse_id           = $material_stock->warehouse_id;
                $available_qty          = $material_stock->available_qty;

                $refresh = DB::select(db::raw("
                refresh materialized view material_requirement_cartons_mv;"
                ));

                $material_requirements = db::table('material_requirement_cartons_mv')
                                         ->where('item_id', $item_id)
                                         ->where('warehouse_id',$warehouse_id)
                                        ->where('qty_outstanding', '>', 0)
                                        ->orderBy('psd', 'asc')
                                        ->orderBy('promise_date', 'asc')
                                        ->orderBy('qty_required', 'asc')
                                        ->get();

                //dd($material_stock->item_id, $material_stock->id);

                $movement_date = carbon::now();

                try 
                {
                DB::beginTransaction();
                //cek kebutuhan alokasinya 
                foreach($material_requirements as $key2 => $material_requirement)
                {
                    $booking_number = $material_requirement->batch_code;
                    $po_buyer       = $material_requirement->po_buyer;
                    $article_no     = $material_requirement->article_no;
                    $qty_required   = $material_requirement->qty_required;
                    $promise_date   = $material_requirement->promise_date;
                    $lc_date        = $material_requirement->lc_date;
                    $season         = $material_requirement->season;
                    $qty_in_house   = $material_requirement->qty_allocation;
                    $qty_need       = $material_requirement->qty_outstanding;
                    //dd($po_buyer, $item_id, $qty_need);
                    if($qty_need > 0 && $available_qty > 0)
                    {
                        if($available_qty >= $qty_need)
                        {
                            $qty_allocation  = $qty_need;
                            $available_qty  -= $qty_need;
                            //$total_qty_need -= $qty_need;
                        }
                        else
                        {
                            $qty_allocation  = $available_qty;
                            $available_qty  -= $available_qty;
                            //$total_qty_need -= $available_qty;
                        }
                    }
                    else
                    {
                        $qty_allocation = 0;
                        $available_qty  = 0;
                        break;
                    }
                    //dd($qty_allocation, $available_qty);
                    if($qty_allocation > 0)
                    {
                        $exists = AutoAllocation::where('document_allocation_number', $booking_number)
                                  ->where('po_buyer', $po_buyer)
                                  ->where('item_id_source', $item_id)
                                  ->where('c_order_id', $c_order_id)
                                  ->where('warehouse_id', $warehouse_id)
                                  ->where('qty_allocation', $qty_allocation)
                                  ->whereNull('deleted_at')
                                  ->exists();

                        if($exists == false)
                        {
                            //jika kebutuhan ada buat alokasinya
                            $auto_allocation = AutoAllocation::FirstOrCreate([
                                'document_allocation_number'        => $booking_number,
                                'lc_date'                           => $lc_date,
                                'season'                            => $season,
                                'document_no'                       => $document_no,
                                'c_order_id'                        => $c_order_id,
                                'c_bpartner_id'                     => $c_bpartner_id,
                                'supplier_name'                     => $supplier_name,
                                'po_buyer'                          => $po_buyer,
                                'old_po_buyer'                      => $po_buyer,
                                'item_code_source'                  => $item_code_source,
                                'item_id_book'                      => $item_id,
                                'item_id_source'                    => $item_id,
                                'item_code'                         => $item_code_book,
                                'item_code_book'                    => $item_code_book,
                                'article_no'                        => $article_no,
                                'item_desc'                         => $item_desc,
                                'category'                          => $category,
                                'uom'                               => $uom,
                                'warehouse_name'                    => $warehouse_name,
                                'warehouse_id'                      => $warehouse_id,
                                'qty_allocation'                    => $qty_allocation,
                                'qty_outstanding'                   => $qty_allocation,
                                'qty_allocated'                     => 0,
                                'is_fabric'                         => false,
                                'remark'                            => 'UPLOAD AUTO ALLOCATION CARTON',
                                'remark_update'                     => null,
                                'is_already_generate_form_booking'  => false,
                                'is_upload_manual'                  => true,
                                'type_stock'                        => $type_stock,
                                'type_stock_erp_code'               => $type_stock_erp_code,
                                'promise_date'                      => $promise_date,
                                'deleted_at'                        => null,
                                'status_po_buyer'                   => 'active',
                                'created_at'                        => $movement_date,
                                'updated_at'                        => $movement_date,
                                'user_id'                           => '91'//auth::user()->id,
                            ]);

                            $note = 'TERJADI PENAMBAHAN ALOKASI';
                            $note_code = '1';

                            HistoryAutoAllocations::FirstOrCreate([
                                'auto_allocation_id'        => $auto_allocation->id,
                                'c_bpartner_id_new'         => $c_bpartner_id,
                                'supplier_name_new'         => $supplier_name,
                                'document_no_new'           => $document_no,
                                'item_id_new'               => $item_id,
                                'item_code_new'             => $item_code_book,
                                'item_source_id_new'        => $item_id,
                                'item_code_source_new'      => $item_code_source,
                                'po_buyer_new'              => $po_buyer,
                                'uom_new'                   => $uom,
                                'qty_allocated_new'         => $qty_allocation,
                                'warehouse_id_new'          => $warehouse_id,
                                'type_stock_new'            => $type_stock,
                                'type_stock_erp_code_new'   => $type_stock_erp_code,
                                'note_code'                 => $note_code,
                                'note'                      => $note,
                                'operator'                  => $qty_allocation,
                                'is_integrate'              => true,
                                'user_id'                   => '91'//auth::user()->id
                            ]);
                        }
                        else
                        {
                            break;
                        }
                        
                    }

                }

                DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                ReportStock::doAllocation($material_stock, '91', $movement_date);

            }


        }


    } 

    private function setStatus($scheduler,$status)
    {
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler)
    {
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler)
    {
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
