<?php namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;
use App\Http\Controllers\ErpMaterialRequirementController;

class WeekendRefreshMr extends Command
{
    protected $signature = 'weekendMRP:refresh';
    protected $description = 'Weekend MRP Refresh';
    
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        DB::select(db::raw("SELECT * FROM refresh_mrp();"));
        ErpMaterialRequirementController::daily_cron_insert();
        $this->info('Weekend Refresh has been successfully');
    }
}
