<?php namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\User;
use App\Models\Locator;
use App\Models\Scheduler;
use App\Models\MaterialStock;
use App\Models\MaterialCheck;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\MaterialPreparation;
use App\Models\MaterialMovementLine;
use App\Models\MaterialRollHandoverFabric;

class DailyIntegrationFreeStock extends Command
{
    protected $signature    = 'syncIntegrationFreeStock:daily';
    protected $description  = 'daily sync integration free stock';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','SYNC_INTEGRATION_FREE_STOCK')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job' => 'SYNC_INTEGRATION_FREE_STOCK',
                'status' => 'ongoing'
            ]);
            $this->setStartJob($new_scheduler);
            
            $this->info('SYNC DAILY INTEGRATION FREE STOCK START JOB AT '.carbon::now());
            $this->moveFromFreeStockToFreeStockErp();
            $this->moveFromFreeStockToInventoryErp();
            $this->accessoriesMoveFromInventoryToFreeStockForAdjustment();
            $this->accessoriesMoveFromInventoryToFreeStockForCancelOrReroute();
           // $this->accessoriesMoveFromInventoryToRejectErp();
            //$this->fabricMoveFromInventoryToRejectErp();
            //$this->fabricMoveHandover();
            $this->info('SYNC DAILY INTEGRATION FREE STOCK END JOB AT '.carbon::now());

            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
        }else{
            $this->info('SYNC DAILY INTEGRATION FREE STOCK SEDANG BERJALAN');
        } 
    }

    private function moveFromFreeStockToFreeStockErp()
    {
        $data = MaterialPreparation::where('is_need_inserted_to_locator_free_stock_erp',true)
        ->whereNull('insert_to_locator_free_stock_erp_date')
        ->get();
       
        
        $movement_date = carbon::now()->todatetimestring();
        foreach ($data as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();
                
                $material_preparation_id    = $value->id;
                $po_buyer                   = $value->po_buyer;
                $item_code                  = $value->item_code;
                $item_id                    = $value->item_id;
                $c_order_id                 = $value->c_order_id;
                $uom_conversion             = $value->uom_conversion;
                $c_bpartner_id              = $value->c_bpartner_id;
                $supplier_name              = $value->supplier_name;
                $document_no                = $value->document_no;
                $warehouse_id               = $value->warehouse;
                $created_at                 = $value->created_at;
                $type_po                    = $value->type_po;
                $user_id                    = $value->user_id;
                $po_detail_id               = $value->po_detail_id;
                $qty_conversion             = sprintf('%0.8f',$value->qty_conversion);

                if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                {
                    $no_packing_list        = null;
                    $no_invoice             = null;
                    $c_orderline_id         = null;
                }else
                {
                    $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                    ->whereNull('material_roll_handover_fabric_id')
                    ->where('po_detail_id',$po_detail_id)
                    ->first();

                    $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : null);
                    $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : null);
                    $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : null);
                }
            
            
                $from_location = Locator::where([
                    ['is_active',true],
                    ['is_destination',false],
                    ['rack','FREE STOCK'],
                ])
                ->whereHas('area',function ($query) use($warehouse_id)
                {
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','FREE STOCK');
                })
                ->first();

                $to_location = Locator::where('is_active',true)
                ->whereHas('area',function ($query) use($warehouse_id)
                {
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','ERP FREE STOCK');
                })
                ->first();

                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();

                    
                if($from_location && $to_location)
                {
                    $is_movement_exists = MaterialMovement::where([
                        ['from_location',$from_location->id],
                        ['to_destination',$to_location->id],
                        ['from_locator_erp_id',$from_location->area->erp_id],
                        ['to_locator_erp_id',$to_location->area->erp_id],
                        ['is_integrate',false],
                        ['is_active',true],
                        ['po_buyer',$po_buyer],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        ['status','integration-to-free-stock-erp'],
                        ['created_at',$created_at],
                    ])
                    ->first();

                    if(!$is_movement_exists)
                    {
                        $material_movement = MaterialMovement::firstOrCreate([
                            'from_location'         => $from_location->id,
                            'to_destination'        => $to_location->id,
                            'from_locator_erp_id'   => $from_location->area->erp_id,
                            'to_locator_erp_id'     => $to_location->area->erp_id,
                            'is_integrate'          => false,
                            'is_active'             => true,
                            'po_buyer'              => $po_buyer,
                            'no_packing_list'       => $no_packing_list,
                            'no_invoice'            => $no_invoice,
                            'status'                => 'integration-to-free-stock-erp',
                            'created_at'            => $created_at,
                            'updated_at'            => $created_at,
                        ]);

                        $material_movement_id = $material_movement->id;
                    }else
                    {
                        $material_movement_id = $is_movement_exists->id;
                    }
                    

                    MaterialMovementLine::Create([
                        'material_movement_id'          => $material_movement_id,
                        'material_preparation_id'       => $material_preparation_id,
                        'item_code'                     => $item_code,
                        'item_id'                       => $item_id,
                        'c_order_id'                    => $c_order_id,
                        'c_orderline_id'                => $c_orderline_id,
                        'c_bpartner_id'                 => $c_bpartner_id,
                        'supplier_name'                 => $supplier_name,
                        'type_po'                       => $type_po,
                        'uom_movement'                  => $uom_conversion,
                        'qty_movement'                  => $qty_conversion,
                        'date_movement'                 => $created_at,
                        'created_at'                    => $created_at,
                        'updated_at'                    => $created_at,
                        'warehouse_id'                  => $warehouse_id,
                        'document_no'                   => $document_no,
                        'date_receive_on_destination'   => $created_at,
                        'nomor_roll'                    => '-',
                        'is_active'                     => true,
                        'note'                          => 'PROSES INTEGRASI KE LOCATOR FREE STOCK ERP',
                        'user_id'                       => $system->id,
                    ]);

                    $value->insert_to_locator_free_stock_erp_date = $movement_date;
                    $value->save();
                }

                DB::commit();
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
        }
    }

    private function moveFromFreeStockToInventoryErp()
    {
        $data = MaterialPreparation::where('is_need_inserted_to_locator_inventory_erp',true)
        ->whereNull('insert_to_locator_inventory_erp_date')
        ->whereNotNull('first_inserted_to_locator_date')
        ->get();
        
        $movement_date = carbon::now()->todatetimestring();

        foreach ($data as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();
                $material_preparation_id    = $value->id;
                $po_buyer                   = $value->po_buyer;
                $item_code                  = $value->item_code;
                $item_id                    = $value->item_id;
                $c_order_id                 = $value->c_order_id;
                $uom_conversion             = $value->uom_conversion;
                $c_bpartner_id              = $value->c_bpartner_id;
                $supplier_name              = $value->supplier_name;
                $document_no                = $value->document_no;
                $warehouse_id               = $value->warehouse;
                $created_at                 = $value->created_at;
                $type_po                    = $value->type_po;
                $user_id                    = $value->user_id;
                $po_detail_id               = $value->po_detail_id;
                $qty_conversion             = sprintf('%0.8f',$value->qty_conversion);

                if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                {
                    $no_packing_list        = null;
                    $no_invoice             = null;
                    $c_orderline_id         = null;
                }else
                {
                    $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                    ->whereNull('material_roll_handover_fabric_id')
                    ->where('po_detail_id',$po_detail_id)
                    ->first();
                    
                    $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : null);
                    $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : null);
                    $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : null);
                }

                $from_location = Locator::with('area')
                ->where('is_active',true)
                ->whereHas('area',function ($query) use($warehouse_id)
                {
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','ERP FREE STOCK');
                })
                ->first();

                $to_location = Locator::with('area')
                ->where([
                    ['is_active',true],
                    ['rack','ERP INVENTORY'],
                ])
                ->whereHas('area',function ($query) use($warehouse_id)
                {
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','ERP INVENTORY');
                })
                ->first();

                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();

                if($from_location && $to_location)
                {
                    $is_movement_exists = MaterialMovement::where([
                        ['from_location',$from_location->id],
                        ['to_destination',$to_location->id],
                        ['from_locator_erp_id',$from_location->area->erp_id],
                        ['to_locator_erp_id',$to_location->area->erp_id],
                        ['is_integrate',false],
                        ['is_active',true],
                        ['po_buyer',$po_buyer],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        ['status','integration-to-inventory-erp'],
                        ['created_at',$created_at],
                    ])
                    ->first();

                    if(!$is_movement_exists)
                    {
                        $material_movement = MaterialMovement::firstOrCreate([
                            'from_location'         => $from_location->id,
                            'to_destination'        => $to_location->id,
                            'from_locator_erp_id'   => $from_location->area->erp_id,
                            'to_locator_erp_id'     => $to_location->area->erp_id,
                            'is_integrate'          => false,
                            'is_active'             => true,
                            'po_buyer'              => $po_buyer,
                            'no_packing_list'       => $no_packing_list,
                            'no_invoice'            => $no_invoice,
                            'status'                => 'integration-to-inventory-erp',
                            'created_at'            => $created_at,
                            'updated_at'            => $created_at,
                        ]);

                        $material_movement_id = $material_movement->id;
                    }else
                    {
                        $material_movement_id = $is_movement_exists->id;
                    }
                    

                    MaterialMovementLine::Create([
                        'material_movement_id'          => $material_movement_id,
                        'material_preparation_id'       => $material_preparation_id,
                        'item_code'                     => $item_code,
                        'item_id'                       => $item_id,
                        'c_order_id'                    => $c_order_id,
                        'c_orderline_id'                => $c_orderline_id,
                        'c_bpartner_id'                 => $c_bpartner_id,
                        'supplier_name'                 => $supplier_name,
                        'type_po'                       => $type_po,
                        'uom_movement'                  => $uom_conversion,
                        'qty_movement'                  => $qty_conversion,
                        'date_movement'                 => $created_at,
                        'created_at'                    => $created_at,
                        'updated_at'                    => $created_at,
                        'date_receive_on_destination'   => $created_at,
                        'warehouse_id'                  => $warehouse_id,
                        'document_no'                   => $document_no,
                        'nomor_roll'                    => '-',
                        'note'                          => 'PROSES INTEGRASI KE LOCATOR INVENTORY ERP',
                        'is_active'                     => true,
                        'user_id'                       => $system->id,
                    ]);

                    $value->insert_to_locator_inventory_erp_date = $movement_date;
                    $value->save();
                }
                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
        
       
    }

    private function accessoriesMoveFromInventoryToFreeStockForAdjustment()
    {
        $data = MaterialPreparation::where('is_from_adjustment',true)
        ->whereNull('is_need_inserted_to_locator_free_stock_for_adjustment_erp_date')
        ->get();
       
        $movement_date = carbon::now()->todatetimestring();

        foreach ($data as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();
                $material_preparation_id    = $value->id;
                $po_buyer                   = $value->po_buyer;
                $item_code                  = $value->item_code;
                $item_id                    = $value->item_id;
                $c_order_id                 = $value->c_order_id;
                $uom_conversion             = $value->uom_conversion;
                $c_bpartner_id              = $value->c_bpartner_id;
                $supplier_name              = $value->supplier_name;
                $document_no                = $value->document_no;
                $warehouse_id               = $value->warehouse;
                $created_at                 = $value->created_at;
                $type_po                    = $value->type_po;
                $user_id                    = $value->user_id;
                $po_detail_id               = $value->po_detail_id;
                $qty_conversion             = sprintf('%0.8f',$value->adjustment);

                $from_location = Locator::with('area')
                ->where([
                    ['is_active',true],
                    ['rack','ERP INVENTORY'],
                ])
                ->whereHas('area',function ($query) use($warehouse_id)
                {
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','ERP INVENTORY');
                })
                ->first();

                $to_location = Locator::with('area')
                ->where([
                    ['is_active',true],
                    ['rack','ERP INVENTORY'],
                ])
                ->whereHas('area',function ($query) use($warehouse_id)
                {
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','ERP INVENTORY');
                })
                ->first();

                if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                {
                    $no_packing_list        = null;
                    $no_invoice             = null;
                    $c_orderline_id         = null;
                }else
                {
                    $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                    ->whereNull('material_roll_handover_fabric_id')
                    ->where('po_detail_id',$po_detail_id)
                    ->first();
                    
                    $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : null);
                    $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : null);
                    $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : null);
                }

                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();

                if($from_location && $to_location)
                {
                    $is_movement_exists = MaterialMovement::where([
                        ['from_location',$from_location->id],
                        ['to_destination',$to_location->id],
                        ['from_locator_erp_id',$from_location->area->erp_id],
                        ['to_locator_erp_id',$to_location->area->erp_id],
                        ['is_integrate',false],
                        ['is_active',true],
                        ['po_buyer',$po_buyer],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        ['status','integration-to-inventory-erp'],
                        ['created_at',$created_at],
                    ])
                    ->first();

                    if(!$is_movement_exists)
                    {
                        $material_movement = MaterialMovement::firstOrCreate([
                            'from_location'         => $from_location->id,
                            'to_destination'        => $to_location->id,
                            'from_locator_erp_id'   => $from_location->area->erp_id,
                            'to_locator_erp_id'     => $to_location->area->erp_id,
                            'is_integrate'          => false,
                            'is_active'             => true,
                            'po_buyer'              => $po_buyer,
                            'no_packing_list'       => $no_packing_list,
                            'no_invoice'            => $no_invoice,
                            'status'                => 'integration-to-inventory-erp',
                            'created_at'            => $created_at,
                            'updated_at'            => $created_at,
                        ]);

                        $material_movement_id = $material_movement->id;
                    }else
                    {
                        $material_movement_id = $is_movement_exists->id;
                    }
                    

                    MaterialMovementLine::Create([
                        'material_movement_id'          => $material_movement_id,
                        'material_preparation_id'       => $material_preparation_id,
                        'item_code'                     => $item_code,
                        'item_id'                       => $item_id,
                        'c_order_id'                    => $c_order_id,
                        'c_orderline_id'                => $c_orderline_id,
                        'c_bpartner_id'                 => $c_bpartner_id,
                        'supplier_name'                 => $supplier_name,
                        'type_po'                       => $type_po,
                        'uom_movement'                  => $uom_conversion,
                        'qty_movement'                  => '-'.$qty_conversion,
                        'date_movement'                 => $created_at,
                        'created_at'                    => $created_at,
                        'updated_at'                    => $created_at,
                        'date_receive_on_destination'   => $created_at,
                        'nomor_roll'                    => '-',
                        'warehouse_id'                  => $warehouse_id,
                        'document_no'                   => $document_no,
                        'note'                          => 'PROSES INTEGRASI UNTUK PENYESUAIAN QTY DIKARNAKAN ADA REDUCE PEMAKAIAN',
                        'is_active'                     => true,
                        'user_id'                       => $system->id,
                    ]);

                    $value->is_need_inserted_to_locator_free_stock_for_adjustment_erp_date = $movement_date;
                    $value->save();
                }

                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }

    private function accessoriesMoveFromInventoryToFreeStockForCancelOrReroute()
    {
        $data = MaterialPreparation::where('is_from_cancel_or_reroute',true)
        ->whereNull('is_need_inserted_to_locator_free_stock_for_cancel_or_reroute_da')
        ->whereNotNull('first_inserted_to_locator_date')
        ->get();
       
        $movement_date = carbon::now()->todatetimestring();

        foreach ($data as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();

                $material_preparation_id    = $value->id;
                $po_buyer                   = $value->po_buyer;
                $item_code                  = $value->item_code;
                $item_id                    = $value->item_id;
                $c_order_id                 = $value->c_order_id;
                $uom_conversion             = $value->uom_conversion;
                $c_bpartner_id              = $value->c_bpartner_id;
                $supplier_name              = $value->supplier_name;
                $document_no                = $value->document_no;
                $warehouse_id               = $value->warehouse;
                $created_at                 = $value->created_at;
                $type_po                    = $value->type_po;
                $user_id                    = $value->user_id;
                $po_detail_id               = $value->po_detail_id;
                $qty_conversion             = sprintf('%0.8f',$value->qty_conversion);


                if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                {
                    $no_packing_list        = null;
                    $no_invoice             = null;
                    $c_orderline_id         = null;
                }else
                {
                    $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                    ->whereNull('material_roll_handover_fabric_id')
                    ->where('po_detail_id',$po_detail_id)
                    ->first();
                    
                    $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : null);
                    $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : null);
                    $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : null);
                }

                $from_location = Locator::with('area')
                ->where([
                    ['is_active',true],
                    ['rack','ERP INVENTORY'],
                ])
                ->whereHas('area',function ($query) use($warehouse_id)
                {
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','ERP INVENTORY');
                })
                ->first();

                $to_location = Locator::with('area')
                ->where('is_active',true)
                ->whereHas('area',function ($query) use($warehouse_id)
                {
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','ERP FREE STOCK');
                })
                ->first();

                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();
            

                if($from_location && $to_location)
                {
                    $is_movement_exists = MaterialMovement::where([
                        ['from_location',$from_location->id],
                        ['to_destination',$to_location->id],
                        ['from_locator_erp_id',$from_location->area->erp_id],
                        ['to_locator_erp_id',$to_location->area->erp_id],
                        ['is_integrate',false],
                        ['is_active',true],
                        ['po_buyer',$po_buyer],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        ['status','integration-to-inventory-erp'],
                    ])
                    ->first();

                    if(!$is_movement_exists)
                    {
                        $material_movement = MaterialMovement::firstOrCreate([
                            'from_location'         => $from_location->id,
                            'to_destination'        => $to_location->id,
                            'from_locator_erp_id'   => $from_location->area->erp_id,
                            'to_locator_erp_id'     => $to_location->area->erp_id,
                            'is_integrate'          => false,
                            'is_active'             => true,
                            'po_buyer'              => $po_buyer,
                            'no_packing_list'       => $no_packing_list,
                            'no_invoice'            => $no_invoice,
                            'status'                => 'integration-to-inventory-erp',
                            'created_at'            => $created_at,
                            'updated_at'            => $created_at,
                        ]);

                        $material_movement_id = $material_movement->id;
                    }else
                    {
                        $material_movement_id = $is_movement_exists->id;
                    }
                    

                    MaterialMovementLine::Create([
                        'material_movement_id'          => $material_movement_id,
                        'material_preparation_id'       => $material_preparation_id,
                        'item_code'                     => $item_code,
                        'item_id'                       => $item_id,
                        'c_order_id'                    => $c_order_id,
                        'c_orderline_id'                => $c_orderline_id,
                        'c_bpartner_id'                 => $c_bpartner_id,
                        'supplier_name'                 => $supplier_name,
                        'type_po'                       => $type_po,
                        'uom_movement'                  => $uom_conversion,
                        'qty_movement'                  => $qty_conversion,
                        'date_movement'                 => $created_at,
                        'created_at'                    => $created_at,
                        'updated_at'                    => $created_at,
                        'date_receive_on_destination'   => $created_at,
                        'nomor_roll'                    => '-',
                        'warehouse_id'                  => $warehouse_id,
                        'document_no'                   => $document_no,
                        'note'                          => 'PROSES INTEGRASI DARI LOCATOR INVENTORY KE FREE STOCK ERP DIKARNAKAN ITEM INI CANCEL / REROUTE / TIDAK JADI GUNAKAN KARENA SUDAH TERCOVER OLEH FREE STOCK',
                        'is_active'                     => true,
                        'user_id'                       => $system->id,
                    ]);

                    $value->is_need_inserted_to_locator_free_stock_for_cancel_or_reroute_da = $movement_date;
                    $value->save();
                }

                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }

    /*private function fabricMoveHandover()
    {
        $data = MaterialRollHandoverFabric::whereNotNull('receive_date')
        ->whereNull('insert_to_locator_movement_handover')
        ->get();
       
        try 
        {
            DB::beginTransaction();
            
            foreach ($data as $key => $value) 
            {
                $material_stock_id          = $value->id;
                $po_buyer                   = null;
                $item_code                  = $value->item_code;
                $item_id                    = $value->item_id;
                $warehouse_id               = $value->warehouse_id;
                $c_order_id                 = $value->c_order_id;
                $c_bpartner_id              = $value->c_bpartner_id;
                $uom                        = $value->uom;
                $po_detail_id               = $value->po_detail_id;
                $no_invoice                 = $value->no_invoice;
                $no_packing_list            = $value->no_packing_list;
                $is_reject_by_lot           = $value->is_reject_by_lot;
                $is_reject_by_rma           = $value->is_reject_by_rma;

                if($is_reject_by_lot)
                {
                    $created_at             = $value->reject_by_lot_date;
                    $user_id                = $value->user_lab_id;
                    $user_name              = $value->userInspectLab->name;
                    $remark                 = 'LOT TIDAK SESUAI ('.$value->inspect_lab_remark.')';
                   
                } else if($is_reject_by_rma)
                {   
                    $created_at             = $value->reject_by_rma_date;
                    $user_id                = $value->reject_user_id;
                    $user_name              = $value->userReject->name;
                    $remark                 = 'MATERIAL YANG DITERIMA SALAH';
                } 

                $qty_reject             = sprintf('%0.8f',$value->qty_reject_non_by_inspect);
                $type_po                = $value->type_po;
                
                $from_location = Locator::whereHas('area',function ($query) use($warehouse_id)
                {
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','ERP INVENTORY');
                })
                ->first();

                $to_location = Locator::whereHas('area',function ($query) use($warehouse_id)
                {
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','ERP REJECT');
                })
                ->first();

                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();

                if($from_location && $to_location && $no_invoice && $no_packing_list)
                {
                    $is_movement_exists = MaterialMovement::where([
                        ['from_location',$from_location->id],
                        ['to_destination',$to_location->id],
                        ['is_integrate',false],
                        ['is_active',true],
                        ['po_buyer',$po_buyer],
                        ['no_invoice',$no_invoice],
                        ['no_packing_list',$no_packing_list],
                        ['status','integration-to-locator-reject-erp'],
                        ['created_at',$created_at],
                    ])
                    ->first();

                    if(!$is_movement_exists)
                    {
                        $material_movement = MaterialMovement::firstOrCreate([
                            'from_location'         => $from_location->id,
                            'to_destination'        => $to_location->id,
                            'is_integrate'          => false,
                            'is_active'             => true,
                            'po_buyer'              => $po_buyer,
                            'no_invoice'            => $no_invoice,
                            'no_packing_list'       => $no_packing_list,
                            'status'                => 'integration-to-locator-reject-erp',
                            'created_at'            => $created_at,
                            'updated_at'            => $created_at,
                        ]);

                        $material_movement_id = $material_movement->id;
                    }else
                    {
                        $material_movement_id = $is_movement_exists->id;
                    }
                    

                    MaterialMovementLine::Create([
                        'material_movement_id'          => $material_movement_id,
                        'material_stock_id'             => $material_stock_id,
                        'item_code'                     => $item_code,
                        'item_id'                       => $item_id,
                        'type_po'                       => $type_po,
                        'c_order_id'                    => $c_order_id,
                        'c_bpartner_id'                 => $c_bpartner_id,
                        'warehouse_id'                  => $warehouse_id,
                        'uom_movement'                  => $uom,
                        'qty_movement'                  => $qty_reject,
                        'date_movement'                 => $created_at,
                        'note'                          => 'PROSES INTEGRASI KE LOCATOR REJECT ERP. PENYEBAB REJECTNYA ADALAH '.$remark.' DILAKUKAN OLEH '.$user_name,
                        'is_active'                     => true,
                        'user_id'                       => $system->id,
                    ]);
                    $value->insert_to_locator_reject_erp_date = $movement_date;
                }

                $value->save();

                
            }

            DB::commit();
        } catch (Exception $e) 
        {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    }*/

    private function setStatus($scheduler,$status)
    {
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler)
    {
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler)
    {
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
