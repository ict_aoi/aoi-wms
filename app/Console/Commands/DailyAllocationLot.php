<?php

namespace App\Console\Commands;
use DB;
use Carbon\Carbon;
use App\Models\Temporary;
use App\Models\Scheduler;
use App\Models\AutoAllocation;
use App\Models\MaterialStock;

use Illuminate\Console\Command;
use App\Http\Controllers\FabricMaterialInspectLabController;
use App\Http\Controllers\FabricReportMaterialMonitoringController;

class DailyAllocationLot extends Command
{
    protected $signature = 'dailyAllocationLot:update';
    protected $description = 'update stock reserve acc';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','SYNC_UPDATE_ALLOCATION_LOT')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job' => 'SYNC_UPDATE_ALLOCATION_LOT',
                'status' => 'ongoing'
            ]);
            $this->setStartJob($new_scheduler);
            
            $this->info('SYNC UPDATE ALLOCATION LOT START JOB AT '.carbon::now());
            $this->doJob();
            $this->info('SYNC UPDATE ALLOCATION LOT  END JOB AT '.carbon::now());

            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
        }else{
            $this->info('SYNC UPDATE ALLOCATION LOT SEDANG BERJALAN');
        } 
    }

    static function doJob()
    {

        $temporaries            = Temporary::where('status','complete_lot')
                                  ->where('user_id', '!=', '107')->get();
        $confirm_date           = Carbon::now();
            
        foreach ($temporaries as $key => $temporary) 
        {
            try 
            {
                DB::beginTransaction();
                    $monitoring_receiving_fabric_id = $temporary->barcode;
                    $material_stocks                = MaterialStock::where('monitoring_receiving_fabric_id', $monitoring_receiving_fabric_id)
                                                    ->first();
                    $c_order_id   = $material_stocks->c_order_id;
                    $item_id      = $material_stocks->item_id;
                    $warehouse_id = $material_stocks->warehouse_id;

                    //update status alokasi sudah inhouse / belum 
                    FabricReportMaterialMonitoringController::dailyUpdateMonitoringMaterial($c_order_id, $item_id, $warehouse_id);

                    // //list alokasi yang dapat jatah lot
                    
                    $auto_allocations = AutoAllocation::where([
                        //['is_already_generate_form_booking',false],
                        ['is_fabric',true],
                        ['status_po_buyer','active'],
                        ['c_order_id', $c_order_id],
                        ['item_id_source', $item_id],
                        ['warehouse_id', $warehouse_id],
                        ['qty_in_house', '>', 0],
                    ])
                    ->whereNull('deleted_at')
                    ->orderBy('is_ordering_for_japan_china', 'desc')
                    ->orderby('promise_date','asc')
                    ->orderby('qty_outstanding','asc')
                    ->get();

                    //dd($auto_allocations);

                    //batalin allocation item yang lotnya masih kosong 
                    foreach ($auto_allocations as $key => $auto_allocation) 
                    {
                        FabricMaterialInspectLabController::allocationLot($auto_allocation->id);
                    }

                    $temporary->delete();
                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

        }

    } 

    private function setStatus($scheduler,$status)
    {
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler)
    {
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler)
    {
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}

