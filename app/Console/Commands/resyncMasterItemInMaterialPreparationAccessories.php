<?php namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Scheduler;
use App\Models\MaterialPreparation;

class resyncMasterItemInMaterialPreparationAccessories extends Command
{
    protected $signature    = 'resync:itemMaterialPreparationAccessories';
    protected $description  = 'resync master item in material preparation accessories';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','RESYNC_ITEM_MATERIAL_PREPARATION_ACCESSORIES')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job' => 'RESYNC_ITEM_MATERIAL_PREPARATION_ACCESSORIES',
                'status' => 'ongoing'
            ]);
            $this->setStartJob($new_scheduler);
            
            $this->info('SYNC MASTER ITEM MATERIAL PREPARATION ACCESSORIES  START JOB AT '.carbon::now());
            $this->updateData();
            $this->info('SYNC MASTER ITEM MATERIAL PREPARATION ACCESSORIES  END JOB AT '.carbon::now());

            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
        } 
    }

    private function updateData()
    {
        $data = DB::table('resync_master_item_in_preparations_v')->get();
        foreach ($data as $key => $value) 
        {
            $id                 = $value->id;
            $item               = MaterialPreparation::find($id);

            $item->item_code    = $value->item_code_master;
            $item->item_desc    = $value->item_desc_master;
            $item->save();
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
