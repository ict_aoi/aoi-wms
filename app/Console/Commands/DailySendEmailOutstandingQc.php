<?php namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Http\Controllers\FabricReportMaterialQualityControlController;

use App\Models\Scheduler;

class DailySendEmailOutstandingQc extends Command
{
    protected $signature = 'dailySendEmailOutsandingQc:mail';
    protected $description = 'Sending email outstanding qc';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $scheduler_planning = Scheduler::where('job','SEND_EMAIL_OUTSTANDING_QC')
        ->where('status','queue')
        ->first();

        if(!empty($scheduler_planning)){
            $this->setStatus($scheduler_planning,'ongoing');
            $this->setStartJob($scheduler_planning);

            $this->info('SEND EMAIL OUTSTANDING QC FABRIC JOB AT '.carbon::now());
            FabricReportMaterialQualityControlController::sendEmailOutstandingConfirmation();
            $this->info('DONE SEND EMAIL OUTSTANDING QC FABRIC JOB AT '.carbon::now());

            $this->setStatus($scheduler_planning,'done');
            $this->setEndJob($scheduler_planning);
            
        }else{
            $is_schedule_on_going = Scheduler::where('job','SEND_EMAIL_OUTSTANDING_QC')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SEND_EMAIL_OUTSTANDING_QC',
                    'status' => 'ongoing'
                ]);
                $this->setStartJob($new_scheduler);
                
                $this->info('SEND EMAIL OUTSTANDING QC FABRIC JOB AT '.carbon::now());
                FabricReportMaterialQualityControlController::sendEmailOutstandingConfirmation();
                $this->info('DONE SEND EMAIL OUTSTANDING QC FABRIC JOB AT '.carbon::now());

                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                
            }else{
                $this->info('SEND EMAIL OUTSTANDING QC FABRIC JOB IS RUNNING');
            }
        }

        
        $this->info('Daily Insert has been successfully');
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
