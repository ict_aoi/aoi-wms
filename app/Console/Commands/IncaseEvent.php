<?php namespace App\Console\Commands;
use App\Http\Controllers\MaterialCheckController;
use App\Http\Controllers\FabricMaterialPlanningController;
use App\Http\Controllers\AreaController;
use App\Http\Controllers\MaterialArrivalFabricController;
use App\Http\Controllers\ERPController;
use App\Http\Controllers\AccessoriesMaterialArrivalController as InsertMaterialReceive;
use App\Http\Controllers\MasterDataAutoAllocationController;
use App\Http\Controllers\MovementOutController;
use App\Http\Controllers\MaterialPreparationFabricController as PreparationFabric;
use App\Http\Controllers\FabricReportDailyMaterialPreparationController as ReportPreparationFabric;
use App\Http\Controllers\MaterialStockFabricController;
use App\Http\Controllers\PoBuyerController;
use App\Http\Controllers\MasterDataPoBuyerController;
use App\Http\Controllers\MaterialStockController;
use App\Http\Controllers\ErpMaterialRequirementController;
use App\Http\Controllers\AccessoriesMaterialStockApprovalController as HistoryStock;
use App\Http\Controllers\MasterDataAutoAllocationController as AllocationFabric;
use App\Http\Controllers\FabricReportMaterialMonitoringController;
use App\Http\Controllers\AccessoriesReportMaterialStockController as ReportStock;
use App\Http\Controllers\FabricMaterialStockController as HistoryStockPerLot;
use App\Http\Controllers\FabricReportMaterialMonitoringController as Dashboard;
use App\Http\Controllers\AccessoriesMaterialStockOnTheFlyController as SOTF;

use Carbon\Carbon;
use DB;
use Config;
use StdClass;
use Illuminate\Console\Command;

use App\Models\DetailMaterialRequirement;
use App\Models\PurchaseOrderDetail;
use App\Models\MaterialRequirementPerSize;
use App\Models\Temporary;
use App\Models\MaterialMovementPerSize;
use App\Models\MovementStockHistory;
use App\Models\UomConversion;
use App\Models\MaterialMovement;
use App\Models\MaterialMovementLine;
use App\Models\MaterialSubcont;
use App\Models\SummaryHandoverMaterial;
use App\Models\MonitoringReceivingFabric;
use App\Models\AutoAllocation;
use App\Models\MaterialStock;
use App\Models\CuttingInstruction;
use App\Models\Locator;
use App\Models\Barcode;
use App\Models\MaterialPlanningFabric;
use App\Models\User;
use App\Models\MaterialArrival;
use App\Models\DetailMaterialArrival;
use App\Models\MaterialRequirementPerPart;
use App\Models\MaterialRequirement;
use App\Models\Supplier;
use App\Models\PoSupplier;
use App\Models\DetailMaterialStock;
use App\Models\PoBuyer;
use App\Models\AllocationItem;
use App\Models\MaterialRollHandoverFabric;
use App\Models\DetailMaterialPlanningFabric;
use App\Models\DetailMaterialPreparationFabric;
use App\Models\DetailMaterialPreparation;
use App\Models\MaterialPreparation;
use App\Models\MaterialPreparationFabric;
use App\Models\MappingStocks;
use App\Models\SummaryStockFabric;
use App\Models\DetailPoBuyerPerRoll;
use App\Models\MaterialStockPerLot;
use App\Models\HistoryMaterialStockPerLot;
use App\Models\HistoryPreparationFabric;
use App\Models\HistoryAutoAllocations;
use App\Models\Item;
use App\Models\DetailMaterialPlanningFabricPerPart;
use App\Http\Controllers\FabricReportDailyMaterialPreparationController as FabricReportDailyMaterialPreparation;
use App\Http\Controllers\MasterDataReroutePoBuyerController as MDRPC;

use App\Models\ReroutePoBuyer;

class IncaseEvent extends Command
{
    protected $signature = 'incaseEvent:update';
    protected $description = 'update status incase';

    public function __construct()
    {
        parent::__construct();
    }   

    public function handle()
    {
        
        $this->info('START EVENT AT '.carbon::now());
        
        //$this->insertAdjustmentStockFabric();
        //$this->insertPoBuyerPerRoll();
        //$this->CuttingInstructionHeaderReport();
        //$this->insertDetailAllocation();
        //$this->insertAllocationClosingBalance();
        //$this->updateStyleJobOrder();
        //$this->updateBrandPoBuyer();
        //$this->updateTypeStockAutoAllocation();
        //$this->updateItemBookAutoAllocation();
        //$this->updateJobOrder();
        //$this->deleteDuplicatePurchase();
        //$this->updateMonitoringHandoverFabric();
        //$this->updateQtyArrival();
        //$this->updateCOrderIdAutoAllocation();
        //$this->updateCOrderIdAllocationItem();
        //$this->updateAutoAllocationSalahView();
        //this->updateSeason();
        //$this->recalculateSummaryStockFab();
        //$this->updateCorderLineIdReject();
        //$this->caseClosingPreparationFabric();
        //$this->updateDetailPoBuyerPerRoll();
        //$this->recalculateAutoAllocationFabric();
       // $this->updateRerouteAutoAllocation();
        //$this->updateRecalculateAutoAllocation();
        //$this->updateCorderLineLine();
        //$this->updateClosinganFabric();
       // $this->doMovementPerSize();
        //$this->recalculateStockAcc();
        //$this->printBarcodePreparationFabric();
        //$this->updateAutoAllocationVsPreparation();
        //$this->updateTotalPreparationFabric();
       // $this->storeMonitoringHandoverReceiving();
       // $this->updateTotalAllocationSOTF();
        //$this->updateCOrderIdMaterialSubcont();
        //$this->moveAllocationIntoPreparation();
        //$this->updateCOrderIdMonitoringRecevingFabric();
        //$this->updateCOrderIdSummaryHandoverFabric();
        //$this->updateCOrderIdSummaryStockFabric();
        //$this->updateCOrderIdSummaryStockFabric();
        //$this->updateCOrderIdMaterialPreparationFabric();
        //$this->updateCOrderIdDetailMaterialPlanningFabric();
        // $this->updateMonitoringReceivingFabric();
        //$this->updateSequenceBarcode();
        //$this->insertBarcode();
        //$this->updateTypeStockPoSupplier();
        //$this->updateRereouteAllocation();
        //$this->updateBarcodeRereoute();
        //$this->updateTypeStockPoSupplierSummaryFabric();
        //$this->reAllocationStock();
        //$this->adjustmentStockPerlot();
        //$this->cek();
        //$this->autoAllocationCarton();
        //$this->autoAllocationFreeStockCarton();
        //$this->deleteAlokasiNonPoBuyer();
        //$this->reAllocationFab();
        //$this->allocationManual();
        //$this->SSH();
        //$this->syncItem();
        //$this->insertMaterialStockPerlot();
        //$this->insertCronLot();
        //$this->insertMaterialArrivals();
        //MaterialStockController::updateTypeStock();
        //ERPController::supplierCronInsert();
        //ERPController::daily_purchase_items();
        //ERPController::daily_insert_purchase_allocation_items();
        //MaterialCheckController::update_qc_status();
        //FabricMaterialPlanningController::case_update_qty();
        //AreaController::update_barcode();
        //MaterialArrivalFabricController::incaseInsertSummaryStockFabric();
        //MaterialArrivalFabricController::update_style_in_monitoring_receiving();
        //MaterialArrivalFabricController::incase_insert_monitoring_receiving();
       // MaterialArrivalFabricController::incase_insert_summary_stock_fabric();
        //MaterialPreparationFabricController::insert_all_po_buyer_per_roll();
        //PoBuyerController::syncClosePoBuyer();
        //PoBuyerController::syncPurchaseCancel();
        //FabricMaterialPlanningController::incase_insert_material_planning_piping();
        //MasterDataAutoAllocationController::sync_auto_allocation_erp();
        //MasterDataAutoAllocationController::insert_allocation_accessories_items();
        //MaterialStockFabricController::daily_incase_summary_stock_fabric();
        //MaterialStockFabricController::daily_incase_summary_stock_fabric();
        //MaterialStockFabricController::summaryStockRecalculate();
        //ErpMaterialRequirementController::reduceAllocation('0125456994');
        //MovementOutController::insertSubcontSchedular();
        //FabricReportMaterialMonitoringController::updateMonitoringMaterial();
        //FabricReportMaterialMonitoringController::insertToDashboardErp();
        //$this->internalUseErp();
        //$this->dailyRecalcaulateDashboardFab();
        //$this->cancelPoBuyerInsert();
        //$this->updateQtyPerStyle();
        // FabricReportMaterialMonitoringController::updateMonitoringMaterialACC();
        //$this->insertCaseInMaterialStock();
        // $this->dailyPurchaseItems();
        // $this->storeSummaryStockFabric();
        // $this->recalculateAllocatingPoBuyer();
        // $this->dailyPurchaseItemsMar();
        // $this->dailyPurchaseItemsFix();
        // $this->allocationManualCaseElva();
        // $this->insertRePrepareRollToBuyer();

        // $this->doReroute();

        // $this->sync_po_buyer();
        // $this->reAllocationFab();

        // $this->dailyPurchaseItemsFix();
        // $this->allocationManualFix();

        // $this->sikatAlokasi();

        // $this->DoCancelPoBuyer();

        $this->updateMonitoringMaterial();
        // $this->setMonitoringRcv();

        $this->info('END EVENT AT '.carbon::now());
    }
    static function setMonitoringRcv()
    {
        $warehouse_id = ['1000011','1000001'];
        $data_autos = db::table('mna_test')->get();

        $change_date = carbon::now()->format('Y-m-d');

        $app_env = Config::get('app.env');
        if($app_env == 'live')
        {
            $data_erp = DB::connection('erp');
            $data     = DB::connection('web_po');
            $webpo_new = DB::connection('webpo_new');
        }

        // dd($auto_allocations);
        try 
            {
            DB::beginTransaction();

           
            foreach($data_autos as $key => $auto_allocation)
            {   
                $getAuto = AutoAllocation::where('id', $auto_allocation->id)->first();

                $eta_date     = null;
                $etd_date     = null;
                $receive_date = $auto_allocation->receive_date;
                // dd($resceive_date);
                $no_invoice   = null;
                $qty_in_house = $getAuto->qty_allocation;
                $qty_on_ship  = 0;
                $qty_mrd      = 0;
                $mrd          = null;
                $dd_pi        = null;
                $eta_pi       = null;
                $etd_pi       = null;

            
                echo $auto_allocation->id . "\n";
                
               
                
                $auto_allocation_update               = AutoAllocation::find($auto_allocation->id);
                $auto_allocation_update->is_closed = true;
                $auto_allocation_update->qty_in_house = $qty_in_house;
                $auto_allocation_update->qty_on_ship = $qty_on_ship;
                $auto_allocation_update->receive_date = $receive_date;
                $auto_allocation_update->save();

            }

                DB::commit();

                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                }

    } 

    static function updateMonitoringMaterial()
    {
        $warehouse_id = ['1000011','1000001'];
        // $ids=['56a6f9a0-9523-11ec-a96c-ab2cb3debc0b',
        // '59afd370-9523-11ec-9aa9-2b5732ce391f'];
        // $ids = db::table('mna_test')->pluck('id')->toArray();
      
        // $getid = db::table('auto_allocations')->wherein('id', $ids)->get();
        // dd($getid);
        $refresh = DB::select(db::raw("
        refresh materialized view monitoring_allocations_mv;"
        ));

        //list kalkulasi
        $auto_allocations = db::table('monitoring_allocations_mv')
        ->where('is_additional',false)
        ->where('lc_date', '>=', '2020-08-01')
        // ->wherein('id', $ids)
        //->where('lc_date', '<', '2020-08-01')
        ->where('c_order_id','1928524')
        ->where('item_id_source','2155293')
        // ->where('qty_allocated', '>', '0')
        ->where(db::raw("qty_allocated - COALESCE(qty_in_house, 0)"), '>' ,'0')
        ->whereIn('warehouse_id',$warehouse_id)
        ->orderBy('statistical_date', 'asc')
        ->orderBy('promise_date', 'asc')
        ->orderBy('qty_allocation', 'asc')
        ->get();
        // dd($auto_allocations);
        // $auto_allocations = db::table('auto_allocations')
        // ->where('c_order_id','1928524')
        // ->where('item_id_source','2155293')
        // ->wherenull('deleted_at')
        // ->get();

        $change_date = carbon::now()->format('Y-m-d');

        $app_env = Config::get('app.env');
        if($app_env == 'live')
        {
            $data_erp = DB::connection('erp');
            $data     = DB::connection('web_po');
            $webpo_new = DB::connection('webpo_new');
        }

        // dd($auto_allocations);
        try 
            {
            DB::beginTransaction();

            $reset_alocation = AutoAllocation::where('is_additional',false)
            //->whereBetween('lc_date', [$start_date, $end_date])
            ->where('lc_date', '>=', '2020-08-01')
            // ->wherein('id', $ids)
            //->where(db::raw("to_char(lc_date,'YYYYMM')"),['202006'])
            ->where('c_order_id','1928524')
            ->where('item_id_source','2155293')
            // ->where('qty_allocated', '>', '0')
            ->where(db::raw("qty_allocated - COALESCE(qty_in_house, 0)"), '>' ,'0')
            ->whereIn('warehouse_id',$warehouse_id)
            ->whereNull('deleted_at')
            ->update([
                'qty_in_house' => null,
                'qty_on_ship'  => null,
                'qty_mrd'      => null,
                // 'eta_actual'   => null,
                // 'eta_actual'   => null,
                // 'no_invoice'   => null,
                // 'receive_date' => null,
                // 'mrd'          => null,
                // 'dd_pi'        => null,
                // 'eta_date'     => null,
                // 'etd_date'     => null,
            ]);

            foreach($auto_allocations as $key => $auto_allocation)
            {   
                $eta_date     = null;
                $etd_date     = null;
                $receive_date = null;
                $no_invoice   = null;
                $qty_in_house = 0;
                $qty_on_ship  = 0;
                $qty_mrd      = 0;
                $mrd          = null;
                $dd_pi        = null;
                $eta_pi       = null;
                $etd_pi       = null;

                $auto_allocation_id = $auto_allocation->id;
                $item_id            = $auto_allocation->item_id_source;
                $warehouse_id       = $auto_allocation->warehouse_id;
                $c_order_id         = $auto_allocation->c_order_id;
                $lc_date             = $auto_allocation->lc_date;
                $qty_need           = sprintf('%0.4f',$auto_allocation->qty_allocation);
                $qty_on_ship_alocation = sprintf('%0.4f',$auto_allocation->qty_on_ship);

                if($c_order_id == 'FREE STOCK')
                {
                    $qty_in_house = $qty_need;
                    $eta_actual   = $lc_date;
                    $etd_actual   = $lc_date;
                    $receive_date = $lc_date;
                    $no_invoice   = null;

                }
                else
                {

                $material_stock = MaterialStock::select(db::raw('sum(stock) as stock'), db::raw('max(created_at) as created_at'))
                ->where([
                ['item_id', $item_id],
                ['c_order_id', $c_order_id],
                ['is_stock_mm', false],
                ['is_closing_balance', false],
                ['approval_date', '!=', null],
                ['is_from_handover', false],
                ['is_master_roll', true],
                //['warehouse_id', $warehouse_id], tidak cek warehouse
                ])
                ->whereNull('case')
                ->first();
                //->whereNull('deleted_at')
                //->sum('stock');
                // dd($material_stock);
                $sum_qty_stock = $material_stock->stock;
                $receive_date    = $material_stock->created_at;
                //$no_invoice    = $material_stock->no_invoice;

                $sum_qty_in_house = AutoAllocation::where([
                    ['item_id_source', $item_id],
                    ['c_order_id', $c_order_id],
                    //['warehouse_id', $warehouse_id],
                    ])
                    ->whereNull('deleted_at')
                    ->sum('qty_in_house');

                //sisa qty in house 
                $material_in_house = sprintf('%0.4f',$sum_qty_stock) - sprintf('%0.4f',$sum_qty_in_house);
                // dd($material_in_house);
                //jika material yang tersdia > 0
                if($material_in_house > 0)
                {
                    //dd($material_in_house - $qty_need);
                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                    //dd($material_in_house/$qty_need);
                    if(sprintf('%0.4f',$material_in_house)/$qty_need >= 1)
                    {
                        $qty_in_house = $qty_need;
                        $qty_on_ship  = 0;

                        //$app_env = Config::get('app.env');
                        // if($app_env == 'live')
                        // {
                            //$data = DB::connection('web_po');
                            $list_on_ships = $data->table('material_on_ship_v')
                            ->select(db::raw('sum(qty_upload) as qty_upload'), 'kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            ->where([
                                ['m_product_id', $item_id],
                                ['c_order_id', $c_order_id],
                                ['isactive', true],//ganti dulu 
                                ['flag_wms', true]
                            ])
                            ->groupBy('kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            ->orderBy('kst_etadate', 'asc')
                            ->orderBy('kst_etddate', 'asc')
                            ->get();

                            $sum_on_ship_web_po = $list_on_ships->sum('qty_upload');

                            //dd($list_on_ships);

                            if(count($list_on_ships) > 0)
                            {
                                $temp_qty_on_ship = 0;
                                foreach($list_on_ships as $key => $list_on_ship)
                                {
                                    $sisa = 0;
                                    $no_invoice           = $list_on_ship->kst_invoicevendor;
                                    $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
                                    $temp_qty_on_ship += $total_on_ship_web_po;

                                    $sum_all_qty_on_ship = AutoAllocation::where([
                                        ['item_id_source', $item_id],
                                        ['c_order_id', $c_order_id],
                                        //['warehouse_id', $warehouse_id],
                                        ])
                                        ->whereNull('deleted_at')
                                        ->sum('qty_on_ship');

                                    // $sum_qty_on_ship = AutoAllocation::where([
                                    //     ['item_id_source', $item_id],
                                    //     ['c_order_id', $c_order_id],
                                    //     ['warehouse_id', $warehouse_id],
                                    //     ['no_invoice', $no_invoice],
                                    //     ])
                                    //     ->whereNull('deleted_at')
                                    //     ->sum('qty_on_ship');
                                    
                                       
                                    if(sprintf('%0.4f', $sum_all_qty_on_ship) - $temp_qty_on_ship > 0 )
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        $outstanding_on_ship =  $temp_qty_on_ship - sprintf('%0.4f', $sum_all_qty_on_ship);
                                    }

                                    //$outstanding_on_ship = sprintf('%0.4f',$total_on_ship_web_po) - sprintf('%0.4f',$sum_qty_on_ship);
                                    
                                    //echo $no_invoice. ' total  '. $total_on_ship_web_po. ' sisa '.$outstanding_on_ship. PHP_EOL;

                                    //echo (sprintf('%0.4f',$sum_on_ship_web_po) - sprintf('%0.4f', $sum_all_qty_on_ship)). PHP_EOL;
                                    if(sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship) != 0)
                                    {
                                        if($outstanding_on_ship > 0)
                                        {
                                            //jika material yang tersedia lebih dari atau sama dengan alokasi
                                            if($outstanding_on_ship >= ($qty_need - $qty_in_house))
                                            {
                                                $qty_on_ship              = $qty_need - $qty_in_house;
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                //$receive_date             = null;
                                                //echo $qty_on_ship. PHP_EOL;
                                                break;
                                            }
                                            else
                                            {
                                                $qty_on_ship       += $outstanding_on_ship;
                                                if($qty_on_ship >= ($qty_need - $qty_in_house))
                                                {
                                                    $qty_on_ship  = $qty_need - $qty_in_house;
                                                    $eta_date     = $list_on_ship->kst_etadate;
                                                    $etd_date     = $list_on_ship->kst_etddate;
                                                    //$receive_date = null;
                                                    //echo $qty_on_ship. PHP_EOL;
                                                    break;
                                                    
                                                }
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                //$receive_date             = $receive_date;
                                                //echo $qty_on_ship. PHP_EOL;
                                            }
                                            

                                        }
                                    }
                                    // else
                                    // {
                                    //     $qty_on_ship  = 0;
                                    //     $eta_date     = null;
                                    //     $etd_date     = null;
                                    //     $receive_date = null;
                                    // }    
                                }
                                //get mrd
                                $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();
                                    
                                    //dd($list_on_ship_mrds);
                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                            $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                ['status_po_buyer', 'active'] //permintaan elva karena kemakan po cancel 
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            
                                                
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }

                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        //$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_mrd. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                        //$receive_date = $receive_date;
                                                        //echo $qty_mrd. PHP_EOL;
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }
                            }
                        //dd($no_invoice);

                        //$data_erp = DB::connection('erp');

                        // $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                        // ->select(db::raw('max(mrd) as mrd, max(dd_pi) as dd_pi, max(eta) as eta, max(etd) as etd'))
                        // ->where([
                        //     ['m_product_id', $item_id],
                        //     ['c_order_id', $c_order_id],
                        // ])
                        // ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                        // ->orderBy('eta', 'desc')
                        // ->orderBy('etd', 'desc')
                        // ->first();

                        // //dd($list_on_ship_mrds);
                        // if($list_on_ship_mrds)
                        // {
                        //     $mrd          = $list_on_ship_mrds->mrd;
                        //     $dd_pi        = $list_on_ship_mrds->dd_pi;
                        //     $eta_pi       = $list_on_ship_mrds->eta;
                        //     $etd_pi       = $list_on_ship_mrds->etd;
                        // }

                        $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();
                                    
                                    // dd($list_on_ship_mrds);
                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                            $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            // dd($sum_all_qty_on_ship_mrd);
                                                
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }

                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        //$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        $receive_date = $receive_date;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_mrd. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                        //echo $qty_mrd. PHP_EOL;
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }

                    }
                    else
                    {
                        $qty_in_house = $material_in_house;
                        //cek kekurangan di web po
                        // $app_env = Config::get('app.env');
                        // if($app_env == 'live')
                        // {
                        //     $data = DB::connection('web_po');
                        $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                        ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                        ->where([
                            ['m_product_id', $item_id],
                            ['c_order_id', $c_order_id],
                        ])
                        ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                        ->orderBy('mrd', 'asc')
                        ->orderBy('dd_pi', 'asc')
                        ->get();
                        
                        //dd($list_on_ship_mrds);
                        $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                        if(count($list_on_ship_mrds) > 0)
                        {
                            $temp_qty_on_ship_mrd = 0;
                            foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                            {
                                $sisa = 0;
                                $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                    ['item_id_source', $item_id],
                                    ['c_order_id', $c_order_id],
                                    ['warehouse_id', $warehouse_id],
                                    ])
                                    ->whereNull('deleted_at')
                                    ->sum('qty_mrd');
                                
                                    
                                if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                {
                                    continue;
                                }
                                else
                                {
                                    $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                }

                                if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                {
                                    if($outstanding_on_ship_mrd > 0)
                                    {
                                        //jika material yang tersedia lebih dari atau sama dengan alokasi
                                        if($outstanding_on_ship_mrd >= $qty_need)
                                        {
                                            $qty_mrd      = $qty_need;
                                            $mrd          = $list_on_ship_mrd->mrd;
                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                            $eta_pi       = $list_on_ship_mrd->eta;
                                            $etd_pi       = $list_on_ship_mrd->etd;
                                            //$receive_date = null;
                                            //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                            break;
                                        }
                                        else
                                        {
                                            $qty_mrd       += $outstanding_on_ship_mrd;
                                            $mrd          = $list_on_ship_mrd->mrd;
                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                            $eta_pi       = $list_on_ship_mrd->eta;
                                            $etd_pi       = $list_on_ship_mrd->etd;
                                            $receive_date = $receive_date;
                                            //echo $qty_mrd. PHP_EOL;

                                            if($qty_mrd >= $qty_need)
                                            {
                                                $qty_mrd      = $qty_need;
                                                $mrd          = $list_on_ship_mrd->mrd;
                                                $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                $eta_pi       = $list_on_ship_mrd->eta;
                                                $etd_pi       = $list_on_ship_mrd->etd;
                                                //$receive_date = null;
                                                //echo $qty_mrd. PHP_EOL;
                                                break;
                                                
                                            }
                                        }
                                        

                                    }
                                }
                            }
                        }

                            $list_on_ships = $data->table('material_on_ship_v')
                            ->select(db::raw('sum(qty_upload) as qty_upload'), 'kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            ->where([
                                ['m_product_id', $item_id],
                                ['c_order_id', $c_order_id],
                                ['isactive', true],
                                ['flag_wms', false]
                            ])
                            ->groupBy('kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            ->orderBy('kst_etadate', 'asc')
                            ->orderBy('kst_etddate', 'asc')
                            ->get();

                            //dd($list_on_ships);
                            $sum_on_ship_web_po = $list_on_ships->sum('qty_upload');

                            if(count($list_on_ships) > 0)
                            {
                                $temp_qty_on_ship = 0;
                                $outstanding_on_ship = 0;
                                foreach($list_on_ships as $key => $list_on_ship)
                                {
                                    $no_invoice           = $list_on_ship->kst_invoicevendor;
                                    $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
                                    $temp_qty_on_ship += $total_on_ship_web_po;

                                    $sum_all_qty_on_ship = AutoAllocation::where([
                                        ['item_id_source', $item_id],
                                        ['c_order_id', $c_order_id],
                                        //['warehouse_id', $warehouse_id],
                                        ])
                                        ->whereNull('deleted_at')
                                        ->sum('qty_on_ship');

                                    // $sum_qty_on_ship = AutoAllocation::where([
                                    //     ['item_id_source', $item_id],
                                    //     ['c_order_id', $c_order_id],
                                    //     ['warehouse_id', $warehouse_id],
                                    //     ['no_invoice', $no_invoice],
                                    //     ])
                                    //     ->whereNull('deleted_at')
                                    //     ->sum('qty_on_ship');
        
                                    // $qty_on_ship_web_po += $sisa_qty_on_ship_web_po;
                                    
                                    //sisa qty on ship
                                    if(sprintf('%0.4f', $sum_all_qty_on_ship) - $temp_qty_on_ship > 0)
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        $outstanding_on_ship =  $temp_qty_on_ship - sprintf('%0.4f', $sum_all_qty_on_ship);
                                    }
                                    
                                    //echo $no_invoice. ' total  '. $total_on_ship_web_po. ' sisa '.$outstanding_on_ship. PHP_EOL;

                                    //echo (sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship)). PHP_EOL;
                                    if(sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship) != 0)
                                    {
                                        if($outstanding_on_ship > 0)
                                        {
                                            //jika material yang tersedia lebih dari atau sama dengan alokasi
                                            if($outstanding_on_ship >= ($qty_need - $qty_in_house))
                                            {
                                                $qty_on_ship              = $qty_need - $qty_in_house;
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                $receive_date             = $receive_date;
                                                //echo $qty_on_ship. PHP_EOL;
                                                break;
                                            }
                                            else
                                            {
                                                $qty_on_ship       += $outstanding_on_ship;
                                                if($qty_on_ship >= ($qty_need - $qty_in_house))
                                                {
                                                    $qty_on_ship  = $qty_need - $qty_in_house;
                                                    $eta_date     = $list_on_ship->kst_etadate;
                                                    $etd_date     = $list_on_ship->kst_etddate;
                                                    $receive_date = $receive_date;
                                                    //echo $qty_on_ship. PHP_EOL;
                                                    break;
                                                    
                                                }
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                $receive_date             = $receive_date;
                                                //echo $qty_on_ship. PHP_EOL;
                                            }
                                            

                                        }
                                    }
                                    // else
                                    // {
                                    //     $qty_on_ship  = 0;
                                    //     $eta_date     = null;
                                    //     $etd_date     = null;
                                    //     $receive_date = null;
                                    // }    
                                }
                            }
                            else
                            {
                                // $qty_on_ship  = 0;
                                // $eta_date     = null;
                                // $etd_date     = null;
                                // $receive_date = null;
                                // $no_invoice   = null;
                                //dd('sini');
                                //get MRD

                                // //cek web po yang sudah diterima 
                                // $app_env = Config::get('app.env');
                                // if($app_env == 'live')
                                // {
                                //     $data = DB::connection('web_po');
                                //     $list_eta_etd_in_house = $data->table('material_on_ship_v')
                                //     ->select(db::raw('max(kst_etadate) as etadate , max(kst_etddate) as etddate, kst_invoicevendor'))
                                //     ->where([
                                //         ['m_product_id', $item_id],
                                //         ['c_order_id', $c_order_id],
                                //         ['isactive', true],
                                //         ['flag_wms', true]
                                //     ])
                                //     ->groupBy('kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                                //     ->orderBy('kst_etadate', 'asc')
                                //     ->orderBy('kst_etddate', 'asc')
                                //     ->first();
                                // }
                                //dd($list_eta_etd_in_house);
                                // if($list_eta_etd_in_house)
                                // {
                                //     $eta_date     = $list_eta_etd_in_house->etadate;
                                //     $etd_date     = $list_eta_etd_in_house->etddate;
                                //     $receive_date = $receive_date;
                                //     $no_invoice   = $list_eta_etd_in_house->kst_invoicevendor;
                                // }

                                // if($app_env == 'live')
                                // {
                                    //$data_erp = DB::connection('erp');

                                    $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();
                                    
                                    //dd($list_on_ship_mrds);
                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                            $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            
                                                
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }

                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        //$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        $receive_date = $receive_date;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_mrd. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                        //echo $qty_mrd. PHP_EOL;
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }
                                //}
                            }

                        //}

                    }

                }
                else
                {
                    $qty_in_house = 0;
                    //cek qty on ship di web po 
                    // $app_env = Config::get('app.env');
                    // if($app_env == 'live')
                    //     {
                            //$data = DB::connection('web_po');
                            // dd($item_id, $c_order_id);
                            $list_on_ships = $data->table('material_on_ship_v')
                            ->select(db::raw('sum(qty_upload) as qty_upload'), 'kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            ->where([
                                ['m_product_id', $item_id],
                                ['c_order_id', $c_order_id],
                                ['isactive', true],//ganti dulu 
                                ['flag_wms', false]
                            ])
                            ->groupBy('kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            ->orderBy('kst_etadate', 'asc')
                            ->orderBy('kst_etddate', 'asc')
                            ->get();

                            $sum_on_ship_web_po = $list_on_ships->sum('qty_upload');

                            // dd(count($list_on_ships) );

                            if(count($list_on_ships) > 0)
                            {
                                $temp_qty_on_ship = 0;
                                foreach($list_on_ships as $key => $list_on_ship)
                                {
                                    $sisa = 0;
                                    $no_invoice           = $list_on_ship->kst_invoicevendor;
                                    $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
                                    $temp_qty_on_ship += $total_on_ship_web_po;

                                    $sum_all_qty_on_ship = AutoAllocation::where([
                                        ['item_id_source', $item_id],
                                        ['c_order_id', $c_order_id],
                                        //['warehouse_id', $warehouse_id],
                                        ])
                                        ->whereNull('deleted_at')
                                        ->sum('qty_on_ship');

                                    // $sum_qty_on_ship = AutoAllocation::where([
                                    //     ['item_id_source', $item_id],
                                    //     ['c_order_id', $c_order_id],
                                    //     ['warehouse_id', $warehouse_id],
                                    //     ['no_invoice', $no_invoice],
                                    //     ])
                                    //     ->whereNull('deleted_at')
                                    //     ->sum('qty_on_ship');
                                       
                                    if(sprintf('%0.4f', $sum_all_qty_on_ship) - $temp_qty_on_ship > 0)
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        $outstanding_on_ship =  $temp_qty_on_ship - sprintf('%0.4f', $sum_all_qty_on_ship);
                                    }

                                    //$outstanding_on_ship = sprintf('%0.4f',$total_on_ship_web_po) - sprintf('%0.4f',$sum_qty_on_ship);
                                    
                                    //echo $no_invoice. ' total  '. $total_on_ship_web_po. ' sisa '.$outstanding_on_ship. PHP_EOL;

                                    //echo (sprintf('%0.4f',$sum_on_ship_web_po) - sprintf('%0.4f', $sum_all_qty_on_ship)). PHP_EOL;
                                    if(sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship) != 0)
                                    {
                                        if($outstanding_on_ship > 0)
                                        {
                                            //jika material yang tersedia lebih dari atau sama dengan alokasi
                                            if($outstanding_on_ship >= ($qty_need - $qty_in_house))
                                            {
                                                $qty_on_ship              = $qty_need - $qty_in_house;
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                $receive_date             = null;
                                                //echo $qty_on_ship. PHP_EOL;
                                                break;
                                            }
                                            else
                                            {
                                                $qty_on_ship       += $outstanding_on_ship;
                                                if($qty_on_ship >= ($qty_need - $qty_in_house))
                                                {
                                                    $qty_on_ship  = $qty_need - $qty_in_house;
                                                    $eta_date     = $list_on_ship->kst_etadate;
                                                    $etd_date     = $list_on_ship->kst_etddate;
                                                    //$receive_date = null;
                                                    //echo $qty_on_ship. PHP_EOL;
                                                    break;
                                                    
                                                }
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                $receive_date             = $receive_date;
                                                //echo $qty_on_ship. PHP_EOL;
                                            }
                                            

                                        }
                                    }
                                    // else
                                    // {
                                    //     $qty_on_ship  = 0;
                                    //     $eta_date     = null;
                                    //     $etd_date     = null;
                                    //     $receive_date = null;
                                    // }    
                                }
                                //get mrd
                                $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();
                                    
                                    // dd($list_on_ship_mrds);
                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                            $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            
                                                
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }

                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        //$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        $receive_date = $receive_date;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_mrd. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                        //echo $qty_mrd. PHP_EOL;
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }
                            }
                            else
                            {
                                // $qty_on_ship  = 0;
                                // $eta_date     = null;
                                // $etd_date     = null;
                                // $receive_date = null;
                                // $no_invoice   = null;

                                //get MRD
                                // if($app_env == 'live')
                                // {
                                    //$data_erp = DB::connection('erp');

                                    $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();
                                   
                                    
                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');
                                    // dd($item_id.','. $c_order_id);
                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_ship_web_po;
                                           
                                            $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                ['status_po_buyer', 'active'] //permintaan elva karena kemakan po cancel 
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }
                                            // dd(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd));
                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    // dd($outstanding_on_ship_mrd .' , '. $qty_need );
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        ////$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        $receive_date = $receive_date;
                                                        //echo $qty_on_ship. PHP_EOL;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_on_ship. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }
                                //}
                            }

                    //}
                }

            }

            // foreach($auto_allocations as $key => $auto_allocation)
            // {   
            //     $eta_date     = null;
            //     $etd_date     = null;
            //     $receive_date = null;
            //     $no_invoice   = null;
            //     $qty_in_house = 0;
            //     $qty_on_ship  = 0;
            //     $qty_mrd      = 0;
            //     $mrd          = null;
            //     $dd_pi        = null;
            //     $eta_pi       = null;
            //     $etd_pi       = null;

            //     $auto_allocation_id = $auto_allocation->id;
            //     $item_id            = $auto_allocation->item_id_source;
            //     $warehouse_id       = $auto_allocation->warehouse_id;
            //     $c_order_id         = $auto_allocation->c_order_id;
            //     $lc_date             = $auto_allocation->lc_date;
            //     $qty_need           = sprintf('%0.4f',$auto_allocation->qty_allocation);
            //     $qty_on_ship_alocation = sprintf('%0.4f',$auto_allocation->qty_on_ship);
            //     echo $auto_allocation_id . "\n";
            //     if($c_order_id == 'FREE STOCK')
            //     {
            //         $qty_in_house = $qty_need;
            //         $eta_actual   = $lc_date;
            //         $etd_actual   = $lc_date;
            //         $receive_date = $lc_date;
            //         $no_invoice   = null;

            //     }
            //     else
            //     {

            //     $material_stock = MaterialStock::select(db::raw('sum(stock) as stock'), db::raw('max(created_at) as created_at'))
            //     ->where([
            //     ['item_id', $item_id],
            //     ['c_order_id', $c_order_id],
            //     ['is_stock_mm', false],
            //     ['is_closing_balance', false],
            //     ['approval_date', '!=', null],
            //     ['is_from_handover', false],
            //     ['is_master_roll', true],
            //     //['warehouse_id', $warehouse_id], tidak cek warehouse
            //     ])
            //     ->whereNull('case')
            //     ->first();
            //     //->whereNull('deleted_at')
            //     //->sum('stock');
            //     // dd($material_stock);
            //     $sum_qty_stock = $material_stock->stock;
            //     $receive_date    = $material_stock->created_at;
            //     //$no_invoice    = $material_stock->no_invoice;

            //     $sum_qty_in_house = AutoAllocation::where([
            //         ['item_id_source', $item_id],
            //         ['c_order_id', $c_order_id],
            //         //['warehouse_id', $warehouse_id],
            //         ])
            //         ->whereNull('deleted_at')
            //         ->sum('qty_in_house');

            //     //sisa qty in house 
            //     $material_in_house = sprintf('%0.4f',$sum_qty_stock) - sprintf('%0.4f',$sum_qty_in_house);
            //     // dd($material_in_house);
            //     //jika material yang tersdia > 0
                
            //     if($material_in_house > 0)
            //     {
            //         //dd($material_in_house - $qty_need);
            //         //jika material yang tersedia lebih dari atau sama dengan alokasi
            //         //dd($material_in_house/$qty_need);
            //         if(sprintf('%0.4f',$material_in_house)/$qty_need >= 1)
            //         {
            //             $qty_in_house = $qty_need;
            //             $qty_on_ship  = 0;

            //             //$app_env = Config::get('app.env');
            //             // if($app_env == 'live')
            //             // {
            //                 //$data = DB::connection('web_po');
            //                 $list_on_ships = $webpo_new->select("SELECT sum(qty_upload) as qty_upload, kst_etadate, kst_etddate, kst_invoicevendor,wms_receiving_date
            //                                             FROM material_on_ship_f('$c_order_id','$item_id','t') GROUP BY kst_etadate,kst_etddate,kst_invoicevendor,wms_receiving_date ORDER BY kst_etadate asc , kst_etddate asc");
                        

            //                 if(count($list_on_ships) > 0)
            //                 {
            //                     $sum_on_ship_web_po = 0;
            //                     foreach ($list_on_ships as $item) {
            //                         $sum_on_ship_web_po += $item->qty_upload;
            //                     }
            //                     $temp_qty_on_ship = 0;
            //                     foreach($list_on_ships as $key => $list_on_ship)
            //                     {
            //                         $sisa = 0;
            //                         $no_invoice           = $list_on_ship->kst_invoicevendor;
            //                         $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
            //                         $temp_qty_on_ship += $total_on_ship_web_po;

            //                         $sum_all_qty_on_ship = AutoAllocation::where([
            //                             ['item_id_source', $item_id],
            //                             ['c_order_id', $c_order_id],
            //                             //['warehouse_id', $warehouse_id],
            //                             ])
            //                             ->whereNull('deleted_at')
            //                             ->sum('qty_on_ship');

                                   
                                       
            //                         if(sprintf('%0.4f', $sum_all_qty_on_ship) - $temp_qty_on_ship > 0 )
            //                         {
            //                             continue;
            //                         }
            //                         else
            //                         {
            //                             $outstanding_on_ship =  $temp_qty_on_ship - sprintf('%0.4f', $sum_all_qty_on_ship);
            //                         }

            //                         //$outstanding_on_ship = sprintf('%0.4f',$total_on_ship_web_po) - sprintf('%0.4f',$sum_qty_on_ship);
                                    
            //                         //echo $no_invoice. ' total  '. $total_on_ship_web_po. ' sisa '.$outstanding_on_ship. PHP_EOL;

            //                         //echo (sprintf('%0.4f',$sum_on_ship_web_po) - sprintf('%0.4f', $sum_all_qty_on_ship)). PHP_EOL;
            //                         if(sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship) != 0)
            //                         {
            //                             if($outstanding_on_ship > 0)
            //                             {
            //                                 //jika material yang tersedia lebih dari atau sama dengan alokasi
            //                                 echo "\n" .$outstanding_on_ship >= ($qty_need - $qty_in_house). "\n";
            //                                 if($outstanding_on_ship >= ($qty_need - $qty_in_house))
            //                                 {
                                                
            //                                         $qty_on_ship              = $qty_need - $qty_in_house;
            //                                         $eta_date                 = $list_on_ship->kst_etadate;
            //                                         $etd_date                 = $list_on_ship->kst_etddate;
                                                
                                               
            //                                     //$receive_date             = null;
            //                                     //echo $qty_on_ship. PHP_EOL;
            //                                     break;
            //                                 }
            //                                 else
            //                                 {
            //                                     $qty_on_ship       += $outstanding_on_ship;
            //                                     if($qty_on_ship >= ($qty_need - $qty_in_house))
            //                                     {
            //                                         $qty_on_ship  = $qty_need - $qty_in_house;
            //                                         $eta_date     = $list_on_ship->kst_etadate;
            //                                         $etd_date     = $list_on_ship->kst_etddate;
            //                                         //$receive_date = null;
            //                                         //echo $qty_on_ship. PHP_EOL;
            //                                         break;
                                                    
            //                                     }
            //                                     $eta_date                 = $list_on_ship->kst_etadate;
            //                                     $etd_date                 = $list_on_ship->kst_etddate;
            //                                     //$receive_date             = $receive_date;
            //                                     //echo $qty_on_ship. PHP_EOL;
            //                                 }
                                            

            //                             }
            //                         }
            //                     }
            //                     //get mrd
            //                     $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
            //                         ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
            //                         ->where([
            //                             ['m_product_id', $item_id],
            //                             ['c_order_id', $c_order_id],
            //                         ])
            //                         ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
            //                         ->orderBy('mrd', 'asc')
            //                         ->orderBy('dd_pi', 'asc')
            //                         ->get();
                                    
            //                         //dd($list_on_ship_mrds);
            //                         $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

            //                         if(count($list_on_ship_mrds) > 0)
            //                         {
            //                             $temp_qty_on_ship_mrd = 0;
            //                             foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
            //                             {
            //                                 $sisa = 0;
            //                                 $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
            //                                 $temp_qty_on_ship_mrd += $total_on_qty_mrd;

            //                                 $sum_all_qty_on_ship_mrd = AutoAllocation::where([
            //                                     ['item_id_source', $item_id],
            //                                     ['c_order_id', $c_order_id],
            //                                     ['status_po_buyer', 'active'] //permintaan elva karena kemakan po cancel 
            //                                     //['warehouse_id', $warehouse_id],
            //                                     ])
            //                                     ->whereNull('deleted_at')
            //                                     ->sum('qty_mrd');
                                            
                                                
            //                                 if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
            //                                 {
            //                                     continue;
            //                                 }
            //                                 else
            //                                 {
            //                                     $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
            //                                 }

            //                                 if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
            //                                 {
            //                                     if($outstanding_on_ship_mrd > 0)
            //                                     {
            //                                         //jika material yang tersedia lebih dari atau sama dengan alokasi
            //                                         if($outstanding_on_ship_mrd >= $qty_need)
            //                                         {
            //                                             $qty_mrd      = $qty_need;
            //                                             $mrd          = $list_on_ship_mrd->mrd;
            //                                             $dd_pi        = $list_on_ship_mrd->dd_pi;
            //                                             $eta_pi       = $list_on_ship_mrd->eta;
            //                                             $etd_pi       = $list_on_ship_mrd->etd;
            //                                             //$receive_date = null;
            //                                             //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
            //                                             break;
            //                                         }
            //                                         else
            //                                         {
            //                                             $qty_mrd       += $outstanding_on_ship_mrd;
            //                                             $mrd          = $list_on_ship_mrd->mrd;
            //                                             $dd_pi        = $list_on_ship_mrd->dd_pi;
            //                                             $eta_pi       = $list_on_ship_mrd->eta;
            //                                             $etd_pi       = $list_on_ship_mrd->etd;

            //                                             if($qty_mrd >= $qty_need)
            //                                             {
            //                                                 $qty_mrd      = $qty_need;
            //                                                 $mrd          = $list_on_ship_mrd->mrd;
            //                                                 $dd_pi        = $list_on_ship_mrd->dd_pi;
            //                                                 $eta_pi       = $list_on_ship_mrd->eta;
            //                                                 $etd_pi       = $list_on_ship_mrd->etd;
            //                                                 //$receive_date = null;
            //                                                 //echo $qty_mrd. PHP_EOL;
            //                                                 break;
                                                            
            //                                             }
            //                                             //$receive_date = $receive_date;
            //                                             //echo $qty_mrd. PHP_EOL;
            //                                         }
                                                    

            //                                     }
            //                                 }
            //                             }
            //                         }
            //                 }
                        
            //                         $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
            //                         ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
            //                         ->where([
            //                             ['m_product_id', $item_id],
            //                             ['c_order_id', $c_order_id],
            //                         ])
            //                         ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
            //                         ->orderBy('mrd', 'asc')
            //                         ->orderBy('dd_pi', 'asc')
            //                         ->get();
                                    
            //                         //dd($list_on_ship_mrds);
            //                         $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

            //                         if(count($list_on_ship_mrds) > 0)
            //                         {
            //                             $temp_qty_on_ship_mrd = 0;
            //                             foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
            //                             {
            //                                 $sisa = 0;
            //                                 $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
            //                                 $temp_qty_on_ship_mrd += $total_on_qty_mrd;

            //                                 $sum_all_qty_on_ship_mrd = AutoAllocation::where([
            //                                     ['item_id_source', $item_id],
            //                                     ['c_order_id', $c_order_id],
            //                                     //['warehouse_id', $warehouse_id],
            //                                     ])
            //                                     ->whereNull('deleted_at')
            //                                     ->sum('qty_mrd');
                                            
                                                
            //                                 if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
            //                                 {
            //                                     continue;
            //                                 }
            //                                 else
            //                                 {
            //                                     $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
            //                                 }

            //                                 if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
            //                                 {
            //                                     if($outstanding_on_ship_mrd > 0)
            //                                     {
            //                                         //jika material yang tersedia lebih dari atau sama dengan alokasi
            //                                         if($outstanding_on_ship_mrd >= $qty_need)
            //                                         {
            //                                             $qty_mrd      = $qty_need;
            //                                             $mrd          = $list_on_ship_mrd->mrd;
            //                                             $dd_pi        = $list_on_ship_mrd->dd_pi;
            //                                             $eta_pi       = $list_on_ship_mrd->eta;
            //                                             $etd_pi       = $list_on_ship_mrd->etd;
            //                                             //$receive_date = null;
            //                                             //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
            //                                             break;
            //                                         }
            //                                         else
            //                                         {
            //                                             $qty_mrd       += $outstanding_on_ship_mrd;
            //                                             $mrd          = $list_on_ship_mrd->mrd;
            //                                             $dd_pi        = $list_on_ship_mrd->dd_pi;
            //                                             $eta_pi       = $list_on_ship_mrd->eta;
            //                                             $etd_pi       = $list_on_ship_mrd->etd;
            //                                             $receive_date = $receive_date;

            //                                             if($qty_mrd >= $qty_need)
            //                                             {
            //                                                 $qty_mrd      = $qty_need;
            //                                                 $mrd          = $list_on_ship_mrd->mrd;
            //                                                 $dd_pi        = $list_on_ship_mrd->dd_pi;
            //                                                 $eta_pi       = $list_on_ship_mrd->eta;
            //                                                 $etd_pi       = $list_on_ship_mrd->etd;
            //                                                 //$receive_date = null;
            //                                                 //echo $qty_mrd. PHP_EOL;
            //                                                 break;
                                                            
            //                                             }
            //                                             //echo $qty_mrd. PHP_EOL;
            //                                         }
                                                    

            //                                     }
            //                                 }
            //                             }
            //                         }

            //         }
            //         else
            //         {
            //             $qty_in_house = $material_in_house;
            //             //cek kekurangan di web po
            //             // $app_env = Config::get('app.env');
            //             // if($app_env == 'live')
            //             // {
            //             //     $data = DB::connection('web_po');
            //             $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
            //             ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
            //             ->where([
            //                 ['m_product_id', $item_id],
            //                 ['c_order_id', $c_order_id],
            //             ])
            //             ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
            //             ->orderBy('mrd', 'asc')
            //             ->orderBy('dd_pi', 'asc')
            //             ->get();
                        
            //             //dd($list_on_ship_mrds);
            //             $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

            //             if(count($list_on_ship_mrds) > 0)
            //             {
            //                 $temp_qty_on_ship_mrd = 0;
            //                 foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
            //                 {
            //                     $sisa = 0;
            //                     $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
            //                     $temp_qty_on_ship_mrd += $total_on_qty_mrd;

            //                     $sum_all_qty_on_ship_mrd = AutoAllocation::where([
            //                         ['item_id_source', $item_id],
            //                         ['c_order_id', $c_order_id],
            //                         ['warehouse_id', $warehouse_id],
            //                         ])
            //                         ->whereNull('deleted_at')
            //                         ->sum('qty_mrd');
                                
                                    
            //                     if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
            //                     {
            //                         continue;
            //                     }
            //                     else
            //                     {
            //                         $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
            //                     }

            //                     if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
            //                     {
            //                         if($outstanding_on_ship_mrd > 0)
            //                         {
            //                             //jika material yang tersedia lebih dari atau sama dengan alokasi
            //                             if($outstanding_on_ship_mrd >= $qty_need)
            //                             {
            //                                 $qty_mrd      = $qty_need;
            //                                 $mrd          = $list_on_ship_mrd->mrd;
            //                                 $dd_pi        = $list_on_ship_mrd->dd_pi;
            //                                 $eta_pi       = $list_on_ship_mrd->eta;
            //                                 $etd_pi       = $list_on_ship_mrd->etd;
            //                                 //$receive_date = null;
            //                                 //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
            //                                 break;
            //                             }
            //                             else
            //                             {
            //                                 $qty_mrd       += $outstanding_on_ship_mrd;
            //                                 $mrd          = $list_on_ship_mrd->mrd;
            //                                 $dd_pi        = $list_on_ship_mrd->dd_pi;
            //                                 $eta_pi       = $list_on_ship_mrd->eta;
            //                                 $etd_pi       = $list_on_ship_mrd->etd;
            //                                 $receive_date = $receive_date;
            //                                 //echo $qty_mrd. PHP_EOL;

            //                                 if($qty_mrd >= $qty_need)
            //                                 {
            //                                     $qty_mrd      = $qty_need;
            //                                     $mrd          = $list_on_ship_mrd->mrd;
            //                                     $dd_pi        = $list_on_ship_mrd->dd_pi;
            //                                     $eta_pi       = $list_on_ship_mrd->eta;
            //                                     $etd_pi       = $list_on_ship_mrd->etd;
            //                                     //$receive_date = null;
            //                                     //echo $qty_mrd. PHP_EOL;
            //                                     break;
                                                
            //                                 }
            //                             }
                                        

            //                         }
            //                     }
            //                 }
            //             }

            //                 $list_on_ships = $webpo_new->select("SELECT sum(qty_upload) as qty_upload, kst_etadate, kst_etddate, kst_invoicevendor
            //                         FROM material_on_ship_f('$c_order_id','$item_id','f') GROUP BY kst_etadate,kst_etddate,kst_invoicevendor ORDER BY kst_etadate asc , kst_etddate asc");
            //                 // $list_on_ships = $webpo_new->table('material_on_ship_v')
            //                 // ->select(db::raw('sum(qty_upload) as qty_upload'), 'kst_etadate', 'kst_etddate', 'kst_invoicevendor')
            //                 // ->where([
            //                 //     ['item_id', $item_id],
            //                 //     ['c_order_id', $c_order_id],
            //                 //     // ['isactive', true],
            //                 //     ['flag_wms', false]
            //                 // ])
            //                 // ->groupBy('kst_etadate', 'kst_etddate', 'kst_invoicevendor')
            //                 // ->orderBy('kst_etadate', 'asc')
            //                 // ->orderBy('kst_etddate', 'asc')
            //                 // ->get();

            //                 //dd($list_on_ships);
                         
            //                 if(count($list_on_ships) > 0)
            //                 {
            //                     $sum_on_ship_web_po = 0;
            //                     foreach ($list_on_ships as $item) {
            //                         $sum_on_ship_web_po += $item->qty_upload;
            //                     }
            //                     $temp_qty_on_ship = 0;
            //                     $outstanding_on_ship = 0;
            //                     foreach($list_on_ships as $key => $list_on_ship)
            //                     {
            //                         $no_invoice           = $list_on_ship->kst_invoicevendor;
            //                         $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
            //                         $temp_qty_on_ship += $total_on_ship_web_po;

            //                         $sum_all_qty_on_ship = AutoAllocation::where([
            //                             ['item_id_source', $item_id],
            //                             ['c_order_id', $c_order_id],
            //                             //['warehouse_id', $warehouse_id],
            //                             ])
            //                             ->whereNull('deleted_at')
            //                             ->sum('qty_on_ship');

            //                         // $sum_qty_on_ship = AutoAllocation::where([
            //                         //     ['item_id_source', $item_id],
            //                         //     ['c_order_id', $c_order_id],
            //                         //     ['warehouse_id', $warehouse_id],
            //                         //     ['no_invoice', $no_invoice],
            //                         //     ])
            //                         //     ->whereNull('deleted_at')
            //                         //     ->sum('qty_on_ship');
        
            //                         // $qty_on_ship_web_po += $sisa_qty_on_ship_web_po;
                                    
            //                         //sisa qty on ship
            //                         if(sprintf('%0.4f', $sum_all_qty_on_ship) - $temp_qty_on_ship > 0)
            //                         {
            //                             continue;
            //                         }
            //                         else
            //                         {
            //                             $outstanding_on_ship =  $temp_qty_on_ship - sprintf('%0.4f', $sum_all_qty_on_ship);
            //                         }
                                    
            //                         //echo $no_invoice. ' total  '. $total_on_ship_web_po. ' sisa '.$outstanding_on_ship. PHP_EOL;

            //                         //echo (sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship)). PHP_EOL;
            //                         if(sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship) != 0)
            //                         {
            //                             if($outstanding_on_ship > 0)
            //                             {
            //                                 //jika material yang tersedia lebih dari atau sama dengan alokasi
            //                                 if($outstanding_on_ship >= ($qty_need - $qty_in_house))
            //                                 {
            //                                     $qty_on_ship              = $qty_need - $qty_in_house;
            //                                     $eta_date                 = $list_on_ship->kst_etadate;
            //                                     $etd_date                 = $list_on_ship->kst_etddate;
            //                                     $receive_date             = $receive_date;
            //                                     //echo $qty_on_ship. PHP_EOL;
            //                                     break;
            //                                 }
            //                                 else
            //                                 {
            //                                     $qty_on_ship       += $outstanding_on_ship;
            //                                     if($qty_on_ship >= ($qty_need - $qty_in_house))
            //                                     {
            //                                         $qty_on_ship  = $qty_need - $qty_in_house;
            //                                         $eta_date     = $list_on_ship->kst_etadate;
            //                                         $etd_date     = $list_on_ship->kst_etddate;
            //                                         $receive_date = $receive_date;
            //                                         //echo $qty_on_ship. PHP_EOL;
            //                                         break;
                                                    
            //                                     }
            //                                     $eta_date                 = $list_on_ship->kst_etadate;
            //                                     $etd_date                 = $list_on_ship->kst_etddate;
            //                                     $receive_date             = $receive_date;
            //                                     //echo $qty_on_ship. PHP_EOL;
            //                                 }
                                            

            //                             }
            //                         }
            //                         // else
            //                         // {
            //                         //     $qty_on_ship  = 0;
            //                         //     $eta_date     = null;
            //                         //     $etd_date     = null;
            //                         //     $receive_date = null;
            //                         // }    
            //                     }
            //                 }
            //                 else
            //                 {
            //                     $list_on_ships = $webpo_new->select("SELECT sum(qty_upload) as qty_upload, kst_etadate, kst_etddate, kst_invoicevendor, wms_receiving_date
            //                         FROM material_on_ship_f('$c_order_id','$item_id','t') GROUP BY kst_etadate, kst_etddate, kst_invoicevendor, wms_receiving_date ORDER BY kst_etadate asc , kst_etddate asc");
            //                         // $qty_in_house = $qty_need;
            //                         if(count($list_on_ships) > 0)
            //                         {
            //                             $sum_on_ship_web_po = 0;
            //                             foreach ($list_on_ships as $item) {
            //                                 $sum_on_ship_web_po += $item->qty_upload;
            //                             }
            //                             $temp_qty_on_ship = 0;
                                        
            //                             foreach($list_on_ships as $key => $list_on_ship)
            //                             {
                                            

            //                                 $sisa = 0;
            //                                 $no_invoice           = $list_on_ship->kst_invoicevendor;
            //                                 $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
            //                                 $temp_qty_on_ship += $total_on_ship_web_po;

            //                                 $sum_all_qty_on_ship = AutoAllocation::where([
            //                                     ['item_id_source', $item_id],
            //                                     ['c_order_id', $c_order_id],
            //                                     //['warehouse_id', $warehouse_id],
            //                                     ])
            //                                     ->whereNull('deleted_at')
            //                                     ->sum('qty_on_ship');

            //                                 if(sprintf('%0.4f', $sum_all_qty_on_ship) - $temp_qty_on_ship > 0 )
            //                                 {
                                                
            //                                     continue;

            //                                 }
            //                                 else
            //                                 {
            //                                     $outstanding_on_ship =  $temp_qty_on_ship - sprintf('%0.4f', $sum_all_qty_on_ship);
            //                                 }

            //                                 //$outstanding_on_ship = sprintf('%0.4f',$total_on_ship_web_po) - sprintf('%0.4f',$sum_qty_on_ship);
                                            
            //                                 //echo $no_invoice. ' total  '. $total_on_ship_web_po. ' sisa '.$outstanding_on_ship. PHP_EOL;

            //                                 // echo (sprintf('%0.4f',$sum_on_ship_web_po) - sprintf('%0.4f', $sum_all_qty_on_ship)). PHP_EOL;
            //                                 if(sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship) != 0)
            //                                 {
            //                                     if($outstanding_on_ship > 0)
            //                                     {
            //                                         //jika material yang tersedia lebih dari atau sama dengan alokasi
            //                                         if($outstanding_on_ship >= ($qty_need - $qty_in_house))
            //                                         {
                                                       
            //                                                 $qty_on_ship              = $qty_need - $qty_in_house;
            //                                                 $eta_date                 = $list_on_ship->kst_etadate;
            //                                                 $etd_date                 = $list_on_ship->kst_etddate;
                                                            
            //                                                 $receive_date             = null;
                                                        
                                                       
                                                        
            //                                             //echo $qty_on_ship. PHP_EOL;
            //                                             break;
            //                                         }
            //                                         else
            //                                         {
            //                                             $qty_on_ship       += $outstanding_on_ship;
            //                                             if($qty_on_ship >= ($qty_need - $qty_in_house))
            //                                             {
            //                                                 $qty_on_ship  = $qty_need - $qty_in_house;
            //                                                 $eta_date     = $list_on_ship->kst_etadate;
            //                                                 $etd_date     = $list_on_ship->kst_etddate;
            //                                                 //$receive_date = null;
            //                                                 //echo $qty_on_ship. PHP_EOL;
            //                                                 break;
                                                            
            //                                             }
            //                                             $eta_date                 = $list_on_ship->kst_etadate;
            //                                             $etd_date                 = $list_on_ship->kst_etddate;
            //                                             //$receive_date             = $receive_date;
            //                                             //echo $qty_on_ship. PHP_EOL;
            //                                         }
                                                    

            //                                     }
            //                                 }
            //                                 // else
            //                                 // {
            //                                 //     $qty_on_ship  = 0;
            //                                 //     $eta_date     = null;
            //                                 //     $etd_date     = null;
            //                                 //     $receive_date = null;
            //                                 // }    
            //                             }
                                        
            //                             // foreach($list_on_ships as $key => $list_on_ship)
            //                             // {
            //                             //     $qty_in_house = $qty_need;
            //                             //     $no_invoice   = $list_on_ship->kst_invoicevendor;
            //                             //     $eta_date                 = $list_on_ship->kst_etadate;
            //                             //     $etd_date                 = $list_on_ship->kst_etddate;
            //                             //     $sisa = 0;
            //                             //     $no_invoice           = $list_on_ship->kst_invoicevendor;
            //                             //     $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
            //                             //     // $temp_qty_on_ship += $total_on_ship_web_po;
            //                             //     $receive_date = $list_on_ship->wms_receiving_date;
    
            //                             // }
            //                             //get mrd
            //                             $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
            //                                 ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
            //                                 ->where([
            //                                     ['m_product_id', $item_id],
            //                                     ['c_order_id', $c_order_id],
            //                                 ])
            //                                 ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
            //                                 ->orderBy('mrd', 'asc')
            //                                 ->orderBy('dd_pi', 'asc')
            //                                 ->get();
                                            
            //                                 // dd($list_on_ship_mrds);
            //                                 $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

            //                                 if(count($list_on_ship_mrds) > 0)
            //                                 {
            //                                     $temp_qty_on_ship_mrd = 0;
            //                                     foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
            //                                     {
            //                                         $sisa = 0;
            //                                         $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
            //                                         $temp_qty_on_ship_mrd += $total_on_qty_mrd;

            //                                         $sum_all_qty_on_ship_mrd = AutoAllocation::where([
            //                                             ['item_id_source', $item_id],
            //                                             ['c_order_id', $c_order_id],
            //                                             //['warehouse_id', $warehouse_id],
            //                                             ])
            //                                             ->whereNull('deleted_at')
            //                                             ->sum('qty_mrd');
                                                    
                                                        
            //                                         if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
            //                                         {
            //                                             continue;
            //                                         }
            //                                         else
            //                                         {
            //                                             $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
            //                                         }

            //                                         if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
            //                                         {
            //                                             if($outstanding_on_ship_mrd > 0)
            //                                             {
            //                                                 //jika material yang tersedia lebih dari atau sama dengan alokasi
            //                                                 if($outstanding_on_ship_mrd >= $qty_need)
            //                                                 {
            //                                                     $qty_mrd      = $qty_need;
            //                                                     $mrd          = $list_on_ship_mrd->mrd;
            //                                                     $dd_pi        = $list_on_ship_mrd->dd_pi;
            //                                                     $eta_pi       = $list_on_ship_mrd->eta;
            //                                                     $etd_pi       = $list_on_ship_mrd->etd;
            //                                                     //$receive_date = null;
            //                                                     //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
            //                                                     break;
            //                                                 }
            //                                                 else
            //                                                 {
            //                                                     $qty_mrd       += $outstanding_on_ship_mrd;
            //                                                     $mrd          = $list_on_ship_mrd->mrd;
            //                                                     $dd_pi        = $list_on_ship_mrd->dd_pi;
            //                                                     $eta_pi       = $list_on_ship_mrd->eta;
            //                                                     $etd_pi       = $list_on_ship_mrd->etd;
            //                                                     $receive_date = $receive_date;

            //                                                     if($qty_mrd >= $qty_need)
            //                                                     {
            //                                                         $qty_mrd      = $qty_need;
            //                                                         $mrd          = $list_on_ship_mrd->mrd;
            //                                                         $dd_pi        = $list_on_ship_mrd->dd_pi;
            //                                                         $eta_pi       = $list_on_ship_mrd->eta;
            //                                                         $etd_pi       = $list_on_ship_mrd->etd;
            //                                                         //$receive_date = null;
            //                                                         //echo $qty_mrd. PHP_EOL;
            //                                                         break;
                                                                    
            //                                                     }
            //                                                     //echo $qty_mrd. PHP_EOL;
            //                                                 }
                                                            

            //                                             }
            //                                         }
            //                                     }
            //                                 }
            //                         }else{
            //                             $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
            //                             ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
            //                             ->where([
            //                                 ['m_product_id', $item_id],
            //                                 ['c_order_id', $c_order_id],
            //                             ])
            //                             ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
            //                             ->orderBy('mrd', 'asc')
            //                             ->orderBy('dd_pi', 'asc')
            //                             ->get();
                                        
            //                             //dd($list_on_ship_mrds);
            //                             $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

            //                             if(count($list_on_ship_mrds) > 0)
            //                             {
            //                                 $temp_qty_on_ship_mrd = 0;
            //                                 foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
            //                                 {
            //                                     $sisa = 0;
            //                                     $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
            //                                     $temp_qty_on_ship_mrd += $total_on_qty_mrd;

            //                                     $sum_all_qty_on_ship_mrd = AutoAllocation::where([
            //                                         ['item_id_source', $item_id],
            //                                         ['c_order_id', $c_order_id],
            //                                         //['warehouse_id', $warehouse_id],
            //                                         ])
            //                                         ->whereNull('deleted_at')
            //                                         ->sum('qty_mrd');
                                                
                                                    
            //                                     if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
            //                                     {
            //                                         continue;
            //                                     }
            //                                     else
            //                                     {
            //                                         $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
            //                                     }

            //                                     if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
            //                                     {
            //                                         if($outstanding_on_ship_mrd > 0)
            //                                         {
            //                                             //jika material yang tersedia lebih dari atau sama dengan alokasi
            //                                             if($outstanding_on_ship_mrd >= $qty_need)
            //                                             {
            //                                                 $qty_mrd      = $qty_need;
            //                                                 $mrd          = $list_on_ship_mrd->mrd;
            //                                                 $dd_pi        = $list_on_ship_mrd->dd_pi;
            //                                                 $eta_pi       = $list_on_ship_mrd->eta;
            //                                                 $etd_pi       = $list_on_ship_mrd->etd;
            //                                                 //$receive_date = null;
            //                                                 //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
            //                                                 break;
            //                                             }
            //                                             else
            //                                             {
            //                                                 $qty_mrd       += $outstanding_on_ship_mrd;
            //                                                 $mrd          = $list_on_ship_mrd->mrd;
            //                                                 $dd_pi        = $list_on_ship_mrd->dd_pi;
            //                                                 $eta_pi       = $list_on_ship_mrd->eta;
            //                                                 $etd_pi       = $list_on_ship_mrd->etd;
            //                                                 $receive_date = $receive_date;

            //                                                 if($qty_mrd >= $qty_need)
            //                                                 {
            //                                                     $qty_mrd      = $qty_need;
            //                                                     $mrd          = $list_on_ship_mrd->mrd;
            //                                                     $dd_pi        = $list_on_ship_mrd->dd_pi;
            //                                                     $eta_pi       = $list_on_ship_mrd->eta;
            //                                                     $etd_pi       = $list_on_ship_mrd->etd;
            //                                                     //$receive_date = null;
            //                                                     //echo $qty_mrd. PHP_EOL;
            //                                                     break;
                                                                
            //                                                 }
            //                                                 //echo $qty_mrd. PHP_EOL;
            //                                             }
                                                        

            //                                         }
            //                                     }
            //                                 }
            //                             }

            //                         }

                                    
            //                     //}
            //                 }

            //             //}

            //         }

            //     }
            //     else
            //     {
            //         $qty_in_house = 0;
            //         //cek qty on ship di web po 
            //         // $app_env = Config::get('app.env');
            //         // if($app_env == 'live')
            //         //     {
            //                 //$data = DB::connection('web_po');
            //                 // dd($item_id, $c_order_id);
            //                 $list_on_ships = $webpo_new->select("SELECT sum(qty_upload) as qty_upload, kst_etadate, kst_etddate, kst_invoicevendor
            //                 FROM material_on_ship_f('$c_order_id','$item_id','f') GROUP BY kst_etadate,kst_etddate,kst_invoicevendor ORDER BY kst_etadate asc , kst_etddate asc");
            //                 // $list_on_ships = $webpo_new->table('material_on_ship_v')
            //                 // ->select(db::raw('sum(qty_upload) as qty_upload'), 'kst_etadate', 'kst_etddate', 'kst_invoicevendor')
            //                 // ->where([
            //                 //     ['item_id', $item_id],
            //                 //     ['c_order_id', $c_order_id],
            //                 //     // ['isactive', true],//ganti dulu 
            //                 //     ['flag_wms', false]
            //                 // ])
            //                 // ->groupBy('kst_etadate', 'kst_etddate', 'kst_invoicevendor')
            //                 // ->orderBy('kst_etadate', 'asc')
            //                 // ->orderBy('kst_etddate', 'asc')
            //                 // ->get();

                           
                         
            //                 if(count($list_on_ships) > 0)
            //                 {
            //                     $sum_on_ship_web_po = 0;
            //                     foreach ($list_on_ships as $item) {
            //                         $sum_on_ship_web_po += $item->qty_upload;
            //                     }
            //                     $temp_qty_on_ship = 0;
            //                     foreach($list_on_ships as $key => $list_on_ship)
            //                     {
            //                         $sisa = 0;
            //                         $no_invoice           = $list_on_ship->kst_invoicevendor;
            //                         $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
            //                         $temp_qty_on_ship += $total_on_ship_web_po;

            //                         $sum_all_qty_on_ship = AutoAllocation::where([
            //                             ['item_id_source', $item_id],
            //                             ['c_order_id', $c_order_id],
            //                             //['warehouse_id', $warehouse_id],
            //                             ])
            //                             ->whereNull('deleted_at')
            //                             ->sum('qty_on_ship');

            //                         // $sum_qty_on_ship = AutoAllocation::where([
            //                         //     ['item_id_source', $item_id],
            //                         //     ['c_order_id', $c_order_id],
            //                         //     ['warehouse_id', $warehouse_id],
            //                         //     ['no_invoice', $no_invoice],
            //                         //     ])
            //                         //     ->whereNull('deleted_at')
            //                         //     ->sum('qty_on_ship');
                                       
            //                         if(sprintf('%0.4f', $sum_all_qty_on_ship) - $temp_qty_on_ship > 0)
            //                         {
            //                             continue;
            //                         }
            //                         else
            //                         {
            //                             $outstanding_on_ship =  $temp_qty_on_ship - sprintf('%0.4f', $sum_all_qty_on_ship);
            //                         }

            //                         //$outstanding_on_ship = sprintf('%0.4f',$total_on_ship_web_po) - sprintf('%0.4f',$sum_qty_on_ship);
                                    
            //                         //echo $no_invoice. ' total  '. $total_on_ship_web_po. ' sisa '.$outstanding_on_ship. PHP_EOL;

            //                         //echo (sprintf('%0.4f',$sum_on_ship_web_po) - sprintf('%0.4f', $sum_all_qty_on_ship)). PHP_EOL;
            //                         if(sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship) != 0)
            //                         {
            //                             if($outstanding_on_ship > 0)
            //                             {
            //                                 //jika material yang tersedia lebih dari atau sama dengan alokasi
            //                                 if($outstanding_on_ship >= ($qty_need - $qty_in_house))
            //                                 {
            //                                     $qty_on_ship              = $qty_need - $qty_in_house;
            //                                     $eta_date                 = $list_on_ship->kst_etadate;
            //                                     $etd_date                 = $list_on_ship->kst_etddate;
            //                                     $receive_date             = null;
            //                                     //echo $qty_on_ship. PHP_EOL;
            //                                     break;
            //                                 }
            //                                 else
            //                                 {
            //                                     $qty_on_ship       += $outstanding_on_ship;
            //                                     if($qty_on_ship >= ($qty_need - $qty_in_house))
            //                                     {
            //                                         $qty_on_ship  = $qty_need - $qty_in_house;
            //                                         $eta_date     = $list_on_ship->kst_etadate;
            //                                         $etd_date     = $list_on_ship->kst_etddate;
            //                                         //$receive_date = null;
            //                                         //echo $qty_on_ship. PHP_EOL;
            //                                         break;
                                                    
            //                                     }
            //                                     $eta_date                 = $list_on_ship->kst_etadate;
            //                                     $etd_date                 = $list_on_ship->kst_etddate;
            //                                     $receive_date             = $receive_date;
            //                                     //echo $qty_on_ship. PHP_EOL;
            //                                 }
                                            

            //                             }
            //                         }
            //                         // else
            //                         // {
            //                         //     $qty_on_ship  = 0;
            //                         //     $eta_date     = null;
            //                         //     $etd_date     = null;
            //                         //     $receive_date = null;
            //                         // }    
            //                     }
            //                     //get mrd
            //                     $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
            //                         ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
            //                         ->where([
            //                             ['m_product_id', $item_id],
            //                             ['c_order_id', $c_order_id],
            //                         ])
            //                         ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
            //                         ->orderBy('mrd', 'asc')
            //                         ->orderBy('dd_pi', 'asc')
            //                         ->get();
                                    
            //                         // dd($list_on_ship_mrds);
            //                         $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

            //                         if(count($list_on_ship_mrds) > 0)
            //                         {
            //                             $temp_qty_on_ship_mrd = 0;
            //                             foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
            //                             {
            //                                 $sisa = 0;
            //                                 $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
            //                                 $temp_qty_on_ship_mrd += $total_on_qty_mrd;

            //                                 $sum_all_qty_on_ship_mrd = AutoAllocation::where([
            //                                     ['item_id_source', $item_id],
            //                                     ['c_order_id', $c_order_id],
            //                                     //['warehouse_id', $warehouse_id],
            //                                     ])
            //                                     ->whereNull('deleted_at')
            //                                     ->sum('qty_mrd');
                                            
                                                
            //                                 if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
            //                                 {
            //                                     continue;
            //                                 }
            //                                 else
            //                                 {
            //                                     $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
            //                                 }

            //                                 if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
            //                                 {
            //                                     if($outstanding_on_ship_mrd > 0)
            //                                     {
            //                                         //jika material yang tersedia lebih dari atau sama dengan alokasi
            //                                         if($outstanding_on_ship_mrd >= $qty_need)
            //                                         {
            //                                             $qty_mrd      = $qty_need;
            //                                             $mrd          = $list_on_ship_mrd->mrd;
            //                                             $dd_pi        = $list_on_ship_mrd->dd_pi;
            //                                             $eta_pi       = $list_on_ship_mrd->eta;
            //                                             $etd_pi       = $list_on_ship_mrd->etd;
            //                                             //$receive_date = null;
            //                                             //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
            //                                             break;
            //                                         }
            //                                         else
            //                                         {
            //                                             $qty_mrd       += $outstanding_on_ship_mrd;
            //                                             $mrd          = $list_on_ship_mrd->mrd;
            //                                             $dd_pi        = $list_on_ship_mrd->dd_pi;
            //                                             $eta_pi       = $list_on_ship_mrd->eta;
            //                                             $etd_pi       = $list_on_ship_mrd->etd;
            //                                             $receive_date = $receive_date;

            //                                             if($qty_mrd >= $qty_need)
            //                                             {
            //                                                 $qty_mrd      = $qty_need;
            //                                                 $mrd          = $list_on_ship_mrd->mrd;
            //                                                 $dd_pi        = $list_on_ship_mrd->dd_pi;
            //                                                 $eta_pi       = $list_on_ship_mrd->eta;
            //                                                 $etd_pi       = $list_on_ship_mrd->etd;
            //                                                 //$receive_date = null;
            //                                                 //echo $qty_mrd. PHP_EOL;
            //                                                 break;
                                                            
            //                                             }
            //                                             //echo $qty_mrd. PHP_EOL;
            //                                         }
                                                    

            //                                     }
            //                                 }
            //                             }
            //                         }
            //                 }
            //                 else
            //                 {
            //                     $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
            //                         ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
            //                         ->where([
            //                             ['m_product_id', $item_id],
            //                             ['c_order_id', $c_order_id],
            //                         ])
            //                         ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
            //                         ->orderBy('mrd', 'asc')
            //                         ->orderBy('dd_pi', 'asc')
            //                         ->get();
                            

            //                         $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');
            //                         // dd($sum_on_ship_mrds);
            //                         if(count($list_on_ship_mrds) > 0)
            //                         {
            //                             $temp_qty_on_ship_mrd = 0;
            //                             foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
            //                             {
            //                                 $sisa = 0;
            //                                 $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
            //                                 $temp_qty_on_ship_mrd += $total_on_ship_web_po;
                                        
            //                                 $sum_all_qty_on_ship_mrd = AutoAllocation::where([
            //                                     ['item_id_source', $item_id],
            //                                     ['c_order_id', $c_order_id],
            //                                     ['status_po_buyer', 'active'] //permintaan elva karena kemakan po cancel 
            //                                 //['warehouse_id', $warehouse_id],
            //                                     ])
            //                                     ->whereNull('deleted_at')
            //                                     ->sum('qty_mrd');
                                            
            //                                 // dd(sprintf('%0.4f', $sum_all_qty_on_ship_mrd)- $temp_qty_on_ship_mrd);
            //                                 if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
            //                                 {
            //                                     continue;
            //                                 }
            //                                 else
            //                                 {
            //                                     $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
            //                                 }
            //                                 if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
            //                                 {
            //                                     if($outstanding_on_ship_mrd > 0)
            //                                     {
            //                                         //jika material yang tersedia lebih dari atau sama dengan alokasi
            //                                         if($outstanding_on_ship_mrd >= $qty_need)
            //                                         {
            //                                             $qty_mrd      = $qty_need;
            //                                             $mrd          = $list_on_ship_mrd->mrd;
            //                                             $dd_pi        = $list_on_ship_mrd->dd_pi;
            //                                             $eta_pi       = $list_on_ship_mrd->eta;
            //                                             $etd_pi       = $list_on_ship_mrd->etd;
            //                                             ////$receive_date = null;
            //                                             //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
            //                                             break;
            //                                         }
            //                                         else
            //                                         {
            //                                             $qty_mrd       += $outstanding_on_ship_mrd;
            //                                             $mrd          = $list_on_ship_mrd->mrd;
            //                                             $dd_pi        = $list_on_ship_mrd->dd_pi;
            //                                             $eta_pi       = $list_on_ship_mrd->eta;
            //                                             $etd_pi       = $list_on_ship_mrd->etd;
            //                                             $receive_date = $receive_date;
            //                                             //echo $qty_on_ship. PHP_EOL;

            //                                             if($qty_mrd >= $qty_need)
            //                                             {
            //                                                 $qty_mrd      = $qty_need;
            //                                                 $mrd          = $list_on_ship_mrd->mrd;
            //                                                 $dd_pi        = $list_on_ship_mrd->dd_pi;
            //                                                 $eta_pi       = $list_on_ship_mrd->eta;
            //                                                 $etd_pi       = $list_on_ship_mrd->etd;
            //                                                 //$receive_date = null;
            //                                                 //echo $qty_on_ship. PHP_EOL;
            //                                                 break;
                                                            
            //                                             }
            //                                         }
                                                    

            //                                     }
            //                                 }
            //                             }
            //                         }
                                
            //                     //}
            //                 }

            //         //}
            //     }

            // }
                // dd($qty_need , $qty_in_house , $qty_on_ship);
                //kalau parsial masih ada qty open
                if(sprintf('%0.4f', $qty_need - $qty_in_house - $qty_on_ship) > 0.001)
                {
                    $eta_date     = null;
                    $etd_date     = null;
                    $receive_date = null;
                    $no_invoice   = null;
                }
                
                $auto_allocation_update               = AutoAllocation::find($auto_allocation_id);
                $auto_allocation_update->qty_in_house = $qty_in_house;
                $auto_allocation_update->qty_on_ship  = $qty_on_ship;
                if($auto_allocation_update->eta_actual != null)
                {
                    if($auto_allocation_update->eta_actual != $eta_date)
                    {
                        $auto_allocation_update->remark_delay = $auto_allocation_update->remark_delay.' Delay Pengiriman '.$change_date;
                    }
                }
                $auto_allocation_update->eta_actual   = $eta_date;
                $auto_allocation_update->etd_actual   = $etd_date;
                $auto_allocation_update->eta_date     = $eta_pi;
                $auto_allocation_update->etd_date     = $etd_pi;
                $auto_allocation_update->mrd          = $mrd;
                $auto_allocation_update->dd_pi        = $dd_pi;
                $auto_allocation_update->qty_mrd      = $qty_mrd;
                $auto_allocation_update->receive_date = $receive_date;
                $auto_allocation_update->no_invoice   = $no_invoice;
                $auto_allocation_update->save();

            }

                DB::commit();

                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                }

    } 
    
    public function DoCancelPoBuyer()
    {
        $po_buyers = [
            '0130304547',
            '0130554934',
            '0130632684',
            '0130632685',
            '0130653483',
            '0130653486',
            '0130653538',
            '0130653544',
            '0130653589',
            '0130653671',
            '0130656837',
            '0130656840',
            '0130656843',
            '0130656847',
            '0130656851',
            '0130656859',
            '0130660917',
            '0130663885',
            '0130663932',
            '0130664072',
            '0130664767',
            '0130664774',
            '0130664820',
            '0130664878',
            '0130665910',
            '0130699327',
            '0130700762',
            '0130700927',
            '0130700969',
            '0130701217',
            '0130701253',
            '0130701275',
            '0130701342',
            '0130701379',
            '0130705677',
            '0130709605',
            '0130709828',
            '0130710146',
            '0130711076',
            '0130711170',
            '0130711256',
            '0130711286',
            '0130712006',
            '0130714273',
            '0130733025',
            '0130733239',
            '0130736133',
            '0130736184',
            '0130736272',
            '0130736336',
            '0130736399',
            '0130736601',
            '0130738055',
            '0130738056',
            '0130738130',
            '0130738190',
            '0130743843',
            '0130743854',
            '0130743884'
        ];

        foreach($po_buyers as $po_buyer) {

            $_po_buyer          = $po_buyer;
            $cancel_reason      = "GANTUNGAN CANCEL ELVA";
            $start_date         = MaterialPlanningFabric::where('po_buyer',$_po_buyer)->min('planning_date');
            $end_date           = MaterialPlanningFabric::where('po_buyer',$_po_buyer)->max('planning_date');
        
            try 
            {
                DB::beginTransaction();

                $this->deletePoBuyer($cancel_reason,$_po_buyer);
                $this->deletePlanningFabrics($_po_buyer);
                MasterDataAutoAllocationController::cancelPoBuyer($cancel_reason,$_po_buyer);
                $this->storeCuttingInstructionHeaderReport($start_date,$end_date);
            
                DB::commit();

            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            echo $_po_buyer ." - ". Carbon::now() . "\n";
        }
    }

    static function deletePoBuyer($cancel_reason,$_po_buyer)
    {
        $_po_buyer      = $_po_buyer;
        $cancel_reason  = $cancel_reason;

        try 
        {
            DB::beginTransaction();

            MaterialRequirement::where('po_buyer',$_po_buyer)->update(['qty_required' => 0]);
            MaterialRequirementPerPart::where('po_buyer',$_po_buyer)->update(['qty_required' => 0]);
            DetailMaterialRequirement::where('po_buyer',$_po_buyer)->update(['qty_required' => 0]);
            
            $user           = User::where('name','system')->first();


            PoBuyer::where('po_buyer',$_po_buyer)
            ->whereNull('cancel_date')
            ->update([
                'cancel_reason'     => $cancel_reason,
                'cancel_user_id'    => $user->id,
                'cancel_date'       => carbon::now()
            ]);

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function deletePlanningFabrics($_po_buyer)
    {
        try 
        {
            DB::beginTransaction();

            $detail_material_plannings = DetailMaterialPlanningFabric::select('c_order_id'
                ,'article_no'
                ,'warehouse_id'
                ,'item_id_book'
                ,'item_id_source'
                ,'_style'
                ,'uom'
                ,'planning_date'
                ,'is_piping'
                ,db::raw("sum(qty_booking) as total_booking"))
            ->where('po_buyer','!=',$_po_buyer)
            ->groupby('c_order_id'
                ,'warehouse_id'
                ,'uom'
                ,'item_id_book'
                ,'item_id_source'
                ,'_style'
                ,'article_no'
                ,'planning_date'
                ,'is_piping')
            ->get();

            $movement_date  = carbon::now()->toDateTimeString();
            
            foreach ($detail_material_plannings as $key => $detail_material_planning) 
            {
                
                $c_order_id_preparation         = $detail_material_planning->c_order_id;
                $_style_preparation             = $detail_material_planning->_style;
                $_article_no_preparation        = $detail_material_planning->article_no;
                $_document_no_preparation       = $detail_material_planning->document_no;
                $_warehouse_id_preparation      = $detail_material_planning->warehouse_id;
                $_item_id_book                  = $detail_material_planning->item_id_book;
                $_item_id_source_preparation    = $detail_material_planning->item_id_source;
                $_item_code                     = $detail_material_planning->item_code;
                $_item_code_source_preparation  = $detail_material_planning->item_code_source;
                $_planning_date_preparation     = $detail_material_planning->planning_date->format('d/M/y');
                $_is_piping_preparation         = $detail_material_planning->is_piping;
                $_uom                           = $detail_material_planning->uom;
                $_total_booking_preparation     = sprintf('%0.8f',$detail_material_planning->total_booking);

                $is_material_preparation_exists = MaterialPreparationFabric::where([
                    ['c_order_id',$c_order_id_preparation],
                    ['item_id_book',$_item_id_book],
                    ['item_id_source',$_item_id_source_preparation],
                    ['_style',$_style_preparation],
                    ['article_no',$_article_no_preparation],
                    ['warehouse_id',$_warehouse_id_preparation],
                    ['planning_date',$_planning_date_preparation],
                    ['is_piping',$_is_piping_preparation],
                ])
                ->first();

                if($is_material_preparation_exists)
                {
                    $total_reserved_qty     = sprintf('%0.8f',$is_material_preparation_exists->total_reserved_qty);
                    $total_qty_relax        = sprintf('%0.8f',$is_material_preparation_exists->total_qty_rilex);
                    $curr_remark_planning   = $is_material_preparation_exists->remark_planning;

                    if($_total_booking_preparation != $total_reserved_qty)
                    {
                        $new_qty_oustanding = sprintf('%0.8f',$_total_booking_preparation - $total_qty_relax);
                       
                        if($new_qty_oustanding < 0) $remark_planning = 'pengurangan qty alokasi';
                        else $remark_planning = 'penambahan qty alokasi';

                        HistoryPreparationFabric::create([
                            'material_preparation_fabric_id'    => $is_material_preparation_exists->id,
                            'remark'                            => $remark_planning.'. Po buyer '.$_po_buyer.' Cancel',
                            'qty_before'                        => $total_reserved_qty,
                            'qty_after'                         => $_total_booking_preparation,
                            'created_at'                        => $movement_date,
                            'updated_at'                        => $movement_date,
                        ]);

                        $is_material_preparation_exists->total_reserved_qty     = $_total_booking_preparation;
                        $is_material_preparation_exists->total_qty_outstanding  = $new_qty_oustanding;
                        $is_material_preparation_exists->remark_planning        = ( $curr_remark_planning ? $curr_remark_planning .', Po Buyer '.$_po_buyer.' cancel please reduce qty '.(-1*$new_qty_oustanding).'('.$_uom.') from preparation and do re-preparation roll to buyer' : 'Po Buyer '.$_po_buyer.' cancel please reduce qty '.(-1*$new_qty_oustanding).'('.$_uom.') from preparation and do re-preparation roll to buyer' );
                        $is_material_preparation_exists->save();
                    }
                }
            }
            

            MaterialPlanningFabric::where('po_buyer',$_po_buyer)->delete();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function storeCuttingInstructionHeaderReport($start_date,$end_date)
    {

        $material_planning_fabrics = MaterialPlanningFabric::select(db::raw("substr(job_order,0,POSITION ( ':' IN job_order )) AS style")
            ,'article_no'
            ,'planning_date'
            ,'uom'
            ,'warehouse_id'
            ,db::raw("string_agg ( distinct item_code, ',' ) AS item_code")
            ,db::raw("string_agg ( distinct po_buyer, ',' ) AS po_buyer")
            ,db::raw("sum(qty_consumtion) as qty_need"))
        ->whereBetween('planning_date',[$start_date,$end_date])
        ->groupby(db::raw("substr(job_order,0,POSITION ( ':' IN job_order ))")
            ,'uom'
            ,'article_no'
            ,'warehouse_id'
            ,'planning_date')
        ->get();
        
        try 
        {
            DB::beginTransaction();

            CuttingInstruction::whereBetween('planning_date',[$start_date,$end_date])->delete();

            $movement_date = carbon::now()->toDateTimeString();
            foreach ($material_planning_fabrics as $key => $material_planning_fabric) 
            {
                $qty_need       = sprintf("%.8f",$material_planning_fabric->qty_need);
                $style          = $material_planning_fabric->style;
                $article_no     = $material_planning_fabric->article_no;
                $uom            = $material_planning_fabric->uom;
                $po_buyer       = $material_planning_fabric->po_buyer;
                $item_code      = $material_planning_fabric->item_code;
                $warehouse_id   = $material_planning_fabric->warehouse_id;
                $planning_date  = $material_planning_fabric->planning_date;
                
                $is_exists = CuttingInstruction::where([
                    ['article_no',$article_no],
                    ['style',$style],
                    ['uom',$uom],
                    ['item_code',$item_code],
                    ['planning_date',$planning_date],
                    ['po_buyer',$po_buyer],
                    ['qty_need',$qty_need],
                    ['warehouse_id',$warehouse_id],
                ])
                ->exists();

                if(!$is_exists)
                {
                    CuttingInstruction::FirstOrCreate([
                        'created_at'        => $movement_date,
                        'updated_at'        => $movement_date,
                        'article_no'        => $article_no,
                        'uom'               => $uom,
                        'style'             => $style,
                        'item_code'         => $item_code,
                        'planning_date'     => $planning_date,
                        'po_buyer'          => $po_buyer,
                        'qty_need'          => $qty_need,
                        'warehouse_id'      => $warehouse_id,
                    ]);
                }
                
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function sikatAlokasi()
    {

        $temporaries            = Temporary::where('status','reserved_stock')
                                //   ->where('warehouse', '1000013')
                                  ->orderBy('created_at', 'asc')
                                //   ->take(10)
                                  ->get();
        
        //$update_status = $temporaries->update(['string_1' => 'running']);

        // if(count($temporaries) == 0)
        // {
        //     $temporaries        = Temporary::where('status','reserved_stock')
        //                         ->where('warehouse', '1000002')
        //                         ->whereNull('string_1')
        //                         ->orderBy('created_at', 'desc')
        //                         ->take(10)
        //                         ->get();
        // }
        $confirm_date           = Carbon::now();
        $urut = 1;
            
        foreach ($temporaries as $key => $temporary) 
        {
            try 
            {
                DB::beginTransaction();
                $material_stock_id = $temporary->barcode;

                $material_stock     = MaterialStock::where([
                    ['id',$material_stock_id],
                    ['is_running_stock',true],
                    ['is_closing_balance',false],
                    ['is_allocated',false],
                    ['category','!=','CT'],
                ])
                ->whereNull('last_status')
                ->whereNotNull('approval_date')
                ->whereNull('deleted_at')
                ->first();

                if($material_stock)
                {
                    $material_stock->last_status    = 'prepared';
                    $material_stock->save();

                    $system                     = User::where([
                        ['name','system'],
                        ['warehouse',$material_stock->warehouse_id]
                    ])
                    ->first();

                    ReportStock::doAllocation($material_stock,$system->id,$confirm_date);
    
                    $material_stock->last_status                = null;
                    $material_stock->save();
                }

                $temporary->delete();

                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            echo $urut . ". " . $temporary->barcode . " " . $temporary->warehouse . " " . Carbon::now() . "\n";

            $urut++;
        }

    } 

    private function reAllocationFab()
    {
        $id_alokasi = [
            '078845e0-a021-11ec-a8ba-e9baf8093c26',
            '8b77a3e0-b4ab-11ec-a709-33484755e5d0',
            '7a129110-b4ab-11ec-b26b-3554873aaf94'
        ];

        $auto_allocations = AutoAllocation::whereIn('id', $id_alokasi)
        ->get();

        foreach ($auto_allocations as $key => $auto_allocation)
        {
        
            echo $auto_allocation->id . ' ' . $auto_allocation->qty_allocated .' - '. carbon::now() . "\n";
            //cancel alokasinya

            $auto_allocation_id = $auto_allocation->id;
            //$auto_allocation_id = $auto_allocation;

            $auto_allocation        = AutoAllocation::find($auto_allocation_id);

            if($auto_allocation->is_upload_manual)
            {
                $is_integrate       = false;
            }else
            {
                if($auto_allocation->is_allocation_purchase)  $is_integrate = true;
                else $is_integrate = false;
            }
            
            if($auto_allocation->is_fabric)
            {
                $allocation_item_fabrics = AllocationItem::whereNotNull('auto_allocation_id')
                ->where('auto_allocation_id',$auto_allocation_id)
                ->get();

                try 
                {
                    DB::beginTransaction();

                    foreach ($allocation_item_fabrics as $key => $allocation_item_fabric) 
                    {
                        //kembalikan qty material stock per lot
                        $material_stock_per_lot = MaterialStockPerLot::find($allocation_item_fabric->material_stock_per_lot_id);
                        if($material_stock_per_lot)
                        {
                            $qty_available_lot       = sprintf('%0.8f',$material_stock_per_lot->qty_available);
                            $qty_reserved_lot        = sprintf('%0.8f',$material_stock_per_lot->qty_reserved);

                            $qty_available_lot += $allocation_item_fabric->qty_booking;
                            $qty_reserved_lot  -= $allocation_item_fabric->qty_booking;
                            $material_stock_per_lot->qty_available  = $qty_available_lot;
                            $material_stock_per_lot->qty_reserved   = $qty_reserved_lot;
                            $material_stock_per_lot->save();
                        }

                        DetailMaterialPlanningFabric::where('allocation_item_id',$allocation_item_fabric->id)->delete();
                        $allocation_item_fabric->delete();
                    }
                    
                    $detail_material_plannings = DetailMaterialPlanningFabric::select('document_no'
                        ,'c_order_id'
                        ,'warehouse_id'
                        ,'_style'
                        ,'article_no'
                        ,'item_code'
                        ,'item_code_source'
                        ,'item_id_book'
                        ,'item_id_source'
                        ,'uom'
                        ,'planning_date'
                        ,'is_piping'
                        ,db::raw("sum(qty_booking) as total_booking"))
                    ->where('document_no',$auto_allocation->document_no)
                    ->where('item_code',$auto_allocation->item_code)
                    ->groupby('document_no'
                        ,'warehouse_id'
                        ,'uom'
                        ,'item_code'
                        ,'c_order_id'
                        ,'item_code_source'
                        ,'item_id_book'
                        ,'item_id_source'
                        ,'article_no'
                        ,'_style'
                        ,'planning_date'
                        ,'is_piping')
                    ->get();
        
                    $movement_date  = carbon::now()->toDateTimeString();
                    
                    foreach ($detail_material_plannings as $key => $detail_material_planning) 
                    {
                        
                        $_c_order_id_preparation        = $detail_material_planning->c_order_id;
                        $_article_no_preparation        = $detail_material_planning->article_no;
                        $_document_no_preparation       = $detail_material_planning->document_no;
                        $_style_preparation             = $detail_material_planning->_style;
                        $_warehouse_id_preparation      = $detail_material_planning->warehouse_id;
                        $_item_code                     = $detail_material_planning->item_code;
                        $_item_code_source_preparation  = $detail_material_planning->item_code_source;
                        $_item_id_book                  = $detail_material_planning->item_id_book;
                        $_item_id_source                = $detail_material_planning->item_id_source;
                        $_planning_date_preparation     = $detail_material_planning->planning_date->format('d/M/y');
                        $_is_piping_preparation         = $detail_material_planning->is_piping;
                        $_uom                           = $detail_material_planning->uom;
                        $_total_booking_preparation     = sprintf('%0.8f',$detail_material_planning->total_booking);
        
                        $is_material_preparation_exists = MaterialPreparationFabric::where([
                            ['item_id_book',$_item_id_book],
                            ['item_id_source',$_item_id_source],
                            ['c_order_id',$_c_order_id_preparation],
                            ['article_no',$_article_no_preparation],
                            ['warehouse_id',$_warehouse_id_preparation],
                            ['_style',$_style_preparation],
                            ['planning_date',$_planning_date_preparation],
                            ['is_piping',$_is_piping_preparation],
                        ])
                        ->first();
        
                        if($is_material_preparation_exists)
                        {
                            $total_reserved_qty     = sprintf('%0.8f',$is_material_preparation_exists->total_reserved_qty);
                            $total_qty_relax        = sprintf('%0.8f',$is_material_preparation_exists->total_qty_rilex);
                            $curr_remark_planning   = $is_material_preparation_exists->remark_planning;
        
                            if($_total_booking_preparation != $total_reserved_qty)
                            {
                                $new_qty_oustanding = sprintf('%0.8f',$_total_booking_preparation - $total_qty_relax);
                            
                                if($total_reserved_qty < $_total_booking_preparation) $remark_planning = null;
                                else $remark_planning = null;
        
                                HistoryPreparationFabric::create([
                                    'material_preparation_fabric_id'    => $is_material_preparation_exists->id,
                                    'remark'                            => $remark_planning,
                                    'qty_before'                        => $total_reserved_qty,
                                    'qty_after'                         => $_total_booking_preparation,
                                    'created_at'                        => $movement_date,
                                    'updated_at'                        => $movement_date,
                                ]);
        
                                $is_material_preparation_exists->total_reserved_qty     = $_total_booking_preparation;
                                $is_material_preparation_exists->total_qty_outstanding  = $new_qty_oustanding;
                                $is_material_preparation_exists->remark_planning        = $remark_planning;
                                $is_material_preparation_exists->save();
                            }
                        }
                    }
                    
        
                    DB::commit();
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }
            $item = Item::where('item_code',$auto_allocation->item_code_source)->first();

            HistoryAutoAllocations::FirstOrCreate([
                'auto_allocation_id'        => $auto_allocation_id,
                'document_no_old'           => $auto_allocation->document_no,
                'c_bpartner_id_old'         => $auto_allocation->c_bpartner_id,
                'supplier_name_old'         => $auto_allocation->supplier_name,
                'qty_allocated_old'         => $auto_allocation->qty_allocation,
                'po_buyer_old'              => $auto_allocation->po_buyer,
                'item_id_old'               => $item->item_id,
                'item_code_old'             => $auto_allocation->item_code,
                'warehouse_id_old'          => $auto_allocation->warehouse_id,

                'document_no_new'           => $auto_allocation->document_no,
                'c_bpartner_id_new'         => $auto_allocation->c_bpartner_id,
                'supplier_name_new'         => $auto_allocation->supplier_name,
                'qty_allocated_new'         => $auto_allocation->qty_allocation,
                'po_buyer_new'              => $auto_allocation->po_buyer,
                'item_id_new'               => $item->item_id,
                'item_code_new'             => $auto_allocation->item_code,
                'warehouse_id_new'          => $auto_allocation->warehouse_id,
                
                'remark'                    => 'ALOKASI ULANG UNTUK ALOKASI PER LOT'.' (DULU SUDAH DI ALOKASI SEBESAR '.$auto_allocation->qty_allocated.')',
                'is_integrate'              => true,
                'user_id'                   => '107'
            ]);

            $auto_allocation->update_user_id                    = '107';
            $auto_allocation->qty_outstanding                   = $auto_allocation->qty_allocation;
            $auto_allocation->qty_allocated                     = 0;
            $auto_allocation->is_already_generate_form_booking  = false;
            $auto_allocation->generate_form_booking             = null;
            $auto_allocation->save();

            //alokasiin ulang
            AllocationFabric::updateAllocationFabricItems([$auto_allocation_id],$auto_allocation_id);

        }
    }

    static function doReroute()
    {
        $data = ReroutePoBuyer::whereIn('id',function($q)
        {
            $q->select('string_1')
            ->from('temporaries')
            ->where('string_2','reroute po buyer');
        })
        ->orderby('created_at')
        ->get();
        
        
        //Temporary::where('column_2','')->get();

        foreach ($data as $key => $value)
        {
            echo $value->old_po_buyer.'';

            MDRPC::copyFromOldToNew($value->old_po_buyer,$value->new_po_buyer,null,$value->created_at,$value->note);
            MDRPC::removeOldPoBuyer($value->old_po_buyer,$value->new_po_buyer,null,$value->note);
            //MDRPC::deletePlanningFabrics($value->old_po_buyer);

            Temporary::where([
                ['string_2','reroute po buyer'],
                ['string_1',$value->id],
            ])
            ->delete();

        }
    }

    public function sync_po_buyer()
    {
        $po_buyers       = [
            '0130569460',
            '0130569998',
            '0130597760',
            '0130569166',
            '0130569471',
            '0130569474',
            '0130569478',
            '0130569501',
            '0130569800',
            '0130569821',
            '0130570211',
            '0130570229',
            '0130570235',
            '0130570786',
            '0130570788',
            '0130570793',
            '0130570855',
            '0130570870',
            '0130570871',
            '0130570880',
            '0130570881',
            '0130571011',
            '0130571230',
            '0130571301',
            '0130571305',
            '0130571309',
            '0130571334',
            '0130571374',
            '0130571598',
            '0130571608',
            '0130571772',
            '0130571795',
            '0130572005',
            '0130572022',
            '0130572025',
            '0130572026',
            '0130572036',
            '0130572038',
            '0130572046',
            '0130572051',
            '0130572057',
            '0130572071',
            '0130572099',
            '0130572110',
            '0130572168',
            '0130572171',
            '0130572234',
            '0130572250',
            '0130572459',
            '0130572470',
            '0130572480',
            '0130572481',
            '0130572485',
            '0130572486',
            '0130572495',
            '0130572496',
            '0130572523',
            '0130572532',
            '0130572555',
            '0130572987',
            '0130573009',
            '0130573022',
            '0130573030',
            '0130573045',
            '0130573118',
            '0130573120',
            '0130573130',
            '0130573139',
            '0130573315',
            '0130573674',
            '0130573697',
            '0130573727',
            '0130573742',
            '0130573936',
            '0130574207',
            '0130574235',
            '0130574977',
            '0130575011',
            '0130575273',
            '0130575291',
            '0130575292',
            '0130575293',
            '0130575302',
            '0130575379',
            '0130575389',
            '0130575427',
            '0130575435',
            '0130575442',
            '0130575537',
            '0130575538',
            '0130576227',
            '0130576397',
            '0130576743',
            '0130576865',
            '0130576866',
            '0130576867',
            '0130576868',
            '0130576877',
            '0130576881',
            '0130576884',
            '0130576886',
            '0130576887',
            '0130576890',
            '0130576892',
            '0130576898',
            '0130576901',
            '0130576903',
            '0130576904',
            '0130576907',
            '0130576908',
            '0130576910',
            '0130576914',
            '0130576917',
            '0130576920',
            '0130576921',
            '0130576926',
            '0130576927',
            '0130576929',
            '0130576930',
            '0130576932',
            '0130576938',
            '0130576940',
            '0130576942',
            '0130576945',
            '0130576946',
            '0130576950',
            '0130576972',
            '0130577077',
            '0130577078',
            '0130577082',
            '0130577085',
            '0130577108',
            '0130577115',
            '0130577116',
            '0130577134',
            '0130577143',
            '0130577186',
            '0130577223',
            '0130577225',
            '0130577227',
            '0130577228',
            '0130577231',
            '0130577233',
            '0130577235',
            '0130577238',
            '0130577241',
            '0130577242',
            '0130577250',
            '0130577252',
            '0130577253',
            '0130577256',
            '0130577258',
            '0130577259',
            '0130577260',
            '0130577348',
            '0130577455',
            '0130577641',
            '0130577950',
            '0130577962',
            '0130578115',
            '0130578116',
            '0130578118',
            '0130578119',
            '0130578122',
            '0130578131',
            '0130578148',
            '0130578166',
            '0130578167',
            '0130578168',
            '0130578169',
            '0130579163',
            '0130579164',
            '0130579165',
            '0130579166',
            '0130579176',
            '0130579178',
            '0130579375',
            '0130597296',
            '0130597329',
            '0130597418',
            '0130597440',
            '0130597441',
            '0130598058',
            '0130598111',
            '0130598257',
            '0130598259',
            '0130598273',
            '0130598275',
            '0130598298'
        ];

        $movement_date  = carbon::now()->toDateTimeString();

        foreach($po_buyers as $po_buyer) {

            $check = DB::table('material_requirements')
                    ->where('po_buyer', $po_buyer)
                    ->exists();

            if(!$check) {
                ErpMaterialRequirementController::getPobuyer($po_buyer, '');
                ErpMaterialRequirementController::getSummaryBuyerPerPart($po_buyer, '');
                ErpMaterialRequirementController::getSummaryBuyer($po_buyer, '');

                FabricMaterialPlanningController::getAllocationFabricPerBuyer([$po_buyer]);

                echo $po_buyer." == ".carbon::now()."\n";
            }

        }
        
    }

    static function dailyPurchaseItemsFix() {

        $uuidz = [
            '29749cf8-866e-42bf-b884-64ed67b27178',
            'dda4b528-e7bf-4b76-8968-460c2d9c96b5',
            '4a651f25-7218-416f-af4e-5903cc8cc089',
            '800fa355-112d-4233-b48f-ea7d1a3268df',
            'df775924-4ccb-4552-9f34-abf64f60eb31',
            '9e884388-2499-41ec-929d-1f00bc56acda',
            '55c8d5ba-67dd-4774-b80e-f60bb824849b',
            '7920aa0b-c3e3-4bf8-ab8c-dee6813c9377',
            '3657c05e-908c-4602-bff4-cb40c19997e6',
            'ddccc661-4f07-4a7b-b605-ba184ef7c4b6',
            '9513f3b0-67f5-468b-9279-784a6c86faa3',
            '973526cc-558f-4024-b03a-2a81355172e6',
            '50a8e1a7-97c1-46f7-8470-8ade5529d5aa',
            '3fe2c85b-8008-4d71-b068-62f72a17f833',
            'a221a47e-d01b-4404-a6d5-052e8f6e48e3',
            '1199f114-b3ef-4a3c-b00b-d2973b86b9af',
            'f9ee6eb7-444f-4e01-8d37-e0accb5ede94',
            '4c4a09fa-67be-4a2d-a8f2-4a87c29329d0',
            'fcf405c1-e55b-48d3-8ba4-b5253bf674cf',
            '94d2c588-d6ad-4131-8ebf-fe07936e176c',
            'ccf0f470-0bfe-4dfe-b94a-97226b23247f',
            '6089ef63-df5c-4a84-8973-6058d89527d8',
            'a905968e-a2fe-4324-b625-e22e4a7f465a',
            '4cdae4b1-9b04-483d-b150-7474013f689a',
            '325e8241-ceec-493b-999e-5d7149c8c931',
            'c7257899-86ff-4a07-9957-9f81194f8197',
            'e1490114-10d9-4c8f-8768-b9dbaf57af9d',
            'fca9d4b5-7b60-4ddd-8fab-c768823a1258',
            '83674f18-9309-4570-ad72-9451eef9c642',
            '648298a7-4e07-4fbe-afa2-c872dc8565dc',
            '0c272f63-224a-4f91-8782-d6ddcc253f2b',
            '86a539a5-5ce3-4a6f-ba8f-a1ae93127f43',
            '08a254d7-9708-4014-a147-f8e4459ca72b',
            '012bfef1-4607-40ad-9956-58a7ab320a6e',
            'a5453b9a-64d5-45b4-bd06-b06f971b0356',
            '881bf9c8-18ab-4935-98a8-e10858295f0a',
            '36e677c9-7cab-4194-aa5e-580086eeb5f6',
            'ebc4d0c9-a634-4ed4-ac49-a9931b0a0101',
            '7e853b79-fd7f-44be-9f3f-2447494cb359',
            'e9da70d6-6677-42a8-8631-ae0fc3a59080',
            '56f01c16-0e5e-4d60-b112-e62a20ee4c98',
            'cb40bc09-661f-4d62-b10f-209fd8fa06c8',
            'eac33969-e947-472f-bf2c-19743a519055',
            'c55f001c-5109-4022-87b4-5bbc228ce2d4',
            'c680b3b7-e46a-4840-83b5-f420deccec93',
            'a23946af-9a58-4a10-ab88-adfe3d50194d',
            'a68f709a-cfcc-416e-b79a-6a4ad353a5af',
            'd12f4e2b-3682-4062-baf5-a0e34698ffc8',
            '74d129b1-cecb-4349-8196-9c6d413c689b',
            '074bbb92-1b6b-44f6-9690-514bc00cfb88',
            '14b9589b-4ee6-489c-8a78-1bd8015b2f52',
            '96c24167-2e2e-4de4-a0c3-d4c52b9c770f',
            '231b51af-c2d5-4efd-a01d-3f077b1d8213',
            '8ad631b5-0bfc-47fc-b8c7-a767b4f7a5c9',
            '4464c4f0-cce0-40c8-852a-97ba4fcfd028',
            'a04ceaf4-4505-401e-9ad8-8f297919d396',
            '164c86db-4cde-48a3-b2ae-11f1a0252b6f',
            '59662e1f-aad7-4bfb-94e0-d146227de8df',
            '7c7faf72-92fe-4d17-b64e-f27b3aaab740',
            'b09ccafc-fea7-4973-8f1e-4743d3b13c6f',
            '16f4d4cd-b89f-45fd-8d5a-3239549b4100',
            '8fd42fac-62fa-44cd-a39e-d5b8da204240',
            'bd5604ad-10fd-46f6-83c8-44fad8aed485',
            '9d05bb7a-c79c-4f0a-b039-9f2ceeeb27de',
            '8f2f33f0-1b69-4f2f-a306-00cd1f2d49f3',
            '816600ff-c3c8-4ad7-b314-9c1f51f1c115',
            '9855aa3d-64a9-4e8d-af48-9877cfab6b52',
            'db7d538c-39b1-4b75-bb11-38e4474a8360',
            'ddf0a2b5-5194-4c27-b141-6df1b2e41274',
            'ce6213b0-7180-49f1-a0e0-b9eafaf3b30a',
            '67616ca4-5bf5-4ead-9738-70a809e78ad4',
            '1b398e52-5f01-4223-93e2-8018b99e07c6',
            '0b11785d-94f0-435d-ac67-59dea9a498ae',
            'fd5a48d7-809e-4a3d-b5ff-f527601c213c',
            '602eed15-8b13-44ee-b52e-c47e184e5b47',
            '5afc3901-3258-4af5-9cf7-0ae881dcdbcd',
            'eb26241b-d40f-4744-9498-554964e848fc',
            '23fdb625-62fa-4ba1-b54f-b00dda99ddf4',
            'c09f40f9-5ac5-4177-94ea-199b7c622c8e',
            '2db7e2de-e552-40a9-b0fd-7b2d7ee8ac28',
            'a22575bb-6000-4909-b3f8-359c4ebed1c5',
            '97cb5991-a19f-4791-8792-a10d3055d804',
            '9715af76-776e-42f7-a80f-217043299e5a',
            'c4c2cdf7-0900-4fe4-b519-1bc943e05323',
            '8d81989b-db30-4e64-9ca9-2a6cedd14b2b',
            '7863c449-b95a-49e3-9f97-9a25cd64322e',
            '11866c5f-e7a3-4507-af07-f275bd96d9ca',
            '955eca1c-5104-44f4-a005-996690c0ec02',
            'bc5dfe41-e99c-41e9-bcc8-ef992f631993',
            'e43ce876-67de-4495-84a9-645ea0d77dd1',
            'a7f22583-071d-4b82-8727-e48f5071d32c',
            '50b7c1f0-9af4-4212-aa30-5826fa4ec8b6',
            '9cad8825-1b85-429d-b5b4-4c6eef643c6c'
        ];

        $urutan = 1;

        $movement_date = Carbon::now();
        $erp_id        = array();

        $purchase_items = DB::connection('erp') //dev_erp //erp
            ->table('rma_wms_planning_alokasi') // rma_wms_planning_v2_cron -->viewnya
            // ->where([
            //     ['isintegrate',false],
            //     ['status_lc','PR'],
            //     ['is_additional', 'f'],
            // ])
            ->whereIN('uuid', $uuidz)
            ->get();

        foreach ($purchase_items as $key => $purchase_item) {
            
            echo $urutan . ". " . $purchase_item->uuid ." == ". $movement_date . "\n";

            $check_wms = DB::table('auto_allocations')
                            ->whereNull('deleted_at')
                            ->Where('erp_allocation_id', $purchase_item->uuid)
                            ->exists();
            
            if(!$check_wms) {

                $c_order_id        = $purchase_item->c_order_id;
                $item_id           = $purchase_item->item_id;
                $c_bpartner_id     = $purchase_item->c_bpartner_id;
                $supplier_name     = strtoupper($purchase_item->supplier_name);
                $item_code         = strtoupper($purchase_item->item_code);
                $document_no       = strtoupper($purchase_item->document_no);
                $purchase_po_buyer = trim(str_replace('-S', '',$purchase_item->po_buyer));

                $qty_sum = DB::connection('erp') //dev_erp //erp
                ->table('rma_wms_planning_alokasi') // rma_wms_planning_v2_cron -->viewnya
                ->where([
                    ['status_lc','PR'],
                    ['is_additional', 'f'],
                    ['c_order_id', $c_order_id],
                    ['po_buyer', 'like', '%'.$purchase_po_buyer.'%'],
                    ['document_no', $document_no],
                    ['item_code', $item_code],
                    ['item_id', $item_id],
                    ['c_bpartner_id', $c_bpartner_id]
                ])
                ->sum('qty');
                
                $warehouse_id      = $purchase_item->m_warehouse_id;
                $uom               = $purchase_item->uom;
                $category          = $purchase_item->category;
                $qty               = sprintf('%0.8f',$qty_sum);
                $erp_allocation_id = $purchase_item->uuid;
                $is_fabric         = $purchase_item->isfabric;

                if ($purchase_item->is_additional == 'f') {
                    $is_additional = false;
                } else {
                    $is_additional = true;
                }
                
                if($warehouse_id == '1000011') $warehouse_name = 'FABRIC AOI 2';
                else if($warehouse_id == '1000001') $warehouse_name = 'FABRIC AOI 1';
                else if($warehouse_id == '1000013') $warehouse_name = 'ACCESSORIES AOI 2';
                else if($warehouse_id == '1000002') $warehouse_name = 'ACCESSORIES AOI 1';

                $reroute            = ReroutePoBuyer::where('old_po_buyer',$purchase_po_buyer)->first();

                if ($reroute) {
                    $new_po_buyer = $reroute->new_po_buyer;
                    $old_po_buyer = $reroute->old_po_buyer;
                } else {
                    $new_po_buyer = $purchase_po_buyer;
                    $old_po_buyer = $purchase_po_buyer;
                }

                $buyer_detail = PoBuyer::where('po_buyer',$new_po_buyer)->first();
                

                if ($buyer_detail) {
                    $lc_date                    = $buyer_detail->lc_date;
                    $promise_date               = ($buyer_detail->statistical_date ? $buyer_detail->statistical_date : $buyer_detail->promise_date);
                    $season                     = $buyer_detail->season;
                    $type_stock_erp_code        = $buyer_detail->type_stock_erp_code;
                    $cancel_date                = $buyer_detail->cancel_date;

                    if($is_fabric == 'N')
                    {
                        $tgl_date                   = ($buyer_detail->lc_date ? $buyer_detail->lc_date->format('d') : 'LC NOT FOUND');
                        $month_lc                   = ($buyer_detail->lc_date ? $buyer_detail->lc_date->format('my')  : 'LC NOT FOUND');

                        if($warehouse_id == '1000002') $warehouse_sequnce = '001';
                        else if($warehouse_id == '1000013') $warehouse_sequnce = '002';
                    
                        $document_allocation_number = 'PRLC'.$tgl_date.'-'.$month_lc.'-'.$warehouse_sequnce;
                    }
                    
                } else {
                    $promise_date               = null;
                    $lc_date                    = null;
                    $season                     = null;
                    $type_stock_erp_code        = null;
                    $cancel_date                = null;
                    $document_allocation_number = null;
                }
                
                if ($type_stock_erp_code == 1) $type_stock      = 'SLT';
                else if($type_stock_erp_code == 2) $type_stock  = 'REGULER';
                else if($type_stock_erp_code == 3) $type_stock  = 'PR/SR';
                else if($type_stock_erp_code == 4) $type_stock  = 'MTFC';
                else if($type_stock_erp_code == 5) $type_stock  = 'NB';
                else $type_stock                                = null;

                
                if($cancel_date != null) $status_po_buyer = 'cancel';
                else $status_po_buyer                     = 'active';
                
                $system             = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();
    
                $item               = Item::where('item_id',$item_id)->first();
                $item_desc          = ($item)? strtoupper($item->item_desc): 'data not found';
                $is_exists          = null;

                try {
                    
                    DB::beginTransaction();

                    //alokasi FABRIC
                    if($lc_date >= '2019-08-15' && $category == 'FB') {
                        
                            $is_exists = AutoAllocation::whereNotNull('erp_allocation_id')
                            ->where([
                                ['c_order_id',$c_order_id],
                                ['c_bpartner_id',$c_bpartner_id],
                                ['item_id_book',$item_id],
                                ['item_id_source',$item_id],
                                ['po_buyer',$new_po_buyer],
                                ['erp_allocation_id',$erp_allocation_id],
                                ['warehouse_id',$warehouse_id],
                                ['is_fabric',true]
                            ])
                            ->exists();

                            if(!$is_exists)
                            {
                                $newAllocation = AutoAllocation::create([
                                    'type_stock_erp_code'               => $type_stock_erp_code,
                                    'type_stock'                        => $type_stock,
                                    'lc_date'                           => $lc_date,
                                    'promise_date'                      => $promise_date,
                                    'season'                            => $season,
                                    'document_no'                       => $document_no,
                                    'c_bpartner_id'                     => $c_bpartner_id,
                                    'supplier_name'                     => $supplier_name,
                                    'po_buyer'                          => $new_po_buyer,
                                    'c_order_id'                        => $c_order_id,
                                    'item_id_book'                      => $item_id,
                                    'item_id_source'                    => $item_id,
                                    'old_po_buyer'                      => $old_po_buyer,
                                    'item_code_source'                  => $item_code,
                                    'item_code_book'                    => $item_code,
                                    'item_code'                         => $item_code,
                                    'item_desc'                         => $item_desc,
                                    'category'                          => $category,
                                    'uom'                               => $uom,
                                    'warehouse_name'                    => $warehouse_name,
                                    'warehouse_id'                      => $warehouse_id,
                                    'qty_allocation'                    => $qty,
                                    'qty_outstanding'                   => $qty,
                                    'qty_allocated'                     => 0,
                                    'status_po_buyer'                   => $status_po_buyer,
                                    'is_fabric'                         => true,
                                    'is_already_generate_form_booking'  => false,
                                    'is_upload_manual'                  => false,
                                    'is_allocation_purchase'            => true,
                                    'user_id'                           => $system->id,
                                    'created_at'                        => $movement_date,
                                    'updated_at'                        => $movement_date,
                                    'erp_allocation_id'                 => $erp_allocation_id,
                                    'is_additional'                     => $is_additional,
                                ]);

                                $erp_id [] = $newAllocation->erp_allocation_id;   
                            }
                            else
                            {
                                $system = User::where([
                                    ['name','system'],
                                    ['warehouse',$warehouse_id]
                                ])
                                ->first();

                                $old_qty_allocation  = sprintf('%0.8f',$is_exists->qty_allocation);
                                $old_qty_outstanding = sprintf('%0.8f',$is_exists->qty_outstanding);
                                $remark              = $is_exists->remark;
            
                                if ($old_qty_allocation-$qty == 0) {
                                    $is_exists->erp_allocation_id = $erp_allocation_id;
                                    $is_exists->save();
                                } else {
                                    $is_exists->qty_allocation    = $qty;
                                    $is_exists->qty_outstanding   = $qty - $old_qty_outstanding;
                                    $is_exists->erp_allocation_id = $erp_allocation_id;
                                    $tot = number_format($qty, 0, '.', '') - number_format($old_qty_outstanding, 0, '.', '');
                                    $is_exists->remark            = $remark. 'PENAMBAHAN QTY ALLOCATION SEBESAR '. strval($tot);
                                    $is_exists->user_id           = $system->id;
                                    $is_exists->save();
                                }

                                $erp_id [] = $erp_allocation_id;
                            }

                    //alokasi acc
                    } else if($category != 'FB' && ($new_po_buyer != null || $new_po_buyer != '')) {

                        $is_exists = AutoAllocation::whereNotNull('erp_allocation_id')
                        ->where([
                            ['c_order_id',$c_order_id],
                            ['c_bpartner_id',$c_bpartner_id],
                            ['item_id_book',$item_id],
                            ['item_id_source',$item_id],
                            ['po_buyer',$new_po_buyer],
                            ['warehouse_id',$warehouse_id],
                            ['is_fabric',false],
                        ])
                        ->whereNull('deleted_at')
                        ->first();

                        if(!$is_exists)
                        {
                            $newAllocation = AutoAllocation::create([
                                'document_allocation_number'        => $document_allocation_number,
                                'type_stock_erp_code'               => $type_stock_erp_code,
                                'type_stock'                        => $type_stock,
                                'lc_date'                           => $lc_date,
                                'promise_date'                      => $promise_date,
                                'season'                            => $season,
                                'c_order_id'                        => $c_order_id,
                                'document_no'                       => $document_no,
                                'c_bpartner_id'                     => $c_bpartner_id,
                                'supplier_name'                     => $supplier_name,
                                'po_buyer'                          => $new_po_buyer,
                                'old_po_buyer'                      => $old_po_buyer,
                                'item_code_source'                  => $item_code,
                                'item_code_book'                    => $item_code,
                                'item_id_book'                      => $item_id,
                                'item_id_source'                    => $item_id,
                                'item_code'                         => $item_code,
                                'item_desc'                         => $item_desc,
                                'category'                          => $category,
                                'uom'                               => $uom,
                                'warehouse_name'                    => $warehouse_name,
                                'warehouse_id'                      => $warehouse_id,
                                'qty_allocation'                    => $qty,
                                'qty_outstanding'                   => $qty,
                                'qty_allocated'                     => 0,
                                'status_po_buyer'                   => $status_po_buyer,
                                'is_fabric'                         => false,
                                'is_already_generate_form_booking'  => false,
                                'is_upload_manual'                  => false,
                                'is_allocation_purchase'            => true,
                                'created_at'                        => $movement_date,
                                'updated_at'                        => $movement_date,
                                'user_id'                           => $system->id,
                                'erp_allocation_id'                 => $erp_allocation_id,
                                'is_additional'                     => $is_additional,
                            ]);

                            $erp_id [] = $newAllocation->erp_allocation_id;

                        } else {

                                $system = User::where([
                                    ['name','system'],
                                    ['warehouse',$warehouse_id]
                                ])
                                ->first();

                                $old_qty_allocation  = sprintf('%0.8f',$is_exists->qty_allocation);
                                $old_qty_outstanding = sprintf('%0.8f',$is_exists->qty_outstanding);
                                $remark              = $is_exists->remark;
            
                                if($qty == 0){
                                    $bagi = 0;
                                } else {
                                    $bagi = $old_qty_allocation/$qty;
                                }

                                if($bagi == 1) {

                                    $is_exists->erp_allocation_id = $erp_allocation_id;
                                    $is_exists->save();

                                } else {

                                    $is_exists->qty_allocation    = $old_qty_allocation + $qty;
                                    $is_exists->qty_outstanding   = $old_qty_outstanding + $qty;
                                    $is_exists->erp_allocation_id = $erp_allocation_id;
                                    $is_exists->remark            = $remark. 'PENAMBAHAN QTY ALLOCATION SEBESAR '.$qty;
                                    $is_exists->user_id           = $system->id;
                                    $is_exists->save();

                                }

                                if ($old_qty_allocation-$qty == 0) {
                                    $is_exists->erp_allocation_id = $erp_allocation_id;
                                    $is_exists->save();
                                } else {
                                    $is_exists->qty_allocation    = $qty;
                                    $is_exists->qty_outstanding   = $qty - $old_qty_outstanding;
                                    $is_exists->erp_allocation_id = $erp_allocation_id;

                                    $tot = number_format($qty, 0, '.', '') - number_format($old_qty_outstanding, 0, '.', '');
                                    $is_exists->remark            = $remark. 'PENAMBAHAN QTY ALLOCATION SEBESAR '. strval($tot);

                                    $is_exists->user_id           = $system->id;
                                    $is_exists->save();
                                }

                                $erp_id [] = $erp_allocation_id;

                        }
                        
                    }

                    DB::connection('erp') //dev_erp //erp
                    ->table('rma_wms_planning_alokasi')
                    ->whereIn('uuid',$erp_id)
                    ->update([
                        'isintegrate' => true,
                        'updated'     => $movement_date,
                        'source'    => 'test'
                    ]);

                    DB::commit();

                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

            }

            $urutan++;

        }

        // $checkAllocations =  DB::table('auto_allocations') //double check alokasi yg dimasukin pake erp_allocation_id
        //             ->whereIn('erp_allocation_id', $erp_id)
        //             ->whereNull('deleted_at')
        //             ->whereDate('created_at', $movement_date)
        //             ->get();

        // $groupByErpId  =  $checkAllocations->groupBy('erp_allocation_id'); //gatau knp kalo di grouping diatas tetep double, jadi tak grouping disini

        // foreach ($groupByErpId as $key => $del){

        //     if(count($del) >1) {

        //         $id = $del[0]->id;
        //         $t = $del->where('id','!=', $id)->pluck('id');
        //         AutoAllocation::whereIn('id', $t)->delete();

        //     }

        // }

    }

    static function allocationManualFix() 
    {
        $update_allocations_acc = [
            'af8aa1a0-9d7c-11ec-b35d-6770339afe6f',
            'e3abd600-a3c9-11ec-873b-7b444c7fa81f',
            '57b63270-9d7d-11ec-835a-d9aac2911762',
            '0510abf0-8ea9-11ec-b5f6-076c4555378b',
            'fff2e080-9d7d-11ec-bba2-77777873f118',
            'a7273cf0-9d7e-11ec-ae7f-4ff74bb32688',
            '4f41b030-9d7f-11ec-8b32-2128d9d763a7',
            'f7447040-9d7f-11ec-8d1e-5b1a2512f428',
            '9f7982a0-9d80-11ec-af9b-2bdd1903e389',
            '7143d9b0-a891-11ec-9057-f7d174098fdf',
            '72b8fa90-a891-11ec-bb60-e19bac76ba37',
            '74363180-a891-11ec-8dfd-2d1fa3ef28bc',
            '75b21450-a891-11ec-852b-b71d2b5569ee',
            '77399c70-a891-11ec-83db-015f42a312a4',
            '848509b0-a891-11ec-b3a4-2fdb308a8885',
            '863dbde0-a891-11ec-a7ad-99d9a899c23d',
            'f503ce00-a305-11ec-94b3-ddfc47ea5472',
            '0b5be880-a306-11ec-a05d-11c8a7317514',
            '21b25390-a306-11ec-9f8b-b952d2da9266',
            '38320a80-a306-11ec-8bfb-e7e9f3fb286b',
            '0174b260-a893-11ec-a4f0-e9bb0931f746',
            '06022a70-a893-11ec-ac4d-1d01a936cd33',
            '0a928a70-a893-11ec-bf07-53c8d5b3ff1b',
            '13c45f60-a893-11ec-8d0a-3f92ac6199f9',
            '1869f790-a893-11ec-b120-63f62c87d7ab',
            '4e9831b0-a306-11ec-b784-dd46ed970b17',
            '1d11a840-a893-11ec-840e-27fbde65f7d7',
            '21c219f0-a893-11ec-bee2-3d81b6cab0ae',
            '267ba1c0-a893-11ec-bdc9-a5bd3dd4956f',
            '2b3d7fa0-a893-11ec-9474-f572e998fcf2',
            '3003f860-a893-11ec-92bd-6f950582e983',
            '34cbf9c0-a893-11ec-b2e4-3b551ccd4f46',
            '399aab20-a893-11ec-8610-211a157d1c12',
            '3e6c5e00-a893-11ec-8f45-4b4451ac075e',
            '650e0ee0-a306-11ec-a020-7375729cc362',
            '701d0e50-a893-11ec-933c-63aa6849e2a9',
            '752f5e80-a893-11ec-8dea-6b85a4226a68',
            '7b72a260-a306-11ec-a8f9-031fba7261ba',
            '922f7b20-a306-11ec-8f01-39a10cbcf6da',
            'a8ac3f80-a306-11ec-8fc9-19edd096a060',
            'bf43ea40-a306-11ec-90ad-fd485087b79b',
            'd6310840-a306-11ec-92bf-855f5c1d1f8e',
            'ecc98390-a306-11ec-97e4-7f39ebfec198',
            '4809e120-9d81-11ec-a933-95a10791ac84',
            'ef6bdfc0-9d81-11ec-9e87-2f7e266a2e1a',
            'd638a9c0-a30e-11ec-9b6b-473731636dba',
            '41530720-a309-11ec-bc91-fdd1aec4d0a1',
            '6f9fbe40-a309-11ec-aef8-d1f9c0da13c5',
            '86caa670-a309-11ec-8c73-c5176ab362d5',
            '9dfad5e0-a309-11ec-a287-470fe28b62fb',
            'ccc128f0-a309-11ec-8ec9-b343d756c96c',
            'ee950750-a30e-11ec-b173-dda947f53a46',
            'e430f7e0-a309-11ec-badf-b7995d640c47',
            'fbbcf420-a309-11ec-8ee0-45983a98137f',
            '137533b0-a30a-11ec-8c89-39f10589faf6',
            '2af91900-a30a-11ec-b7c1-af24b8477531',
            '427c5ab0-a30a-11ec-9d54-c39003985628',
            '5a104480-a30a-11ec-85ed-1b51d7484837',
            '719e1e20-a30a-11ec-b52e-7b3c29011162',
            '890b9420-a30a-11ec-ad05-97db0eb72f7f',
            'a07edf80-a30a-11ec-bc52-99ca6635a3f4',
            'b7e77030-a30a-11ec-b5f3-5b934d8b1509',
            'cf747e60-a30a-11ec-a717-5d4d64b90a2d',
            'e6fe5b10-a30a-11ec-aaa4-0f7743f92e7e',
            '9760fcd0-9d82-11ec-9289-bf3ebc1ca3a8',
            'fea68030-a30a-11ec-b588-99293f66afd6',
            '164b8300-a30b-11ec-bebd-5b7e3ba7819f',
            '2dec1980-a30b-11ec-9866-1f447aa33dc8',
            '0700b970-a30f-11ec-96ed-8303a6863f68',
            '3ef170e0-9d83-11ec-b222-e16985221661',
            'f35dc420-a6fe-11ec-83b2-95e8e6cf6cbd',
            'f5a42a10-a6fe-11ec-bccf-d566e96910a8',
            'da5ff5a0-a30c-11ec-8930-f93accf52771',
            'f27c0080-a30c-11ec-aa95-8f8764fed68a',
            '75bf06c0-a6ff-11ec-b2bc-6f59128786ab',
            '7919ad20-a6ff-11ec-8996-817614184601',
            '0a7f4c20-a30d-11ec-8ec8-cde1f265de9f',
            '7c8504f0-a6ff-11ec-b448-0336306ec676',
            '7fe82ce0-a6ff-11ec-8f08-b1e6efd26972',
            '22871320-a30d-11ec-b685-b53ba669069c',
            '3aad7260-a30d-11ec-bbf6-cb5864b83e3f',
            '52996790-a30d-11ec-a3c2-afd181fff10e',
            '1f8b87d0-a30f-11ec-94f1-19e517dfe0ed',
            '6a8b7e30-a30d-11ec-a9e1-c3e41bb11893',
            '82ac4c70-a30d-11ec-816d-4ff8142fe7f1',
            '9ac93ac0-a30d-11ec-88f4-dd4b3d7b64bc',
            'b2dd31d0-a30d-11ec-80ac-e5075393cd9d',
            'cb33b2a0-a30d-11ec-ba87-0584766bd393',
            'e3604b50-a30d-11ec-8bab-0d98180d602b',
            'fb79d6a0-a30d-11ec-b6cb-eb77708c2b3d',
            '13c3a520-a30e-11ec-a19a-f9ea2fd078d6',
            '2c05db90-a30e-11ec-be69-9171f2581a76',
            '442fe7f0-a30e-11ec-be40-09dd5e6a7d5a',
            '5c87d460-a30e-11ec-8b6b-532f99f1daba',
            '74e3f830-a30e-11ec-b880-fbe01d64f56c',
            '8d312bc0-a30e-11ec-a19c-abdd53450974',
            'a57c02f0-a30e-11ec-a20a-73e180f06d73',
            'bdd5fa20-a30e-11ec-bbe0-bda0c3a5727f',
            '38054dd0-a30f-11ec-b742-cfe1c88698d9',
            '508664a0-a30f-11ec-961c-81a0e862e81a',
            '69075600-a30f-11ec-94fd-ab0550095bac',
            '44cab0e0-98cc-11ec-be50-275bea3c5d09',
            'd641bc50-9d87-11ec-949e-db22989f8447',
            '7de622c0-9d88-11ec-b79d-c1ab73337f13',
            '817d59e0-a30f-11ec-a7a6-f1f1acb1fc98',
            '99ffc950-a30f-11ec-9016-7924faa4f94e',
            '2558c250-9d89-11ec-9cbb-2107e554bf42',
            'b27895e0-a30f-11ec-9e90-9154badd4aac',
            'caf11d90-a30f-11ec-b51c-c7a1249a77d0',
            'e3754220-a30f-11ec-8555-d3936e97c2ec',
            'fc0d83c0-a30f-11ec-92d3-210f25e59718',
            '14945810-a310-11ec-9b83-6f2512fef46f',
            '2d3b33a0-a310-11ec-b759-05f19677d151',
            '461b3100-a310-11ec-aded-0b8eae7f4297',
            '5f482800-a310-11ec-8603-45192f657ccf',
            'f7c535c0-a497-11ec-a005-0b414395c7f8',
            'f1cf8f60-a497-11ec-bc1a-5bf1861b25e3',
            'f64b9770-a497-11ec-bc2d-b3219f692374',
            'f4ccf260-a497-11ec-8ddc-79e4a0155b03',
            '780c09a0-a310-11ec-9fdb-c71c021992f9',
            'ccf36130-9d89-11ec-8eaa-7d1e5d15b90c',
            '90e51ac0-a310-11ec-a418-c3d405cc458a',
            '74bd5a40-9d8a-11ec-8e4b-d3c2f11b7f10',
            '1c5554c0-9d8b-11ec-9a03-1769ebc5ad7b',
            'c453e900-9d8b-11ec-b119-095306b6f946',
            'fc96f100-a497-11ec-b628-97e2ce5c28d0',
            'fe1c1a20-a497-11ec-90f1-fff873c20ccd',
            'ff995610-a497-11ec-be10-3f938e017390',
            '06512760-a31a-11ec-a6f0-b1f799241bdc',
            '3caff2d0-a31a-11ec-929a-a58f38677564',
            '6c680a30-9d8c-11ec-b948-f5ec2b7c914f',
            '14b3d460-9d8d-11ec-ab7f-a99b7c8c9c05',
            '58070bc0-a31a-11ec-a5f9-5df9ddf07d02',
            '435f6350-a321-11ec-87be-3bc7b47a7a00',
            '607728a0-a321-11ec-bfe2-dbdd63ec4aca',
            '7d57b6e0-a321-11ec-8fe4-7f86c3fd5ac1',
            '0d8a8b10-9d8f-11ec-986d-e9b5e415b8bc',
            'b5b8e260-9d8f-11ec-b548-45ebb482073f',
            'e05cb190-a3c3-11ec-a885-5baa01772887',
            'e1d37a60-a3c3-11ec-93d0-852e3139f4c1',
            'af43ba80-9d91-11ec-a172-9f238eaf2b4a',
            'b3739890-a48e-11ec-b398-89381358f484',
            '57974630-9d92-11ec-bb70-61188076a06e',
            'ffe6ee70-9d92-11ec-8d3b-fbd9ddf21c14',
            'a86d6f40-9d93-11ec-ab6f-9bd89ffd51b9',
            'df8af2e0-8f8f-11ec-bc45-c98834464006',
            '5033cd00-9d94-11ec-84f4-ef22db28c7de',
            'e572c080-a498-11ec-aac2-65e08b457944',
            'e87e5be0-a498-11ec-8c6f-873ebc280be6',
            'e0db4220-a498-11ec-a57f-891dcf5a0549',
            'e264ad10-a498-11ec-b6b3-ad42c179c523',
            'f8d24460-9d94-11ec-b501-fde8154372af',
            '82634c10-a4cc-11ec-ab79-7d13e5df815a',
            'a1119360-9d95-11ec-af1d-35e44a87fbd2',
            '55289cc0-a4cc-11ec-b3f8-7fbc91bbbf8c',
            'd365f610-a304-11ec-baac-797b242d3fad',
            'e9923060-a304-11ec-a54b-b90253d39589',
            'ffbd5d70-a304-11ec-b3ff-0bea913bc4a8',
            '15f35c30-a305-11ec-b268-d5838c2376a5',
            '2c37c6f0-a305-11ec-b831-53bd73e3a4ca',
            '4249c080-a305-11ec-9fb4-31371b672b3e',
            '58b923e0-a305-11ec-bc60-13003a79b68e',
            '6f32a5a0-a305-11ec-ae96-117b04ce729e',
            '85648e80-a305-11ec-9b54-b567228ff84c',
            '9baddb30-a305-11ec-996e-73bff16432ac',
            'b2141860-a305-11ec-b29d-6505dbe85399',
            'c8599e40-a305-11ec-9a1c-adb73c280fc0',
            'deb40180-a305-11ec-bc10-fff6305311cb',
            'bf5f44a0-a48d-11ec-b4bb-33bb394c07af',
            'a0983230-9e68-11ec-8357-49e0f26dd637',
            '029adb30-a498-11ec-9036-81416efbd881',
            '08d9ea90-a498-11ec-aefa-ab20be9e9692',
            '05b452b0-a498-11ec-87df-2360bf7cb2cd',
            'e6fdc060-a498-11ec-bb2b-7973e45abb76',
            'f0553480-a497-11ec-94e8-81e301236e24',
            '77ff74e0-a4cc-11ec-8bde-c1849309957c',
            '9c6bf9b0-9c96-11ec-b51c-a541d0c88be2',
            '75268a00-9e46-11ec-aaef-61e537dd9f52',
            '5719e610-92af-11ec-ad07-0324a4d617ce',
            'a0667a60-9e42-11ec-92dc-4b583a26deb6',
            '28b42630-9e55-11ec-ba5c-41d904d2b3bd',
            '29dce9a0-9e55-11ec-8fc8-87048b6e4d5b',
            '34621bd0-9e55-11ec-82a9-13baf8ed4a15',
            '992edd20-9e55-11ec-9eb7-fb78fb240c05',
            '9a5638e0-9e55-11ec-a176-4b5d4974a93c',
            '358e5580-9e55-11ec-8071-ef8a7034dc1a',
            '4e122f60-9e67-11ec-a40a-b9986ab9dffb',
            '4f3c9c10-9e67-11ec-a642-1d6ff8e4a928',
            '4cea1010-9e67-11ec-a64c-a93fc825f457',
            '50691310-9e67-11ec-87d6-c1b0fa7096e0',
            '2d5b24d0-9e55-11ec-86c2-296fd765f3bd',
            '2e8b3490-9e55-11ec-8c8e-7f0a75ec4bad',
            '8d956590-9e55-11ec-87d7-fded21842706',
            '8ebe3770-9e55-11ec-86cc-c190b7d46470',
            '5c2175d0-9e67-11ec-934b-3193393657f5',
            '576e2a00-9e67-11ec-8d07-ede689cf4231',
            '589ab880-9e67-11ec-9acc-0ddfd28dc023',
            '59c3ce40-9e67-11ec-a6b1-f1c38a9f7027',
            '5af314e0-9e67-11ec-b229-2b5a99e01e0a',
            '6e838400-9b63-11ec-b8f4-2b9f030b3bfb',
            '9353b6d0-9e67-11ec-8ea8-bf8f506984fd',
            '9480f2d0-9e67-11ec-8bc3-6bc8a477f5bf',
            '99360600-9e67-11ec-9e42-576a755028b9',
            '4e2b7780-a4cc-11ec-965f-09c7ce0e147a',
            '8f62a780-9e46-11ec-bc2e-cb3f70b2b29b',
            '9b425390-92ae-11ec-8070-41083876c5b3',
            'a672e2b0-92ae-11ec-a321-9ba5080b54f7',
            'e7554090-92ae-11ec-b49c-a36ff581f06a',
            'b27ac4b0-92ae-11ec-a7d8-63ceb0de6903',
            '795d6f90-9ca0-11ec-be53-1591ca42b5de',
            'b6325270-9ca0-11ec-a14b-0d1eb8ab409e',
            'f2e66000-9ca0-11ec-b51d-4dc8e0534c40',
            '2f0e2180-9ca1-11ec-af45-fba280e1cbc0',
            'ca6a3c80-92ae-11ec-a498-396f98e925ff',
            '08d8ee70-92af-11ec-9e00-ed6fb1c4eac2',
            '15b8f3b0-92af-11ec-9ed8-4db99d2b4e8a',
            '3f122bd0-92af-11ec-a5ba-e7f327bbb3c1',
            '64380070-9345-11ec-b6de-117426ed1acb',
            '658e6dc0-9346-11ec-b177-7dd68c2c45d5',
            '90579070-9346-11ec-9464-85b70fa71f5c',
            '9d4a0e00-9a11-11ec-b879-e193c514d54e',
            '11166e30-9347-11ec-a92e-cba2c6c24230',
            'e7359200-9347-11ec-b71d-416150c08913',
            '12102660-9348-11ec-95b0-97b7f3803256',
            '3daf8980-9348-11ec-abd6-5b92cf556923',
            '5fa2de80-9e46-11ec-8b96-e58dfc00de16',
            '91cb8ce0-9e46-11ec-9c6a-61912ab1c781',
            '884612a0-9e46-11ec-8c4c-87f34cd571cb',
            '80c396e0-9940-11ec-9056-a5700be2b4ab',
            '6e07caf0-9b89-11ec-8e01-3738478d09b4',
            '74e70300-9e55-11ec-9997-a7a75cfb200b',
            '58e3f650-a3c4-11ec-890c-7d47a969543b',
            'f9c091f0-9c59-11ec-a3a4-ab4e372578e5',
            '91113db0-9e55-11ec-8cf2-43693780d0ee',
            'c28f5170-934e-11ec-968e-3df802db1bb1',
            '3402d3f0-9e42-11ec-8838-7d66f81c19bf',
            '9ba78390-9e42-11ec-acac-abce9e2718f6',
            'c9412940-a48d-11ec-8b0d-19d8a2272901',
            '9b719e20-931e-11ec-ae90-cbd0b8d9840e',
            '53e5b880-931f-11ec-ac2e-e7c7a37fbe96',
            'b4f7d130-9e5a-11ec-b922-13b0683bba4a',
            'b61f9030-9e5a-11ec-b022-5ddb85f2f6bf',
            'efbf5470-9e5a-11ec-8a8c-d59a7d957a8c',
            '1e4a7470-9e47-11ec-a010-99da9d359faf',
            'ec311090-9e5a-11ec-8018-f7efe8c16a9e',
            'ed648660-9e5a-11ec-a258-4b68ef5b5c38',
            'b7219ca0-994d-11ec-9c62-a3945a70aea7',
            '0707b800-9b90-11ec-91ea-0dc4482e4a03',
            '0a32db90-994e-11ec-9ab6-8b00344b1b94',
            '320befb0-994e-11ec-a9a0-5171aaac5f51',
            'b5f56930-9b8f-11ec-a487-85a5167b106c',
            '0f884de0-9e69-11ec-bed6-57228cd6f7bf',
            '5ae48990-9e46-11ec-a784-99eeaa534431',
            '516cc3a0-9e69-11ec-9404-914e75b4ac9c',
            '52951e00-9e69-11ec-92f8-b15303c9e9fb',
            '53bd8190-9e69-11ec-8732-894555e833fc',
            '54f3b770-9e69-11ec-aa05-c5b5388abbc3',
            '561ef830-9e69-11ec-b49f-5943e9ef1a77',
            '57474db0-9e69-11ec-8a44-8331c8bb126d',
            '587211e0-9e69-11ec-b78f-a3cd3fd06330',
            'a39335e0-9e66-11ec-ab98-ef190e05cb57',
            'fd71f150-a495-11ec-94b8-59d5d7d9e168',
            'fee96280-a495-11ec-aebf-27b883ebf07a',
            'f73549e0-a495-11ec-a1f4-e5ba5e083a4a',
            '02031b10-a496-11ec-867a-1132d84fe10b',
            'b8f14600-9e45-11ec-8d65-0700e05159ed',
            'a3d11790-9e67-11ec-af10-8b0dedf7a321',
            'a5005190-9e67-11ec-8a47-4b7c4a1b2743',
            'e458c220-9e45-11ec-9548-dfbb494d8a90',
            '63197ed0-9e42-11ec-9402-7dbc447479fa',
            'c35f7c70-9e55-11ec-b1eb-59865e5073f0',
            'c48b90f0-9e55-11ec-a03d-2182ccd2f270',
            'c5b6f450-9e55-11ec-b92b-a3b287dbef75',
            'c6dfaee0-9e55-11ec-b42a-e3cdb82510c8',
            'c80b51e0-9e55-11ec-ba45-33116784dbaf',
            'c93624c0-9e55-11ec-b49d-dd45f501aeaf',
            '44c60af0-9e55-11ec-8499-2fe1332f1f31',
            '786a73a0-9e55-11ec-b478-85fca51a9871',
            '049ba9b0-9c52-11ec-97be-1b71164c5e49',
            '173a9f60-9e47-11ec-b2fd-ffa02ec2d0e7',
            'b833b920-9e68-11ec-8382-976a65a17bdb',
            'bbc137b0-9e68-11ec-8bf9-3d1074d8ec51',
            '3cccb240-9e46-11ec-bac9-ef7e62501310',
            '3b45f620-9c53-11ec-85b2-517b2a8e9a98',
            '10351e30-9e47-11ec-8be2-07fa018f4f57',
            'dcde0160-9b3c-11ec-b083-9d1ba0253c63',
            '0fa2cc30-9b3d-11ec-9360-95bd06b43f6b',
            '2deab8b0-9c4d-11ec-a652-5522fef82d02',
            '13200a20-9e46-11ec-9d13-8feb56f77085',
            '614bafa0-9c4d-11ec-a407-79a260d6dee1',
            'f8bc03c0-9e45-11ec-894b-09d4570185d4',
            'd9675a20-9e68-11ec-a1fc-81442fe3f156',
            'df431470-9e68-11ec-93bb-5b97e3c1a327',
            'e06b33a0-9e68-11ec-a8b4-e197bde2b1bb',
            'a13fe3b0-9e66-11ec-83e2-cbc3b812d77a',
            '9620ec60-934e-11ec-9d01-17531668ee47',
            '6d88bbb0-9e65-11ec-8432-49db6d2d33ff',
            'e53930d0-9c56-11ec-8bc0-31d71a236374',
            '2e144e20-9e66-11ec-bd30-19310bf874d6',
            'f4026350-9e45-11ec-9009-e15fbdf6a890',
            '61c39ad0-9e66-11ec-b56e-13913f0154e5',
            'f17268a0-9e46-11ec-8326-af0828c6e53a',
            'a2962a20-9e55-11ec-b2a3-d93f8ea41766',
            'a3c1f230-9e55-11ec-8fc3-9127409a665b',
            '6a6a5c40-9e46-11ec-a2b3-8974ab8ad0d2',
            '6634cb70-9c90-11ec-8a87-f3eb43cc80a6',
            'b6806950-9e45-11ec-9e8e-778c4bfd0ebd',
            '45efc6c0-9e55-11ec-ba87-8f9ea05081a5',
            '65707610-9e42-11ec-b22e-2b63c27bdd50',
            '8fbd9210-9e42-11ec-bdc7-adb8e602416f',
            '439967b0-9e55-11ec-90e2-8dc186d159c0',
            'bc5e6000-98cc-11ec-9b4a-29fee832b611',
            '773c4b80-9e55-11ec-b0e3-a99f58b696bb',
            '41fb5340-9e66-11ec-9ac4-0db14c8aa79b',
            '10ee26f0-9b3f-11ec-a19b-37ba1b49fa6b',
            '1413a280-98c7-11ec-bcad-5da9a81432d7',
            '2f081840-98c7-11ec-a257-a70a7140d97c',
            '28f10460-9e47-11ec-9735-65bad1ab3f41',
            '93477090-9c8e-11ec-bb5d-c5556dc77277',
            'fb68e360-9c4d-11ec-8343-63d2e1068203',
            '5e7336b0-9e67-11ec-93cb-edec926a6e80',
            '61f9a0f0-9e67-11ec-ac3b-e5a11bf8daa3',
            '63230140-9e67-11ec-8a8b-1386daf66aaa',
            '64536930-9e67-11ec-a9d7-cbdb0d61a5ec',
            '65824730-9e67-11ec-870f-6f3c8131df5b',
            '66ae8540-9e67-11ec-8473-2fedfada33a4',
            '7832d400-9950-11ec-848f-0df7d3d2892d',
            'cce45d40-9950-11ec-ae9c-41b4de90757c',
            '55d4db90-9c85-11ec-a2cf-a796e4e41223',
            '25ef4b20-a498-11ec-963b-15938ae82c3e',
            '22e41fc0-a498-11ec-93fd-7b6bd8921753',
            '245d1c90-a498-11ec-a79c-f59dab4efeb5',
            '9ffec750-9c52-11ec-b1d6-e93812e685c9',
            '28d92bd0-a498-11ec-a37c-61385d60cd7e',
            'fe854110-9b93-11ec-9271-71588d1a9dad',
            '0b4db7e0-9956-11ec-9b4e-6f1b853adaf7',
            '7b184e40-a49a-11ec-9e16-257673c61076',
            '112a60c0-998f-11ec-9ab6-e7cdc4155fcb',
            'a3e6ac70-9bb3-11ec-917d-1f133e4db2c6',
            'd1ec3380-a48d-11ec-b2c9-d7b4710cc0bc',
            'e119f8e0-998e-11ec-9d93-47fcaed746aa',
            '7fd5e900-998e-11ec-a179-81fc02cb1885',
            'b09be820-998e-11ec-9a7f-bb9b8eadec5a',
            'bfb07be0-9bb3-11ec-8258-3fc9e43e60c0',
            'db9a0a60-9bb3-11ec-94f0-2996155ebd6c',
            '2d5d7060-a498-11ec-b15c-d557fe76dbff',
            '547fdbf0-92ae-11ec-a48b-3906cbe6b1f2',
            'c92b24a0-92ad-11ec-b32e-85b81b020fcf',
            '68f05cb0-92ae-11ec-ade4-e5d33e12b8e0',
            'a0119390-9e66-11ec-9003-8daf136da534',
            '48e82fe0-9e46-11ec-9f04-85ce4c0e2e1c',
            '7dbe6220-a4cc-11ec-86ef-55dba223d222',
            'eaf35110-9b66-11ec-9b66-2b3b63d21fee',
            '1458e160-9e5b-11ec-97cb-f5574580954a',
            '1581dd40-9e5b-11ec-8f06-d9903e01bba1',
            '16aa6070-9e5b-11ec-abfe-496a4cc28247',
            '17d67cc0-9e5b-11ec-93eb-d580d0d55d27',
            '84b2f650-9e46-11ec-b6b6-57e535e8b20f',
            '47b6eb40-9e46-11ec-86af-3d73bf901ab5',
            '1d1cd4f0-9e47-11ec-85c0-796d482b10c8',
            '8711f070-9e46-11ec-8f60-6f7a70cb5bb9',
            '726e68e0-9e69-11ec-84a3-132b05346724',
            '7397a990-9e69-11ec-abf9-b57965405e09',
            '86302be0-9e66-11ec-b6f5-3bf7053a135c',
            '6c956260-9e69-11ec-aadf-5f4f3c1bc230',
            '6dc408e0-9e69-11ec-a137-6914215ae6ed',
            '31596670-9c4f-11ec-b9d2-430b5a8ea27f',
            'f042fc60-9e46-11ec-be9d-bdbadf4412d1',
            'b45383b0-9e67-11ec-b812-3d2b7f61b438',
            'b582a6e0-9e67-11ec-b79c-a18aed0470f4',
            'dbd4c3a0-9cb2-11ec-afe5-aba610c478b9',
            '39bce710-9e66-11ec-bac1-759953f518c9',
            'af9754b0-9e67-11ec-94cb-2d5dae386580',
            'b0c30fc0-9e67-11ec-984a-a321dacbb5b2',
            '842ff020-9e55-11ec-a8f7-0f17c666d3c7',
            '855eac20-9e55-11ec-a1a1-2de611117374',
            '803dfaa0-9e66-11ec-b481-cfcf019d0a71',
            'b7097ce0-9e68-11ec-b615-e9ffbf5ec7e2',
            '7bc00dc0-9e42-11ec-b18a-4387a707fa37',
            '24b66da0-9e66-11ec-90b3-25d452edf931',
            'a1737be0-9e67-11ec-a5ce-d9fc95439a66',
            'a2a2b100-9e67-11ec-bb1d-d54508074ac7',
            '30a9b3c0-9b67-11ec-8294-89139127bb8d',
            'be224780-9e68-11ec-9179-d7641d87a060',
            '6c6c2620-aaee-11ec-ac22-e9a08072baa7',
            'b91260c0-9b2e-11ec-9231-8f4fbcf387d1',
            'e330d940-9b2e-11ec-b524-15f757d81c97',
            '0d91b970-9b2f-11ec-bab0-ade2eeaf1b2e',
            'a6179080-9e55-11ec-80f6-253aace42977',
            'd3ce60e0-9c52-11ec-8264-8b4dba245495',
            'a3d777d0-9b30-11ec-9089-af9f8d9d5850',
            'cba129d0-9c4f-11ec-88a5-2727310a09f8',
            'c8116760-9c4d-11ec-b4c3-fd7805328ae5',
            'ec032e50-9f32-11ec-9518-c1bad41ebeb0',
            'a2742110-9f33-11ec-a8d1-5971bdfaf998',
            '7f1741e0-9110-11ec-908e-632c97c7f4e9',
            '1fd8b670-9e6a-11ec-9f10-8324aed21d98',
            '25a08dd0-9e6a-11ec-bd48-997a78b136fd',
            '20fee9e0-9e6a-11ec-82c7-b1bf97a65db5',
            '222650d0-9e6a-11ec-91ac-334b82338447',
            '234f6f00-9e6a-11ec-bce1-7362a9a09d5f',
            '24770710-9e6a-11ec-862f-a9d974d4a6df',
            '26d28fc0-9e6a-11ec-a530-fb6fa0b9fe48',
            '28030f30-9e6a-11ec-83eb-237347c1cf5c',
            '0a53cd00-9e47-11ec-a041-1d3f1cdfbdc8',
            'aecddab0-9110-11ec-bbb6-d70d29987b35',
            '5049b1f0-9997-11ec-9d84-55a5ccd913bf',
            '520db510-931e-11ec-9bd1-5dd10d54db01',
            'e5401f50-931e-11ec-ada9-2f3d83c4dc80',
            '599ccc20-9e69-11ec-ad31-9b262027bfbd',
            '5acdfae0-9e69-11ec-9b6e-d5e907068c28',
            '5bfac270-9e69-11ec-bb26-cfa34c6083ed',
            '65e250e0-8ea9-11ec-8771-adfa2e828ba1',
            '5d344b30-9e69-11ec-9c0c-930673c6d520',
            '5e602340-9e69-11ec-bd31-2f710b3c3171',
            '60bdbf40-9e69-11ec-b599-f7eadd1e7a7b',
            '6316b870-9e69-11ec-ac48-87b70b20653f',
            '32566680-92af-11ec-b91a-cfa21b6b2adc',
            '4b861940-92af-11ec-8320-cdbe164a9ddd',
            '10e5ec60-9f35-11ec-8542-838161c55bcf',
            'd82cf360-9e68-11ec-b439-0589c7a4c118',
            '4b5000d0-9bb4-11ec-a28b-75bf645944ad',
            'a5c2ed70-9f38-11ec-bc7f-298cbc5d67ef',
            '61ee3de0-9f3e-11ec-8247-e7652f551d18',
            '1c9fe4a0-9f3f-11ec-88e3-57d9c84757c9',
            'de1917d0-9e68-11ec-9eed-2f3a5caabced',
            '7a40de20-9998-11ec-b325-fd93fd3e7aff',
            '17373aa0-9998-11ec-98bf-1bb82f28f1c2',
            '3e4ddd60-9bb8-11ec-8c84-bfab7b8a9167',
            '983a5110-9c4f-11ec-99f1-8bdfd68b4625',
            'acfdc160-a48d-11ec-a92a-2534d46e00e7',
            'eb8e1f30-a498-11ec-b4c6-237a8f13cca3',
            'b3bf2680-9997-11ec-89a8-3ff37da89ff7',
            '8fdaf9b0-92ae-11ec-898e-87a993474aa2',
            'bb75b750-9346-11ec-ab22-6fffd20d62fa',
            '06d2fc50-a3c4-11ec-8480-3be79c4e50e2',
            'cb16c8f0-9e68-11ec-901b-57eed524e1c7',
            'b4b19b20-a95a-11ec-9352-7f8bfadaed7d',
            'f95f1d60-a497-11ec-9583-613340cec98c',
            '0ad32710-9e69-11ec-97b4-7fd7e2dbcf69',
            '52f50180-a3c4-11ec-8d24-e102b5c788a4',
            'd9d340b0-90c7-11ec-ab00-5d817397946f',
            '0e5dec60-9e69-11ec-9dba-83dbf243574a',
            'e5581290-90c1-11ec-a998-5b31dde024de',
            '10b74930-9e69-11ec-808d-1fed5cc0653e',
            'b2e94730-8f8f-11ec-94ec-bba96d94d309',
            '7037a2a0-aaee-11ec-84b3-9fa3f372cf8e',
            'b79c5be0-a48d-11ec-bc6c-715d52411084',
            'cc3b6050-9111-11ec-b16e-957d05674b6f',
            '73fa8920-aaee-11ec-b903-872e08f9dacf',
            'd85358c0-9f5a-11ec-a02e-6f555b3f0858',
            '74e239c0-a49a-11ec-a0ca-f9226c76b2d8',
            '59d38540-9f41-11ec-90e4-11e79ed80323',
            '1ca8e9a0-9f42-11ec-87f5-579861d23a34',
            'f0d4a6e0-910f-11ec-8894-ddf57b237407',
            '98770e90-9f40-11ec-96fc-314aab28fe92',
            '4368b020-90bb-11ec-8c74-6777b87d8e7e',
            'b0122e40-9f03-11ec-bf41-3fbec8604f09',
            '67a70c80-9f04-11ec-ac8d-ef0a02afc950',
            '1ddcaa90-9f05-11ec-8710-2551f06e3beb',
            'd4738770-9f05-11ec-af1a-e745369f1636',
            'c7f7f6d0-9f30-11ec-a2d3-ff21533d3e47',
            '7ee15490-9f31-11ec-abab-33b91987a27b',
            '3595d550-9f32-11ec-8bba-1942f68b02b7',
            '4fada960-9110-11ec-810e-63b909d20ca7',
            '11497a80-9f30-11ec-9326-633c0449f500',
            '7e807100-9f36-11ec-a126-237a218272fb',
            '35707150-9f37-11ec-856d-7ffbb5a5dfec',
            'ede2a300-9f37-11ec-b3f7-5fdc3550424f',
            '59659660-9f34-11ec-a49c-f709624778e1',
            '0693e8d0-9f5a-11ec-a49c-cde57b137d17',
            'a94722e0-9f5b-11ec-8e71-b9e6b2660fce',
            '7a134b70-9f5c-11ec-8e05-85a3bb3725ee',
            'e52087f0-9f5e-11ec-af9c-e5fc4c57eaec',
            '4cdb35b0-9f5d-11ec-ae49-575378b7be8b',
            '1a68f9f0-9f5e-11ec-842b-4b260449c06f',
            'f5f85990-90bb-11ec-9552-b71891f6d3fe',
            '77cb6850-aaee-11ec-9e9c-2bc46ebefa7f',
            'e3668630-9f0f-11ec-bf86-bd8a0be08fd9',
            '9b69c3e0-9f10-11ec-af14-51eed6f988b5',
            '0b2e8e60-9f12-11ec-9b83-73d3da73e7a5',
            '19c2ee00-90bc-11ec-99da-5df6c27eb903',
            '3d88d0c0-9111-11ec-a03c-5b76b23359f3',
            'ef05a5d0-934e-11ec-af6e-dba08d1eb893',
            '1a0b9e20-8f90-11ec-ab06-9740f8a50873',
            '16db87a0-9e51-11ec-8d05-113b8a1a6ba9',
            'da3ce2c0-92ae-11ec-be9d-0742c225bab2',
            '627f5940-92af-11ec-921a-ade336a6ccc0',
            '84ea03e0-92ae-11ec-aeb6-9bd583b66cc3',
            'f8260a50-92ae-11ec-8724-b91aa2d006b4',
            'be3f62c0-92ae-11ec-ad97-878b45e3b143',
            '21cd2170-92af-11ec-afba-ebe24a22e64a',
            '3bd110e0-9347-11ec-a0f7-2988d8b5db49',
            '6694ba90-9347-11ec-82cc-7f6ad5f9be65',
            '9157fb10-9347-11ec-85a0-e1b779230ceb',
            'bc6f69a0-9347-11ec-933c-89cef155423a',
            '73b89a30-9e55-11ec-a8f4-913a93854b05',
            '14e71a40-9b07-11ec-8c6b-4b54e70c5429',
            'e2a4b770-a497-11ec-b759-6b9342f260e9',
            'de3238b0-a497-11ec-b16c-9904c7f1f812',
            'e5a5f990-a497-11ec-ab52-ddfb943aa4ea',
            'ed8aef10-9171-11ec-beb7-b9be14db6acb',
            '393a9480-9e46-11ec-a317-5f64db9a0bea',
            '7eafa170-9e46-11ec-8741-2beced1caf24',
            'f34cae20-a497-11ec-bd75-631fbc02d8c4',
            '33a432e0-90c4-11ec-a8c8-9ba2820a8a4a',
            'f779ab00-9bb3-11ec-9102-f725129f2633',
            '4f0db310-9e69-11ec-a342-7116f394b36c',
            'e12f69f0-a497-11ec-89c2-2389c147f069',
            '19bc59a0-9c57-11ec-b2f7-ef8f5a95043d',
            '5f923e40-9e69-11ec-b75c-c5228cdba92f',
            '3fea6bc0-a4d4-11ec-b99b-8b726f11660c',
            '61ec1270-9e69-11ec-96a2-c5b85b715e09',
            '37bb6810-92ae-11ec-b108-d10c679817fe',
            '74872ae0-92ae-11ec-84b4-f3330cc5d012',
            'a58f1030-92ad-11ec-af7c-afe2b3f7c0c6',
            'b1f4c570-92ad-11ec-8e35-33bcb2a9fee9',
            'eaf00c40-92ad-11ec-8712-db6663d47cc1',
            'f63bb6d0-92ad-11ec-89d5-b70428dc83f3',
            '025a5040-92ae-11ec-8a35-6d682de84761',
            '7814efd0-90c2-11ec-8a5f-930afecc7f3e',
            '9f6e34c0-90c3-11ec-abbc-ffcb6cafbec3',
            '180a8020-9e51-11ec-88a5-51dd5164b2fb',
            '68aaafd0-9e65-11ec-ab55-0f678a6c4028',
            '74c19630-9e69-11ec-b669-f1122d5448b7',
            '92467740-9460-11ec-a68f-6fec387a1bfe',
            'a879be20-9460-11ec-915a-71775d7cc5e7',
            'beb21f60-9460-11ec-9e1f-e59484e1939c',
            'd41a6550-9460-11ec-937e-9f00c825d36f',
            'ea731250-9460-11ec-bf0c-43ede161459e',
            '0140b850-9461-11ec-ad17-87c9cc17df83',
            '9731cae0-9e68-11ec-9488-49cd22e28d72',
            '9be90e00-9e68-11ec-bb14-93dc13dab560',
            'c3975040-a48d-11ec-98f5-4f546d4b5902',
            '9d1757f0-9e68-11ec-92d0-7d3ef823958e',
            '9e40dbe0-9e68-11ec-bacf-39a49ee4e6c1',
            '6cf283a0-9111-11ec-bdd8-3989111b0b2d',
            '9caf26e0-9111-11ec-95fa-5de024901f7e',
            '85268820-90bc-11ec-85c9-f16fb13f416f',
            '46b09e30-9e66-11ec-a6f0-3d5efb8673ac',
            '81673a60-9e66-11ec-95bc-7b10face52b8',
            '6143c880-aaee-11ec-b3a3-bbeae5070771',
            '8c14d3f0-9e66-11ec-a8f0-1b6d85d8be97',
            '8d436780-9e66-11ec-864b-ed5e4ae3a7cc',
            'b7937e60-a95a-11ec-b826-a180e0b89f0d',
            '50aa5c50-9e54-11ec-adb8-636162f7c3e3',
            '58f48340-9e54-11ec-812d-b9289d23c3d4',
            '5a27ded0-9e54-11ec-8f9f-cbefe4467a70',
            '5b52b7f0-9e54-11ec-9ae7-5384e1566e85',
            '68a69b70-aaee-11ec-bc09-7f7a1f7a7cd7',
            'a2697990-9e66-11ec-9a3d-3955988f6473',
            'b3e4b8d0-aaee-11ec-ba69-3f0b2695ca19',
            '201f3c60-9110-11ec-9045-874281585a68',
            '8550d0f0-a4cc-11ec-980e-b7720e596340',
            'de4b2fb0-9110-11ec-bf31-f3294c6d2bc9',
            'f3304620-a498-11ec-bf16-afbdfb7b1eba',
            'ad3f7aa0-9e67-11ec-bb87-a558e9da57cb',
            'b7ef16c0-9e67-11ec-8556-297ad41b7b48',
            'e3f15e40-a498-11ec-a4b0-ffb3add4d21f',
            'fbe0bb70-a495-11ec-b60f-55070a668f98',
            '00799e80-a496-11ec-94c1-e354b75cd5b8',
            '62f9d2d0-910f-11ec-85ed-9f5f2d9db650',
            '5b1fe1d0-9bb8-11ec-afdc-d1ed8123162b',
            'e7fcefe0-9bb7-11ec-9da0-232ae5c389ce',
            'f8c5c660-a495-11ec-a5c1-f1daa7ccc547',
            'fa539c00-a495-11ec-90c6-150d6d6ac9dc',
            '1bf30cd0-9e47-11ec-ab0d-8903f125e008',
            '7e1a5ac0-9c8a-11ec-b3b1-f55b38f07be0',
            '4a907860-9e55-11ec-9023-cf0b91bf6740',
            '7e52b690-9e55-11ec-a124-db5c5ee60c0d',
            'ea06f2b0-a498-11ec-b2db-15f648d0ca88',
            'a5d8cbc0-9965-11ec-ae84-5711f1b5d4b5',
            '542aa4b0-9e54-11ec-a119-cd93028e7d37',
            '5c7a8830-9e54-11ec-8288-e11136e87249',
            '2a5e3c70-a498-11ec-8dd7-99ee9ae835c7',
            '57b0d590-9e54-11ec-991a-919dde26791e',
            '5da35240-9e54-11ec-811f-91d105409a05',
            '61202ef0-9e54-11ec-afb2-8d8f93178a5c',
            '6248c440-9e54-11ec-ae02-37a96ded2d6e',
            '980bd330-9e67-11ec-88d6-0d3913c58ad3',
            'aadfff00-9e67-11ec-888a-4b4f54999da0',
            'ac166d20-9e67-11ec-a3b4-0b26949ed634',
            'e07274a0-9e5c-11ec-8989-1762c5e77c0e',
            'e19dd680-9e5c-11ec-a1c9-73d404fc14fc',
            '82b15b40-9c57-11ec-95fa-cba034f9595b',
            '2ef50ac0-9c4e-11ec-aa03-2b63b39dbdfa',
            '5e6ba120-9e46-11ec-a695-431338a791fe',
            '6eede170-9e69-11ec-b8b2-0587b588b43b',
            '32ea5600-9b57-11ec-a5fb-9fa410a00cab',
            'da41a980-a1bc-11ec-84c0-b93af0a92eea',
            '9d556130-9cb2-11ec-9155-fd1b10957b7f',
            'f01195a0-a498-11ec-855b-f5e14462e39c',
            'f1a27240-a498-11ec-8b89-6b2531803b42',
            '5f743130-9cb2-11ec-a870-772ad6d4d0fb',
            '3c90f3a0-9ca0-11ec-86c8-0f70e1255345',
            '95b7c8e0-9e55-11ec-ba64-215840d6c1f0',
            '1a8862a0-9b90-11ec-9ab2-832e3ecb52fd',
            'dfba3a40-9b8f-11ec-a564-49d374a4d782',
            'f3357d50-9b8f-11ec-8753-cbf72c112849',
            '5fae9e90-994e-11ec-97aa-d5504e66aee6',
            '8a6376c0-994e-11ec-8aa7-6537018ce52b',
            '388352c0-9c52-11ec-ae0d-e1d472242bf2',
            '8fe6eeb0-9e55-11ec-b366-93deabc0eb88',
            '23867fd0-9e66-11ec-bfb7-9d1cf18ad6f4',
            '94a768f0-9c4d-11ec-8402-8bb87884473d',
            '77ff05d0-a49a-11ec-899d-c7d9ec6c5786',
            '798771d0-a49a-11ec-a398-532b9e8db829',
            '76a11d00-931e-11ec-af37-ef5fb92be243',
            '7910fc40-931f-11ec-b5f4-1d33ec2d86b8',
            'c0993d30-931e-11ec-9d14-bbbd7242d3e8',
            'c4c29900-9c59-11ec-b410-b57e5b9e9b55',
            '6edf1d70-9e67-11ec-b27c-89516958e965',
            'dfb0afb0-a497-11ec-b88b-5d9c3dc19f3b',
            '422a4a30-a2e3-11ec-8f4d-9bf0bdb1207b',
            '5e8be990-a2e3-11ec-8b26-2dff467f3e2d',
            '26d22c90-a2e4-11ec-bb5e-4f61b510aea7',
            '30ef99d0-a2e4-11ec-b252-5925ef30ef29',
            '466f9300-9e46-11ec-a15d-638caafe1fb8',
            '4b6311e0-9e66-11ec-9d88-4f78c193b655',
            '4c8bd520-9e66-11ec-9f88-35de035e4dac',
            '27c27ed0-9e47-11ec-8d6e-e11d00b782cc',
            '9ddf8270-9e55-11ec-a989-b3b26af05ee4',
            '01188710-a2ea-11ec-a5d8-7db43a12a0b8',
            'f3dbb380-a2e9-11ec-9a54-efe174d2cb65',
            '0e767ce0-a2ea-11ec-9a69-a3e5cb6f9496',
            '1b949770-a2ea-11ec-aa2a-8378c0598a29',
            '67554330-9bb4-11ec-b129-e519afb990c7',
            '1d2f92f0-998e-11ec-83cc-c1c60f46c15b',
            '1352a640-9bb4-11ec-ba4e-c79c4111f1f4',
            '2f4a0f90-9bb4-11ec-9ab1-578e6847cf68',
            '28a8a4a0-a2ea-11ec-8e22-df521f3579b4',
            'a215d550-a48d-11ec-8775-ad6398c7e7bb',
            'a418c5e0-9e68-11ec-93b6-9de401c50dd2',
            'a1c3e270-9e68-11ec-ad99-531266df63f9',
            'a7a3fb90-9e68-11ec-8bdd-47f4ff86348d',
            'a8d0bca0-9e68-11ec-bc18-3707c52584d4',
            'ab35a2c0-9e68-11ec-bc4c-3799aa25c0ec',
            'b5726540-9b63-11ec-90fe-731288313c04',
            '717dd320-9e46-11ec-9d69-03dda7d904a8',
            '612b7810-9c96-11ec-a938-b162c6630fc2',
            'd7711be0-9c96-11ec-99c5-030963a119d0',
            'b7aab000-9e45-11ec-af9e-bfe77c9fa078',
            '5d4a86b0-9e67-11ec-890f-bdb276ebac78',
            'ed0b8580-a498-11ec-8c36-ef4f9805d1b4',
            '71ce1d60-a49a-11ec-b972-2d29bf387df6',
            'be376400-9c58-11ec-b5a9-b18a30be6c8e',
            'fb0c66a0-a497-11ec-b660-73c3c247ab7d',
            '50ca2940-a4cc-11ec-96a6-ab6d4d732587',
            '89b5de30-9c58-11ec-82f3-0f7bef689ea3',
            'c7d5f5b0-a48d-11ec-a044-a9303572a207',
            '6f65b260-9b71-11ec-a963-b5afbb261195',
            '8f9eb3d0-9e66-11ec-960b-215af07d8757',
            '0ad8ea60-9c8a-11ec-8bab-2d937fef5587',
            '778ba890-9e46-11ec-a5d9-ed9e74afc359',
            '27673c10-a498-11ec-bc4f-b7ff9bd88bc6',
            '09e73040-931f-11ec-8fb6-2dd29c9950e2',
            '2f0937d0-931f-11ec-9e23-ed8ddd469a2f',
            '2d6f54d0-931e-11ec-9c9c-45e44a8afec0',
            'd10c6ff0-9c89-11ec-af50-fff24f810398',
            'b3288790-9e67-11ec-a372-438577ddfd7a',
            '33661fb0-a2fa-11ec-ab0d-6d877036fccc',
            'df5c0310-9e50-11ec-ada8-abb9f7506da2',
            '46749ca0-a2fa-11ec-8468-b58f75776495',
            '596797f0-a2fa-11ec-8484-d325353576e5',
            '6c6644c0-a2fa-11ec-a8db-d712b2a87cbb',
            '7f5ec3a0-a2fa-11ec-9dca-310a36d88c7e',
            '9276a120-a2fa-11ec-be0b-25d34b766ab9',
            'a568fdb0-a2fa-11ec-9b09-b193281e58a1',
            'a1935440-9e42-11ec-975d-b7a8bb939f67',
            '98e72150-9e46-11ec-817e-a7183a8a5016',
            '9986b6b0-9e68-11ec-9cbd-81f92b9218d3',
            '9ab362b0-9e68-11ec-a08a-f502f00b3723',
            '5ecc3b10-9e54-11ec-9f7a-83a2c9942d6d',
            '5ff3b8d0-9e54-11ec-8d62-e7950da0702a',
            '426b7380-9b3d-11ec-affa-f31c8ab15135',
            '533e04d0-98c7-11ec-acf8-5b261429b720',
            '9a7e8960-9e42-11ec-89fb-a5c60de504c5',
            '0af83990-9c54-11ec-973d-29088fbf8365',
            '956ee9c0-9e58-11ec-ad59-dfba6b1d4043',
            '92fc07d0-9e46-11ec-bba2-ebd68352015d',
            '4ddfafd0-8f28-11ec-af39-c3bb2d44fe2e',
            '94b40290-9bb8-11ec-8390-c394e8534e53',
            'dd9b6ce0-9998-11ec-9932-cfe61de2cba4',
            '5f3b40a0-9b3f-11ec-8ff4-2b1dc8fd62e1',
            '49047230-9998-11ec-92b8-bbd502ec7ab9',
            '4e8ae730-9b91-11ec-9bca-f5a6d81f3167',
            'ba20ccb0-9e45-11ec-8590-dfed521a39fd',
            '4408b6e0-9e46-11ec-93d9-b7472bd7dbeb',
            '2bbfa400-9e66-11ec-b070-3b4246ee3b6f',
            '14e41e70-9e47-11ec-8be3-3fcb0f1078b5',
            '4877ae10-a3c4-11ec-bd8f-b7e99821897e',
            '9f0df900-9e55-11ec-be1b-b1df0955dd5b',
            'cd6120d0-8f28-11ec-844e-e348b1c8fc65',
            '5d8ccb80-9c89-11ec-845f-5d2c739109db',
            '97d0ab10-9e66-11ec-a1a5-2be684f92516',
            '62142f20-9e46-11ec-a598-55506d54c4ab',
            'bb5cf480-9e45-11ec-8778-2b74f19f4bb6',
            'ee9672c0-a498-11ec-a966-2fa7b84df81a',
            'bef3edf0-9e45-11ec-8b3b-db9528266d41',
            'ddc22d00-a498-11ec-bcda-c7053419a4e5',
            '84132190-9e42-11ec-9ed6-e9af237adbd8',
            '9a1237a0-9e46-11ec-bbd6-496254a8a698',
            '8f5d5360-9345-11ec-8113-313f7e40937a',
            '0fe3c060-9346-11ec-8edc-c51521edf2bd',
            'e647b000-9346-11ec-a8fd-b72d11a2a563',
            'ba6f4cb0-9345-11ec-9619-c504c36d4557',
            'e55625f0-9345-11ec-940d-1d30696cfd10',
            '3aeb02e0-9346-11ec-999b-49f6cadb6426',
            'e422f500-a497-11ec-b6d0-41cdcdf551bd',
            '10e8f330-a363-11ec-bb74-e90b637f0564',
            '95622940-9e46-11ec-be5d-d97a51581f88',
            '6c3b8ec0-9c52-11ec-8097-17f31db32a96',
            '7d8596e0-9e46-11ec-bd76-1f481e9f09cb',
            'f51a6970-9e46-11ec-9146-4f0dddb87c5a',
            'a03b1740-9e55-11ec-bacc-1dc46e71966e',
            '8e4e7b10-9b30-11ec-8028-5daa13c6cef2',
            '25e6c440-9e66-11ec-adb8-9905f3d3802d',
            '27119ea0-9e66-11ec-afe9-d778a41b0fb8',
            'f6490a50-9e46-11ec-b696-d931e244c795',
            '92f92ec0-9c4b-11ec-ade0-c984d1647c59',
            'c635d990-9c4b-11ec-b62d-2fbcc33ab070',
            '704b3990-9e46-11ec-95ce-0ff8696566e9',
            '22cf76b0-9e55-11ec-ae36-bdf4f50ef997',
            '81d348c0-9e55-11ec-a839-d3ac39518387',
            '83062b20-9e55-11ec-acb8-6ff95c3b978e',
            'd3e8fdc0-92ad-11ec-9b81-bda584005cfa',
            '0e6a5760-92ae-11ec-9351-3715c9f53373',
            'df53b3f0-92ad-11ec-985e-7f14ab758386',
            '1ac1bbc0-9e47-11ec-99ce-51d0c67d6ba8',
            '0119f290-a498-11ec-bd8f-a1bbb4bab558',
            '6808c860-9e46-11ec-9b3d-7d35da1ecb59',
            'a0c32490-9c90-11ec-bd53-939695994866',
            'b171ec00-9e5a-11ec-bb43-dfc262ffc4ee',
            'b29bd5c0-9e5a-11ec-9a61-5fe43de6bf45',
            'b3d00f60-9e5a-11ec-99d7-5b66841406cc',
            '0a29dc60-995b-11ec-a0cb-796e11159fee',
            'ca68b7b0-9e55-11ec-986c-894ac14918c3',
            '074fe3b0-a498-11ec-95db-bd36cbbad96b',
            '0424a8f0-a498-11ec-be2d-6fe2b62b248a',
            'de2f51e0-9e50-11ec-85d1-45fd33ef1d1a',
            '8c03c510-9b73-11ec-b84e-4d5451a7e65b',
            '95076cd0-9b73-11ec-b3c6-edb19ac91644',
            '82bce960-9b73-11ec-aaf4-ffee4ba98156',
            '0de3cbb0-9b67-11ec-b983-87566a08f248',
            '62586940-9c4e-11ec-9d5e-7f678d24e54b',
            'daa94a40-9e50-11ec-b380-112ef87b4e95',
            '8c875990-9e42-11ec-8449-d70430a77b60',
            'a2bf8f00-9e42-11ec-9b5e-5d48230e7cc6',
            'd97ccc80-9e50-11ec-a27c-f9e36650dc31',
            'bd070120-92ad-11ec-a415-2155e4f06e12',
            '1999b1f0-92ae-11ec-a5f4-87bfa6ce30da',
            '2a179830-92ae-11ec-95f8-9d96cfe26d3b',
            '44ae0790-92ae-11ec-a98d-1f16ff40dfad',
            'ffbcfb30-9e46-11ec-885d-05918b16d9e2',
            'e4239bc0-9cb1-11ec-aa63-ad06b6c82ff2',
            '21c68e00-9cb2-11ec-9e38-d5b07c052773',
            '2bd834a0-a498-11ec-ae09-b5d9633ddc5e',
            'a7127da0-90c5-11ec-8584-05c854e35388',
            '3ec3d7c0-9c54-11ec-9ca1-19d8345402b7',
            'bcd1c7f0-9d8d-11ec-ac77-130afe6e3fa1',
            '65400370-9d8e-11ec-8834-619ca46c6b4f',
            '5d84b730-9d90-11ec-bd1e-91593a722a83',
            '065483d0-9d91-11ec-a3ee-d7ab383d6521',
            'd098a2c0-9c51-11ec-9f31-d9d0a4650648',
            'b1cc05e0-9e45-11ec-8180-6ba02ec9d1d2',
            '9e050bb0-9e42-11ec-b488-1db69feeb1e0',
            '66b77600-9c50-11ec-90c3-4b3ddd58cd71',
            'df51b650-a498-11ec-a263-63caa39d0f98',
            '84f024e0-9e66-11ec-b267-e315cc3b335d',
            'ff3bb2e0-9c4f-11ec-aed8-dd6ce2b62090',
            'b61ef400-a95a-11ec-a529-9f3d141c73be',
            'd7b24ea0-9f3f-11ec-a06b-0f6813c14425',
            'c78d5930-9f35-11ec-a22c-9b7539623dc7',
            'a4ea23b0-9e55-11ec-8e2d-ab272683bf6b',
            '0dfa46c0-9111-11ec-a62c-1782a78ec934',
            '2bfbcdc0-9f0f-11ec-8c6a-37caf125548c',
            '5115bcf0-9f11-11ec-b43d-31252b8abbde',
            '0c0c5ef0-8f90-11ec-8763-53d5aa29a247',
            '915edc20-9f0b-11ec-87b3-aba78cfaf025',
            '5520b950-9c58-11ec-a40e-afbed2c43d6b',
            '5d428d90-9e46-11ec-a519-4b114e675ebd',
            'a3898c60-a48d-11ec-9272-b1dcc776b89f',
            '8b2e6c20-9f06-11ec-9268-e785323ca66e',
            '51947740-9e67-11ec-b8f3-1ddc6d3c77ec',
            'a1283580-9950-11ec-812e-7576ed0e29a5',
            '4324c450-9e66-11ec-a27c-072d8b146778',
            '4453e890-9e66-11ec-a042-2daabd1858fd',
            'dee78d60-a3c3-11ec-a4f4-f59cbf51d146',
            '2ee948e0-a498-11ec-bd32-8394f5dbb31d',
            '3066f670-a498-11ec-8b8f-8fe1a073a4d7',
            '426dd230-9e55-11ec-9dad-bf31e30bbcae',
            '76102f30-9e55-11ec-8760-d146e6459fb2',
            '7f138550-9e66-11ec-ac38-ebe70c125378',
            '0af2b440-a1bb-11ec-9bcc-415ade82d140',
            '1398b5a0-a1bb-11ec-9b4e-6f8b58ec9aff',
            '1e82fd50-a1bb-11ec-8e53-4d673ece0c57',
            '7134c3c0-9e42-11ec-a4e1-ebc2738249e4',
            '271d7520-a1bb-11ec-af9e-f7f5c97a2cef',
            '19e2f910-9d78-11ec-89d3-75dab8474292',
            'c1bf2160-9d78-11ec-b28a-4111c2634aa0',
            '69371700-9d79-11ec-b9b3-c1f233804b42',
            '10965570-9d7a-11ec-8974-df433afa34b0',
            'b7aea9e0-9d7a-11ec-b0a4-7d17e8f9182f',
            '5f4ba7a0-9d7b-11ec-9853-91441177536b',
            '073bbb20-9d7c-11ec-8c1c-4fa58251fbd6',
            '926991f0-a4cc-11ec-a7db-7d62adfa3a6b',
            '6b641fe0-9e69-11ec-ab28-2b4048873a91',
            '701e72a0-9e69-11ec-8f98-b3b370fdbf4a',
            '71466e50-9e69-11ec-bf95-57ca3693278b',
            'c6642900-a48d-11ec-91c7-a33c30126385',
            'a8832ef0-9e67-11ec-8ae1-cd4fa55e5410',
            'a9b63d20-9e67-11ec-ba5c-0b36aa27a0c7',
            '8bd29500-9e46-11ec-9b6d-6dacb5fcf087',
            '160fa780-9e47-11ec-9789-aff121b12a7e',
            'a401c310-8ea8-11ec-9f10-c5ecad3d8660',
            '6aa2e9e0-8f8f-11ec-8fb9-91274cb4916b',
            '97c82050-8f8f-11ec-b7ae-1d5342b4cded',
            'bf4ac640-9e68-11ec-9816-5fc15862b103',
            '811f68a0-9e46-11ec-b72e-2b51d63356c2',
            '7a90d410-9e42-11ec-8608-0fbb59f99915',
            '74b7dc90-9e42-11ec-a617-650185110c14',
            '994dc100-9e42-11ec-8772-255b26664276',
            '9486d440-9e42-11ec-a56e-6f9339732e51',
            '9f316660-9e42-11ec-a37d-4b1e5b8fd74a',
            '67d86e70-9e67-11ec-97a8-df752848e566',
            '69026e90-9e67-11ec-8bfc-3397d1ed20e0',
            '4b235ec0-9b63-11ec-acda-03a207f77073',
            '799f3b30-9e55-11ec-ab56-77d5f851b6f1',
            'e8b065a0-9e5c-11ec-9b2f-d5fc7c285d9a',
            'e9ddca40-9e5c-11ec-a70d-f1278f567da5',
            'a1690510-9e55-11ec-94ec-117992d1755d',
            '60562c70-9c4b-11ec-90d4-bf2f902d64e7',
            '82964c30-9e66-11ec-a331-bdf7a229f097',
            '6f1c7710-9e46-11ec-82f1-07f20f5fe308',
            '21a42250-9e55-11ec-a102-a18b4184154e',
            '948e28b0-9e55-11ec-a4bd-1ff11d3d12fb',
            '0c002140-9e69-11ec-90ab-6d35905f3930',
            '0d328800-9e69-11ec-b655-c30e14d611e3',
            '7fe4c8c0-9e46-11ec-a1ee-dfdfb0c1f536',
            '60363eb0-9c4c-11ec-ae69-076712abfa2c',
            '93770dd0-9c4c-11ec-bcba-e34405ac54a9',
            '24262270-9e47-11ec-b8ee-51c6db03c917',
            '7b1cb500-9e46-11ec-b9fb-514e0374817c',
            'e0aaf4b0-9e50-11ec-887a-2923b8415d14',
            'ee94e900-9e5a-11ec-a113-6daeac0ef7ba',
            'f0e998c0-9e5a-11ec-9fe3-27ccf461974a',
            '42b6d8c0-9b97-11ec-bdad-cf2f3f1ac985',
            '1c5eedc0-9c85-11ec-bd2f-7d71a609d7f8',
            '4e17ca80-9c57-11ec-a3ad-09b5a48c7e52',
            '656bae50-98c7-11ec-9e14-9dce04ee2866',
            '77c256c0-98c7-11ec-bc5b-cbed7cec1851',
            'fef453d0-9b64-11ec-89ce-577ac9c55951',
            '50433b20-9e69-11ec-847b-994cde3467be',
            '423daa90-998f-11ec-9aa0-83081fe8100a',
            '97bb8be0-9e46-11ec-985b-936522b0d120',
            '9e908210-9b73-11ec-89dd-b11cf580a9b6',
            'b10e93b0-9c56-11ec-a939-2f9d7b11b3eb',
            '60951560-9e66-11ec-a291-a93074409f04',
            '372f08e0-9c84-11ec-8752-8b306ef7241f',
            'e57bb0a0-98cf-11ec-9ab1-43ee8e97d23e',
            '05567a20-a3c4-11ec-9253-e1bff2dfff29',
            '03b14140-a3c4-11ec-80f2-d5e7d7e3f1ed',
            '546e7c50-a3c4-11ec-b790-77960238a683',
            '4d713630-9c97-11ec-bb43-af8c622c474f',
            '128fe220-9c97-11ec-967f-9f8a385af192',
            'd6fe1380-9e68-11ec-8638-2fbc3891d713',
            'da9d3280-9e68-11ec-86ad-9fd5669302b4',
            'dbc8c780-9e68-11ec-92c0-459bdc76d276',
            'dcf1c8b0-9e68-11ec-a606-27d2273b272d',
            '8e733a70-9e66-11ec-9723-c97265b2771f',
            'bdf75570-a48d-11ec-95f9-5b82a6e6cd31',
            'ba794630-a48d-11ec-b375-5de76fb58507',
            'c226d6b0-a48d-11ec-ba09-55f18dacf13c',
            'd07a4ab0-a48d-11ec-a648-cbaa4cba4904',
            'd366b7e0-a48d-11ec-af25-957a4d95ec42',
            'c4fe8410-a48d-11ec-afeb-e9bf9f707d07',
            'cab02c50-a48d-11ec-8541-4bed68789233',
            'cd8f7e40-a48d-11ec-90ed-5f87f4b1fd26',
            'cc198c60-a48d-11ec-995f-51dc1e95ff5e',
            'd1112de0-a48e-11ec-a992-57e056b4ba0a',
            'f9494dc0-9f02-11ec-8c86-932ddb4081ec',
            '78b9e960-9e46-11ec-b8de-3b06b73f145a',
            '494bed70-9f0c-11ec-b958-8b37a25708f2',
            '00dcb150-9f0d-11ec-a52d-f5931c8fa0d2',
            'ba83bbc0-9f0d-11ec-8bca-9d3886ebeb3d',
            '73d75500-9f0e-11ec-8264-2b0afa4203fc',
            '06cf01c0-9e47-11ec-a2b5-e19eb63dda35',
            'dd03c720-9e50-11ec-9317-aba2d99cac13',
            '4a199960-9e46-11ec-801f-7b96ace9dba7',
            '909a69a0-9e46-11ec-8250-81b0a44d4107',
            '9b3c5e60-9e46-11ec-a1e3-7d2b23c25731',
            '32bdaf40-9e66-11ec-a3f6-cd860a3d93db',
            '82506520-9e46-11ec-a13a-bf4c903d2ce3',
            'f10c26c0-9c8f-11ec-84df-77cac62887b0',
            'b1b7a7a0-9b4b-11ec-8445-f340744af58f',
            'cd6b8640-9b4b-11ec-aa12-bbc3e8724a20',
            '79fe8a80-9b4b-11ec-afb1-83a378f4a5a8',
            '50c0cba0-98cc-11ec-919f-b5811ac9c384',
            '4842d000-9e55-11ec-a279-d9ee41101273',
            '7bf9ae60-9e55-11ec-8ed5-d15e7c7b7c2e',
            '2d33da90-98cc-11ec-8624-616c3cc2e1b4',
            '7acfad40-9e55-11ec-bbd7-7faa29f19837',
            '985d88c0-9e68-11ec-992d-cbc373002101',
            '9f6d87d0-9e68-11ec-90ef-099cb8b796a6',
            '875a6da0-9e66-11ec-a5c5-31419103fe45',
            '9db9c280-9e66-11ec-b77f-51ace8103003',
            'c19fb7a0-9e68-11ec-9875-63d61e0fc8d3',
            'c3fc1e00-9e68-11ec-80ff-413b65dac6af',
            'c528e750-9e68-11ec-a605-71651faddebc',
            'c6540fb0-9e68-11ec-b6e7-4170d7619c7f',
            'c784ec80-9e68-11ec-9b7f-55d3658771bf',
            'c8b8aaa0-9e68-11ec-ab74-dfe94d0bed21',
            'c9ebf350-9e68-11ec-88b5-2986933ed9cf',
            '9a29c930-9e66-11ec-9964-3b178a8015df',
            'c2d1ec60-9e68-11ec-89fd-d55da0cbcb3b',
            '9ee4cb20-9e66-11ec-8fb4-cdbf0b7e2f51',
            '70029420-9e65-11ec-bc1a-bd291094e264',
            'f15b0980-9c8a-11ec-8d72-2378a103b5a7',
            '6383e0a0-9c48-11ec-a49c-c77b16cdde80',
            '4d042330-9c8c-11ec-9542-9d6784cf3a8f',
            '52bd38f0-9e67-11ec-b0b8-d33ded42d33f',
            '53e78b60-9e67-11ec-b566-3b56897a6313',
            '5514ba40-9e67-11ec-a3df-0d871ecfb3ca',
            '5642d3d0-9e67-11ec-8697-515634d89ca7',
            'ac19d150-9998-11ec-b484-0f335300a09f',
            '77e597b0-9bb8-11ec-a669-f70ee1fa9a06',
            '216d02c0-9bb8-11ec-ac8f-07887e97e2ca',
            'e5914460-9997-11ec-8793-33b930e60e57',
            'b18641c0-9bb8-11ec-add5-25f7fd1aa263',
            'ce5c0f60-9bb8-11ec-90ca-113c7bb58525',
            '55540180-9e54-11ec-8673-47d4668d9aa7',
            '5688fcf0-9e54-11ec-8110-f5ad7ec47b41',
            '51d16490-9e54-11ec-9398-63ed0c683717',
            '52fd8af0-9e54-11ec-b87a-f1b7f85e5451',
            'f9825fe0-9c4b-11ec-95e5-23ea4da2bc6c',
            '2d0c4d40-9c4c-11ec-bc9d-0db7bc060621',
            '1ddba240-9e46-11ec-8120-0fc34b6df72d',
            '968d8970-9e46-11ec-9ea8-454940b20764',
            '9cbfa400-9c51-11ec-bc82-1bbbf4b81704',
            'a2ef9b20-9e68-11ec-b1a8-4952e12a0041',
            'a54c86f0-9e68-11ec-8af0-450a81493e8e',
            'a6780c80-9e68-11ec-b0bb-e5e443287d51',
            'aa010b80-9e68-11ec-959f-6dbfdbbd4298',
            '96a34460-9e66-11ec-b1db-abe8a86b6f02',
            'fc3432e0-9e46-11ec-b3a6-c9c227c2b1bd',
            '9cd83d80-9e42-11ec-99ac-59df8a06456e',
            '9cbf3610-9e67-11ec-95e8-21494b7b1a3f',
            '9dea1660-9e67-11ec-a007-05950e8ea156',
            '604f8630-9b66-11ec-afca-f7f8833db755',
            '7d236010-9e55-11ec-9807-a3cf6fe11b12',
            '4969d4d0-9e55-11ec-97bb-2d3bc4543c0e',
            'a62e1060-9e67-11ec-9f08-9b7b2f79924a',
            'a757d540-9e67-11ec-8626-a7f1071718e3',
            '942d03f0-9e46-11ec-8f24-712ed906eed9',
            '4af69400-9b9b-11ec-ba24-4da96533d382',
            '26a370f0-9b63-11ec-ac23-fbe6a91455b0',
            'e40880a0-9e5c-11ec-8963-edffd0304ad3',
            'e2cc2020-9e5c-11ec-9eb1-99d1f997d76f',
            'e6598660-9e5c-11ec-bf19-337c59d74c09',
            'e78304d0-9e5c-11ec-92fc-6b994a245b26',
            '981a1bc0-9e42-11ec-905a-3551cad04da4',
            'c01e7a90-9e45-11ec-8b28-51bc56536022',
            '69116480-9c51-11ec-a493-b50044e17367',
            '70d76810-9c84-11ec-8b98-adcae72fb41d',
            'c15e3040-9e45-11ec-84f4-7dd4c72f3346',
            '45a41420-9b65-11ec-a4ba-bbbc04d552ed',
            '2b30cda0-9c8b-11ec-812a-1fdc5e7735c5',
            '9140b8f0-9b63-11ec-86e3-e96ed26140e6',
            '64d40f30-9c4f-11ec-b1f4-a1a7c1716c53',
            '9f160a90-9e67-11ec-ba5b-b3e18db696ea',
            'fe8e30c0-9e46-11ec-b1b5-dd7ec0b415cc',
            'a0434b20-9e67-11ec-ad0e-d7a557ff3c78',
            '96e39420-9e42-11ec-bc42-1ff323aeadb3',
            '9b590890-9e66-11ec-a7b0-19c66aad72fb',
            '83c311c0-9e66-11ec-810b-19083172bc17',
            '96e13890-9e67-11ec-805b-29bb64b9cf4d',
            '95b3e090-9e67-11ec-a346-a5dc12316f0d',
            '23c8d0d0-9c89-11ec-82ab-a70f42102774',
            '6cc19760-9e46-11ec-b09b-17572d7df658',
            '8aa758d0-9e46-11ec-9765-79c4720f85c7',
            '3a13a520-993f-11ec-9aac-45e48c72f046',
            'c586fc00-9b88-11ec-8b8a-9d0adbd4c40d',
            '44720cc0-9c8a-11ec-8091-1125e55b8a37',
            '6b58b590-9e67-11ec-9494-c913bc6b2663',
            '6c83b990-9e67-11ec-b1b6-e16952e0260c',
            '6db39490-9e67-11ec-9fa2-5d8f304efd6f',
            '700af6f0-9e67-11ec-8e91-a17a94ac70d8',
            '6a2b5e90-9e67-11ec-82a1-b3577aa12b85',
            '9c8b86c0-9e66-11ec-8ea9-3318abbeeb61',
            'fb22abb0-9e45-11ec-9860-319ba31267b8',
            '8385d770-9e46-11ec-9745-2fb30984df15',
            '677ec820-9e65-11ec-abc7-03af1ea76820',
            '6652e0f0-9e65-11ec-8cd3-2d96d1eeafca',
            'f8a76c30-9e46-11ec-830d-ffdc69be06a6',
            'ec6d5000-998d-11ec-bbc1-8b236c40a43d',
            '4e9e48c0-998e-11ec-b80e-f715c4b8ab40',
            '89789a30-9e46-11ec-82b0-0dd3ab4f563c',
            '60c53780-9e67-11ec-8ca4-4fb0fb428869',
            'b7bc43f0-9c8a-11ec-9e3f-9faa080ab954',
            '85dda5e0-9e46-11ec-96fc-77fef910b84c',
            '292d1240-9e6a-11ec-af2d-698ba1d0b2ad',
            '2a580520-9e6a-11ec-b9ef-67970c8a16da',
            '762444c0-9b71-11ec-8196-f1015080875e',
            'b1fdb8a0-9e67-11ec-9863-9504ae22c582',
            'd3d5aff0-9b65-11ec-9c60-d51fbd262cfc',
            'd4c33390-9e50-11ec-82b3-33bcf6d17290',
            'b097cbc0-9e45-11ec-8130-276b343c8fc1',
            'e005ef50-9a10-11ec-be7f-af1b4385283e',
            'b960c060-9e68-11ec-b335-33666ba62951',
            'bcf3da10-9e68-11ec-bb8a-27b1a04e8201',
            'ba954440-9e68-11ec-8362-ff8bf1890754',
            'c07520b0-9e68-11ec-adf6-a7b3b410fa48',
            'f5307370-9e45-11ec-a426-f5d3a1cef542',
            '0218a820-9e47-11ec-8d87-5d9b85182175',
            'fda223a0-9c4e-11ec-b52d-c335192fa3ca',
            'd85283a0-9e50-11ec-880e-014734b89e9e',
            'dbd25400-9e50-11ec-9aea-6b3daa1306b7',
            '35a1a360-9e46-11ec-a756-1fe9639c3c72',
            '097e5480-9e46-11ec-b60d-e13b500a05d6',
            '962b8130-9c4e-11ec-b484-4ba4a211f396',
            '61ed6190-9e42-11ec-bc6e-71dd12604f26',
            '8b4962f0-9e42-11ec-a09e-6b7addd62bb3',
            '324a4320-9b91-11ec-ae75-412fe044b981',
            '1315d8b0-9b91-11ec-9772-e7657dd47c42',
            '9a63d5d0-9c50-11ec-8512-f5d1496b4d58',
            '33032ac0-9c50-11ec-b10c-311e1c53c5cb',
            '73f17a10-9e46-11ec-8695-3960503c714d',
            '128a9da0-9e47-11ec-ae0a-05c2e0e9d702',
            '6fd1d630-9c8d-11ec-b809-d3283a9d41c2',
            '8e350430-9e46-11ec-a9cd-896ddf0938b3',
            '5db9b1c0-9b6d-11ec-be40-ef3e545ee58e',
            '26216700-9e46-11ec-9940-e360c6bd5eb7',
            '76591420-9e46-11ec-bf82-3b3e35e700d8',
            '2c80f9e0-9e47-11ec-8563-131a88368e22',
            'c85a9eb0-9b4c-11ec-aaa8-d9fa3579a10b',
            'b91a4d00-9e67-11ec-bbbb-17d90ca68678',
            '074eab00-9e69-11ec-bc5b-3d4d96cff8ef',
            '087bf220-9e69-11ec-8dcc-b7bbe52d2d5b',
            '1cb23bd0-9e46-11ec-8070-f97853dea495',
            '09a96980-9e69-11ec-b09b-05c2208903c4',
            '25591ca0-9e47-11ec-ab94-835a1e1ac694',
            '97567860-9c89-11ec-934a-8554f25e9a2d',
            'c23337b0-9e55-11ec-96c5-257fd743debf',
            '59b12340-9e46-11ec-89b0-05a722d21bb1',
            '79ffeb30-9b73-11ec-803a-290b613aaf0b',
            'c9eabcd0-9c4e-11ec-8e76-3dc9413e2df3',
            'faaccaf0-9c4c-11ec-b92c-2b9a230ba1d0',
            '363d5f40-9e66-11ec-8ebb-bb2cc8e7cea9',
            '3766fd20-9e66-11ec-9608-43b94ff13af5',
            'ae6a9c90-9e67-11ec-9d25-9b1ae8096064',
            'e1fd49b0-9e45-11ec-a0e2-0f0ca7b30914',
            '0b7f78e0-9e47-11ec-a594-233bf2d75590'
        ];
    
        $urut = 1;

        $material_stocks = MaterialStock::where([
            ['is_allocated',false],
            ['is_closing_balance',false],
            ['is_stock_on_the_fly',false]
        ])
        ->whereNotNull('approval_date')
        ->whereNull('deleted_at')
        ->whereIn(db::raw("(c_order_id, item_id, warehouse_id)"),function($query) use($update_allocations_acc) 
        {
            $query->select('c_order_id','item_id_source','warehouse_id')
            ->from('auto_allocations')
            ->whereIn('id',$update_allocations_acc)
            // ->whereIn('erp_allocation_id',$update_allocations_acc)                    
            ->groupby('c_order_id','item_id_source','warehouse_id');
        })
        ->get();

        // dd($material_stocks);

        $movement_date = carbon::now();

        foreach ($material_stocks as $key => $material_stock )
        {
            echo $urut . ". " . $material_stock->document_no . " " . $material_stock->item_code . " Stock = ". $material_stock->stock . " == " .Carbon::now() . "\n";
            // echo $material_stock->document_no."_".$material_stock->item_code;
            if(!$material_stock->last_status) ReportStock::doAllocation($material_stock, '91', $movement_date);
            
            $urut++;
        }

    }

    static function dailyPurchaseItemsMar()
    {
        $movement_date  = carbon::now();
        $udpateErp = array();

        try 
        {
            DB::beginTransaction();

            $purchase_items = DB::connection('erp') //dev_erp //erp
            ->table('rma_wms_planning_alokasi') // rma_wms_planning_v2_cron -->viewnya
            ->where([
                ['isintegrate',false],
                ['status_lc','PR'],
                ['is_additional', 'f'],
            ])->where('category','not like', '%FB%')
            ->get();
            // dd($purchase_items);
           
            foreach ($purchase_items as $key => $purchase_item)
            {
                
                $c_order_id        = $purchase_item->c_order_id;
                $item_id           = $purchase_item->item_id;
                $c_bpartner_id     = $purchase_item->c_bpartner_id;
                $supplier_name     = strtoupper($purchase_item->supplier_name);
                $item_code         = strtoupper($purchase_item->item_code);
                $document_no       = strtoupper($purchase_item->document_no);
                $po_sample         = strpos($purchase_item->po_buyer, '-S');
                // $po_buyers         = PoBuyer::where('po_buyer', $purchase_item->po_buyer)->where('brand', 'ADIDAS')->exists();
                // if($po_buyers == true)
                // {
                $purchase_po_buyer = trim(str_replace('-S', '',$purchase_item->po_buyer));
                // }
                // {
                //     $purchase_po_buyer = $purchase_item->po_buyer;
                // }
                $warehouse_id      = $purchase_item->m_warehouse_id;
                $uom               = $purchase_item->uom;
                $category          = $purchase_item->category;
                $qty               = sprintf('%0.8f',$purchase_item->qty);
                $erp_allocation_id = $purchase_item->uuid;
                $is_fabric         = $purchase_item->isfabric;
                if($purchase_item->is_additional == 'f')
                {
                    $is_additional = false;
                }
                else
                {
                    $is_additional = true;
                }
                
                if($warehouse_id == '1000011') $warehouse_name = 'FABRIC AOI 2';
                else if($warehouse_id == '1000001') $warehouse_name = 'FABRIC AOI 1';
                else if($warehouse_id == '1000013') $warehouse_name = 'ACCESSORIES AOI 2';
                else if($warehouse_id == '1000002') $warehouse_name = 'ACCESSORIES AOI 1';

                $reroute            = ReroutePoBuyer::where('old_po_buyer',$purchase_po_buyer)->first();
                if($reroute)
                {
                    $new_po_buyer = $reroute->new_po_buyer;
                    $old_po_buyer = $reroute->old_po_buyer;
                }else
                {
                    $new_po_buyer = $purchase_po_buyer;
                    $old_po_buyer = $purchase_po_buyer;
                }

                $__po_buyer = PoBuyer::where('po_buyer',$new_po_buyer)->first();
                

                if($__po_buyer)
                {
                    $lc_date                    = $__po_buyer->lc_date;
                    $promise_date               = ($__po_buyer->statistical_date ? $__po_buyer->statistical_date : $__po_buyer->promise_date);
                    $season                     = $__po_buyer->season;
                    $type_stock_erp_code        = $__po_buyer->type_stock_erp_code;
                    $cancel_date                = $__po_buyer->cancel_date;

                    if($is_fabric == 'N')
                    {
                        $tgl_date                   = ($__po_buyer->lc_date ? $__po_buyer->lc_date->format('d') : 'LC NOT FOUND');
                        $month_lc                   = ($__po_buyer->lc_date ? $__po_buyer->lc_date->format('my')  : 'LC NOT FOUND');

                        if($warehouse_id == '1000002') $warehouse_sequnce = '001';
                        else if($warehouse_id == '1000013') $warehouse_sequnce = '002';
                    
                        $document_allocation_number = 'PRLC'.$tgl_date.'-'.$month_lc.'-'.$warehouse_sequnce;
                    }
                    
                }else
                {
                    $promise_date               = null;
                    $lc_date                    = null;
                    $season                     = null;
                    $type_stock_erp_code        = null;
                    $cancel_date                = null;
                    $document_allocation_number = null;
                }
                
                if($type_stock_erp_code == 1) $type_stock       = 'SLT';
                else if($type_stock_erp_code == 2) $type_stock  = 'REGULER';
                else if($type_stock_erp_code == 3) $type_stock  = 'PR/SR';
                else if($type_stock_erp_code == 4) $type_stock  = 'MTFC';
                else if($type_stock_erp_code == 5) $type_stock  = 'NB';
                else $type_stock                                = null;

                
                if($cancel_date != null) $status_po_buyer = 'cancel';
                else $status_po_buyer                     = 'active';
                
                $system             = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();
    
                $item               = Item::where('item_id',$item_id)->first();
                $item_desc          = ($item)? strtoupper($item->item_desc): 'data not found';
                $is_exists = null;
                
                if($lc_date >= '2019-08-15' && $category == 'FB') // cut off mulai lc tanggal ini, user baru setuju mau maintain
                {
                    
                        $is_exists = AutoAllocation::whereNotNull('erp_allocation_id')
                        ->where([
                            ['c_order_id',$c_order_id],
                            ['c_bpartner_id',$c_bpartner_id],
                            ['item_id_book',$item_id],
                            ['item_id_source',$item_id],
                            ['po_buyer',$new_po_buyer],
                            ['erp_allocation_id',$erp_allocation_id],
                            ['warehouse_id',$warehouse_id],
                            ['is_fabric',true],
                        ])
                        ->first();

                        if(!$is_exists)
                        {
                            $newAllocation = AutoAllocation::create([
                                'type_stock_erp_code'               => $type_stock_erp_code,
                                'type_stock'                        => $type_stock,
                                'lc_date'                           => $lc_date,
                                'promise_date'                      => $promise_date,
                                'season'                            => $season,
                                'document_no'                       => $document_no,
                                'c_bpartner_id'                     => $c_bpartner_id,
                                'supplier_name'                     => $supplier_name,
                                'po_buyer'                          => $new_po_buyer,
                                'c_order_id'                        => $c_order_id,
                                'item_id_book'                      => $item_id,
                                'item_id_source'                    => $item_id,
                                'old_po_buyer'                      => $old_po_buyer,
                                'item_code_source'                  => $item_code,
                                'item_code_book'                    => $item_code,
                                'item_code'                         => $item_code,
                                'item_desc'                         => $item_desc,
                                'category'                          => $category,
                                'uom'                               => $uom,
                                'warehouse_name'                    => $warehouse_name,
                                'warehouse_id'                      => $warehouse_id,
                                'qty_allocation'                    => $qty,
                                'qty_outstanding'                   => $qty,
                                'qty_allocated'                     => 0,
                                'status_po_buyer'                   => $status_po_buyer,
                                'is_fabric'                         => true,
                                'is_already_generate_form_booking'  => false,
                                'is_upload_manual'                  => false,
                                'is_allocation_purchase'            => true,
                                'user_id'                           => $system->id,
                                'created_at'                        => $movement_date,
                                'updated_at'                        => $movement_date,
                                'erp_allocation_id'                 => $erp_allocation_id,
                                'is_additional'                     => $is_additional,
                            ]);

                            $udpateErp [] = $newAllocation->erp_allocation_id;   
                        }
                        else
                        {
                            $system = User::where([
                                ['name','system'],
                                ['warehouse',$warehouse_id]
                            ])
                            ->first();
                            $old_qty_allocation  = sprintf('%0.8f',$is_exists->qty_allocation);
                            $old_qty_outstanding = sprintf('%0.8f',$is_exists->qty_outstanding);
                            $remark              = $is_exists->remark;
        
                            if($old_qty_allocation/$qty == 1)
                            {
                                $is_exists->erp_allocation_id = $erp_allocation_id;
                                $is_exists->save();
                            }
                            else
                            {
                                $is_exists->qty_allocation    = $old_qty_allocation + $qty;
                                $is_exists->qty_outstanding   = $old_qty_outstanding + $qty;
                                $is_exists->erp_allocation_id = $erp_allocation_id;
                                $is_exists->remark            = $remark. 'PENAMBAHAN QTY ALLOCATION SEBESAR '.$qty;
                                $is_exists->user_id           = $system->id;
                                $is_exists->save();


                            }
                            $udpateErp [] = $erp_allocation_id;      
                    
                    }
                }
                else if($category != 'FB' && ($new_po_buyer != null || $new_po_buyer != ''))
                {
                    echo $erp_allocation_id;
                    $is_exists = AutoAllocation::whereNotNull('erp_allocation_id')
                    ->where([
                        ['c_order_id',$c_order_id],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['item_id_book',$item_id],
                        ['item_id_source',$item_id],
                        ['po_buyer',$new_po_buyer],
                        ['warehouse_id',$warehouse_id],
                        ['is_fabric',false],
                    ])
                    ->whereNull('deleted_at')
                    ->first();

                    if(!$is_exists)
                    {
                        $newAllocation = AutoAllocation::create([
                            'document_allocation_number'        => $document_allocation_number,
                            'type_stock_erp_code'               => $type_stock_erp_code,
                            'type_stock'                        => $type_stock,
                            'lc_date'                           => $lc_date,
                            'promise_date'                      => $promise_date,
                            'season'                            => $season,
                            'c_order_id'                        => $c_order_id,
                            'document_no'                       => $document_no,
                            'c_bpartner_id'                     => $c_bpartner_id,
                            'supplier_name'                     => $supplier_name,
                            'po_buyer'                          => $new_po_buyer,
                            'old_po_buyer'                      => $old_po_buyer,
                            'item_code_source'                  => $item_code,
                            'item_code_book'                    => $item_code,
                            'item_id_book'                      => $item_id,
                            'item_id_source'                    => $item_id,
                            'item_code'                         => $item_code,
                            'item_desc'                         => $item_desc,
                            'category'                          => $category,
                            'uom'                               => $uom,
                            'warehouse_name'                    => $warehouse_name,
                            'warehouse_id'                      => $warehouse_id,
                            'qty_allocation'                    => $qty,
                            'qty_outstanding'                   => $qty,
                            'qty_allocated'                     => 0,
                            'status_po_buyer'                   => $status_po_buyer,
                            'is_fabric'                         => false,
                            'is_already_generate_form_booking'  => false,
                            'is_upload_manual'                  => false,
                            'is_allocation_purchase'            => true,
                            'created_at'                        => $movement_date,
                            'updated_at'                        => $movement_date,
                            'user_id'                           => $system->id,
                            'erp_allocation_id'                 => $erp_allocation_id,
                            'is_additional'                     => $is_additional,
                        ]);

                        $udpateErp [] = $newAllocation->erp_allocation_id;
                    }else
                    {
                            $system = User::where([
                                ['name','system'],
                                ['warehouse',$warehouse_id]
                            ])
                            ->first();
                            $old_qty_allocation  = sprintf('%0.8f',$is_exists->qty_allocation);
                            $old_qty_outstanding = sprintf('%0.8f',$is_exists->qty_outstanding);
                            $remark              = $is_exists->remark;
        
                            if($old_qty_allocation/$qty == 1)
                            {
                                $is_exists->erp_allocation_id = $erp_allocation_id;
                                $is_exists->save();
                            }
                            else
                            {
                                $is_exists->qty_allocation    = $old_qty_allocation + $qty;
                                $is_exists->qty_outstanding   = $old_qty_outstanding + $qty;
                                $is_exists->erp_allocation_id = $erp_allocation_id;
                                $is_exists->remark            = $remark. 'PENAMBAHAN QTY ALLOCATION SEBESAR '.$qty;
                                $is_exists->user_id           = $system->id;
                                $is_exists->save();
                            }

                            $udpateErp [] = $erp_allocation_id;

                    }
                    
                }

                DB::connection('erp') //dev_erp //erp
                ->table('rma_wms_planning_alokasi')
                ->whereIn('uuid',$udpateErp)
                ->update([
                    'isintegrate' => true,
                    'updated'     => $movement_date,
                    'source'    => 'test'
                ]);

                DB::commit();
                
            }

        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        $checkAllocations =  DB::table('auto_allocations') //double check alokasi yg dimasukin pake erp_allocation_id
        ->whereIn('erp_allocation_id', $udpateErp)
        // ->whereDate('created_at', '2022-02-12')
        ->get();

        $groupByErpId  =  $checkAllocations->groupBy('erp_allocation_id'); //gatau knp kalo di grouping diatas tetep double, jadi tak grouping disini

        foreach ($groupByErpId as $key => $value){

            if(count($value) >1) {

            $id = $value[0]->id;
            $t = $value->where('id','!=', $id)->pluck('id');
            AutoAllocation::whereIn('id', $t)->delete();

            }

        }
       
    }

    static function insertRePrepareRollToBuyer()
    {
        $getIdPreparation = MaterialPreparationFabric::whereBetween('planning_date', array('2021-12-01', '2021-12-31'))
                        ->where('warehouse_id','1000011')->get();
            
        // whereBetween('planning_date', array('2021-12-01', '2021-12-31')
        foreach ($getIdPreparation as $key => $prepare) {
            $id = $prepare->id;
        
            MaterialPreparation::whereIn('id',function($query) use($id)
            {
                $query->select('material_preparation_id')
                ->from('detail_material_preparations')
                ->whereNotNull('detail_material_preparation_fabric_id')
                ->whereIn('detail_material_preparation_fabric_id',function($query2) use ($id)
                {
                    $query2->select('id')
                    ->from('detail_material_preparation_fabrics')
                    ->where('material_preparation_fabric_id',$id);
                })
                ->groupby('material_preparation_id');
            })
            ->delete();
            
            DetailPoBuyerPerRoll::whereIn('detail_material_preparation_fabric_id',function($query) use($id)
            {
                $query->select('id')
                ->from('detail_material_preparation_fabrics')
                ->where('material_preparation_fabric_id',$id);
            })
            ->delete();
            

            $material_preparation_fabric = MaterialPreparationFabric::find($id);

            
            if($material_preparation_fabric)
            {
                $__detail_material_planning_per_part    = new DetailMaterialPlanningFabricPerPart();
                $material_preparation_fabric_id         = $material_preparation_fabric->id;
                $c_order_id                             = $material_preparation_fabric->c_order_id;
                $document_no                            = $material_preparation_fabric->document_no;
                $c_bpartner_id                          = $material_preparation_fabric->c_bpartner_id;
                $planning_date                          = $material_preparation_fabric->planning_date;
                $warehouse_id                           = $material_preparation_fabric->warehouse_id;
                $article_no                             = $material_preparation_fabric->article_no;
                $item_id_book                           = $material_preparation_fabric->item_id_book;
                $item_id_source                         = $material_preparation_fabric->item_id_source;
                $is_piping                              = $material_preparation_fabric->is_piping;
                $_style                                 = $material_preparation_fabric->_style;
                $is_from_additional                     = $material_preparation_fabric->is_from_additional;
                $item_code                              = strtoupper(trim($material_preparation_fabric->item_code));
                $movement_date                          = carbon::now()->toDateTimeString();
                $detail_material_preparation_ids        = array();
                $delete_material_movement_lines         = array();
                
                $inventory_erp = Locator::with('area')
                ->where([
                    ['is_active',true],
                    ['rack','ERP INVENTORY'],
                ])
                ->whereHas('area',function ($query) use($warehouse_id)
                {
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','ERP INVENTORY');
                })
                ->first();

                $inventory = Locator::where('rack','INVENTORY')
                ->whereHas('area',function ($query) use ($warehouse_id)
                {
                    $query->where('name','INVENTORY')
                    ->where('warehouse',$warehouse_id);
                })
                ->first();
                
                
                try 
                {
                    DB::beginTransaction();

                

                    if(!$is_from_additional)
                    {
                        $detail_preparation_fabrics = DetailMaterialPreparationFabric::where('material_preparation_fabric_id',$material_preparation_fabric_id)
                        ->orderby('actual_lot','asc')
                        ->get();

                        foreach ($detail_preparation_fabrics as $key => $detail_preparation_fabric) 
                        {
                            $is_closing                             = $detail_preparation_fabric->is_closing;
                            $user_id                                = $detail_preparation_fabric->user_id;
                            $_reserved_qty                          = sprintf('%0.8f',$detail_preparation_fabric->reserved_qty);
                            $detail_material_preparation_fabric_id  = $detail_preparation_fabric->id;
                            // $material_stock_id  = $detail_preparation_fabric->material_stock_id;
                            // $actual_qty                             = MaterialStock::find($material_stock_id);
                            // $_reserved_qty                          = $actual_qty->actual_length;
                            $total_prepare                          = DetailPoBuyerPerRoll::where('detail_material_preparation_fabric_id',$detail_material_preparation_fabric_id)->sum('qty_booking');
                            $reserved_qty                           =  sprintf('%0.8f',$_reserved_qty - $total_prepare);

                            $detail_material_plannings = DetailMaterialPlanningFabric::where([
                                ['_style',$_style],
                                ['article_no',$article_no],
                                ['c_order_id',$c_order_id],
                                ['item_id_book',$item_id_book],
                                ['item_id_source',$item_id_source],
                                ['planning_date',$planning_date],
                                ['is_piping',$is_piping],
                                ['warehouse_id',$warehouse_id],
                            ])
                            ->whereNull('deleted_at')
                            ->orderby('po_buyer','asc')
                            ->get();

                            foreach ($detail_material_plannings as $key => $detail_material_planning) 
                            {
                                $material_planning_fabric_id        = $detail_material_planning->material_planning_fabric_id;
                                $po_buyer                           = $detail_material_planning->po_buyer;
                                $style                              = $detail_material_planning->style;
                                $job_order                          = $detail_material_planning->materialPlanningFabric->job_order;
                                $style_in_job_order                 = explode('::',$job_order)[0];
                                $check_saldo                        = $detail_material_planning->checkSaldo($_style,$article_no,$c_order_id,$item_id_book,$item_id_source,$planning_date,$is_piping,$warehouse_id,$po_buyer)[0]->exists;
                                                            
                                if($check_saldo) $qty_consumtion    = sprintf('%0.8f',$detail_material_planning->qty_booking - $detail_material_planning->qtyAllocated($detail_material_planning->id));
                                else $qty_consumtion = $reserved_qty;
                                //echo 'reserved'.$reserved_qty.PHP_EOL;
                                if($qty_consumtion > 0 && $reserved_qty > 0)
                                {
                                    
                                    if ($reserved_qty/$qty_consumtion >= 1) $_supplied1 = $qty_consumtion;
                                    else  $_supplied1 = $reserved_qty;

                                    $material_requirement_per_parts = DetailMaterialPlanningFabricPerPart::where('material_planning_fabric_id',$material_planning_fabric_id)
                                    ->orderby('part_no','asc')
                                    ->get();
                                    
                                    foreach ($material_requirement_per_parts as $key => $material_requirement_per_part) 
                                    {
                                        $qty_need_per_part                  = $material_requirement_per_part->qty_per_part;
                                        $part_no                            = $material_requirement_per_part->part_no;
                                        $checkSaldoPerPart                 = $material_requirement_per_part->checkSaldo($material_planning_fabric_id)[0]->exists;
                        
                                        if($checkSaldoPerPart)
                                        {
                                            $total_preparation_per_part         = $material_requirement_per_part->getTotalPreparationFabric($material_requirement_per_part->id);
                                            $qty_required_per_part              = sprintf('%0.8f',$qty_need_per_part - $total_preparation_per_part);
                                        }else
                                        {
                                            $total_preparation_per_part         = 1;
                                            $qty_required_per_part              = $qty_consumtion;//harus dikurangi per po supplier
                                        } 
                                        
                                        //echo $check_saldo.' '.$detail_material_preparation_fabric_id.' '.$po_buyer.' id planning '. $material_requirement_per_part->material_planning_fabric_id. ' part'.  $part_no .' kebutuhan '.$qty_required_per_part .' total prepare '.$total_preparation_per_part.' supply'.$__supplied.PHP_EOL;
                                        ////echo $check_saldo.' '.$detail_material_preparation_fabric_id.' '.$po_buyer.' '.$qty_consumtion.' '.$part_no.' '.$reserved_qty.' '.$total_preparation_per_part.' '.$qty_required_per_part.' '.$__supplied.PHP_EOL;
                                        //echo $__supplied.PHP_EOL;
                                        if($qty_required_per_part > 0)
                                        {
                                            if ($_supplied1/$qty_required_per_part >= 1) $_supplied_per_part = $qty_required_per_part;
                                            else  $_supplied_per_part = $_supplied1;
                                            
                                            

                                            DetailPoBuyerPerRoll::FirstorCreate([
                                                'material_planning_fabric_id'           => $material_planning_fabric_id,
                                                'detail_material_preparation_fabric_id' => $detail_material_preparation_fabric_id,
                                                'detail_material_planning_fabric_id'    => $detail_material_planning->id,
                                                'detail_material_planning_fabric_per_part_id' => $material_requirement_per_part->id,
                                                'planning_date'                         => $planning_date,
                                                'part_no'                               => $part_no,
                                                'po_buyer'                              => $po_buyer,
                                                'article_no'                            => $article_no,
                                                'style'                                 => $style,
                                                'style_in_job_order'                    => $style_in_job_order,
                                                'job_order'                             => $job_order,
                                                'qty_booking'                           => sprintf('%0.8f',$_supplied_per_part),
                                                'warehouse_id'                          => $warehouse_id,
                                                'user_id'                               => $user_id,
                                                'is_closing'                            => $is_closing,
                                                'created_at'                            => $movement_date,
                                                'updated_at'                            => $movement_date,
                                            ]);

                                            $qty_required_per_part -= $_supplied_per_part;
                                            $_supplied1 -= $_supplied1;
                                            $qty_consumtion -= $_supplied_per_part;
                                            $reserved_qty   -= $_supplied_per_part;
                                        }
                                    }

                                    

                                    
                                }

                                $detail_material_preparation_ids [] = $detail_material_preparation_fabric_id;
                            }

                            
                        }   
                    }
                    
                    DetailMaterialPreparationFabric::where('material_preparation_fabric_id',$id)
                    ->update([
                        'is_status_prepare_inserted_to_material_preparation'    => false,
                        'is_status_out_inserted_to_material_preparation'        => false,
                        'repreparation_roll_to_buyer_date'                      => $movement_date,
                    ]);

            
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }
        }

    }

    static function recalculateAllocatingPoBuyer()
    {
            $data_recalculate = DB::table('mna_check_recalculate')->get();
            // dd($data_recalculate);

            foreach ($data_recalculate as $key => $recal) {
                $auto_allocation_id = $recal->id;
                $is_purchase_item_stock = AutoAllocation::where('id',$auto_allocation_id)
                ->where('is_allocation_purchase',true)
                ->exists();
                echo $auto_allocation_id;
                echo '</br>';
            

                $material_stocks = MaterialStock::where([
                    ['is_allocated',false],
                    ['is_closing_balance',false],
                    ['is_stock_on_the_fly',false],
                ])
                ->whereNotNull('approval_date')
                ->whereNull('deleted_at')
                ->whereIn(db::raw("(c_order_id, item_id, warehouse_id)"),function($query) use($auto_allocation_id) 
                {
                    $query->select('c_order_id','item_id_source','warehouse_id')
                    ->from('auto_allocations')
                    ->where('id',$auto_allocation_id)                    
                    ->groupby('c_order_id','item_id_source','warehouse_id');
                })
                ->get();

                foreach ($material_stocks as $key => $material_stock )
                {
                    if(!$material_stock->last_status) ReportStock::doAllocation($material_stock, '91', carbon::now());
                }
            }
            
    }

    static function storeSummaryStockFabric()
    {
        $material_stock_ids = [   
            'e23a3580-2cb5-11ec-9bba-adb42a6f1545',
            'e7885570-2cb5-11ec-9b21-43f3d1a47fd1',
            'e4259140-2cb5-11ec-a42a-c788d4af6676',
            'e2d27650-2cb5-11ec-81e0-f7300a81d67d',
            'e83e1a80-2cb5-11ec-96cf-c1283d4fb4f6',
            '90c88e80-2cb5-11ec-9b2b-0969d70476b7',
            'dc5c47b0-2cb5-11ec-a687-0beeda938be3',
            'e3794170-2cb5-11ec-bc13-351e7fdd673b'                      
        ];
        $warehouse_id = '1000011';


        $material_stocks = MaterialStock::select('document_no'
        ,'c_order_id'
        ,'c_bpartner_id'
        ,'item_code'
        ,'item_id'
        ,'uom'
        ,db::raw('sum(stock) as stock')
        ,db::raw('sum(qty_order) as qty_order'))
        ->whereIn('id',$material_stock_ids)
        ->groupby('document_no','c_order_id','c_bpartner_id','item_code','item_id','uom')
        ->get();
        // dd($material_stocks);

        $summary_stocks = array();
        try 
        {
            DB::beginTransaction();

            foreach ($material_stocks as $key => $material_stock) 
            {
                echo $material_stock->document_no;
            
                $item                   = Item::where('item_code',$material_stock->item_code)->first();
                $new_c_order_id         = $material_stock->c_order_id;
                $new_c_bpartner_id      = $material_stock->c_bpartner_id;
                $new_document_no        = $material_stock->document_no;
                $new_item_id            = $material_stock->item_id;
                $new_item_code          = $material_stock->item_code;

                $supplier               = Supplier::select('supplier_code','supplier_name')->where('c_bpartner_id',$new_c_bpartner_id)->groupby('supplier_code','supplier_name')->first();
                $supplier_code          = ($supplier)? $supplier->supplier_code : null;
                $supplier_name          = ($supplier)? $supplier->supplier_name : null;
                $new_color              = ($item)? $item->color : 'NOT FOUND';
                $new_uom                = $material_stock->uom;
                $new_stock              = sprintf('%0.8f',$material_stock->stock);
                $qty_order              = sprintf('%0.8f',$material_stock->qty_order);
            
                if($new_document_no == 'FREE STOCK' || $new_document_no == 'FREE STOCK-CELUP')
                {
                    $mapping_stock_id       = null;
                    $type_stock_erp_code    = '2';
                    $type_stock             = 'REGULER';
                }else
                {
                    $get_4_digit_from_beginning = substr($new_document_no,0, 4);
                    $_mapping_stock_4_digt      = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                    
                    if($_mapping_stock_4_digt)
                    {
                        $mapping_stock_id       = $_mapping_stock_4_digt->id;
                        $type_stock_erp_code    = $_mapping_stock_4_digt->type_stock_erp_code;
                        $type_stock             = $_mapping_stock_4_digt->type_stock;
                    }else
                    {
                        $get_5_digit_from_beginning = substr($new_document_no,0, 5);
                        $_mapping_stock_5_digt      = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                        
                        if($_mapping_stock_5_digt)
                        {
                            $mapping_stock_id       = $_mapping_stock_5_digt->id;
                            $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                            $type_stock             = $_mapping_stock_5_digt->type_stock;
                        }else
                        {
                            $mapping_stock_id       = null;
                            $type_stock_erp_code    = null;
                            $type_stock             = null;
                        }
                        
                    }
                }

                $is_exists = SummaryStockFabric::where([
                    ['document_no',$new_document_no],
                    ['c_bpartner_id',$new_c_bpartner_id],
                    ['item_id',$new_item_id],
                    ['warehouse_id',$warehouse_id],
                    ['is_from_handover', true]
                ])
                ->first();

                if($is_exists)
                {
                    $_summary_stock_id = $is_exists->id;
                }else
                {
                    if($new_stock >= $qty_order) $_qty_order = $new_stock;
                    else $_qty_order = $qty_order;

                    $summary_stock_fabric = SummaryStockFabric::FirstOrCreate([
                        'c_bpartner_id'         => $new_c_bpartner_id,
                        'c_order_id'            => $new_c_order_id,
                        'document_no'           => $new_document_no,
                        'supplier_code'         => $supplier_code,
                        'supplier_name'         => $supplier_name,
                        'item_code'             => $new_item_code,
                        'item_id'               => $new_item_id,
                        'color'                 => $new_color,
                        'category_stock'        => $type_stock,
                        'uom'                   => $new_uom,
                        'stock'                 => $new_stock,
                        'reserved_qty'          => 0,
                        'available_qty'         => $new_stock,
                        'qty_order'             => sprintf('%0.8f',$_qty_order),
                        'warehouse_id'          => $warehouse_id,
                    ]);
                    
                    $_summary_stock_id = $summary_stock_fabric->id;
                }
                echo '
                ';
                echo $_summary_stock_id;
                MaterialStock::where([
                    ['document_no',$new_document_no],
                    ['c_bpartner_id',$new_c_bpartner_id],
                    ['item_id',$new_item_id],
                    ['warehouse_id',$warehouse_id],
                ])
                ->whereNull('summary_stock_fabric_id')
                ->wherein('id',$material_stock_ids)
                ->update(['summary_stock_fabric_id' => $_summary_stock_id]);

                $summary_stocks [] =  $_summary_stock_id;
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        try 
        {
            DB::beginTransaction();
            $sum_summary_stocks = MaterialStock::select(
                'summary_stock_fabric_id',
                db::raw('sum(stock) as total_stock'),
                db::raw('sum(reserved_qty) as total_reserved'),
                db::raw('sum(available_qty) as total_available')
            )
            ->whereIn('summary_stock_fabric_id',$summary_stocks)
            ->groupby('summary_stock_fabric_id')
            ->get();

            foreach ($sum_summary_stocks as $key => $sum_summary_stock) 
            {
                $summary_stock_fabric   = SummaryStockFabric::find($sum_summary_stock->summary_stock_fabric_id);
                $qty_order              = $summary_stock_fabric->qty_order;
                $qty_order_reserved     = $summary_stock_fabric->qty_order_reserved;

                $new_stock              = $sum_summary_stock->total_stock;
                $total_reserved         = $sum_summary_stock->total_reserved;
                $total_available        = $sum_summary_stock->total_available;
            

                if($new_stock >= $qty_order) $_qty_order = $new_stock;
                else $_qty_order = $qty_order;

                $new_available_qty_order = $_qty_order - $qty_order_reserved;

                if($new_available_qty_order > 0)
                    $summary_stock_fabric->is_allocated = false;
                
                $summary_stock_fabric->stock                = sprintf('%0.8f',$new_stock);
                $summary_stock_fabric->reserved_qty         = sprintf('%0.8f',$total_reserved);
                $summary_stock_fabric->available_qty        = sprintf('%0.8f',$total_available);
                $summary_stock_fabric->qty_order            = sprintf('%0.8f',$_qty_order);
                $summary_stock_fabric->save();
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        // return $summary_stocks;

    }

    static function dailyPurchaseItems()
    {
        $movement_date = carbon::now()->todatetimestring();
        $purchase_items = DB::connection('erp') //dev_erp //erp
            ->table('rma_wms_planning_alokasi') // rma_wms_planning_v2_cron -->viewnya
            ->where([
                ['isintegrate',true],
                [DB::raw("to_char(created,'YYYYMMDD')"),'>=','2021-08-01'],
                ['isfabric', 'N'],
                ['status_lc','PR'],
                ['is_additional', 'f']
               // ['po_buyer','0124774790'],
            ])
            // ->limit(5)
            ->get();

            

        foreach ($purchase_items as $key => $purchase_item) {

            $is_exists = AutoAllocation::whereNotNull('erp_allocation_id')
            ->where('erp_allocation_id',$purchase_item->uuid)
            ->exists();

            echo $is_exists.' '.$purchase_item->uuid.'
                ';

            if(!$is_exists)
            {
                
                DB::connection('erp') //dev_erp //erp
                ->table('rma_wms_planning_alokasi')
                ->where('uuid',$purchase_item->uuid)
                ->update([
                    'isintegrate' => false,
                    'updated'     => $movement_date
                ]);

            }
            # code...
        }
    }

    static function caseClosingPreparationFabric()
    {
        $data   = db::select(db::raw("
            select id,warehouse_id,planning_date,total_reserved_qty,total_qty_outstanding,total_qty_rilex from material_preparation_fabrics
            where planning_date is not null
            and article_no is not null
            and _style is not null
            and total_reserved_qty != 0
            and total_qty_rilex != 0
            and total_reserved_qty::numeric != total_qty_rilex::numeric
            and to_char(planning_date, 'YYYY') = '2020'
            AND total_qty_outstanding < 1
            AND total_qty_outstanding >= 0
        "));


        foreach ($data as $key => $value) 
        {
            $id             = $value->id;
            $warehouse_id   = $value->warehouse_id;

            $system = User::where([
                ['name','system'],
                ['warehouse',$warehouse_id]
            ])
            ->first();

            ReportPreparationFabric::doClose($id,'AKTUAL SUDAH CLOSE',$system->id);
        }
    }

    static function recalculateAutoAllocationFabric()
    {
        $auto_allocations   = AutoAllocation::whereNull('deleted_at')
        ->where([
            ['status_po_buyer','active'],
            ['category','FB'],
            ['is_fabric',true],
            ['qty_allocated','>',0],
            [db::raw("qty_allocation::numeric"),'!=',db::raw("qty_allocated::numeric")],
        ])
        ->get();


        foreach ($auto_allocations as $key => $auto_allocation) 
        {
            $auto_allocation_id = $auto_allocation->id;
            $warehouse_id       = $auto_allocation->warehouse_id;

            $system = User::where([
                ['name','system'],
                ['warehouse',$warehouse_id]
            ])
            ->first();

            AllocationFabric::doRecalculate($auto_allocation_id);
            $data = AutoAllocation::find($auto_allocation_id);;

            if($data->qty_allocation != $data->qty_allocated && $data->qty_allocated != 0)
            {
                AllocationFabric::doCancellation($auto_allocation_id,'CANCEL KARNA QTY ALLOCATED TIDAK SAMA DENGAN QTY ALLOCATION, MENGGUNAKAN SCHEDULER',$system->id);
                AllocationFabric::updateAllocationFabricItems([$auto_allocation_id],$auto_allocation_id);

            }
        }
    }

    private function updateDetailPoBuyerPerRoll()
    {
        $data           = db::select(db::raw("
            select material_preparation_fabric_id from detail_preparation_vs_detail_roll_per_buyer_fabric
            where --total_relax::numeric != total_qty_booking_per_roll::numeric and 
            material_preparation_fabric_id ='d18f7a70-7d5c-11eb-8e20-475f56c2c6b4'
            --and warehouse_id = '1000001'
            --and to_char(planning_date, 'YYYY') = '2020'
            GROUP BY material_preparation_fabric_id
            order by count(0) desc
            "));

        foreach ($data as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();
                $material_preparation_fabric_id         = $value->material_preparation_fabric_id;
                
                FabricReportDailyMaterialPreparation::insertRePrepareRollToBuyer($material_preparation_fabric_id,0);

                DB::commit();
                
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }

    private function updateRerouteAutoAllocation()
    {
        $data           = db::select(db::raw("select id,barcode,material_stock_id,auto_allocation_id From material_preparations
        where po_buyer in (
        select new_po_buyer From reroute_po_buyers
        GROUP BY new_po_buyer ) 
        and auto_allocation_id is null
        and material_stock_id is not null
        and is_from_barcode_bom = false
        and type_po = '2'
        and is_from_closing = false
        order by created_at desc"));

        foreach ($data as $key => $value) 
        {
            try 
            {
               
                
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }

    private function updateClosinganFabric()
    {
        $data           = DetailMaterialPreparationFabric::where('is_closing',true)->get();
        foreach ($data as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();
                $created_at                             = $value->created_at;
                $material_preparation_fabric_id         = $value->material_preparation_fabric_id;
                $material_preparation_fabric            = MaterialPreparationFabric::find($material_preparation_fabric_id);
                
                $warehouse_id                           = $material_preparation_fabric->warehouse_id;
    
                if($material_preparation_fabric->is_piping)
                {
                    $distribution = Locator::whereHas('area',function($query) use ($warehouse_id)
                    {
                        $query->where([
                            ['name','DISTRIBUTION'],
                            ['warehouse',$warehouse_id],
                        ]);
                    })
                    ->first();
    
                    $last_locator_id    = $distribution->id;
                }else 
                {
                    $cutting = Locator::whereHas('area',function($query) use ($warehouse_id)
                    {
                        $query->where([
                            ['name','CUTTING'],
                            ['warehouse',$warehouse_id],
                        ]);
                    })
                    ->first();
    
                    $last_locator_id     = $cutting->id;
                }

                $value->last_locator_id         = $last_locator_id;
                $value->movement_out_date       = $created_at;
                $value->last_movement_date      = $created_at;
                $value->last_status_movement    = 'out';
                $value->save();
                

                DB::commit();
                
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }

    private function updateCorderLineLine()
    {

        // digunakan jika remark_integration ke locator rejet Shipment Not Found 
        $data           = DB::table('integration_movement_inventory_to_reject')
        ->where('remark_integration','Shipment Not Found')
        ->get();

        foreach ($data as $key => $value) 
        {
            try
            {
                DB::beginTransaction();
                //return view('errors.503');
               
                $material_movement_line     = MaterialMovementLine::find($value->material_movement_line_id);
                $material_stock_id          = $material_movement_line->material_stock_id;
                $material_preparation_id    = $material_movement_line->material_preparation_id;
                $item_id                    = $material_movement_line->item_id;
                $document_no                = strtoupper($material_movement_line->document_no);
                $no_packing_list            = $material_movement_line->movement->no_packing_list;
                $no_invoice                 = $material_movement_line->movement->no_invoice;
    
                if($material_preparation_id)
                {
                    $material_preparation   = MaterialPreparation::find($material_preparation_id);
                    $po_detail_id           = $material_preparation->po_detail_id;
    
                    $data_erp                   = DB::connection('erp')
                    ->table('tpb_cek_invoice')
                    ->where([
                        ['item_id',$item_id],
                        [db::raw('upper(trim(no_packing_list))'),strtoupper($no_packing_list)],
                        [db::raw('upper(trim(no_invoice))'),strtoupper($no_invoice)],
                    ])
                    ->where(db::raw('upper(trim(po_supplier))'),'LIKE',"%$document_no%")
                    ->first();

                    
                    $material_arrival           = MaterialArrival::whereNull('material_subcont_id')
                    ->whereNull('material_roll_handover_fabric_id')
                    ->where('po_detail_id',$po_detail_id)
                    ->first();
    
                    if($data_erp)
                    {
                        $material_arrival->c_orderline_id           = $data_erp->c_orderline_id;
                        $material_arrival->save();

                        $remark_integration                         = $material_movement_line->remark_integration;
                        $material_movement_line->remark_integration = $remark_integration.', SUDAH DI BENERIN C_ORDER_LINE_ID NYA';

                        $material_movement_line->c_orderline_id     = $data_erp->c_orderline_id;
                        $material_movement_line->save();
                    }
                    
                }else
                {
                    $material_stock         = MaterialStock::find($material_stock_id);
                    $po_detail_id           = $material_stock->po_detail_id;
    
                    $material_arrival           = MaterialArrival::whereNull('material_subcont_id')
                    ->whereNull('material_roll_handover_fabric_id')
                    ->where('po_detail_id',$po_detail_id)
                    ->first();
    
                    $data_erp                   = DB::connection('erp')
                    ->table('tpb_cek_invoice')
                    ->where([
                        ['item_id',$item_id],
                        ['no_roll',$material_stock->nomor_roll],
                        [db::raw('upper(trim(no_packing_list))'),strtoupper($no_packing_list)],
                        [db::raw('upper(trim(no_invoice))'),strtoupper($no_invoice)],
                    ])
                    ->where(db::raw('upper(trim(po_supplier))'),'LIKE',"%$document_no%")
                    ->first();

                    if($data_erp)
                    {
                        $material_arrival->c_orderline_id           = $data_erp->c_orderline_id;
                        $material_arrival->save();

                        $remark_integration                         = $material_movement_line->remark_integration;
                        $material_movement_line->remark_integration = $remark_integration.', SUDAH DI BENERIN C_ORDER_LINE_ID NYA';


                        $material_movement_line->c_orderline_id     = $data_erp->c_orderline_id;
                        $material_movement_line->save();
                    }
                }
                
                DB::commit();
            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }

    private function updateRecalculateAutoAllocation()
    {

        $data           = AutoAllocation::where([
            ['item_code','NB11-TAP-NEBA38-CAVIAR-LEAD-00'],
            ['document_no','PONB2-2001-0014'],
        ])->get();

        foreach ($data as $key => $value) 
        {
            try
            {
                DB::beginTransaction();
                //return view('errors.503');
               
                $id                     = $value->id;
                $auto_allocation        = AutoAllocation::find($id);

                if($auto_allocation->status_po_buyer =='active' && !$auto_allocation->is_reduce)
                {
                    if($auto_allocation->is_fabric)
                    {
                        $qty_allocation         = sprintf('%0.8f',$auto_allocation->qty_allocation);
        
                        $qty_allocated  = AllocationItem::where([
                            ['auto_allocation_id',$id],
                        ])
                        ->sum('qty_booking');
        
                        $new_oustanding = sprintf('%0.8f',$qty_allocation - ($qty_allocated));
        
                        if($new_oustanding <= 0)
                        {
                            $auto_allocation->is_already_generate_form_booking  = true;
                            $auto_allocation->generate_form_booking             = carbon::now();
                        }else
                        {
                            $auto_allocation->is_already_generate_form_booking  = false;
                            $auto_allocation->generate_form_booking             = null;
                        }
                    
                        if ($new_oustanding == 0) $new_oustanding = '0';
                        else $new_oustanding = sprintf('%0.8f',$new_oustanding);
        
                        $auto_allocation->qty_allocated     = sprintf('%0.8f',($qty_allocated));
                        $auto_allocation->qty_outstanding   = sprintf('%0.8f',$new_oustanding);
                        $auto_allocation->save();
        
                    }else 
                    {
                        $qty_allocation         = sprintf('%0.8f',$auto_allocation->qty_allocation);
        
                        $qty_allocated  = MaterialPreparation::where([
                            ['auto_allocation_id',$id],
                            //['last_status_movement','!=','out-handover'],
                            ['last_status_movement','!=','reroute'],
                            ['last_status_movement','!=','adjustment'],
                            ['is_from_handover',false],
                        ])
                        ->sum(db::raw("(qty_conversion + COALESCE(qty_borrow,0) + COALESCE(qty_reject,0))"));
        
                        $new_oustanding = sprintf('%0.8f',$qty_allocation - ($qty_allocated));
        
                        if($new_oustanding <= 0)
                        {
                            $auto_allocation->is_already_generate_form_booking  = true;
                            $auto_allocation->generate_form_booking             = carbon::now();
                        }else
                        {
                            $auto_allocation->is_already_generate_form_booking  = false;
                            $auto_allocation->generate_form_booking             = null;
                        }
                    
                        if ($new_oustanding == 0) $new_oustanding = '0';
                        else $new_oustanding = sprintf('%0.8f',$new_oustanding);
        
                        $auto_allocation->qty_allocated     = sprintf('%0.8f',($qty_allocated));
                        $auto_allocation->qty_outstanding   = sprintf('%0.8f',$new_oustanding);
                        $auto_allocation->save();
        
                        
                    }
                }
               
                
                DB::commit();
            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }

    private function updateCorderLineIdReject()
    {
        $data           = DB::table('integration_movement_inventory_to_reject')
        ->get();

        foreach ($data as $key => $value) 
        {
            $material_movement_line     = MaterialMovementLine::find($value->material_movement_line_id);
            $material_stock_id          = $material_movement_line->material_stock_id;
            $material_preparation_id    = $material_movement_line->material_preparation_id;

            if($material_preparation_id)
            {
                $material_preparation   = MaterialPreparation::find($material_preparation_id);
                $po_detail_id           = $material_preparation->po_detail_id;

                $material_arrival           = MaterialArrival::whereNull('material_subcont_id')
                ->whereNull('material_roll_handover_fabric_id')
                ->where('po_detail_id',$po_detail_id)
                ->first();

                $material_movement_line->c_orderline_id = ($material_arrival? $material_arrival->c_orderline_id : '-');
                $material_movement_line->save();
            }else
            {
                $material_stock         = MaterialStock::find($material_stock_id);
                $po_detail_id           = $material_stock->po_detail_id;

                $material_arrival           = MaterialArrival::whereNull('material_subcont_id')
                ->whereNull('material_roll_handover_fabric_id')
                ->where('po_detail_id',$po_detail_id)
                ->first();

                $material_movement_line->c_orderline_id = ($material_arrival? $material_arrival->c_orderline_id : '-');
                $material_movement_line->save();
            }
        }
    }

    private function recalculateSummaryStockFab()
    {
        $data           =  DB::table('summary_vs_stock_v')->get();

        $insert_date    = Carbon::now();

        foreach ($data as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();

                $summary_stock_fabric = SummaryStockFabric::find($value->id);
       
                if($summary_stock_fabric && $value)
                {
                    $total_stock         = sprintf('%0.8f',$value->detail_total_stock);
                    $total_reserved_qty  = sprintf('%0.8f',$value->detail_total_reserved_qty);
                    $total_available_qty = sprintf('%0.8f',$value->detail_total_available_qty);
                    $total_arrival_qty   = sprintf('%0.8f',$value->detail_total_arrival_qty);
                    
                    $summary_stock_fabric->stock         = $total_stock;
                    $summary_stock_fabric->reserved_qty  = $total_reserved_qty;
                    $summary_stock_fabric->available_qty = $total_available_qty;
                    $summary_stock_fabric->qty_order     = $total_arrival_qty;
                    $summary_stock_fabric->save();
                }

                DB::commit();

            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    
    private function doMovementPerSize()
    {
        $data           = DB::table('outstanding_material_movement_per_size_v')
        ->where(db::raw("to_char(date_movement, 'YYYY')"),'2018')
        ->get();

        $insert_date    = Carbon::now();

        foreach ($data as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();

                $material_movement_line_id  = $value->id;;
                $material_movement_line     = MaterialMovementLine::find($material_movement_line_id);

                if($material_movement_line)
                {
                    $po_buyer       = ($material_movement_line->material_preparation_id ? $material_movement_line->materialPreparation->po_buyer : null);
                    $item_id        = ($material_movement_line->material_preparation_id ? $material_movement_line->materialPreparation->item_id : null);
                    $item_id_source = ($material_movement_line->material_preparation_id ? $material_movement_line->materialPreparation->item_id_source : null);
                    $qty_movement   = sprintf('%0.8f',$material_movement_line->qty_movement);
                    $created_at     = $material_movement_line->created_at;

                    if($po_buyer && $item_id)
                    {
                        $detail_material_requirements = MaterialRequirementPerSize::where([
                            ['po_buyer',$po_buyer],
                            ['item_id',$item_id],
                        ])
                        ->orderby('garment_size','asc')
                        ->orderby('total_qty','asc')
                        ->get();

                        $total_line = count($detail_material_requirements);
                        if($total_line > 0)
                        {
                            foreach ($detail_material_requirements as $key_line => $detail_material_requirement) 
                            {
                                $size                   = $detail_material_requirement->garment_size;
                                $total_qty              = sprintf('%0.8f',$detail_material_requirement->total_qty);
                                
                                $flag                   = 0;
                                $data_saldo_per_lines    =  DB::select(db::raw("SELECT * FROM get_outstanding_per_size(
                                    '".$po_buyer."',
                                    '".$item_id."'
                                    )"
                                ));
    
                                foreach ($data_saldo_per_lines as $key_1 => $data_saldo_per_line) 
                                {
                                    if($data_saldo_per_line->outstanding > 0) $flag++;
                                }
    
                                if($total_line == 1) $total_outstanding = sprintf('%0.8f',$qty_movement);
                                else
                                {
                                    $total_allocated    = MaterialMovementPerSize::where([
                                        ['size',$size],
                                        ['po_buyer',$po_buyer],
                                        ['item_id',$item_id],
                                    ])
                                    ->whereNull('reverse_date')
                                    ->sum('qty_per_size');
                                    
                                    if($flag == 1)$total_outstanding = sprintf('%0.8f',$qty_movement);
                                    else $total_outstanding = sprintf('%0.8f',$total_qty - $total_allocated);
                                }
                                
                                if($total_outstanding > 0 && $qty_movement >0)
                                {
                                    if ($qty_movement/$total_outstanding >= 1) $supplied = $total_outstanding;
                                    else  $supplied = $qty_movement;
    
                                    if($supplied > 0)
                                    {
                                        $is_exists              = MaterialMovementPerSize::where([
                                            ['material_movement_line_id',$material_movement_line_id],
                                            ['size',$size],
                                            ['po_buyer',$po_buyer],
                                            ['item_id',$item_id],
                                            ['qty_per_size',sprintf('%0.8f',$supplied)],
                                        ])
                                        ->whereNull('reverse_date')
                                        ->exists();
    
                                        if(!$is_exists)
                                        {
                                            MaterialMovementPerSize::FirstOrCreate([
                                                'material_movement_line_id' => $material_movement_line_id,
                                                'size'                      => $size,
                                                'po_buyer'                  => $po_buyer,
                                                'item_id'                   => $item_id,
                                                'item_id_source'            => $item_id_source,
                                                'qty_per_size'              => sprintf('%0.8f',$supplied),
                                                'reverse_date'              => null,
                                                'created_at'                => $created_at,
                                                'updated_at'                => $created_at,
                                                'integration_date'          => $created_at,
                                                'is_integrate'              => true,
                                            ]);
                                        }
    
                                        $qty_movement       -= $supplied;
                                        $total_outstanding  -= $supplied;
                                    }
                                }else if($total_outstanding == 0 && $flag == 0)
                                {
                                    $supplied = $qty_movement;
    
                                    if($supplied > 0)
                                    {
                                        $is_exists              = MaterialMovementPerSize::where([
                                            ['material_movement_line_id',$material_movement_line_id],
                                            ['size',$size],
                                            ['po_buyer',$po_buyer],
                                            ['item_id',$item_id],
                                            ['qty_per_size',sprintf('%0.8f',$supplied)],
                                        ])
                                        ->whereNull('reverse_date')
                                        ->exists();
        
                                        if(!$is_exists)
                                        {
                                            MaterialMovementPerSize::FirstOrCreate([
                                                'material_movement_line_id' => $material_movement_line_id,
                                                'size'                      => $size,
                                                'po_buyer'                  => $po_buyer,
                                                'item_id'                   => $item_id,
                                                'item_id_source'            => $item_id_source,
                                                'qty_per_size'              => sprintf('%0.8f',$supplied),
                                                'reverse_date'              => null,
                                                'created_at'                => $created_at,
                                                'updated_at'                => $created_at,
                                                'integration_date'          => $created_at,
                                                'is_integrate'              => true,
                                            ]);
                                        }
                                    }
                                    
    
                                    $qty_movement       -= $supplied;
                                }
                            }
    
                            $material_movement_line->inserted_to_material_movement_per_size_date = $insert_date;
                            $material_movement_line->save();
                        }else
                        {
                            // klo ga ketemu bom per itemnya pake globlan aja
                            $detail_material_requirements = MaterialRequirementPerSize::where('po_buyer',$po_buyer)
                            ->orderby('garment_size','asc')
                            ->orderby('total_qty','asc')
                            ->get();

                            foreach ($detail_material_requirements as $key_line => $detail_material_requirement) 
                            {
                                $size                   = $detail_material_requirement->garment_size;
                                $total_qty              = sprintf('%0.8f',$detail_material_requirement->total_qty);
                                
                                $flag                   = 0;
                                
                                if($total_line == 1) $total_outstanding = sprintf('%0.8f',$qty_movement);
                                else
                                {
                                    $total_allocated    = MaterialMovementPerSize::where([
                                        ['size',$size],
                                        ['po_buyer',$po_buyer],
                                        ['item_id',$item_id],
                                    ])
                                    ->whereNull('reverse_date')
                                    ->sum('qty_per_size');
                                    
                                    if($flag == 1)$total_outstanding = sprintf('%0.8f',$qty_movement);
                                    else $total_outstanding = sprintf('%0.8f',$total_qty - $total_allocated);
                                }
                                
                                if($total_outstanding > 0 && $qty_movement >0)
                                {
                                    if ($qty_movement/$total_outstanding >= 1) $supplied = $total_outstanding;
                                    else  $supplied = $qty_movement;
    
                                    if($supplied > 0)
                                    {
                                        $is_exists              = MaterialMovementPerSize::where([
                                            ['material_movement_line_id',$material_movement_line_id],
                                            ['size',$size],
                                            ['po_buyer',$po_buyer],
                                            ['item_id',$item_id],
                                            ['qty_per_size',sprintf('%0.8f',$supplied)],
                                        ])
                                        ->whereNull('reverse_date')
                                        ->exists();
    
                                        if(!$is_exists)
                                        {
                                            MaterialMovementPerSize::FirstOrCreate([
                                                'material_movement_line_id' => $material_movement_line_id,
                                                'size'                      => $size,
                                                'po_buyer'                  => $po_buyer,
                                                'item_id'                   => $item_id,
                                                'item_id_source'            => $item_id_source,
                                                'qty_per_size'              => sprintf('%0.8f',$supplied),
                                                'reverse_date'              => null,
                                                'created_at'                => $created_at,
                                                'updated_at'                => $created_at,
                                                'integration_date'          => $created_at,
                                                'is_integrate'              => true,
                                            ]);
                                        }
    
                                        $qty_movement       -= $supplied;
                                        $total_outstanding  -= $supplied;
                                    }
                                }else if($total_outstanding == 0 && $flag == 0)
                                {
                                    $supplied = $qty_movement;
    
                                    if($supplied > 0)
                                    {
                                        $is_exists              = MaterialMovementPerSize::where([
                                            ['material_movement_line_id',$material_movement_line_id],
                                            ['size',$size],
                                            ['po_buyer',$po_buyer],
                                            ['item_id',$item_id],
                                            ['qty_per_size',sprintf('%0.8f',$supplied)],
                                        ])
                                        ->whereNull('reverse_date')
                                        ->exists();
        
                                        if(!$is_exists)
                                        {
                                            MaterialMovementPerSize::FirstOrCreate([
                                                'material_movement_line_id' => $material_movement_line_id,
                                                'size'                      => $size,
                                                'po_buyer'                  => $po_buyer,
                                                'item_id'                   => $item_id,
                                                'item_id_source'            => $item_id_source,
                                                'qty_per_size'              => sprintf('%0.8f',$supplied),
                                                'reverse_date'              => null,
                                                'created_at'                => $created_at,
                                                'updated_at'                => $created_at,
                                                'integration_date'          => $created_at,
                                                'is_integrate'              => true,
                                            ]);
                                        }
                                    }
                                    
    
                                    $qty_movement       -= $supplied;
                                }
                            }
    
                            $material_movement_line->inserted_to_material_movement_per_size_date = $insert_date;
                            $material_movement_line->save();
                        }
                        
                    }
                    
                }

                DB::commit();

            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    
    private function updateSeason()
    {
        $data = MaterialArrival::where([
            ['is_subcont',false],
            ['is_ila',false],
        ])
        ->whereNull('season')
        ->whereNull('material_subcont_id')
        ->whereNull('material_roll_handover_fabric_id')
        ->get();

        foreach ($data as $key => $value) 
        {
            $web_po         = PurchaseOrderDetail::where('po_detail_id',$value->po_detail_id)->first();
            $value->season  = ($web_po ? $web_po->season : null);
            $value->save();
        }

        $data = MaterialStock::whereNull('season')->get();

        foreach ($data as $key => $value) 
        {
            $web_po         = PurchaseOrderDetail::where('po_detail_id',$value->po_detail_id)->first();
            $value->season  = ($web_po ? $web_po->season : null);
            $value->save();
        }
    }

    private function updateAutoAllocationSalahView()
    {
        $data = DB::select(db::raw("select id, lc_date,document_no,item_code,po_buyer,warehouse_id,qty_allocation,qty_outstanding
        From auto_allocations
        where (c_order_id,item_id_book,po_buyer,warehouse_id) in (
        select c_order_id,item_id_book,po_buyer,warehouse_id from auto_allocations
        where is_allocation_purchase = true
        and is_fabric = false
        GROUP BY c_order_id,item_id_book,po_buyer,warehouse_id
        having count(0) > 1) 
        and lc_Date = '2019-11-14'
        order by lc_date desc, document_no,item_code,po_buyer,warehouse_id asc,
        qty_allocation asc"));

        foreach ($data as $key => $value) 
        {
           $auto_allocation = AutoAllocation::find($value->id);
           $total_qty_allocation = AutoAllocation::where([
               ['lc_date',$value->lc_date],
               ['document_no',$value->document_no],
               ['item_code',$value->item_code],
               ['po_buyer',$value->po_buyer],
               ['warehouse_id',$value->warehouse_id],
               ['is_allocation_purchase',true],
           ])
           ->sum('qty_allocation');

           $auto_allocation->qty_allocation = $total_qty_allocation;
           $auto_allocation->qty_outstanding = $total_qty_allocation;
           $auto_allocation->save();
        }
        dd($data);
    }

    private function updateRereouteAllocation()
    {
        $data = DB::select(db::raw("select id,old_po_buyer,po_buyer,warehouse_id,document_no,item_code,item_code_book,qty_allocation,qty_outstanding,qty_allocated from auto_allocations
        where po_buyer in (
        select new_po_buyer 
        From reroute_po_buyers
        where new_po_buyer is not null
        GROUP BY new_po_buyer)
        and qty_outstanding != 0
        and deleted_at is null
        and is_fabric = false
        and qty_outstanding > 0
        and qty_allocated = 0
        and is_allocation_purchase = false"));

        foreach ($data as $key => $value) 
        {
            $old_auto_allocation_id = $value->id;
            $new_po_buyer           = $value->po_buyer;
            $old_po_buyer           = $value->old_po_buyer;
            $old_warehouse          = $value->warehouse_id;
            $old_document_no        = $value->document_no;
            $old_item_code          = $value->item_code_book;

            $old_preparations        = MaterialPreparation::where([
                ['po_buyer',$old_po_buyer],
                ['warehouse',$old_warehouse],
                ['document_no',$old_document_no],
                ['item_code',$old_item_code],
                ['auto_allocation_id',$old_auto_allocation_id],
            ])
            ->get();
            
            foreach ($old_preparations as $key => $old_preparation) 
            {
                $old_barcode            = explode('REROUTE_',$old_preparation->barcode); 
                $old_material_stock_id  = $old_preparation->material_stock_id;

                MaterialPreparation::where([
                    ['po_buyer',$new_po_buyer],
                    ['warehouse',$old_warehouse],
                    ['document_no',$old_document_no],
                    ['item_code',$old_item_code],
                    ['barcode',$old_barcode[1]],
                ])
                ->update([
                    'material_stock_id' => $old_material_stock_id,
                    'auto_allocation_id' => $old_auto_allocation_id,
                ]);
            }
           
        }
    }

    private function recalculateStockAcc()
    {

        $data = db::table('material_stock_minus_v')->get();
        $movement_date          = carbon::now();
        foreach ($data as $key => $value) 
        {
            try
            {
                DB::beginTransaction();
                //return view('errors.503');
                $id                     = $value->id; 
                $material_stock         = MaterialStock::find($id);
                $_stock                 = sprintf("%0.4f",$material_stock->stock);
                $type_stock             = $material_stock->type_stock;
                $type_stock_erp_code    = $material_stock->type_stock_erp_code;
                $po_buyer               = $material_stock->po_buyer;
                $warehouse_id           = $material_stock->warehouse_id;
                $po_detail_id           = $material_stock->po_detail_id;
                $old_available_qty      = sprintf("%0.4f",$material_stock->available_qty);
                $total_detail_stock     = sprintf("%0.4f",DetailMaterialStock::where('material_stock_id',$id)
                ->whereNotNull('approve_date_mm')
                ->whereNotNull('approve_date_accounting')
                ->sum('qty'));
    
                if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                {
                    $c_order_id             = 'FREE STOCK';
                    $no_packing_list        = '-';
                    $no_invoice             = '-';
                    $c_orderline_id         = '-';
                }else
                {
                    $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                    ->whereNull('material_roll_handover_fabric_id')
                    ->where('po_detail_id',$po_detail_id)
                    ->first();
                    
                    $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                    $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                    $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                }
    
                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();

                $inventory_erp = Locator::with('area')
                ->where([
                    ['is_active',true],
                    ['rack','ERP INVENTORY'],
                ])
                ->whereHas('area',function ($query) use($warehouse_id)
                {
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','ERP INVENTORY');
                })
                ->first();
    
                if($total_detail_stock != $_stock) $stock = $total_detail_stock;
                else $stock = $_stock;
    
                $total_allocation_item  = AllocationItem::where('material_stock_id',$id)
                ->whereNull('deleted_at')
                ->where(function($query){
                    $query->where('confirm_by_warehouse','approved')
                    ->orWhereNull('confirm_by_warehouse');
                })
                ->whereNotNull('mm_approval_date')
                ->whereNotNull('accounting_approval_date')
                ->sum('qty_booking');
    
                $total_preparation_item  = MaterialPreparation::where([
                    ['material_stock_id',$id],
                    ['last_status_movement','!=','reroute'],
                    ['warehouse',$warehouse_id],
                ])
                ->sum(db::raw("(qty_conversion + COALESCE(adjustment,0) + COALESCE(qty_borrow,0) + COALESCE(qty_reject,0))"));
    
                $new_available_qty = sprintf("%0.4f",$stock - ($total_allocation_item + $total_preparation_item ));
    
                if($new_available_qty <=0)
                {
                    $material_stock->is_allocated   = true;
                    $material_stock->is_active      = false;
                }else
                {
                    $material_stock->is_allocated   = false;
                    $material_stock->is_active      = true;
                }
    
                $locator_id = $material_stock->locator_id;
    
                if($locator_id)
                {
                    $count_item_on_locator = MaterialStock::where([
                        ['locator_id',$locator_id],
                        ['is_allocated',false],
                        ['is_closing_balance',false],
                    ])
                    ->whereNull('deleted_at')
                    ->count();
    
                    $locator = Locator::find($locator_id);
                    if($locator)
                    {
                        $locator->counter_in = $count_item_on_locator ;
                        $locator->save();
                    }
                }
    
                if ($new_available_qty == 0) $new_available_qty = '0';
                else $new_available_qty = sprintf("%0.4f",$new_available_qty);
    
                if($new_available_qty != $old_available_qty)
                {
                    $operator = sprintf("%0.4f",$new_available_qty - $old_available_qty);
                    
                    HistoryStock::approved($id
                    ,$new_available_qty
                    ,$old_available_qty
                    ,null
                    ,null
                    ,null
                    ,null
                    ,null
                    ,'RECALCULATE STOCK'
                    ,$system->name
                    ,$system->id
                    ,$type_stock_erp_code
                    ,$type_stock
                    ,false
                    ,false);
                } 
    
                $material_stock->stock          = sprintf("%0.4f",$stock);
                $material_stock->reserved_qty   = sprintf("%0.4f",($total_allocation_item + $total_preparation_item ));
                $material_stock->available_qty  = sprintf("%0.4f",$new_available_qty);
                $material_stock->save();
    
                DB::commit();
    
            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }

            
    }

    private function printBarcodePreparationFabric()
    {
        $data = DetailMaterialPreparationFabric::where('barcode','BELUM DIPRINT')->get();

        try
        {
            DB::beginTransaction();
            foreach ($data as $key => $barcode_preparation_fabric) 
            {
                if($barcode_preparation_fabric->barcode =='BELUM DIPRINT')
                {
                     $get_barcode            = $this->randomPreparationCode($barcode_preparation_fabric->materialStock->barcode_supplier);
                     $barcode                = $get_barcode->barcode;
                     $referral_code          = $get_barcode->referral_code;
                     $sequence               = $get_barcode->sequence;
                     
                     $_warehouse_id          = $barcode_preparation_fabric->materialStock->warehouse_id;
                     $is_additional          = $barcode_preparation_fabric->materialPreparationFabric->is_from_additional;
                     $planning_date          = $barcode_preparation_fabric->materialPreparationFabric->planning_date;
                     $qty_booking            = $barcode_preparation_fabric->reserved_qty;
                     
     
                     $inventory  = Locator::where('rack','INVENTORY')
                     ->whereHas('area',function ($query) use ($_warehouse_id)
                     {
                         $query->where('name','INVENTORY')
                         ->where('warehouse',$_warehouse_id);
                     })
                     ->first();
     
                     $relax = Locator::whereHas('area',function ($query) use ($_warehouse_id)
                     {
                         $query->where('name','RELAX')
                         ->where('warehouse',$_warehouse_id);
                     })
                     ->first();
     
                     if($is_additional == true) $note = 'DIGUNAKAN SEBANYAK '.$qty_booking.' UNTUK ADDITIONAL OLEH '.strtoupper($barcode_preparation_fabric->user->name);
                     else $note = ' DIGUNAKAN SEBANYAK '.$qty_booking.' UNTUK PCD '.$planning_date.' OLEH '.strtoupper($barcode_preparation_fabric->user->name);
     
     
                     MovementStockHistory::create([
                         'material_stock_id' => $barcode_preparation_fabric->material_stock_id,
                         'from_location'     => $inventory->id,
                         'to_destination'    => $relax->id,
                         'status'            => 'relax',
                         'uom'               => $barcode_preparation_fabric->uom,
                         'qty'               => $barcode_preparation_fabric->reserved_qty,
                         'movement_date'     => $barcode_preparation_fabric->created_at,
                         'note'              => $note.' ( NOMOR SERI BARCODE '.$barcode.')',
                         'user_id'           => $barcode_preparation_fabric->user_id
                     ]);
     
                     $barcode_preparation_fabric->barcode        = $barcode;
                     $barcode_preparation_fabric->referral_code  = $referral_code;
                     $barcode_preparation_fabric->sequence       = $sequence;
                     $barcode_preparation_fabric->save();
                }
            } 

            DB::commit();
            
        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function randomPreparationCode($barcode)
    {
        $sequence = Barcode::where('referral_code',$barcode)->max('sequence');

        if ($sequence == null) $sequence = 1;
        else $sequence += 1;
        
        Barcode::FirstOrCreate([
            'barcode'           => $barcode.'-'.$sequence,
            'referral_code'     => $barcode,
            'sequence'          => $sequence,
        ]);
        
        $obj                = new stdClass();
        $obj->barcode       = $barcode.'-'.$sequence;
        $obj->referral_code = $barcode;
        $obj->sequence      = $sequence;

        return $obj;

    }

    private function updateBarcodeRereoute()
    {
        $data = DB::select(db::raw("select * From material_preparations
        where barcode like 'REROUTE_REROUTE_%'"));

        foreach ($data as $key => $value) 
        {
            $id = $value->id;
            

            $material_preparation   = MaterialPreparation::find($id);
            $barcode    = explode('REROUTE_REROUTE_',$material_preparation->barcode); 

            echo $material_preparation->id.' '.'REROUTE_'.$barcode[1].' ';
            
            $material_preparation->barcode = 'REROUTE_'.$barcode[1];
            $material_preparation->save();
           
        }
    }

    private function moveAllocationIntoPreparation()
    {
        $allocations = AllocationItem::whereNotNull('auto_allocation_id')
        ->whereNull('material_preparation_id')
        ->whereIn('warehouse',['1000013','1000002'])
        ->get();

        $concatenate           = '';

        try 
        {
            DB::beginTransaction();

            foreach ($allocations as $key => $allocation) 
            {
                $material_stock_id  = $allocation->material_stock_id;
                $auto_allocation_id = $allocation->auto_allocation_id;
                $c_order_id         = $allocation->c_order_id;
                $c_bpartner_id      = $allocation->c_bpartner_id;
                $new_po_buyer       = $allocation->po_buyer;
                $item_id_book       = $allocation->item_id_book;
                $uom_stock          = $allocation->uom;
                $warehouse_id       = $allocation->warehouse;
                $style              = $allocation->style;
                $article_no         = $allocation->article_no;
                $job_order          = $allocation->job;
                $qty_booking        = $allocation->qty_booking;
                $_style             = $allocation->_style;
                $user               = $allocation->user_id;
                $confirm_date       = $allocation->created_at;

                $material_stock     = MaterialStock::find($material_stock_id);

                $po_detail_id       = $material_stock->po_detail_id;
                $supplier_name      = $material_stock->supplier_name;
                $document_no        = $material_stock->document_no;
                $item_code          = $material_stock->item_code;
                $item_desc          = $material_stock->item_desc;
                $category           = $material_stock->category;
                $uom_source         = ($material_stock->uom_source ? $material_stock->uom_source : $material_stock->uom);
                
                $material_arrival   = MaterialArrival::whereNull('material_subcont_id')
                ->whereNull('material_roll_handover_fabric_id')
                ->where([
                    ['po_detail_id',$po_detail_id],
                    ['warehouse_id',$warehouse_id],
                ])
                ->whereNull('material_subcont_id')
                ->first();

                $material_arrival_id  = ($material_arrival ? $material_arrival->id : null);

                $allocation_stock = Locator::with('area')
                ->whereHas('area',function ($query) use($warehouse_id)
                {
                    $query->where('is_destination',false);
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','FREE STOCK');
                })
                ->where('rack','ALLOCATION STOCK')
                ->first();

                $conversion = UomConversion::where([
                    ['item_code',$item_code],
                    ['uom_to',$uom_source]
                ])
                ->first();
            
                if($conversion)
                {
                    $multiplyrate   = $conversion->multiplyrate;
                    $dividerate     = $conversion->dividerate;
                }else
                {
                    $dividerate     = 1;
                    $multiplyrate   = 1;
                }
                
                $is_exists = MaterialPreparation::where([
                    ['material_stock_id',$material_stock_id],
                    ['auto_allocation_id',$auto_allocation_id],
                    ['c_order_id',$c_order_id],
                    ['c_bpartner_id',$c_bpartner_id],
                    ['po_buyer',$new_po_buyer],
                    ['item_id',$item_id_book],
                    ['uom_conversion',$uom_stock],
                    ['warehouse',$warehouse_id],
                    ['style',$style],
                    ['article_no',$article_no],
                    ['qty_conversion',$qty_booking],
                    ['is_backlog',false],
                ]);

                if($po_detail_id) $is_exists = $is_exists->where('po_detail_id',$po_detail_id);

                $is_exists = $is_exists->exists();

                if(!$is_exists)
                {
                    
                    $qty_reconversion       = sprintf('%0.8f',$qty_booking * $multiplyrate);
                    $material_preparation = MaterialPreparation::FirstOrCreate([
                        'material_stock_id'         => $material_stock_id,
                        'auto_allocation_id'        => $auto_allocation_id,
                        'po_detail_id'              => $po_detail_id,
                        'barcode'                   => 'BELUM DI PRINT',
                        'referral_code'             => 'BELUM DI PRINT',
                        'sequence'                  => 0,
                        'item_id'                   => $item_id_book,
                        'c_order_id'                => $c_order_id,
                        'c_bpartner_id'             => $c_bpartner_id,
                        'supplier_name'             => $supplier_name,
                        'document_no'               => $document_no,
                        'po_buyer'                  => $new_po_buyer,
                        'uom_conversion'            => $uom_stock,
                        'qty_conversion'            => $qty_booking,
                        'qty_reconversion'          => $qty_reconversion,
                        'job_order'                 => $job_order,
                        'style'                     => $style,
                        '_style'                    => $_style,
                        'article_no'                => $article_no,
                        'warehouse'                 => $warehouse_id,
                        'item_code'                 => $item_code,
                        'item_desc'                 => $item_desc,
                        'category'                  => $category,
                        'is_backlog'                => false,
                        'total_carton'              => 1,
                        'type_po'                   => 2,
                        'user_id'                   => $user,
                        'last_status_movement'      => 'allocation',
                        'is_need_to_be_switch'      => false,
                        'last_locator_id'           => $allocation_stock->id,
                        'last_movement_date'        => $confirm_date,
                        'created_at'                => $confirm_date,
                        'updated_at'                => $confirm_date,
                        'first_print_date'          => null,
                        'deleted_at'                => null,
                        'last_user_movement_id'     => $user,
                        'is_stock_already_created'  => false
                    ]);

                    $material_movement = MaterialMovement::FirstOrCreate([
                        'from_location'     => $allocation_stock->id,
                        'to_destination'    => $allocation_stock->id,
                        'is_active'         => true,
                        'po_buyer'          => $new_po_buyer,
                        'created_at'        => $confirm_date,
                        'updated_at'        => $confirm_date,
                        'status'            => 'allocation'
                    ]);

                    MaterialMovementLine::FirstOrCreate([
                        'material_movement_id'      => $material_movement->id,
                        'material_preparation_id'   => $material_preparation->id,
                        'item_code'                 => $item_code,
                        'type_po'                   => 2,
                        'qty_movement'              => $qty_booking,
                        'date_movement'             => $confirm_date,
                        'created_at'                => $confirm_date,
                        'updated_at'                => $confirm_date,
                        'is_active'                 => false,
                        'user_id'                   => $user,
                        'created_at'                => $confirm_date,
                        'updated_at'                => $confirm_date,
                    ]);

                    DetailMaterialPreparation::FirstOrCreate([
                        'material_preparation_id'   => $material_preparation->id,
                        'material_arrival_id'       => $material_arrival_id,
                        'user_id'                   => $user
                    ]);
                    
                    $concatenate .= "'" .$material_preparation->barcode."',";
                }

                $allocation->auto_allocation_id = null;
                $allocation->material_stock_id  = null;
                $allocation->deleted_at         = carbon::now();
                $allocation->save();
            }

            $concatenate = substr_replace($concatenate, '', -1);
            if($concatenate !='')
            {
                DB::select(db::raw("SELECT * FROM delete_duplicate_preparation_acc(array[".$concatenate."]);"));
                DB::select(db::raw("SELECT * FROM delete_duplicate_movement(array[".$concatenate."]);"));
            }
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    private function insertAdjustmentStockFabric()
    {

        $begining_stocks = DB::table('adjustment_stock_fabric_v')->get();
       
        try 
        {
            DB::beginTransaction();

            foreach ($begining_stocks as $key => $begining_stock) 
            {
                $c_order_id         = $begining_stock->c_order_id;
                $item_id            = $begining_stock->item_id_source;
                $item_code          = $begining_stock->item_code;
                $item_desc          = $begining_stock->item_desc;
                $warehouse_id       = $begining_stock->warehouse_id;
                $document_no        = $begining_stock->document_no;
                $c_bpartner_id      = $begining_stock->c_bpartner_id;
                $supplier_code      = $begining_stock->supplier_code;
                $supplier_name      = $begining_stock->supplier_name;
                $total_allocation   = AllocationItem::where([
                    ['c_order_id',$c_order_id],
                    ['item_id_source',$item_id],
                    ['c_bpartner_id',$c_bpartner_id],
                    ['warehouse',$warehouse_id],
                ])
                ->sum('qty_booking');

                $stock              = sprintf("%0.4f",$total_allocation);
                
                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();

                if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
                {
                    $mapping_stocks             = null;
                }else
                {
                    $get_4_digit_from_beginning = substr($document_no,0, 4);
                    $mapping_stocks             = MappingStocks::select('type_stock','type_stock_erp_code')->where('document_number',$get_4_digit_from_beginning)->first();

                    if($mapping_stocks)
                    {
                        $get_5_digit_from_beginning = substr($document_no,0, 5);
                        $mapping_stocks             = MappingStocks::select('type_stock','type_stock_erp_code')->where('document_number',$get_4_digit_from_beginning)->first();
                    }
                }

                if($mapping_stocks != null)
                {
                    $type_stock          = $mapping_stocks->type_stock;
                    $type_stock_erp_code = $mapping_stocks->type_stock_erp_code;
                }else
                {
                    $type_stock          = 'REGULER';
                    $type_stock_erp_code = '2';
                }

                $check_stock_erp = DB::connection('erp')
                ->table('bw_master_free_stock') 
                ->where([
                    ['c_order_id',$c_order_id],
                    ['item_id',$item_id],
                    ['c_bpartner_id',$c_bpartner_id],
                    ['warehouse_id',$warehouse_id],
                    ['type_stock',$type_stock_erp_code],
                ])
                ->first();

                if($check_stock_erp)
                {
                    $available_stock_on_erp = sprintf("%0.4f",$check_stock_erp->qty_available);
                    $new_stock              = sprintf("%0.4f",$stock + $available_stock_on_erp);

                    $check_material_stock = MaterialStock::where([
                        ['c_order_id',$c_order_id],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['document_no',$document_no],
                        ['item_code',$item_code],
                        ['warehouse_id',$warehouse_id],
                        ['is_stock_on_the_fly',true],
                        ['is_stock_mm',true],
                        ['stock',$new_stock],
                        ['type_stock_erp_code',$type_stock_erp_code],
                    ])
                    ->exists();
    
                    if(!$check_material_stock)
                    {
                        $material_stock = MaterialStock::FirstOrCreate([
                            'type_stock_erp_code'   => $type_stock_erp_code,
                            'type_stock'            => $type_stock,
                            'supplier_code'         => $supplier_code,
                            'supplier_name'         => $supplier_name,
                            'document_no'           => $document_no,
                            'c_bpartner_id'         => $c_bpartner_id,
                            'c_order_id'            => $c_order_id,
                            'item_id'               => $item_id,
                            'item_code'             => $item_code,
                            'item_desc'             => $item_desc,
                            'category'              => 'FB',
                            'type_po'               => 1,
                            'warehouse_id'          => $warehouse_id,
                            'uom'                   => 'YDS',
                            'qty_carton'            => 1,
                            'stock'                 => $new_stock,
                            'reserved_qty'          => 0,
                            'is_active'             => true,
                            'is_stock_on_the_fly'   => true,
                            'is_stock_mm'           => true,
                            'source'                => 'ADJ STOCK FAB',
                            'user_id'               => $system->id
                        ]);
    
                        $material_stock_id = $material_stock->id;
    
                        HistoryStock::approved($material_stock_id
                        ,$new_stock
                        ,$available_stock_on_erp
                        ,null
                        ,null
                        ,null
                        ,null
                        ,null
                        ,'ADJ STOCK FAB'
                        ,$system->name
                        ,$system->id
                        ,$type_stock_erp_code
                        ,$type_stock
                        ,false
                        ,false);

                        $allocations = AllocationItem::where([
                            ['c_order_id',$c_order_id],
                            ['c_bpartner_id',$c_bpartner_id],
                            ['warehouse',$warehouse_id],
                            ['item_id_source',$item_id],
                        ])
                        ->get();
        
                        $total_reserved = 0;
                        foreach ($allocations as $key => $allocation) 
                        {
                            $user_id                        = $allocation->user_id;
                            $user_name                      = $allocation->user->name;
                            $po_buyer                       = $allocation->po_buyer;
                            $style                          = $allocation->style;
                            $article_no                     = $allocation->article_no;
                            $lc_date                        = $allocation->lc_date;
        
                            $qty_booking                    = sprintf("%0.4f",$allocation->qty_booking);
                            
                            $old                            = $new_stock;
                            $new                            = $new_stock - $qty_booking;
                            
                            if($new <= 0) $new              = '0';
                            else $new                       = $new;
        
                            HistoryStock::approved($material_stock_id
                            ,$new
                            ,$old
                            ,$qty_booking
                            ,$po_buyer
                            ,$style
                            ,$article_no
                            ,$lc_date
                            ,'ALLOCATION WMS AWAL'
                            ,$user_name
                            ,$user_id
                            ,$type_stock_erp_code
                            ,$type_stock
                            ,false
                            ,true);
        
                            $new_stock         -= $qty_booking;
                            $total_reserved    += $qty_booking;
                            $allocation->material_stock_id  = $material_stock_id;
                            $allocation->save();
                        }
        
                        $material_stock->reserved_qty   = sprintf("%0.4f",$total_reserved);
                        $material_stock->available_qty  = sprintf("%0.4f",$new_stock);
                        $material_stock->save();
                        
                    }

                    
                }else
                {
                    $check_material_stock = MaterialStock::where([
                        ['c_order_id',$c_order_id],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['document_no',$document_no],
                        ['item_code',$item_code],
                        ['warehouse_id',$warehouse_id],
                        ['is_stock_on_the_fly',true],
                        ['is_stock_mm',true],
                        ['stock',$stock],
                        ['type_stock_erp_code',$type_stock_erp_code],
                    ])
                    ->exists();

                    if(!$check_material_stock)
                    {
                        $material_stock = MaterialStock::FirstOrCreate([
                            'type_stock_erp_code'   => $type_stock_erp_code,
                            'type_stock'            => $type_stock,
                            'supplier_code'         => $supplier_code,
                            'supplier_name'         => $supplier_name,
                            'document_no'           => $document_no,
                            'c_bpartner_id'         => $c_bpartner_id,
                            'c_order_id'            => $c_order_id,
                            'item_id'               => $item_id,
                            'item_code'             => $item_code,
                            'item_desc'             => $item_desc,
                            'category'              => 'FB',
                            'type_po'               => 1,
                            'warehouse_id'          => $warehouse_id,
                            'uom'                   => 'YDS',
                            'qty_carton'            => 1,
                            'stock'                 => $stock,
                            'reserved_qty'          => 0,
                            'is_active'             => true,
                            'is_stock_on_the_fly'   => true,
                            'is_stock_mm'           => true,
                            'source'                => 'ADJ STOCK FAB',
                            'user_id'               => $system->id
                        ]);
    
                        $material_stock_id = $material_stock->id;
    
                        HistoryStock::approved($material_stock_id
                        ,$stock
                        ,'0'
                        ,null
                        ,null
                        ,null
                        ,null
                        ,null
                        ,'ADJ STOCK FAB'
                        ,$system->name
                        ,$system->id
                        ,$type_stock_erp_code
                        ,$type_stock
                        ,false
                        ,false);

                        $allocations = AllocationItem::where([
                            ['c_order_id',$c_order_id],
                            ['c_bpartner_id',$c_bpartner_id],
                            ['warehouse',$warehouse_id],
                            ['item_id_source',$item_id],
                        ])
                        ->get();
        
                        $total_reserved = 0;
                        foreach ($allocations as $key => $allocation) 
                        {
                            $user_id                        = $allocation->user_id;
                            $user_name                      = $allocation->user->name;
                            $po_buyer                       = $allocation->po_buyer;
                            $style                          = $allocation->style;
                            $article_no                     = $allocation->article_no;
                            $lc_date                        = $allocation->lc_date;
        
                            $qty_booking                    = sprintf("%0.4f",$allocation->qty_booking);
                            
                            $old                            = $stock;
                            $new                            = $stock - $qty_booking;
                            
                            if($new <= 0) $new              = '0';
                            else $new                       = $new;
        
                            HistoryStock::approved($material_stock_id
                            ,$new
                            ,$old
                            ,$qty_booking
                            ,$po_buyer
                            ,$style
                            ,$article_no
                            ,$lc_date
                            ,'ALLOCATION WMS AWAL'
                            ,$user_name
                            ,$user_id
                            ,$type_stock_erp_code
                            ,$type_stock
                            ,false
                            ,true);
        
                            $stock             -= $qty_booking;
                            $total_reserved    += $qty_booking;
                            $allocation->material_stock_id  = $material_stock_id;
                            $allocation->save();
                        }
        
                        $material_stock->reserved_qty   = sprintf("%0.4f",$total_reserved);
                        $material_stock->available_qty  = sprintf("%0.4f",$stock);
                        $material_stock->save();
                    }
                }
            }
           
            
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }
    
    private function insertPoBuyerPerRoll()
    {
        $material_preparation_fabrics = MaterialPreparationFabric::whereNotNull('article_no')
        ->whereNotNull('_style')
        ->where([
            ['is_from_additional',false],
            ['total_qty_rilex','!=','0'],
        ])
        ->whereNull('deleted_at')
        ->get();
        
        try 
        {
            DB::beginTransaction();
            
            $detail_material_preparation_ids = array();
            foreach ($material_preparation_fabrics as $key => $material_preparation_fabric) 
            {
                $id             = $material_preparation_fabric->id;
                
                DetailPoBuyerPerRoll::whereIn('detail_material_preparation_fabric_id',function($query) use($id)
                {
                    $query->select('id')
                    ->from('detail_material_preparation_fabrics')
                    ->where('material_preparation_fabric_id',$id);
                })
                ->delete();
                
                $warehouse_id   = $material_preparation_fabric->warehouse_id;

                $system = User::where([
                    ['name','system'],
                    ['warehouse',$material_preparation_fabric->warehouse_id]
                ])
                ->first();
                
                $material_preparation_fabric_id     = $material_preparation_fabric->id;
                $document_no                        = $material_preparation_fabric->document_no;
                $c_bpartner_id                      = $material_preparation_fabric->c_bpartner_id;
                $planning_date                      = $material_preparation_fabric->planning_date;
                $warehouse_id                       = $material_preparation_fabric->warehouse_id;
                $article_no                         = $material_preparation_fabric->article_no;
                $item_id_book                       = $material_preparation_fabric->item_id_book;
                $item_id_source                     = $material_preparation_fabric->item_id_source;
                $is_piping                          = $material_preparation_fabric->is_piping;
                $_style                             = $material_preparation_fabric->_style;
                $is_from_additional                 = $material_preparation_fabric->is_from_additional;
                $item_code                          = strtoupper(trim($material_preparation_fabric->item_code));
                $movement_date                      = carbon::now()->toDateTimeString();
                $detail_material_preparation_ids    = array();
    
                $material_preparation_fabric_id     = $material_preparation_fabric->id;
                $document_no                        = $material_preparation_fabric->document_no;
                $c_bpartner_id                      = $material_preparation_fabric->c_bpartner_id;
                $planning_date                      = $material_preparation_fabric->planning_date;
                $warehouse_id                       = $material_preparation_fabric->warehouse_id;
                $article_no                         = $material_preparation_fabric->article_no;
                $item_id_book                       = $material_preparation_fabric->item_id_book;
                $item_id_source                     = $material_preparation_fabric->item_id_source;
                $is_piping                          = $material_preparation_fabric->is_piping;
                $_style                             = $material_preparation_fabric->_style;
                $is_from_additional                 = $material_preparation_fabric->is_from_additional;
                $item_code                          = strtoupper(trim($material_preparation_fabric->item_code));
                $movement_date                      = carbon::now()->toDateTimeString();
                $detail_material_preparation_ids    = array();

                if(!$is_from_additional)
                {
                    $detail_preparation_fabrics = DetailMaterialPreparationFabric::select('detail_material_preparation_fabrics.id as detail_material_preparation_fabric_id','material_stocks.load_actual','detail_material_preparation_fabrics.reserved_qty')
                    ->join('material_stocks','material_stocks.id','detail_material_preparation_fabrics.material_stock_id')
                    ->where('material_preparation_fabric_id',$material_preparation_fabric_id)
                    ->orderby('material_stocks.load_actual','asc')
                    ->get();

                                                
                    foreach ($detail_preparation_fabrics as $key => $detail_preparation_fabric) 
                    {
                        $reserved_qty                           = $detail_preparation_fabric->reserved_qty;
                        $detail_material_preparation_fabric_id  = $detail_preparation_fabric->detail_material_preparation_fabric_id;
                        $total_prepare                          = DetailPoBuyerPerRoll::where('detail_material_preparation_fabric_id',$detail_material_preparation_fabric_id)->sum('qty_booking');

                        $material_planning_fabrics = MaterialPlanningFabric::where([
                            ['article_no',$article_no],
                            ['is_piping',$is_piping],
                            ['item_id',$item_id_book],
                            ['warehouse_id',$warehouse_id],
                            ['planning_date',$planning_date]
                        ])
                        ->whereIn('id',function($query) use ($_style,$warehouse_id,$article_no,$planning_date,$document_no,$item_id_source,$item_id_book)
                        {
                            $query->select('material_planning_fabric_id')
                            ->from('detail_material_planning_fabrics')
                            ->where([
                                ['_style',$_style],
                                ['warehouse_id',$warehouse_id],
                                ['article_no',$article_no],
                                ['document_no',$document_no],
                                ['planning_date',$planning_date],
                                ['item_id_source',$item_id_source],
                                ['item_id_book',$item_id_book],
                            ])
                            ->groupby('material_planning_fabric_id');
                        })
                        ->whereNull('deleted_at')
                        ->orderby('article_no','asc')
                        ->orderby('style','asc')
                        ->get();
                    
                        $_reserved_qty = $reserved_qty - $total_prepare;
                        foreach ($material_planning_fabrics as $key => $material_planning_fabric) 
                        {
                            $material_planning_fabric_id        = $material_planning_fabric->id;
                            $po_buyer                           = $material_planning_fabric->po_buyer;
                            $planning_date                      = $material_planning_fabric->planning_date;
                            $style                              = $material_planning_fabric->style;
                            $job_order                          = $material_planning_fabric->job_order;
                            $article_no                         = $material_planning_fabric->article_no;
                            $qty_consumtion                     = $material_planning_fabric->qty_consumtion - $material_planning_fabric->qtyAllocated($material_planning_fabric_id);
                            $style_in_job_order                 = explode('::',$job_order)[0];
                            
                            if($qty_consumtion > 0 && $_reserved_qty > 0)
                            {
                                if ($_reserved_qty/$qty_consumtion >= 1) $_supplied = $qty_consumtion;
                                else  $_supplied = $_reserved_qty;

                                if($_supplied > 0)
                                {
                                    $material_requirement_per_parts = MaterialRequirementPerPart::where([
                                        ['article_no',$article_no],
                                        ['po_buyer',$po_buyer],
                                        ['is_piping',$is_piping],
                                        ['style',$style],
                                        ['item_id',$item_id_book],
                                    ])
                                    ->get();

                                    if(count($material_requirement_per_parts) > 0)
                                    {
                                        $__supplied = $_supplied;
                                        foreach ($material_requirement_per_parts as $key => $material_requirement_per_part) 
                                        {
                                            $part_no                            = $material_requirement_per_part->part_no;
                                            $total_preparation_per_part         = $material_requirement_per_part->getTotalPreparationFabric($material_requirement_per_part->po_buyer
                                                ,$material_requirement_per_part->item_id
                                                ,$material_requirement_per_part->style
                                                ,$material_requirement_per_part->article_no
                                                ,$material_requirement_per_part->part_no
                                                ,$material_requirement_per_part->is_piping);

                                            $qty_required_per_part              = sprintf("%0.4f",$material_requirement_per_part->qty_required - $total_preparation_per_part);

                                            if($qty_required_per_part > 0 && $__supplied > 0)
                                            {
                                                if ($__supplied/$qty_required_per_part >= 1) $_supplied_per_part = $qty_required_per_part;
                                                else  $_supplied_per_part = $__supplied;

                                                DetailPoBuyerPerRoll::FirstorCreate([
                                                    'material_planning_fabric_id'           => $material_planning_fabric_id,
                                                    'detail_material_preparation_fabric_id' => $detail_material_preparation_fabric_id,
                                                    'planning_date'                         => $planning_date,
                                                    'part_no'                               => $part_no,
                                                    'po_buyer'                              => $po_buyer,
                                                    'article_no'                            => $article_no,
                                                    'style'                                 => $style,
                                                    'style_in_job_order'                    => $style_in_job_order,
                                                    'job_order'                             => $job_order,
                                                    'qty_booking'                           => sprintf("%0.4f",$_supplied_per_part),
                                                    'warehouse_id'                          => $warehouse_id,
                                                    'user_id'                               => $system->id,
                                                    'created_at'                            => $movement_date,
                                                    'updated_at'                            => $movement_date,
                                                ]);

                                                $__supplied -= $_supplied_per_part;
                                            }
                                        }
                                    }else
                                    {
                                        DetailPoBuyerPerRoll::FirstorCreate([
                                            'material_planning_fabric_id'           => $material_planning_fabric_id,
                                            'detail_material_preparation_fabric_id' => $detail_material_preparation_fabric_id,
                                            'planning_date'                         => $planning_date,
                                            'po_buyer'                              => $po_buyer,
                                            'article_no'                            => $article_no,
                                            'style'                                 => $style,
                                            'style_in_job_order'                    => $style_in_job_order,
                                            'job_order'                             => $job_order,
                                            'qty_booking'                           => sprintf("%0.4f",$_supplied),
                                            'warehouse_id'                          => $warehouse_id,
                                            'user_id'                               => $system->id,
                                            'created_at'                            => $movement_date,
                                            'updated_at'                            => $movement_date,
                                        ]);
                                    }
                                
                                }

                                $_reserved_qty -= $_supplied;
                            }

                            $detail_material_preparation_ids [] = $detail_material_preparation_fabric_id;
                        }
                    }   
                }
            }
           
            $this->insertMovementPrepareIntegration($detail_material_preparation_ids);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function insertMovementPrepareIntegration($detail_material_preparation_ids)
    {
        $data = DetailMaterialPreparationFabric::where('is_status_prepare_inserted_to_material_preparation',false)
        ->whereIn('id',$detail_material_preparation_ids)
        ->get();
        
        $concatenate = '';
        try
        {
            db::beginTransaction();

            foreach ($data as $key => $value) 
            {
                $warehouse_id = $value->materialPreparationFabric->warehouse_id;

                $inventory = Locator::where('rack','INVENTORY')
                ->whereHas('area',function ($query) use ($warehouse_id)
                {
                    $query->where('name','INVENTORY')
                    ->where('warehouse',$warehouse_id);
                })
                ->first();

                $relax = Locator::whereHas('area',function ($query) use ($warehouse_id){
                    $query->where('name','RELAX')
                    ->where('warehouse',$warehouse_id);
                })
                ->first();
                
                $data_preparations = DetailPoBuyerPerRoll::select('detail_material_preparation_fabrics.preparation_date'
                ,'material_preparation_fabrics.document_no'
                ,'material_preparation_fabrics.item_code'
                ,'items.item_desc'
                ,'material_preparation_fabrics.c_bpartner_id'
                ,'suppliers.supplier_name'
                ,'material_stocks.po_detail_id'
                ,'material_stocks.c_order_id'
                ,'material_preparation_fabrics.planning_date'
                ,'material_preparation_fabrics.warehouse_id'
                ,'items.category'
                ,'items.uom'
                ,'items.item_id'
                ,'detail_material_preparation_fabrics.barcode'
                ,'detail_material_preparation_fabrics.referral_code'
                ,'detail_material_preparation_fabrics.user_id'
                ,'detail_material_preparation_fabrics.sequence'
                ,'detail_po_buyer_per_rolls.po_buyer'
                ,'detail_po_buyer_per_rolls.style'
                ,'detail_po_buyer_per_rolls.article_no'
                ,'detail_po_buyer_per_rolls.job_order'
                ,db::raw("sum(detail_po_buyer_per_rolls.qty_booking) as qty_booking")
                )
                ->join('detail_material_preparation_fabrics','detail_material_preparation_fabrics.id','detail_po_buyer_per_rolls.detail_material_preparation_fabric_id')
                ->leftjoin('material_preparation_fabrics','material_preparation_fabrics.id','detail_material_preparation_fabrics.material_preparation_fabric_id')
                ->leftjoin('material_stocks','material_stocks.id','detail_material_preparation_fabrics.material_stock_id')
                ->leftjoin('items','items.item_code','material_preparation_fabrics.item_code')
                ->leftjoin('po_suppliers',db::raw('upper(po_suppliers.document_no)'),db::raw('upper(material_preparation_fabrics.document_no)'))
                ->leftjoin('suppliers',db::raw('upper(suppliers.c_bpartner_id)'),db::raw('upper(material_preparation_fabrics.c_bpartner_id)'))
                ->where('detail_material_preparation_fabrics.id',$value->id)
                ->groupby('detail_material_preparation_fabrics.preparation_date'
                    ,'material_preparation_fabrics.document_no'
                    ,'material_preparation_fabrics.item_code'
                    ,'items.item_desc'
                    ,'material_preparation_fabrics.c_bpartner_id'
                    ,'suppliers.supplier_name'
                    ,'material_stocks.po_detail_id'
                    ,'material_stocks.c_order_id'
                    ,'material_preparation_fabrics.planning_date'
                    ,'material_preparation_fabrics.warehouse_id'
                    ,'items.category'
                    ,'items.uom'
                    ,'items.item_id'
                    ,'detail_material_preparation_fabrics.barcode'
                    ,'detail_material_preparation_fabrics.referral_code'
                    ,'detail_material_preparation_fabrics.user_id'
                    ,'detail_material_preparation_fabrics.sequence'
                    ,'detail_po_buyer_per_rolls.po_buyer'
                    ,'detail_po_buyer_per_rolls.style'
                    ,'detail_po_buyer_per_rolls.article_no'
                    ,'detail_po_buyer_per_rolls.job_order'
                )
                ->get();
                
                foreach ($data_preparations as $key_2 => $data_preparation) 
                {
                    $is_preparation_exists = MaterialPreparation::where([
                        ['barcode',$data_preparation->barcode.'-'.$data_preparation->po_buyer],
                        ['po_buyer',$data_preparation->po_buyer],
                        ['qty_conversion',sprintf('%0.8f',$data_preparation->qty_booking)],
                        ['warehouse',$data_preparation->warehouse_id]
                    ])
                    ->whereNull('cancel_planning')
                    ->exists();

                    if(!$is_preparation_exists)
                    {
                        $material_preparation = MaterialPreparation::FirstOrCreate([
                            'barcode'               => $data_preparation->barcode.'-'.$data_preparation->po_buyer,
                            'referral_code'         => $data_preparation->referral_code,
                            'sequence'              => $data_preparation->sequence,
                            'item_id'               => $data_preparation->item_id,
                            'c_order_id'            => $data_preparation->c_order_id,
                            'c_bpartner_id'         => $data_preparation->c_bpartner_id,
                            'supplier_name'         => $data_preparation->supplier_name,
                            'document_no'           => $data_preparation->document_no,
                            'po_buyer'              => $data_preparation->po_buyer,
                            'uom_conversion'        => $data_preparation->uom,
                            'qty_conversion'        => sprintf('%0.8f',$data_preparation->qty_booking),
                            'qty_reconversion'      => sprintf('%0.8f',$data_preparation->qty_booking),
                            'job_order'             => $data_preparation->job_order,
                            'style'                 => $data_preparation->style,
                            'article_no'            => $data_preparation->article_no,
                            'warehouse'             => $data_preparation->warehouse_id,
                            'item_code'             => $data_preparation->item_code,
                            'item_desc'             => $data_preparation->item_desc,
                            'category'              => $data_preparation->category,
                            'po_detail_id'          => $data_preparation->po_detail_id,
                            'cancel_planning'       => null,
                            'is_backlog'            => false,
                            'total_carton'          => 1,
                            'type_po'               => 1,
                            'planning_cutting_date' => $data_preparation->planning_date,
                            'user_id'               => $data_preparation->user_id,
                            'last_status_movement'  => 'relax',
                            'is_need_to_be_switch'  => false,
                            'last_locator_id'       => $relax->id,
                            'last_movement_date'    => $data_preparation->preparation_date,
                            'created_at'            => $data_preparation->preparation_date,
                            'updated_at'            => $data_preparation->preparation_date,
                            'last_user_movement_id' => $data_preparation->user_id
                        ]);
    
                        $is_movement_exists = MaterialMovement::where([
                            ['is_active',true],
                            ['is_integrate',false],
                            ['po_buyer',$data_preparation->po_buyer],
                            ['status','relax'],
                        ])
                        ->first();

                        if($is_movement_exists)
                        {
                            $is_movement_exists->updated_at = $data_preparation->preparation_date;
                            $is_movement_exists->save();

                            $material_movement_id = $is_movement_exists->id;
                        }else
                        {
                            $material_movement = MaterialMovement::FirstOrCreate([
                                'from_location'         => $inventory->id,
                                'to_destination'        => $relax->id,
                                'is_active'             => true,
                                'po_buyer'              => $data_preparation->po_buyer,
                                'status'                => 'relax',
                                'created_at'            => $data_preparation->preparation_date,
                                'updated_at'            => $data_preparation->preparation_date,
                            ]);

                            $material_movement_id = $material_movement->id;
                        }
                        
    
                        MaterialMovementLine::FirstOrCreate([
                            'material_movement_id'        => $material_movement_id,
                            'material_preparation_id'     => $material_preparation->id,
                            'item_id'                     => $data_preparation->item_id,
                            'item_code'                   => $data_preparation->item_code,
                            'type_po'                     => 1,
                            'qty_movement'                => sprintf('%0.8f',$data_preparation->qty_booking),
                            'date_movement'               => $data_preparation->preparation_date,
                            'created_at'                  => $data_preparation->preparation_date,
                            'updated_at'                  => $data_preparation->preparation_date,
                            'date_receive_on_destination' => $data_preparation->preparation_date,
                            'is_active'                   => true,
                            'user_id'                     => $data_preparation->user_id
                        ]);
    
                        DetailMaterialPreparation::FirstOrCreate([
                            'material_preparation_id'               => $material_preparation->id,
                            'detail_material_preparation_fabric_id' => $value->id,
                            'user_id'                               => $data_preparation->user_id
                        ]);
    
                        Temporary::Create([
                            'barcode'   => $data_preparation->po_buyer,
                            'status'    => 'mrp',
                            'user_id'   => $data_preparation->user_id
                        ]);
    
                        $concatenate .= "'" .$material_preparation->barcode."',";
                    }
                }

                $value->is_status_prepare_inserted_to_material_preparation = true;
                $value->save();
            }

            db::commit();
        }catch (Exception $ex){
            db::rollback();
            $message = $ex->getMessage();
        }

        $concatenate = substr_replace($concatenate, '', -1);
        if($concatenate !='')
        {
            DB::select(db::raw("SELECT * FROM delete_duplicate_preparation_acc(array[".$concatenate."]);"));
            DB::select(db::raw("SELECT * FROM delete_duplicate_movement(array[".$concatenate."]);"));
        }
    }

    private function updateCOrderIdMaterialSubcont()
    {
        
        $data = MaterialSubcont::whereNull('c_order_id')
        ->orWhereNull('supplier_name')
        ->get();

        foreach ($data as $key => $value) 
        {
            $document_no    = strtoupper($value->document_no);
            $po_supplier    = DB::connection('erp')
            ->table('wms_po_supplier')
            ->where(db::raw('upper(documentno)'),$document_no)
            ->first();

            $value->c_order_id      = ($po_supplier ? $po_supplier->c_order_id : null);
            $value->supplier_name   = ($po_supplier ? $po_supplier->supplier_name : null);
            $value->save();
        }
    }
    
    private function updateCOrderIdDetailMaterialPlanningFabric()
    {
        $data = DetailMaterialPlanningFabric::whereNull('c_order_id')->get();
        foreach ($data as $key => $value) 
        {
            $document_no = strtoupper($value->document_no);
            $po_supplier  = DB::connection('erp')
            ->table('wms_po_supplier')
            ->where(db::raw('upper(documentno)'),$document_no)
            ->first();

            $value->c_order_id = ($po_supplier ? $po_supplier->c_order_id : null);
            $value->save();
        }
    }

    private function updateTotalAllocationSOTF()
    {
        $data = DB::select(db::raw("SELECT * FROM get_total_auto_allocation();"));
        foreach ($data as $key => $value) 
        {
            $id                             = $value->id;
            $material_stock                 = MaterialStock::find($id);
            $total_allocation               = sprintf('%0.8f',$value->total_allocation);

            $material_stock->reserved_qty   = $total_allocation;
            $material_stock->save();
        }
    }

    private function updateCOrderIdMaterialPreparationFabric()
    {
        $data = MaterialPreparationFabric::whereNull('c_order_id')->get();
        foreach ($data as $key => $value) 
        {
            $document_no = strtoupper($value->document_no);
            $po_supplier  = DB::connection('erp')
            ->table('wms_po_supplier')
            ->where(db::raw('upper(documentno)'),$document_no)
            ->first();

            $value->c_order_id = ($po_supplier ? $po_supplier->c_order_id : null);
            $value->save();
        }
    }


    private function updateCOrderIdSummaryHandoverFabric()
    {
        $data = SummaryHandoverMaterial::whereNull('c_order_id')->get();
        foreach ($data as $key => $value) 
        {
            $document_no = strtoupper($value->document_no);
            $po_supplier  = DB::connection('erp')
            ->table('wms_po_supplier')
            ->where(db::raw('upper(documentno)'),$document_no)
            ->first();

            $value->c_order_id = ($po_supplier ? $po_supplier->c_order_id : null);
            $value->save();
        }
    }

    private function updateCOrderIdSummaryStockFabric()
    {
        $data = SummaryStockFabric::whereNull('c_order_id')->get();
        foreach ($data as $key => $value) 
        {
            $document_no = strtoupper($value->document_no);
            $po_supplier  = DB::connection('erp')
            ->table('wms_po_supplier')
            ->where(db::raw('upper(documentno)'),$document_no)
            ->first();

            $value->c_order_id = ($po_supplier ? $po_supplier->c_order_id : null);
            $value->save();
        }
    }

    private function updateCOrderIdAutoAllocation()
    {
        $data = AutoAllocation::whereNull('c_order_id')->get();
        foreach ($data as $key => $value) 
        {
            $document_no = strtoupper($value->document_no);
            $po_supplier  = DB::connection('erp')
            ->table('wms_po_supplier')
            ->where(db::raw('upper(documentno)'),$document_no)
            ->first();

            $value->c_order_id = ($po_supplier ? $po_supplier->c_order_id : null);
            $value->save();
        }
    }

    private function updateAutoAllocationVsPreparation()
    {
        
        $data  = db::table('accessories_allocation_vs_preparation')->get();

        foreach ($data as $key => $value) 
        {
            $auto_allocation        = AutoAllocation::find($value->id);

                if($auto_allocation->is_fabric)
                {
                    $qty_allocation         = sprintf("%0.4f",$auto_allocation->qty_allocation);

                    $qty_allocated  = AllocationItem::where([
                        ['auto_allocation_id',$value->id],
                    ])
                    ->sum('qty_booking');

                    $new_oustanding = sprintf("%0.4f",$qty_allocation - ($qty_allocated));

                    if($new_oustanding <= 0)
                    {
                        $auto_allocation->is_already_generate_form_booking  = true;
                        $auto_allocation->generate_form_booking             = carbon::now();
                    }else
                    {
                        $auto_allocation->is_already_generate_form_booking  = false;
                        $auto_allocation->generate_form_booking             = null;
                    }
                
                    if ($new_oustanding == 0) $new_oustanding = '0';
                    else $new_oustanding = sprintf("%0.4f",$new_oustanding);

                    $auto_allocation->qty_allocated     = sprintf("%0.4f",($qty_allocated));
                    $auto_allocation->qty_outstanding   = sprintf("%0.4f",$new_oustanding);
                    $auto_allocation->save();

                }else 
                {
                    $qty_allocation         = sprintf("%0.4f",$auto_allocation->qty_allocation);

                    $qty_allocated  = MaterialPreparation::where([
                        ['auto_allocation_id',$value->id],
                        //['last_status_movement','!=','out-handover'],
                        ['last_status_movement','!=','reroute'],
                        ['last_status_movement','!=','adjustment'],
                        ['is_from_handover',false],
                    ])
                    ->sum(db::raw("(qty_conversion + COALESCE(qty_borrow,0) + COALESCE(qty_reject,0))"));

                    $new_oustanding = sprintf("%0.4f",$qty_allocation - ($qty_allocated));

                    if($new_oustanding <= 0)
                    {
                        $auto_allocation->is_already_generate_form_booking  = true;
                        $auto_allocation->generate_form_booking             = carbon::now();
                    }else
                    {
                        $auto_allocation->is_already_generate_form_booking  = false;
                        $auto_allocation->generate_form_booking             = null;
                    }
                
                    if ($new_oustanding == 0) $new_oustanding = '0';
                    else $new_oustanding = sprintf("%0.4f",$new_oustanding);

                    $auto_allocation->qty_allocated     = sprintf("%0.4f",($qty_allocated));
                    $auto_allocation->qty_outstanding   = sprintf("%0.4f",$new_oustanding);
                    $auto_allocation->save();
                }
        }
    }

    private function updateTotalPreparationFabric()
    {
        $data = MaterialPreparationFabric::whereIn
         ('id',[
            '4b0ce640-699d-11ea-a1e3-c59a1b2dfd46',
            '56aa1110-699d-11ea-846f-87fb00ba17a0',
            '509e17f0-697f-11ea-a0f7-a58846918772',
            '599ea120-699d-11ea-b08b-9111a7805ebb',
            '5603e310-699d-11ea-8466-772661c9d130',
            '807687a0-6852-11ea-8ea7-8d8ccb5763ea',
            '5640d2d0-699d-11ea-9b04-ad7040064dc8',
            'd40bb110-6cb9-11ea-bcd4-b1035db803f2',
            '53e31900-699d-11ea-992f-6de6f635c043',
            'de828d00-6cb9-11ea-8679-638a4fdc656f',
            'f35551f0-68cb-11ea-b593-374bc92326dd',
            '4a7bc650-697f-11ea-b4ad-c120cd0af8e0',
            '4fd16d00-697f-11ea-88fd-cfd28fce99e4',
            '4f40d960-699d-11ea-bcb2-5f8d5c9a36b9',
            'e1a845a0-68cb-11ea-ba38-25576458603a',
            '47dc5700-699d-11ea-b89d-dd2638a6574c',
            'f2206250-68cb-11ea-b21f-7b670c4fbea2',
            'dc5b1160-6cb9-11ea-bbf9-a7f3deaff30e',
            '50b22fa0-697f-11ea-8b99-d5b68fe1654b',
            'd85908e0-6cb9-11ea-b8fd-4d0e4dc6907a',
            '491dfc80-699d-11ea-aed4-e1944136069c',
            '4e972c60-697f-11ea-b143-75cd3fd652b9',
            'e39dcc10-6cb9-11ea-a409-37875f0921fa'
        ])
        ->get();

        foreach ($data as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();
    
                FabricReportDailyMaterialPreparation::updateTotalPreparation($value->id);
                DB::commit();
    
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }

    private function updateOrderingJapanChina()
    {
        $data  = PoBuyer::select('po_buyer')->groupby('po_buyer')->get();

        foreach ($data as $key => $value) 
        {
            $erp = DB::connection('erp')
            ->table('wms_lc_buyers')
            ->where('poreference',$value->po_buyer)
            ->first();
            
            PoBuyer::where('po_buyer',$value->po_buyer)
            ->update([
                'is_ordering_for_japan_china' => ($erp ? $erp->is_japan_china : 0)
            ]);

            AutoAllocation::where('po_buyer',$value->po_buyer)
            ->update([
                'is_ordering_for_japan_china' => ($erp ? $erp->is_japan_china : 0)
            ]);
        }
    }

    

    private function updateCOrderIdAllocationItem()
    {
        $data = AllocationItem::whereNull('c_order_id')->get();
        foreach ($data as $key => $value) 
        {
            $document_no = strtoupper($value->document_no);
            $po_supplier  = DB::connection('erp')
            ->table('wms_po_supplier')
            ->where(db::raw('upper(documentno)'),'LIKE',"%$document_no%")
            ->first();

            $value->c_order_id = ($po_supplier ? $po_supplier->c_order_id : null);
            $value->save();
        }
    }

    private function storeMonitoringHandoverReceiving()
    {
        $arrivals = MaterialArrival::select('summary_handover_material_id')
        ->whereIn('id',['344a4a80-eb13-11e9-a27a-d9fa9a2be12e'])
        ->groupby('summary_handover_material_id')
        ->get();

        try 
        {
            DB::beginTransaction();
            
            foreach ($arrivals as $key => $arrival) 
            {
                $summary_handover_material_id       = $arrival->summary_handover_material_id;

                $monitoring                         = DB::select(db::raw("SELECT * FROM get_monitoring_receiving_fabric_handover_new('".$summary_handover_material_id."');"));
                $summary_handover_material          = SummaryHandoverMaterial::find($summary_handover_material_id);
                $no_pt                              = $summary_handover_material->handover_document_no;

                foreach ($monitoring as $key_2 => $value)
                {
                    $c_bpartner_id                  = $value->c_bpartner_id;
                    $arrival_date                   = $value->arrival_date;
                    $summary_handover_material_id   = $value->summary_handover_material_id;
                    $document_no                    = $value->document_no;
                    $c_order_id                     = $value->c_order_id;
                    $no_invoice                     = $value->no_invoice;
                    $item_id                        = $value->item_id;
                    $item_code                      = $value->item_code;
                    $no_packing_list                = $value->no_packing_list;
                    $supplier_name                  = $value->supplier_name;
                    $color                          = $value->color;
                    $warehouse_id                   = $value->warehouse_id;
                    $total_batch                    = $value->total_batch;
                    $total_roll                     = $value->total_roll;
                    $total_yard                     = $value->total_yard;
                    $user_receive                   = $value->user_receive;
                    $type_supplier                  = $value->type_supplier;

                    
                    $is_exists = MonitoringReceivingFabric::where([
                        [db::raw('upper(document_no)'),$document_no],
                        ['summary_handover_material_id',$summary_handover_material_id],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        [db::raw('upper(item_code)'),$item_code],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['warehouse_id',$warehouse_id],
                        ['is_handover',true],
                    ])
                    ->first();

                    if($is_exists)
                    {
                        $is_exists->arrival_date        = $arrival_date;
                        $is_exists->c_order_id          = $c_order_id;
                        $is_exists->item_id             = $item_id;
                        $is_exists->total_batch         = $total_batch;
                        $is_exists->total_roll          = $total_roll;
                        $is_exists->total_yard          = $total_yard;
                        $is_exists->user_receive        = $user_receive;
                        $is_exists->no_pt               = $no_pt;
                        $is_exists->jenis_po            = $type_supplier;
                        $is_exists->save();
                        
                        $monitoring_receiving_fabric_id = $is_exists->id;

                    }else
                    {
                        $monitoring_receiving_fabric    = MonitoringReceivingFabric::FirstOrCreate([
                            'summary_handover_material_id'          => $summary_handover_material_id,
                            'arrival_date'                          => $arrival_date,
                            'c_order_id'                            => $c_order_id,
                            'document_no'                           => $document_no,
                            'no_packing_list'                       => $no_packing_list,
                            'no_invoice'                            => $no_invoice,
                            'supplier_name'                         => $supplier_name,
                            'item_id'                               => $item_id,
                            'item_code'                             => $item_code,
                            'c_bpartner_id'                         => $c_bpartner_id,
                            'color'                                 => $color,
                            'warehouse_id'                          => $warehouse_id,
                            'total_batch'                           => $total_batch,
                            'total_roll'                            => $total_roll,
                            'total_yard'                            => $total_yard,
                            'user_receive'                          => $user_receive,
                            'is_handover'                           => true,
                            'jenis_po'                              => $type_supplier,
                            'no_pt'                                 => $no_pt
                        ]);

                        $monitoring_receiving_fabric_id = $monitoring_receiving_fabric->id;
                    }

                    MaterialStock::where('summary_handover_material_id',$summary_handover_material_id)
                    ->whereNotNull('summary_handover_material_id')
                    ->whereNotNull('material_roll_handover_fabric_id')
                    ->whereNull('monitoring_receiving_fabric_id')
                    ->update([
                        'monitoring_receiving_fabric_id' => $monitoring_receiving_fabric_id
                    ]);
                    
                }
            }

            DB::commit();
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    } 

    private function updateCOrderIdMonitoringRecevingFabric()
    {
        $data = MonitoringReceivingFabric::whereNull('c_order_id')->get();
        foreach ($data as $key => $value) 
        {
            $document_no = strtoupper($value->document_no);
            $po_supplier  = DB::connection('erp')
            ->table('wms_po_supplier')
            ->where(db::raw('upper(documentno)'),$document_no)
            ->first();

            $value->c_order_id = ($po_supplier ? $po_supplier->c_order_id : null);
            $value->save();
        }
    }

    private function updateStyleJobOrder()
    {
        //1. update _style in allocation items
        $allocations = AllocationItem::whereNull('_style')->get();
        foreach ($allocations as $key => $allocation) {
            $allocation->_style = explode('::',$allocation->job)[0];
            $allocation->save();
        }

        //2. update _style in allocation items
        $preparations = MaterialPreparation::whereNotNull('job_order')->whereNull('_style')->get();
        foreach ($preparations as $key => $preparation) {
            $preparation->_style = explode('::',$preparation->job_order)[0];
            $preparation->save();
        }
    }

    private function closingStock()
    {
        $material_stocks = MaterialStock::where([
            ['is_closing_balance',true],
            ['type_po',2]
        ])
        ->get();

        foreach ($material_stocks as $key => $material_stock) {
            $locator_id =$material_stock->locator_id;
            $locator = Locator::find($locator_id);
            $curr_available_qty = $material_stock->available_qty;
            $curr_reserved_qty = $material_stock->reserved_qty;

            $system = User::where([
                ['name','system'],
                ['warehouse',$material_stock->warehouse_id]
            ])
            ->first();

            AllocationItem::FirstOrCreate([
                'material_stock_id' => $material_stock->id,
                'document_no' => $material_stock->document_no,
                'item_code' => $material_stock->item_code,
                'item_desc' => $material_stock->item_desc,
                'category' => $material_stock->category,
                'uom' => $material_stock->uom,
                'warehouse' => $material_stock->warehouse_id,
                'qty_booking' => sprintf('%0.8f',$curr_available_qty),
                'is_not_allocation' => true,
                'user_id' => $system->id,
                'confirm_user_id' => $system->id,
                'confirm_by_warehouse' => 'approved',
                'confirm_date' => carbon::now(),
                'remark' => 'STOCK DI HAPUS',
            ]);

            $material_stock->reserved_qty = sprintf('%0.8f',$curr_available_qty + $curr_reserved_qty);
            $material_stock->available_qty = 0;
            $material_stock->is_allocated = true;
            $material_stock->is_closing_balance = true;
            $material_stock->is_active = false;
            $material_stock->save();
        }
    }

    private function updateTypeStockAutoAllocation()
    {
        $data = AutoAllocation::whereNull('type_stock')->get();
       
        foreach ($data as $key => $value){
            $_po_buyer = PoBuyer::where(db::raw('po_buyer'),$value->po_buyer)->first();

            if($_po_buyer){
                $type_stock_erp_code = $_po_buyer->type_stock_erp_code;
                if($type_stock_erp_code == 1) $type_stock = 'SLT';
                elseif($type_stock_erp_code == 2) $type_stock = 'REGULER';
                elseif($type_stock_erp_code == 3) $type_stock = 'PR/SR';
                elseif($type_stock_erp_code == 4) $type_stock = 'MTFC';
            }

            AutoAllocation::where('id',$value->id)
            ->update([
                'type_stock_erp_code' =>  $type_stock_erp_code,
                'type_stock' =>  $type_stock
             ]);

        }
    }

    private function updateItemBookAutoAllocation()
    {
        $data = AutoAllocation::whereNull('item_code_book')->get();
       
        foreach ($data as $key => $value){
            $pos = strpos($value->item_code, '|');
            if ($pos === false){
                $item_code_book = $value->item_code;
                $article_no = null;
            }else {
                $split = explode('|',$value->item_code);
                $item_code_book = $split[0];
                $article_no = $split[1];
            }
           
            AutoAllocation::where('id',$value->id)
            ->update([
                'item_code_book' =>  $item_code_book,
                'article_no' =>  $article_no
             ]);

        }
    }

    private function updateTypeStockPoSupplier()
    {
        //$data = AutoAllocation::get();
        $data = MaterialStock::whereNull('type_stock')->get();

        foreach ($data as $key => $value)
        {
            $document_no = $value->document_no;

            if($document_no == 'FREE STOCK')
            {
                $_mapping_stock         = MappingStocks::where('document_number',$document_no)->first();
                $mapping_stock_id       = $_mapping_stock->id;
                $type_stock_erp_code    = $_mapping_stock->type_stock_erp_code;
                $type_stock             = $_mapping_stock->type_stock;
            }else
            {
                $get_4_digit_from_beginning = substr($document_no,0, 4);
                $_mapping_stock_4_digt      = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();

                if($_mapping_stock_4_digt)
                {
                    $type_stock_erp_code    = $_mapping_stock_4_digt->type_stock_erp_code;
                    $type_stock             = $_mapping_stock_4_digt->type_stock;
                }else
                {
                    $get_5_digit_from_beginning = substr($document_no,0, 5);
                    $_mapping_stock_5_digt      = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                    
                    if($_mapping_stock_5_digt)
                    {
                        $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                        $type_stock             = $_mapping_stock_5_digt->type_stock;
                    }else
                    {
                        $type_stock_erp_code    = null;
                        $type_stock             = null;
                    }
                    
                }
            }

            MaterialStock::where('id',$value->id)
            ->update([
                'type_stock_erp_code' =>  $type_stock_erp_code,
                'type_stock' =>  $type_stock
             ]);

        }
    }

    private function updateTypeStockPoSupplierSummaryFabric()
    {
        //$data = AutoAllocation::get();
        $data = SummaryStockFabric::get();

        foreach ($data as $key => $value){
            $document_no = $value->document_no;
            if($document_no == 'FREE STOCK'){
                $_mapping_stock = MappingStocks::where('document_number',$document_no)->first();
                $mapping_stock_id = $_mapping_stock->id;
                $type_stock_erp_code = $_mapping_stock->type_stock_erp_code;
                $type_stock = $_mapping_stock->type_stock;
            }else{
                $get_4_digit_from_beginning = substr($document_no,0, 4);
                $_mapping_stock_4_digt = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                if($_mapping_stock_4_digt){
                    $type_stock_erp_code = $_mapping_stock_4_digt->type_stock_erp_code;
                    $type_stock = $_mapping_stock_4_digt->type_stock;
                }else{
                    $get_5_digit_from_beginning = substr($document_no,0, 5);
                    $_mapping_stock_5_digt = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                    if($_mapping_stock_5_digt){
                        $type_stock_erp_code = $_mapping_stock_5_digt->type_stock_erp_code;
                        $type_stock = $_mapping_stock_5_digt->type_stock;
                    }else{
                        $type_stock_erp_code = null;
                        $type_stock = null;
                    }
                    
                }
            }

            SummaryStockFabric::where('id',$value->id)
            ->update([
                'category_stock' =>  $type_stock
             ]);

        }
    }

    private function updateQtyArrival()
    {
        $material_stocks = db::select(db::raw("select * from testing_v"));

        foreach ($material_stocks as $key => $material_stock) 
        {
            $document_no        = $material_stock->document_no;
            $item_code          = $material_stock->item_code;
            $warehouse_id       = $material_stock->warehouse_id;
            $c_bpartner_id      = $material_stock->c_bpartner_id;
            $po_buyer           = $material_stock->po_buyer;
            $qty_arrival        = sprintf("%0.4f",$material_stock->stock);

            $sotfs           = MaterialStock::where([
                ['document_no',$document_no],
                ['item_code',$item_code],
                ['warehouse_id',$warehouse_id],
                ['c_bpartner_id',$c_bpartner_id],
                ['is_stock_on_the_fly',true],
            ]);

            if($po_buyer) $sotfs = $sotfs->where('po_buyer',$po_buyer);
            $sotfs = $sotfs->orderby('stock','asc')
            ->get();

            $total_line = count($sotfs);
            foreach ($sotfs as $key_line => $sotf) 
            {
                $_begining_stock    = sprintf("%0.4f",$sotf->stock);
                $available_qty      = ( $sotf->available_qty ? sprintf("%0.4f",$sotf->available_qty) : '0');
                
                if($total_line != ($key_line+1)) $begining_stock     = sprintf("%0.4f",$_begining_stock - $available_qty);
                else $begining_stock     = sprintf("%0.4f",$qty_arrival);
                
                
                if($begining_stock > 0)
                {
                    if ($qty_arrival/$begining_stock >= 1) $supplied = $begining_stock;
                    else  $supplied = $qty_arrival;

                    if($supplied > 0)
                    {
                        $sotf->available_qty = sprintf("%0.4f",$supplied);
                        $sotf->save();

                        $begining_stock -= $supplied;
                        $qty_arrival    -= $supplied;
                    }
                }
                
            }
        }
    }

    private function updateBrandPoBuyer()
    {
        $po_buyers = PoBuyer::get();

        foreach ($po_buyers as $key => $value) 
        {
            $po_buyer = DB::connection('erp') //dev_erp //erp
            ->table('wms_lc_buyers')
            ->select('kst_statisticaldate as statistical_date'
            ,'kst_statisticaldate2 as statistical_date_2'
            ,'ordertype as order_type'
            ,'brand as brand'
            ,'kst_season as season'
            ,'datepromised as promise_date')
            ->where('poreference',$value->po_buyer)
            ->first();

            if($po_buyer){
                if($po_buyer->statistical_date_2) $statistical_date = $po_buyer->statistical_date_2;
                else  $statistical_date = $po_buyer->statistical_date;

                if($po_buyer->brand == 'NEW BALANC') $brand ='NEW BALANCE';
                else $brand = $po_buyer->brand;

                if($po_buyer->order_type == 'MTF'){
                    $type_stock = 'MTFC';
                    $type_stock_erp_code = '4';
                }else if($po_buyer->order_type == 'SLT'){
                    $type_stock = 'SLT';
                    $type_stock_erp_code = '1';
                }else if($po_buyer->order_type == 'SR'){
                    $type_stock = 'SR';
                    $type_stock_erp_code = '3';
                }else{
                    if($brand == 'NEW BALANCE'){
                        $type_stock = null;
                        $type_stock_erp_code = null;
                    }else{
                        $type_stock = 'REGULER';
                        $type_stock_erp_code = '2';
                    }
                }
                
                if($statistical_date && ( $value->statistical_date == '' || $value->statistical_date == null)){
                    $value->statistical_date = $statistical_date;
                    $value->requirement_date = null;
                }
                   
                $value->promise_date = $po_buyer->promise_date;
                $value->season = $po_buyer->season;
                $value->brand = $brand;
                $value->type_stock = $type_stock;
                $value->type_stock_erp_code = $type_stock_erp_code;
                $value->save();
            }
        }
    }

    private function insertBarcode()
    {
        $material_preparations = MaterialPreparation::whereNotNull('referral_code')
        ->whereNotIn('barcode',function($query){
            $query->select('barcode')
            ->from('barcodes')
            ->groupby('barcode');
        })
        ->get();
        
        foreach ($material_preparations as $key => $value) {
            Barcode::FirstOrCreate([
                'barcode' => $value->barcode,
                'referral_code' => $value->referral_code,
                'sequence' => $value->sequence
            ]);
        }

        $material_handovers = MaterialRollHandoverFabric::whereNotNull('referral_code')
        ->whereNotIn('barcode',function($query){
            $query->select('barcode')
            ->from('barcodes')
            ->groupby('barcode');
        })
        ->get();
        
        foreach ($material_handovers as $key => $value) {
            Barcode::FirstOrCreate([
                'barcode' => $value->barcode,
                'referral_code' => $value->referral_code,
                'sequence' => $value->sequence
            ]);
        }

        $material_stocks = MaterialStock::whereNotNull('referral_code')
        ->whereNotIn('barcode_supplier',function($query){
            $query->select('barcode')
            ->from('barcodes')
            ->groupby('barcode');
        })
        ->get();
        
        foreach ($material_stocks as $key => $value) {
            Barcode::FirstOrCreate([
                'barcode' => $value->barcode_supplier,
                'referral_code' => $value->referral_code,
                'sequence' => $value->sequence
            ]);
        }

        $details = DetailMaterialPreparationFabric::whereNotNull('referral_code')
        ->whereNotIn('barcode',function($query){
            $query->select('barcode')
            ->from('barcodes')
            ->groupby('barcode');
        })
        ->get();
        
        foreach ($details as $key => $value) {
            Barcode::FirstOrCreate([
                'barcode' => $value->barcode_supplier,
                'referral_code' => $value->referral_code,
                'sequence' => $value->sequence
            ]);
        }
    }

    private function deleteDuplicatePurchase()
    {
        
        DB::select(db::raw("SELECT * FROM delete_duplicate_purchase_item();"));
    }

    private function updateJobOrder()
    {
        $details = DetailPoBuyerPerRoll::where('style_in_job_order','like',"%-%")
        ->get();
        
        foreach ($details as $key => $detail) {
            $job_order = $detail->job_order;
            $style_in_job_order = explode('::',$job_order)[0];

            $detail->style_in_job_order = $style_in_job_order;
            $detail->save();
        }
    }

    private function updateSequenceBarcode()
    {
        $data = Barcode::select('referral_code')
        ->where('barcode','like',"2I%")
        ->where('barcode','not like',"%-%")
        ->where('barcode','not like',"% %")
        ->where(db::raw('length(substr(barcode,0, length(barcode)))'),'<','9')
        ->groupby('referral_code')
        ->orderby('referral_code','desc')
        ->get();

        foreach ($data as $key => $value) 
        {
            try{
                $count = Barcode::where('referral_code',$value->referral_code)->count();
                for ($i=0; $i < $count; $i++) 
                { 
                    $barcode = Barcode::where([
                        ['referral_code',$value->referral_code],
                        ['fixing',false],
                    ])
                    ->first();
                  
                    $barcode->barcode = $value->referral_code.($i+1);
                    $barcode->sequence = ($i+1);
                    $barcode->fixing = true;
                    $barcode->save();
                }
            }catch(Exception $e){

                dd($e->getMessage());
            }
            
        }
    }
    
    private function updateMonitoringReceivingFabric()
    {
        /*$material_stocks = MaterialStock::select('material_arrival_id')
        ->whereNull('monitoring_receiving_fabric_id')
        ->whereNotNull('material_roll_handover_fabric_id')
        ->get()
        ->toArray();*/

        $arrivals = MaterialArrival::select('document_no','no_invoice','no_packing_list','item_id','c_bpartner_id','warehouse_id')
        ->whereNull('summary_handover_material_id')
        ->whereNull('material_roll_handover_fabric_id')
        ->wherein('id',['eec45980-2cb4-11ec-8f01-890f2196882b','f14dacc0-2cb4-11ec-bc3d-1fb1dc924511'])
        ->where('type_po','1')
        ->groupby('document_no','no_invoice','item_id','no_packing_list','c_bpartner_id','warehouse_id')
        ->get();
        // dd($arrivals);

        try 
        {
            DB::beginTransaction();

            foreach ($arrivals as $key => $arrival) 
            {
                $_no_packing_list       = ($arrival->no_packing_list ? $arrival->no_packing_list : '1');
                $_no_invoice            = ($arrival->no_invoice ?  $arrival->no_invoice : '1');
                $_document_no           = $arrival->document_no;
                $_item_id               = $arrival->item_id;
                $_c_bpartner_id         = $arrival->c_bpartner_id;
                $_warehouse_id          = $arrival->warehouse_id;

                $monitoring          = DB::select(db::raw("SELECT * FROM get_monitoring_receiving_fabric_new(
                    '".$_no_packing_list."',
                    '".$_no_invoice."',
                    '".$_document_no."',
                    '".$_c_bpartner_id."',
                    '".$_item_id."',
                    '".$_warehouse_id."'
                );"));
                // echo $monitoring;

                foreach ($monitoring as $key_2 => $value)
                {
                    $c_bpartner_id                  = $value->c_bpartner_id;
                    $arrival_date                   = $value->arrival_date;
                    $po_detail_id                   = $value->po_detail_id;
                    $document_no                    = $value->document_no;
                    $no_invoice                     = $value->no_invoice;
                    $item_id                        = $value->item_id;
                    $item_code                      = $value->item_code;
                    $no_packing_list                = $value->no_packing_list;
                    $supplier_name                  = $value->supplier_name;
                    $color                          = $value->color;
                    $warehouse_id                   = $value->warehouse_id;
                    $total_batch                    = $value->total_batch;
                    $total_roll                     = $value->total_roll;
                    $total_yard                     = $value->total_yard;
                    $user_receive                   = $value->user_receive;
                    $type_supplier                  = $value->type_supplier;

                    $is_exists = MonitoringReceivingFabric::where([
                        [db::raw('upper(document_no)'),$document_no],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        [db::raw('upper(item_code)'),$item_code],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['warehouse_id',$warehouse_id],
                        ['is_handover',false],
                    ])
                    ->first();

                    // dd($is_exists);

                    if($is_exists)
                    {
                        $is_exists->po_detail_id    = $po_detail_id;
                        $is_exists->arrival_date    = $arrival_date;
                        $is_exists->total_batch     = $total_batch;
                        $is_exists->total_roll      = $total_roll;
                        $is_exists->item_id         = $item_id;
                        $is_exists->total_yard      = $total_yard;
                        $is_exists->user_receive    = $user_receive;
                        $is_exists->jenis_po        = $type_supplier;
                        $is_exists->save();

                        $monitoring_receiving_fabric_id = $is_exists->id;

                    }else
                    {
                        
                        $monitoring_receiving_fabric = MonitoringReceivingFabric::FirstOrCreate([
                            'po_detail_id'          => $po_detail_id,
                            'arrival_date'          => $arrival_date,
                            'document_no'           => $document_no,
                            'no_packing_list'       => $no_packing_list,
                            'no_invoice'            => $no_invoice,
                            'supplier_name'         => $supplier_name,
                            'item_code'             => $item_code,
                            'item_id'               => $item_id,
                            'c_bpartner_id'         => $c_bpartner_id,
                            'color'                 => $color,
                            'warehouse_id'          => $warehouse_id,
                            'total_batch'           => $total_batch,
                            'total_roll'            => $total_roll,
                            'total_yard'            => $total_yard,
                            'user_receive'          => $user_receive,
                            'jenis_po'              => $type_supplier
                        ]);

                        $monitoring_receiving_fabric_id = $monitoring_receiving_fabric->id;
                    }

                    MaterialStock::where([
                        ['document_no',$document_no],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        ['item_code',$item_code],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['warehouse_id',$warehouse_id],
                    ]) 
                    ->where('type_po','1')
                    ->whereNull('material_roll_handover_fabric_id')
                    ->whereNull('monitoring_receiving_fabric_id')
                    ->update([
                        'monitoring_receiving_fabric_id' => $monitoring_receiving_fabric_id
                    ]);
                }
            }

            DB::commit();
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    }

    private function updateMonitoringHandoverFabric()
    {
        $arrivals = MaterialArrival::select('summary_handover_material_id')
        ->where('type_po','1')
        ->whereNotNull('summary_handover_material_id')
        ->whereNotNull('material_roll_handover_fabric_id')
        ->groupby('summary_handover_material_id')
        ->get();

        try 
        {
            DB::beginTransaction();
            
            foreach ($arrivals as $key => $arrival) 
            {
                $summary_handover_material_id       = $arrival->summary_handover_material_id;

                $monitoring                         = DB::select(db::raw("SELECT * FROM get_monitoring_receiving_fabric_handover_new('".$summary_handover_material_id."');"));
                $summary_handover_material          = SummaryHandoverMaterial::find($summary_handover_material_id);
                $no_pt                              = $summary_handover_material->handover_document_no;

                foreach ($monitoring as $key_2 => $value)
                {
                    $c_bpartner_id                  = $value->c_bpartner_id;
                    $arrival_date                   = $value->arrival_date;
                    $summary_handover_material_id   = $value->summary_handover_material_id;
                    $document_no                    = $value->document_no;
                    $no_invoice                     = $value->no_invoice;
                    $item_id                        = $value->item_id;
                    $item_code                      = $value->item_code;
                    $no_packing_list                = $value->no_packing_list;
                    $supplier_name                  = $value->supplier_name;
                    $color                          = $value->color;
                    $warehouse_id                   = $value->warehouse_id;
                    $total_batch                    = $value->total_batch;
                    $total_roll                     = $value->total_roll;
                    $total_yard                     = $value->total_yard;
                    $user_receive                   = $value->user_receive;
                    $type_supplier                  = $value->type_supplier;

                    
                    $is_exists = MonitoringReceivingFabric::where([
                        [db::raw('upper(document_no)'),$document_no],
                        ['summary_handover_material_id',$summary_handover_material_id],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        [db::raw('upper(item_code)'),$item_code],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['warehouse_id',$warehouse_id],
                        ['is_handover',true],
                    ])
                    ->first();

                    if($is_exists)
                    {
                        $is_exists->arrival_date        = $arrival_date;
                        $is_exists->item_id             = $item_id;
                        $is_exists->total_batch         = $total_batch;
                        $is_exists->total_roll          = $total_roll;
                        $is_exists->total_yard          = $total_yard;
                        $is_exists->user_receive        = $user_receive;
                        $is_exists->no_pt               = $no_pt;
                        $is_exists->jenis_po            = $type_supplier;
                        $is_exists->save();
                        $monitoring_receiving_fabric_id = $is_exists->id;

                    }else
                    {
                        $monitoring_receiving_fabric    = MonitoringReceivingFabric::FirstOrCreate([
                            'summary_handover_material_id'          => $summary_handover_material_id,
                            'arrival_date'                          => $arrival_date,
                            'document_no'                           => $document_no,
                            'no_packing_list'                       => $no_packing_list,
                            'no_invoice'                            => $no_invoice,
                            'supplier_name'                         => $supplier_name,
                            'item_id'                               => $item_id,
                            'item_code'                             => $item_code,
                            'c_bpartner_id'                         => $c_bpartner_id,
                            'color'                                 => $color,
                            'warehouse_id'                          => $warehouse_id,
                            'total_batch'                           => $total_batch,
                            'total_roll'                            => $total_roll,
                            'total_yard'                            => $total_yard,
                            'user_receive'                          => $user_receive,
                            'is_handover'                           => true,
                            'jenis_po'                              => $type_supplier,
                            'no_pt'                                 => $no_pt
                        ]);

                        $monitoring_receiving_fabric_id = $monitoring_receiving_fabric->id;
                    }

                    MaterialStock::where('summary_handover_material_id',$summary_handover_material_id)
                    ->whereNotNull('summary_handover_material_id')
                    ->whereNotNull('material_roll_handover_fabric_id')
                    ->whereNull('monitoring_receiving_fabric_id')
                    ->update([
                        'monitoring_receiving_fabric_id' => $monitoring_receiving_fabric_id
                    ]);
                    
                }
            }

            DB::commit();
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    }

    private function insertAllocationClosingBalance()
    {
        $data = MaterialStock::where([
            ['is_closing_balance',true],
            ['available_qty','>',0],
        ])
        ->get();

        try 
        {
    		DB::beginTransaction();
            

            foreach ($data as $key => $value) 
            {
                $cur_reserved_qty   = sprintf('%0.8f',$value->reserved_qty);
                $curr_available_qty = sprintf('%0.8f',$value->available_qty);

                AllocationItem::FirstOrCreate([
                    'material_stock_id'         => $value->id,
                    'document_no'               => $value->document_no,
                    'item_code'                 => $value->item_code,
                    'item_desc'                 => $value->item_desc,
                    'category'                  => $value->category,
                    'uom'                       => $value->uom,
                    'warehouse'                 => $value->warehouse_id,
                    'qty_booking'               => $curr_available_qty,
                    'is_not_allocation'         => true,
                    'user_id'                   => $value->user_id,
                    'confirm_user_id'           => $value->user_id,
                    'confirm_by_warehouse'      => 'approved',
                    'created_at'                => $value->deleted_at,
                    'updated_at'                => $value->deleted_at,
                    'confirm_date'              => $value->deleted_at,
                    'remark'                    => 'STOCK DI HAPUS, REQUEST BY USER',
                ]);

                $value->reserved_qty = sprintf('%0.8f',$cur_reserved_qty + $curr_available_qty);
                $value->available_qty = 0;
                $value->save();
            }


            DB::commit();
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
        
    }

    private function insertDetailAllocation()
    {
        $data = db::table('detail_stock_vs_stock')->get();

        try 
        {
    		DB::beginTransaction();
            

            foreach ($data as $key => $value) 
            {
                $diff_qty           = sprintf('%0.8f',-1*$value->available_qty);
                $material_stock     = MaterialStock::find($value->id);
                $material_stock_id  = $material_stock->id;
                $created_at         = $material_stock->created_at;
                $warehouse_id       = $material_stock->warehouse_id;

                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();

                DetailMaterialStock::FirstOrCreate([
                    'material_stock_id'     => $material_stock_id,
                    'remark'                => 'adjustment system',
                    'uom'                   => $material_stock->uom,
                    'qty'                   => $diff_qty,
                    'locator_id'            => $material_stock->locator_id,
                    'user_id'               => $system->id,
                    'created_at'            => $created_at,
                    'updated_at'            => $created_at,
                ]);
                
            }


            DB::commit();
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
        
    }

    private function CuttingInstructionHeaderReport()
    {

        $material_planning_fabrics = MaterialPlanningFabric::select(
            db::raw("substr(job_order,0,POSITION ( ':' IN job_order )) AS style")
            ,'article_no'
            ,'planning_date'
            ,'uom'
            ,'warehouse_id'
            ,db::raw("string_agg ( distinct item_code, ',' ) AS item_code")
            ,db::raw("string_agg ( distinct po_buyer, ',' ) AS po_buyer")
            ,db::raw("sum(qty_consumtion) as qty_need"))
        ->whereBetween('planning_date',['2019-01-01','2019-08-31'])
        ->groupby(db::raw("substr(job_order,0,POSITION ( ':' IN job_order ))")
            ,'uom'
            ,'article_no'
            ,'warehouse_id'
            ,'planning_date')
        ->get();
        
        try 
        {
            DB::beginTransaction();

           

            $movement_date = carbon::now()->toDateTimeString();
            foreach ($material_planning_fabrics as $key => $material_planning_fabric) 
            {
                $qty_need       = sprintf("%.5f",$material_planning_fabric->qty_need);
                $style          = $material_planning_fabric->style;
                $article_no     = $material_planning_fabric->article_no;
                $uom            = $material_planning_fabric->uom;
                $po_buyer       = $material_planning_fabric->po_buyer;
                $item_code      = $material_planning_fabric->item_code;
                $warehouse_id   = $material_planning_fabric->warehouse_id;
                $planning_date  = $material_planning_fabric->planning_date;
                
                CuttingInstruction::where([
                    ['planning_date',$planning_date],
                    ['style',$style],
                    ['article_no',$article_no],
                    ['warehouse_id',$warehouse_id],
                ])
                ->delete();

                $is_exists = CuttingInstruction::where([
                    ['article_no',$article_no],
                    ['style',$style],
                    ['uom',$uom],
                    ['item_code',$item_code],
                    ['planning_date',$planning_date],
                    ['po_buyer',$po_buyer],
                    ['qty_need',$qty_need],
                    ['warehouse_id',$warehouse_id],
                ])
                ->exists();

                if(!$is_exists)
                {
                    CuttingInstruction::FirstOrCreate([
                        'created_at'        => $movement_date,
                        'updated_at'        => $movement_date,
                        'article_no'        => $article_no,
                        'uom'               => $uom,
                        'style'             => $style,
                        'item_code'         => $item_code,
                        'planning_date'     => $planning_date,
                        'po_buyer'          => $po_buyer,
                        'qty_need'          => $qty_need,
                        'warehouse_id'      => $warehouse_id,
                    ]);
                }
                
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    private function reAllocationStock()
    {
        $material_cancel_allocations = db::table('cancel_allocation_v')
                                      //->where('po_detail_id', '1713667')
                                      ->get();
        
        foreach ($material_cancel_allocations as $key => $material_cancel_allocation) 
        {
            //$po_detail_id = $material_cancel_allocation->po_detail_id;
            $material_stock_id = $material_cancel_allocation->material_stock_id;
            $created_by = '107';
            $movement_date = carbon::now();

            //benerin uom
            // try 
            // {

            // DB::beginTransaction();

            // $material_arrival = MaterialArrival::where(
            //     'po_detail_id', $po_detail_id
            //     )->first();

            // $material_arrival->uom = 'PCS';
            // $material_arrival->prepared_status = 'NO NEED PREPARED';
            // $material_arrival->save();

            // DB::commit();

            // } catch (Exception $e) {
            //     DB::rollBack();
            //     $message = $e->getMessage();
            //     ErrorHandler::db($message);
            // }

            //cancel alokasi
            try 
            {

                DB::beginTransaction();

                $_material_stock    = MaterialStock::find($material_stock_id);
                
                $material_preparations   = MaterialPreparation::where('material_stock_id',$material_stock_id)
                ->whereNotNull('auto_allocation_id')
                ->get();

                $movement_date = carbon::now()->toDateTimeString();

                foreach ($material_preparations as $key => $material_preparation) 
                {
                    Temporary::Create([
                        'barcode'       => $material_preparation->po_buyer,
                        'status'        => 'mrp',
                        'user_id'       => $created_by,
                        'created_at'    => $movement_date,
                        'updated_at'    => $movement_date,
                        
                    ]);

                    $auto_allocation_id                 = $material_preparation->auto_allocation_id;
                    $material_preparation_id            = $material_preparation->id;
                    $last_status_movement               = $material_preparation->last_status_movement;

                    if($last_status_movement !='reroute'
                        && $last_status_movement !='out'
                        && $last_status_movement !='out-subcont'
                        && $last_status_movement !='cancel order'
                        && $last_status_movement !='cancel item'
                        && $last_status_movement !='out switch'
                        && $last_status_movement !='hold'
                    )
                    {
                        $qty_booking                        = sprintf('%0.8f',$material_preparation->qty_conversion);
                        $po_buyer                           = $material_preparation->po_buyer;
                        $style                              = $material_preparation->style;
                        $article_no                         = $material_preparation->article_no;
                        $lc_date                            = $material_preparation->lc_date;
                        
                        $auto_allocation                    = AutoAllocation::find($auto_allocation_id);
                        $qty_allocated                      = sprintf('%0.8f',$auto_allocation->qty_allocated);
                        $qty_outstanding                    = sprintf('%0.8f',$auto_allocation->qty_outstanding);
                        
                        $new_qty_outstanding                = sprintf('%0.8f',$qty_outstanding + $qty_booking);
                        $new_qty_allocated                  = sprintf('%0.8f',$qty_allocated - $qty_booking);

                        $auto_allocation->qty_outstanding   = $new_qty_outstanding;
                        $auto_allocation->qty_allocated     = $new_qty_allocated;
                        
                        if($new_qty_allocated <=0 )
                        {
                            $auto_allocation->is_already_generate_form_booking  = false;
                            $auto_allocation->generate_form_booking             = null;
                        }

                        $material_stock     = MaterialStock::find($material_stock_id);
                        $stock              = sprintf('%0.8f',$material_stock->stock);
                        $reserved_qty       = sprintf('%0.8f',$material_stock->reserved_qty);
                        $new_reserverd_qty  = sprintf('%0.8f',$reserved_qty - $qty_booking);
                        $new_available_qty  = sprintf('%0.8f',$stock - $new_reserverd_qty);
                        $old_available_qty  = sprintf('%0.8f',$material_stock->available_qty);
                        $_new_available_qty = sprintf('%0.8f',$old_available_qty + $material_preparation->qty_conversion);

                        if($material_stock->is_closing_balance == false)
                        {
                            $material_stock->reserved_qty   = $new_reserverd_qty;
                            $material_stock->available_qty  = $new_available_qty;

                            if($new_available_qty > 0)
                            {
                                $material_stock->is_allocated = false;
                                $material_stock->is_active = true;
                            }

                            HistoryStock::approved($material_stock_id
                            ,$_new_available_qty
                            ,$old_available_qty
                            ,(-1*$material_preparation->qty_conversion)
                            ,$po_buyer
                            ,$style
                            ,$article_no
                            ,$lc_date
                            ,'CANCEL ALLOCATION'
                            ,'System'
                            ,$created_by
                            ,$material_stock->type_stock_erp_code
                            ,$material_stock->type_stock
                            ,false
                            ,true);
                                
                            $material_stock->save();
                        }

                        $auto_allocation->save();
                    }
                    
                    
                    $material_preparation->delete();
                }
            
            DB::commit();

            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            //alokasi ulang 
            try 
            {

            DB::beginTransaction();
            
            $material_stock    = MaterialStock::find($material_stock_id);
            ReportStock::doAllocation($material_stock, $created_by, $movement_date);

            DB::commit();

            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }



        }                

    }

    //adjustment qty stock per lot
    private function adjustmentStockPerlot()
    {
        $material_stock_per_lots = db::table('material_stock_per_lots_vs_material_stocks_v')
                                      ->get();
        
        try 
        {

        DB::beginTransaction();
        foreach ($material_stock_per_lots as $key => $material_stock_per_lot) 
        {
            $material_stock_per_lot_id                 = $material_stock_per_lot->id;
            $new_available_qty                         = $material_stock_per_lot->new_available_qty;
            $material_stock_per_lot_new                = MaterialStockPerLot::find($material_stock_per_lot_id);
            $curr_type_stock_erp_code                  = $material_stock_per_lot_new->type_stock_erp_code;
            $curr_type_stock                           = $material_stock_per_lot_new->type_stock;
            $old_available_qty                         = $material_stock_per_lot_new->qty_available;
            $new_reserved_qty                          = $material_stock_per_lot_new->qty_stock - $new_available_qty;
            $material_stock_per_lot_new->qty_available = $new_available_qty;
            $material_stock_per_lot_new->qty_reserved  = $new_reserved_qty;
            $material_stock_per_lot_new->save();

            HistoryStockPerLot::historyStockPerLot($material_stock_per_lot_id
                        ,$new_available_qty
                        ,$old_available_qty
                        ,null
                        ,null
                        ,null
                        ,null
                        ,null
                        ,'ADJUSTMENT STOCK PER LOT'
                        ,'SYSTEM'
                        ,'107'
                        ,$curr_type_stock_erp_code
                        ,$curr_type_stock
                        ,false
                        ,false);


        }
        DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

    }

    //isnert stock per lot lama 
    private function insertMaterialStockPerlot()
    {   
        $movement_date = carbon::now();
        $material_stock_per_lots = db::table('insert_material_stock_per_lots_v')
                                      ->get();

        try 
        {

        DB::beginTransaction();
        foreach ($material_stock_per_lots as $key => $material_stock_per_lot) 
        {
                $monitoring_receiving_fabric_id = $material_stock_per_lot->monitoring_receiving_fabric_id;
                $c_order_id                     = $material_stock_per_lot->c_order_id;
                $c_bpartner_id                  = $material_stock_per_lot->c_bpartner_id;
                $warehouse_id                   = $material_stock_per_lot->warehouse_id;
                $item_id                        = $material_stock_per_lot->item_id;
                $item_code                      = $material_stock_per_lot->item_code;
                $no_invoice                     = $material_stock_per_lot->no_invoice;
                $no_packing_list                = $material_stock_per_lot->no_packing_list;
                $document_no                    = $material_stock_per_lot->document_no;
                $supplier_code                  = $material_stock_per_lot->supplier_code;
                $supplier_name                  = $material_stock_per_lot->supplier_name;
                $type_stock                     = $material_stock_per_lot->type_stock;
                $type_stock_erp_code            = $material_stock_per_lot->type_stock_erp_code;
                $actual_lot                     = $material_stock_per_lot->load_actual;
                $uom                            = $material_stock_per_lot->uom;
                $total_arrival                  = sprintf('%0.8f',$material_stock_per_lot->qty);

                $is_exists = MaterialStockPerLot::whereNull('deleted_at')
                ->where([
                    ['c_order_id',$c_order_id],
                    ['c_bpartner_id',$c_bpartner_id],
                    ['item_id',$item_id],
                    ['warehouse_id',$warehouse_id],
                    ['actual_lot',$actual_lot],
                    ['type_stock',$type_stock],
                    ['no_invoice',$no_invoice],
                    ['no_packing_list',$no_packing_list],
                ])
                ->first();

                if(!$is_exists)
                {
                    $material_stock_per_lot = MaterialStockPerLot::FirstOrCreate([
                        'c_order_id'          => $c_order_id,
                        'c_bpartner_id'       => $c_bpartner_id,
                        'warehouse_id'        => $warehouse_id,
                        'item_id'             => $item_id,
                        'item_code'           => $item_code,  
                        'no_invoice'          => $no_invoice,
                        'no_packing_list'     => $no_packing_list,
                        'document_no'         => $document_no,
                        'supplier_code'       => $supplier_code,
                        'supplier_name'       => $supplier_name,
                        'type_stock'          => $type_stock,
                        'type_stock_erp_code' => $type_stock_erp_code,
                        'actual_lot'          => $actual_lot,
                        'uom'                 => $uom,
                        'qty_stock'           => $total_arrival,
                        'qty_available'       => $total_arrival,
                        'created_at'          => $movement_date,
                        'updated_at'          => $movement_date,
                    ]);

                    $material_stock_per_lot_id  = $material_stock_per_lot->id;
                    $old_available              = '0';
                    $new_available              = $material_stock_per_lot->qty_available;
                    $curr_type_stock_erp_code   = $material_stock_per_lot->type_stock_erp_code;
                    $curr_type_stock            = $material_stock_per_lot->type_stock;
                    $curr_actual_lot            = $material_stock_per_lot->actual_lot;
                }else
                {
                    $material_stock_per_lot_id  = $is_exists->id;
                    $reserved_qty               = $is_exists->qty_reserved;
                    $new_stock                  = $total_arrival;
                    $availabilty_qty            = $new_stock - $reserved_qty;

                    $new_available              = $availabilty_qty;
                    $old_available              = $is_exists->qty_available;
                    $curr_type_stock_erp_code   = $is_exists->type_stock_erp_code;
                    $curr_type_stock            = $is_exists->type_stock;
                    
                    $is_exists->qty_stock       = sprintf('%0.8f',$new_stock);
                    $is_exists->qty_available   = sprintf('%0.8f',$availabilty_qty);
                    $is_exists->save();
                
                }
            
                HistoryStockPerLot::historyStockPerLot($material_stock_per_lot_id
                ,$new_available
                ,$old_available
                ,null
                ,null
                ,null
                ,null
                ,null
                ,'TAMBAH STOCK'
                ,'SYSTEM'
                ,'107'
                ,$curr_type_stock_erp_code
                ,$curr_type_stock
                ,false
                ,false);

                MaterialStock::where([
                ['monitoring_receiving_fabric_id',$monitoring_receiving_fabric_id],
                ['load_actual',$actual_lot],
                ['c_order_id',$c_order_id],
                ['c_bpartner_id',$c_bpartner_id],
                ['item_id',$item_id],
                ['warehouse_id',$warehouse_id],
                ['no_invoice',$no_invoice],
                ['no_packing_list',$no_packing_list],
                ['inspect_lot_result', 'RELEASE'],
                ])
                ->update([
                'material_stock_per_lot_id' => $material_stock_per_lot_id
                ]);

        }
        DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }                        

    }

    private function allocationManual() 
    {
            $update_allocations_acc = array(
            'ebfe28c0-aab6-11ea-8410-91521674d8e9',
            'eb5fb900-aeb8-11ea-819e-577704f6ae94',
            '087668a0-a57b-11ea-9e0f-e175b3b87b8b',
            '1a79f3b0-98a8-11ea-9e08-8f5be380a1e4',
            '65fc1890-91ac-11ea-9c86-91aa4e200f64',
            '88e23ce0-9efc-11ea-9c4a-5d0d2148bf30',
            '898a2610-ac63-11ea-b50f-577b5998e2d9',
            '8a7eb620-ac63-11ea-8ab7-a97d1c2fcd88',
            '89cf8260-ac63-11ea-ac0e-85dae001a684',
            '8bdf1a20-ac63-11ea-be24-77d2a449a0e8',
            '89ea4c00-ac63-11ea-886c-8bf8898cecde',
            'a582cc00-aab6-11ea-abec-53990474881c',
            '634583e0-98d5-11ea-b4be-495308115201',
            '647ee9e0-91ac-11ea-aaff-5134620eb942',
            '65bbfec0-91ac-11ea-a0a3-c3e0f04e2e31',
            '64f551a0-91ac-11ea-a536-cdbd063de2bb',
            '394e1290-ac81-11ea-8c1b-ed0ab51d720e',
            '3a5d9170-ac81-11ea-a18a-cb48e7bbf310',
            '00954bc0-aa0c-11ea-8dcc-a3cc56b33651',
            '5ef82b30-8f7b-11ea-9d0b-4dade7e1eb16',
            '7c404c60-958a-11ea-bd86-41c6f53c8025',
            '7935c630-958a-11ea-bf3d-1d14a56e1fad',
            '8969f940-ac63-11ea-95d7-a539524c1798',
            '8c6732f0-ac63-11ea-a16f-4965fb3affa1',
            '89256f60-ac63-11ea-ba1a-633c90d8afdd',
            '894311e0-ac63-11ea-93d2-776487046f5e',
            'a839afe0-a15c-11ea-a75f-197ab4ab6809',
            '3972bd60-ac81-11ea-94b4-d31ebce61340',
            '663d8fe0-91ac-11ea-b22c-b1df2dc0a8af'    
        );
    

    $material_stocks = MaterialStock::where([
        ['is_allocated',false],
        ['is_closing_balance',false],
        ['is_stock_on_the_fly',false],
    ])
    ->whereNotNull('approval_date')
    ->whereNull('deleted_at')
    ->whereIn(db::raw("(c_order_id, item_id, warehouse_id)"),function($query) use($update_allocations_acc) 
    {
        $query->select('c_order_id','item_id_source','warehouse_id')
        ->from('auto_allocations')
        ->whereIn('id',$update_allocations_acc)                    
        ->groupby('c_order_id','item_id_source','warehouse_id');
    })
    ->get();

    $movement_date = carbon::now();

    foreach ($material_stocks as $key => $material_stock )
    {
        if(!$material_stock->last_status) ReportStock::doAllocation($material_stock, '91', $movement_date);
    }

    }

    private function syncItem()
    {
        $data = DB::connection('erp')
        ->table('wms_master_items')
        ->whereIn('item_code', [
            'BBIS-HTG-M60-DRY-0000-00',
            'BBIS-HTG-TG-0004T-0000-00',
            'BBIS-LBI-NGT108-2051A294-0000-L',
            'BBIS-STC-AJP-E-2051A294-300-0000-L',
            'BBIS-TAPE-YK20050-580-00',
            'BBIS-THR-2396GH-SPUN-60/3-GN19-19AW-00',
            'BBIS-STC-AJP-E-2051A294-401-0000-L',
            'BBIS-THR-2630GH-SPUN-60/3-BL44-21AW-00',
            'BBIS-STC-AJP-E-2051A294-001-0000-L',
            'BBIS-THR-998-SPUN-60/3-1000175-00',
            'BBIS-HTG-MM40033-0000-00',
            'BBIS-LBI-NGT108-2104A038-0000-150',
            'BBIS-LBL-AUTHENTIC-0000-00',
            'BBIS-STC-AJP-E-2104A038-002-0000-150',
            'BBIS-TAPE-SJT001-BLACK-00',
            'BBIS-TRD-998-POLYNA-TWST150D-1000175-00',
            'BBIS-STC-AJP-E-2104A038-300-0000-150',
            'BBIS-THR-303-SPUN-60/3-GN31-20SC-00',
            'BBIS-TRD-303-POLYNA-TWST150D-GN31-20SC-00',
            'BBIS-THR-544-SPUN-60/3-MAKOBLUE-00',
            'BBIS-TRD-544-POLYNA-TWST150D-MAKOBLUE-00',
            'BBIS-STC-AJP-E-2104A038-401-0000-150',
            
        ])
        ->get();

        $sync_date = Carbon::now();
        foreach ($data as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();
        
                $item_id          = $value->item_id;
                $item_code        = trim(strtoupper($value->item_code));
                $item_desc        = trim(strtoupper($value->item_desc));
                $color            = trim(strtoupper($value->color));
                $category         = trim(strtoupper($value->category));
                $uom              = trim(strtoupper($value->uom));
                $upc              = trim(strtoupper($value->upc));
                $composition      = trim(strtoupper($value->description));
                $category_name    = trim(strtoupper($value->category_name));
                $width            = trim(strtoupper($value->width));
                $_is_stock        = trim(strtoupper($value->isstock));

                if($_is_stock == 'Y') $is_stock = true;
                else $is_stock = false;

                $is_exits = Item::where('item_id',$item_id)->first();

                if($is_exits)
                {
                    $is_exits->item_code        = $item_code;
                    $is_exits->item_desc        = $item_desc;
                    $is_exits->color            = $color;
                    $is_exits->category         = $category;
                    $is_exits->uom              = $uom;
                    $is_exits->upc              = $upc;
                    $is_exits->composition      = $composition;
                    $is_exits->category_name    = $category_name;
                    $is_exits->width            = $width;
                    $is_exits->is_stock         = $is_stock;
                    $is_exits->updated_at       = $sync_date;
                    $is_exits->save();
                }else
                {
                    Item::firstorCreate([
                        'item_id'           => $item_id,
                        'item_code'         => $item_code,
                        'item_desc'         => $item_desc,
                        'category'          => $category,
                        'uom'               => $uom,
                        'color'             => $color,
                        'upc'               => $upc,
                        'composition'       => $composition,
                        'category_name'     => $category_name,
                        'width'             => $width,
                        'is_stock'          => $is_stock,
                        'created_at'        => $sync_date,
                        'updated_at'        => $sync_date
                    ]);
                }

                DB::commit();

            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }

    private function insertCronLot()
    {
        $data = db::table('complete_lot_v')->get();
        $movement_date = carbon::now();
        foreach ($data as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();

                Temporary::create([
                    'barcode'       => $value->monitoring_receiving_fabric_id,
                    'status'        => 'complete_lot',
                    'user_id'       => '107',
                    'created_at'    => $movement_date,
                    'updated_at'    => $movement_date,
                ]);

                DB::commit();

            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }

    }

    private function cek()
    {
        $material_requirements = DB::select(db::raw("SELECT * FROM get_summary_material_requirement(
            '0126044410'
            );"
        ));

        dd($material_requirements);

        $_po_buyer = PoBuyer::select('statistical_date','lc_date','promise_date')
        ->where('po_buyer','0126044410')
        ->groupby('statistical_date','lc_date','promise_date')
        ->first();

        if(count($material_requirements) > 0){
            MaterialRequirement::where([
                ['po_buyer','0126044410'],
                ['is_upload_manual',false],
            ])
            ->delete();

            foreach ($material_requirements as $key2 => $material_requirement) 
            {
                $item   = Item::where('item_id',$material_requirement->item_id)->first();
                $_style = explode('::',$material_requirement->job_order)[0];

                if($material_requirement->item_desc) $_item_desc = $material_requirement->item_desc;
                else $_item_desc = $item->item_desc;
                
                MaterialRequirement::create([
                    'lc_date'           => ($_po_buyer)? $_po_buyer->lc_date : null,
                    'season'            => $material_requirement->season,
                    'po_buyer'          => $material_requirement->po_buyer,
                    'article_no'        => $material_requirement->article_no,
                    'item_id'           => $material_requirement->item_id,
                    'item_code'         => strtoupper($material_requirement->item_code),
                    'item_desc'         => $_item_desc,
                    'uom'               => $material_requirement->uom,
                    'style'             => $material_requirement->style,
                    '_style'            => $_style,
                    'category'          => $material_requirement->category,
                    'job_order'         => $material_requirement->job_order,
                    'statistical_date'  => ($_po_buyer)? $_po_buyer->statistical_date : null,
                    'promise_date'      => ($_po_buyer)? $_po_buyer->promise_date : null,
                    'qty_required'      => sprintf('%0.8f',$material_requirement->qty_required),
                    'warehouse_id'      => $material_requirement->warehouse_id,
                    'warehouse_name'    => $material_requirement->warehouse_name,
                    'component'         => $material_requirement->component,
                    'destination_name'  => $material_requirement->destination_name
                ]);
            }
        }

    }

    private function autoAllocationCarton()
    {
        $po_detail_id ='1712752';
        //ambil data dari material_stock 
        $material_stock = MaterialStock::where('po_detail_id', $po_detail_id)->first();
        $material_stock_id      = $material_stock->id;
        $c_order_id             = $material_stock->c_order_id;
        $type_stock             = $material_stock->type_stock;
        $type_stock_erp_code    = $material_stock->type_stock_erp_code;
        $c_bpartner_id          = $material_stock->c_bpartner_id;
        $supplier_name          = $material_stock->supplier_name;
        $document_no            = $material_stock->document_no;
        $item_id                = $material_stock->item_id;
        $item_code_source       = $material_stock->item_code;
        $item_code_book         = $material_stock->item_code;
        $item_desc              = $material_stock->item_desc;
        $category               = $material_stock->category;
        $uom                    = $material_stock->uom;
        $warehouse_name         = $material_stock->warehouse_id;
        $warehouse_id           = $material_stock->warehouse_id;
        $available_qty          = $material_stock->available_qty;

        //cek kebutuhan berdasarkan item untuk semua po buyer
        //ganti pakai view aja 
        $material_requirements = db::table('material_requirement_cartons_v')->where('item_id', $item_id)
        ->where('qty_outstanding', '>', 0)
        ->orderBy('psd', 'asc')
        ->orderBy('promise_date', 'asc')
        ->get();

        $movement_date = carbon::now();
        try 
        {
        DB::beginTransaction();
        //cek kebutuhan alokasinya 
        foreach($material_requirements as $key2 => $material_requirement)
        {
            $booking_number = $material_requirement->batch_code;
            $po_buyer       = $material_requirement->po_buyer;
            $article_no     = $material_requirement->article_no;
            $qty_required   = $material_requirement->qty_required;
            $promise_date   = $material_requirement->promise_date;
            $lc_date        = $material_requirement->lc_date;
            $season         = $material_requirement->season;
            $qty_in_house   = $material_requirement->qty_allocation;
            $qty_need       = $material_requirement->qty_outstanding;
            //dd($po_buyer, $item_id, $qty_need);
            if($qty_need > 0 && $available_qty > 0)
            {
                if($available_qty > $qty_need)
                {
                    $qty_allocation = $qty_need;
                    $available_qty -= $qty_need;
                }
                else
                {
                    $qty_allocation = $available_qty;
                    $available_qty -= $available_qty;
                }
            }
            else
            {
                $qty_allocation = 0;
                $available_qty  = 0;
            }
            //dd($qty_allocation, $available_qty);
            if($qty_allocation > 0)
            {
                //jika kebutuhan ada buat alokasinya
                $auto_allocation = AutoAllocation::FirstOrCreate([
                    'document_allocation_number'        => $booking_number,
                    'lc_date'                           => $lc_date,
                    'season'                            => $season,
                    'document_no'                       => $document_no,
                    'c_order_id'                        => $c_order_id,
                    'c_bpartner_id'                     => $c_bpartner_id,
                    'supplier_name'                     => $supplier_name,
                    'po_buyer'                          => $po_buyer,
                    'old_po_buyer'                      => $po_buyer,
                    'item_code_source'                  => $item_code_source,
                    'item_id_book'                      => $item_id,
                    'item_id_source'                    => $item_id,
                    'item_code'                         => $item_code_book,
                    'item_code_book'                    => $item_code_book,
                    'article_no'                        => $article_no,
                    'item_desc'                         => $item_desc,
                    'category'                          => $category,
                    'uom'                               => $uom,
                    'warehouse_name'                    => $warehouse_name,
                    'warehouse_id'                      => $warehouse_id,
                    'qty_allocation'                    => $qty_allocation,
                    'qty_outstanding'                   => $qty_allocation,
                    'qty_allocated'                     => 0,
                    'is_fabric'                         => false,
                    'remark'                            => 'UPLOAD AUTO ALLOCATION CARTON',
                    'remark_update'                     => null,
                    'is_already_generate_form_booking'  => false,
                    'is_upload_manual'                  => true,
                    'type_stock'                        => $type_stock,
                    'type_stock_erp_code'               => $type_stock_erp_code,
                    'promise_date'                      => $promise_date,
                    'deleted_at'                        => null,
                    'status_po_buyer'                   => 'active',
                    'created_at'                        => $movement_date,
                    'updated_at'                        => $movement_date,
                    'user_id'                           => '91'//auth::user()->id,
                ]);

                $note = 'TERJADI PENAMBAHAN ALOKASI';
                $note_code = '1';

                HistoryAutoAllocations::FirstOrCreate([
                    'auto_allocation_id'        => $auto_allocation->id,
                    'c_bpartner_id_new'         => $c_bpartner_id,
                    'supplier_name_new'         => $supplier_name,
                    'document_no_new'           => $document_no,
                    'item_id_new'               => $item_id,
                    'item_code_new'             => $item_code_book,
                    'item_source_id_new'        => $item_id,
                    'item_code_source_new'      => $item_code_source,
                    'po_buyer_new'              => $po_buyer,
                    'uom_new'                   => $uom,
                    'qty_allocated_new'         => $qty_allocation,
                    'warehouse_id_new'          => $warehouse_id,
                    'type_stock_new'            => $type_stock,
                    'type_stock_erp_code_new'   => $type_stock_erp_code,
                    'note_code'                 => $note_code,
                    'note'                      => $note,
                    'operator'                  => $qty_allocation,
                    'is_integrate'              => true,
                    'user_id'                   => '91'//auth::user()->id
                ]);
                
            }

        }

        DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        ReportStock::doAllocation($material_stock, '91', $movement_date);

    }

    private function autoAllocationFreeStockCarton()
    {
        $refresh = DB::select(db::raw("
        refresh materialized view material_requirement_cartons_mv;"
        ));

        $items = db::table('material_requirement_cartons_mv')
        ->select('item_id', 'warehouse_id')
        ->where('qty_outstanding', '>', '0')
        ->groupBy('item_id', 'warehouse_id')
        ->orderBy('item_id', 'asc')
        ->get();
        //dd($items);

        foreach($items as $key => $item)
        {

            $item_id = $item->item_id;
            $warehouse_id = $item->warehouse_id;

            $auto_allocations = db::table('allocated_carton_v')
            ->where('item_id_source',$item_id)
            ->where('warehouse_id',$warehouse_id)
            ->get();

            if(count($auto_allocations) > 0)
            {
                foreach($auto_allocations as $key4 => $auto_allocation)
                {
                    $auto_allocation_id = $auto_allocation->id;
                    //dd($auto_allocation_id);
                    AllocationFabric::doCancellation($auto_allocation_id,'auto allocation carton', '91');
                }
            }

            $material_stocks = MaterialStock::select('id',
            'item_id',
            'item_code',
            'c_order_id',
            'warehouse_id',
            'type_stock',
            'type_stock_erp_code',
            'c_bpartner_id',
            'supplier_name',
            'document_no',
            'item_desc',
            'category',
            'uom',
            'available_qty')
            ->where('item_id',$item_id)
            ->where('warehouse_id',$warehouse_id)
            ->where('deleted_at',null)
            ->where('available_qty','>','0')
            ->where('is_running_stock', false)
            ->where('is_closing_balance', false)
            ->where('is_allocated', false)
            ->where('is_active', true)
            ->where('is_stock_on_the_fly', false)
            ->get();

            //dd($material_stocks);

            foreach($material_stocks as $key2 => $material_stock)
            {
                $material_stock_id      = $material_stock->id;
                $c_order_id             = $material_stock->c_order_id;
                $type_stock             = $material_stock->type_stock;
                $type_stock_erp_code    = $material_stock->type_stock_erp_code;
                $c_bpartner_id          = $material_stock->c_bpartner_id;
                $supplier_name          = $material_stock->supplier_name;
                $document_no            = $material_stock->document_no;
                $item_id                = $material_stock->item_id;
                $item_code_source       = $material_stock->item_code;
                $item_code_book         = $material_stock->item_code;
                $item_desc              = $material_stock->item_desc;
                $category               = $material_stock->category;
                $uom                    = $material_stock->uom;
                $warehouse_name         = $material_stock->warehouse_id;
                $warehouse_id           = $material_stock->warehouse_id;
                $available_qty          = $material_stock->available_qty;

                $material_requirements = db::table('material_requirement_cartons_mv')
                                         ->where('item_id', $item_id)
                                         ->where('warehouse_id',$warehouse_id)
                                        ->where('qty_outstanding', '>', 0)
                                        ->orderBy('psd', 'asc')
                                        ->orderBy('promise_date', 'asc')
                                        ->orderBy('qty_required', 'asc')
                                        ->get();

                //dd($material_stock->item_id, $material_stock->id);

                $movement_date = carbon::now();

                try 
                {
                DB::beginTransaction();
                //cek kebutuhan alokasinya 
                foreach($material_requirements as $key2 => $material_requirement)
                {
                    $booking_number = $material_requirement->batch_code;
                    $po_buyer       = $material_requirement->po_buyer;
                    $article_no     = $material_requirement->article_no;
                    $qty_required   = $material_requirement->qty_required;
                    $promise_date   = $material_requirement->promise_date;
                    $lc_date        = $material_requirement->lc_date;
                    $season         = $material_requirement->season;
                    $qty_in_house   = $material_requirement->qty_allocation;
                    $qty_need       = $material_requirement->qty_outstanding;
                    //dd($po_buyer, $item_id, $qty_need);
                    if($qty_need > 0 && $available_qty > 0)
                    {
                        if($available_qty >= $qty_need)
                        {
                            $qty_allocation = $qty_need;
                            $available_qty -= $qty_need;
                        }
                        else
                        {
                            $qty_allocation = $available_qty;
                            $available_qty -= $available_qty;
                        }
                    }
                    else
                    {
                        $qty_allocation = 0;
                        $available_qty  = 0;
                    }
                    //dd($qty_allocation, $available_qty);
                    if($qty_allocation > 0)
                    {
                        //jika kebutuhan ada buat alokasinya
                        $auto_allocation = AutoAllocation::FirstOrCreate([
                            'document_allocation_number'        => $booking_number,
                            'lc_date'                           => $lc_date,
                            'season'                            => $season,
                            'document_no'                       => $document_no,
                            'c_order_id'                        => $c_order_id,
                            'c_bpartner_id'                     => $c_bpartner_id,
                            'supplier_name'                     => $supplier_name,
                            'po_buyer'                          => $po_buyer,
                            'old_po_buyer'                      => $po_buyer,
                            'item_code_source'                  => $item_code_source,
                            'item_id_book'                      => $item_id,
                            'item_id_source'                    => $item_id,
                            'item_code'                         => $item_code_book,
                            'item_code_book'                    => $item_code_book,
                            'article_no'                        => $article_no,
                            'item_desc'                         => $item_desc,
                            'category'                          => $category,
                            'uom'                               => $uom,
                            'warehouse_name'                    => $warehouse_name,
                            'warehouse_id'                      => $warehouse_id,
                            'qty_allocation'                    => $qty_allocation,
                            'qty_outstanding'                   => $qty_allocation,
                            'qty_allocated'                     => 0,
                            'is_fabric'                         => false,
                            'remark'                            => 'UPLOAD AUTO ALLOCATION CARTON',
                            'remark_update'                     => null,
                            'is_already_generate_form_booking'  => false,
                            'is_upload_manual'                  => true,
                            'type_stock'                        => $type_stock,
                            'type_stock_erp_code'               => $type_stock_erp_code,
                            'promise_date'                      => $promise_date,
                            'deleted_at'                        => null,
                            'status_po_buyer'                   => 'active',
                            'created_at'                        => $movement_date,
                            'updated_at'                        => $movement_date,
                            'user_id'                           => '91'//auth::user()->id,
                        ]);

                        $note = 'TERJADI PENAMBAHAN ALOKASI';
                        $note_code = '1';

                        HistoryAutoAllocations::FirstOrCreate([
                            'auto_allocation_id'        => $auto_allocation->id,
                            'c_bpartner_id_new'         => $c_bpartner_id,
                            'supplier_name_new'         => $supplier_name,
                            'document_no_new'           => $document_no,
                            'item_id_new'               => $item_id,
                            'item_code_new'             => $item_code_book,
                            'item_source_id_new'        => $item_id,
                            'item_code_source_new'      => $item_code_source,
                            'po_buyer_new'              => $po_buyer,
                            'uom_new'                   => $uom,
                            'qty_allocated_new'         => $qty_allocation,
                            'warehouse_id_new'          => $warehouse_id,
                            'type_stock_new'            => $type_stock,
                            'type_stock_erp_code_new'   => $type_stock_erp_code,
                            'note_code'                 => $note_code,
                            'note'                      => $note,
                            'operator'                  => $qty_allocation,
                            'is_integrate'              => true,
                            'user_id'                   => '91'//auth::user()->id
                        ]);
                        
                    }

                }

                DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                ReportStock::doAllocation($material_stock, '91', $movement_date);

            }


        }
    }

    private function internalUseErp()
    {
        $data = [
            // [
            //     'item_code' => '60005775-54F0-58',
            //     'qty'       => 64
            // ],
            // [
            //     'item_code' => '60014885-095A-60',
            //     'qty'   => 61
            // ],
            // [
            //     'item_code' => '60017205-83F7-61',
            //     'qty'   => 6
            // ],
            // [
            //     'item_code' => '60017205-83F7-64',
            //     'qty'   => 7 
            // ],
            // [
            //     'item_code' => '60017438-095A-60',
            //     'qty'       => 1749
            // ],
            // [
            //     'item_code' => '60021691-9407-62',
            //     'qty'   => 745 
            // ],
            // [
            //     'item_code' => '60031754-095A-58',
            //     'qty'   => 97 
            // ],
            // [
            //     'item_code' => '60035389-095A-60',
            //     'qty'       => 282
            // ],
            // [
            //     'item_code' => '60038564-095A-60',
            //     'qty'   => 12 
            // ],
            // [
            //     'item_code' => '62682592-001A-60',
            //     'qty'   => 6
            // ],
            // [
            //     'item_code' => '62682592-095A-58',
            //     'qty'       => 343
            // ],
            // [
            //     'item_code' => '62682592-095A-60',
            //     'qty'   => 38 
            // ],
            // [
            //     'item_code' => '62682592-AA35-60',
            //     'qty'   => 182 
            // ],
            [
                'item_code' => '62696410-001A-70',
                'qty'   => 2088 
            ],
            // [
            //     'item_code' => '62696410-043A-70',
            //     'qty'   => 168 
            // ],
        ];
        $movement_date  = Carbon::now();
        
        foreach ($data as $key => $value) 
            {
                try 
                {
                    DB::beginTransaction();
                    $item_code      = $value['item_code'];
                    $item           = Item::where('item_code',$item_code)->first();
                    $item_id        = $item->item_id;
                    $warehouse_id   = '1000001';
                    $uom            = 'YDS';
                    $operator       = sprintf("%.5f",$value['qty']);
                //    $operator       = $value->operator;
                    //$item           = Item::where('item_id',$item_id)->first();

                    $system = User::where([
                        ['name','system'],
                        ['warehouse',$warehouse_id]
                    ])
                    ->first();
                    
                    $inventory_erp = Locator::with('area')
                    ->where([
                        ['is_active',true],
                        ['rack','ERP INVENTORY'],
                    ])
                    ->whereHas('area',function ($query) use($warehouse_id)
                    {
                        $query->where('is_active',true);
                        $query->where('warehouse',$warehouse_id);
                        $query->where('name','ERP INVENTORY');
                    })
                    ->first();

                    $is_movement_integration_exists = MaterialMovement::where([
                        ['from_location',$inventory_erp->id],
                        ['to_destination',$inventory_erp->id],
                        ['from_locator_erp_id',$inventory_erp->area->erp_id],
                        ['to_locator_erp_id',$inventory_erp->area->erp_id],
                        ['po_buyer','-'],
                        ['is_integrate',false],
                        ['is_active',true],
                        ['no_packing_list','-'],
                        ['no_invoice','-'],
                        ['status','integration-to-inventory-erp'],
                    ])
                    ->first();
                    
                    if(!$is_movement_integration_exists)
                    {
                        $movement_integration = MaterialMovement::firstOrCreate([
                            'from_location'         => $inventory_erp->id,
                            'to_destination'        => $inventory_erp->id,
                            'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                            'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                            'po_buyer'              => '-',
                            'is_integrate'          => false,
                            'is_active'             => true,
                            'no_packing_list'       => '-',
                            'no_invoice'            => '-',
                            'status'                => 'integration-to-inventory-erp',
                            'created_at'            => $movement_date,
                            'updated_at'            => $movement_date,
                        ]);

                        $movement_integration_id = $movement_integration->id;
                    }else
                    {
                        $is_movement_integration_exists->updated_at = $movement_date;
                        $is_movement_integration_exists->save();

                        $movement_integration_id = $is_movement_integration_exists->id;
                    }

                    $is_material_movement_line_integration_exists = MaterialMovementLine::whereNull('material_preparation_id')
                    ->whereNull('material_stock_id')
                    ->where([
                        ['material_movement_id',$movement_integration_id],
                        ['qty_movement',$operator],
                        ['item_id',$item_id],
                        ['warehouse_id',$warehouse_id],
                        ['is_integrate',false],
                        ['is_active',true],
                        ['date_movement',$movement_date],
                    ])
                    ->exists();

                    if(!$is_material_movement_line_integration_exists)
                    {
                        MaterialMovementLine::Create([
                            'material_movement_id'          => $movement_integration_id,
                            'item_code'                     => $item->item_code,
                            'item_id'                       => $item_id,
                            'item_id_source'                => null,
                            'c_order_id'                    => '-',
                            'c_orderline_id'                => '-',
                            'c_bpartner_id'                 => '-',
                            'supplier_name'                 => '-',
                            'type_po'                       => 2,
                            'is_integrate'                  => false,
                            'is_active'                     => true,
                            'uom_movement'                  => $uom,
                            'qty_movement'                  => $operator,
                            'date_movement'                 => $movement_date,
                            'created_at'                    => $movement_date,
                            'updated_at'                    => $movement_date,
                            'date_receive_on_destination'   => $movement_date,
                            'nomor_roll'                    => '-',
                            'warehouse_id'                  => $warehouse_id,
                            'document_no'                   => '-',
                            'note'                          => 'ADJ VARIANCE ERP FAB UNTUK BAPB',
                            'is_active'                     => true,
                            'user_id'                       => $system->id,
                        ]);
                    }

                    DB::commit();

                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }




    }

    private function syncAutoAllocationPR()
    {
        $app_env = Config::get('app.env');
        if($app_env == 'live')
        {
            $data_erp = DB::connection('erp');
        }
        //get data yang berubah 
        $allocation_pr_cancels = $data_erp->table('rma_pr_update')
                                ->whereNotNull('poreference')
                                ->whereNotNull('c_order_id')
                                ->where('isactive', 'N')
                                ->get();

        //get data alokasi yang akan dibatalkan 
        foreach($allocation_pr_cancels as $key => $allocation_pr_cancel)
        {
            $c_order_id = $allocation_pr_cancel->c_order_id;
            $item_id    = $allocation_pr_cancel->m_product_id;
            $po_buyer    = $allocation_pr_cancel->poreference;
            
            $auto_allocation = AutoAllocation::where('c_order_id', $c_order_id)
                              ->where('item_id_book', $item_id)
                              ->where('po_buyer', $po_buyer)
                              ->whereNull('deleled_at')
                              ->first();
            //cancel data yang berubah 
            // foreach($auto_allocations as $key1 => $auto_allocation)
            // {
                $auto_allocation_id = $auto_allocation->id;
                $po_buyer           = $auto_allocation->po_buyer;
                $item_id            = $auto_allocation->item_id_book;
                $category           = $auto_allocation->category;

                if($category == 'FB')
                {

                AllocationFabric::doCancellation($auto_allocation_id,'Alokasi PR sudah tidak aktif', '91');

                //alocating ulang  
                $auto_allocation_active = AutoAllocation::where('item_id_book', $item_id)
                ->where('po_buyer', $po_buyer)
                ->whereNull('deleled_at')
                ->first();

                if($auto_allocation_active)
                {
                    Temporary::FirstOrCreate([
                        'barcode'               => $auto_allocation_active->id,
                        'status'                => 'update_auto_allocation_fabric',
                        'user_id'               => $auto_allocation_active->user_id,
                        'created_at'            => $auto_allocation_active->created_at,
                        'updated_at'            => $auto_allocation_active->updated_at,
                    ]);  
                }

                //}

            }
        }
    }


    private function dailyRecalcaulateDashboardFab()
    {
        $app_env = Config::get('app.env');
        if($app_env == 'live')
        {
            $data_erp = DB::connection('erp');
            $data_web_po    = DB::connection('web_po');
        }

        //list update dari erp 
        $list_update_erps = $data_erp->table('rma_po_mrd')
                           ->select('c_order_id', db::raw("m_product_id as item_id"))
                           ->where('category', 'FB')
                           ->where('updated', '>=', db::raw("NOW() - INTERVAL '24 HOURS'"))
                           ->groupBy('c_order_id', 'm_product_id')
                           ->get();

        $list_update_erps = $list_update_erps->toArray();
        //list update dari web po

        $list_update_web_pos = $data_web_po->table('material_on_ship_v')
                           ->select('c_order_id', db::raw("m_product_id as item_id"))
                           ->where('category', 'FB')
                           ->where('updated_at','>=', db::raw("NOW() - INTERVAL '24 HOURS'"))
                           ->groupBy('c_order_id', 'm_product_id')
                           ->get();

        $list_update_web_pos = $list_update_web_pos->toArray();

        //list update dari wms
        $list_material_arrivals = MaterialArrival::select('c_order_id', 'item_id')
                                ->where('category', 'FB')
                                ->where('created_at', '>=', db::raw("NOW() - INTERVAL '24 HOURS'"))
                                ->groupBy('c_order_id', 'item_id')
                                ->get();

        $list_material_arrivals = $list_material_arrivals->toArray();

        //dd($list_update_web_pos);

        //jadiin satu terus remove duplicat
        $array_merges = array_merge($list_update_erps, $list_update_web_pos, $list_material_arrivals);
        //dd(count($array_merges), count($list_update_web_pos), count($list_update_erps), count($list_material_arrivals));
        //recalculate dari list yang ada 
        
        foreach($array_merges as $key => $array_merge)
        {
            $c_order_id = $array_merge->c_order_id;
            $item_id    = $array_merge->item_id;

            Dashboard::dailyUpdateMonitoringMaterial($c_order_id, $item_id, null);

        }

        //:)

    }

    private function syncBomPerItem()
    {
        $material_requirements = DB::connection('erp')
            ->table('wms_material_requirements_detail_v')
            ->select('style',
                    'item_id',
                    'item_code',
                    'item_desc',
                    'uom',
                    'po_buyer',
                    'job_order',
                    'qty_required',
                    'article_no',
                    'is_piping',
                    'season',
                    'garment_size',
                    'part_no',
                    'warehouse_id',
                    'warehouse_name',
                    'city_name',
                    'component',
                    'category',
                    'supp_code',
                    'supp_name',
                    'qty_ordered_garment')
            ->whereIn(db::raw("(po_buyer, item_code)"), db::raw("(
                ('0126934452','NON STANDARD QTY-0000-00'),
('0126934478','NON STANDARD QTY-0000-00'),
('0126934417','NON STANDARD QTY-0000-00'),
('0126934442','NON STANDARD QTY-0000-00'),
('0126934488','NON STANDARD QTY-0000-00'),
('0126933755','NON STANDARD QTY-0000-00'),
('0126933762','NON STANDARD QTY-0000-00'),
('0126933765','NON STANDARD QTY-0000-00'),
('0126933771','NON STANDARD QTY-0000-00'),
('0126933780','NON STANDARD QTY-0000-00'),
('0126933781','NON STANDARD QTY-0000-00'),
('0126936097','NON STANDARD QTY-0000-00'),
('0126934236','NON STANDARD QTY-0000-00'),
('0126934243','NON STANDARD QTY-0000-00'),
('0126934449','NON STANDARD QTY-0000-00'),
('0126934136','NON STANDARD QTY-0000-00'),
('0126934137','NON STANDARD QTY-0000-00'),
('0126933952','NON STANDARD QTY-0000-00'),
('0126934178','NON STANDARD QTY-0000-00'),
('0126907605','REDBLANKSTICKER-0000-00'),
('0126907734','REDBLANKSTICKER-0000-00'),
('0126907905','REDBLANKSTICKER-0000-00'),
('0126907923','REDBLANKSTICKER-0000-00'),
('0126908471','REDBLANKSTICKER-0000-00'),
('0126908479','REDBLANKSTICKER-0000-00'),
('0126934481','NON STANDARD QTY-0000-00'),
('0126934489','NON STANDARD QTY-0000-00'),
('0126934482','NON STANDARD QTY-0000-00'),
('0126934493','NON STANDARD QTY-0000-00'),
('0126934450','NON STANDARD QTY-0000-00'),
('0126934502','NON STANDARD QTY-0000-00'),
('0126934285','NON STANDARD QTY-0000-00'),
('0126936089','NON STANDARD QTY-0000-00'),
('0126934304','NON STANDARD QTY-0000-00'),
('0126934307','NON STANDARD QTY-0000-00'),
('0126934312','NON STANDARD QTY-0000-00'),
('0126934238','NON STANDARD QTY-0000-00'),
('0126936106','NON STANDARD QTY-0000-00'),
('0126933757','NON STANDARD QTY-0000-00'),
('0126933763','NON STANDARD QTY-0000-00'),
('0126933770','NON STANDARD QTY-0000-00'),
('0126933785','NON STANDARD QTY-0000-00'),
('0126933792','NON STANDARD QTY-0000-00'),
('0126933794','NON STANDARD QTY-0000-00'),
('0126934454','NON STANDARD QTY-0000-00'),
('0126934487','NON STANDARD QTY-0000-00'),
('0126934055','NON STANDARD QTY-0000-00'),
('0126891042','NON STANDARD QTY-0000-00'),
('0126891048','NON STANDARD QTY-0000-00'),
('0126891043','NON STANDARD QTY-0000-00'),
('0126895245','NON STANDARD QTY-0000-00'),
('0126908456','REDBLANKSTICKER-0000-00'),
('0126934027','NON STANDARD QTY-0000-00'),
('0126934028','NON STANDARD QTY-0000-00'),
('0126934036','NON STANDARD QTY-0000-00'),
('0126934228','NON STANDARD QTY-0000-00'),
('0126934232','NON STANDARD QTY-0000-00'),
('0126934233','NON STANDARD QTY-0000-00'),
('0126934197','NON STANDARD QTY-0000-00'),
('0126908431','REDBLANKSTICKER-0000-00'),
('0126908457','REDBLANKSTICKER-0000-00'),
('0126908477','REDBLANKSTICKER-0000-00'),
('0126908466','REDBLANKSTICKER-0000-00'),
('0126934247','NON STANDARD QTY-0000-00'),
('0126934254','NON STANDARD QTY-0000-00'),
('0126933736','NON STANDARD QTY-0000-00'),
('0126933760','NON STANDARD QTY-0000-00'),
('0126934282','NON STANDARD QTY-0000-00'),
('0126934237','NON STANDARD QTY-0000-00'),
('0126934292','NON STANDARD QTY-0000-00'),
('0126933737','NON STANDARD QTY-0000-00'),
('0126933747','NON STANDARD QTY-0000-00'),
('0126933754','NON STANDARD QTY-0000-00'),
('0126933759','NON STANDARD QTY-0000-00'),
('0126934191','NON STANDARD QTY-0000-00'),
('0126934192','NON STANDARD QTY-0000-00'),
('0126934193','NON STANDARD QTY-0000-00'),
('0126934195','NON STANDARD QTY-0000-00'),
('0126934200','NON STANDARD QTY-0000-00'),
('0126934201','NON STANDARD QTY-0000-00'),
('0126934189','NON STANDARD QTY-0000-00'),
('0126935920','NON STANDARD QTY-0000-00'),
('0126907708','REDBLANKSTICKER-0000-00'),
('0126907715','REDBLANKSTICKER-0000-00'),
('0126907952','REDBLANKSTICKER-0000-00'),
('0126908448','REDBLANKSTICKER-0000-00'),
('0126908462','REDBLANKSTICKER-0000-00'),
('0126908474','REDBLANKSTICKER-0000-00'),
('0126908475','REDBLANKSTICKER-0000-00'),
('0126908488','REDBLANKSTICKER-0000-00'),
('0126908465','REDBLANKSTICKER-0000-00'),
('0126908469','REDBLANKSTICKER-0000-00'),
('0126908480','REDBLANKSTICKER-0000-00'),
            )"))
            ->get();

            if($material_requirements->count() > 0)
            {
                DetailMaterialRequirement::whereIn(db::raw("(po_buyer, item_code)"), db::raw("(
                    ('0126934452','NON STANDARD QTY-0000-00'),
    ('0126934478','NON STANDARD QTY-0000-00'),
    ('0126934417','NON STANDARD QTY-0000-00'),
    ('0126934442','NON STANDARD QTY-0000-00'),
    ('0126934488','NON STANDARD QTY-0000-00'),
    ('0126933755','NON STANDARD QTY-0000-00'),
    ('0126933762','NON STANDARD QTY-0000-00'),
    ('0126933765','NON STANDARD QTY-0000-00'),
    ('0126933771','NON STANDARD QTY-0000-00'),
    ('0126933780','NON STANDARD QTY-0000-00'),
    ('0126933781','NON STANDARD QTY-0000-00'),
    ('0126936097','NON STANDARD QTY-0000-00'),
    ('0126934236','NON STANDARD QTY-0000-00'),
    ('0126934243','NON STANDARD QTY-0000-00'),
    ('0126934449','NON STANDARD QTY-0000-00'),
    ('0126934136','NON STANDARD QTY-0000-00'),
    ('0126934137','NON STANDARD QTY-0000-00'),
    ('0126933952','NON STANDARD QTY-0000-00'),
    ('0126934178','NON STANDARD QTY-0000-00'),
    ('0126907605','REDBLANKSTICKER-0000-00'),
    ('0126907734','REDBLANKSTICKER-0000-00'),
    ('0126907905','REDBLANKSTICKER-0000-00'),
    ('0126907923','REDBLANKSTICKER-0000-00'),
    ('0126908471','REDBLANKSTICKER-0000-00'),
    ('0126908479','REDBLANKSTICKER-0000-00'),
    ('0126934481','NON STANDARD QTY-0000-00'),
    ('0126934489','NON STANDARD QTY-0000-00'),
    ('0126934482','NON STANDARD QTY-0000-00'),
    ('0126934493','NON STANDARD QTY-0000-00'),
    ('0126934450','NON STANDARD QTY-0000-00'),
    ('0126934502','NON STANDARD QTY-0000-00'),
    ('0126934285','NON STANDARD QTY-0000-00'),
    ('0126936089','NON STANDARD QTY-0000-00'),
    ('0126934304','NON STANDARD QTY-0000-00'),
    ('0126934307','NON STANDARD QTY-0000-00'),
    ('0126934312','NON STANDARD QTY-0000-00'),
    ('0126934238','NON STANDARD QTY-0000-00'),
    ('0126936106','NON STANDARD QTY-0000-00'),
    ('0126933757','NON STANDARD QTY-0000-00'),
    ('0126933763','NON STANDARD QTY-0000-00'),
    ('0126933770','NON STANDARD QTY-0000-00'),
    ('0126933785','NON STANDARD QTY-0000-00'),
    ('0126933792','NON STANDARD QTY-0000-00'),
    ('0126933794','NON STANDARD QTY-0000-00'),
    ('0126934454','NON STANDARD QTY-0000-00'),
    ('0126934487','NON STANDARD QTY-0000-00'),
    ('0126934055','NON STANDARD QTY-0000-00'),
    ('0126891042','NON STANDARD QTY-0000-00'),
    ('0126891048','NON STANDARD QTY-0000-00'),
    ('0126891043','NON STANDARD QTY-0000-00'),
    ('0126895245','NON STANDARD QTY-0000-00'),
    ('0126908456','REDBLANKSTICKER-0000-00'),
    ('0126934027','NON STANDARD QTY-0000-00'),
    ('0126934028','NON STANDARD QTY-0000-00'),
    ('0126934036','NON STANDARD QTY-0000-00'),
    ('0126934228','NON STANDARD QTY-0000-00'),
    ('0126934232','NON STANDARD QTY-0000-00'),
    ('0126934233','NON STANDARD QTY-0000-00'),
    ('0126934197','NON STANDARD QTY-0000-00'),
    ('0126908431','REDBLANKSTICKER-0000-00'),
    ('0126908457','REDBLANKSTICKER-0000-00'),
    ('0126908477','REDBLANKSTICKER-0000-00'),
    ('0126908466','REDBLANKSTICKER-0000-00'),
    ('0126934247','NON STANDARD QTY-0000-00'),
    ('0126934254','NON STANDARD QTY-0000-00'),
    ('0126933736','NON STANDARD QTY-0000-00'),
    ('0126933760','NON STANDARD QTY-0000-00'),
    ('0126934282','NON STANDARD QTY-0000-00'),
    ('0126934237','NON STANDARD QTY-0000-00'),
    ('0126934292','NON STANDARD QTY-0000-00'),
    ('0126933737','NON STANDARD QTY-0000-00'),
    ('0126933747','NON STANDARD QTY-0000-00'),
    ('0126933754','NON STANDARD QTY-0000-00'),
    ('0126933759','NON STANDARD QTY-0000-00'),
    ('0126934191','NON STANDARD QTY-0000-00'),
    ('0126934192','NON STANDARD QTY-0000-00'),
    ('0126934193','NON STANDARD QTY-0000-00'),
    ('0126934195','NON STANDARD QTY-0000-00'),
    ('0126934200','NON STANDARD QTY-0000-00'),
    ('0126934201','NON STANDARD QTY-0000-00'),
    ('0126934189','NON STANDARD QTY-0000-00'),
    ('0126935920','NON STANDARD QTY-0000-00'),
    ('0126907708','REDBLANKSTICKER-0000-00'),
    ('0126907715','REDBLANKSTICKER-0000-00'),
    ('0126907952','REDBLANKSTICKER-0000-00'),
    ('0126908448','REDBLANKSTICKER-0000-00'),
    ('0126908462','REDBLANKSTICKER-0000-00'),
    ('0126908474','REDBLANKSTICKER-0000-00'),
    ('0126908475','REDBLANKSTICKER-0000-00'),
    ('0126908488','REDBLANKSTICKER-0000-00'),
    ('0126908465','REDBLANKSTICKER-0000-00'),
    ('0126908469','REDBLANKSTICKER-0000-00'),
    ('0126908480','REDBLANKSTICKER-0000-00'),
                )"))->delete();
                
                foreach ($material_requirements as $key2 => $material_requirement) 
                {
                    DetailMaterialRequirement::FirstOrCreate([
                        'season'              => $material_requirement->season,
                        'po_buyer'            => str_replace('-S', '',$material_requirement->po_buyer),
                        'article_no'          => $material_requirement->article_no,
                        'item_id'             => $material_requirement->item_id,
                        'item_code'           => $material_requirement->item_code,
                        'item_desc'           => $material_requirement->item_desc,
                        'uom'                 => $material_requirement->uom,
                        'style'               => $material_requirement->style,
                        'category'            => $material_requirement->category,
                        'job_order'           => $material_requirement->job_order,
                        'statistical_date'    => $statistical_date,
                        'lc_date'             => ($data)? $data->lc_date : null,
                        'promise_date'        => ($data)? $data->promise_date : null,
                        'is_piping'           => $material_requirement->is_piping,
                        'garment_size'        => $material_requirement->garment_size,
                        'part_no'             => $material_requirement->part_no,
                        'qty_required'        => sprintf('%0.8f',$material_requirement->qty_required),
                        'warehouse_id'        => $material_requirement->warehouse_id,
                        'warehouse_name'      => $material_requirement->warehouse_name,
                        'component'           => $material_requirement->component,
                        'destination_name'    => $material_requirement->city_name,
                        'supplier_code'       => $material_requirement->supp_code,
                        'supplier_name'       => $material_requirement->supp_name,
                        'qty_ordered_garment' => $material_requirement->qty_ordered_garment,
                    ]);
                }
            }
            else
            {
                DetailMaterialRequirement::where('po_buyer',$po_buyer)->delete();
            }

            
    }


    private function deleteAlokasiNonPoBuyer()
    {
        $data = AllocationItem::where('remark', 'like', '%BIF%')->get();

        foreach($data as $key => $value)
        {
            $allocation_item        = AllocationItem::find($value->id);
            $material_stock         = MaterialStock::find($allocation_item->material_stock_id);

        if($material_stock)
        {
            try 
            {
            DB::beginTransaction();

            $old_available_qty      = $material_stock->available_qty;
            $stock                  = sprintf('%0.8f',$material_stock->stock);
            $reserved_qty           = sprintf('%0.8f',$material_stock->reserved_qty);
            $type_stock             = sprintf('%0.8f',$material_stock->type_stock);
            $type_stock_erp_code    = $material_stock->type_stock_erp_code;
            
            $new_reserverd_qty      = sprintf('%0.8f',$reserved_qty - $allocation_item->qty_booking);
            $new_available_qty      = sprintf('%0.8f',$stock - $new_reserverd_qty);
            $_new_available_qty     = sprintf('%0.8f',$old_available_qty + $allocation_item->qty_booking);

        
            $material_stock->reserved_qty   = $new_reserverd_qty;
            $material_stock->available_qty  = $new_available_qty;

            if($new_available_qty > 0)
            {
                $material_stock->is_allocated   = false;
                $material_stock->is_active      = true;
            }
            //$material_stock->save();

            $allocation_item->deleted_at = carbon::now();

            HistoryStock::approved($allocation_item->material_stock_id
            ,$_new_available_qty
            ,$old_available_qty
            ,(-1*$allocation_item->qty_booking)
            ,$allocation_item->po_buyer
            ,$allocation_item->style
            ,$allocation_item->article_no
            ,$allocation_item->lc_date
            ,'CANCEL ALLOCATION NON BUYER, ('.$allocation_item->remark.')'
            ,'System'
            ,'91'
            ,$type_stock_erp_code
            ,$type_stock
            ,false
            ,true);

            $material_stock->save();
            $allocation_item->save();

            DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }   
        }

        }
    }

    private function insertCaseInMaterialStock()
    {
        $material_stocks = db::table('list_duplicat_stock_v')->get();

        $no = 1;
        try 
        {
            DB::beginTransaction();
        foreach($material_stocks as $key => $material_stock)
        {
            $id =$material_stock->id;
            $update_stock = MaterialStock::find($id);
            $update_stock->case = 'Terindikasi Duplicat Stock no.'.$no;
            $update_stock->save();
            $no++;

        }
        DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    private function cancelPoBuyerInsert()
    {
        $app_env = Config::get('app.env');
        if($app_env == 'live')
        {
            $data_erp = DB::connection('erp');
        }

        //dd($data_erp);
        //list cancel po buyer
        $cancel_po_buyers = $data_erp->table('bw_report_monitoring_ba')
                           ->where('flag_erp', 'Y')
                           ->where('so_case', 'Cancel Order')
                           ->whereNull('flag_wms')
                           ->get();

        foreach($cancel_po_buyers as $key =>$cancel_po_buyer)
        {
            Temporary::FirstOrCreate([
                'barcode'       => $cancel_po_buyer->old_po,
                'status'        => 'sync_cancel_po_buyer',
                'text_1'        => $cancel_po_buyer->documentno,
                'user_id'       => 1,
                'created_at'    => carbon::now(),
                'updated_at'    => carbon::now(),
            ]);
        }

    }

    private function updateQtyPerStyle()
    {
        $material_requirements = MaterialRequirement::where('po_buyer', '0127199681')
                                 ->whereIn('item_code', [
                                    'TH140029.00-001A-00',
                                    'TH140030.00-ADWE-00',
                                    '80014583-001A-00',
                                    // 'TH140030.00-ADWE-00',
                                    // '80014583-001A-00',
                                    // 'TH140029.00-ADWE-00'

                                 ])
                                 ->get();
        $total_qty_need = $material_requirements->sum('qty_required');

        $sum_material_preparation = 0;
        foreach($material_requirements as $key => $material_requirement)
        {
            $po_buyer   = $material_requirement->po_buyer;
            $item_code  = $material_requirement->item_code;
            $style      = $material_requirement->style;
            $article_no = $material_requirement->article_no;
            $qty_need    = $material_requirement->qty_required;

            if($sum_material_preparation == 0)
            {
                $sum_material_preparation = MaterialPreparation::where('po_buyer', $po_buyer)
                ->where('item_code', $item_code)
                //->where('style', $style)
                //->where('article_no', $article_no)
                ->sum('qty_conversion');
            }

            $material_preparations = MaterialPreparation::where('po_buyer', $po_buyer)
            ->where('item_code', $item_code)
            ->where('style', $style)
            ->where('article_no', $article_no)
            ->get();

            $count_material_preparations = count($material_preparations);

            //dd($po_buyer, $item_code, $total_qty_need, $sum_material_preparation);
            if($total_qty_need == $sum_material_preparation)
            {
                //dd($total_qty_need, $sum_material_preparation); 
                foreach($material_preparations as $key => $material_preparation)
                {
                    //belum tau mau dibuat kek gimana 
                    if($count_material_preparations > 1)
                    {

                    }
                    else
                    {
                        $material_preparation_id = $material_preparation->id;
                        $material_preparation_update = MaterialPreparation::find($material_preparation_id);
                        $material_preparation_update->qty_conversion = $qty_need;
                        $material_preparation_update->save();

                        $update_movement_line = MaterialMovementLine::where('material_preparation_id', $material_preparation_id)
                                                ->update(['qty_movement' => $qty_need]);

                    }
                    $sum_material_preparation = 0;
                }

            }

        }
    }

    private function insertMaterialArrivals()
    {
        $packing_list           = array();
        $material_stocks        = array();
        $barcode_arr            = array();
        $flag_no_subcont        = 0;
        $flag_insert_stock      = 0;


        // $validator = Validator::make($request->all(), [
        //     'material_arrival' => 'required|not_in:[]'
        // ]);

        // if ($validator->passes())
        // {
            // $barcodes       = json_decode($request->material_arrival);
            // $concatenate    = '';

            $app_env = Config::get('app.env');
            if($app_env == 'live')
            {
                $data     = DB::connection('web_po');
            }
            $po_details = $data->table('check_receiving_wms_v')
                            // ->whereIn('po_detail_id', [
                            //     '1945126',
                            //     '1945134',
                            //     '1945127',
                            //     '1945137',
                            //     '1945122'
                            // ])
                            ///->take(10)
                            ->get();

            $concatenate    = '';

            try 
            {
                DB::beginTransaction();
                
                foreach ($po_details as $key => $po_detail) 
                {
                    $po_detail_id    = $po_detail->po_detail_id;
                    $item_id         = $po_detail->m_product_id;
                    $_warehouse_id   = $po_detail->m_warehouse_id;

                    $handover_location = Locator::with('area')
                    ->whereHas('area',function ($query) use($_warehouse_id)
                    {
                        $query->where('is_destination',false);
                        $query->where('is_active',true);
                        $query->where('warehouse',$_warehouse_id);
                        $query->where('name','HANDOVER');
                    })
                    ->first();

                    $receiving_location     = Locator::with('area')
                    ->whereHas('area',function ($query) use($_warehouse_id)
                    {
                        $query->where('is_destination',false);
                        $query->where('is_active',true);
                        $query->where('warehouse',$_warehouse_id);
                        $query->where('name','RECEIVING');
                    })
                    ->first();


                    $inventory_erp = Locator::with('area')
                    ->where([
                        ['is_active',true],
                        ['rack','ERP INVENTORY'],
                    ])
                    ->whereHas('area',function ($query) use($_warehouse_id)
                    {
                        $query->where('is_active',true);
                        $query->where('warehouse',$_warehouse_id);
                        $query->where('name','ERP INVENTORY');
                    })
                    ->first();

                    $item_code       = $po_detail->item;
                    $item_desc       = $po_detail->desc_product;
                    $c_order_id      = $po_detail->c_order_id;
                    $c_orderline_id  = $po_detail->c_orderline_id;
                    $c_bpartner_id   = $po_detail->c_bpartner_id;
                    $document_no     = $po_detail->documentno;

                    $no_packing_list = $po_detail->no_packinglist;
                    $qty_entered     = $po_detail->qtyentered;
                    $qty_ordered     = $po_detail->qtyordered;
                    $qty_carton      = $po_detail->qty_carton;
                    $qty_upload      = $po_detail->qty_upload;
                    $category        = $po_detail->category;

                    $uom             = $po_detail->uomsymbol;
                    $supplier_name   = $po_detail->supplier;
                    $type_po         = $po_detail->type_po;
                    $warehouse_id    = $po_detail->m_warehouse_id;

                    $no_resi         = $po_detail->kst_resi;
                    $no_surat_jalan  = $po_detail->kst_suratjalanvendor;
                    $no_invoice      = $po_detail->kst_invoicevendor;
                    $season          = $po_detail->kst_season;

                    //$c_order_id             = trim(strtoupper($po_detail->c_order_id));
                    $c_orderline_id         = trim(strtoupper($po_detail->c_orderline_id));
                    // $c_bpartner_id          = trim(strtoupper($po_detail->c_bpartner_id));
                    // $supplier_name          = trim(strtoupper($po_detail->supplier_name));
                    // $document_no            = trim(strtoupper($po_detail->document_no));
                    // $item_code              = trim(strtoupper($po_detail->item_code));
                    $_item                  = Item::where('item_code',$item_code)->first();
                    //$item_id                = ($po_detail->item_id)? trim(strtoupper($po_detail->item_id)) : $_item->item_id;
                    // $item_desc              = trim(strtoupper($po_detail->item_desc));
                    // $category               = $po_detail->category;
                    // $no_packing_list        = $po_detail->no_packing_list;
                    // $uom                    = $po_detail->uom;
                    // $no_surat_jalan         = $po_detail->no_surat_jalan;
                    // $no_resi                = $po_detail->no_resi;
                    // $no_invoice             = $po_detail->no_invoice;
                    // $invoice_confirm_id     = md5($no_invoice.$no_resi.$no_packing_list);
                    //dd($invoice_confirm_id);
                    //$season                 = $po_detail->season;
                    $po_buyer               = trim(str_replace('-S', '',$po_detail->pobuyer));
                    $type_po                = $po_detail->type_po;
                    $is_moq                 = false;
                    $is_backlog             = false;
                    $qty_carton             = $po_detail->qty_carton;
                    $qty_upload             = $po_detail->qty_upload;
                    $etd_date               = $po_detail->kst_etddate;
                    $eta_date               = $po_detail->kst_etadate;
                    $material_subcont_id    = null;
                    $material_subcont       = MaterialSubcont::find($material_subcont_id);
                    $po_detail_id           = ($material_subcont ? ($material_subcont->po_detail_id ? $material_subcont->po_detail_id : $material_subcont->barcode) : $po_detail->po_detail_id);
                    $barcode                = $po_detail_id;
                    $is_subcont             = false;
                    $counter                = 1;
                    $conversion = UomConversion::where([
                        ['item_code',$item_code],
                        ['category',$category],
                        ['uom_from',strtoupper($uom)]
                    ])
                    ->first();
    
                    if($conversion)
                    {
                        $dividerate     = $conversion->dividerate;
                        $uom_conversion = $conversion->uom_to;
                    }else
                    {
                        $dividerate     = 1;
                        $uom_conversion = $uom;
                    }
    
                    $qty_conversion = sprintf('%0.8f', $dividerate * $qty_upload);
                    $qty_conversion         = $qty_conversion;
                    $qty_entered            = $po_detail->qtyentered;
                    $qty_ordered            = $po_detail->qtyordered;
                    $qty_delivered           = $po_detail->qtydelivered;
                    $foc                    = $po_detail->foc;
                    $prepared_status        = InsertMaterialReceive::getNeedPrepareStatus($po_buyer,$item_code,$category,$uom,$is_subcont,$qty_conversion);
                    if($uom == 'YDS' or $uom == 'M' or $uom=='CNS')
                    {
                        $prepared_status        = 'NEED PREPARED';
                    }
                    else
                    {
                        $prepared_status        = $prepared_status;
                    }
                    $movement_date          = Carbon::now()->toDateTimeString();

                    $obj                        = new StdClass();
                    $obj->material_arrival_id   = -1;
                    $obj->po_buyer              = $po_buyer;
                    $obj->barcode_supplier      = $po_detail_id;
                    $obj->qty_delivered         = $qty_delivered;
                    $obj->qty_upload            = $qty_upload;
                    $details      = $obj;

                    // $details = [
                    //     'material_arrival_id'       => -1,
                    //     'po_buyer'                  => $po_buyer,
                    //     'barcode_supplier'          => $po_detail_id,
                    //     'qty_delivered'             => $qty_delivered,
                    //     'qty_upload'                => $qty_upload,
                    // ];

                    $barcode_details        = $details;
                    $warehouse_id           = $_warehouse_id;
                    $barcode_header_id      = -1;
                    $uom_conversion         = $uom_conversion;
                    $packing_list[]         = $no_packing_list;
                    $barcode_arr[]          = $po_detail_id;
                    $concatenate            .= "'".$po_detail_id."',";

                    if($counter == $qty_carton)
                    {
                        $is_ready_to_prepare = 'true';
                        if($material_subcont_id && $material_subcont)
                        {
                            $receive_on_destination         = carbon::now();
                            $material_subcont->is_active    = false;
                            $material_subcont->deleted_at   = $receive_on_destination;
                            $material_subcont->date_receive = $receive_on_destination;
                            $material_subcont->save();

                            if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                            {
                                $c_order_id             = 'FREE STOCK';
                                $no_packing_list        = '-';
                                $no_invoice             = '-';
                            }else
                            {
                                $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                ->whereNull('material_roll_handover_fabric_id')
                                ->where('po_detail_id',$po_detail_id)
                                ->first();
                                
                                $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                            }
                            
                            $is_movement_handover_exists = MaterialMovement::where([
                                ['from_location',$inventory_erp->id],
                                ['to_destination',$inventory_erp->id],
                                ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                ['to_locator_erp_id',$inventory_erp->area->erp_id],
                                ['po_buyer',$po_buyer],
                                ['is_integrate',false],
                                ['is_active',true],
                                ['no_packing_list',$no_packing_list],
                                ['no_invoice',$no_invoice],
                                ['status','integration-to-inventory-erp'],
                            ])
                            ->first();

                            if(!$is_movement_handover_exists)
                            {
                                $material_handover_movement = MaterialMovement::firstOrCreate([
                                    'from_location'         => $inventory_erp->id,
                                    'to_destination'        => $inventory_erp->id,
                                    'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                    'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                    'po_buyer'              => $po_buyer,
                                    'is_integrate'          => false,
                                    'is_active'             => true,
                                    'no_packing_list'       => $no_packing_list,
                                    'no_invoice'            => $no_invoice,
                                    'status'                => 'integration-to-inventory-erp',
                                    'created_at'            => $receive_on_destination,
                                    'updated_at'            => $receive_on_destination,
                                ]);
        
                                $material_handover_movement_id = $material_handover_movement->id;
                            }else
                            {
                                $is_movement_handover_exists->updated_at = $receive_on_destination;
                                $is_movement_handover_exists->save();
        
                                $material_handover_movement_id = $is_movement_handover_exists->id;
                            }

                            if($_warehouse_id == '1000013') $_whs_handover  = 'AOI 1';
                            else if($_warehouse_id == '1000002') $_whs_handover  = 'AOI 2';

                            $integration_note = 'BARANG PINDAH TANGAN DITERIMA DARI '.$_whs_handover;
                            

                            $is_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                            ->where([
                                ['material_movement_id',$material_handover_movement_id],
                                ['material_preparation_id',$material_subcont->materialMovementLine->material_preparation_id],
                                ['qty_movement',$material_subcont->qty_upload],
                                ['item_id',$item_id],
                                ['is_integrate',false],
                                ['is_active',true],
                                ['warehouse_id',$_warehouse_id],
                                ['date_movement',$receive_on_destination],
                            ])
                            ->exists();

                            if(!$is_line_exists)
                            {
                                MaterialMovementLine::Create([
                                    'material_movement_id'          => $material_handover_movement_id,
                                    'material_preparation_id'       => $material_subcont->materialMovementLine->material_preparation_id,
                                    'item_code'                     => $item_code,
                                    'item_id'                       => $item_id,
                                    'c_order_id'                    => $c_order_id,
                                    'c_orderline_id'                => $c_orderline_id,
                                    'c_bpartner_id'                 => $c_bpartner_id,
                                    'supplier_name'                 => $supplier_name,
                                    'type_po'                       => 2,
                                    'is_integrate'                  => false,
                                    'is_active'                     => true,
                                    'uom_movement'                  => $uom,
                                    'qty_movement'                  => $material_subcont->qty_upload,
                                    'date_movement'                 => $receive_on_destination,
                                    'created_at'                    => $receive_on_destination,
                                    'updated_at'                    => $receive_on_destination,
                                    'date_receive_on_destination'   => $receive_on_destination,
                                    'nomor_roll'                    => '-',
                                    'warehouse_id'                  => $_warehouse_id,
                                    'document_no'                   => $document_no,
                                    'note'                          => 'INTEGRASI PINDAH TANGAN, '.$integration_note,
                                    'is_active'                     => true,
                                    'user_id'                       => auth::user()->id,
                                ]);
                            }
                            
                            $material_movement_line_id      = $material_subcont->material_movement_line_id;

                            if($material_movement_line_id)
                            {
                                $update_material_movement_line                              = MaterialMovementLine::find($material_movement_line_id);
                                $update_material_movement_line->date_receive_on_destination = $receive_on_destination;
                                $update_material_movement_line->save();
                            }
                            
                        }

                    }else
                    {
                        $is_ready_to_prepare = 'false';
                    }

                    
                    $is_exists = MaterialArrival::where([
                        ['po_detail_id',$po_detail_id],
                        ['warehouse_id',$warehouse_id]
                    ]);

                    if($material_subcont_id) $is_exists = $is_exists->where('material_subcont_id',$material_subcont_id);
                    
                    $is_exists = $is_exists->first();

                    //dd(!$is_exists);
                    
                    if($barcode_header_id == -1 && !$is_exists)
                    {
                        if($material_subcont_id)
                        {
                            $copy_preparation   = MaterialPreparation::whereIn('id',function($query) use($material_subcont_id) 
                            {
                                $query->select('material_preparation_id')
                                ->from('material_movement_lines')
                                ->whereIn('id',function($query2) use ($material_subcont_id)
                                {
                                    $query2->select('material_movement_line_id')
                                    ->from('material_subconts')
                                    ->where('id',$material_subcont_id);
                                })
                                ->groupby('material_preparation_id');
                            })
                            ->first();
        
                            $is_exists = MaterialPreparation::where([
                                'po_detail_id'          => $copy_preparation->po_detail_id,
                                'item_id'               => $copy_preparation->item_id,
                                'c_order_id'            => $copy_preparation->c_order_id,
                                'barcode'               => $copy_preparation->barcode,
                                'po_buyer'              => $copy_preparation->po_buyer,
                                'qty_conversion'        => $copy_preparation->qty_conversion,
                                'style'                 => $copy_preparation->style,
                                'article_no'            => $copy_preparation->article_no,
                                'warehouse'             => $warehouse_id,
                                'item_code'             => $copy_preparation->item_code,
                                'type_po'               => 2,
                            ])
                            ->first();
                            
                            if($is_exists)
                            {
                                $is_exists->last_status_movement        = 'receiving';
                                $is_exists->last_locator_id             = $receiving_location->id;
                                $is_exists->last_movement_date          = $movement_date;
                                $is_exists->deleted_at                  = null;
                                $is_exists->last_user_movement_id       = auth::user()->id;
                                $is_exists->save();
    
                                if($is_exists->po_detail_id == 'FREE STOCK' || $is_exists->po_detail_id == 'FREE STOCK-CELUP')
                                {
                                    $c_order_id             = 'FREE STOCK';
                                    $no_packing_list        = '-';
                                    $no_invoice             = '-';
                                    $c_orderline_receive_id  = '-';
                                }else
                                {
                                    $material_arrival       = MaterialArrival::where('po_detail_id',$is_exists->po_detail_id)->first();
                                    $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                    $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                    $c_orderline_receive_id = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                                }
                                
                                $is_movement_receive_exists = MaterialMovement::where([
                                    ['from_location',$handover_location->id],
                                    ['to_destination',$receiving_location->id],
                                    ['from_locator_erp_id',$handover_location->area->erp_id],
                                    ['to_locator_erp_id',$receiving_location->area->erp_id],
                                    ['po_buyer',$is_exists->po_buyer],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                    ['no_packing_list',$no_packing_list],
                                    ['no_invoice',$no_invoice],
                                    ['status','receive'],
                                ])
                                ->first();
                                
                                if(!$is_movement_receive_exists)
                                {
                                    $material_receive_movement = MaterialMovement::firstorcreate([
                                        'from_location'         => $handover_location->id,
                                        'to_destination'        => $receiving_location->id,
                                        'from_locator_erp_id'   => $handover_location->area->erp_id,
                                        'to_locator_erp_id'     => $receiving_location->area->erp_id,
                                        'is_integrate'          => false,
                                        'is_active'             => true,
                                        'po_buyer'              => $is_exists->po_buyer,
                                        'status'                => 'receive',
                                        'no_packing_list'       => $no_packing_list,
                                        'no_invoice'            => $no_invoice,
                                        'created_at'            => $movement_date,
                                        'updated_at'            => $movement_date,
        
                                    ]);
                                    $material_receive_movement_id = $material_receive_movement->id;
                                }else
                                {
                                    $is_movement_receive_exists->updated_at = $movement_date;
                                    $is_movement_receive_exists->save();
            
                                    $material_receive_movement_id = $is_movement_receive_exists->id;
                                }
                                
                                $is_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                ->where([
                                    ['material_movement_id',$material_receive_movement_id],
                                    ['material_preparation_id',$is_exists->id],
                                    ['item_id',$is_exists->item_id],
                                    ['warehouse_id',$is_exists->warehouse],
                                    ['is_active',true],
                                    ['is_integrate',false],
                                    ['date_movement',$movement_date],
                                    ['qty_movement',$is_exists->qty_conversion],
                                ])
                                ->exists();

                                if(!$is_line_exists)
                                {
                                    $material_movement_line_receive = MaterialMovementLine::firstorcreate([
                                        'material_movement_id'      => $material_receive_movement_id,
                                        'material_preparation_id'   => $is_exists->id,
                                        'item_id'                   => $is_exists->item_id,
                                        'item_code'                 => $is_exists->item_code,
                                        'type_po'                   => $is_exists->type_po,
                                        'c_order_id'                => $is_exists->c_order_id,
                                        'c_bpartner_id'             => $is_exists->c_bpartner_id,
                                        'supplier_name'             => $is_exists->supplier_name,
                                        'c_orderline_id'            => $c_orderline_id,
                                        'uom_movement'              => $is_exists->uom_conversion,
                                        'qty_movement'              => $is_exists->qty_conversion,
                                        'date_movement'             => $movement_date,
                                        'created_at'                => $movement_date,
                                        'updated_at'                => $movement_date,
                                        'is_active'                 => true,
                                        'is_integrate'              => false,
                                        'warehouse_id'              => $is_exists->warehouse,
                                        'document_no'               => $is_exists->document_no,
                                        'nomor_roll'                => '-',
                                        'user_id'                   => '91'
                                    ]);
    
                                    $is_exists->last_material_movement_line_id  = $material_movement_line_receive->id;
                                    $is_exists->save();
                                }
                                
    
                                $temporary = Temporary::Create([
                                    'barcode'       => $is_exists->po_buyer,
                                    'status'        => 'mrp',
                                    'user_id'       => '91',
                                    'created_at'    => $movement_date,
                                    'updated_at'    => $movement_date,
                                ]);
                            }else
                            {
                                $replace_po_buyer = trim(str_replace('-S', '',$po_buyer));
                                $list_po_buyer    = explode(', ', $replace_po_buyer);
                                $po_buyer         = array_unique($list_po_buyer);
                                $po_buyer         = implode(',', $po_buyer);
                                $has_coma         = strpos($po_buyer, ",");
                            
                                $material_arrival = MaterialArrival::FirstOrCreate([
                                    'po_detail_id'          => $po_detail_id,
                                    'material_subcont_id'   => $material_subcont_id,
                                    'item_id'               => $item_id,
                                    'item_code'             => $item_code,
                                    'item_desc'             => $item_desc,
                                    'supplier_name'         => $supplier_name,
                                    'document_no'           => $document_no,
                                    'type_po'               => $type_po,
                                    'c_order_id'            => $c_order_id,
                                    'c_orderline_id'        => $c_orderline_id,
                                    'c_bpartner_id'         => $c_bpartner_id,
                                    'warehouse_id'          => $warehouse_id,
                                    'po_buyer'              => $po_buyer,
                                    'category'              => $category,
                                    'uom'                   => $uom,
                                    'no_packing_list'       => $no_packing_list,
                                    'no_resi'               => $no_resi,
                                    'no_surat_jalan'        => $no_surat_jalan,
                                    'no_invoice'            => $no_invoice,
                                    'etd_date'              => $etd_date,
                                    'eta_date'              => $eta_date,
                                    'qty_entered'           => $qty_entered,
                                    'qty_ordered'           => $qty_ordered,
                                    'qty_carton'            => $qty_carton,
                                    'qty_upload'            => $qty_upload,
                                    'foc_available_rma'     => $foc,
                                    'qty_available_rma'     => $qty_upload,
                                    'qty_reserved_rma'      => 0,
                                    'foc_reserved_rma'      => 0,
                                    'foc'                   => $foc,
                                    'is_subcont'            => $is_subcont,
                                    'prepared_status'       => $prepared_status,
                                    'is_ready_to_prepare'   => $is_ready_to_prepare,
                                    'is_active'             => true,
                                    'is_moq'                => $is_moq,
                                    'user_id'               => '91',
                                    //'invoice_confirm_id'    => $invoice_confirm_id,
                                ]);
                                
                                $material_arrival_id = $material_arrival->id;

                                $material_stocks [] = $material_arrival_id;
                                $flag_insert_stock++;

                                foreach ($barcode_details as $key => $detail) 
                                {
                                    $detail_material_arrival_id     = $detail->material_arrival_id;
                                    $detail_barcode_supplier        = $detail->barcode_supplier;
                                    $detail_qty_delivered           = $detail->qty_delivered;
                                    $detail_qty_upload              = $detail->qty_upload;

                                    if($detail_material_arrival_id == -1)
                                    {
                                        $has_coma = strpos($po_buyer, ",");
                                        
                                        DetailMaterialArrival::firstOrCreate([
                                            'material_arrival_id'   => $material_arrival_id,
                                            'po_buyer'              => $po_buyer,
                                            'barcode_supplier'      => $detail_barcode_supplier,
                                            'qty_delivered'         => $detail_qty_delivered,
                                            'qty_upload'            => $detail_qty_upload,
                                            'user_id'               =>  '91',
                                            'is_active'             =>  true
                                        ]);
                                    }
                                }
                            }
                            
                        }else
                        {

                            $replace_po_buyer = trim(str_replace('-S', '',$po_buyer));
                            $list_po_buyer    = explode(', ', $replace_po_buyer);
                            $po_buyer         = array_unique($list_po_buyer);
                            $po_buyer         = implode(',', $po_buyer);
                            $has_coma = strpos($po_buyer, ",");
                            
                            $material_arrival = MaterialArrival::FirstOrCreate([
                                'po_detail_id'          => $po_detail_id,
                                'material_subcont_id'   => $material_subcont_id,
                                'item_id'               => $item_id,
                                'item_code'             => $item_code,
                                'item_desc'             => $item_desc,
                                'supplier_name'         => $supplier_name,
                                'document_no'           => $document_no,
                                'type_po'               => $type_po,
                                'c_order_id'            => $c_order_id,
                                'c_orderline_id'        => $c_orderline_id,
                                'c_bpartner_id'         => $c_bpartner_id,
                                'warehouse_id'          => $warehouse_id,
                                'po_buyer'              => $po_buyer,
                                'category'              => $category,
                                'uom'                   => $uom,
                                'no_packing_list'       => $no_packing_list,
                                'no_resi'               => $no_resi,
                                'no_surat_jalan'        => $no_surat_jalan,
                                'no_invoice'            => $no_invoice,
                                'etd_date'              => $etd_date,
                                'eta_date'              => $eta_date,
                                'season'                => $season,
                                'qty_entered'           => $qty_entered,
                                'qty_ordered'           => $qty_ordered,
                                'qty_carton'            => $qty_carton,
                                'qty_upload'            => $qty_upload,
                                'foc_available_rma'     => $foc,
                                'qty_available_rma'     => $qty_upload,
                                'qty_reserved_rma'      => 0,
                                'foc_reserved_rma'      => 0,
                                'foc'                   => $foc,
                                'is_subcont'            => $is_subcont,
                                'prepared_status'       => $prepared_status,
                                'is_ready_to_prepare'   => $is_ready_to_prepare,
                                'is_active'             => true,
                                'is_moq'                => $is_moq,
                                'user_id'               => '91',
                                //'invoice_confirm_id'    => $invoice_confirm_id,
                            ]);
                            $material_arrival_id = $material_arrival->id;

                            $material_stocks [] = $material_arrival_id;
                            $flag_insert_stock++;

                            //dd($barcode_details);
                            foreach ($barcode_details as $key => $detail) 
                            {
                                $detail_material_arrival_id     = $material_arrival_id;
                                $detail_barcode_supplier        = $po_detail_id;
                                $detail_qty_delivered           = $qty_delivered;
                                $detail_qty_upload              = $qty_upload;

                                if($detail_material_arrival_id == -1)
                                {
                                    $has_coma = strpos($po_buyer, ",");
                                    
                                    DetailMaterialArrival::firstOrCreate([
                                        'material_arrival_id'   => $material_arrival_id,
                                        'po_buyer'              => $po_buyer,
                                        'barcode_supplier'      => $detail_barcode_supplier,
                                        'qty_delivered'         => $detail_qty_delivered,
                                        'qty_upload'            => $detail_qty_upload,
                                        'user_id'               =>  '91',
                                        'is_active'             =>  true
                                    ]);
                                }
                            }
                        }
                        
                    }else
                    {
                        if($material_subcont_id)
                        {
                            $copy_preparation   = MaterialPreparation::whereIn('id',function($query) use($material_subcont_id) 
                            {
                                $query->select('material_preparation_id')
                                ->from('material_movement_lines')
                                ->whereIn('id',function($query2) use ($material_subcont_id)
                                {
                                    $query2->select('material_movement_line_id')
                                    ->from('material_subconts')
                                    ->where('id',$material_subcont_id);
                                })
                                ->groupby('material_preparation_id');
                            })
                            ->first();
        
                            $is_exists = MaterialPreparation::where([
                                'po_detail_id'          => $copy_preparation->po_detail_id,
                                'item_id'               => $copy_preparation->item_id,
                                'c_order_id'            => $copy_preparation->c_order_id,
                                'barcode'               => $copy_preparation->barcode,
                                'po_buyer'              => $copy_preparation->po_buyer,
                                'qty_conversion'        => $copy_preparation->qty_conversion,
                                'style'                 => $copy_preparation->style,
                                'article_no'            => $copy_preparation->article_no,
                                'warehouse'             => $warehouse_id,
                                'item_code'             => $copy_preparation->item_code,
                                'type_po'               => 2,
                            ])
                            ->first();
                            
                            if($is_exists)
                            {
                                $is_exists->last_status_movement        = 'receiving';
                                $is_exists->last_locator_id             = $receiving_location->id;
                                $is_exists->last_movement_date          = $movement_date;
                                $is_exists->deleted_at                  = null;
                                $is_exists->last_user_movement_id       = auth::user()->id;
                                $is_exists->save();

                                if($is_exists->po_detail_id == 'FREE STOCK' || $is_exists->po_detail_id == 'FREE STOCK-CELUP')
                                {
                                    $c_order_id             = 'FREE STOCK';
                                    $no_packing_list        = '-';
                                    $no_invoice             = '-';
                                    $c_orderline_receive_id  = '-';
                                }else
                                {
                                    $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                    ->whereNull('material_roll_handover_fabric_id')
                                    ->where('po_detail_id',$is_exists->po_detail_id)
                                    ->first();

                                    $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                    $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                    $c_orderline_receive_id = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                                }
                                
                                $is_movement_receive_exists = MaterialMovement::where([
                                    ['from_location',$handover_location->id],
                                    ['to_destination',$receiving_location->id],
                                    ['from_locator_erp_id',$handover_location->area->erp_id],
                                    ['to_locator_erp_id',$receiving_location->area->erp_id],
                                    ['po_buyer',$is_exists->po_buyer],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                    ['no_packing_list',$no_packing_list],
                                    ['no_invoice',$no_invoice],
                                    ['status','receive'],
                                ])
                                ->first();
                                
                                if(!$is_movement_receive_exists)
                                {
                                    $material_receive_movement = MaterialMovement::firstorcreate([
                                        'from_location'         => $handover_location->id,
                                        'to_destination'        => $receiving_location->id,
                                        'from_locator_erp_id'   => $handover_location->area->erp_id,
                                        'to_locator_erp_id'     => $receiving_location->area->erp_id,
                                        'is_integrate'          => false,
                                        'is_active'             => true,
                                        'po_buyer'              => $is_exists->po_buyer,
                                        'status'                => 'receive',
                                        'no_packing_list'       => $no_packing_list,
                                        'no_invoice'            => $no_invoice,
                                        'created_at'            => $movement_date,
                                        'updated_at'            => $movement_date,
        
                                    ]);
                                    $material_receive_movement_id = $material_receive_movement->id;
                                }else
                                {
                                    $is_movement_receive_exists->updated_at = $movement_date;
                                    $is_movement_receive_exists->save();
            
                                    $material_receive_movement_id = $is_movement_receive_exists->id;
                                }

                                $_is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                ->where([
                                    ['material_movement_id',$material_receive_movement_id],
                                    ['material_preparation_id',$is_exists->id],
                                    ['item_id',$is_exists->item_id],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                    ['warehouse_id',$is_exists->warehouse],
                                    ['qty_movement',$is_exists->qty_conversion],
                                    ['date_movement',$movement_date],
                                ])
                                ->exists();

                                if(!$_is_material_movement_line_exists)
                                {
                                    $material_movement_line_receive = MaterialMovementLine::firstorcreate([
                                        'material_movement_id'      => $material_receive_movement_id,
                                        'material_preparation_id'   => $is_exists->id,
                                        'item_id'                   => $is_exists->item_id,
                                        'item_code'                 => $is_exists->item_code,
                                        'type_po'                   => $is_exists->type_po,
                                        'c_order_id'                => $is_exists->c_order_id,
                                        'c_bpartner_id'             => $is_exists->c_bpartner_id,
                                        'supplier_name'             => $is_exists->supplier_name,
                                        'c_orderline_id'            => $c_orderline_id,
                                        'uom_movement'              => $is_exists->uom_conversion,
                                        'qty_movement'              => $is_exists->qty_conversion,
                                        'date_movement'             => $movement_date,
                                        'created_at'                => $movement_date,
                                        'updated_at'                => $movement_date,
                                        'is_active'                 => true,
                                        'is_integrate'              => false,
                                        'warehouse_id'              => $is_exists->warehouse,
                                        'document_no'               => $is_exists->document_no,
                                        'nomor_roll'                => '-',
                                        'user_id'                   => '91'
                                    ]);
    
                                    $is_exists->last_material_movement_line_id  = $material_movement_line_receive->id;
                                    $is_exists->save();
                                    
                                }
                                
                                
                                $temporary = Temporary::Create([
                                    'barcode'       => $is_exists->po_buyer,
                                    'status'        => 'mrp',
                                    'user_id'       => '91',
                                    'created_at'    => $movement_date,
                                    'updated_at'    => $movement_date,
                                ]);
                            }
                        }else
                        {
                            $material_arrival_id            = $is_exists->id;
                            $is_exists->is_ready_to_prepare = $is_ready_to_prepare;
                            $is_exists->save();

                            foreach ($barcode_details as $key => $detail) 
                            {
                                $detail_material_arrival_id     = $detail->material_arrival_id;
                                $detail_barcode_supplier        = $detail->barcode_supplier;
                                $detail_qty_delivered           = $detail->qty_delivered;
                                $detail_qty_upload              = $detail->qty_upload;

                                if($detail_material_arrival_id == -1)
                                {
                                    $has_coma = strpos($po_buyer, ",");
                                    
                                    DetailMaterialArrival::firstOrCreate([
                                        'material_arrival_id'   => $material_arrival_id,
                                        'po_buyer'              => $po_buyer,
                                        'barcode_supplier'      => $detail_barcode_supplier,
                                        'qty_delivered'         => $detail_qty_delivered,
                                        'qty_upload'            => $detail_qty_upload,
                                        'user_id'               =>  '91',
                                        'is_active'             =>  true
                                    ]);
                                }
                            }
                        }
                        
                    }

                    if(!$material_subcont_id)
                    {
                        if($material_arrival != null)
                        {
                            $po_detail_id = $material_arrival->po_detail_id;
                            //dd($po_detail_id );
                            $total_carton_in = DetailMaterialArrival::where('material_arrival_id',$material_arrival_id)->count();
                            MaterialArrival::find($material_arrival_id)->update(['total_carton_in'=> $total_carton_in]);

                            $app_env = Config::get('app.env');
                            if($app_env == 'live')
                            {
                                $po_detail = PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->first();
                                if(!$po_detail->user_warehouse_receive) PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true,'user_warehouse_receive' => 'System']);
                                else PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true]);

                            }else if($app_env == 'dev')
                            {
                                $po_detail = PurchaseOrderDetailDev::where('po_detail_id',$po_detail_id)->first();

                                if(!$po_detail->user_warehouse_receive) PurchaseOrderDetailDev::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true,'user_warehouse_receive' => 'System']);
                                else PurchaseOrderDetailDev::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true]);

                            }else
                            {
                                $po_detail = PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->first();

                                if(!$po_detail->user_warehouse_receive) PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true,'user_warehouse_receive' => 'System']);
                                else PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true]);
                            }
                            
                            $flag_no_subcont++;
                        }
                        else
                        {
                            return response()->json('Failed Save',422);
                        }
                    }

                    Temporary::where([
                        ['status','material_arrival'],
                        ['barcode',$po_detail_id],
                    ])
                    ->delete();


                }
                
                if($concatenate != '')
                {
                    $concatenate = substr_replace($concatenate, '', -1);
                    DB::select(db::raw("SELECT *  FROM delete_duplicate_arrival(array[".$concatenate ."]);" ));
                }
                
                if($flag_no_subcont > 0)
                {
                    InsertMaterialReceive::getItemPackingList($packing_list);
                    $po_buyer = InsertMaterialReceive::getReadyPrepare($barcode_arr,$_warehouse_id);
                }
                
                if($flag_insert_stock > 0) $this->insertMaterialStock($material_stocks,$_warehouse_id);

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            
        // }else
        // {
        //     return response()->json('silahkan scan barcode terlebih dahulu.',422);
        // }

        // return response()->json('success',200);
    }

    private function insertMaterialStock($material_arrival_ids,$_warehouse_id)
    {
        $stock_accessories                              = array();
        $stock_reserved_accessories                     = array();
        $stock_reserved_handover_accessories            = array();

        //dd($material_arrival_ids);
        
        $material_arrivals = MaterialArrival::select('id','po_detail_id','season','warehouse_id','item_id','material_subcont_id','po_buyer','supplier_name','document_no','c_bpartner_id','item_code','c_order_id','uom','qty_upload','qty_carton')
        ->whereIn('id',$material_arrival_ids)
        ->where('warehouse_id',$_warehouse_id)
        ->groupby('id','warehouse_id','document_no','po_detail_id','season','c_bpartner_id','item_id','item_code','material_subcont_id','po_buyer','supplier_name','c_order_id','uom','qty_upload','qty_carton')
        ->get();
        
        $free_stock_destination = Locator::with('area')
        ->whereHas('area',function ($query) use($_warehouse_id) 
        {
            $query->where([
                ['warehouse',$_warehouse_id],
                ['name','FREE STOCK'],
                ['is_destination',true]
            ]);

        })
        ->first();
        $movement_date = Carbon::now();

        try 
        {
            DB::beginTransaction();
            $upload_date                = Carbon::now()->toDateTimeString();

            foreach ($material_arrivals as $key => $value) 
            {
                $po_detail_id           = $value->po_detail_id; 
                $po_buyer               = $value->po_buyer; 
                $material_subcont_id    = $value->material_subcont_id; 
                $material_arrival_id    = $value->id; 
                $qty_upload             = $value->qty_upload; 
                $qty_carton             = $value->qty_carton; 
                $warehouse_id           = $value->warehouse_id; 
                $season                 = $value->season; 
                $document_no            = $value->document_no; 
                $c_bpartner_id          = $value->c_bpartner_id; 
                $item_code              = strtoupper($value->item_code); 
                $c_order_id             = $value->c_order_id; 
                $item_id                = $value->item_id; 
                $uom                    = $value->uom;
                $has_many_po_buyer      = strpos($po_buyer, ",");

                $is_running_stock       = false;
                $is_integrate           = false;

                if($material_subcont_id)
                {
                    $material_subcont       = MaterialSubcont::find($material_subcont_id);
                    $source                 = 'RESERVED HANDOVER STOCK';
                    $remark_detail_stock    = 'KEDATANGAN DARI PINDAH TANGAN';
                    $is_running_stock       = true;
                    $is_integrate           = true;
                    $po_detail_id           = ($material_subcont->po_detail_id ? $material_subcont->po_detail_id : $material_subcont->barcode ); 
                }else
                {
                    if($po_buyer != '' || $po_buyer != null)
                    {
                        $source                 = 'RESERVED STOCK';
                        $is_running_stock       = true;
                        $is_integrate           = true;
                    }else
                    {
                        $source                 = 'NON RESERVED STOCK';
                    }
                    $remark_detail_stock    = 'KEDATANGAN DARI SUPPLIER';
                }

                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();

                $is_pfao         = substr($document_no,0, 4); 
                $_item           = Item::where('item_id',$item_id)->first();
                $upc_item        = ($_item)? $_item->upc : 'MASTER ITEM TIDAK DITEMUKAN';
                $item_desc       = ($_item)? $_item->item_desc : 'MASTER ITEM TIDAK DITEMUKAN';
                $category        = ($_item)? $_item->category : 'MASTER ITEM TIDAK DITEMUKAN';
                $supplier        = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                
                if($c_bpartner_id == 'FREE STOCK')
                {
                    $supplier_code = 'FREE STOCK';
                    $supplier_name = 'FREE STOCK';
                }else
                {
                    $supplier_code   = ($supplier)? strtoupper($supplier->supplier_code) : 'MASTER SUPPLIER NOT FOUND';
                    $supplier_name   = ($supplier)? strtoupper($supplier->supplier_name) : $value->supplier_name;
                }
               
                if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
                {
                    $mapping_stock_id       = null;
                    $type_stock_erp_code    = '2';
                    $type_stock             = 'REGULER';
                }else
                {
                    $get_4_digit_from_beginning = substr($document_no,0, 4);
                    $_mapping_stock_4_digt = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                    if($_mapping_stock_4_digt)
                    {
                        $mapping_stock_id       = $_mapping_stock_4_digt->id;
                        $type_stock_erp_code    = $_mapping_stock_4_digt->type_stock_erp_code;
                        $type_stock             = $_mapping_stock_4_digt->type_stock;
                    }else
                    {
                        $get_5_digit_from_beginning = substr($document_no,0, 5);
                        $_mapping_stock_5_digt = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                        if($_mapping_stock_5_digt)
                        {
                            $mapping_stock_id       = $_mapping_stock_5_digt->id;
                            $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                            $type_stock             = $_mapping_stock_5_digt->type_stock;
                        }else
                        {
                            $mapping_stock_id       = null;
                            $type_stock_erp_code    = null;
                            $type_stock             = null;
                        }
                        
                    }
                }

                $conversion = UomConversion::where([
                    ['item_id',$item_id],
                    ['uom_from',$uom]
                ])
                ->first();
            
                if($conversion)
                {
                    $multiplyrate   = $conversion->multiplyrate;
                    $dividerate     = $conversion->dividerate;
                    $uom_conversion = $conversion->uom_to;
                }else
                {
                    $dividerate     = 1;
                    $multiplyrate   = 1;
                    $uom_conversion = $uom;
                }
                
                $qty_conversion = sprintf('%0.8f',$qty_upload*$dividerate);

                if(!$is_running_stock)
                {
                    $is_stock_already_created = MaterialStock::where([
                        ['c_order_id',$c_order_id],
                        ['item_id',$item_id],
                        ['warehouse_id',$warehouse_id],
                        ['is_stock_on_the_fly',true],
                    ])
                    ->whereNull('po_buyer')
                    ->exists();
                }else
                {
                    $is_stock_already_created = false;
                }
                
                $is_exists = MaterialStock::where([
                    ['po_detail_id',$po_detail_id],
                    ['c_order_id',$c_order_id],
                    ['item_id',$item_id],
                    ['type_po',2],
                    ['warehouse_id',$warehouse_id],
                    ['type_stock',$type_stock],
                    ['uom',$uom_conversion],
                    ['is_material_others',false],
                    ['is_closing_balance',false],
                    ['is_stock_on_the_fly',false],
                ])
                ->whereNull('deleted_at');

                if(!$is_running_stock) $is_exists = $is_exists->where('locator_id',$free_stock_destination->id);
                else $is_exists= $is_exists->whereNull('locator_id');

                if($material_subcont_id) $is_exists = $is_exists->where('remark',$material_subcont_id);
                
                $is_exists = $is_exists->whereNotNull('approval_date')
                ->whereNull('po_buyer')
                ->exists();
                
                //dd(!$is_exists);
                if(!$is_exists)
                {
                    $material_stock = MaterialStock::FirstOrCreate([
                        'po_detail_id'              => $po_detail_id,
                        'locator_id'                => (!$is_running_stock ? $free_stock_destination->id : null),
                        'mapping_stock_id'          => $mapping_stock_id,
                        'type_stock_erp_code'       => $type_stock_erp_code,
                        'type_stock'                => $type_stock,
                        'document_no'               => $document_no,
                        'supplier_code'             => $supplier_code,
                        'supplier_name'             => $supplier_name,
                        'c_order_id'                => $c_order_id,
                        'c_bpartner_id'             => $c_bpartner_id,
                        'item_id'                   => $item_id,
                        'item_code'                 => $item_code,
                        'item_desc'                 => $item_desc,
                        'season'                    => $season,
                        'category'                  => $category,
                        'type_po'                   => 2,
                        'warehouse_id'              => $warehouse_id,
                        'uom'                       => $uom_conversion ,
                        'uom_source'                => $uom ,
                        'qty_carton'                => intval($qty_carton),
                        'qty_arrival'               => $qty_conversion,
                        'stock'                     => $qty_conversion,
                        'reserved_qty'              => 0,
                        'available_qty'             => $qty_conversion,
                        'is_general_item'           => true,
                        'is_material_others'        => false,
                        'is_active'                 => true,
                        'remark'                    => ($material_subcont_id  ? $material_subcont_id : null),
                        'is_running_stock'          => $is_running_stock,
                        'source'                    => $source,
                        'upc_item'                  => $upc_item,
                        'created_at'                => $movement_date,
                        'updated_at'                => $movement_date,
                        'approval_date'             => ($is_pfao == 'PFAO')? $movement_date : NULL,
                        'approval_user_id'          => ($is_pfao == 'PFAO')? $system->id : NULL,
                        'user_id'                   => '91'
                    ]);

                    $material_stock_id  = $material_stock->id;
                    $locator_id         = $material_stock->locator_id;

                    if($is_running_stock)
                    {
                        $is_integrate = true;
                    }else
                    {
                        if($is_stock_already_created) $is_integrate = true;
                        else $is_integrate = false;
                    }
                    
                
                    HistoryStock::approved($material_stock_id
                    ,$qty_conversion
                    ,'0'
                    ,null
                    ,null
                    ,null
                    ,null
                    ,null
                    ,$remark_detail_stock
                    ,'System'
                    ,'91'
                    ,$type_stock_erp_code
                    ,$type_stock
                    ,$is_integrate
                    ,true
                    );

                    DetailMaterialStock::FirstOrCreate([
                        'material_stock_id'         => $material_stock_id,
                        'material_arrival_id'       => $material_arrival_id,
                        'remark'                    => $remark_detail_stock,
                        'uom'                       => $uom_conversion,
                        'qty'                       => $qty_conversion,
                        'user_id'                   => '91',
                        'mm_user_id'                => $system->id,
                        'accounting_user_id'        => $system->id,
                        'created_at'                => $upload_date,
                        'updated_at'                => $upload_date,
                        'approve_date_accounting'   => $upload_date,
                        'approve_date_mm'           => $upload_date,
                        'locator_id'                => $locator_id,
                    ]);

                    
                }else
                {
                    /*$locator_id         = $is_exists->locator_id;
                    $material_stock_id  = $is_exists->id;
                    $stock              = $is_exists->stock;
                    $reserved_qty       = $is_exists->reserved_qty;
                    $old_available_qty  = $is_exists->available_qty;
                    $new_stock          = $stock + $qty_conversion;
                    $availability_stock = $new_stock - $reserved_qty;

                    $is_exists->stock           = sprintf('%0.8f',$new_stock);
                    $is_exists->available_qty   = sprintf('%0.8f',$availability_stock);

                    if($availability_stock > 0)
                    {
                        $is_exists->is_allocated = false;
                        $is_exists->is_active = true;
                    }

                    if($is_running_stock)
                    {
                        $is_integrate = true;
                    }else
                    {
                        if($is_stock_already_created) $is_integrate = true;
                        else $is_integrate = false;
                    }

                    HistoryStock::approved($material_stock_id
                    ,$new_stock
                    ,$old_available_qty
                    ,null
                    ,null
                    ,null
                    ,null
                    ,null
                    ,null
                    ,'System'
                    ,'91'
                    ,$type_stock_erp_code
                    ,$type_stock
                    ,$is_integrate
                    ,true
                    );
                    
                    $is_exists->save();*/
                }

                if(!$is_running_stock) $stock_accessories [] = $material_stock_id;
                else 
                {
                    if(!$material_subcont_id)
                    {
                        $temporary = Temporary::Create([
                            'barcode'       => $material_stock_id,
                            'status'        => 'reserved_stock',
                            'warehouse'     => $warehouse_id,
                            'user_id'       => '91',
                            'created_at'    => $upload_date,
                            'updated_at'    => $upload_date,
                        ]);
                    } 
                    else
                    {
                        $temporary = Temporary::Create([
                            'barcode'       => $material_stock_id,
                            'status'        => 'reserved_handover_stock',
                            'user_id'       => '91',
                            'created_at'    => $upload_date,
                            'updated_at'    => $upload_date,
                        ]);
                    }
                }
            }

            SOTF::updateStockArrival($stock_accessories);
            if(count($stock_accessories) > 0)
            {
                $material_stocks = MaterialStock::whereIn('id',$stock_accessories)->get();

                foreach ($material_stocks as $key => $material_stock )
                {
                    if(!$material_stock->last_status) ReportStock::doAllocation($material_stock, auth::user()->id, $movement_date);
                }
            }

            //AutoAllocation::insertAllocationFromReservedAccessories($stock_reserved_accessories);
            //$this->copyAllocationHandoverccessories($stock_reserved_handover_accessories);
            
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }


}
