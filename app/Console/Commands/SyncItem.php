<?php namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\User;
use App\Models\Item;
use App\Models\PoBuyer;
use App\Models\Supplier;
use App\Models\Scheduler;
use App\Models\PoSupplier;
use App\Models\PoBuyerLog;
use App\Models\ItemPurchase;
use App\Models\PurchaseItem;
use App\Models\MaterialStock;
use App\Models\MappingStocks;
use App\Models\AutoAlocation;
use App\Models\MaterialCheck;
use App\Models\UomConversion;
use App\Models\ReroutePoBuyer;
use App\Models\AutoAllocation;
use App\Models\AllocationItem;
use App\Models\MaterialArrival;
use App\Models\OrderPreferance;
use App\Models\SummaryStockFabric;
use App\Models\PurchaseOrderDetail;
use App\Models\MaterialRequirement;
use App\Models\MaterialPreparation;
use App\Models\SummaryHandoverMaterial;
use App\Models\MonitoringReceivingFabric;
use App\Models\MaterialPreparationFabric;
use App\Models\MaterialRollHandoverFabric;
use App\Models\DetailMaterialPreparationFabric;

class SyncItem extends Command
{
    
    protected $signature = 'sync:item';
    protected $description = 'Sinkron item';
    
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_supplier_on_going = Scheduler::where('job','SYNC_ITEM')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_supplier_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job' => 'SYNC_ITEM',
                'status' => 'ongoing'
            ]);
            $this->info('SYNC ITEM START JOB AT '.carbon::now());
            $this->setStartJob($new_scheduler);
            $this->doJob();
            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
            $this->info('SYNC ITEM END JOB AT '.carbon::now());
        }else{
            $this->info('SYNC ITEM SEDANG BERJALAN');
        }
    }

    private function doJob()
    {
        //list perubahan items

        $data = DB::connection('erp')
        ->table('aoi_populate_product')
        ->where('isintegrate', 'N')
        ->get();

        $sync_date = Carbon::now();
        foreach ($data as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();
        
                $item_id          = $value->item_id;
                $item_code        = trim(strtoupper($value->item_code));
                $item_desc        = trim(strtoupper($value->item_desc));
                $color            = trim(strtoupper($value->color));
                $category         = trim(strtoupper($value->category));
                $uom              = trim(strtoupper($value->uom));
                $upc              = trim(strtoupper($value->upc));
                $composition      = trim(strtoupper($value->description));
                $category_name    = trim(strtoupper($value->category_name));
                $width            = trim(strtoupper($value->width));
                $_is_stock        = trim(strtoupper($value->isstock));
                $recycle          = trim(strtoupper($value->recycle));

                if($_is_stock == 'Y') $is_stock = true;
                else $is_stock = false;

                if($recycle == 'Y') $recycle = true;
                else $recycle = false;

                $is_exits = Item::where('item_id',$item_id)->first();

                if($is_exits)
                {
                    $is_exits->item_code        = $item_code;
                    $is_exits->item_desc        = $item_desc;
                    $is_exits->color            = $color;
                    $is_exits->category         = $category;
                    $is_exits->uom              = $uom;
                    $is_exits->upc              = $upc;
                    $is_exits->composition      = $composition;
                    $is_exits->category_name    = $category_name;
                    $is_exits->width            = $width;
                    $is_exits->is_stock         = $is_stock;
                    $is_exits->updated_at       = $sync_date;
                    $is_exits->recycle          = $recycle;
                    $is_exits->save();
                }else
                {
                    Item::firstorCreate([
                        'item_id'           => $item_id,
                        'item_code'         => $item_code,
                        'item_desc'         => $item_desc,
                        'category'          => $category,
                        'uom'               => $uom,
                        'color'             => $color,
                        'upc'               => $upc,
                        'composition'       => $composition,
                        'category_name'     => $category_name,
                        'width'             => $width,
                        'is_stock'          => $is_stock,
                        'created_at'        => $sync_date,
                        'updated_at'        => $sync_date,
                        'recycle'           => $recycle
                    ]);
                }

                DB::commit();

                DB::connection('erp') //dev_erp //erp
                    ->table('aoi_populate_product')
                    ->where('aoi_populate_product_uu',$value->aoi_populate_product_uu)
                    ->update([
                        'isintegrate' => 'Y',
                    ]);

            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }



        $data_not_active = DB::select(db::raw("select *
            from items
            where item_id not in (
                select item_id from master_item_erp_v
            );"
        ));

        // hapus item
        foreach ($data_not_active as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();

                $item_old           = Item::find($value->id);
                $item_code          = trim(strtoupper($value->item_code));
                
                $material_item_erp  =  DB::connection('erp')
                ->table('wms_master_items')
                ->where('item_code',$item_code)
                ->first();

                if($material_item_erp)
                {
                    $item_id          = $material_item_erp->item_id;
                    $item_code        = trim(strtoupper($material_item_erp->item_code));
                    $item_desc        = trim(strtoupper($material_item_erp->item_desc));
                    $color            = trim(strtoupper($material_item_erp->color));
                    $category         = trim(strtoupper($material_item_erp->category));
                    $uom              = trim(strtoupper($material_item_erp->uom));
                    $upc              = trim(strtoupper($material_item_erp->upc));
                    $composition      = trim(strtoupper($material_item_erp->description));
                    $category_name    = trim(strtoupper($material_item_erp->category_name));
                    $width            = trim(strtoupper($material_item_erp->width));
                    $_is_stock        = trim(strtoupper($material_item_erp->isstock));

                    if($_is_stock == 'Y') $is_stock = true;
                    else $is_stock = false;

                    $is_exits = Item::where([
                        ['item_id',$item_id],
                        ['item_id','!=',$item_old->item_id],
                    ])
                    ->first();

                    if($is_exits)
                    {
                        $_item_id   = $is_exits->item_id;
                        $_item_code = $is_exits->item_code;
                        $_item_desc = $is_exits->item_desc;
                        $_color     = $is_exits->color;
                        
                        $item_old->delete();
                    }else
                    {
                        $new_item = Item::firstorCreate([
                            'item_id'           => $item_id,
                            'item_code'         => $item_code,
                            'item_desc'         => $item_desc,
                            'category'          => $category,
                            'uom'               => $uom,
                            'color'             => $color,
                            'upc'               => $upc,
                            'composition'       => $composition,
                            'category_name'     => $category_name,
                            'width'             => $width,
                            'is_stock'          => $is_stock,
                            'created_at'        => $sync_date,
                            'updated_at'        => $sync_date
                        ]);

                        $_item_id   = $new_item->item_id;
                        $_item_code = $new_item->item_code;
                        $_item_desc = $new_item->item_desc;
                        $_color     = $new_item->color;
                    }



                    MaterialPreparation::where(db::raw('upper(item_code)'),$_item_code)
                    ->update([
                        'item_id' => $_item_id,
                        'item_desc' => $_item_desc
                    ]);
                    
                    MaterialRequirement::where(db::raw('upper(item_code)'),$_item_code)
                    ->update([
                        'item_id' => $_item_id,
                        'item_desc' => $_item_desc
                    ]);
                    
                    SummaryStockFabric::where(db::raw('upper(item_code)'),$_item_code)
                    ->update([
                            'item_id' => $_item_id,
                            'color'   => $_color,
                    ]);

                    $auto_allocations = AutoAllocation::where(db::raw('upper(item_code)'),$_item_code)->get();
                    foreach ($auto_allocations as $key => $auto_allocation) 
                    {
                        $article_no = $auto_allocation->article_no;
                        if($article_no) $item_code = $_item_code.'|'.$article_no;
                        else $item_code = $_item_code;

                        $auto_allocation->item_code         = $item_code;
                        $auto_allocation->item_code_book    = $_item_code;
                        $auto_allocation->item_id_book      = $_item_id;
                        $auto_allocation->save();
                    }

                    AutoAllocation::where(db::raw('upper(item_code_source)'),$_item_code)
                    ->update([
                            'item_id_source' => $_item_id,
                    ]);

                    MaterialArrival::where(db::raw('upper(item_code)'),$_item_code)
                    ->update([
                            'item_id'   => $_item_id,
                            'item_desc' => $_item_code
                    ]);

                    MaterialStock::where(db::raw('upper(item_code)'),$_item_code)
                    ->update([
                            'color'     => $_color,
                            'item_desc' => $_item_desc,
                            'item_id'   => $_item_id,
                    ]);

                    MaterialPreparationFabric::where(db::raw('upper(item_code)'),$_item_code)
                    ->update([
                            'item_id_book' => $_item_id,
                    ]);

                    MaterialPreparationFabric::where(db::raw('upper(item_code_source)'),$_item_code)
                    ->update([
                            'item_id_source' => $_item_id,
                    ]);
                    
                    DetailMaterialPreparationFabric::whereIn('material_stock_id',function($query) use($_item_code,$_color,$_item_id)
                    {
                            $query->select('id')
                            ->from('material_stocks')
                            ->where(db::raw('upper(item_code)'),$_item_code);
                    })
                        ->update([
                            'color'     => $_color,
                            'item_code' => $_item_code,
                            'item_id'   => $_item_id,
                        ]);

                    MaterialCheck::whereNotNull('material_preparation_id')
                    ->whereIn('material_preparation_id',function($query) use($_item_code,$_color,$_item_id,$_item_desc)
                    {
                            $query->select('id')
                            ->from('material_preparations')
                            ->where(db::raw('upper(item_code)'),$_item_code);
                    })
                    ->update([
                            'color'     => $_color,
                            'item_desc' => $_item_desc,
                            //'item_id'   => $_item_id,
                    ]);

                    MaterialCheck::whereNotNull('material_stock_id')
                    ->whereIn('material_stock_id',function($query)  use($_item_code,$_color,$_item_id,$_item_desc)
                    {
                            $query->select('id')
                            ->from('material_stocks')
                            ->where(db::raw('upper(item_code)'),$_item_code);
                    })
                    ->update([
                            'color'     => $_color,
                            'item_desc' => $_item_desc,
                            //'item_id'   => $_item_id,
                    ]);

                    MonitoringReceivingFabric::where(db::raw('upper(item_code)'),$_item_code)
                    ->update([
                            'color'         => $_color,
                            'item_id'       => $_item_id
                    ]);

                    SummaryHandoverMaterial::where(db::raw('upper(item_code)'),$_item_code)
                    ->update([
                            'item_id'     => $_item_id
                    ]);

                    MaterialRollHandoverFabric::where(db::raw('upper(item_code)'),$_item_code)
                    ->update([
                            'color'         => $_color,
                            'item_id'     => $_item_id
                    ]);

                }else
                {
                    $item_old->delete();
                }
            
                DB::commit();

            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
            
        }
    }

    private function setStatus($scheduler,$status)
    {
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler)
    {
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler)
    {
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
