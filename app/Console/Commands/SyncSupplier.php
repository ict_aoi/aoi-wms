<?php namespace App\Console\Commands;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Scheduler;

use App\Http\Controllers\ERPController;

class SyncSupplier extends Command
{
    protected $signature = 'sync:supplier';
    protected $description = 'Sinkron Supplier';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $scheduler_master_supplier = Scheduler::where('job','SYNC_MASTER_SUPPLIER')
        ->where('status','queue')
        ->first();
        
        if(!empty($scheduler_master_supplier))
        {
            $this->setStatus($scheduler_master_supplier,'ongoing');
            $this->setStartJob($scheduler_master_supplier);
            
            $this->info('SYNC MASTER SUPPLIER START JOB AT '.carbon::now());
            ERPController::supplierCronInsert();
            $this->info('SYNC MASTER SUPPLIER END JOB AT '.carbon::now());

            $this->setStatus($scheduler_master_supplier,'done');
            $this->setEndJob($scheduler_master_supplier);
            
        }else
        {
            $is_schedule_supplier_on_going = Scheduler::where('job','SYNC_MASTER_SUPPLIER')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_supplier_on_going)
            {
                $new_scheduler_master_supplier = Scheduler::create([
                    'job' => 'SYNC_MASTER_SUPPLIER',
                    'status' => 'ongoing'
                ]);
                $this->setStartJob($new_scheduler_master_supplier);
                
                $this->info('SYNC MASTER SUPPLIER START JOB AT '.carbon::now());
                ERPController::supplierCronInsert();
                $this->info('SYNC SUPPLIER END JOB AT '.carbon::now());

                $this->setStatus($new_scheduler_master_supplier,'done');
                $this->setEndJob($new_scheduler_master_supplier); 
            }else{
                $this->info('SYNC MASTER SUPPLIER SEDANG BERJALAN');
            }
        }  
        
        
        $scheduler_po_supplier = Scheduler::where('job','SYNC_PO_SUPPLIER')
        ->where('status','queue')
        ->first();
        
        if(!empty($scheduler_po_supplier))
        {
            $this->setStatus($scheduler_po_supplier,'ongoing');
            $this->setStartJob($scheduler_po_supplier);
            
            $this->info('SYNC PO SUPPLIER START JOB AT '.carbon::now());
            ERPController::poSupplierCronInsert();
            $this->info('SYNC PO SUPPLIER END JOB AT '.carbon::now());

            $this->setStatus($scheduler_po_supplier,'done');
            $this->setEndJob($scheduler_po_supplier);
            
        }else
        {
            $is_schedule_po_supplier_on_going = Scheduler::where('job','SYNC_PO_SUPPLIER')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_po_supplier_on_going)
            {
                $new_scheduler_po_supplier = Scheduler::create([
                    'job' => 'SYNC_PO_SUPPLIER',
                    'status' => 'ongoing'
                ]);
                $this->setStartJob($new_scheduler_po_supplier);
                
                $this->info('SYNC PO SUPPLIER START JOB AT '.carbon::now());
                ERPController::poSupplierCronInsert();
                $this->info('SYNC PO SUPPLIER END JOB AT '.carbon::now());

                $this->setStatus($new_scheduler_po_supplier,'done');
                $this->setEndJob($new_scheduler_po_supplier); 
            }else{
                $this->info('SYNC PO SUPPLIER SEDANG BERJALAN');
            }
        }

        $schedular_purchase_item = Scheduler::where('job','SYNC_PURCHASE_ITEM')
        ->where('status','queue')
        ->first();

        if(!empty($schedular_purchase_item)){
            $this->info('SYNC PURCHASE ITEM JOB AT '.carbon::now());
            $this->setStatus($schedular_purchase_item,'ongoing');
            $this->setStartJob($schedular_purchase_item);
            ERPController::dailyPurchaseItems();
            $this->setStatus($schedular_purchase_item,'done');
            $this->setEndJob($schedular_purchase_item);
            $this->info('DONE SYNC PURCHASE ITEM AT '.carbon::now());
        }else{
            $is_schedule_on_going = Scheduler::where('job','SYNC_PURCHASE_ITEM')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_PURCHASE_ITEM',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC PURCHASE ITEM JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                ERPController::dailyPurchaseItems();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE PURCHASE ITEM JOB AT '.carbon::now());
            }else{
                $this->info('SYNC PURCHASE ITEM SEDANG BERJALAN');
            }
        }

        $schedular_purchase_item_fab = Scheduler::where('job','SYNC_PURCHASE_ITEM_FAB')
        ->where('status','queue')
        ->first();

        if(!empty($schedular_purchase_item_fab)){
            $this->info('SYNC PURCHASE ITEM FAB JOB AT '.carbon::now());
            $this->setStatus($schedular_purchase_item_fab,'ongoing');
            $this->setStartJob($schedular_purchase_item_fab);
            ERPController::dailyInsertPurchaseAllocationItems();
            $this->setStatus($schedular_purchase_item_fab,'done');
            $this->setEndJob($schedular_purchase_item_fab);
            $this->info('DONE SYNC PURCHASE ITEM FAB AT '.carbon::now());
        }else{
            $is_schedule_on_going = Scheduler::where('job','SYNC_PURCHASE_ITEM_FAB')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_PURCHASE_ITEM_FAB',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC PURCHASE ITEM FAB JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                ERPController::dailyInsertPurchaseAllocationItems();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE PURCHASE ITEM FAB JOB AT '.carbon::now());
            }else{
                $this->info('SYNC PURCHASE ITEM FAB SEDANG BERJALAN');
            }
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
