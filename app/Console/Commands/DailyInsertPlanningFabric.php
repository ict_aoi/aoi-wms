<?php namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Http\Controllers\FabricMaterialPlanningController;

use App\Models\Scheduler;
class DailyInsertPlanningFabric extends Command
{
    
    protected $signature = 'dailyPlanningFabric:insert';
    protected $description = 'Insert Daily Planning Purchase';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','SYNC_PLANNING_FABRIC')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $start_date     = Carbon::today()->subDays(30)->format('Y-m-d');
            $end_date       = Carbon::now()->addDays(30)->format('Y-m-d');
            $movement_date  = carbon::now()->toDateTimeString();
            
            $new_scheduler = Scheduler::create([
                'job' => 'SYNC_PLANNING_FABRIC',
                'status' => 'ongoing'
            ]);
            $this->setStartJob($new_scheduler);
            
            $this->info('SYNC PLANNING FABRIC JOB AT '.carbon::now());
            FabricMaterialPlanningController::dailyInsertPlanning($start_date,$end_date,$movement_date);
            $this->info('DONE SYNC PO BUYER PER LC JOB AT '.carbon::now());

            $this->info('SYNC INSERT CUTTING INSTRUCTION FABRIC JOB AT '.carbon::now());
            FabricMaterialPlanningController::getCuttingInstruction($start_date,$end_date,$movement_date);
            $this->info('DONE INSERT CUTTING INSTRUCTION FABRIC JOB AT '.carbon::now());

            // $this->info('SYNC INSERT RECALCULATE FABRIC JOB AT '.carbon::now());
            // FabricMaterialPlanningController::recalculateAutoAllocationFabric($start_date,$end_date,$movement_date);
            // $this->info('DONE INSERT RECALCULATE FABRIC JOB AT '.carbon::now());

            $this->info('SYNC INSERT ALLOCATION FABRIC JOB AT '.carbon::now());
            FabricMaterialPlanningController::getAllocationFabricSchedule($start_date,$end_date,$movement_date);
            $this->info('DONE INSERT ALLOCATION FABRIC JOB AT '.carbon::now());

            $this->info('SYNC INSERT PREPARATION FABRIC JOB AT '.carbon::now());
            FabricMaterialPlanningController::insertMaterialPreparationFabricSchedule($start_date,$end_date,$movement_date);
            $this->info('DONE INSERT PREPARATION FABRIC JOB AT '.carbon::now());
            
            $this->info('SYNC CHECK PREPARATION FABRIC JOB AT '.carbon::now());
            FabricMaterialPlanningController::insertCheckMaterialPreparationFabricSchedule($start_date,$end_date,$movement_date);
            $this->info('DONE CHECK PREPARATION FABRIC JOB AT '.carbon::now());

            $this->info('SYNC SEND EMAIL FABRIC JOB AT '.carbon::now());
            FabricMaterialPlanningController::sendEmailOutstandingAllocation($start_date,$end_date,$movement_date);
            $this->info('DONE SEND EMAIL FABRIC JOB AT '.carbon::now());

            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
            
        }else
        {
            $this->info('SYNC PLANNING FABRIC JOB SEDANG BERJALAN');
        }

        
        $this->info('Daily Insert has been successfully');
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
