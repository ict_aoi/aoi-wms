<?php namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Http\Controllers\WmsOldController;
use App\Http\Controllers\AccessoriesMaterialInController;

use App\Models\Scheduler;

class HourlyInsertMrp extends Command
{
    protected $signature = 'hourlymrp:insert';
    protected $description = 'Insert Mrp';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(){
       // $is_exists_schedular_on_going = Scheduler::where('status','ongoing')->exists();
        //if(!$is_exists_schedular_on_going){
            $scheduler = Scheduler::where('job','SYNC_MRP')
            ->where('status','queue')
            ->first();

            if(!empty($scheduler)){
                $this->info('SYNC MRP START JOB AT '.carbon::now());
                $this->setStatus($scheduler,'ongoing');
                $this->setStartJob($scheduler);
                AccessoriesMaterialInController::mrp();
                $this->setStatus($scheduler,'done');
                $this->setEndJob($scheduler);
                $this->info('SYNC MRP END JOB AT '.carbon::now());
            }else{
                $is_mrp_schedule_on_going = Scheduler::where('job','SYNC_MRP')
                ->where('status','ongoing')
                ->exists();

                if(!$is_mrp_schedule_on_going){
                    $new_scheduler = Scheduler::create([
                        'job' => 'SYNC_MRP',
                        'status' => 'ongoing'
                    ]);
                    $this->info('SYNC MRP START JOB AT '.carbon::now());
                    $this->setStartJob($new_scheduler);
                    AccessoriesMaterialInController::mrp(); 
                    $this->setStatus($new_scheduler,'done');
                    $this->setEndJob($new_scheduler); 
                    $this->info('SYNC MRP END JOB AT '.carbon::now());
                }else{
                    $this->info('SYNC MRP SEDANG BERJALAN');
                }
            }
        //}else{
        //    $this->info('SCHEDULE LAIN SEDANG JALAN');
       // }
        
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
