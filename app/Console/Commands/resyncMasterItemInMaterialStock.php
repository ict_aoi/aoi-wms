<?php namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Scheduler;
use App\Models\MaterialStock;
use App\Models\HistoryMaterialStocks;

class resyncMasterItemInMaterialStock extends Command
{
    protected $signature    = 'resync:itemMaterialStock';
    protected $description  = 'resync master item in material stock';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','RESYNC_ITEM_MATERIAL_STOCK')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job' => 'RESYNC_ITEM_MATERIAL_STOCK',
                'status' => 'ongoing'
            ]);
            $this->setStartJob($new_scheduler);
            
            $this->info('SYNC MASTER ITEM MATERIAL STOCK START JOB AT '.carbon::now());
            $this->updateData();
            $this->info('SYNC MASTER ITEM MATERIAL STOCK END JOB AT '.carbon::now());

            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
        } 
    }

    private function updateData()
    {
        $data = DB::table('resync_master_item_in_material_stocks_v')->get();
        foreach ($data as $key => $value) 
        {
            $id     = $value->id;
            $material_stock   = MaterialStock::find($id);

            $material_stock->item_code = $value->item_code_master;
            $material_stock->item_desc = $value->item_desc_master;
            $material_stock->upc_item  = $value->upc_master;
            $material_stock->color     = $value->color_master;
            $material_stock->category  = $value->category_master;
            $material_stock->uom       = $value->uom_master;
            $material_stock->save();
            
            HistoryMaterialStocks::where('material_stock_id',$id)
            ->update([
                'uom'                   => $material_stock->uom,
                'type_stock_erp_code'   => $material_stock->type_stock_erp_code,
                'type_stock'            => $material_stock->type_stock,
                'item_id'               => $material_stock->item_id,
            ]);
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
