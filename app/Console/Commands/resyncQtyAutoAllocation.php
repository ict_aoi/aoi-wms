<?php namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Scheduler;
use App\Models\AutoAllocation;
use App\Models\AllocationItem;
use App\Models\MaterialPreparation;


class resyncQtyAutoAllocation extends Command
{
    protected $signature    = 'resync:qtyPrAutoAllocation';
    protected $description  = 'resync qty PR in auto allocation';

    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
        $scheduler = Scheduler::where('job','RESYNC_QTY_AUTO_ALLOCATION_PR')
        ->where('status','queue')
        ->first();
        
        if(!empty($scheduler))
        {
            $this->setStatus($scheduler,'ongoing');
            $this->setStartJob($scheduler);
            
            $this->info('RESYNC QTY PR IN AUTO ALLOCATION START JOB AT '.carbon::now());
            $this->updateData();
            $this->info('SYNC QTY PR IN AUTO ALLOCATION END JOB AT '.carbon::now());

            $this->setStatus($scheduler,'done');
            $this->setEndJob($scheduler);
            
        }else
        {
            $is_schedule_on_going = Scheduler::where('job','RESYNC_QTY_AUTO_ALLOCATION_PR')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going)
            {
                $new_scheduler = Scheduler::create([
                    'job' => 'RESYNC_QTY_AUTO_ALLOCATION_PR',
                    'status' => 'ongoing'
                ]);
                $this->setStartJob($new_scheduler);
                
                $this->info('SYNC QTY PR IN AUTO ALLOCATION  START JOB AT '.carbon::now());
                $this->updateData();
                $this->info('SYNC QTY PR IN AUTO ALLOCATION  END JOB AT '.carbon::now());

                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
            }else{
                $this->info('SYNC QTY PR IN AUTO ALLOCATION  SEDANG BERJALAN');
            }
        } 
    }

    private function updateData()
    {
        $data = DB::table('resync_qty_auto_allocation_pr_v')->get();
        $array = array();
        foreach ($data as $key => $value) 
        {
            $id     = $value->id;
            $item   = AutoAllocation::find($id);

            $item->qty_allocation   = sprintf('%0.8f',$value->erp_qty_allocation);
            $item->save();

            $array [] = $item->id;
        }

        try
        {
            DB::beginTransaction();
            //return view('errors.503');
            $auto_allocations        = AutoAllocation::whereIn('id',$array)->get();

            foreach ($auto_allocations as $key => $auto_allocation) 
            {
                if($auto_allocation->is_fabric)
                {
                    $qty_allocation         = sprintf("%0.4f",$auto_allocation->qty_allocation);

                    $qty_allocated  = AllocationItem::where([
                        ['auto_allocation_id',$id],
                    ])
                    ->sum('qty_booking');

                    $new_oustanding = sprintf("%0.4f",$qty_allocation - ($qty_allocated));

                    if($new_oustanding <= 0)
                    {
                        $auto_allocation->is_already_generate_form_booking  = true;
                        $auto_allocation->generate_form_booking             = carbon::now();
                    }else
                    {
                        $auto_allocation->is_already_generate_form_booking  = false;
                        $auto_allocation->generate_form_booking             = null;
                    }
                
                    if ($new_oustanding == 0) $new_oustanding = '0';
                    else $new_oustanding = sprintf("%0.4f",$new_oustanding);

                    $auto_allocation->qty_allocated     = sprintf("%0.4f",($qty_allocated));
                    $auto_allocation->qty_outstanding   = sprintf("%0.4f",$new_oustanding);
                    $auto_allocation->save();

                }else 
                {
                    $qty_allocation         = sprintf("%0.4f",$auto_allocation->qty_allocation);

                    $qty_allocated  = MaterialPreparation::where([
                        ['auto_allocation_id',$id],
                        ['last_status_movement','!=','reroute'],
                    ])
                    ->sum(db::raw("(qty_conversion + COALESCE(adjustment,0) + COALESCE(qty_borrow,0) + COALESCE(qty_reject,0))"));

                    $new_oustanding = sprintf("%0.4f",$qty_allocation - ($qty_allocated));

                    if($new_oustanding <= 0)
                    {
                        $auto_allocation->is_already_generate_form_booking  = true;
                        $auto_allocation->generate_form_booking             = carbon::now();
                    }else
                    {
                        $auto_allocation->is_already_generate_form_booking  = false;
                        $auto_allocation->generate_form_booking             = null;
                    }
                
                    if ($new_oustanding == 0) $new_oustanding = '0';
                    else $new_oustanding = sprintf("%0.4f",$new_oustanding);

                    $auto_allocation->qty_allocated     = sprintf("%0.4f",($qty_allocated));
                    $auto_allocation->qty_outstanding   = sprintf("%0.4f",$new_oustanding);
                    $auto_allocation->save();

                    
                }
            }
            
            DB::commit();
        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
