<?php
namespace App\Console\Commands;

use Carbon\Carbon;
use DB;
use Config;
use App\Models\Scheduler;

use Illuminate\Console\Command;

class AutoCancelPoBuyerFromERP extends Command
{
    protected $signature = 'dailyAutoCancelPoBuyer:insert';
    protected $description = 'daily auto cancel po buyer insert';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','SYNC_CANCEL_PO_BUYER_FROM_ERP')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job' => 'SYNC_CANCEL_PO_BUYER_FROM_ERP',
                'status' => 'ongoing'
            ]);
            $this->setStartJob($new_scheduler);
            
            $this->info('SYNC CANCEL PO BUYER FROM ERP START JOB AT '.carbon::now());
            $this->doJob();
            $this->info('SYNC CANCEL PO BUYER FROM ERP  END JOB AT '.carbon::now());

            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
        }else{
            $this->info('SYNC_CANCEL_PO_BUYER_FROM_ERP SEDANG BERJALAN');
        } 
    }

    static function doJob()
    {
            
        foreach ($temporaries as $key => $temporary) 
        {
            try 
            {
                DB::beginTransaction();

                $app_env = Config::get('app.env');

                if($app_env == 'live')
                {
                    $data_erp = DB::connection('erp');
                }

                //list cancel po buyer
                $cancel_po_buyers = $data_erp->table('bw_report_monitoring_ba')
                                ->where('flag_erp', 'Y')
                                ->where('so_case', 'Cancel Order')
                                ->whereNull('flag_wms')
                                ->get();

                foreach($cancel_po_buyers as $key =>$cancel_po_buyer)
                {
                    Temporary::FirstOrCreate([
                        'barcode'       => $cancel_po_buyer->old_po,
                        'status'        => 'sync_cancel_po_buyer',
                        'text_1'        => $cancel_po_buyer->documentno,
                        'user_id'       => 1,
                        'created_at'    => carbon::now(),
                        'updated_at'    => carbon::now(),
                    ]);
                }
                    
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

        }

    } 

    private function setStatus($scheduler,$status)
    {
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler)
    {
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler)
    {
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
