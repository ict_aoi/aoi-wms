<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\AllocationFabricController;

class DailyUpdateWarehouseProduction extends Command
{
    protected $signature = 'dailyWarehouseProduction:update';
    protected $description = 'Update Warehouse Production Fabric';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        AllocationFabricController::daily_get_warehouse_production();
        $this->info('Daily Update has been successfully');
    }
}
