<?php namespace App\Console\Commands;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Http\Controllers\MasterDataAutoAllocationController;

use App\Models\Scheduler;

class DailyAutoAllocation extends Command
{
    protected $signature = 'autoAllocation:insert';
    protected $description = 'insert auto allocation';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $schedular = Scheduler::where('job','SYNC_AUTO_ALLOCATION')
        ->where('status','queue')
        ->first();

        if(!empty($schedular)){
            $this->info('SYNC AUTO ALLOCATION JOB AT '.carbon::now());
            $this->setStatus($schedular,'ongoing');
            $this->setStartJob($schedular);
            MasterDataAutoAllocationController::syncAutoAllocationErp();
            $this->setStatus($schedular,'done');
            $this->setEndJob($schedular);
            $this->info('DONE SYNC AUTO ALLOCATION AT '.carbon::now());
        }else{
            $is_schedule_on_going = Scheduler::where('job','SYNC_AUTO_ALLOCATION')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_AUTO_ALLOCATION',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC AUTO ALLOCATION JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                MasterDataAutoAllocationController::syncAutoAllocationErp();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC AUTO ALLOCATION JOB AT '.carbon::now());
            }else{
                $this->info('SYNC AUTO ALLOCATION SEDANG BERJALAN');
            }
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
