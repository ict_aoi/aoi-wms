<?php namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Http\Controllers\ERPController;

use App\Models\Scheduler;

class DailyInsertPurchaseItems extends Command
{
    protected $signature = 'dailyPurchaseItems:insert';
    protected $description = 'Insert Purchase Items';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {

        $is_schedule_purchase_on_going = Scheduler::where('job','SYNC_PURCHASE_ITEM')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_purchase_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job' => 'SYNC_PURCHASE_ITEM',
                'status' => 'ongoing'
            ]);
            $this->info('SYNC PURCHASE ITEM JOB AT '.carbon::now());
            $this->setStartJob($new_scheduler);
            ERPController::dailyPurchaseItems();
            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
            $this->info('DONE PURCHASE ITEM JOB AT '.carbon::now());
        }else
        {
            $this->info('SYNC PURCHASE ITEM SEDANG BERJALAN');
        }

        $is_schedule_fab_on_going = Scheduler::where('job','SYNC_PURCHASE_ITEM_FAB')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_fab_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job' => 'SYNC_PURCHASE_ITEM_FAB',
                'status' => 'ongoing'
            ]);
            $this->info('SYNC PURCHASE ITEM FAB JOB AT '.carbon::now());
            $this->setStartJob($new_scheduler);
            ERPController::dailyInsertPurchaseAllocationItems();
            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
            $this->info('DONE PURCHASE ITEM FAB JOB AT '.carbon::now());
        }else{
            $this->info('SYNC PURCHASE ITEM FAB SEDANG BERJALAN');
        }
    }

    private function setStatus($scheduler,$status)
    {
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler)
    {
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
