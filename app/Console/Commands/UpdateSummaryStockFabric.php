<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\MaterialStockFabricController;

class UpdateSummaryStockFabric extends Command
{
    protected $signature = 'incaseSummaryFabric:update';
    protected $description = 'update summary stock fabric incase';
    
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        MaterialStockFabricController::update_summary_stock_fabric();
        $this->info('Incase Update has been successfully');
    }
}
