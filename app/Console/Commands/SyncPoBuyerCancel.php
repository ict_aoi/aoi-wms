<?php namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Scheduler;
use App\Models\Temporary;


use App\Http\Controllers\PoBuyerController;

class SyncPoBuyerCancel extends Command
{
    protected $signature = 'sync:pobuyerCancel';
    protected $description = 'Sinkron po buyer cancel';
    
    public function __construct(){
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','sync_cancel_po_buyer')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going){
            $new_scheduler = Scheduler::create([
                'job'       => 'sync_cancel_po_buyer',
                'status'    => 'ongoing'
            ]);
            $this->info('SYNC PO BUYER CANCEL START JOB AT '.carbon::now());
            $this->setStartJob($new_scheduler);
            PoBuyerController::syncPoCancel();
            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
            $this->info('SYNC PO BUYER CANCEL END JOB AT '.carbon::now());
        }else{
            $this->info('SYNC PO BUYER CANCEL SEDANG BERJALAN');
        }  
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
