<?php namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Scheduler;
use App\Models\MaterialCheck;


class resyncMasterItemInMaterialCheckFabric extends Command
{
    protected $signature    = 'resync:itemMaterialCheckFabric';
    protected $description  = 'resync master item in Monitoring Handover fabric';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $scheduler = Scheduler::where('job','RESYNC_ITEM_MATERIAL_CHECK_FABRIC')
        ->where('status','queue')
        ->first();
        
        if(!empty($scheduler))
        {
            $this->setStatus($scheduler,'ongoing');
            $this->setStartJob($scheduler);
            
            $this->info('RESYNC MASTER ITEM MATERIAL CHECK FABRIC START JOB AT '.carbon::now());
            $this->updateData();
            $this->info('SYNC MASTER ITEM MATERIAL CHECK FABRIC  END JOB AT '.carbon::now());

            $this->setStatus($scheduler,'done');
            $this->setEndJob($scheduler);
            
        }else
        {
            $is_schedule_on_going = Scheduler::where('job','RESYNC_ITEM_MATERIAL_CHECK_FABRIC')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going)
            {
                $new_scheduler = Scheduler::create([
                    'job' => 'RESYNC_ITEM_MATERIAL_CHECK_FABRIC',
                    'status' => 'ongoing'
                ]);
                $this->setStartJob($new_scheduler);
                
                $this->info('SYNC MASTER ITEM MATERIAL CHECK FABRIC  START JOB AT '.carbon::now());
                $this->updateData();
                $this->info('SYNC MASTER ITEM MATERIAL CHECK FABRIC  END JOB AT '.carbon::now());

                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
            }else{
                $this->info('SYNC MASTER ITEM MATERIAL CHECK FABRIC  SEDANG BERJALAN');
            }
        }  
    }

    private function updateData()
    {
        $data = DB::table('resync_master_item_in_material_checks_fabrics_v')->get();
        foreach ($data as $key => $value) 
        {
            $id     = $value->id;
            $item   = MaterialCheck::find($id);

            $item->item_code            = $value->item_code_master;
            $item->item_desc            = $value->desc_master;
            $item->color                = $value->color_master;
            $item->width_on_barcode     = $value->width_master;
            $item->save();
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
