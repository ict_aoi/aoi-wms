<?php namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Http\Controllers\ERPController;

use App\Models\Scheduler;

class SyncUomConversion extends Command
{
    protected $signature = 'sync:uomConversion';
    protected $description = 'Sinkron uom conversion';
    
    public function __construct(){
        parent::__construct();
    }

    public function handle()
    {
        $scheduler = Scheduler::where('job','SYNC_UOM')
        ->where('status','queue')
        ->first();
        
        if(!empty($scheduler))
        {
            $this->info('SYNC UOM START JOB AT '.carbon::now());
            $this->setStatus($scheduler,'ongoing');
            $this->setStartJob($scheduler);
            ERPController::uomCronInsert();
            $this->setStatus($scheduler,'done');
            $this->setEndJob($scheduler);
            $this->info('SYNC UOM END JOB AT '.carbon::now());
        }else
        {
            $is_schedule_supplier_on_going = Scheduler::where('job','SYNC_UOM')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_supplier_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_UOM',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC UOM START JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                ERPController::uomCronInsert();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('SYNC UOM END JOB AT '.carbon::now());
            }else{
                $this->info('SYNC UOM SEDANG BERJALAN');
            }
        }    
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
