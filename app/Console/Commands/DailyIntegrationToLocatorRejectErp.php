<?php namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\User;
use App\Models\Locator;
use App\Models\Scheduler;
use App\Models\MaterialStock;
use App\Models\MaterialCheck;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\MaterialPreparation;
use App\Models\MaterialMovementLine;
use App\Models\MaterialRollHandoverFabric;

class DailyIntegrationToLocatorRejectErp extends Command
{
    protected $signature    = 'dailyIntegrationToLocatorReject:daily';
    protected $description  = 'daily insert to locator reject erp';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','SYNC_INTEGRATION_TO_LOCATOR_REJECT')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job'       => 'SYNC_INTEGRATION_TO_LOCATOR_REJECT',
                'status'    => 'ongoing'
            ]);
            $this->setStartJob($new_scheduler);
            
            $this->info('SYNC DAILY INTEGRATION TO LOCATOR REJECT START JOB AT '.carbon::now());
            $this->accessoriesMoveFromInventoryToRejectErp();
            $this->fabricMoveFromInventoryToRejectErp();
            $this->info('SYNC DAILY INTEGRATION TO LOCATOR REJECT END JOB AT '.carbon::now());

            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
        }else{
            $this->info('SYNC DAILY INTEGRATION TO LOCATOR REJECT SEDANG BERJALAN');
        }  
    }

    private function accessoriesMoveFromInventoryToRejectErp()
    {
        $data = MaterialCheck::where('is_reject',true)
        ->whereNull('insert_to_locator_reject_erp_date')
        ->whereNotNull('material_preparation_id')
        ->get();
       
        
            $movement_date = carbon::now()->todatetimestring();

            foreach ($data as $key => $value) 
            {
                try 
                {
                    DB::beginTransaction();

                    $material_preparation_id    = $value->material_preparation_id;
                    $material_preparation       = MaterialPreparation::find($material_preparation_id);
                    $po_buyer                   = $material_preparation->po_buyer;
                    $item_code                  = $material_preparation->item_code;
                    $item_id                    = $material_preparation->item_id;
                    $warehouse_id               = $material_preparation->warehouse;
                    $document_no                = $material_preparation->document_no;
                    $c_order_id                 = $material_preparation->c_order_id;
                    $c_bpartner_id              = $material_preparation->c_bpartner_id;
                    $uom                        = $material_preparation->uom_conversion;
                    $po_detail_id               = $material_preparation->po_detail_id;
                    $created_at                 = $value->created_at;
                    $type_po                    = $material_preparation->type_po;
                    $user_id                    = $value->user_id;
                    $user_name                  = strtoupper($value->user->name);
                    $remark                     = trim(strtoupper($value->remark));
                    $qty_reject                 = sprintf('%0.8f',$value->qty_reject);
                    $supplier_name              = $material_preparation->supplier_name;
                    
                    $material_arrival           = MaterialArrival::whereNull('material_subcont_id')
                    ->whereNull('material_roll_handover_fabric_id')
                    ->where('po_detail_id',$po_detail_id)
                    ->first();

                    $no_invoice                 = ($material_arrival ? $material_arrival->no_invoice : null);
                    $no_packing_list            = ($material_arrival ? $material_arrival->no_packing_list : null);
                    $c_orderline_id             = ($material_arrival ? $material_arrival->c_orderline_id : null);
                    $warehouse_arrival_id       = ($material_arrival ? $material_arrival->warehouse_id : $warehouse_id);

                    if($warehouse_arrival_id != $warehouse_id)
                    {
                        $from_location = Locator::whereHas('area',function ($query) use($warehouse_arrival_id)
                        {
                            $query->where('is_active',true);
                            $query->where('warehouse',$warehouse_arrival_id);
                            $query->where('name','ERP INVENTORY');
                        })
                        ->first();

                        $to_location = Locator::whereHas('area',function ($query) use($warehouse_arrival_id)
                        {
                            $query->where('is_active',true);
                            $query->where('warehouse',$warehouse_arrival_id);
                            $query->where('name','ERP REJECT');
                        })
                        ->first();

                        $system = User::where([
                            ['name','system'],
                            ['warehouse',$warehouse_arrival_id]
                        ])
                        ->first();

                        if($c_order_id != 'FREE STOCK' && $po_detail_id != 'FREE STOCK')
                        {
                            if($from_location && $to_location)
                            {
                                // 1. buat nambah stock, untuk motong rma di warehouse awal
                                $inventory_erp = Locator::with('area')
                                ->where([
                                    ['is_active',true],
                                    ['rack','ERP INVENTORY'],
                                ])
                                ->whereHas('area',function ($query) use($warehouse_arrival_id)
                                {
                                    $query->where('is_active',true);
                                    $query->where('warehouse',$warehouse_arrival_id);
                                    $query->where('name','ERP INVENTORY');
                                })
                                ->first();

                                $is_movement_integration_header_exists = MaterialMovement::where([
                                    ['from_location',$inventory_erp->id],
                                    ['to_destination',$inventory_erp->id],
                                    ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                    ['to_locator_erp_id',$inventory_erp->area->erp_id],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                    ['no_packing_list',$no_packing_list],
                                    ['no_invoice',$no_invoice],
                                    ['status','integration-to-inventory-erp'],
                                ])
                                ->first();
            
                                if(!$is_movement_integration_header_exists)
                                {
                                    $movement_integration = MaterialMovement::firstOrCreate([
                                        'from_location'         => $inventory_erp->id,
                                        'to_destination'        => $inventory_erp->id,
                                        'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                        'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                        'is_integrate'          => false,
                                        'is_active'             => true,
                                        'no_packing_list'       => $no_packing_list,
                                        'no_invoice'            => $no_invoice,
                                        'status'                => 'integration-to-inventory-erp',
                                        'created_at'            => $created_at,
                                        'updated_at'            => $created_at,
                                    ]);
            
                                    $material_movement_integration_id = $movement_integration->id;
                                }else
                                {
                                    $is_movement_integration_header_exists->updated_at = $created_at;
                                    $is_movement_integration_header_exists->save();
            
                                    $material_movement_integration_id = $is_movement_integration_header_exists->id;
                                }
            
                                $is_material_movement_line_integration_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                ->where([
                                    ['material_movement_id',$material_movement_integration_id],
                                    ['item_id',$item_id],
                                    ['warehouse_id',$warehouse_arrival_id],
                                    ['material_preparation_id',$material_preparation_id],
                                    ['qty_movement',$qty_reject],
                                    ['date_movement',$created_at],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                ])
                                ->exists();
            
                                if(!$is_material_movement_line_integration_exists)
                                {
                                    MaterialMovementLine::Create([
                                        'material_movement_id'          => $material_movement_integration_id,
                                        'material_preparation_id'       => $material_preparation_id,
                                        'item_code'                     => $item_code,
                                        'item_id'                       => $item_id,
                                        'c_order_id'                    => $c_order_id,
                                        'c_orderline_id'                => $c_orderline_id,
                                        'c_bpartner_id'                 => $c_bpartner_id,
                                        'supplier_name'                 => $supplier_name,
                                        'type_po'                       => 2,
                                        'is_integrate'                  => false,
                                        'uom_movement'                  => $uom,
                                        'qty_movement'                  => $qty_reject,
                                        'date_movement'                 => $created_at,
                                        'created_at'                    => $created_at,
                                        'updated_at'                    => $created_at,
                                        'date_receive_on_destination'   => $created_at,
                                        'nomor_roll'                    => '-',
                                        'warehouse_id'                  => $warehouse_arrival_id,
                                        'document_no'                   => $document_no,
                                        'note'                          => 'PROSES INTEGRASI UNTUK MENAMBAHKAN STOCK DI ERP, DIKARNAKAN HASIL QC RESULTNYA REJECT DAN POSISI WAREHOUSE BERBEDA DENGAN KEDATANGAN',
                                        'is_active'                     => true,
                                        'user_id'                       => $system->id,
                                    ]);
                                }

                                // 2. motong stock di warehouse sesuai dengan penerimaan, agar bisa dibuatkan rma
                                $is_movement_exists = MaterialMovement::where([
                                    ['from_location',$from_location->id],
                                    ['to_destination',$to_location->id],
                                    ['from_locator_erp_id',$from_location->area->erp_id],
                                    ['to_locator_erp_id',$to_location->area->erp_id],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                    ['po_buyer',$po_buyer],
                                    ['no_invoice',$no_invoice],
                                    ['no_packing_list',$no_packing_list],
                                    ['status','integration-to-locator-reject-erp'],
                                ])
                                ->first();

                                if(!$is_movement_exists)
                                {
                                    $material_movement = MaterialMovement::firstOrCreate([
                                        'from_location'         => $from_location->id,
                                        'to_destination'        => $to_location->id,
                                        'from_locator_erp_id'   => $from_location->area->erp_id,
                                        'to_locator_erp_id'     => $to_location->area->erp_id,
                                        'is_integrate'          => false,
                                        'is_active'             => true,
                                        'po_buyer'              => $po_buyer,
                                        'no_invoice'            => $material_arrival->no_invoice,
                                        'no_packing_list'       => $material_arrival->no_packing_list,
                                        'status'                => 'integration-to-locator-reject-erp',
                                        'created_at'            => $created_at,
                                        'updated_at'            => $created_at,
                                    ]);

                                    $material_movement_id = $material_movement->id;
                                }else
                                {
                                    $is_movement_exists->created_at = $created_at;
                                    $is_movement_exists->updated_at = $created_at;
                                    $is_movement_exists->save();

                                    $material_movement_id = $is_movement_exists->id;
                                }
                                
                                $is_movement_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                ->where([
                                    ['material_movement_id',$material_movement_id],
                                    ['material_preparation_id',$material_preparation_id],
                                    ['warehouse_id',$warehouse_arrival_id],
                                    ['is_active',true],
                                    ['is_integrate',false],
                                    ['item_id',$item_id],
                                    ['date_movement',$created_at],
                                    ['qty_movement',$qty_reject],
                                ])
                                ->exists();
            
                                if(!$is_movement_line_exists)
                                {
                                    MaterialMovementLine::Create([
                                        'material_movement_id'                      => $material_movement_id,
                                        'material_preparation_id'                   => $material_preparation_id,
                                        'item_code'                                 => $item_code,
                                        'item_id'                                   => $item_id,
                                        'type_po'                                   => $type_po,
                                        'c_order_id'                                => $c_order_id,
                                        'c_orderline_id'                            => $c_orderline_id,
                                        'document_no'                               => $document_no,
                                        'c_bpartner_id'                             => $c_bpartner_id,
                                        'uom_movement'                              => $uom,
                                        'qty_movement'                              => $qty_reject,
                                        'date_movement'                             => $created_at,
                                        'created_at'                                => $created_at,
                                        'updated_at'                                => $created_at,
                                        'note'                                      => 'PROSES INTEGRASI KE LOCATOR REJECT ERP. PENYEBAB REJECTNYA ADALAH '.$remark.' DILAKUKAN OLEH '.$user_name.' DIGUNAKAN UNTUK PO BUYER '.$po_buyer,
                                        'is_active'                                 => true,
                                        'is_integrate'                              => false,
                                        'nomor_roll'                                => '-',
                                        'warehouse_id'                              => $warehouse_arrival_id,
                                        'supplier_name'                             => $supplier_name,
                                        'is_inserted_to_material_movement_per_size' => false,
                                        'user_id'                                   => $system->id,
                                    ]);
                                }
                                
                                // 3. motong stock di erp sesuai dengan warehouse stock nya,pake internal use
                                $system_current_warehouse = User::where([
                                    ['name','system'],
                                    ['warehouse',$warehouse_id]
                                ])
                                ->first();
                                
                                $inventory_erp_current_warehouse = Locator::with('area')
                                ->where([
                                    ['is_active',true],
                                    ['rack','ERP INVENTORY'],
                                ])
                                ->whereHas('area',function ($query) use($warehouse_id)
                                {
                                    $query->where('is_active',true);
                                    $query->where('warehouse',$warehouse_id);
                                    $query->where('name','ERP INVENTORY');
                                })
                                ->first();

                                $is_movement_integration_internal_use_exists = MaterialMovement::where([
                                    ['from_location',$inventory_erp_current_warehouse->id],
                                    ['to_destination',$inventory_erp_current_warehouse->id],
                                    ['from_locator_erp_id',$inventory_erp_current_warehouse->area->erp_id],
                                    ['to_locator_erp_id',$inventory_erp_current_warehouse->area->erp_id],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                    ['no_packing_list',$no_packing_list],
                                    ['no_invoice',$no_invoice],
                                    ['status','integration-to-inventory-erp'],
                                ])
                                ->first();
            
                                if(!$is_movement_integration_internal_use_exists)
                                {
                                    $movement_integration_internal_use = MaterialMovement::firstOrCreate([
                                        'from_location'         => $inventory_erp_current_warehouse->id,
                                        'to_destination'        => $inventory_erp_current_warehouse->id,
                                        'from_locator_erp_id'   => $inventory_erp_current_warehouse->area->erp_id,
                                        'to_locator_erp_id'     => $inventory_erp_current_warehouse->area->erp_id,
                                        'is_integrate'          => false,
                                        'is_active'             => true,
                                        'no_packing_list'       => $no_packing_list,
                                        'no_invoice'            => $no_invoice,
                                        'status'                => 'integration-to-inventory-erp',
                                        'created_at'            => $created_at,
                                        'updated_at'            => $created_at,
                                    ]);
            
                                    $material_movement_integration_internal_use_id = $movement_integration_internal_use->id;
                                }else
                                {
                                    $is_movement_integration_internal_use_exists->updated_at = $created_at;
                                    $is_movement_integration_internal_use_exists->save();
            
                                    $material_movement_integration_internal_use_id = $is_movement_integration_internal_use_exists->id;
                                }
            
                                $is_material_movement_line_integration_internal_use_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                ->where([
                                    ['material_movement_id',$material_movement_integration_internal_use_id],
                                    ['item_id',$item_id],
                                    ['warehouse_id',$warehouse_id],
                                    ['material_preparation_id',$material_preparation_id],
                                    ['qty_movement',-1*$qty_reject],
                                    ['date_movement',$created_at],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                ])
                                ->exists();
            
                                if(!$is_material_movement_line_integration_internal_use_exists)
                                {
                                    MaterialMovementLine::Create([
                                        'material_movement_id'          => $material_movement_integration_internal_use_id,
                                        'material_preparation_id'       => $material_preparation_id,
                                        'item_code'                     => $item_code,
                                        'item_id'                       => $item_id,
                                        'c_order_id'                    => $c_order_id,
                                        'c_orderline_id'                => $c_orderline_id,
                                        'c_bpartner_id'                 => $c_bpartner_id,
                                        'supplier_name'                 => $supplier_name,
                                        'type_po'                       => 2,
                                        'is_integrate'                  => false,
                                        'uom_movement'                  => $uom,
                                        'qty_movement'                  => -1*$qty_reject,
                                        'date_movement'                 => $created_at,
                                        'created_at'                    => $created_at,
                                        'updated_at'                    => $created_at,
                                        'date_receive_on_destination'   => $created_at,
                                        'nomor_roll'                    => '-',
                                        'warehouse_id'                  => $warehouse_id,
                                        'document_no'                   => $document_no,
                                        'note'                          => 'PROSES INTEGRASI UNTUK MEMOTONG STOCK DI ERP, DIKARNAKAN HASIL QC RESULTNYA REJECT DAN POSISI WAREHOUSE BERBEDA DENGAN KEDATANGAN',
                                        'is_active'                     => true,
                                        'user_id'                       => $system_current_warehouse->id,
                                    ]);
                                }

                                $value->insert_to_locator_reject_erp_date = $movement_date;
                            }
                        }else
                        {
                            $value->is_reject                           = false;
                            $value->insert_to_locator_reject_erp_date   = $movement_date;
                        }
    
                    }else
                    {
                        $from_location = Locator::whereHas('area',function ($query) use($warehouse_id)
                        {
                            $query->where('is_active',true);
                            $query->where('warehouse',$warehouse_id);
                            $query->where('name','ERP INVENTORY');
                        })
                        ->first();

                        $to_location = Locator::whereHas('area',function ($query) use($warehouse_id)
                        {
                            $query->where('is_active',true);
                            $query->where('warehouse',$warehouse_id);
                            $query->where('name','ERP REJECT');
                        })
                        ->first();

                        $system = User::where([
                            ['name','system'],
                            ['warehouse',$warehouse_id]
                        ])
                        ->first();
                        
                        if($c_order_id != 'FREE STOCK' && $po_detail_id != 'FREE STOCK')
                        {
                            if($from_location && $to_location)
                            {
                                $is_movement_exists = MaterialMovement::where([
                                    ['from_location',$from_location->id],
                                    ['to_destination',$to_location->id],
                                    ['from_locator_erp_id',$from_location->area->erp_id],
                                    ['to_locator_erp_id',$to_location->area->erp_id],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                    ['po_buyer',$po_buyer],
                                    ['no_invoice',$no_invoice],
                                    ['no_packing_list',$no_packing_list],
                                    ['status','integration-to-locator-reject-erp'],
                                ])
                                ->first();

                                if(!$is_movement_exists)
                                {
                                    $material_movement = MaterialMovement::firstOrCreate([
                                        'from_location'         => $from_location->id,
                                        'to_destination'        => $to_location->id,
                                        'from_locator_erp_id'   => $from_location->area->erp_id,
                                        'to_locator_erp_id'     => $to_location->area->erp_id,
                                        'is_integrate'          => false,
                                        'is_active'             => true,
                                        'po_buyer'              => $po_buyer,
                                        'no_invoice'            => $material_arrival->no_invoice,
                                        'no_packing_list'       => $material_arrival->no_packing_list,
                                        'status'                => 'integration-to-locator-reject-erp',
                                        'created_at'            => $created_at,
                                        'updated_at'            => $created_at,
                                    ]);

                                    $material_movement_id = $material_movement->id;
                                }else
                                {
                                    $is_movement_exists->created_at = $created_at;
                                    $is_movement_exists->updated_at = $created_at;
                                    $is_movement_exists->save();

                                    $material_movement_id = $is_movement_exists->id;
                                }
                                
                                $is_movement_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                ->where([
                                    ['material_movement_id',$material_movement_id],
                                    ['material_preparation_id',$material_preparation_id],
                                    ['is_active',true],
                                    ['is_integrate',false],
                                    ['item_id',$item_id],
                                    ['date_movement',$created_at],
                                    ['qty_movement',$qty_reject],
                                ])
                                ->exists();
            
                                if(!$is_movement_line_exists)
                                {
                                    MaterialMovementLine::Create([
                                        'material_movement_id'                      => $material_movement_id,
                                        'material_preparation_id'                   => $material_preparation_id,
                                        'item_code'                                 => $item_code,
                                        'item_id'                                   => $item_id,
                                        'type_po'                                   => $type_po,
                                        'c_order_id'                                => $c_order_id,
                                        'c_orderline_id'                            => $c_orderline_id,
                                        'document_no'                               => $document_no,
                                        'c_bpartner_id'                             => $c_bpartner_id,
                                        'uom_movement'                              => $uom,
                                        'qty_movement'                              => $qty_reject,
                                        'date_movement'                             => $created_at,
                                        'created_at'                                => $created_at,
                                        'updated_at'                                => $created_at,
                                        'note'                                      => 'PROSES INTEGRASI KE LOCATOR REJECT ERP. PENYEBAB REJECTNYA ADALAH '.$remark.' DILAKUKAN OLEH '.$user_name.' DIGUNAKAN UNTUK PO BUYER '.$po_buyer,
                                        'is_active'                                 => true,
                                        'is_integrate'                              => false,
                                        'nomor_roll'                                => '-',
                                        'warehouse_id'                              => $warehouse_id,
                                        'supplier_name'                             => $supplier_name,
                                        'is_inserted_to_material_movement_per_size' => false,
                                        'user_id'                                   => $system->id,
                                    ]);
                                }
                                

                                $value->insert_to_locator_reject_erp_date = $movement_date;
                            }
                        }else
                        {
                            $value->is_reject                           = false;
                            $value->insert_to_locator_reject_erp_date   = $movement_date;
                        }
                    }
                    
                    $value->save();

                    DB::commit();
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }

            
    }

    private function fabricMoveFromInventoryToRejectErp()
    {
        //1. reject karna short roll
        $data = MaterialStock::where('is_short_roll',true)
        ->whereNull('insert_to_locator_reject_erp_date_from_short_roll')
        ->whereNotNull('movement_to_locator_reject_date_from_short_roll')
        ->get();

        $movement_date = carbon::now()->todatetimestring();

        foreach ($data as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();

                $material_stock_id          = $value->id;
                $po_buyer                   = null;
                $item_code                  = $value->item_code;
                $item_id                    = $value->item_id;
                $warehouse_id               = $value->warehouse_id;
                $document_no                = $value->document_no;
                $c_bpartner_id              = $value->c_bpartner_id;
                $uom                        = $value->uom;
                $po_detail_id               = $value->po_detail_id;
                $nomor_roll                 = $value->nomor_roll;
                $supplier_name              = $value->supplier_name;
                $created_at                 = $value->movement_to_locator_reject_date_from_short_roll;
                $remark                     = 'SHORT ROLL';
                $qty_reject                 = sprintf('%0.8f',$value->qty_reject_by_short_roll);
                $type_po                    = $value->type_po;
                
                $material_arrival           =  MaterialArrival::whereNull('material_subcont_id')
                ->whereNull('material_roll_handover_fabric_id')
                ->where('po_detail_id',$po_detail_id)
                ->first();

                $c_order_id                 = ($material_arrival ? $material_arrival->c_order_id : null);
                $c_orderline_id             = ($material_arrival ? $material_arrival->c_orderline_id : null);
                $no_invoice                 = ($material_arrival ? $material_arrival->no_invoice : $value->no_invoice);
                $no_packing_list            = ($material_arrival ? $material_arrival->no_packing_list : $value->no_packing_list);
                $c_orderline_id             = ($material_arrival ? $material_arrival->c_orderline_id : null);
                $warehouse_arrival_id       = ($material_arrival ? $material_arrival->warehouse_id : $warehouse_id);
                

                if($warehouse_id != $warehouse_arrival_id)
                {
                    $from_location = Locator::whereHas('area',function ($query) use($warehouse_arrival_id)
                    {
                        $query->where('is_active',true);
                        $query->where('warehouse',$warehouse_arrival_id);
                        $query->where('name','ERP INVENTORY');
                    })
                    ->first();
    
                    $to_location = Locator::whereHas('area',function ($query) use($warehouse_arrival_id)
                    {
                        $query->where('is_active',true);
                        $query->where('warehouse',$warehouse_arrival_id);
                        $query->where('name','ERP REJECT');
                    })
                    ->first();
    
                    $system = User::where([
                        ['name','system'],
                        ['warehouse',$warehouse_arrival_id]
                    ])
                    ->first();
    
                    if($from_location && $to_location && $no_invoice && $no_packing_list && $po_detail_id != 'FREE STOCK')
                    {
                        // 1. ditambahkan stocknya untuk whs asal nya dlu
                        $inventory_erp = Locator::with('area')
                        ->where([
                            ['is_active',true],
                            ['rack','ERP INVENTORY'],
                        ])
                        ->whereHas('area',function ($query) use($warehouse_arrival_id)
                        {
                            $query->where('is_active',true);
                            $query->where('warehouse',$warehouse_arrival_id);
                            $query->where('name','ERP INVENTORY');
                        })
                        ->first();

                        $is_movement_integration_header_exists = MaterialMovement::where([
                            ['from_location',$inventory_erp->id],
                            ['to_destination',$inventory_erp->id],
                            ['from_locator_erp_id',$inventory_erp->area->erp_id],
                            ['to_locator_erp_id',$inventory_erp->area->erp_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['no_packing_list',$no_packing_list],
                            ['no_invoice',$no_invoice],
                            ['status','integration-to-inventory-erp'],
                        ])
                        ->first();
    
                        if(!$is_movement_integration_header_exists)
                        {
                            $movement_integration = MaterialMovement::firstOrCreate([
                                'from_location'         => $inventory_erp->id,
                                'to_destination'        => $inventory_erp->id,
                                'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                'is_integrate'          => false,
                                'is_active'             => true,
                                'no_packing_list'       => $no_packing_list,
                                'no_invoice'            => $no_invoice,
                                'status'                => 'integration-to-inventory-erp',
                                'created_at'            => $created_at,
                                'updated_at'            => $created_at,
                            ]);
    
                            $material_movement_integration_id = $movement_integration->id;
                        }else
                        {
                            $is_movement_integration_header_exists->updated_at = $created_at;
                            $is_movement_integration_header_exists->save();
    
                            $material_movement_integration_id = $is_movement_integration_header_exists->id;
                        }
    
                        $is_material_movement_line_integration_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                        ->where([
                            ['material_movement_id',$material_movement_integration_id],
                            ['item_id',$item_id],
                            ['warehouse_id',$warehouse_arrival_id],
                            ['material_stock_id',$material_stock_id],
                            ['qty_movement',$qty_reject],
                            ['date_movement',$created_at],
                            ['is_integrate',false],
                            ['is_active',true],
                        ])
                        ->exists();
    
                        if(!$is_material_movement_line_integration_exists)
                        {
                            MaterialMovementLine::Create([
                                'material_movement_id'          => $material_movement_integration_id,
                                'material_stock_id'             => $material_stock_id,
                                'item_code'                     => $item_code,
                                'item_id'                       => $item_id,
                                'c_order_id'                    => $c_order_id,
                                'c_orderline_id'                => $c_orderline_id,
                                'c_bpartner_id'                 => $c_bpartner_id,
                                'supplier_name'                 => $supplier_name,
                                'type_po'                       => 1,
                                'is_integrate'                  => false,
                                'uom_movement'                  => $uom,
                                'qty_movement'                  => $qty_reject,
                                'date_movement'                 => $created_at,
                                'created_at'                    => $created_at,
                                'updated_at'                    => $created_at,
                                'date_receive_on_destination'   => $created_at,
                                'nomor_roll'                    => '-',
                                'warehouse_id'                  => $warehouse_arrival_id,
                                'document_no'                   => $document_no,
                                'note'                          => 'PROSES INTEGRASI UNTUK MENAMBAHKAN STOCK DI ERP, DIKARNAKAN HASIL QC RESULTNYA SHORT ROLL DAN POSISI WAREHOUSE BERBEDA DENGAN KEDATANGAN',
                                'is_active'                     => true,
                                'user_id'                       => $system->id,
                            ]);
                        }

                        // 2. integration rma 
                        $is_movement_exists = MaterialMovement::where([
                            ['from_location',$from_location->id],
                            ['to_destination',$to_location->id],
                            ['from_locator_erp_id',$from_location->area->erp_id],
                            ['to_locator_erp_id',$to_location->area->erp_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['po_buyer',$po_buyer],
                            ['no_invoice',$no_invoice],
                            ['no_packing_list',$no_packing_list],
                            ['status','integration-to-locator-reject-erp'],
                        ])
                        ->first();
    
                        if(!$is_movement_exists)
                        {
                            $material_movement = MaterialMovement::firstOrCreate([
                                'from_location'         => $from_location->id,
                                'to_destination'        => $to_location->id,
                                'from_locator_erp_id'   => $from_location->area->erp_id,
                                'to_locator_erp_id'     => $to_location->area->erp_id,
                                'is_integrate'          => false,
                                'is_active'             => true,
                                'po_buyer'              => $po_buyer,
                                'no_invoice'            => $no_invoice,
                                'no_packing_list'       => $no_packing_list,
                                'status'                => 'integration-to-locator-reject-erp',
                                'created_at'            => $created_at,
                                'updated_at'            => $created_at,
                                'no_packing_list'       => $no_packing_list,
                                'no_invoice'            => $no_invoice,
                            ]);
    
                            $material_movement_id = $material_movement->id;
                        }else
                        {
                            $is_movement_exists->created_at = $created_at;
                            $is_movement_exists->updated_at = $created_at;
                            $is_movement_exists->save();
    
                            $material_movement_id = $is_movement_exists->id;
                        }
                        
                        $is_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                        ->where([
                            ['material_movement_id',$material_movement_id],
                            ['material_stock_id',$material_stock_id],
                            ['warehouse_id',$warehouse_arrival_id],
                            ['item_id',$item_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['qty_movement',$qty_reject],
                            ['date_movement',$created_at],
                        ])
                        ->exists();
    
                        if(!$is_line_exists)
                        {
                            MaterialMovementLine::Create([
                                'material_movement_id'                      => $material_movement_id,
                                'material_stock_id'                         => $material_stock_id,
                                'item_code'                                 => $item_code,
                                'item_id'                                   => $item_id,
                                'type_po'                                   => 1,
                                'c_order_id'                                => $c_order_id,
                                'c_orderline_id'                            => $c_orderline_id,
                                'document_no'                               => $document_no,
                                'nomor_roll'                                => $nomor_roll,
                                'c_bpartner_id'                             => $c_bpartner_id,
                                'uom_movement'                              => $uom,
                                'qty_movement'                              => $qty_reject,
                                'date_movement'                             => $created_at,
                                'created_at'                                => $created_at,
                                'updated_at'                                => $created_at,
                                'date_receive_on_destination'               => $created_at,
                                'note'                                      => 'PROSES INTEGRASI KE LOCATOR REJECT ERP. PENYEBAB REJECTNYA ADALAH SHORT ROLL BY QC',
                                'is_active'                                 => true,
                                'is_integrate'                              => false,
                                'warehouse_id'                              => $warehouse_arrival_id,
                                'supplier_name'                             => $supplier_name,
                                'is_inserted_to_material_movement_per_size' => false,
                                'user_id'                                   => $system->id,
                            ]);
                        }
                        
                        //3. di kurangi stocknya sesuai dengan warehouse saat ini
                        $system_current_warehouse = User::where([
                            ['name','system'],
                            ['warehouse',$warehouse_id]
                        ])
                        ->first();

                        $inventory_erp_current_warehouse = Locator::with('area')
                        ->where([
                            ['is_active',true],
                            ['rack','ERP INVENTORY'],
                        ])
                        ->whereHas('area',function ($query) use($warehouse_id)
                        {
                            $query->where('is_active',true);
                            $query->where('warehouse',$warehouse_id);
                            $query->where('name','ERP INVENTORY');
                        })
                        ->first();

                        $is_movement_integration_internal_use_exists = MaterialMovement::where([
                            ['from_location',$inventory_erp_current_warehouse->id],
                            ['to_destination',$inventory_erp_current_warehouse->id],
                            ['from_locator_erp_id',$inventory_erp_current_warehouse->area->erp_id],
                            ['to_locator_erp_id',$inventory_erp_current_warehouse->area->erp_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['no_packing_list',$no_packing_list],
                            ['no_invoice',$no_invoice],
                            ['status','integration-to-inventory-erp'],
                        ])
                        ->first();
    
                        if(!$is_movement_integration_internal_use_exists)
                        {
                            $movement_integration_internal_use = MaterialMovement::firstOrCreate([
                                'from_location'         => $inventory_erp_current_warehouse->id,
                                'to_destination'        => $inventory_erp_current_warehouse->id,
                                'from_locator_erp_id'   => $inventory_erp_current_warehouse->area->erp_id,
                                'to_locator_erp_id'     => $inventory_erp_current_warehouse->area->erp_id,
                                'is_integrate'          => false,
                                'is_active'             => true,
                                'no_packing_list'       => $no_packing_list,
                                'no_invoice'            => $no_invoice,
                                'status'                => 'integration-to-inventory-erp',
                                'created_at'            => $created_at,
                                'updated_at'            => $created_at,
                            ]);
    
                            $material_movement_integration_internal_use_id = $movement_integration_internal_use->id;
                        }else
                        {
                            $is_movement_integration_internal_use_exists->updated_at = $created_at;
                            $is_movement_integration_internal_use_exists->save();
    
                            $material_movement_integration_internal_use_id = $is_movement_integration_internal_use_exists->id;
                        }
    
                        $is_material_movement_line_integration_internal_use_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                        ->where([
                            ['material_movement_id',$material_movement_integration_internal_use_id],
                            ['item_id',$item_id],
                            ['warehouse_id',$warehouse_id],
                            ['material_stock_id',$material_stock_id],
                            ['qty_movement',-1*$qty_reject],
                            ['date_movement',$created_at],
                            ['is_integrate',false],
                            ['is_active',true],
                        ])
                        ->exists();
    
                        if(!$is_material_movement_line_integration_internal_use_exists)
                        {
                            MaterialMovementLine::Create([
                                'material_movement_id'          => $material_movement_integration_internal_use_id,
                                'material_stock_id'             => $material_stock_id,
                                'item_code'                     => $item_code,
                                'item_id'                       => $item_id,
                                'c_order_id'                    => $c_order_id,
                                'c_orderline_id'                => $c_orderline_id,
                                'c_bpartner_id'                 => $c_bpartner_id,
                                'supplier_name'                 => $supplier_name,
                                'type_po'                       => 1,
                                'is_integrate'                  => false,
                                'uom_movement'                  => $uom,
                                'qty_movement'                  => -1*$qty_reject,
                                'date_movement'                 => $created_at,
                                'created_at'                    => $created_at,
                                'updated_at'                    => $created_at,
                                'date_receive_on_destination'   => $created_at,
                                'nomor_roll'                    => '-',
                                'warehouse_id'                  => $warehouse_id,
                                'document_no'                   => $document_no,
                                'note'                          => 'PROSES INTEGRASI UNTUK MEMOTONG STOCK DI ERP, DIKARNAKAN HASIL QC RESULTNYA SHORT TOLL DAN POSISI WAREHOUSE BERBEDA DENGAN KEDATANGAN',
                                'is_active'                     => true,
                                'user_id'                       => $system_current_warehouse->id,
                            ]);
                        }
                        
                        $value->insert_to_locator_reject_erp_date_from_short_roll = $movement_date;
                    }
                }else
                {
                    $from_location = Locator::whereHas('area',function ($query) use($warehouse_id)
                    {
                        $query->where('is_active',true);
                        $query->where('warehouse',$warehouse_id);
                        $query->where('name','ERP INVENTORY');
                    })
                    ->first();
    
                    $to_location = Locator::whereHas('area',function ($query) use($warehouse_id)
                    {
                        $query->where('is_active',true);
                        $query->where('warehouse',$warehouse_id);
                        $query->where('name','ERP REJECT');
                    })
                    ->first();
    
                    $system = User::where([
                        ['name','system'],
                        ['warehouse',$warehouse_id]
                    ])
                    ->first();
    
                    if($from_location && $to_location && $no_invoice && $no_packing_list && $po_detail_id != 'FREE STOCK')
                    {
                        $is_movement_exists = MaterialMovement::where([
                            ['from_location',$from_location->id],
                            ['to_destination',$to_location->id],
                            ['from_locator_erp_id',$from_location->area->erp_id],
                            ['to_locator_erp_id',$to_location->area->erp_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['po_buyer',$po_buyer],
                            ['no_invoice',$no_invoice],
                            ['no_packing_list',$no_packing_list],
                            ['status','integration-to-locator-reject-erp'],
                        ])
                        ->first();
    
                        if(!$is_movement_exists)
                        {
                            $material_movement = MaterialMovement::firstOrCreate([
                                'from_location'         => $from_location->id,
                                'to_destination'        => $to_location->id,
                                'from_locator_erp_id'   => $from_location->area->erp_id,
                                'to_locator_erp_id'     => $to_location->area->erp_id,
                                'is_integrate'          => false,
                                'is_active'             => true,
                                'po_buyer'              => $po_buyer,
                                'no_invoice'            => $no_invoice,
                                'no_packing_list'       => $no_packing_list,
                                'status'                => 'integration-to-locator-reject-erp',
                                'created_at'            => $created_at,
                                'updated_at'            => $created_at,
                                'no_packing_list'       => $no_packing_list,
                                'no_invoice'            => $no_invoice,
                            ]);
    
                            $material_movement_id = $material_movement->id;
                        }else
                        {
                            $is_movement_exists->created_at = $created_at;
                            $is_movement_exists->updated_at = $created_at;
                            $is_movement_exists->save();
    
                            $material_movement_id = $is_movement_exists->id;
                        }
                        
                        $is_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                        ->where([
                            ['material_movement_id',$material_movement_id],
                            ['material_stock_id',$material_stock_id],
                            ['item_id',$item_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['qty_movement',$qty_reject],
                            ['date_movement',$created_at],
                        ])
                        ->exists();
    
                        if(!$is_line_exists)
                        {
                            MaterialMovementLine::Create([
                                'material_movement_id'                      => $material_movement_id,
                                'material_stock_id'                         => $material_stock_id,
                                'item_code'                                 => $item_code,
                                'item_id'                                   => $item_id,
                                'type_po'                                   => $type_po,
                                'c_order_id'                                => $c_order_id,
                                'c_orderline_id'                            => $c_orderline_id,
                                'document_no'                               => $document_no,
                                'nomor_roll'                                => $nomor_roll,
                                'c_bpartner_id'                             => $c_bpartner_id,
                                'uom_movement'                              => $uom,
                                'qty_movement'                              => $qty_reject,
                                'date_movement'                             => $created_at,
                                'created_at'                                => $created_at,
                                'updated_at'                                => $created_at,
                                'date_receive_on_destination'               => $created_at,
                                'note'                                      => 'PROSES INTEGRASI KE LOCATOR REJECT ERP. PENYEBAB REJECTNYA ADALAH SHORT ROLL BY QC',
                                'is_active'                                 => true,
                                'is_integrate'                              => false,
                                'warehouse_id'                              => $warehouse_id,
                                'supplier_name'                             => $supplier_name,
                                'is_inserted_to_material_movement_per_size' => false,
                                'user_id'                                   => $system->id,
                            ]);
                        }
    
                        $value->insert_to_locator_reject_erp_date_from_short_roll = $movement_date;
                    }
                }

                $value->save();

                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            
        }

        //2. reject karna RMA atau color not match
        $data = MaterialStock::where('is_reject_by_lot',true)
        ->whereNull('insert_to_locator_reject_erp_date_from_lot')
        ->whereNotNull('movement_to_locator_reject_date_from_lot')
        ->get();

        $movement_date = carbon::now()->todatetimestring();

        foreach ($data as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();

                $material_stock_id          = $value->id;
                $po_buyer                   = null;
                $item_code                  = $value->item_code;
                $item_id                    = $value->item_id;
                $warehouse_id               = $value->warehouse_id;
                $c_bpartner_id              = $value->c_bpartner_id;
                $uom                        = $value->uom;
                $po_detail_id               = $value->po_detail_id;
                $nomor_roll                 = $value->nomor_roll;
                $document_no                = $value->document_no;
                $supplier_name              = $value->supplier_name;
                $created_at                 = $value->movement_to_locator_reject_date_from_lot;
                $user_id                    = $value->user_lab_id;
                $user_name                  = strtoupper(($value->user_lab_id)? $value->userInspectLab->name : null);
                $remark                     = 'LOT TIDAK SESUAI'.($value->inspect_lab_remark ? ', ( '.$value->inspect_lab_remark.' )' : null);

                $qty_reject                 = sprintf('%0.8f',$value->qty_reject_non_by_inspect);
                $type_po                    = 1;
            
                $material_arrival           =  MaterialArrival::whereNull('material_subcont_id')
                ->whereNull('material_roll_handover_fabric_id')
                ->where('po_detail_id',$po_detail_id)
                ->first();

                $c_order_id                 = ($material_arrival ? $material_arrival->c_order_id : null);
                $c_orderline_id             = ($material_arrival ? $material_arrival->c_orderline_id : null);
                $no_invoice                 = ($material_arrival ? $material_arrival->no_invoice : $value->no_invoice);
                $no_packing_list            = ($material_arrival ? $material_arrival->no_packing_list : $value->no_packing_list);
                $c_orderline_id             = ($material_arrival ? $material_arrival->c_orderline_id : null);
                $warehouse_arrival_id       = ($material_arrival ? $material_arrival->warehouse_id : $warehouse_id);
                
                if($warehouse_id != $warehouse_arrival_id)
                {
                    $from_location = Locator::whereHas('area',function ($query) use($warehouse_arrival_id)
                    {
                        $query->where('is_active',true);
                        $query->where('warehouse',$warehouse_arrival_id);
                        $query->where('name','ERP INVENTORY');
                    })
                    ->first();
    
                    $to_location = Locator::whereHas('area',function ($query) use($warehouse_arrival_id)
                    {
                        $query->where('is_active',true);
                        $query->where('warehouse',$warehouse_arrival_id);
                        $query->where('name','ERP REJECT');
                    })
                    ->first();
    
                    $system = User::where([
                        ['name','system'],
                        ['warehouse',$warehouse_arrival_id]
                    ])
                    ->first();
    
                    if($from_location && $to_location && $no_invoice && $no_packing_list && $po_detail_id != 'FREE STOCK')
                    {
                        // 1. ditambahkan stocknya untuk whs asal nya dlu
                        $inventory_erp = Locator::with('area')
                        ->where([
                            ['is_active',true],
                            ['rack','ERP INVENTORY'],
                        ])
                        ->whereHas('area',function ($query) use($warehouse_arrival_id)
                        {
                            $query->where('is_active',true);
                            $query->where('warehouse',$warehouse_arrival_id);
                            $query->where('name','ERP INVENTORY');
                        })
                        ->first();

                        $is_movement_integration_header_exists = MaterialMovement::where([
                            ['from_location',$inventory_erp->id],
                            ['to_destination',$inventory_erp->id],
                            ['from_locator_erp_id',$inventory_erp->area->erp_id],
                            ['to_locator_erp_id',$inventory_erp->area->erp_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['no_packing_list',$no_packing_list],
                            ['no_invoice',$no_invoice],
                            ['status','integration-to-inventory-erp'],
                        ])
                        ->first();
    
                        if(!$is_movement_integration_header_exists)
                        {
                            $movement_integration = MaterialMovement::firstOrCreate([
                                'from_location'         => $inventory_erp->id,
                                'to_destination'        => $inventory_erp->id,
                                'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                'is_integrate'          => false,
                                'is_active'             => true,
                                'no_packing_list'       => $no_packing_list,
                                'no_invoice'            => $no_invoice,
                                'status'                => 'integration-to-inventory-erp',
                                'created_at'            => $created_at,
                                'updated_at'            => $created_at,
                            ]);
    
                            $material_movement_integration_id = $movement_integration->id;
                        }else
                        {
                            $is_movement_integration_header_exists->updated_at = $created_at;
                            $is_movement_integration_header_exists->save();
    
                            $material_movement_integration_id = $is_movement_integration_header_exists->id;
                        }
    
                        $is_material_movement_line_integration_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                        ->where([
                            ['material_movement_id',$material_movement_integration_id],
                            ['item_id',$item_id],
                            ['warehouse_id',$warehouse_arrival_id],
                            ['material_stock_id',$material_stock_id],
                            ['qty_movement',$qty_reject],
                            ['date_movement',$created_at],
                            ['is_integrate',false],
                            ['is_active',true],
                        ])
                        ->exists();
    
                        if(!$is_material_movement_line_integration_exists)
                        {
                            MaterialMovementLine::Create([
                                'material_movement_id'          => $material_movement_integration_id,
                                'material_stock_id'             => $material_stock_id,
                                'item_code'                     => $item_code,
                                'item_id'                       => $item_id,
                                'c_order_id'                    => $c_order_id,
                                'c_orderline_id'                => $c_orderline_id,
                                'c_bpartner_id'                 => $c_bpartner_id,
                                'supplier_name'                 => $supplier_name,
                                'type_po'                       => 1,
                                'is_integrate'                  => false,
                                'uom_movement'                  => $uom,
                                'qty_movement'                  => $qty_reject,
                                'date_movement'                 => $created_at,
                                'created_at'                    => $created_at,
                                'updated_at'                    => $created_at,
                                'date_receive_on_destination'   => $created_at,
                                'nomor_roll'                    => $nomor_roll,
                                'warehouse_id'                  => $warehouse_arrival_id,
                                'document_no'                   => $document_no,
                                'note'                          => 'PROSES INTEGRASI KE LOCATOR REJECT ERP. KARENA '.$remark.' MENGUNAKAN INTERNAL USE KARNA WAREHOUSE KEDATANGANNYA BEDA',
                                'is_active'                     => true,
                                'user_id'                       => $system->id,
                            ]);
                        }

                        // 2. integration rma sesuai dengan kedatangan awal
                        $is_movement_exists = MaterialMovement::where([
                            ['from_location',$from_location->id],
                            ['to_destination',$to_location->id],
                            ['from_locator_erp_id',$from_location->area->erp_id],
                            ['to_locator_erp_id',$to_location->area->erp_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['po_buyer',$po_buyer],
                            ['no_invoice',$no_invoice],
                            ['no_packing_list',$no_packing_list],
                            ['status','integration-to-locator-reject-erp'],
                        ])
                        ->first();
    
                        if(!$is_movement_exists)
                        {
                            $material_movement = MaterialMovement::firstOrCreate([
                                'from_location'         => $from_location->id,
                                'to_destination'        => $to_location->id,
                                'from_locator_erp_id'   => $from_location->area->erp_id,
                                'to_locator_erp_id'     => $to_location->area->erp_id,
                                'is_integrate'          => false,
                                'is_active'             => true,
                                'po_buyer'              => $po_buyer,
                                'no_invoice'            => $no_invoice,
                                'no_packing_list'       => $no_packing_list,
                                'status'                => 'integration-to-locator-reject-erp',
                                'created_at'            => $created_at,
                                'updated_at'            => $created_at,
                                'no_packing_list'       => $no_packing_list,
                                'no_invoice'            => $no_invoice,
                            ]);
    
                            $material_movement_id = $material_movement->id;
                        }else
                        {
                            $is_movement_exists->created_at = $created_at;
                            $is_movement_exists->updated_at = $created_at;
                            $is_movement_exists->save();
    
                            $material_movement_id = $is_movement_exists->id;
                        }
                        
                        $is_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                        ->where([
                            ['material_movement_id',$material_movement_id],
                            ['material_stock_id',$material_stock_id],
                            ['warehouse_id',$warehouse_arrival_id],
                            ['item_id',$item_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['qty_movement',$qty_reject],
                            ['date_movement',$created_at],
                        ])
                        ->exists();
    
                        if(!$is_line_exists)
                        {
                            MaterialMovementLine::Create([
                                'material_movement_id'                      => $material_movement_id,
                                'material_stock_id'                         => $material_stock_id,
                                'item_code'                                 => $item_code,
                                'item_id'                                   => $item_id,
                                'type_po'                                   => 1,
                                'c_order_id'                                => $c_order_id,
                                'c_orderline_id'                            => $c_orderline_id,
                                'document_no'                               => $document_no,
                                'nomor_roll'                                => $nomor_roll,
                                'c_bpartner_id'                             => $c_bpartner_id,
                                'uom_movement'                              => $uom,
                                'qty_movement'                              => $qty_reject,
                                'date_movement'                             => $created_at,
                                'created_at'                                => $created_at,
                                'updated_at'                                => $created_at,
                                'date_receive_on_destination'               => $created_at,
                                'note'                                      => 'PROSES INTEGRASI KE LOCATOR REJECT ERP. KARENA '.$remark.' MENGUNAKAN INTERNAL USE KARNA WAREHOUSE KEDATANGANNYA BEDA',
                                'is_active'                                 => true,
                                'is_integrate'                              => false,
                                'warehouse_id'                              => $warehouse_arrival_id,
                                'supplier_name'                             => $supplier_name,
                                'is_inserted_to_material_movement_per_size' => false,
                                'user_id'                                   => $system->id,
                            ]);
                        }
                        
                        //3. di kurangi stocknya sesuai dengan warehouse saat ini
                        $system_current_warehouse = User::where([
                            ['name','system'],
                            ['warehouse',$warehouse_id]
                        ])
                        ->first();

                        $inventory_erp_current_warehouse = Locator::with('area')
                        ->where([
                            ['is_active',true],
                            ['rack','ERP INVENTORY'],
                        ])
                        ->whereHas('area',function ($query) use($warehouse_id)
                        {
                            $query->where('is_active',true);
                            $query->where('warehouse',$warehouse_id);
                            $query->where('name','ERP INVENTORY');
                        })
                        ->first();

                        $is_movement_integration_internal_use_exists = MaterialMovement::where([
                            ['from_location',$inventory_erp_current_warehouse->id],
                            ['to_destination',$inventory_erp_current_warehouse->id],
                            ['from_locator_erp_id',$inventory_erp_current_warehouse->area->erp_id],
                            ['to_locator_erp_id',$inventory_erp_current_warehouse->area->erp_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['no_packing_list',$no_packing_list],
                            ['no_invoice',$no_invoice],
                            ['status','integration-to-inventory-erp'],
                        ])
                        ->first();
    
                        if(!$is_movement_integration_internal_use_exists)
                        {
                            $movement_integration_internal_use = MaterialMovement::firstOrCreate([
                                'from_location'         => $inventory_erp_current_warehouse->id,
                                'to_destination'        => $inventory_erp_current_warehouse->id,
                                'from_locator_erp_id'   => $inventory_erp_current_warehouse->area->erp_id,
                                'to_locator_erp_id'     => $inventory_erp_current_warehouse->area->erp_id,
                                'is_integrate'          => false,
                                'is_active'             => true,
                                'no_packing_list'       => $no_packing_list,
                                'no_invoice'            => $no_invoice,
                                'status'                => 'integration-to-inventory-erp',
                                'created_at'            => $created_at,
                                'updated_at'            => $created_at,
                            ]);
    
                            $material_movement_integration_internal_use_id = $movement_integration_internal_use->id;
                        }else
                        {
                            $is_movement_integration_internal_use_exists->updated_at = $created_at;
                            $is_movement_integration_internal_use_exists->save();
    
                            $material_movement_integration_internal_use_id = $is_movement_integration_internal_use_exists->id;
                        }
    
                        $is_material_movement_line_integration_internal_use_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                        ->where([
                            ['material_movement_id',$material_movement_integration_internal_use_id],
                            ['item_id',$item_id],
                            ['warehouse_id',$warehouse_id],
                            ['material_stock_id',$material_stock_id],
                            ['qty_movement',-1*$qty_reject],
                            ['date_movement',$created_at],
                            ['is_integrate',false],
                            ['is_active',true],
                        ])
                        ->exists();
    
                        if(!$is_material_movement_line_integration_internal_use_exists)
                        {
                            MaterialMovementLine::Create([
                                'material_movement_id'          => $material_movement_integration_internal_use_id,
                                'material_stock_id'             => $material_stock_id,
                                'item_code'                     => $item_code,
                                'item_id'                       => $item_id,
                                'c_order_id'                    => $c_order_id,
                                'c_orderline_id'                => $c_orderline_id,
                                'c_bpartner_id'                 => $c_bpartner_id,
                                'supplier_name'                 => $supplier_name,
                                'type_po'                       => 1,
                                'is_integrate'                  => false,
                                'uom_movement'                  => $uom,
                                'qty_movement'                  => -1*$qty_reject,
                                'date_movement'                 => $created_at,
                                'created_at'                    => $created_at,
                                'updated_at'                    => $created_at,
                                'date_receive_on_destination'   => $created_at,
                                'nomor_roll'                    => '-',
                                'warehouse_id'                  => $warehouse_id,
                                'document_no'                   => $document_no,
                                'note'                          => 'PROSES INTEGRASI KE LOCATOR REJECT ERP. KARENA '.$remark.' MENGUNAKAN INTERNAL USE KARNA WAREHOUSE KEDATANGANNYA BEDA',
                                'is_active'                     => true,
                                'user_id'                       => $system_current_warehouse->id,
                            ]);
                        }
                        
                        $value->insert_to_locator_reject_erp_date_from_lot = $movement_date;
                    }
                }else
                {
                    $from_location = Locator::whereHas('area',function ($query) use($warehouse_id)
                    {
                        $query->where('is_active',true);
                        $query->where('warehouse',$warehouse_id);
                        $query->where('name','ERP INVENTORY');
                    })
                    ->first();

                    $to_location = Locator::whereHas('area',function ($query) use($warehouse_id)
                    {
                        $query->where('is_active',true);
                        $query->where('warehouse',$warehouse_id);
                        $query->where('name','ERP REJECT');
                    })
                    ->first();

                    $system = User::where([
                        ['name','system'],
                        ['warehouse',$warehouse_id]
                    ])
                    ->first();

                    if($from_location && $to_location && $no_invoice && $no_packing_list && $po_detail_id != 'FREE STOCK')
                    {
                        $is_movement_exists = MaterialMovement::where([
                            ['from_location',$from_location->id],
                            ['to_destination',$to_location->id],
                            ['from_locator_erp_id',$from_location->area->erp_id],
                            ['to_locator_erp_id',$to_location->area->erp_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['po_buyer',$po_buyer],
                            ['no_invoice',$no_invoice],
                            ['no_packing_list',$no_packing_list],
                            ['status','integration-to-locator-reject-erp'],
                        ])
                        ->first();

                        if(!$is_movement_exists)
                        {
                            $material_movement = MaterialMovement::firstOrCreate([
                                'from_location'         => $from_location->id,
                                'to_destination'        => $to_location->id,
                                'from_locator_erp_id'   => $from_location->area->erp_id,
                                'to_locator_erp_id'     => $to_location->area->erp_id,
                                'is_integrate'          => false,
                                'is_active'             => true,
                                'po_buyer'              => $po_buyer,
                                'no_invoice'            => $no_invoice,
                                'no_packing_list'       => $no_packing_list,
                                'status'                => 'integration-to-locator-reject-erp',
                                'created_at'            => $created_at,
                                'updated_at'            => $created_at,
                                'no_packing_list'       => $no_packing_list,
                                'no_invoice'            => $no_invoice,
                            ]);

                            $material_movement_id = $material_movement->id;
                        }else
                        {
                            $is_movement_exists->created_at = $created_at;
                            $is_movement_exists->updated_at = $created_at;
                            $is_movement_exists->save();
                            $material_movement_id = $is_movement_exists->id;
                        }
                        

                        $is_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                        ->where([
                            ['material_movement_id',$material_movement_id],
                            ['material_stock_id',$material_stock_id],
                            ['item_id',$item_id],
                            ['warehouse_id',$warehouse_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['qty_movement',$qty_reject],
                            ['date_movement',$created_at],
                        ])
                        ->exists();

                        if(!$is_line_exists)
                        {
                            MaterialMovementLine::Create([
                                'material_movement_id'                      => $material_movement_id,
                                'material_stock_id'                         => $material_stock_id,
                                'item_code'                                 => $item_code,
                                'item_id'                                   => $item_id,
                                'type_po'                                   => 1,
                                'c_order_id'                                => $c_order_id,
                                'c_orderline_id'                            => $c_orderline_id,
                                'document_no'                               => $document_no,
                                'nomor_roll'                                => $nomor_roll,
                                'c_bpartner_id'                             => $c_bpartner_id,
                                'warehouse_id'                              => $warehouse_id,
                                'uom_movement'                              => $uom,
                                'qty_movement'                              => $qty_reject,
                                'date_movement'                             => $created_at,
                                'created_at'                                => $created_at,
                                'updated_at'                                => $created_at,
                                'date_receive_on_destination'               => $created_at,
                                'note'                                      => 'PROSES INTEGRASI KE LOCATOR REJECT ERP. '.$remark,
                                'is_active'                                 => true,
                                'is_integrate'                              => false,
                                'warehouse_id'                              => $warehouse_id,
                                'supplier_name'                             => $supplier_name,
                                'is_inserted_to_material_movement_per_size' => false,
                                'user_id'                                   => $system->id,
                            ]);
                        }
                        
                        $value->insert_to_locator_reject_erp_date_from_lot = $movement_date;
                    }
                }
                
                $value->save();
                
                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            
        }

           

        //2. reject karna inspect qc
        $data = MaterialStock::where('is_reject',true)
        ->whereNull('insert_to_locator_reject_erp_date_from_inspect')
        ->whereNotNull('movement_to_locator_reject_date_from_inspect')
        ->get();
        $movement_date = carbon::now()->todatetimestring();

        foreach ($data as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();
                $material_stock_id          = $value->id;
                $po_buyer                   = null;
                $item_code                  = $value->item_code;
                $item_id                    = $value->item_id;
                $warehouse_id               = $value->warehouse_id;
                $document_no                = $value->document_no;
                $c_order_id                 = $value->c_order_id;
                $c_bpartner_id              = $value->c_bpartner_id;
                $uom                        = $value->uom;
                $po_detail_id               = $value->po_detail_id;
                $type_po                    = $value->type_po;
                $nomor_roll                 = $value->nomor_roll;
                $supplier_name              = $value->supplier_name;

                $created_at                 = $value->movement_to_locator_reject_date_from_inspect;
                $remark                     = 'REJECT BY INSPECT QC';
                $qty_reject                 = sprintf('%0.8f',$value->qty_reject_by_inspect);
                $type_po                    = $value->type_po;
                
                $material_arrival           =  MaterialArrival::whereNull('material_subcont_id')
                ->whereNull('material_roll_handover_fabric_id')
                ->where('po_detail_id',$po_detail_id)
                ->first();
                
                $c_order_id                 = ($material_arrival ? $material_arrival->c_order_id : null);
                $c_orderline_id             = ($material_arrival ? $material_arrival->c_orderline_id : null);
                $no_invoice                 = ($material_arrival ? $material_arrival->no_invoice : $value->no_invoice);
                $no_packing_list            = ($material_arrival ? $material_arrival->no_packing_list : $value->no_packing_list);
                $c_orderline_id             = ($material_arrival ? $material_arrival->c_orderline_id : null);
                $warehouse_arrival_id       = ($material_arrival ? $material_arrival->warehouse_id : $warehouse_id);
                

                if($warehouse_id != $warehouse_arrival_id)
                {
                    $from_location = Locator::whereHas('area',function ($query) use($warehouse_arrival_id)
                    {
                        $query->where('is_active',true);
                        $query->where('warehouse',$warehouse_arrival_id);
                        $query->where('name','ERP INVENTORY');
                    })
                    ->first();
    
                    $to_location = Locator::whereHas('area',function ($query) use($warehouse_arrival_id)
                    {
                        $query->where('is_active',true);
                        $query->where('warehouse',$warehouse_arrival_id);
                        $query->where('name','ERP REJECT');
                    })
                    ->first();
    
                    $system = User::where([
                        ['name','system'],
                        ['warehouse',$warehouse_arrival_id]
                    ])
                    ->first();
    
                    if($from_location && $to_location && $no_invoice && $no_packing_list && $po_detail_id != 'FREE STOCK')
                    {
                        // 1. ditambahkan stocknya untuk whs asal nya dlu
                        $inventory_erp = Locator::with('area')
                        ->where([
                            ['is_active',true],
                            ['rack','ERP INVENTORY'],
                        ])
                        ->whereHas('area',function ($query) use($warehouse_arrival_id)
                        {
                            $query->where('is_active',true);
                            $query->where('warehouse',$warehouse_arrival_id);
                            $query->where('name','ERP INVENTORY');
                        })
                        ->first();

                        $is_movement_integration_header_exists = MaterialMovement::where([
                            ['from_location',$inventory_erp->id],
                            ['to_destination',$inventory_erp->id],
                            ['from_locator_erp_id',$inventory_erp->area->erp_id],
                            ['to_locator_erp_id',$inventory_erp->area->erp_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['no_packing_list',$no_packing_list],
                            ['no_invoice',$no_invoice],
                            ['status','integration-to-inventory-erp'],
                        ])
                        ->first();
    
                        if(!$is_movement_integration_header_exists)
                        {
                            $movement_integration = MaterialMovement::firstOrCreate([
                                'from_location'         => $inventory_erp->id,
                                'to_destination'        => $inventory_erp->id,
                                'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                'is_integrate'          => false,
                                'is_active'             => true,
                                'no_packing_list'       => $no_packing_list,
                                'no_invoice'            => $no_invoice,
                                'status'                => 'integration-to-inventory-erp',
                                'created_at'            => $created_at,
                                'updated_at'            => $created_at,
                            ]);
    
                            $material_movement_integration_id = $movement_integration->id;
                        }else
                        {
                            $is_movement_integration_header_exists->updated_at = $created_at;
                            $is_movement_integration_header_exists->save();
    
                            $material_movement_integration_id = $is_movement_integration_header_exists->id;
                        }
    
                        $is_material_movement_line_integration_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                        ->where([
                            ['material_movement_id',$material_movement_integration_id],
                            ['item_id',$item_id],
                            ['warehouse_id',$warehouse_arrival_id],
                            ['material_stock_id',$material_stock_id],
                            ['qty_movement',$qty_reject],
                            ['date_movement',$created_at],
                            ['is_integrate',false],
                            ['is_active',true],
                        ])
                        ->exists();
    
                        if(!$is_material_movement_line_integration_exists)
                        {
                            MaterialMovementLine::Create([
                                'material_movement_id'          => $material_movement_integration_id,
                                'material_stock_id'             => $material_stock_id,
                                'item_code'                     => $item_code,
                                'item_id'                       => $item_id,
                                'c_order_id'                    => $c_order_id,
                                'c_orderline_id'                => $c_orderline_id,
                                'c_bpartner_id'                 => $c_bpartner_id,
                                'supplier_name'                 => $supplier_name,
                                'type_po'                       => 1,
                                'is_integrate'                  => false,
                                'uom_movement'                  => $uom,
                                'qty_movement'                  => $qty_reject,
                                'date_movement'                 => $created_at,
                                'created_at'                    => $created_at,
                                'updated_at'                    => $created_at,
                                'date_receive_on_destination'   => $created_at,
                                'nomor_roll'                    => $nomor_roll,
                                'warehouse_id'                  => $warehouse_arrival_id,
                                'document_no'                   => $document_no,
                                'note'                          => 'PROSES INTEGRASI KE LOCATOR REJECT ERP. KARENA '.$remark.' MENGUNAKAN INTERNAL USE KARNA WAREHOUSE KEDATANGANNYA BEDA',
                                'is_active'                     => true,
                                'user_id'                       => $system->id,
                            ]);
                        }

                        // 2. integration rma sesuai dengan kedatangan awal
                        $is_movement_exists = MaterialMovement::where([
                            ['from_location',$from_location->id],
                            ['to_destination',$to_location->id],
                            ['from_locator_erp_id',$from_location->area->erp_id],
                            ['to_locator_erp_id',$to_location->area->erp_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['po_buyer',$po_buyer],
                            ['no_invoice',$no_invoice],
                            ['no_packing_list',$no_packing_list],
                            ['status','integration-to-locator-reject-erp'],
                        ])
                        ->first();
    
                        if(!$is_movement_exists)
                        {
                            $material_movement = MaterialMovement::firstOrCreate([
                                'from_location'         => $from_location->id,
                                'to_destination'        => $to_location->id,
                                'from_locator_erp_id'   => $from_location->area->erp_id,
                                'to_locator_erp_id'     => $to_location->area->erp_id,
                                'is_integrate'          => false,
                                'is_active'             => true,
                                'po_buyer'              => $po_buyer,
                                'no_invoice'            => $no_invoice,
                                'no_packing_list'       => $no_packing_list,
                                'status'                => 'integration-to-locator-reject-erp',
                                'created_at'            => $created_at,
                                'updated_at'            => $created_at,
                                'no_packing_list'       => $no_packing_list,
                                'no_invoice'            => $no_invoice,
                            ]);
    
                            $material_movement_id = $material_movement->id;
                        }else
                        {
                            $is_movement_exists->created_at = $created_at;
                            $is_movement_exists->updated_at = $created_at;
                            $is_movement_exists->save();
    
                            $material_movement_id = $is_movement_exists->id;
                        }
                        
                        $is_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                        ->where([
                            ['material_movement_id',$material_movement_id],
                            ['material_stock_id',$material_stock_id],
                            ['warehouse_id',$warehouse_arrival_id],
                            ['item_id',$item_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['qty_movement',$qty_reject],
                            ['date_movement',$created_at],
                        ])
                        ->exists();
    
                        if(!$is_line_exists)
                        {
                            MaterialMovementLine::Create([
                                'material_movement_id'                      => $material_movement_id,
                                'material_stock_id'                         => $material_stock_id,
                                'item_code'                                 => $item_code,
                                'item_id'                                   => $item_id,
                                'type_po'                                   => 1,
                                'c_order_id'                                => $c_order_id,
                                'c_orderline_id'                            => $c_orderline_id,
                                'document_no'                               => $document_no,
                                'nomor_roll'                                => $nomor_roll,
                                'c_bpartner_id'                             => $c_bpartner_id,
                                'uom_movement'                              => $uom,
                                'qty_movement'                              => $qty_reject,
                                'date_movement'                             => $created_at,
                                'created_at'                                => $created_at,
                                'updated_at'                                => $created_at,
                                'date_receive_on_destination'               => $created_at,
                                'note'                                      => 'PROSES INTEGRASI KE LOCATOR REJECT ERP. KARENA '.$remark.' MENGUNAKAN INTERNAL USE KARNA WAREHOUSE KEDATANGANNYA BEDA',
                                'is_active'                                 => true,
                                'is_integrate'                              => false,
                                'warehouse_id'                              => $warehouse_arrival_id,
                                'supplier_name'                             => $supplier_name,
                                'is_inserted_to_material_movement_per_size' => false,
                                'user_id'                                   => $system->id,
                            ]);
                        }
                        
                        //3. di kurangi stocknya sesuai dengan warehouse saat ini
                        $system_current_warehouse = User::where([
                            ['name','system'],
                            ['warehouse',$warehouse_id]
                        ])
                        ->first();

                        $inventory_erp_current_warehouse = Locator::with('area')
                        ->where([
                            ['is_active',true],
                            ['rack','ERP INVENTORY'],
                        ])
                        ->whereHas('area',function ($query) use($warehouse_id)
                        {
                            $query->where('is_active',true);
                            $query->where('warehouse',$warehouse_id);
                            $query->where('name','ERP INVENTORY');
                        })
                        ->first();

                        $is_movement_integration_internal_use_exists = MaterialMovement::where([
                            ['from_location',$inventory_erp_current_warehouse->id],
                            ['to_destination',$inventory_erp_current_warehouse->id],
                            ['from_locator_erp_id',$inventory_erp_current_warehouse->area->erp_id],
                            ['to_locator_erp_id',$inventory_erp_current_warehouse->area->erp_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['no_packing_list',$no_packing_list],
                            ['no_invoice',$no_invoice],
                            ['status','integration-to-inventory-erp'],
                        ])
                        ->first();
    
                        if(!$is_movement_integration_internal_use_exists)
                        {
                            $movement_integration_internal_use = MaterialMovement::firstOrCreate([
                                'from_location'         => $inventory_erp_current_warehouse->id,
                                'to_destination'        => $inventory_erp_current_warehouse->id,
                                'from_locator_erp_id'   => $inventory_erp_current_warehouse->area->erp_id,
                                'to_locator_erp_id'     => $inventory_erp_current_warehouse->area->erp_id,
                                'is_integrate'          => false,
                                'is_active'             => true,
                                'no_packing_list'       => $no_packing_list,
                                'no_invoice'            => $no_invoice,
                                'status'                => 'integration-to-inventory-erp',
                                'created_at'            => $created_at,
                                'updated_at'            => $created_at,
                            ]);
    
                            $material_movement_integration_internal_use_id = $movement_integration_internal_use->id;
                        }else
                        {
                            $is_movement_integration_internal_use_exists->updated_at = $created_at;
                            $is_movement_integration_internal_use_exists->save();
    
                            $material_movement_integration_internal_use_id = $is_movement_integration_internal_use_exists->id;
                        }
    
                        $is_material_movement_line_integration_internal_use_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                        ->where([
                            ['material_movement_id',$material_movement_integration_internal_use_id],
                            ['item_id',$item_id],
                            ['warehouse_id',$warehouse_id],
                            ['material_stock_id',$material_stock_id],
                            ['qty_movement',-1*$qty_reject],
                            ['date_movement',$created_at],
                            ['is_integrate',false],
                            ['is_active',true],
                        ])
                        ->exists();
    
                        if(!$is_material_movement_line_integration_internal_use_exists)
                        {
                            MaterialMovementLine::Create([
                                'material_movement_id'          => $material_movement_integration_internal_use_id,
                                'material_stock_id'             => $material_stock_id,
                                'item_code'                     => $item_code,
                                'item_id'                       => $item_id,
                                'c_order_id'                    => $c_order_id,
                                'c_orderline_id'                => $c_orderline_id,
                                'c_bpartner_id'                 => $c_bpartner_id,
                                'supplier_name'                 => $supplier_name,
                                'type_po'                       => 1,
                                'is_integrate'                  => false,
                                'uom_movement'                  => $uom,
                                'qty_movement'                  => -1*$qty_reject,
                                'date_movement'                 => $created_at,
                                'created_at'                    => $created_at,
                                'updated_at'                    => $created_at,
                                'date_receive_on_destination'   => $created_at,
                                'nomor_roll'                    => $nomor_roll,
                                'warehouse_id'                  => $warehouse_id,
                                'document_no'                   => $document_no,
                                'note'                          => 'PROSES INTEGRASI KE LOCATOR REJECT ERP. KARENA '.$remark.' MENGUNAKAN INTERNAL USE KARNA WAREHOUSE KEDATANGANNYA BEDA',
                                'is_active'                     => true,
                                'user_id'                       => $system_current_warehouse->id,
                            ]);
                        }
                        
                        $value->insert_to_locator_reject_erp_date_from_inspect = $movement_date;
                    }
                }else
                {
                    $from_location = Locator::whereHas('area',function ($query) use($warehouse_id)
                    {
                        $query->where('is_active',true);
                        $query->where('warehouse',$warehouse_id);
                        $query->where('name','ERP INVENTORY');
                    })
                    ->first();

                    $to_location = Locator::whereHas('area',function ($query) use($warehouse_id)
                    {
                        $query->where('is_active',true);
                        $query->where('warehouse',$warehouse_id);
                        $query->where('name','ERP REJECT');
                    })
                    ->first();

                    $system = User::where([
                        ['name','system'],
                        ['warehouse',$warehouse_id]
                    ])
                    ->first();

                    if($from_location && $to_location && $no_invoice && $no_packing_list && $po_detail_id != 'FREE STOCK')
                    {
                        $is_movement_exists = MaterialMovement::where([
                            ['from_location',$from_location->id],
                            ['to_destination',$to_location->id],
                            ['from_locator_erp_id',$from_location->area->erp_id],
                            ['to_locator_erp_id',$to_location->area->erp_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['po_buyer',$po_buyer],
                            ['no_invoice',$no_invoice],
                            ['no_packing_list',$no_packing_list],
                            ['status','integration-to-locator-reject-erp'],
                        ])
                        ->first();

                        if(!$is_movement_exists)
                        {
                            $material_movement = MaterialMovement::firstOrCreate([
                                'from_location'         => $from_location->id,
                                'to_destination'        => $to_location->id,
                                'from_locator_erp_id'   => $from_location->area->erp_id,
                                'to_locator_erp_id'     => $to_location->area->erp_id,
                                'is_integrate'          => false,
                                'is_active'             => true,
                                'po_buyer'              => $po_buyer,
                                'no_invoice'            => $no_invoice,
                                'no_packing_list'       => $no_packing_list,
                                'status'                => 'integration-to-locator-reject-erp',
                                'created_at'            => $created_at,
                                'updated_at'            => $created_at,
                                'no_packing_list'       => $no_packing_list,
                                'no_invoice'            => $no_invoice,
                            ]);

                            $material_movement_id = $material_movement->id;
                        }else
                        {
                            $is_movement_exists->updated_at = $created_at;
                            $is_movement_exists->save();
                            
                            $material_movement_id = $is_movement_exists->id;
                        }
                        
                        $is_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                        ->where([
                            ['material_movement_id',$material_movement_id],
                            ['material_stock_id',$material_stock_id],
                            ['item_id',$item_id],
                            ['warehouse_id',$warehouse_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['date_movement',$created_at],
                            ['qty_movement',$qty_reject],
                        ])
                        ->exists();

                        if(!$is_line_exists)
                        {
                            MaterialMovementLine::Create([
                                'material_movement_id'                      => $material_movement_id,
                                'material_stock_id'                         => $material_stock_id,
                                'item_code'                                 => $item_code,
                                'item_id'                                   => $item_id,
                                'type_po'                                   => 1,
                                'c_order_id'                                => $c_order_id,
                                'c_orderline_id'                            => $c_orderline_id,
                                'document_no'                               => $document_no,
                                'nomor_roll'                                => $nomor_roll,
                                'c_bpartner_id'                             => $c_bpartner_id,
                                'warehouse_id'                              => $warehouse_id,
                                'uom_movement'                              => $uom,
                                'qty_movement'                              => $qty_reject,
                                'date_movement'                             => $created_at,
                                'created_at'                                => $created_at,
                                'updated_at'                                => $created_at,
                                'date_receive_on_destination'               => $created_at,
                                'note'                                      => 'PROSES INTEGRASI KE LOCATOR REJECT ERP, '.$remark,
                                'is_active'                                 => true,
                                'is_integrate'                              => false,
                                'warehouse_id'                              => $warehouse_id,
                                'supplier_name'                             => $supplier_name,
                                'is_inserted_to_material_movement_per_size' => false,
                                'user_id'                                   => $system->id,
                            ]);
                        }

                        $value->insert_to_locator_reject_erp_date_from_inspect  = $movement_date;
                    }
                }

                $value->save();

                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            
        }

            
    }

    private function setStatus($scheduler,$status)
    {
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler)
    {
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler)
    {
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }

}
