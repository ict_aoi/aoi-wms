<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Carbon\Carbon;
use App\Models\Scheduler;
use App\Models\CaptureMaterialStock;

class DailyCaptureMaterialStock extends Command
{
    
    protected $signature = 'dailyCaptureMaterialStock:insert';
    protected $description = 'insert capture material stock';


    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','SYNC_CAPTURE_MATERIAL_STOCK')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job' => 'SYNC_CAPTURE_MATERIAL_STOCK',
                'status' => 'ongoing'
            ]);
            $this->setStartJob($new_scheduler);
            
            $this->info('SYNC CAPTURE MATERIAL STOCK START JOB AT '.carbon::now());
            
            //ambil detail stock acc
            $material_stock_accs = db::table('detail_stock_per_item_acc_v')
                                   ->where('qty', '>', '0')
                                   ->get();
            
            //masukin aja ketabel 
            $date = Carbon::now();
            foreach($material_stock_accs as $key => $material_stock_acc)
            {
                CaptureMaterialStock::FirstOrCreate([
                    'warehouse'    => $material_stock_acc->warehouse,
                    'locator'      => $material_stock_acc->locator,
                    'document_no'  => $material_stock_acc->document_no,
                    'po_buyer'     => $material_stock_acc->po_buyer,
                    'item_code'    => $material_stock_acc->item_code,
                    'item_id'      => $material_stock_acc->item_id,
                    'category'     => $material_stock_acc->category,
                    'uom'          => $material_stock_acc->uom,
                    'qty'          => $material_stock_acc->qty,
                    'status_stock' => $material_stock_acc->status_stock,
                    'created_at'   => $date,
                    'updated_at'   => $date,
                ]);

            }

            $material_stock_fabs = db::table('detail_stock_per_item_fab_v')
                                   ->where('qty', '>', '0')
                                   ->get();
            
            foreach($material_stock_fabs as $key => $material_stock_fab)
            {
                CaptureMaterialStock::FirstOrCreate([
                    'warehouse'     => $material_stock_fab->warehouse,
                    'locator'       => $material_stock_fab->locator,
                    'planning_date' => $material_stock_fab->planning_date,
                    'document_no'   => $material_stock_fab->document_no,
                    'nomor_roll'    => $material_stock_fab->nomor_roll,
                    'barcode'       => $material_stock_fab->barcode,
                    'item_code'     => $material_stock_fab->item_code,
                    'item_id'       => $material_stock_fab->item_id,
                    'category'      => $material_stock_fab->category,
                    'uom'           => $material_stock_fab->uom,
                    'qty'           => $material_stock_fab->qty,
                    'status_stock'  => $material_stock_fab->status_stock,
                    'created_at'    => $date,
                    'updated_at'    => $date,
                ]);

            }



            $this->info('SYNC CAPTURE MATERIAL STOCK END JOB AT '.carbon::now());

            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
        }else{
            $this->info('SYNC CAPTURE MATERIAL STOCK SEDANG BERJALAN');
        } 
    }

    private function setStatus($scheduler,$status)
    {
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler)
    {
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler)
    {
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
