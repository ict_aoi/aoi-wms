<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\MaterialStockController;

class UpdateStockIncase extends Command
{
    protected $signature = 'incasestock:update';
    protected $description = 'update stock incase';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        MaterialStockController::updateStock();
        $this->info('Incase Update has been successfully');
    }
}
