<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        Commands\DailyInsertPoBuyerPerRoll::class,
        Commands\DailyMonitoringMaterial::class,
        Commands\IntegrationConvertFromOutIssueToInternalUse::class,
        Commands\UpdateVarianceStockFab::class,
        Commands\UpdateVarianceStockAcc::class,
        Commands\resycnMasterItemAllocationItem::class,
        Commands\resyncMasterSupplierInAllocationItem::class,
        Commands\SyncMovementPerSize::class,
        Commands\resyncMasterItemInMaterialRequirement::class,
        Commands\SyncPoBuyerCancel::class,
        Commands\SyncMaterialRequirement::class,
        Commands\DailyInsertAllocationFabric::class,
        Commands\DailyIntegrationToLocatorRejectErp::class,
        Commands\resyncQtyAutoAllocation::class,
        Commands\ExportFormUpdateAllocation::class,
        Commands\DailyInsertPreparationFabric::class,
        Commands\DailyAutoAllocation::class,
        Commands\DailyAutoAllocationAcc::class,
        Commands\DailyAutoAllocationFab::class,
        Commands\DailySendEmailOutstandingQc::class,
        Commands\DailySendEmailMoveLocatorReject::class,
        Commands\DailyUpdateStockReserveAcc::class,
        Commands\DailyUpdateStockReserveAccAoi2::class,
        Commands\resyncMasterItemInAutoAllocation::class,
        Commands\dailyUpdateStockReserveHandoverAcc::class,
        Commands\resyncMasterSupplierInMaterialArrival::class,
        Commands\resyncMasterItemInSummaryStockFabric::class,
        Commands\resyncMasterItemInMaterialPreparationAccessories::class,
        Commands\resyncMasterSupplierInMonitoringReceivingFabric::class,
        Commands\resyncMasterSupplierInMaterialPreparationFabric::class,
        Commands\resyncMasterSupplierInMaterialCheck::class,
        Commands\resyncMasterSupplierInDetailMaterialPlanning::class,
        Commands\resyncMasterSupplierInAutoAllocation::class,
        Commands\resyncMasterSupplierInSummaryStockFabric::class,
        Commands\resyncMasterSupplierInSummaryHandover::class,
        Commands\resyncMasterItemInSummaryHandoverFabrics::class,
        Commands\resyncMasterSupplierInMaterialStock::class,
        Commands\resyncMasterSupplierInMaterialStock::class,
        Commands\resyncMasterItemInDetailMaterialPreparationFabric::class,
        Commands\resyncMasterItemInMaterialCheckAccessories::class,
        Commands\resyncMasterItemInMaterialCheckFabric::class,
        Commands\resyncMasterItemInMonitoringHandoverFabric::class,
        Commands\resyncMasterItemInMonitoringReceivingFabric::class,
        Commands\resyncMasterItemInSummaryHandoverFabrics::class,
        Commands\resyncMasterItemInMaterialPreparationFabric::class,
        Commands\resyncMasterItemInMaterialArrival::class,
        Commands\resyncMasterItemInMaterialStock::class,
        Commands\DailyIntegrationFreeStock::class,
        //Commands\UpdateStockAccessories::class,
        Commands\UpdateVarianceStockErpAcc::class,
        Commands\UpdateVarianceStockErpFab::class,
        Commands\DailyInsertStockFabricMM::class,
        Commands\HourlyInsertRequirementMaterial::class,
        //Commands\DailyInsertRequirementMaterial::class,
        Commands\DailyInsertMasterErp::class,
        Commands\DailyUpdateLcDateMrp::class,
        Commands\dailyIncaseNoNeedPreparationAcc::class,
        Commands\DailyUpdateStatusQCMaterial::class,
        Commands\DailyDeleteMovement::class,
        //Commands\DailyInsertLogPoBuyer::class,
        //Commands\DailyUpdatePoBuyer::class,
        Commands\HourlyInserWmsOld::class,
        Commands\DailyInsertPoBuyer::class,
        Commands\DailyWmsOld::class,
        Commands\DailyInsertPurchaseItems::class,
        Commands\DailyUpdateWarehouseProduction::class,
        Commands\IncaseStlyeUpdate::class,
        Commands\IncaseEvent::class,
        Commands\IncaseSubcont::class,
        Commands\HourlyInsertMrp::class,
        Commands\WeekendRefreshMr::class,
        Commands\UpdateStockIncase::class,
        Commands\HourlyInsertDetailRequirementMaterial::class,
        Commands\InsertMaterialReadyPreparation::class,
        Commands\DailyUpdateStatisticalDateBuyer::class,
        Commands\UpdateSummaryStockFabric::class,
        Commands\DailyInsertRequirementPerPart::class,
        Commands\DailyInsertPlanningFabric::class,
        Commands\SyncUomConversion::class,
        Commands\SyncItem::class,
        Commands\SyncSupplier::class,
        Commands\SyncAll::class,
        Commands\DailyAllocationLot::class,
        Commands\DailyAutoAllocationCarton::class,
        Commands\dailyAutoAllocationFreeStockCarton::class,
        Commands\DailyUpdateDashboardMonitoringMaterial::class,
        Commands\DailyCaptureMaterialStock::class,
        Commands\AutoCancelPoBuyerFromERP::class,
        Commands\DailyPackingList::class,
        Commands\autoAllocated::class
    ];

    protected function schedule(Schedule $schedule)
    {
        $schedule->command('sync:all')->dailyAt('12:00')->runInBackground()->sendOutputTo('schedule.log');
        $schedule->command('dailySendEmailMoveLocatorReject:mail')
         ->everyThirtyMinutes()
         ->between('8:00', '16:00')
         ->runInBackground()
         ->sendOutputTo('schedule.log');
    }

   protected function commands()
    {
        require base_path('routes/console.php');
    }
}
