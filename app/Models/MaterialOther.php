<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MaterialOther extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $dates = ['created_at','approval_date','reject_date','deleted_at'];
    protected $fillable = ['material_arrival_id','criteria_id','category','system_note', 'warehouse','uom','locator_id','qty','approval_status','document_no',
                        'is_active','approval_date','reject_date','user_id','deleted_at','item_code','item_desc','is_know_datasource','user_approval_id','user_reject_id'];
    

    public function materialArrival(){
        return $this->belongsTo('App\Models\MaterialArrival');
    }

    public function criteria(){
        return $this->belongsTo('App\Models\Criteria');
    }

    public function locator(){
        return $this->belongsTo('App\Models\Locator');
    }


}
