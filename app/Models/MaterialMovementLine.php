<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;
use StdClass;
use App\Uuids;

class MaterialMovementLine extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at'
        ,'date_movement'
        ,'deleted_at'
        ,'updated_at'
        ,'integration_date'
        ,'date_receive_on_destination'
    ];
    
    protected $fillable = ['material_preparation_id'
        ,'material_stock_id'
        ,'type_po'
        ,'item_id'
        ,'item_id_source'
        ,'remark_integration'
        ,'item_code'
        ,'created_at'
        ,'updated_at'
        ,'c_order_id'
        ,'c_orderline_id'
        ,'is_inserted_to_material_movement_per_size'
        ,'inserted_to_material_movement_per_size_date'
        ,'document_no'
        ,'nomor_roll'
        ,'c_bpartner_id'
        ,'warehouse_id'
        ,'uom_movement'
        ,'date_movement'
        ,'qty_movement'
        ,'integration_date'
        ,'date_receive_on_destination'
        ,'is_integrate'
        ,'user_id'
        ,'supplier_name'
        ,'is_active'
        ,'deleted_at'
        ,'material_movement_id'
        ,'parent_id'
        ,'note'
        ,'is_inserted_to_subcont'
        ,'is_exclude'
    ];

    public function movement()
    {
        return $this->belongsTo('App\Models\MaterialMovement','material_movement_id');
    }

    public function materialPreparation()
    {
        return $this->belongsTo('App\Models\MaterialPreparation');
    }
    
    public function parent()
    {
         return $this->belongsTo('App\Models\MaterialMovementLine','parent_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function materialStock()
    {
        return $this->belongsTo('App\Models\MaterialStock');
    }

    public function materialMovementPerSize()
    {
        return $this->hasMany('App\Models\MaterialMovementPerSize');
    } 

    static function totalQtyAllocatedPerSized($id)
    {
        return MaterialMovementPerSize::whereNotNull('material_movement_line_id')
        ->where('material_movement_line_id',$id)
        ->sum('qty_per_size');
    }
}
