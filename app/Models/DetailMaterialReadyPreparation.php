<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class DetailMaterialReadyPreparation extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','deleted_at'];
    protected $guarded = ['id'];
    protected $fillable = ['material_ready_preparation_id','document_no', 'user_id', 'deleted_at'];

    public function materialPreparation(){
        return $this->belongsTo('App\Models\MaterialPreparation');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
