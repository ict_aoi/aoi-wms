<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MaterialStockPerLot extends Model
{
    use Uuids;
    public $timestamps    = false;
    public $incrementing  = false;
    protected $dates      = ['created_at','deleted_at'];
    protected $guarded    = ['id'];
    protected $fillable     = ['c_order_id'
        ,'c_bpartner_id'
        ,'item_id'
        ,'warehouse_id'
        ,'no_invoice'
        ,'no_packing_list'
        ,'document_no'
        ,'supplier_code'
        ,'supplier_name'
        ,'item_code'
        ,'actual_lot'
        ,'type_stock_erp_code'
        ,'type_stock'
        ,'uom'
        ,'qty_stock'
        ,'qty_reserved'
        ,'qty_available'
        ,'created_at'
        ,'updated_at'
        ,'deleted_at'
    ];
}
