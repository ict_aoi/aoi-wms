<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MaterialMoq extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','eta'];
    protected $guarded = ['id'];
    protected $fillable = ['document_no','eta','supplier_name','item_code', 'category','po_buyer','warehouse','uom','qty_po','qty_need','qty_book','user_id','deleted_at'];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
