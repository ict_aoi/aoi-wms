<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class ReroutePoBuyer extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at'];
    protected $guarded = ['id'];
    protected $fillable = ['old_po_buyer','new_po_buyer','user_id','note','is_prefix'];
}
