<?php

namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MaterialBacklog extends Model
{
  use Uuids;
  public $incrementing = false;
  protected $dates = ['created_at'];
  protected $guarded = ['id'];
   protected $fillable = [
     'po_buyer','item_code'
   ];
}
