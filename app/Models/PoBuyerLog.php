<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class PoBuyerLog extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $dates = ['erp_created_date','created_date'];
    protected $fillable = ['old_value','new_value'];

}
