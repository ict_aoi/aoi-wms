<?php namespace App\Models;

use DB;
use Config;
use StdClass;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderDetailDev extends Model
{
    protected $connection = 'dev_web_po';
    protected $guarded = ['id'];
    protected $fillable = ['flag_wms','user_warehouse_receive','wms_is_locked'];
    protected $table = 'po_detail';
    
    
    public function Purchase()
    {
        return $this->belongsTo('App\Models\PurchaseOrder', 'c_order_id');
    }

    public function PurchaseLines()
    {
        return $this->hasMany('App\Models\PurchaseOrderDetailLine', 'c_orderline_id');
    }

    public function getInformationOnWms($po_detail_id = null)
    {
        $is_exists = DB::connection('pgsql')
        ->table('stock_lines')
        ->where('po_detail_id',$po_detail_id)
        ->count();
        
        return $is_exists;
    }

    static function getSeason($po_buyer)
    {
        $season = DB::connection('pgsql')
        ->table('po_buyers')
        ->select('season')
        ->where('po_buyer',$po_buyer)
        ->groupby('season')
        ->first();
        
        if($season)return $season->season;
        else return '-1';
    }

    static function getBom($po_buyer,$item_code)
    {
        $material_requirements = DB::connection('pgsql')
        ->table('material_requirements')
        ->select('style','article_no')
        ->where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code]
        ])
        ->groupby('style','article_no')
        ->get();

        $flag           = 0;
        $style          = null;
        $article_no     = null;

        foreach ($material_requirements as $key => $value) 
        {
            $style      = $value->style;
            $article_no = $value->article_no;
            $flag++;
        }

        
        if($flag > 1)
        {
            $obj                = new stdClass();
            $obj->style         = 'HAS MANY STYLE';
            $obj->article_no    = 'HAS MANY ARTICLE';
        }else
        {
            $obj                = new stdClass();
            $obj->style         = $style;
            $obj->article_no    = $article_no;
        }

        return $obj;
    }


    static function checkFlagWms($po_detail_id)
    {
        $data = DB::connection('dev_web_po')
        ->table('po_detail')
        ->where('po_detail_id',$po_detail_id)
        ->first();

        return $data->flag_wms;
    }
}
