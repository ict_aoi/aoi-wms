<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;
use DB;

class MaterialMovement extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $fillable     = ['from_location'
        ,'to_destination'
        ,'from_locator_erp_id'
        ,'to_locator_erp_id'
        ,'po_buyer'
        ,'no_packing_list'
        ,'no_invoice'
        ,'status'
        ,'is_active'
        ,'user_id'
        ,'remark_integration'
        ,'is_integrate'
        ,'created_at'
        ,'update_at'
        ,'integration_date'
        ,'is_complete_erp'
        ,'document_type'
        ,'document_no_out'
        ,'document_no_kk'
    ];
    
    protected $dates        = ['created_at','integration_date'];

    public function fromLocator()
    {
        return $this->belongsTo('App\Models\Locator','from_location','id');
    }

    public function destinationLocator()
    {
        return $this->belongsTo('App\Models\Locator','to_destination','id');
    }

    public function movementLines()
    {
        return $this->hasMany('App\Models\MaterialMovementLine')->where('is_active',true);
    } 
    
    static function LocatorName($locator_id = null)
    {
        $locator = Locator::select(db::raw('rack||\'.\'||y_row||\'.\'||z_column'))
            ->where('id',$locator_id)
            ->first();
        return $locator;
    }

    static function countLines($id = null)
    {
        $count_all_lines = DB::table('material_movement_lines')
        ->where('material_movement_id',$id)
        ->count();
        
        return $count_all_lines;
    }

    static function countInActiveLines($id = null){
        $count_in_active_lines = DB::table('material_movement_lines')
        ->where([
            ['material_movement_id','=',$id],
            ['is_active','=',false]
        ])
        ->whereNotNull('deleted_at')
        ->count();
        
        return $count_in_active_lines;
    }
}
