<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;
class DetailKontrak extends Model
{
  use Uuids;
  public $incrementing = false;
  protected $fillable = [
    'user_id',
    'kontrak_id',
    'no_order',
    'keterangan',
    'style',
    'kategori',
    'destination',
    'total_qty',
    'statistic_date',
    'price',
    'amount'
  ];
}
