<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MaterialArrival extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','etd_date','eta_date','preparation_date','deleted_at','updated_at','approval_date','reject_date','allocated_date','integration_date'];
    protected $guarded = ['id'];
    protected $fillable = ['po_detail_id'
    ,'material_roll_handover_fabric_id'
    ,'material_subcont_id'
    ,'item_code'
    ,'item_desc'
    ,'is_moq'
    ,'supplier_name'
    ,'item_id'
    ,'qty_upload'
    ,'document_no'
    ,'c_order_id'
    ,'c_orderline_id'
    ,'qty_available_rma'
    ,'qty_reserved_rma'
    ,'warehouse_id'
    ,'category'
    ,'uom'
    ,'prepared_status'
    ,'is_subcont'
    ,'is_allocated'
    ,'allocated_date'
    ,'c_bpartner_id'
    ,'integration_date'
    ,'is_integrate'
    ,'is_general_item'
    ,'qty_entered'
    ,'total_roll'
    ,'total_batch_number'
    ,'total_inspect_batch_number'
    ,'total_inspect_lab_roll'
    ,'no_packing_list'
    ,'season'
    ,'no_resi'
    ,'no_surat_jalan'
    ,'no_invoice'
    ,'etd_date'
    ,'preparation_date'
    ,'eta_date'
    ,'qty_ordered'
    ,'qty_carton'
    ,'user_id'
    ,'is_active'
    ,'foc'
    ,'is_ready_to_prepare'
    ,'type_po'
    ,'deleted_at'
    ,'po_buyer'
    ,'remark'
    ,'locator_id'
    ,'user_approval_id'
    ,'user_reject_id'
    ,'approval_status'
    ,'is_material_other'
    ,'is_ila'
    ,'approval_date'
    ,'reject_date'
    ,'foc_available_rma'
    ,'foc_reserved_rma'
    ,'referral_code'
    ,'sequence'
    ,'total_carton_in'
    ,'jenis_po'
    ,'summary_handover_material_id'];

    public function detailMaterialArrivals()
    {
        return $this->hasMany('App\Models\DetailMaterialArrival');
    }

    public function materialReadyPrepares()
    {
        return $this->hasMany('App\Models\MaterialReadyPreparation');
    }

    public function materialSubcont()
    {
        return $this->belongsTo('App\Models\MaterialSubcont');
    }

    public function locator()
    {
        return $this->belongsTo('App\Models\Locator');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function counter($id = null)
    {
        $counter = DB::table('detail_material_arrivals')
                ->where('material_arrival_id',$id)
                ->count();

        if($counter)
            return $counter;
        else
            return 0;
    }

    public function sumline($id = null)
    {
        $sum = DB::table('detail_material_arrivals')
        ->where('material_arrival_id',$id)
        ->sum('qty_upload');

        if($sum)
            return $sum;
        else
            return 0;
    }

    public function getColor($item_code)
    {
        $item = DB::table('items')->where('item_code',$item_code)->first();

        if($item)
            return $item->color;
        else
            return null;
    }

    public function conversion($id = null)
    {
        $material_arrival = DB::table('material_arrivals')->where('id',$id)->first();
        $conversion  = DB::table('uom_conversions')
        ->select('uom_to','dividerate','multiplyrate')
        ->where([
            ['item_id', '=', $material_arrival->item_id],
            ['category', '=', $material_arrival->category],
            ['uom_from', '=', $material_arrival->uom],
        ])
        ->first();

        if($conversion)
            return $conversion;
        else
            return null;

    }

    static function uomConversion($item_code,$uom_from)
    {
        $conversion  = DB::table('uom_conversions')
        ->where([
            ['item_code', $item_code],
            ['uom_from', $uom_from],
        ])
        ->first();

        if($conversion)
            return $conversion;
        else
            return null;
    }

    static function totalqtyCartonRct($id)
    {
        $qty_carton_rcv = db::table('detail_material_arrivals')
        ->whereIn('material_arrival_id',$id)
        ->count();

        return $qty_carton_rcv;
    }

    static function totalAllocated($material_arrival_id,$status,$po_buyer_top_buttom)
    {
        $detail_material_preparations = db::table('detail_material_preparations')
        ->select('material_preparation_id')
        ->whereIn('material_arrival_id',$material_arrival_id)
        ->whereIn('material_arrival_id',function($query)use($status,$po_buyer_top_buttom)
        {
            if($po_buyer_top_buttom)
            {
                $query->select('id')
                ->from('material_arrivals')
                ->where([
                    ['prepared_status',$status],
                    ['po_buyer',$po_buyer_top_buttom],
                ])
                ->groupby('id');
            }else
            {
                $query->select('id')
                ->from('material_arrivals')
                ->where('prepared_status',$status)
                ->groupby('id');
            }
           
            
        })
        ->groupby('material_preparation_id')
        ->get();

        $total = 0;
        foreach ($detail_material_preparations as $key => $value) {
            $material_preparation = db::table('material_preparations')
            ->where([
                ['is_from_switch',false],
                ['is_reroute',false],
                ['id',$value->material_preparation_id],
                ['warehouse',auth::user()->warehouse],
            ])
            ->first();

            $qty_conversion = ($material_preparation)? $material_preparation->qty_conversion:0;
            $qty_borrow = ($material_preparation)? $material_preparation->qty_borrow:0;
            $total += $qty_conversion + $qty_borrow;

        }

        return $total;
    }

    public function detailattr($id = null)
    {
        $detail_material_arrival = DB::table('detail_material_arrivals')
        ->select('po_buyer','qty_upload')
        ->where('material_arrival_id',$id)
        ->groupby('po_buyer','qty_upload')
        ->first();

        if($detail_material_arrival)
            return $detail_material_arrival;
        else
            return null;
    }

    public function reject($po_detail_id = null)
    {
        $reject = DB::table('material_checks')
        ->where('po_detail_id',$po_detail_id)
        ->sum('qty_reject');

        return $reject;
    }

    public function sumQtyFebric($id = null)
    {
        $qty_roll = DB::table('detail_material_arrivals')
        ->where('material_arrival_id',$id)
        ->sum('qty_upload');

        return $qty_roll;
    }

    public function getBuyers($item_code,$category,$document_no)
    {
        $po_buyers = DB::table('material_ready_preparations')
        ->select('po_buyer')
        ->where([
            ['item_code',$item_code],
            ['category',$category],
            ['document_no',$document_no],
        ])
        ->groupby('po_buyer')
        ->get();

        $_temp = array();
        foreach ($po_buyers as $key1 => $value) {

           $material_requirements = DB::table('material_requirements')
            ->where([
                ['item_code',$item_code],
                ['po_buyer',$value->po_buyer],
            ])
            ->orderby('statistical_date','asc')
            ->orderby('po_buyer','asc')
            ->get();

            if($material_requirements->count() != 0){
                foreach ($material_requirements as $key2 => $material_requirement) {
                    $obj = new stdClass();
                    $obj->po_buyer = $material_requirement->po_buyer;
                    $obj->job_order = $material_requirement->job_order;
                    $obj->article_no = $material_requirement->article_no;
                    $obj->style = $material_requirement->style;
                    $obj->qty_need = max($material_requirement->qty_required - $this->qtyHasPrepared($material_requirement->po_buyer,$item_code,$material_requirement->style),0);
                    $_temp []  = $obj;
                }
            }else{
                $obj = new stdClass();
                $obj->po_buyer = $value->po_buyer;
                $obj->job_order = 'data not found';
                $obj->article_no = 'data not found';
                $obj->style = 'data not found';
                $_temp []  = $obj;
            }


        }
        return $_temp;
    }

    public function getId($document_no,$item_code,$c_bpartner_id,$status,$po_buyer_top_buttom,$po_detail_id)
    {
        $material_arrivals = DB::table('material_ready_preparations')
        ->select('material_arrival_id')
        ->where([
            ['item_code',$item_code],
            ['c_bpartner_id',$c_bpartner_id],
            ['document_no',$document_no],
        ])
        ->whereIn('material_arrival_id',function($query) use ($status,$po_buyer_top_buttom,$po_detail_id)
        {
            if($po_buyer_top_buttom)
            {
                $query->select('id')
                ->from('material_arrivals')
                ->where([
                    ['prepared_status',$status],
                    ['po_buyer',$po_buyer_top_buttom],
                    ['po_detail_id',$po_detail_id],
                ])
                ->groupby('id');
            }else
            {
                $query->select('id')
                ->from('material_arrivals')
                ->where([
                    ['prepared_status',$status],
                    ['po_detail_id',$po_detail_id],
                ])
                ->groupby('id');
            }
            
        })
        ->groupby('material_arrival_id')
        ->get();

        $array = array();
        foreach ($material_arrivals as $key => $value){

           $array [] = $value->material_arrival_id;
        }
        return $array;
    }

    public function getTotalQtyUpload($document_no,$item_code,$c_bpartner_id){
        $total_qty_upload = DB::table('material_arrivals')
         ->where([
            ['item_code',$item_code],
            ['c_bpartner_id',$c_bpartner_id],
            ['warehouse_id',auth::user()->warehouse],
            ['document_no',$document_no],
        ])
        ->sum('qty_upload');

        return $total_qty_upload;
    }

    public function getId2($document_no,$item_code,$po_buyer,$c_bpartner_id){
        $material_arrivals = DB::table('material_ready_preparations')
        ->select('material_arrival_id')
        ->where([
            ['item_code',$item_code],
            ['po_buyer',$po_buyer],
            ['document_no',$document_no],
            ['c_bpartner_id',$c_bpartner_id],
        ])
        ->groupby('material_arrival_id')
        ->get();

        $array = array();
        foreach ($material_arrivals as $key => $value) {
           $array [] = $value->material_arrival_id;
        }
        return $array;
    }

    public function getId3($document_no,$item_code,$po_buyer,$c_bpartner_id){
        $material_arrivals = DB::table('material_ready_preparations')
        ->select('material_arrival_id')
        ->where([
            ['item_code',$item_code],
            ['po_buyer',$po_buyer],
            ['document_no',$document_no],
            ['c_bpartner_id',$c_bpartner_id],
        ])
        ->where(function($query){
            $query->whereRaw('material_arrival_id in ( select id from material_arrivals where prepared_status = \'NEED PREPARED TOP & BOTTOM\')');
        })
        ->groupby('material_arrival_id')
        ->get();

        $array = array();
        foreach ($material_arrivals as $key => $value) {
           $array [] = $value->material_arrival_id;
        }
        return $array;
    }

    public function qtyHasPrepared($po_buyer,$item_code,$style){
        $qty_has_prepared = db::table('material_preparations')
        ->where([
            ['po_buyer','=',$po_buyer],
            ['item_code','=',$item_code],
            ['style','=',$style]
        ])
        ->sum('qty_conversion');

        return $qty_has_prepared;
    }

    static function getPoBuyer($document_no,$item_code,$c_bpartner_id,$status,$material_arrival_ids){
        $array = array();
        
        $data = db::table('material_ready_preparations')
        ->select('po_buyer')
        ->where([
            ['document_no',$document_no],
            ['item_code',$item_code],
            ['c_bpartner_id',$c_bpartner_id]
        ])
        ->whereIn('material_arrival_id',function($query) use($status) {
            $query->select('id')
            ->from('material_arrivals')
            ->where('prepared_status',$status)
            ->groupby('id');
        })  
        ->whereIn('material_arrival_id',$material_arrival_ids)
        ->groupby('po_buyer')
        ->get();

        foreach ($data as $key => $value) 
        {
            //$reroute = db::table('reroute_po_buyers')->where('old_po_buyer',$value->po_buyer)->first();
            $reroute = null;
            $array [] = ($reroute) ? $reroute->new_po_buyer : $value->po_buyer;
        }
        return $array;

    }

    public function items(){
      return $this->belongsTo('App\Models\Item','item_code','item_code');
    }

    public function material_stock(){
      return $this->belongsTo('App\Models\MaterialStock','id','material_arrival_id');
    }
}
