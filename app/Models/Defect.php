<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Defect extends Model
{
    public $incrementing    = false;
    protected $fillable     = ['code','description','description_2','user_id'];
    protected $dates        = ['created_at'];
}
