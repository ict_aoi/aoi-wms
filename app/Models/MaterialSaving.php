<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;
class MaterialSaving extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','updated_at','planning_date','actual_saving_roll_date'];
    protected $guarded = ['id'];
    protected $fillable = ['item_id'
    ,'c_order_id'
    ,'style'
    ,'planning_date'
    ,'article_no'
    ,'item_code'
    ,'color'
    ,'uom'
    ,'po_supplier'
    ,'supplier_name'
    ,'qty_saving'
    ,'warehouse_id'
    ,'c_bpartner_id'
    ,'actual_saving_roll_date'
    ,'upload_user_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User','upload_user_id');
    }
}
