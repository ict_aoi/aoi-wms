<?php namespace App\Models;

use DB;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MaterialOpnameFabricSummary extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['summary_stock_fabric_id','qty_sto', 'remark'];
    protected $dates = ['created_at'];

    

}
