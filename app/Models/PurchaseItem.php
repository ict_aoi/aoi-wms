<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;
use DB;
use Auth;


class PurchaseItem extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['c_order_id'
    ,'item_id'
    ,'document_no'
    ,'promise_date'
    ,'c_bpartner_id'
    ,'po_buyer'
    ,'item_code'
    ,'warehouse'
    ,'qty'
    ,'uom'
    ,'no_invoice'
    ,'eta_date'
    ,'category'
    ,'supplier_name'
    ,'deleted_at'
    ,'is_inserted_to_allocation_item'
    ,'cancel_reason'
    ,'cancel_date'
    ,'status_po_buyer'
    ,'counter_out'
    ,'counter_handover'
    ,'is_allocation_already_created'
    ,'purchase_item_id'
    ,'qty_order'
    ,'qty_moq'
    ,'_qty_moq'
    ,'credit_moq'
    ,'remark_moq'
    ,'is_moq'
    ,'is_stock_moq_already_created'
    ,'counter_moq'];

    protected $dates = ['created_at'];


    static function conversion($item_code,$uom_from,$category)
    {
        $conversion  = UomConversion::where([
            ['item_code', $item_code],
            ['category', $category],
            ['uom_from', $uom_from],
        ])
        ->first();
        
        if($conversion) return $conversion;
        else return null;
    }

    static function reconversion($item_code,$uom_from,$category){
        $reconversion  = UomConversion::where([
            ['item_code', $item_code],
            ['uom_to', $uom_from],
        ])
        ->first();
        
        if($reconversion) return $reconversion;
        else return null;
    }

    static function qtyHasPrepared($po_buyer,$item_code,$document_no,$c_bpartner_id,$po_detail_id)
    {
        $qty_has_prepared = MaterialPreparation::where([
            ['po_buyer',$po_buyer],
            ['document_no',$document_no],
            ['c_bpartner_id',$c_bpartner_id],
            ['item_code',$item_code],
            ['warehouse',auth::user()->warehouse]
        ])
        ->whereIn('id',function($query) use($document_no,$c_bpartner_id,$po_detail_id)
        {
            $query->select('material_preparation_id')
            ->from('detail_material_preparations')
            ->whereIn('material_arrival_id',function($query2) use($document_no,$c_bpartner_id,$po_detail_id)
            {
                $query2->select('id')
                ->from('material_arrivals')
                ->where([
                    ['document_no',$document_no],
                    ['po_detail_id',$po_detail_id],
                    ['c_bpartner_id',$c_bpartner_id],
                    ['warehouse_id',auth::user()->warehouse],
                ])
                ->groupby('id');
            })
            ->groupby('material_preparation_id');
        })
        ->sum(db::raw("(material_preparations.qty_conversion + COALESCE(qty_borrow,0)+ COALESCE(qty_reject,0))"));

        return $qty_has_prepared;
    }

    public function getMaterialArrivalId($document_no,$item_code,$po_buyer,$c_bpartner_id,$status,$po_buyer_top_buttom,$po_detail_id)
    {
        $material_arrivals = MaterialReadyPreparation::select('material_arrival_id')
        ->where([
            ['item_code',$item_code],
            ['po_buyer',($po_buyer_top_buttom)? $po_buyer_top_buttom : $po_buyer],
            ['document_no',$document_no],
            ['c_bpartner_id',$c_bpartner_id],
        ])
        ->whereIn('material_arrival_id',function($query) use ($status,$po_detail_id)
        {
            $query->select('id')
            ->from('material_arrivals')
            ->where([
                ['prepared_status',$status],
                ['po_detail_id',$po_detail_id],
            ])
            ->groupby('id');
            
        })
        ->groupby('material_arrival_id')
        ->get();

        $array = array();
        foreach ($material_arrivals as $key => $value) 
        {
           $array [] = $value->material_arrival_id;
        }
        return $array;
    }

    static function qtyBook($po_buyer,$item_code)
    {
        $qty_booking = AllocationItem::where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code],
        ])
        ->where(function($query){
            $query->where('confirm_by_warehouse','approved')
            ->OrWhereNull('confirm_by_warehouse');
        })
        ->whereNull('purchase_item_id')
        ->whereNull('deleted_at')
        ->sum('qty_booking');

        return $qty_booking;
    }

    public function checkActiveBom($po_buyer,$item_code){
        $data = db::table('material_requirements')
        ->where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code],
        ])
        ->sum('qty_required');

        if($data > 0)
            return true;
        else
            return false;

    }

    public function checkExistsBom($po_buyer,$item_code){
        $is_exists = db::table('material_requirements')
        ->where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code],
        ])
        ->exists();

        return $is_exists;

    }

    public function checkPreparedStatus($po_buyer,$item_code,$document_no,$material_arrival_id){
        $data = db::table('material_ready_preparations')
        ->where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code],
            ['document_no',$document_no],
        ])
        //->whereIn('material_arrival_id',$material_arrival_id)
        ->first();

        $material_arrival = db::table('material_arrivals')->where('id',$data->material_arrival_id)->first();
        
        if($material_arrival)
            return $material_arrival->prepared_status;
    }
}
