<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class DetailMaterialPreparation extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','deleted_at'];
    protected $guarded = ['id'];
    protected $fillable = ['material_arrival_id','material_preparation_id','detail_material_preparation_fabric_id','allocation_item_id',
    'user_id','deleted_at'];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function materialPreparation(){
        return $this->belongsTo('App\Models\MaterialPreparation','material_preparation_id');
    }

    public function materialArrival(){
        return $this->belongsTo('App\Models\MaterialArrival');
    }

    public function allocationItem(){
        return $this->belongsTo('App\Models\AllocationItem');
    }

}
