<?php

namespace App\Models\Lab;

use DB;
use Auth;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MasterTrfTestingLab extends Model
{
   
    use Uuids;
    protected $connection   = 'lab';
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at','deleted_at'];
    protected $fillable     = ['no_trf','buyer','lab_location',
                                'asal_specimen','category_specimen','type_specimen',
                            'test_required','previous_trf','date_information','date_information_remark', 'category',
                            'part_of_specimen','orderno','is_pobuyer','is_mo','size', 'style',
                            'article_no','color','nik','return_test_sample',
                        'created_at','updated_at', 'last_status','testing_method_id','verified_lab_date',
                        'verified_lab_by','testing_date','platform','ishandover','status_handover','reject_by','item', 'destination', 'factory_id','t2_result',
                        'description','is_po_supplier'
                        ,'barcode_supplier'
                        ,'nomor_roll'
                        ,'batch_number'
                        ,'is_repetation','supplier_name','composition'];
    protected $table        = 'master_trf_testings';
}
