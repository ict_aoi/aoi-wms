<?php

namespace App\Models\Lab;

use DB;
use Auth;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class TrfTesting extends Model
{
    use Uuids;
    protected $connection   = 'lab';
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at','deleted_at'];
    protected $fillable     = ['trf_id'
                            ,'buyer'
                            ,'lab_location'
                            ,'nik'
                            ,'platform'
                            ,'factory_id'
                            ,'asal_specimen'
                            ,'category_specimen'
                            ,'type_specimen'
                            ,'category'
                            ,'date_information'
                            ,'date_information_remark'
                            ,'test_required'
                            ,'part_of_specimen'
                            ,'created_at'
                            ,'updated_at'
                            ,'deleted_at'
                            ,'return_test_sample'
                            ,'status'
                            ,'verified_lab_date'
                            ,'verified_lab_by'
                            ,'reject_by'
                            ,'reject_date'
                            ,'is_handover'
                            ,'status_handover'
                            ,'remarks'
                            ,'previous_trf_id'
                            ,'result_t2'];
    protected $table        = 'trf_testings';
}
