<?php

namespace App\Models\Lab;
use DB;
use Auth;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class TrfTestingMethods extends Model
{
    use Uuids;
    protected $connection   = 'lab';
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at','deleted_at'];
    protected $fillable     = ['trf_id'
                            ,'master_method_id'
                            ,'created_at'
                            ,'updated_at'
                            ,'deleted_at'];
    protected $table        = 'trf_testing_methods';
}
