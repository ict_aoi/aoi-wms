<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MappingStocks extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','updated_at','deleted_at'];
    protected $guarded = ['id'];
    protected $fillable = ['type_stock','type_stock_erp_code','document_number','deleted_at'];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
