<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class HistoryReroutePoBuyer extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','etd_date','eta_date'];
    protected $guarded = ['id'];
    protected $fillable = ['old_po_buyer','new_po_buyer','user_id','material_preparation_id'];
    
}
