<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MovementStockHistory extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','updated_at','deleted_at'];
    protected $guarded = ['id'];
    protected $fillable = [
        'from_location'
        ,'to_destination'
        ,'material_stock_id'
        ,'uom'
        ,'qty'
        ,'detail_material_preparation_fabric_id'
        ,'movement_date'
        ,'note'
        ,'status'
        ,'deleted_at'
        ,'user_id'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function materialStock(){
        return $this->belongsTo('App\Models\User');
    }

    public function fromLocator(){
        return $this->belongsTo('App\Models\Locator','from_location','id');
    }

    public function destinationLocator(){
        return $this->belongsTo('App\Models\Locator','to_destination','id');
    }

}
