<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class CuttingInstruction extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','planning_date'];
    protected $fillable     = ['created_at'
        ,'updated_at'
        ,'article_no'
        ,'style'
        ,'item_code'
        ,'uom'
        ,'planning_date'
        ,'po_buyer'
        ,'qty_need'
        ,'warehouse_id'
    ];
}
