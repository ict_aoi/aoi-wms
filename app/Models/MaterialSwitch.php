<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MaterialSwitch extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','updated_at','deleted_at'];
    protected $guarded = ['id'];
    protected $fillable = ['document_no_source', 'document_no_new','po_buyer','warehouse_id','warehouse_source_id','item_code','type_po','qty_switch','style','user_id','deleted_at','remark','is_used_free_stock','warehouse_production_id','is_already_generated','po_buyer_source','type_po','qty_need_source_outstanding','qty_need_new_outstanding','uom'
    ,'qty_need_source_switched','qty_need_new_switched','c_bpartner_id_source','c_bpartner_id_new','supplier_name_source','supplier_name_new','item_id','c_order_id_source','c_order_id_new','_qty_need_source','_qty_need_new'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    static function getSupplier($document_no,$item_code){
        /*$supplier = db::table('purchase_items')
        ->select('c_bpartner_id','supplier_name','eta_date','no_invoice','category','uom')
        ->where([
            [db::raw('upper(document_no)'),$document_no],
            [db::raw('upper(item_code)'),$item_code],
           
        ])
        ->groupby('c_bpartner_id','supplier_name','eta_date','no_invoice','category','uom')
        ->first();*/

        $supplier = DB::connection('erp')
        ->table('rma_wms_planning')
        ->select('c_bpartner_id','supplier_name','eta_date','no_invoice','category','uom_pr as uom')
         ->where([
            [db::raw('upper(document_no)'),'LIKE',"%$document_no%"],
            [db::raw('upper(item_code)'),$item_code],
           
        ])
        ->groupby('c_bpartner_id','supplier_name','eta_date','no_invoice','category','uom_pr')
        ->first();

        if($supplier) return $supplier;
        else return null;
    }
}
