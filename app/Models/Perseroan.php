<?php

namespace App\Models;

use App\Models\KepalaProduksi;
use Illuminate\Database\Eloquent\Model;
class Perseroan extends Model
{
  protected $fillable=[
    'nama','alamat','user_id'
  ];
  public function kepala_produksi()
  {
      return $this->belongsTo(KepalaProduksi::class);
  }
}
