<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class Supplier extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $dates        = ['created_at'];
    protected $guarded      = ['id'];
    protected $fillable     = ['supplier_code'
        ,'supplier_name'
        ,'c_bpartner_id'
        ,'type_supplier'
    ];
    

    public function poSupplier()
    {
        return $this->hasMany('App\Models\PoSupplier');
    }
}
