<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MonitoringReceivingFabric extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $dates        = ['arrival_date'];
    protected $guarded      = ['id'];
    protected $fillable         = ['arrival_date'
    ,'po_detail_id'
    ,'c_order_id'
    ,'summary_handover_material_id'
    ,'no_pt'
    ,'no_bc'
    ,'no_kk'
    ,'no_packing_list'
    ,'document_no'
    ,'no_invoice'
    ,'supplier_name'
    ,'item_id'
    ,'item_code'
    ,'color'
    ,'warehouse_id'
    ,'total_roll'
    ,'total_yard'
    ,'user_receive'
    ,'c_bpartner_id'
    ,'total_batch'
    ,'total_roll_inspected'
    ,'style'
    ,'jenis_po'
    ,'created_at'
    ,'updated_at'
    ,'is_handover'];

}
