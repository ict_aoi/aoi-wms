<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class SummaryStockFabric extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $dates        = ['created_at','updated_at','deleted_at','last_planning_date','last_date_used','inspect_lab_date'];
    protected $guarded      = ['id'];
    protected $fillable     = ['c_bpartner_id'
    , 'c_order_id'
    ,'is_from_handover'
    ,'no_invoice'
    ,'uom'
    ,'document_no'
    ,'supplier_name'
    ,'item_code'
    ,'color'
    ,'category_stock'
    ,'stock'
    ,'reserved_qty'
    ,'available_qty'
    ,'remark'
    ,'supplier_code'
    ,'qty_order'
    ,'qty_order_reserved'
    ,'qty_order_available'
    ,'is_from_receiving'
    ,'is_from_input_stock'
    ,'is_allocated'
    ,'user_id'
    ,'deleted_at'
    ,'item_id'
    ,'type_stock_erp_code'
    ,'warehouse_id'];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function materialStock(){
        return $this->hasMany('App\Models\MaterialStock');
    }
}
