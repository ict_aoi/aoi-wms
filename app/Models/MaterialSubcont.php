<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MaterialSubcont extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at','date_receive'];
    protected $fillable     = ['created_at'
    ,'updated_at'
    ,'po_detail_id'
    ,'item_id'
    ,'material_movement_line_id'
    ,'item_code'
    ,'barcode'
    ,'season'
    ,'inventory_erp_from'
    ,'inventory_erp_to'
    , 'po_buyer'
    ,'item_desc'
    ,'supplier_name'
    ,'document_no'
    ,'c_order_id'
    ,'c_orderline_id'
    ,'warehouse_id'
    ,'category'
    ,'uom'
    ,'no_packing_list'
    ,'type_po'
    ,'c_bpartner_id'
    ,'no_resi'
    ,'no_surat_jalan'
    ,'no_invoice'
    ,'etd_date'
    ,'eta_date'
    ,'qty_carton'
    ,'qty_ordered'
    ,'qty_entered'
    ,'qty_upload'
    ,'is_active'
    ,'user_id'
    ,'date_receive'
    ,'deleted_at'
    ,'is_integrate'
    ,'integration_date'
    ,'is_complete_erp'
    ,'locator_from_erp_id'
    ,'locator_to_erp_id'];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function materialMovementLine(){
        return $this->belongsTo('App\Models\MaterialMovementLine');
    }

    public function detailMaterialSubcont(){
        return $this->hasMany('App\Models\DetailMaterialSubcont');
    
    } 
    public function materialArrival(){
        return $this->hasOne('App\Models\MaterialArrival');
    } 
}
