<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MaterialCancelBuyerFabric extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at'];
    protected $guarded = ['id'];
    protected $fillable = ['material_stock_id','new_material_stock_id','po_buyer','item_code','reserved_qty','user_id','warehouse_id','deleted_at'];

    public function materialStock(){
        return $this->belongsTo('App\Models\MaterialStock');
    }

    public function newMaterialStock(){
        return $this->belongsTo('App\Models\MaterialStock','new_material_stock_id');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

}
