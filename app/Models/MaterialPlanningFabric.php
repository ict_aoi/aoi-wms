<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MaterialPlanningFabric extends Model
{
    use Uuids;
    public $timestamps = false;
    public $incrementing = false;
    protected $dates = ['created_at','planning_date','preparation_date'];
    protected $guarded = ['id'];
    protected $fillable = ['article_no'
    ,'item_id'
    ,'item_code'
    , 'po_buyer'
    ,'style'
    ,'material_preparation_fabric_id'
    ,'planning_date'
    ,'warehouse_id'
    ,'user_id'
    ,'deleted_at'
    ,'color'
    ,'job_order'
    ,'qty_consumtion'
    ,'uom'
    ,'remark'
    ,'created_at'
    ,'updated_at'
    ,'is_piping'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function materialPreparationFabric()
    {
        return $this->belongsTo('App\Models\MaterialPreparationFabric');
    }

    public function detailMaterialPlanningFabric()
    {
        return $this->hasMany('App\Models\DetailMaterialPlanningFabric');
    }

    public function detailPoBuyerPerRoll()
    {
        return $this->hasMany('App\Models\DetailPoBuyerPerRoll');
    }

    public function getActualPlanning($id)
    {
        $detail =  DetailMaterialPreparationFabric::join('barcode_preparation_fabrics','barcode_preparation_fabrics.id','detail_material_preparation_fabrics.barcode_preparation_fabric_id')
        ->select('barcode_preparation_fabrics.actual_planning_warehouse_date')
        ->where('detail_material_preparation_fabrics.material_planning_fabric_id',$id)
        ->groupby('barcode_preparation_fabrics.actual_planning_warehouse_date')
        ->first();

       return ($detail)? $detail->actual_planning_warehouse_date : null;

    }

    static function getTotalAllocation($material_planning_fabric_id)
    {
        $total_allocation =  DetailMaterialPlanningFabric::where('material_planning_fabric_id',$material_planning_fabric_id)->sum('qty_booking');

        return $total_allocation;
    }

    static function qtyAllocated($material_planning_fabric_id)
    {
        $qty_booking =  DetailPoBuyerPerRoll::where('material_planning_fabric_id',$material_planning_fabric_id)
        ->whereNull('deleted_at')
        ->sum('qty_booking');

        return $qty_booking;
    }

    public function getDetailRoll($id,$part_no){
        $details =  DetailMaterialPlanningFabric::join('material_stocks','material_stocks.id','detail_material_preparation_fabrics.material_stock_id')
        ->select('material_stocks.actual_width','detail_material_preparation_fabrics.uom',db::raw('sum(detail_material_preparation_fabrics.reserved_qty) as reserved_qty'))
        ->where([
            ['detail_material_preparation_fabrics.material_planning_fabric_id',$id],
            ['detail_material_preparation_fabrics.part_no',$part_no],
            ['detail_material_preparation_fabrics.is_piping',false],
        ])
        //'material_stocks.nomor_roll'
        ->groupby('material_stocks.actual_width','detail_material_preparation_fabrics.uom')
        ->get();
        
        return $details;
    }

    public function getMargeBuyer($style,$article_no,$pcd,$user_warehouse){
        /*$details =  DB::table('detail_material_preparation_fabrics')
        ->select('po_buyer')
        ->where([
            ['style',$style],
            ['article_no',$article_no],
        ])
        ->whereIn('material_preparation_fabric_id',function($query)use($pcd){
            $query->select('id')
            ->from('material_preparation_fabrics')
            ->where('planning_date',$pcd);
        })
        ->groupby('po_buyer')
        ->get();*/

        $details =  DB::table('material_planning_fabrics')
        ->select('po_buyer')
        ->where([
            ['style',$style],
            ['article_no',$article_no],
            ['planning_date',$pcd],
            ['warehouse_id',$user_warehouse],
        ])
        ->whereIn('id',function($query) use($user_warehouse){
            $query->select('material_planning_fabric_id')
              ->from('detail_material_preparation_fabrics')
              ->whereIn('material_preparation_fabric_id',function($query) use ($user_warehouse){
                $query->select('id')
                ->from('material_preparation_fabrics')
                ->where('warehouse_id',$user_warehouse);
              })
              ->groupby('material_planning_fabric_id');
        })
        ->groupby('po_buyer')
        ->get();
        
        $marge = '';
        $po_buyers = array();
        foreach ($details as $key => $detail) {
            $marge .=  $detail->po_buyer.',';
            $po_buyers [] = $detail->po_buyer;
        }

        $obj = new stdClass();
        $obj->marge = substr($marge,0,-1);
        $obj->po_buyers = $po_buyers;
        return $obj;

    }
}
