<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class HistoryAutoAllocations extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $dates = ['created_at','integration_date'];
    protected $fillable = ['auto_allocation_id'
    ,'document_no_old'
    ,'c_bpartner_id_old'
    ,'supplier_name_old'
    ,'warehouse_id_old'
    ,'po_buyer_old'
    ,'item_code_source_old'
    ,'item_code_old'
    ,'item_id_old'
    ,'item_source_id_old'
    ,'uom_old'
    ,'qty_allocated_old'
    
    ,'document_no_new'
    ,'c_bpartner_id_new'
    ,'supplier_name_new'
    ,'warehouse_id_new'
    ,'po_buyer_new'
    ,'item_code_source_new'
    ,'item_code_new'
    ,'item_id_new'
    ,'item_source_id_new'
    ,'uom_new'
    ,'qty_allocated_new'
    
    ,'note'
    ,'operator'
    ,'is_integrate'
    ,'integration_date'
    ,'user_id'
    ,'note_code'
    ,'type_stock_old'
    ,'type_stock_erp_code_old'
    ,'type_stock_new'
    ,'type_stock_erp_code_new'
    ,'c_order_id'
    ,'lc_date'
    ,'remark'
    ,'purchase_item_id'];


    public function user(){
        return $this->belongsTo('App\Models\User','user_id');
    }
}
