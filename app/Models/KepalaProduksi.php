<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Perseroan;
class KepalaProduksi extends Model
{
    protected $fillable=[
      'user_id',
      'perseroan_id',
      'nama_lengkap',
      'no_ktp',
      'alamat',
      'kewarganegaraan',
      'negara',
    ];
    public function perseroan(){
      return $this->belongsTo(Perseroan::class);
    }
}
