<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class DetailMaterialSubcont extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at'];
    protected $guarded = ['id'];
    protected $fillable = ['material_subcont_id','barcode', 'qty', 'batch_number','nomor_roll',
    'user_id','deleted_at'];

    public function materialSubcont(){
        return $this->belongsTo('App\Models\MaterialSubcont');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
