<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;
use StdClass;
use App\Uuids;

class StockLine extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at'];
    protected $guarded = ['id'];
    protected $fillable = ['stock_id','material_arrival_id', 'status', 'uom_conversion','po_buyer',
                            'qty_reconversion','qty_conversion','job_order','article_no','style',
                            'barcode','is_active','note','user_id'];

    public function stock()
    {
        return $this->belongsTo('App\Models\Stock');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function StockMovementLines(){
        return $this->hasMany('App\Models\StockMovementLine');
    }

    public function Miscellaneous(){
        return $this->hasOne('App\Models\MiscellaneousItem');
    }

    public function Locator($po_buyer){
        $locator = DB::table('locators')
            ->select('locators.id','locators.code')
            ->whereRaw('
                id in (
                    select stock_movements.to_destination
                    from stock_movements
                    where po_buyer = \''.$po_buyer.'\'
                    and status in (\'in\',\'change\')
                    and is_active = true
                )
            ');
           
        
        if($locator->count() == 0){
            return null;
        }else if($locator->count() == 1){
            return $locator->groupby('locators.id','locators.code')
                    ->first();
        }else if($locator->count() > 1){
            $locators = $locator
                    ->orderBy('code','asc')
                    ->get();
            $code = '';
           foreach ($locators as $key => $rak) {
                
                $code .= $rak->code.',';
            }
            $obj = new StdClass();
            $obj->id = null;
            $obj->code = substr($code,0,-1);
            
            return $obj;
        }
        return $locator;
    }

    public function QtyOut($stock_line_id = null){
        $qty_out =  DB::table('stock_movement_lines')
            ->where('stock_line_id',$stock_line_id)
            ->whereRaw('stock_movement_id in (select id 
                from stock_movements
                where is_active = true
                and status in (\'out\'))
            ')
            ->sum('qty_movement');
        
        return $qty_out;
    }

    public function QtyReject($stock_line_id = null){
        $qty_reject =  DB::table('stock_movement_lines')
            ->where('stock_line_id',$stock_line_id)
            ->whereRaw('stock_movement_id in (select id 
                from stock_movements
                where is_active = true
                and status in (\'reject\'))
            ')
            ->sum('qty_movement');
        
        return $qty_reject;
    }

    public function QtBooking($stock_line_id = null){
        $stock_line = DB::table('stock_lines')
                ->where('id',$stock_line_id)
                ->first();

        $qty_book =  DB::table('allocation_items')
            ->where('po_detail_id',$stock_line->po_detail_id)
            ->where('no_po_supplier',$stock_line->document_no)
            ->whereNotNull('confirm_date')
            ->sum('qty_booking');
        
        return $qty_book;
    }

   
}
