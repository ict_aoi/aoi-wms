<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class OrderPreferance extends Model
{
    protected $connection = 'erp';
    //protected $connection = 'dev_erp';
    protected $guarded = ['id'];
    //protected $table = 'kst_order_poreference';
    protected $table = 'wms_history_update_po_buyer';
}
