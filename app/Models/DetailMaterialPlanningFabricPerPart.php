<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class DetailMaterialPlanningFabricPerPart extends Model
{
    use Uuids;
    public $timestamps = false;
    public $incrementing = false;
    protected $dates = ['created_at','planning_date'];
    protected $guarded = ['id'];
    protected $fillable = ['material_planning_fabric_id'
        ,'part_no'
        ,'qty_per_part'
        ,'created_at'
        ,'updated_at'
    ];

    public function materialPlanningFabric()
    {
        return $this->belongsTo('App\Models\MaterialPlanningFabric');
    }

    static function getTotalPreparationFabric($id)
    {
        $qty_booking =  DetailPoBuyerPerRoll::where('detail_material_planning_fabric_per_part_id',$id)
        ->whereNull('deleted_at')
        ->sum('qty_booking');

        return $qty_booking;
    }

    static function checkSaldo($material_planning_fabric_id)
    {
        return DB::select(db::raw("
            select exists(
                select * From (
                    select id,
                    round(float8(COALESCE(detail_material_planning_fabric_per_parts.qty_per_part, 0::numeric))::numeric, 6) AS qty_per_part,
                    round(float8(COALESCE(dtl.total_booked, 0::numeric))::numeric, 6) AS total_booked
                    from detail_material_planning_fabric_per_parts
                    left join (
                        select detail_material_planning_fabric_per_part_id,
                            sum(qty_booking) as total_booked
                        From detail_po_buyer_per_rolls
                        where detail_material_planning_fabric_per_part_id is not null
                        and deleted_at is null
                        and material_planning_fabric_id = '$material_planning_fabric_id'
                        GROUP BY detail_material_planning_fabric_per_part_id
                    )dtl on dtl.detail_material_planning_fabric_per_part_id = detail_material_planning_fabric_per_parts.id
                    where material_planning_fabric_id = '$material_planning_fabric_id'
                )tbl
                where tbl.total_booked < qty_per_part
            );
        "));
    }
}
