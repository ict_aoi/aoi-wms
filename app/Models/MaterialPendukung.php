<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Kontrak;
use App\Uuids;
class MaterialPendukung extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $fillable = [
      'user_id',
      'kontrak_id',
      'material',
      'qty',
      'uom'
    ];

    public function kontrak()
    {
        return $this->belongsTo(Kontrak::class);
    }
}
