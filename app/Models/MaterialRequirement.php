<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class MaterialRequirement extends Model{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $dates = ['created_at','updated_at','statistical_date','lc_date','promise_date'];
    protected $fillable = ['item_id'
        ,'item_code'
        ,'job_order'
        ,'style'
        ,'uom'
        ,'lc_date'
        ,'category'
        ,'qty_required'
        ,'item_desc'
        ,'po_buyer'
        ,'statistical_date'
        ,'season'
        ,'component'
        ,'article_no'
        ,'warehouse_id'
        ,'component'
        ,'promise_date'
        ,'destination_name'
        ,'warehouse_name'
        ,'is_upload_manual'
        ,'_style'
        ,'batch_code'
        ,'supplier_code'
        ,'supplier_name'
    ];

    public function getPlanning($po_buyer,$style,$item_code,$document_no)
    {
        $material_planning_fabric = db::table('material_planning_fabrics')
        ->where([
            ['document_no',$document_no],
            ['item_code',$item_code],
            ['po_buyer',$po_buyer],
            ['style',$style],
        ])
        ->whereNull('deleted_at')
        ->first();

        if($material_planning_fabric){
            $user = db::table('users')
            ->where('id',$material_planning_fabric->user_id)
            ->first();

            $obj = new stdClass();
            $obj->total_reserved_qty = $material_planning_fabric->total_reserved_qty;
            $obj->user_name = $user->name;
            $obj->created_at = $material_planning_fabric->created_at;
            return $obj;
        }else{
            return false;
        }
        
    }
    
    public function qtyRequiredNow($po_buyer,$item_code,$style,$qty_asal) {
        $qty_reserved = DB::table('material_preparation_febrics')
                            ->join('detail_material_preparation_febrics','material_preparation_febrics.id', '=',
                                    'detail_material_preparation_febrics.material_preparation_febric_id')
                            ->where([
                                ['po_buyer', $po_buyer],
                                ['style', $style],
                                ['item_code', $item_code]
                            ])
                            ->sum('reserved_qty');

        $qty_now = $qty_asal - $qty_reserved;
        return $qty_now;
    }

    public function qtyTypeStock($po_buyer){
        $data = db::table('po_buyers')
        ->where('po_buyer',$po_buyer)
        ->first();

        return $data;
    }

    public function qtyHasPrepared($po_buyer,$item_code,$style){
        $baclog_exists = db::table('material_backlogs')
        ->where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code]
        ])
        ->exists();

        if($baclog_exists)  $is_backlog = true;
        else $is_backlog = false;
        

        $qty_has_prepared = db::table('material_preparations')
        ->where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code],
            ['style',$style],
            ['is_backlog',$is_backlog],
            ['warehouse',auth::user()->warehouse]
        ])
        ->where(function($query){
            $query->where('last_status_movement','!=','cancel order')
            ->Where('last_status_movement','!=','cancel item');
        })
        ->sum(db::raw('qty_conversion'));

        return $qty_has_prepared;
    }

    static function qtyAllocated($po_buyer,$item_code,$style,$article_no = null)
    {
        $qty_allocation = db::table('allocation_items')
        ->where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code],
            ['style',$style]
        ]);
        //->whereNotIn('po_buyer',['0124270862','0124257949','0124297947','0124308006','0124258147']);;

        if($article_no)
            $qty_allocation = $qty_allocation->where('article_no',$article_no);
        
        $qty_allocation = $qty_allocation->where(function($query){
            $query->where('confirm_by_warehouse','approved')
            ->OrWhereNull('confirm_by_warehouse');
        })
        ->whereNull('purchase_item_id')
        ->whereNull('deleted_at')
        ->sum(db::raw('qty_booking -  COALESCE(qty_reject,0)'));

        $qty_has_prepared = db::table('material_preparations')
        ->where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code],
            ['style',$style],
            ['is_from_switch',false],
            ['is_allocation',false],
            ['last_status_movement','!=','out-handover']
        ]);
        //->whereNotIn('po_buyer',['0124270862','0124257949','0124297947','0124308006','0124258147']);;

        if($article_no) $qty_has_prepared = $qty_has_prepared->where('article_no',$article_no);

        $qty_has_prepared = $qty_has_prepared->sum(db::raw("(qty_conversion + COALESCE(adjustment,0) + COALESCE(qty_borrow,0) + COALESCE(qty_reject,0))"));
        return $qty_allocation + $qty_has_prepared;
    }

    static function qtyAllocatedAllocation($po_buyer,$item_code,$style,$article_no = null)
    {
        $qty_allocation = db::table('allocation_items')
        ->where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code],
            ['style',$style]
        ]);
        //->whereNotIn('po_buyer',['0124258147']);
        
        if($article_no) $qty_allocation = $qty_allocation->where('article_no',$article_no);
        
        $qty_allocation = $qty_allocation->where(function($query){
            $query->where('confirm_by_warehouse','approved')
            ->OrWhereNull('confirm_by_warehouse');
        })
        ->whereNull('purchase_item_id')
        ->whereNull('deleted_at')
        ->sum(db::raw('qty_booking -  COALESCE(qty_reject,0)'));

        $qty_has_prepared = db::table('material_preparations')
        ->where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code],
            ['style',$style],
            ['is_from_switch',false],
            ['is_allocation',false],
            ['last_status_movement','!=','out-handover']
        ]);
        //->whereNotIn('po_buyer',['0124258147']);

        if($article_no) $qty_has_prepared = $qty_has_prepared->where('article_no',$article_no);

        $qty_has_prepared = $qty_has_prepared->sum(db::raw("(qty_conversion + COALESCE(qty_borrow,0))"));
        return $qty_allocation + $qty_has_prepared;
    }

    static function getPoAllocated($po_buyer,$item_code,$style,$article_no = null,$material_arrivals_id,$document_no)
    {
        $allocations = db::table('allocation_items')
        ->select('document_no','supplier_name','warehouse','qty_booking as qty_allocated')
        ->where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code],
            ['style',$style],
            ['document_no','!=',$document_no]
        ]);

        if($article_no)
            $allocations = $allocations->where('article_no',$article_no);
        
        $allocations = $allocations->where(function($query){
            $query->where('confirm_by_warehouse','approved')
            ->OrWhereNull('confirm_by_warehouse');
        })
        ->whereNull('purchase_item_id')
        ->whereNull('deleted_at');

       

        $preparations = db::table('material_preparations')
        ->select('document_no','supplier_name','warehouse',db::raw("(qty_conversion+COALESCE(qty_borrow,'0')) as qty_allocated"))
        ->where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code],
            ['style',$style],
            ['document_no','!=',$document_no],
            ['is_from_switch',false],
            ['is_allocation',false],
            ['last_status_movement','!=','out-handover']
        ]);

        if($article_no)
            $preparations = $preparations->where('article_no',$article_no);

        $preparations = $preparations->whereNotIn('id',function($query) use ($material_arrivals_id){
            $query->select('material_arrival_id')
            ->from('detail_material_preparations')
            ->whereIn('material_arrival_id',$material_arrivals_id)
            ->groupby('material_arrival_id');
        });

        $get_allocated = $preparations->union($allocations)->get();
        
        $remark                 = '';
        $document_no_in_array      = array();
        $document_no_array      = array();
        foreach ($get_allocated as $key => $allocation) 
        {
            $obj_2                      = new stdClass();
            $obj_2->document_no         = $allocation->document_no;
            $obj_2->supplier_name       = $allocation->supplier_name;
            $obj_2->warehouse       = $allocation->warehouse;
            $obj_2->qty_allocated       = $allocation->qty_allocated;
            $document_no_array []       = $obj_2;
            $document_no_in_array []    = $allocation->document_no;
            $remark                     .= $allocation->document_no.' (QTY '.$allocation->qty_allocated.'),';
        }

        $obj = new stdClass();
        $obj->document_no_array     = $document_no_array;
        $obj->document_no_in_array  = $document_no_in_array;
        $obj->remark                = substr_replace($remark, "", -1);;
        return $obj;

    }

    static function qtyAllocatedFabric($po_buyer,$item_code,$style,$article_no = null)
    {
        $qty_allocation = db::table('allocation_items')
        ->where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code],
            ['style',$style],
            ['is_additional',false]
        ]);

        if($article_no)
            $qty_allocation = $qty_allocation->where('article_no',$article_no);
        
        $qty_allocation = $qty_allocation->where(function($query){
            $query->where('confirm_by_warehouse','approved')
            ->OrWhereNull('confirm_by_warehouse');
        })
        ->whereNull('purchase_item_id')
        ->whereNull('deleted_at')
        ->sum(db::raw('qty_booking - qty_reject'));

        return $qty_allocation;
    }

     public function qtyHasAllocated($po_buyer,$item_code,$style,$article_no = null)
     {
        $qty_has_allocated = db::table('allocation_items')
        ->where([
            ['po_buyer',$po_buyer],
            [db::raw('upper(item_code)'),$item_code],
            ['style',$style],
        ]);

        if($article_no)
            $qty_has_allocated = $qty_has_allocated->where('article_no',$article_no);

        $qty_has_allocated = $qty_has_allocated->where(function($query){
            $query->where('confirm_by_warehouse','approved')
            ->OrWhereNull('confirm_by_warehouse');
        })
        ->whereNull('deleted_at')
        ->sum(db::raw('qty_booking - qty_reject'));
        
        return $qty_has_allocated;
    }

    public function dataMarge($item_code = null, $po_buyer=null, $category=null){
        $material_requirements = db::table('material_requirements')
        ->select('job_order','article_no','style','qty_required')
        ->where([
            ['item_code',$item_code],
            ['po_buyer',$po_buyer],
            ['category',$category],
        ])
        ->groupby('job_order','article_no','style','qty_required')
        ->get();


        if($material_requirements->count() > 0){
            $style= '';
            $total_required =0;

            foreach ($material_requirements as $key => $value) {
                $job_order = $value->job_order;
                $article_no = $value->article_no;
                $style .=$value->style.',';
                $total_required +=$value->qty_required;
            }

            $obj = new StdClass();
            $obj->style = substr($style,0,-1);
            $obj->article_no = $article_no;
            $obj->job_order = $job_order;
            $obj->total_required = $total_required;
            return $obj;
        }else{
            $obj = new StdClass();
            $obj->article_no = 'null';
            $obj->style = 'null';
            $obj->job_order = 'null';
            $obj->total_required = 0;
            return $obj;
        }

    }

    public function qtySupply($item_code = null, $po_buyer=null, $category=null){
        $qty_supply = db::table('material_preparations')
         ->where([
            ['item_code',$item_code],
            ['po_buyer',$po_buyer],
            ['category',$category],
           
        ])
        ->where(function($query){
            $query->where('last_status_movement','in')
            ->orWhere('last_status_movement','receiving')
            ->orWhere('last_status_movement','print')
            ->orWhere('last_status_movement','change')
            ->orWhere('last_status_movement','out');
        })
        ->sum('qty_conversion');


        return $qty_supply;
    }


    public function checkPrepared($po_buyer,$item_code,$style){
        $is_exist = db::table('material_preparations')
        ->where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code],
            ['style',$style],
        ])
        ->first();

        
        if($is_exist){
            $user = db::table('users')->where('id',$is_exist->user_id)->first();

            if($is_exist->warehouse == '1000013')
                $warehouse = 'ACC AOI 2';
            elseif($is_exist->warehouse == '1000002')
                $warehouse = 'ACC AOI 1';

            $obj = new stdclass();
            $obj->created_at = $is_exist->created_at;
            $obj->username = strtoupper($user->name);
            $obj->warehouse = $warehouse;
            return $obj;
        }
        else{
            return null;
        }
           
    }
    
    public function checkIlaPrepared($po_buyer,$item_id,$style,$article_no)
    {
        $is_exists = MaterialPreparation::where([
            ['po_buyer',$po_buyer],
            ['item_id',$item_id],
            ['style',$style],
            ['article_no',$article_no],
            //['is_from_barcode_bom',true],
        ])
        ->first();

        if($is_exists)
        {
            $user = User::where('id',$is_exists->user_id)->first();

            if($is_exists->warehouse == '1000013') $warehouse = 'ACC AOI 2';
            elseif($is_exists->warehouse == '1000002') $warehouse = 'ACC AOI 1';

            $obj                = new stdclass();
            $obj->created_at    = $is_exists->created_at;
            $obj->username      = strtoupper($user->name);
            $obj->warehouse     = $warehouse;
            return $obj;
        } else{
            return null;
        }
           
    }
}
