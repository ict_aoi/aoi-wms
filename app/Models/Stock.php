<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;
use DB;

class Stock extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['item_id','item_code', 'item_desc', 'status','user_id','is_active'];
    protected $dates = ['created_at'];
  
    public function StockLines()
    {
        return $this->hasMany('App\Models\StockLine');
    }

    public function StockMovementLines()
    {
        return $this->hasMany('App\Models\StockMovementLine','item_id','item_id');
    }

    public function User()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function totalQty($stock_id = null)
	{
        $total_stock =  $this->StockLines()
            ->where('stock_id',$stock_id)
            //->where('is_active',true)
            ->sum('qty_conversion');
        
        return $total_stock;
    }
    
    public function totalQtyMovement($item_id = null)
	{
        $total_stock_movement =  $this->StockMovementLines()
            ->where('item_id',$item_id)
            ->whereHas('movement',function ($query){
                $query->whereIn('status',['in']);
            })
           ->sum('qty_movement');
        
        return $total_stock_movement;
    }
    
    public function totalQtyOutMovement($item_id = null)
	{
        $total_stock_movement =  $this->StockMovementLines()
            ->where('item_id',$item_id)
            ->whereHas('movement',function ($query){
                $query->whereIn('status',['out']);
            })
           ->sum('qty_movement');
        
        return $total_stock_movement;
    }
    
    public function getUoMOnERP($item_code = null){
        $uom_erp = DB::connection('erp')
                    ->table('m_product')
                    ->join('c_uom','c_uom.c_uom_id','m_product.c_uom_id')
                    ->select('uomsymbol')
                    ->where('value',$item_code)
                    ->groupBy('uomsymbol')
                    ->first();
        
        return $uom_erp;
    }
}
