<?php namespace App\Models;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class Log extends Model
{
	use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
	protected $dates = ['created_at'];
    
    public function table()
    {
        return $this->morphTo();
    }

    public function user()
    {
    	return $this->belongsTo('App\Models\User', 'user_id');
    }

    public static function log($action, $user = null, $before = null, $trans_id = null, $after = null)
	{
		$log = new Log();
		$act = $action;
		$message = null;
		$action = explode('|', $action);
		$intl_act = $action[0];

		switch ($intl_act) {
			case 'LOGIN.SUCCESS':
				$message = 'Berhasil login';
				break;
			case 'LOGIN.FAILED':
				$message = 'Gagal login';
				break;
			case 'LOGOUT':
				$message = 'Logout dari system';
				break;
			case 'MOVEMENT.SUCCESS':
				$message = 'Berhasil memindahkan item ke dari Receiveing ke rak ' . e($action[1]);
				break;
			case 'CHECKOUT.SUCCESS':
				$message = 'Berhasil mengeluarkan item dari Rak ' . e($action[1]).'ke '.e($action[2]);
				break;
		}

		$log->fill([
			'user_name' => $user->email,
			'user_ip' => $_SERVER['REMOTE_ADDR'],
			'user_agent' => $_SERVER['HTTP_USER_AGENT'],
			'message' => $message,
			'action' => $intl_act
		]);
		$log->save();
		return $log;	
	}

	public static function getLogByUser()
	{
		$listLogs = DB::table('logs')
			->select('action','message')
			->where ('user_name', '=', auth()->guard('admin')->user()->email)
			->limit(5)
			->groupBy('user_name','message','action')
			->get();
		return $listLogs;
	}

}
