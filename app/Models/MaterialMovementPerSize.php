<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Uuids;

class MaterialMovementPerSize extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at'
        ,'updated_at'
        ,'integration_date'
        ,'reverse_date'
    ];

    protected $fillable      = ['created_at'
        ,'updated_at'
        ,'integration_date'
        ,'reverse_date'
        ,'material_movement_line_id'
        ,'is_integrate'
        ,'remark_integration'
        ,'size'
        ,'po_buyer'
        ,'item_id'
        ,'item_id_source'
        ,'qty_per_size'
    ];

    public function materialMovementLine()
    {
        return $this->belongsTo('App\Models\MaterialMovementLine','material_movement_line_id');
    }

}
