<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class HistoryPreparationFabric extends Model
{
    use Uuids;
    public $timestamps = false;
    protected $dates = ['created_at','updated_at'];
    protected $guarded = ['id'];
    protected $fillable = ['material_preparation_fabric_id'
        ,'created_at'
        ,'updated_at'
        ,'remark'
        ,'qty_before'
        ,'qty_after'
    ];
}
