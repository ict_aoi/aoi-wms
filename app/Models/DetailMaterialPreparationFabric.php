<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class DetailMaterialPreparationFabric extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','repreparation_roll_to_buyer_date'];
    protected $guarded = ['id'];
    protected $fillable = ['material_preparation_fabric_id'
        ,'remark_closing'
        ,'remark_ict'
        ,'uom'
        ,'reserved_qty'
        ,'user_id'
        ,'deleted_at'
        ,'remark'
        ,'repreparation_roll_to_buyer_date'
        ,'created_at'
        ,'updated_at'
        ,'material_stock_id'
        ,'item_id'
        ,'item_code'
        ,'color'
        ,'planning_pull_cutting_id'
        ,'actual_preparation_date'
        ,'upc_item'
        ,'last_movement_date'
        ,'last_status_movement'
        ,'last_locator_id'
        ,'last_user_movement_id'
        ,'barcode'
        ,'referral_code'
        ,'sequence'
        ,'qty_saving'
        ,'actual_lot'
        ,'cancel_reason'
        ,'cancel_user_id'
        ,'begin_width'
        ,'middle_width'
        ,'end_width'
        ,'actual_length'
        ,'actual_width'
        ,'warehouse_id'
        ,'user_receive_cutting_id'
        ,'date_receive_cutting'
        ,'preparation_date'
        ,'movement_out_date'
        ,'is_closing'
        ,'is_status_prepare_inserted_to_material_preparation'
        ,'is_status_out_inserted_to_material_preparation'
    ];

    public function materialPreparationFabric()
    {
        return $this->belongsTo('App\Models\MaterialPreparationFabric');
    }

    public function materialStock()
    {
        return $this->belongsTo('App\Models\MaterialStock');
    }

    public function destinationLocator()
    {
        return $this->belongsTo('App\Models\Locator','destination_id');
    }

    public function planningPullCutting()
    {
        return $this->belongsTo('App\Models\PlanningPullCutting');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function locator()
    {
        return $this->belongsTo('App\Models\Locator','last_locator_id');
    }

    public function detailPoBuyerPerRoll()
    {
        return $this->hasMany('App\Models\DetailPoBuyerPerRoll')->whereNull('deleted_at');
    }

    public function getQtyNeedOnBom($po_buyer,$item_code,$style,$article_no,$is_piping,$job_order,$part_no){
        $qty_required =  DB::table('detail_material_requirements')
        ->where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code],
            ['style',$style],
            ['article_no',$article_no],
            ['is_piping',$is_piping],
            ['job_order',$job_order],
            ['part_no',$part_no],
        ])
        ->sum('qty_required');

        return $qty_required;
    }

    public function getDetailRoll($po_buyer,$item_code,$style,$article_no,$is_piping,$job_order,$part_no){
        //'material_stocks.nomor_roll',
        $details =  DB::table('detail_material_preparation_fabrics')
        ->join('material_stocks','material_stocks.id','detail_material_preparation_fabrics.material_stock_id')
        ->select('material_stocks.actual_width','detail_material_preparation_fabrics.uom',db::raw('sum(detail_material_preparation_fabrics.reserved_qty) as reserved_qty'))
        ->where([
            ['detail_material_preparation_fabrics.po_buyer',$po_buyer],
            ['detail_material_preparation_fabrics.item_code',$item_code],
            ['style',$style],
            ['article_no',$article_no],
            ['is_piping',$is_piping],
            ['job_order',$job_order],
            ['part_no',$part_no],
        ])
        //'material_stocks.nomor_roll'
        ->groupby('material_stocks.actual_width','detail_material_preparation_fabrics.uom')
        ->get();
        
        return $details;
    }

    public function getMargeBuyer($style,$article_no,$pcd){
        $details =  DB::table('detail_material_preparation_fabrics')
        ->select('po_buyer')
        ->where([
            ['style',$style],
            ['article_no',$article_no],
        ])
        ->whereIn('material_preparation_fabric_id',function($query)use($pcd){
            $query->select('id')
            ->from('material_preparation_fabrics')
            ->where('planning_date',$pcd);
        })
        ->groupby('po_buyer')
        ->get();
        
        $marge = '';
        $po_buyers = array();
        foreach ($details as $key => $detail) {
            $marge .=  $detail->po_buyer.',';
            $po_buyers [] = $detail->po_buyer;
        }

        $obj = new stdClass();
        $obj->marge = substr($marge,0,-1);
        $obj->po_buyers = $po_buyers;
        return $obj;

    }

    static function getDetailPreparationId($po_buyer,$article_no,$style,$planning_date,$barcode_preparation_fabric_id){
        $details =  DB::table('detail_material_preparation_fabrics')
        ->where([
            ['po_buyer',$po_buyer],
            ['article_no',$article_no],
            ['style',$style],
            ['barcode_preparation_fabric_id',$barcode_preparation_fabric_id],
        ])
        ->whereIn('material_preparation_fabric_id',function($query) use ($planning_date){
            $query->select('id')
            ->from('material_preparation_fabrics')
            ->where('planning_date',$planning_date);
        })
        ->get();

        $array = [];
        foreach ($details as $key => $detail) {
            $array [] = $detail->id;
        }

        return $array;
    }

    static function getQtyBom($po_buyer,$item_code,$style){
        $data = DB::table('material_requirements')
        ->where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code],
            ['style',$style],
        ])
         ->first();

        return ($data)? $data:null;
    }

    static function getOrderId($document_no,$item_code,$c_bpartner_id){
        $data = DB::table('material_arrivals')
        ->select('c_order_id')
        ->where([
            ['document_no',$document_no],
            ['item_code',$item_code],
            ['c_bpartner_id',$c_bpartner_id],
        ])
        ->groupby('c_order_id')
        ->first();

        return $data->c_order_id;
    }

    static function getRecycle($item_id)
    {
        $item = DB::table('items')
        ->where('item_id',$item_id)
        ->where('recycle', true)
        ->first();
        
        if($item)return '-Recycle';
        else return '';
    }
    

}
