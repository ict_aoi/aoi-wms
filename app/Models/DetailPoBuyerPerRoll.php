<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class DetailPoBuyerPerRoll extends Model
{
    use Uuids;
    public $timestmaps      = false;
    public $incrementing    = false;
    protected $dates        = ['created_at','updated_at','planning_date'];
    protected $guarded      = ['id'];
    protected $fillable     = ['material_planning_fabric_id'
        ,'detail_material_preparation_fabric_id'
        ,'detail_material_planning_fabric_id'
        ,'detail_material_planning_fabric_per_part_id'
        ,'planning_date'
        ,'po_buyer'
        ,'is_closing'
        ,'part_no'
        ,'article_no'
        ,'style'
        ,'style_in_job_order'
        ,'job_order'
        ,'qty_booking'
        ,'warehouse_id'
        ,'created_at'
        ,'updated_at'
        ,'deleted_at'
        ,'user_id'
    ];

    public function detailMaterialPreparationFabric(){
        return $this->belongsTo('App\Models\DetailMaterialPreparationFabric');
    }

    public function materialPlanningFabric(){
        return $this->belongsTo('App\Models\MaterialPlanningFabric');
    }


}
