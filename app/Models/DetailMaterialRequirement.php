<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


class DetailMaterialRequirement extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $dates = ['created_at','updated_at','statistical_date','promise_date'];
    protected $fillable = ['style'
    ,'item_code',
    'uom',
    'po_buyer',
    'lc_date',
    'job_order',
    'qty_required',
    'article_no',
    'is_piping',
    'statistical_date',
    'garment_size',
    'part_no',
    'season',
    'item_id',
    'item_desc',
    'warehouse_id',
    'component',
    'destination_name',
    'warehouse_name',
    'promise_date',
    'category',
    'supplier_code',
    'supplier_name',
    'qty_ordered_garment'];


    public function getItemPlanning($po_buyer){
        $items = db::table('material_planning_fabrics')
        ->select('item_code')
        ->where( 'po_buyer',$po_buyer)
        ->whereNull('deleted_at')
        ->groupby('item_code')
        ->get();

        $array = array();
        foreach ($items as $key => $item) {
           $array [] = $item->item_code;
        }
        return $array;
    }


    public function getTotalPreparationFabric($po_buyer,$item_code,$style,$article_no,$part_no,$is_piping){
        $reserved_qty = db::table('detail_material_preparation_fabrics')
        ->where( [
            ['po_buyer',$po_buyer],
            ['item_code',$item_code],
            ['style',$style],
            ['article_no',$article_no],
            ['part_no',$part_no],
            ['is_piping',$is_piping],
        ])
        ->sum('reserved_qty');

        return $reserved_qty;

    }
}
