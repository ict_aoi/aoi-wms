<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderErp extends Model
{
    protected $connection = 'erp';
    protected $guarded = ['c_order_id'];
    protected $table = 'c_order';
}
