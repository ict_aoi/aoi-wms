<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MaterialPlanningFabricPiping extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','preparation_date'];
    protected $guarded = ['id'];
    protected $fillable = ['material_planning_fabric_id','part_no', 'qty_booking','user_id','deleted_at'];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function materialPlanningFabric(){
        return $this->belongsTo('App\Models\MaterialPlanningFabric');
    }
}
