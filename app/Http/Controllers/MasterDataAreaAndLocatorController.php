<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use Illuminate\Support\Facades\Session;

use App\Models\Area;
use App\Models\Locator;
use App\Models\Barcode;

class MasterDataAreaAndLocatorController extends Controller
{
    public function index(Request $request)
    {
        $flag        = $request->session()->get('message');
        return view('master_data_area_and_locator.index',compact('flag'));
    }

    public function data(Request $request)
    {
        if ($request->ajax()) 
        { 
            $warehouse_id   = ($request->warehouse ? $request->warehouse : auth::user()->warehouse );
            $areas = Area::where([
                ['warehouse',$warehouse_id],
                ['is_active',true],
                ['is_destination',false]
            ]);

            return DataTables::of($areas)
            ->addColumn('action', function($areas){ 
                return view('master_data_area_and_locator._action', [
                    'model'     => $areas,
                    'edit'      => route('masterDataAreaAndLocator.edit', $areas->id),
                    'delete'    => route('masterDataAreaAndLocator.destroy', $areas->id)
                ]);
            })    
            ->make(true);
        }
    }

    public function create()
    {
        return view('master_data_area_and_locator.create');
    }

    public function store(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'name' => 'required',
            'area_locator' => 'required|not_in:[]'
        ]);
        

        if ($validator->passes()) 
        {
            $created_at = Carbon::now()->ToDateTimeString();
            $name       = strtoupper(trim(($request->name)));
            $warehouse  = strtoupper(trim(($request->warehouse)));
            $erp_id     = strtoupper(trim(($request->erp_id)));
        
            if($warehouse == '1000002' || $warehouse == '1000013') $type_po = 2;
            else if($warehouse == '1000011' || $warehouse == '1000001')  $type_po = 1;

            $is_area_exists = area::where([
                ['name',$name],
                ['warehouse',$warehouse],
                ['type_po',$type_po],
                ['is_destination',false]
            ]);
            if($erp_id) $is_area_exists = $is_area_exists->where('erp_id',$erp_id);
            $is_area_exists = $is_area_exists->exists();

            if($is_area_exists) return response()->json('Area already exists',422); 
            
            try
            {
                db::beginTransaction();

                $area       = area::FirstOrCreate([
                    'name'              => $name,
                    'warehouse'         => $warehouse,
                    'erp_id'            => $erp_id,
                    'is_destination'    => false,
                    'user_id'           => Auth::user()->id,
                    'created_at'        => $created_at,
                    'updated_at'        => $created_at,
                ]);
                
                $locators = json_decode($request->area_locator);
                $unique = 0;

                foreach ($locators as $key => $locator) 
                {
                    $rack                   = strtoupper(trim($locator->rack));
                    $row                    = $locator->row;
                    $start_column_locator   = $locator->start_colum;
                    $to_column_locator      = $locator->end_colum;
                    $has_many_po_buyer      = $locator->has_many_po_buyer;
                    
                    if($has_many_po_buyer == true)  $_has_many_po_buyer = 'true';
                    else  $_has_many_po_buyer = 'false';

                    
                    for ($i=$start_column_locator; $i <= $to_column_locator; $i++) 
                    { 
                        $get_barcode    = $this->randomCode();
                        $barcode        = $get_barcode->barcode;
                        $referral_code  = $get_barcode->referral_code;
                        $sequence       = $get_barcode->sequence;
                        $code           = $name.'-'.$rack.'.'.$row.'.'.($i);
                        
                        Locator::FirstorCreate([
                            'barcode'           => $barcode,
                            'area_id'           => $area->id,
                            'code'              => strtoupper($code),
                            'rack'              => strtoupper($rack),
                            'y_row'             => $row,
                            'z_column'          => $i,
                            'has_many_po_buyer' => $_has_many_po_buyer,
                            'user_id'           => Auth::user()->id,
                            'created_at'        => $created_at,
                            'updated_at'        => $created_at,
                        ]);
                    }
                }
                
                db::commit();
            }catch (Exception $ex)
            {
                db::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message); 
            }
            
            $request->session()->flash('message', 'success');
            return response()->json('success',200);    
        }else
        {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ],400); 
        }
        
    }

    public function edit($id)
    {
        $area       = Area::find($id);
        $locators   = $area->getRowLocator($id);
        
        return view('master_data_area_and_locator.edit',compact('area','locators'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'          => 'required',
            'area_locator'  => 'required|not_in:[]'
        ]);

        try
        {
            $created_at     = Carbon::now()->ToDateTimeString();
            $name           = strtoupper(trim(($request->name)));
            $warehouse      = strtoupper(trim(($request->warehouse)));
            $erp_id         = strtoupper(trim(($request->erp_id)));

            db::beginTransaction();

            $area               = Area::find($id);
            $area->name         = $name;
            $area->warehouse    = $warehouse;
            $area->erp_id       = $erp_id;
            $area->updated_at   = $created_at;
            $area->save();

            $locators           = json_decode($request->area_locator);
            $unique             = 0;

            foreach ($locators as $key => $locator) 
            {
                $rack                   = strtoupper(trim($locator->rack));
                $row                    = $locator->row;
                $start_column_locator   = $locator->start_colum;
                $to_column_locator      = $locator->end_colum;
                $has_many_po_buyer      = $locator->has_many_po_buyer;

                if($has_many_po_buyer == true) $_has_many_po_buyer = 'true';
                else $_has_many_po_buyer = 'false';
        
                for ($i= $start_column_locator; $i <= $to_column_locator; $i++) 
                { 
                    $code = $name.'-'.$rack.'.'.$row.'.'.($i);

                    $_locator = Locator::where([
                        ['area_id',$id],
                        ['rack',$rack],
                        ['y_row',$row],
                        ['z_column',$i],
                    ])
                    ->first();
                    
                    if(!$_locator)
                    {
                        $get_barcode    = $this->randomCode();
                        $barcode        = $get_barcode->barcode;
                        $referral_code  = $get_barcode->referral_code;
                        $sequence       = $get_barcode->sequence;
                        
                        Locator::FirstorCreate([
                            'barcode'           => $barcode,
                            'area_id'           => $area->id,
                            'code'              => $code,
                            'rack'              => $rack,
                            'y_row'             => $row,
                            'z_column'          => $i,
                            'has_many_po_buyer' => $_has_many_po_buyer,
                            'user_id'           => Auth::user()->id,
                            'created_at'        => $created_at,
                            'updated_at'        => $created_at,
                        ]);
                    }else
                    {
                        $_locator->code              = $code;
                        $_locator->rack              = $rack;
                        $_locator->y_row             = $row;
                        $_locator->z_column          = $i;
                        $_locator->has_many_po_buyer = $_has_many_po_buyer;
                        $_locator->updated_at        = $created_at;
                        $_locator->save();
                    }
                    $unique++;
                }
            }

            db::commit();
        }catch (Exception $ex)
        {
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message); 
        }
       
        $request->session()->flash('message', 'success_2');
        return response()->json('success',200);    


    }
    
    public function destroy($id)
    {
        $area = Area::find($id);
        $area->is_active = false;
        $locator = Locator::where('area_id',$area->id)->first();
        $locator->is_active = false;

        if($area->save() && $locator->save())
        {
            Session::flash("flash_notification", [
                    "level"=>"success",
                    "message"=>"Data deleted successfully"
            ]);

            return redirect()->route('area.index');
        }

	
    }

    static function randomCode()
    {
		$referral_code = 'R2'.Carbon::now()->format('u');
        $sequence = Barcode::where('referral_code',$referral_code)->max('sequence');

        if ($sequence == null) $sequence = 1;
        else $sequence += 1;

        $barcode = $referral_code.''.$sequence;

        Barcode::FirstOrCreate([
            'barcode'       => $barcode,
            'referral_code' => $referral_code,
            'sequence'      => $sequence,
        ]);

        $obj = new stdClass();
        $obj->barcode       = $barcode;
        $obj->referral_code = $referral_code;
        $obj->sequence      = $sequence;
		return $obj; 
    }
}
