<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Excel;
use Yajra\Datatables\Datatables;

class AccessoriesReportMaterialOutController extends Controller
{
    public function index()
    {
       
        return view('accessories_report_material_out.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id           = $request->warehouse;
            $start_date             = ($request->start_date) ? Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d') : Carbon::today()->subDays(30);
            $end_date               = ($request->end_date) ? Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d') : Carbon::now();
            
            $material_in = db::table('report_material_out_acc')
            ->where([
                ['warehouse',$warehouse_id]
            ])
            ->whereBetween('movement_date', [$start_date, $end_date])
            ->orderBy('movement_date','desc');
            
            
            return DataTables::of($material_in)
           
            // ->editColumn('created_at',function ($material_in)
            // {
            //     if($material_in->created_at) return Carbon::createFromFormat('Y-m-d', $material_in->created_at)->format('d/M/Y'); 
            //     else return null;
            // })
            ->editColumn('qty',function ($material_in)
            {
                return number_format($material_in->qty, 4, '.', ',');
            })
            ->editColumn('warehouse',function ($material_in)
            {

                if($material_in->warehouse == '1000002') return 'Warehouse ACC AOI 1';
                else if($material_in->warehouse == '1000013') return 'Warehouse ACC AOI 2';
            })
          
            // ->rawColumns(['is_piping','action','style','status'])
            ->make(true);
        }
    }

    public function exportAll(Request $request)
    {
        $warehouse_id       = ($request->warehouse ? $request->warehouse : auth::user()->warehouse);
        $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
        $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
        
        $_start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d') : Carbon::today()->subDays(30)->format('Y-m-d');
        $_end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d') : Carbon::now()->format('Y-m-d');
    
        
        $warehouse_name     = ($warehouse_id == '1000002' ?'AOI-1':'AOI-2' );

        $material_out = db::table('report_material_out_acc')
        ->where([
            ['warehouse',$warehouse_id]
        ])
        ->whereBetween('movement_date', [$start_date, $end_date])
        ->orderBy('movement_date','desc')
        ->get();
            
        
            

        $file_name = 'REPORT_MATERIAL_OUT'.$warehouse_name.'_FROM_'.$_start_date.'_TO_'.$_end_date;
    
      

        return Excel::create($file_name,function($excel) use ($material_out)
        {
            $excel->sheet('ACTIVE',function($sheet)use($material_out)
            {
                $sheet->setCellValue('A1','DATE_OUT');
                $sheet->setCellValue('B1','WAREHOUSE');
                $sheet->setCellValue('C1','PO_SUPPLIER');
                $sheet->setCellValue('D1','ITEM_CODE');
                $sheet->setCellValue('E1','PO_BUYER');
                $sheet->setCellValue('F1','UOM');
                $sheet->setCellValue('G1','QTY');
                $sheet->setCellValue('H1','STATUS');

            
            $row=2;

           
            foreach ($material_out as $i) 
            {  


                if($i->warehouse == '1000002') $warehouse_name =  'Warehouse Acc AOI 1';
                elseif($i->warehouse == '1000013') $warehouse_name =  'Warehouse Acc AOI 2';

                $sheet->setCellValue('A'.$row,$i->movement_date);
                $sheet->setCellValue('B'.$row,$warehouse_name);
                $sheet->setCellValue('C'.$row,$i->document_no);
                $sheet->setCellValue('D'.$row,$i->item_code);
                $sheet->setCellValue('E'.$row,$i->po_buyer);
                $sheet->setCellValue('F'.$row,$i->uom);
                $sheet->setCellValue('G'.$row,$i->qty);
                $sheet->setCellValue('H'.$row,$i->last_status_movement);
                $row++;
            }
            });

        })
        ->export('xlsx');
    }
}
