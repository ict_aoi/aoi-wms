<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use View;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\CuttingInstruction;
use App\Models\Item;
use App\Models\DetailMaterialPreparationFabric;

class FabricReportDailyRequestFabricController extends Controller
{
    public function index(Request $request)
    {
        return view('fabric_report_request_fabric.index');
    }

    public function data(Request $request)
    {
        // dd($request->toArray());
        if(request()->ajax()) 
        {
            $warehouse_id           = ($request->warehouse) ? $request->warehouse : '';
            $start_date             = ($request->start_date) ? Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d') : Carbon::today()->subDays(30);
            $end_date               = ($request->end_date) ? Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d') : Carbon::now();
            
            // dd($warehouse_id);
            $app_env = Config::get('app.env');
            if($app_env == 'live')
            {
                $data_cdms = DB::connection('cdms');
            } else{
                $data_cdms = DB::connection('cdms');
            }
            // jaz_balance_marke
            // $request_fabric   =  $data_cdms->table('jaz_balance_marker')
            // ->where('factory_id',$warehouse_id)
            // ->whereBetween('cutting_date',[$start_date,$end_date])
            // // ->orderby('queue','asc')
            // ->get();

            $request_fabric   =  $data_cdms->table('dd_balance_csi_ssp')
            ->where('factory_id',$warehouse_id)
            ->whereBetween('cutting_date',[$start_date,$end_date])
            ->orderby('queu','asc')
            ->get();
    

            // dd($request_fabric);
            
            return DataTables::of($request_fabric)
            ->editColumn('factory_id',function ($request_fabric)
            {
                if($request_fabric->factory_id == '1') return 'Warehouse fabric AOI 1';
                elseif($request_fabric->factory_id == '2') return 'Warehouse fabric AOI 2';
            })
            ->editColumn('cutting_date',function ($request_fabric)
            {
                return Carbon::createFromFormat('Y-m-d', $request_fabric->cutting_date)->format('d/M/Y');
            })
            ->editColumn('item_code',function ($request_fabric)
            {
                // $item = Item::where('item_code',$request_fabric->item_code)->first();
                return $request_fabric->item_code;
            })
            ->editColumn('marker_prod',function ($request_fabric)
            {
               
                    return number_format($request_fabric->marker_prod, 4, '.', ',');
                
            })
            ->editColumn('actual_marker_prod',function ($request_fabric)
            {
               
                    return number_format($request_fabric->actual_marker_prod, 4, '.', ',');
                
            })
            ->editColumn('balance',function ($request_fabric)
            {
               
                    return number_format($request_fabric->balance, 4, '.', ',');
                
            })
            ->addColumn('action',function($request_fabric){
                return view('fabric_report_request_fabric._action',[
                    'model'       => $request_fabric,
                    'detail'      => route('fabricReportDailyRequestFabric.detail',$request_fabric->marker_release_detail_id),
                    'confirm_whs' => route('fabricReportDailyRequestFabric.approveWhs',$request_fabric->marker_release_detail_id),
                    'confirm_lab' => route('fabricReportDailyRequestFabric.approveLab',$request_fabric->marker_release_detail_id),
                    'export'      => route('fabricReportDailyRequestFabric.exportDetail',$request_fabric->marker_release_detail_id),
                ]);
            })
            ->rawColumns(['action']) 
            ->make(true);
        }
    }

    public function approveWhs($id)
    {
        $app_env = Config::get('app.env');
            if($app_env == 'live')
            {
                $data_cdms = DB::connection('cdms');
            }

        $data_cdms->table('detail_request_fabrics')->where('id', $id)
                  ->update(['approval_whs_by' => Auth::user()->name, 'approval_whs_at' => Carbon::now()]);


    }

    public function approveLab($id)
    {
        $app_env = Config::get('app.env');
            if($app_env == 'live')
            {
                $data_cdms = DB::connection('cdms');
            }
            
        $data_cdms->table('detail_request_fabrics')->where('id', $id)
                  ->update(['approval_lab_by' => Auth::user()->name, 'approval_lab_at' => Carbon::now()]);
        
    }
}
