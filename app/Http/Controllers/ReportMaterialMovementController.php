<?php namespace App\Http\Controllers;

use DB;
use Excel;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\PoBuyer;

class ReportMaterialMovementController extends Controller
{
    public function index()
    {
        return view('report_material_movement.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $po_buyer       = $request->po_buyer;
            $movements      = DB::select(db::raw("SELECT * FROM get_movements_using_cte(
				'".$po_buyer."'
				)
                order by barcode asc, movement_date asc;"
			));
            
            return DataTables::of($movements)
            ->editColumn('movement_date',function ($movements)
            {
                if($movements->movement_date) return Carbon::createFromFormat('Y-m-d H:i:s', $movements->movement_date)->format('d/M/Y H:i:s');
                else return null;
            })
            ->editColumn('status',function ($movements){
                if($movements->status == 'receive' || $movements->status == 'receiving') return '<span class="label label-info">MATERIAL DITERIMA DIGUDANG</span>';
                elseif($movements->status == 'not receive') return '<span class="label label-default">MATERIAL BELUM DITERIMA DIGUDANG</span>';
                elseif($movements->status == 'in') return '<span class="label label-info">MATERIAL SIAP SUPLAI</span>';
                elseif($movements->status == 'expanse') return '<span class="label label-warning">EXPENSE</span>';
                elseif($movements->status == 'out') return '<span class="label label-success">MATERIAL SUDAH DISUPLAI</span>';
                elseif($movements->status == 'out-handover') return '<span class="label label-success">MATERIAL PINDAH TANGAN</span>';
                elseif($movements->status == 'out-subcont')  return '<span class="label label-success">MATERIAL SUDAH DISUPLAI KE SUBCONT</span>';
                elseif($movements->status == 'reject')  return '<span class="label label-danger">REJECT</span>';
                elseif($movements->status == 'partial reject')  return '<span class="label label-danger">REJECT SETENGAH</span>';
                elseif($movements->status == 'reject-metal-detector')  return '<span class="label label-danger">REJECT METAL DETECTOR</span>';
                elseif($movements->status == 'change') return '<span class="label" style="background-color:black">MATERIAL PINDAH LOKASI RAK</span>';
                elseif($movements->status == 'hold') return '<span class="label" style="background-color:#fb8d00">MATERIAL DITAHAN QC UNTUK PENGECEKAN</span>';
                elseif($movements->status == 'hold-metal-detector') return '<span class="label" style="background-color:#fb8d00">MATERIAL DITAHAN METAL DETECTOR UNTUK PENGECEKAN</span>';
                elseif($movements->status == 'adjustment' || $movements->status == 'adjusment') return '<span class="label" style="background-color:#838606">PENYESUAIAN QTY</span>';
                elseif($movements->status == 'closing') return '<span class="label" style="background-color:#cc6699">CLOSING</span>';
                elseif($movements->status == 'print') return '<span class="label" style="background-color:#cc00ff">CETAK BARCODE</span>';
                elseif($movements->status == 'reroute') return '<span class="label" style="background-color:#ff6666">REROUTE</span>';
                elseif($movements->status == 'cancel item')  return '<span class="label" style="background-color:#ff6666">CANCEL ITEM</span>';
                elseif($movements->status == 'cancel order') return '<span class="label" style="background-color:#ff6666">CANCEL ORDER</span>';
                elseif($movements->status == 'check') return '<span class="label" style="background-color:#ef6a22">MATERIAL DALAM PENGECEKAN QC</span>';
                elseif($movements->status == 'out-cutting') return '<span class="label" style="background-color:#e600e6">MATERIAL SUDAH DISUPLAI KE CUTTING</span>';    
                elseif($movements->status == 'relax') return '<span class="label" style="background-color:#e65c00">RELAX CUTTING</span>';    
                elseif($movements->status == 'reverse' || $movements->status == 'reverse-handover' ||  $movements->status == 'reverse-stock') return '<span class="label" style="background-color:#00e6e6">MATERIAL DIKEMBALIKAN KE INVENTORY</span>';    
                elseif($movements->status == 'reverse-qc') return '<span class="label" style="background-color:#00e6e6">MATERIAL TIDAK JADI REJECT QC</span>';    
                elseif($movements->status == 'release') return '<span class="label" style="background-color:#6cc669">MATERIAL LOLOS QC</span>';
                elseif($movements->status == 'release-metal-detector') return '<span class="label" style="background-color:#6cc669">MATERIAL LOLOS METAL DETECTOR</span>';    
                elseif($movements->status == 'switch') return '<span class="label" style="background-color:#333300">SWITCH</span>';    
                elseif($movements->status == 'cancel backlog') return '<span class="label" style="background-color:#b30000">CANCEL BACKLOG</span>';    
                else return '<span class="label label-default">'.$movements->status.'</span>';
                
            })
            ->editColumn('warehouse',function ($movements)
            {
                if($movements->warehouse == '1000002') return 'Warehouse accessories AOI 1';
                elseif($movements->warehouse == '1000013') return 'Warehouse accessories AOI 2';
                elseif($movements->warehouse == '1000011') return 'Warehouse fabric AOI 2';
                elseif($movements->warehouse == '1000001') return 'Warehouse fabric AOI 1';
            })
            ->editColumn('qty_need',function ($movements)
            {
                return number_format($movements->qty_need, 4, '.', ',');
            })
            ->editColumn('is_additional',function ($movements)
            {
                if ($movements->is_additional) return '<div class="checker border-info-600 text-info-800">
                    <span class="checked"><input type="checkbox" class="control-info" checked="checked"></span>
                </div>';
                else return null;
            })
            ->editColumn('qty_movement',function ($movements)
            {
                return number_format($movements->qty_movement, 4, '.', ',');
            })
            ->setRowAttr([
                'style' => function($movements) {
                    if($movements->qty_need == 0){
                        return  'background-color: #f44336;color:white';
                    }

                    if($movements->warehouse == '1000013'){
                        return  'background-color: #ffd6fe;color:black';
                    }
                },
            ])
            ->rawColumns(['status', 'is_additional'])
            ->make(true);
        }
    }

    public function poBuyerPicklist(Request $request)
    {
        $po_buyer = $request->q;
        $lists = PoBuyer::select('po_buyer')
        ->where(function($query) use ($po_buyer){ 
            $query->where('po_buyer','like',"%$po_buyer%");
        })
        ->groupby('po_buyer')
        ->paginate(10);

        return view('report_material_movement._po_buyer_picklist',compact('lists'));
    }

    public function export(Request $request)
    {
        if($request->po_buyer=='')
        {
            return redirect()->back()
            ->withErrors([
                'po_buyer' => 'Please select po buyer first',
            ]);
        } 

        $po_buyer   = trim($request->po_buyer);
        $data       = DB::select(db::raw("SELECT * FROM get_movements_using_cte(
            '".$po_buyer."'
            );"
        ));

        return Excel::create('LAPORAN_PERPINDAHAN_BARANG_PO_BUYER_'.$po_buyer,function ($excel) use($data){
            $excel->sheet('ACTIVE', function($sheet) use($data) {
                $sheet->setCellValue('A1','#');
                $sheet->setCellValue('B1','PO BUYER');
                $sheet->setCellValue('C1','NO. PO SUPPLIER');
                $sheet->setCellValue('D1','KODE ITEM');
                $sheet->setCellValue('E1','DESKRIPSI ITEM');
                $sheet->setCellValue('F1','KATEGORI');
                $sheet->setCellValue('G1','WAREHOUSE');
                $sheet->setCellValue('H1','STYLE');
                $sheet->setCellValue('I1','ARTICLE_NO');
                $sheet->setCellValue('J1','UOM');
                $sheet->setCellValue('K1','ASAL');
                $sheet->setCellValue('L1','TUJUAN');
                $sheet->setCellValue('M1','NEED');
                $sheet->setCellValue('N1','QTY PINDAH');
                $sheet->setCellValue('O1','STATUS');
                $sheet->setCellValue('P1','IS_ADDITIONAL');
                $sheet->setCellValue('Q1','TANGGAL PINDAH');
                $sheet->setCellValue('R1','USER');
                $sheet->setCellValue('S1','BACKLOG STATUS');
                $sheet->setCellValue('T1','NOTE');
                
                $index = 1;
                $row = 2;
                foreach($data as $i){
                    
                    $warehouse = '';
                    if($i->warehouse == '1000002') $warehouse = 'Warehouse accessories AOI 1';
                    elseif($i->warehouse == '1000013') $warehouse = 'Warehouse accessories AOI 2';
                    elseif($i->warehouse == '1000011') $warehouse = 'Warehouse fabric AOI 2';
                    elseif($i->warehouse == '1000001') $warehouse = 'Warehouse fabric AOI 1';

                    $sheet->setCellValue('A'.$row, $index);
                    $sheet->setCellValue('B'.$row, $i->po_buyer);
                    $sheet->setCellValue('C'.$row, $i->document_no);
                    $sheet->setCellValue('D'.$row, $i->item_code);
                    $sheet->setCellValue('E'.$row, $i->item_desc);
                    $sheet->setCellValue('F'.$row, $i->category);
                    $sheet->setCellValue('G'.$row, $warehouse);
                    $sheet->setCellValue('H'.$row, $i->style);
                    $sheet->setCellValue('I'.$row, $i->article_no);
                    $sheet->setCellValue('J'.$row, $i->uom);
                    $sheet->setCellValue('K'.$row, $i->from_location);
                    $sheet->setCellValue('L'.$row, $i->to_destination);
                    $sheet->setCellValue('M'.$row, number_format($i->qty_need, 4, '.', ','));
                    $sheet->setCellValue('N'.$row, number_format($i->qty_movement, 4, '.', ','));
                    $sheet->setCellValue('O'.$row, $i->status);
                    $sheet->setCellValue('P'.$row, $i->is_additional);
                    $sheet->setCellValue('Q'.$row, $i->movement_date);
                    $sheet->setCellValue('R'.$row, $i->user_name);
                    $sheet->setCellValue('S'.$row, $i->baclog_status);
                    $sheet->setCellValue('T'.$row, $i->movement_line_notes);

                    $index++;
                    $row++;
                }
            });
        })
        ->export('xlsx');
    }
    
}
