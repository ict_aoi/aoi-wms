<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use Config;
use stdClass;
use Validator;
use Carbon\Carbon;
use IntlDateFormatter;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;

use App\Models\PurchaseItem;
use App\Models\MaterialPreparationFabric;
use App\Models\DetailMaterialPlanningFabric;
use App\Models\DetailMaterialPreparationFabric;


class MasterDataPurchaseItemController extends Controller
{
    public function index()
    {
        $months = [
            '01' => 'January',
            '02' => 'February',
            '03' => 'March',
            '04' => 'April',
            '05' => 'May',
            '06' => 'June',
            '07' => 'July',
            '08' => 'August',
            '09' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        ];

        $years              = array();
        $current_year       = 2017;

        while($current_year <= carbon::now()->format('Y'))
        {
            $years[$current_year]    = $current_year;
            $current_year++;
        }

        return view('master_data_purchase_item.index',compact('months','years'));
    }

    public function data(Request $request)
    {
        

        if(request()->ajax()) 
        {
            $month_receive  = ($request->month)? $request->month : carbon::now()->format('m');
            $year_receive   = ($request->year)? $request->year : carbon::now()->format('Y');
            $_year_receive  = Carbon::createFromFormat('Y', $year_receive)->format('y');
            $period_po      = $_year_receive.$month_receive;
            $warehouse      = $request->warehouse;
            
            $purchase_items = PurchaseItem::whereNotNull('po_buyer')
            ->where([
                ['warehouse','like',"%$warehouse%"],
                ['po_buyer','!=',''],
            ])
            ->whereNull('deleted_at')
            ->whereNotNull('po_buyer')
            ->whereIn('document_no',function($query) use($period_po) {
                $query->select('document_no')
                ->from('po_suppliers')
                ->where('periode_po',$period_po)
                ->groupby('document_no');
            })
            ->orderby('po_buyer','asc');
                
        

            return DataTables::of($purchase_items)
            ->editColumn('status_po_buyer',function($purchase_items)
            {
                if($purchase_items->status_po_buyer == 'active') return '<span class="label label-info">ACTIVE</span>';
                else if($purchase_items->status_po_buyer == 'cancel') return '<span class="label label-danger">CANCEL</span>';
                else return $purchase_items->status_po_buyer;
            })
            ->editColumn('warehouse',function($purchase_items)
            {
                if($purchase_items->warehouse == '1000011') return 'Warehouse fabric AOI 2';
                else if($purchase_items->warehouse == '1000001') return 'Warehouse fabric AOI 1';
                else if($purchase_items->warehouse == '1000013') return 'Warehouse accessories AOI 2';
                else if($purchase_items->warehouse == '1000002') return 'Warehouse accessories AOI 1';
            
            })
            ->setRowAttr([
                'style' => function($purchase_items){
                    if($purchase_items->status_po_buyer == 'cancel')
                    {
                        return  'background-color: #f44336;color:white';
                    }
                    if($purchase_items->is_moq)
                    {
                        return  'background-color: #00bcd4;color:white';
                    }
                },
            ])
            ->rawColumns(['status_po_buyer','action','style'])
            ->make(true);
        }

    }
}
