<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\DetailMaterialPreparationFabric;

class FabricReportDailyMaterialOutController extends Controller
{
    public function index()
    {
        return view('fabric_report_daily_material_out.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id       = $request->warehouse;
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
            
            $daily_supplied     = DB::table('monitoring_material_out_fabric_v')
            ->where('warehouse_id','LIKE',"%$warehouse_id%")
            ->whereBetween('planning_date',[$start_date,$end_date])
            ->take(100);

            
            return DataTables::of($daily_supplied)
            ->editColumn('last_scan_prepare_date',function ($daily_supplied)
            {
                if($daily_supplied->last_scan_prepare_date) return  Carbon::createFromFormat('Y-m-d H:i:s', $daily_supplied->last_scan_prepare_date)->format('d/M/Y H:i:s');
                else return null;
            })
            ->editColumn('planning_date',function ($daily_supplied)
            {
                return  Carbon::createFromFormat('Y-m-d', $daily_supplied->planning_date)->format('d/M/Y');
            })
            ->editColumn('last_movement_out_date',function ($daily_supplied)
            {
                if($daily_supplied->last_movement_out_date) return  Carbon::createFromFormat('Y-m-d H:i:s', $daily_supplied->last_movement_out_date)->format('d/M/Y H:i:s');
                else return null;
            })
            ->editColumn('is_piping',function ($daily_supplied)
            {
                if($daily_supplied->is_piping) return '<span class="label label-info">Piping</span>';
                else return '<span class="label label-default">Non Piping</span>';
            })
            ->editColumn('total_prepare',function ($daily_supplied)
            {
                return number_format($daily_supplied->total_prepare, 4, '.', ',').' ('.$daily_supplied->uom.')';
            })
            ->editColumn('total_supply',function ($daily_supplied)
            {
                return number_format($daily_supplied->total_supply, 4, '.', ',').' ('.$daily_supplied->uom.')';
            })
            ->editColumn('balance',function ($daily_supplied)
            {
                return number_format($daily_supplied->balance, 4, '.', ',').' ('.$daily_supplied->uom.')';
            })
            ->editColumn('qty_bom',function ($daily_supplied)
            {
                return number_format($daily_supplied->qty_bom, 4, '.', ',').' ('.$daily_supplied->uom.')';
            })
            ->editColumn('warehouse_id',function ($daily_supplied)
            {
                if($daily_supplied->warehouse_id == '1000001') return 'Warehouse fabric AOI 1';
                elseif($daily_supplied->warehouse_id == '1000011') return 'Warehouse fabric AOI 2';
            })
            ->addColumn('action',function ($daily_supplied)
            {
                return view('fabric_report_daily_material_out._action',[
                    'model'         => $daily_supplied,
                    'detail'        => route('fabricReportDailyMaterialOut.detail',[
                        'planning_date'     => $daily_supplied->planning_date,
                        'warehouse_id'      => $daily_supplied->warehouse_id,
                        'c_order_id'        => $daily_supplied->c_order_id,
                        'item_id_source'    => $daily_supplied->item_id_source,
                        'item_id_book'      => $daily_supplied->item_id_book,
                        'is_piping'         => $daily_supplied->is_piping,
                        'style'             => $daily_supplied->style,
                        'article_no'        => $daily_supplied->article_no,
                        'po_buyer'          => $daily_supplied->po_buyer,
                    ]),
                ]);
            })
            ->setRowAttr([
                'style' => function($daily_supplied)
                {
                    if(round($daily_supplied->total_outstanding, 4) == 0 && round($daily_supplied->total_prepare, 4) != 0) return  'background-color: #d9ffde;';
                },
            ])
            ->rawColumns(['is_piping','action', 'style'])
            ->make(true);
        }
    }

    public function detail(Request $request)
    {
        $planning_date          = $request->planning_date;
        $warehouse_id           = $request->warehouse_id;
        $c_order_id             = $request->c_order_id;
        $item_id_book           = $request->item_id_book;
        $item_id_source         = $request->item_id_source;
        $is_piping              = $request->is_piping;
        $style                  = $request->style;
        $article_no             = $request->article_no;
        $po_buyer               = $request->po_buyer;

        $header                 = DB::table('monitoring_material_out_fabric_v')
        ->where([
            ['planning_date',$planning_date],
            ['warehouse_id',$warehouse_id],
            ['c_order_id',$c_order_id],
            ['item_id_source',$item_id_source],
            ['item_id_book',$item_id_book],
            ['style',$style],
            ['article_no',$article_no],
            ['po_buyer',$po_buyer],
        ])
        ->first();

        return view('fabric_report_daily_material_out.detail',compact('header'));
    }

    public function detailData(Request $request)
    {
        if(request()->ajax()) 
        {
            $planning_date      = $request->planning_date;
            $warehouse_id       = $request->warehouse_id;
            $article_no         = $request->article_no;
            $style              = $request->style;
            $po_buyer           = $request->po_buyer;
            $is_piping          = $request->is_piping;
            $c_order_id         = $request->c_order_id;
            $item_id_book       = $request->item_id_book;
            $item_id_source     = $request->item_id_source;
            
            $detail             = DB::table('detail_monitoring_out_fabric_v')
            ->where([
                ['planning_date',$planning_date],
                ['warehouse_id',$warehouse_id],
                ['article_no',$article_no],
                ['style',$style],
                ['po_buyer',$po_buyer],
                ['is_piping',$is_piping],
                ['c_order_id',$c_order_id],
                ['item_id_book',$item_id_book],
                ['item_id_source',$item_id_source],
            ])
            ->orderby('nomor_roll','desc');

            
            return DataTables::of($detail)
            
            ->addColumn('total_qty',function ($detail)
            {
                return number_format($detail->total_qty, 4, '.', ',').' ('.$detail->uom.')';
            })
            ->make(true);
        }
    }

    public function export(Request $request)
    {
        $warehouse_id       = ($request->warehouse ? $request->warehouse : auth::user()->warehouse);
        $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
        $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
        $warehouse_name     = ($warehouse_id == '1000001' ?'AOI-1':'AOI-2' );
        
        $daily_supplied = DB::table('monitoring_material_out_fabric_v')
        ->whereBetween('planning_date',[$start_date,$end_date])
        ->where('warehouse_id',$warehouse_id)
        ->orderBy('planning_date','ASC')
        ->get();

        $file_name = 'DAILY_MATERIAL_OUT_FABRIC_'.$warehouse_name.'_FROM_PLANNING_'.$start_date.'_TO_'.$end_date;
        
        return Excel::create($file_name,function($excel) use ($daily_supplied)
        {
            $excel->setCreator('ICT')
            ->setCompany('AOI');

            
            $excel->sheet('ACTIVE',function($sheet)use($daily_supplied)
            {
                $sheet->setCellValue('A1','PLANNING_DATE');
                $sheet->setCellValue('B1','STYLE');
                $sheet->setCellValue('C1','PO_BUYER');
                $sheet->setCellValue('D1','DESTINATION');
                $sheet->setCellValue('E1','ARTICLE_NO');
                $sheet->setCellValue('F1','ITEM_CODE_BOOK');
                $sheet->setCellValue('G1','ITEM_CODE_SOURCE');
                $sheet->setCellValue('H1','ITEM_CODE_BOOK_COLOR');
                $sheet->setCellValue('I1','IS_PIPING');
                $sheet->setCellValue('J1','TOTAL_QTY_NEED');
                $sheet->setCellValue('K1','PO_SUPPLIER');
                $sheet->setCellValue('L1','LOT');
                $sheet->setCellValue('M1','TOTAL_ROLL');
                $sheet->setCellValue('N1','TOTAL_QTY_PREPARED');
                $sheet->setCellValue('O1','TOTAL_QTY_SUPPLY');
                $sheet->setCellValue('P1','TOTAL_QTY_OUTSTANDING');
                $sheet->setCellValue('Q1','BALANCE');
                $sheet->setCellValue('R1','LAST_PREPARATION_DATE');
                $sheet->setCellValue('S1','LAST_SUPPLY_DATE');
                $sheet->setCellValue('T1','STATUS');
            
            $row=2;
            foreach ($daily_supplied as $i) 
            {  
                $sheet->setCellValue('A'.$row,$i->planning_date);
                $sheet->setCellValue('B'.$row,$i->style);
                $sheet->setCellValue('C'.$row,$i->po_buyer);
                $sheet->setCellValue('D'.$row,$i->destination);
                $sheet->setCellValue('E'.$row,$i->article_no);
                $sheet->setCellValue('F'.$row,$i->item_code_book);
                $sheet->setCellValue('G'.$row,$i->item_code_source);
                $sheet->setCellValue('H'.$row,$i->color);
                $sheet->setCellValue('I'.$row,$i->is_piping);
                $sheet->setCellValue('J'.$row,$i->qty_bom);
                $sheet->setCellValue('K'.$row,$i->document_no);
                $sheet->setCellValue('L'.$row,$i->actual_lot);
                $sheet->setCellValue('M'.$row,$i->total_roll);
                $sheet->setCellValue('N'.$row,number_format($i->total_prepare, 4, '.', ','));
                $sheet->setCellValue('O'.$row,number_format($i->total_supply, 4, '.', ','));
                $sheet->setCellValue('P'.$row,number_format($i->total_outstanding, 4, '.', ','));
                $sheet->setCellValue('Q'.$row,$i->balance);
                $sheet->setCellValue('R'.$row,$i->last_scan_prepare_date);
                $sheet->setCellValue('S'.$row,$i->last_movement_out_date);
                if(sprintf('%0.4f',$i->total_prepare) > 0.0000)
                {
                    if(sprintf('%0.4f',$i->total_outstanding) > 0.0000)
                    {
                        $status = "BELUM SCAN OUT";
                    }
                    else
                    {
                        $status = "SUDAH SCAN OUT";
                    }

                }
                else
                {
                    $status ="";
                }
                $sheet->setCellValue('T'.$row,$status);
                $row++;
            }
            });

        })
        ->export('xlsx');
    }

    public function getUserPicklist(Request $request)
    {
        $user_id          = $request->user_id;
        $warehouse_id    = $request->warehouse_id;

        $users = DetailMaterialPreparationFabric::join('users','users.id','detail_material_preparation_fabrics.user_id')
        ->where('warehouse_id',$warehouse_id)
        ->groupby('users.name','users.id')
        ->pluck('users.name','users.id')
        ->all();
        
        return response()->json(['users' => $users,'user_id' => $user_id]);
    }
}
