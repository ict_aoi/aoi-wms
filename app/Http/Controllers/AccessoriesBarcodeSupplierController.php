<?php namespace App\Http\Controllers;

use DB;
use Config;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Item;
use App\Models\User;
use App\Models\Temporary;
use App\Models\Locator;
use App\Models\Supplier;
use App\Models\UomConversion;
use App\Models\MaterialStock;
use App\Models\MaterialArrival;
use App\Models\AutoAllocation;
use App\Models\MaterialRequirement;
use App\Models\DetailMaterialStock;
use App\Models\PurchaseOrderDetail;
use App\Models\PurchaseOrderDetailDev;
use App\Models\ItemPackingList;

use App\Http\Controllers\AccessoriesMaterialStockApprovalController as HistoryStock;
use App\Http\Controllers\AccessoriesReportMaterialStockController as ReportStock;
use App\Models\PoBuyer;


class AccessoriesBarcodeSupplierController extends Controller
{
    public function index(request $request)
    {
        // return view('errors.migration');
        $no_invoice         = strtoupper(trim($request->no_invoice));
        $no_packing_list    = strtoupper(trim($request->no_packing_list));
        $document_no        = strtoupper(trim($request->document_no));
        $po_buyer           = strtoupper(trim($request->po_buyer));
        $item_code          = strtoupper(trim($request->item_code));
        //$warehouse          = $request->warehouses;

        if($no_packing_list==null && $document_no==null && $no_invoice==null && $po_buyer== null && $item_code ==null) return view('accessories_barcode_supplier.index');

        $app_env = Config::get('app.env');
        if($app_env == 'live')
        {
            $po_details = PurchaseOrderDetail::select(db::raw('max(po_detail_id) as po_detail_id'),'c_order_id','c_orderline_id',
            'pobuyer','no_packinglist','supplier','documentno','desc_product','uomsymbol','qty_upload','qtyordered','item','qty_carton',
            'kst_joborder','category', 'm_product_id', 'c_bpartner_id', 'qtyentered', 'qtyordered', 'type_po', 'm_warehouse_id', 'kst_resi',
            'kst_suratjalanvendor', 'kst_invoicevendor', 'kst_season')
            ->where([
                ['type_po',2],
                ['is_locked',true],
                ['isactive',true],
                ['flag_erp',true]
            ]);

            if($document_no != null || $document_no !='') $po_details = $po_details->where(db::raw('upper(documentno)'),'LIKE',"%$document_no%");
            if($no_invoice != null || $no_invoice !='') $po_details = $po_details->where(db::raw('upper(kst_invoicevendor)'),$no_invoice);
            if($no_packing_list != null || $no_packing_list !='') $po_details = $po_details->where(db::raw('upper(no_packinglist)'),$no_packing_list);
            if($po_buyer != null || $po_buyer !='') $po_details = $po_details->where('pobuyer',$po_buyer);
            if($item_code != null || $item_code !='') $po_details = $po_details->where(db::raw('upper(item)'),'LIKE',"%$item_code%");

            $po_details = $po_details->groupby('c_order_id','c_orderline_id','pobuyer','no_packinglist','supplier'
            ,'documentno','desc_product','uomsymbol','qty_upload','qtyordered','item','qty_carton','kst_joborder','flag_erp','category', 'm_product_id', 'c_bpartner_id', 'qtyentered', 'qtyordered', 'type_po', 'm_warehouse_id', 'kst_resi',
            'kst_suratjalanvendor', 'kst_invoicevendor', 'kst_season')
            ->get();

            //return response()->json($po_details);
            $data_web_po = $po_details;
            $document_no = $document_no;
        }else if($app_env == 'dev')
        {
            $po_details = PurchaseOrderDetailDev::select(db::raw('max(po_detail_id) as po_detail_id'),'c_order_id','c_orderline_id',
            'pobuyer','no_packinglist','supplier','documentno','desc_product','uomsymbol','qty_upload','qtyordered','item','qty_carton','kst_joborder')
            ->where([
                ['type_po',2],
                ['is_locked',true],
                ['isactive',true],
                ['flag_erp',true]
            ]);

            if($document_no != null || $document_no !='') $po_details = $po_details->where(db::raw('upper(documentno)'),'LIKE',"%$document_no%");
            if($no_invoice != null || $no_invoice !='') $po_details = $po_details->where(db::raw('upper(kst_invoicevendor)'),$no_invoice);
            if($no_packing_list != null || $no_packing_list !='') $po_details = $po_details->where(db::raw('upper(no_packinglist)'),$no_packing_list);
            if($po_buyer != null || $po_buyer !='') $po_details = $po_details->where('pobuyer',$po_buyer);
            if($item_code != null || $item_code !='') $po_details = $po_details->where(db::raw('upper(item)'),'LIKE',"%$item_code%");

            $po_details = $po_details->groupby('c_order_id','c_orderline_id','pobuyer','no_packinglist','supplier'
            ,'documentno','desc_product','uomsymbol','qty_upload','qtyordered','item','qty_carton','kst_joborder','flag_erp')
            ->get();

            //return response()->json($po_details);
            $data_web_po = $po_details;
            $document_no = $document_no;
        }else
        {
            $po_details = PurchaseOrderDetail::select(db::raw('max(po_detail_id) as po_detail_id'),'c_order_id','c_orderline_id',
            'pobuyer','no_packinglist','supplier','documentno','desc_product','uomsymbol','qty_upload','qtyordered','item','qty_carton',
            'kst_joborder','category', 'm_product_id', 'c_bpartner_id', 'qtyentered', 'qtyordered', 'type_po', 'm_warehouse_id', 'kst_resi',
            'kst_suratjalanvendor', 'kst_invoicevendor')
            ->where([
                ['type_po',2],
                ['is_locked',true],
                ['isactive',true],
                ['flag_erp',true]
            ]);

            if($document_no != null || $document_no !='') $po_details = $po_details->where(db::raw('upper(documentno)'),'LIKE',"%$document_no%");
            if($no_invoice != null || $no_invoice !='') $po_details = $po_details->where(db::raw('upper(kst_invoicevendor)'),$no_invoice);
            if($no_packing_list != null || $no_packing_list !='') $po_details = $po_details->where(db::raw('upper(no_packinglist)'),$no_packing_list);
            if($po_buyer != null || $po_buyer !='') $po_details = $po_details->where('pobuyer',$po_buyer);
            if($item_code != null || $item_code !='') $po_details = $po_details->where(db::raw('upper(item)'),'LIKE',"%$item_code%");

            $po_details = $po_details->groupby('c_order_id','c_orderline_id','pobuyer','no_packinglist','supplier'
            ,'documentno','desc_product','uomsymbol','qty_upload','qtyordered','item','qty_carton','kst_joborder','flag_erp')
            ->get();

            //return response()->json($po_details);
            $data_web_po = $po_details;
            $document_no = $document_no;
        }
        //dd($po_details);
        $is_carton = false;
        foreach($po_details as $key => $po_detail)
        {
            if($po_detail->category == 'CT')
            {
                $po_detail_id    = $po_detail->po_detail_id;
                $item_id         = $po_detail->m_product_id;
                $item_code       = $po_detail->item;
                $item_desc       = $po_detail->desc_product;
                $c_order_id      = $po_detail->c_order_id;
                $c_orderline_id  = $po_detail->c_orderline_id;
                $c_bpartner_id   = $po_detail->c_bpartner_id;
                $document_no     = $po_detail->documentno;

                $no_packing_list = $po_detail->no_packinglist;
                $qty_entered     = $po_detail->qtyentered;
                $qty_ordered     = $po_detail->qtyordered;
                $qty_carton      = $po_detail->qty_carton;
                $qty_upload      = $po_detail->qty_upload;
                $category        = $po_detail->category;

                $uom             = $po_detail->uomsymbol;
                $supplier_name   = $po_detail->supplier;
                $type_po         = $po_detail->type_po;
                $warehouse_id    = $po_detail->m_warehouse_id;

                $no_resi         = $po_detail->kst_resi;
                $no_surat_jalan  = $po_detail->kst_suratjalanvendor;
                $no_invoice      = $po_detail->kst_invoicevendor;
                $season          = $po_detail->kst_season;

                $is_carton       = true;

                $is_exist = MaterialArrival::where('po_detail_id', $po_detail_id)->exists();

                if(!$is_exist)
                {
                    
                    try 
                    {
                        DB::beginTransaction();

                        //insert ke material arrivals

                        $is_exist = MaterialArrival::where('po_detail_id', $po_detail_id)->exists();

                        if(!$is_exist)
                        {
                        $material_arrival = MaterialArrival::FirstOrCreate([
                            'po_detail_id'          => $po_detail_id,
                            'material_subcont_id'   => null,
                            'item_id'               => $item_id,
                            'item_code'             => $item_code,
                            'item_desc'             => $item_desc,
                            'supplier_name'         => $supplier_name,
                            'document_no'           => $document_no,
                            'type_po'               => $type_po,
                            'c_order_id'            => $c_order_id,
                            'c_orderline_id'        => $c_orderline_id,
                            'c_bpartner_id'         => $c_bpartner_id,
                            'warehouse_id'          => $warehouse_id,
                            //'po_buyer'              => $po_buyer,
                            'category'              => $category,
                            'uom'                   => $uom,
                            'no_packing_list'       => $no_packing_list,
                            'no_resi'               => $no_resi,
                            'no_surat_jalan'        => $no_surat_jalan,
                            'no_invoice'            => $no_invoice,
                            'qty_entered'           => $qty_entered,
                            'qty_ordered'           => $qty_ordered,
                            'qty_carton'            => $qty_carton,
                            'qty_upload'            => $qty_upload,
                            'qty_available_rma'     => $qty_upload,
                            'qty_reserved_rma'      => 0,
                            'foc_reserved_rma'      => 0,
                            'is_subcont'            => false,
                            'prepared_status'       => 'NEED PREPARED ALLOCATION',
                            'is_active'             => true,
                            //'is_moq'                => $is_moq,
                            'user_id'               => Auth::user()->id
                        ]);

                        

                    //insert ke material stocks
                    $free_stock_destination = Locator::with('area')
                    ->whereHas('area',function ($query) use($warehouse_id) 
                    {
                        $query->where([
                            ['warehouse',$warehouse_id],
                            ['name','FREE STOCK'],
                            ['is_destination',true]
                        ]);

                    })
                    ->first();

                    $system = User::where([
                        ['name','system'],
                        ['warehouse',$warehouse_id]
                    ])
                    ->first();

                    $conversion = UomConversion::where([
                        ['item_code',$item_code],
                        ['uom_from',$uom]
                    ])
                    ->first();
                
                    if($conversion)
                    {
                        $multiplyrate   = $conversion->multiplyrate;
                        $dividerate     = $conversion->dividerate;
                        $uom_conversion = $conversion->uom_to;
                    }else
                    {
                        $dividerate     = 1;
                        $multiplyrate   = 1;
                        $uom_conversion = $uom;
                    }
                    
                    $qty_conversion = sprintf("%0.4f",$qty_upload*$dividerate);

                    $_item           = Item::where(db::raw('upper(item_code)'),$item_id)->first();
                    $upc_item        = ($_item)? $_item->upc : 'MASTER ITEM TIDAK DITEMUKAN';

                    $material_stock = MaterialStock::FirstOrCreate([
                        'po_detail_id'              => $po_detail_id,
                        'locator_id'                => $free_stock_destination->id,
                        'mapping_stock_id'          => null,
                        'type_stock_erp_code'       => 2,
                        'type_stock'                => 'REGULER',
                        'document_no'               => 'FREE STOCK',
                        'supplier_code'             => 'FREE STOCK',
                        'supplier_name'             => 'FREE STOCK',
                        'c_order_id'                => 'FREE STOCK',
                        'c_bpartner_id'             => 'FREE STOCK',
                        'item_id'                   => $item_id,
                        'item_code'                 => $item_code,
                        'item_desc'                 => $item_desc,
                        'season'                    => $season,
                        'category'                  => $category,
                        'type_po'                   => 2,
                        'warehouse_id'              => $warehouse_id,
                        'uom'                       => $uom_conversion ,
                        'uom_source'                => $uom ,
                        'qty_carton'                => intval($qty_carton),
                        'qty_arrival'               => $qty_conversion,
                        'stock'                     => $qty_conversion,
                        'reserved_qty'              => 0,
                        'available_qty'             => $qty_conversion,
                        'is_general_item'           => true,
                        'is_material_others'        => false,
                        'is_active'                 => true,
                        'remark'                    => null,
                        'is_running_stock'          => false,
                        'source'                    => 'NON RESERVED STOCK',
                        'upc_item'                  => $upc_item,
                        'created_at'                => carbon::now(),
                        'updated_at'                => carbon::now(),
                        'approval_date'             => carbon::now(),
                        'approval_user_id'          => $system->id,
                        'user_id'                   => Auth::user()->id
                    ]);

                    DetailMaterialStock::FirstOrCreate([
                        'material_stock_id'         => $material_stock->id,
                        'material_arrival_id'       => $material_arrival->id,
                        'remark'                    => null,
                        'uom'                       => $uom_conversion,
                        'qty'                       => $qty_conversion,
                        'user_id'                   => Auth::user()->id,
                        'mm_user_id'                => $system->id,
                        'accounting_user_id'        => $system->id,
                        'created_at'                => carbon::now(),
                        'updated_at'                => carbon::now(),
                        'approve_date_accounting'   => carbon::now(),
                        'approve_date_mm'           => carbon::now(),
                        'locator_id'                => $free_stock_destination->id,
                    ]);

                    Temporary::create([
                        'barcode'       => $po_detail_id,
                        'status'        => 'auto_allocation_carton',
                        'user_id'       => auth::user()->id,
                        'created_at'    => carbon::now(),
                        'updated_at'    => carbon::now(),
                    ]);


                    $app_env = Config::get('app.env');
                    if($app_env == 'live')
                    {
                        $po_detail = PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->first();
                        if(!$po_detail->user_warehouse_receive) PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true,'user_warehouse_receive' => Auth::user()->name]);
                        else PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true]);

                    }else if($app_env == 'dev')
                    {
                        $po_detail = PurchaseOrderDetailDev::where('po_detail_id',$po_detail_id)->first();

                        if(!$po_detail->user_warehouse_receive) PurchaseOrderDetailDev::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true,'user_warehouse_receive' => Auth::user()->name]);
                        else PurchaseOrderDetailDev::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true]);

                    }

                }
                
                DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
         }

        if($is_carton == true)
        {
            $list_packing_list = $po_details->pluck('no_packinglist')->toArray();
            $this->storeItemPackingList($list_packing_list);
        }
            
        }

        if(substr($po_detail->item,0,3) == 'MAS' && ($po_detail->category == 'HT' || $po_detail->category == 'HE' || $po_detail->category == 'ST' || $po_detail->category == 'LB'))
            {
                $po_detail_id    = $po_detail->po_detail_id;
                $item_id         = $po_detail->m_product_id;
                $item_code       = $po_detail->item;
                $item_desc       = $po_detail->desc_product;
                $c_order_id      = $po_detail->c_order_id;
                $c_orderline_id  = $po_detail->c_orderline_id;
                $c_bpartner_id   = $po_detail->c_bpartner_id;
                $document_no     = $po_detail->documentno;
                $supplier        = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                
                if($c_bpartner_id == 'FREE STOCK')
                {
                    $supplier_code = 'FREE STOCK';
                    $supplier_name = 'FREE STOCK';
                }else
                {
                    $supplier_code   = ($supplier)? strtoupper($supplier->supplier_code) : 'MASTER SUPPLIER NOT FOUND';
                    $supplier_name   = ($supplier)? strtoupper($supplier->supplier_name) : $value->supplier_name;
                }
                $no_packing_list = $po_detail->no_packinglist;
                $qty_entered     = $po_detail->qtyentered;
                $qty_ordered     = $po_detail->qtyordered;
                $qty_carton      = $po_detail->qty_carton;
                $qty_upload      = $po_detail->qty_upload;
                $category        = $po_detail->category;

                $uom             = $po_detail->uomsymbol;
                $supplier_name   = $po_detail->supplier;
                $type_po         = $po_detail->type_po;
                $warehouse_id    = $po_detail->m_warehouse_id;

                $no_resi         = $po_detail->kst_resi;
                $no_surat_jalan  = $po_detail->kst_suratjalanvendor;
                $no_invoice      = $po_detail->kst_invoicevendor;
                $season          = $po_detail->kst_season;

                $is_carton       = true;

                $is_exist = MaterialArrival::where('po_detail_id', $po_detail_id)->exists();

                if(!$is_exist)
                {
                    try 
                    {
                        DB::beginTransaction();

                        //insert ke material arrivals

                        $is_exist = MaterialArrival::where('po_detail_id', $po_detail_id)->exists();

                        if(!$is_exist)
                        {
                        $material_arrival = MaterialArrival::FirstOrCreate([
                            'po_detail_id'          => $po_detail_id,
                            'material_subcont_id'   => null,
                            'item_id'               => $item_id,
                            'item_code'             => $item_code,
                            'item_desc'             => $item_desc,
                            'supplier_name'         => $supplier_name,
                            'document_no'           => $document_no,
                            'type_po'               => $type_po,
                            'c_order_id'            => $c_order_id,
                            'c_orderline_id'        => $c_orderline_id,
                            'c_bpartner_id'         => $c_bpartner_id,
                            'warehouse_id'          => $warehouse_id,
                            //'po_buyer'              => $po_buyer,
                            'category'              => $category,
                            'uom'                   => $uom,
                            'no_packing_list'       => $no_packing_list,
                            'no_resi'               => $no_resi,
                            'no_surat_jalan'        => $no_surat_jalan,
                            'no_invoice'            => $no_invoice,
                            'qty_entered'           => $qty_entered,
                            'qty_ordered'           => $qty_ordered,
                            'qty_carton'            => $qty_carton,
                            'qty_upload'            => $qty_upload,
                            'qty_available_rma'     => $qty_upload,
                            'qty_reserved_rma'      => 0,
                            'foc_reserved_rma'      => 0,
                            'is_subcont'            => false,
                            'prepared_status'       => 'NEED PREPARED ALLOCATION',
                            'is_active'             => true,
                            //'is_moq'                => $is_moq,
                            'user_id'               => Auth::user()->id
                        ]);

                        

                    //insert ke material stocks
                    $free_stock_destination = Locator::with('area')
                    ->whereHas('area',function ($query) use($warehouse_id) 
                    {
                        $query->where([
                            ['warehouse',$warehouse_id],
                            ['name','FREE STOCK'],
                            ['is_destination',true]
                        ]);

                    })
                    ->first();

                    $system = User::where([
                        ['name','system'],
                        ['warehouse',$warehouse_id]
                    ])
                    ->first();

                    $conversion = UomConversion::where([
                        ['item_code',$item_code],
                        ['uom_from',$uom]
                    ])
                    ->first();
                
                    if($conversion)
                    {
                        $multiplyrate   = $conversion->multiplyrate;
                        $dividerate     = $conversion->dividerate;
                        $uom_conversion = $conversion->uom_to;
                    }else
                    {
                        $dividerate     = 1;
                        $multiplyrate   = 1;
                        $uom_conversion = $uom;
                    }
                    
                    $qty_conversion = sprintf("%0.4f",$qty_upload*$dividerate);

                    $_item           = Item::where(db::raw('upper(item_code)'),$item_id)->first();
                    $upc_item        = ($_item)? $_item->upc : 'MASTER ITEM TIDAK DITEMUKAN';

                    $material_stock = MaterialStock::FirstOrCreate([
                        'po_detail_id'              => $po_detail_id,
                        'locator_id'                => $free_stock_destination->id,
                        'mapping_stock_id'          => null,
                        'type_stock_erp_code'       => 2,
                        'type_stock'                => 'REGULER',
                        'document_no'               => $document_no,
                        'supplier_code'             => $supplier_code,
                        'supplier_name'             => $supplier_name,
                        'c_order_id'                => $c_order_id,
                        'c_bpartner_id'             => $c_bpartner_id,
                        'item_id'                   => $item_id,
                        'item_code'                 => $item_code,
                        'item_desc'                 => $item_desc,
                        'season'                    => $season,
                        'category'                  => $category,
                        'type_po'                   => 2,
                        'warehouse_id'              => $warehouse_id,
                        'uom'                       => $uom_conversion ,
                        'uom_source'                => $uom ,
                        'qty_carton'                => intval($qty_carton),
                        'qty_arrival'               => $qty_conversion,
                        'stock'                     => $qty_conversion,
                        'reserved_qty'              => 0,
                        'available_qty'             => $qty_conversion,
                        'is_general_item'           => true,
                        'is_material_others'        => false,
                        'is_active'                 => true,
                        'remark'                    => null,
                        'is_running_stock'          => false,
                        'source'                    => 'NON RESERVED STOCK',
                        'upc_item'                  => $upc_item,
                        'created_at'                => carbon::now(),
                        'updated_at'                => carbon::now(),
                        'approval_date'             => carbon::now(),
                        'approval_user_id'          => $system->id,
                        'user_id'                   => Auth::user()->id
                    ]);

                    DetailMaterialStock::FirstOrCreate([
                        'material_stock_id'         => $material_stock->id,
                        'material_arrival_id'       => $material_arrival->id,
                        'remark'                    => null,
                        'uom'                       => $uom_conversion,
                        'qty'                       => $qty_conversion,
                        'user_id'                   => Auth::user()->id,
                        'mm_user_id'                => $system->id,
                        'accounting_user_id'        => $system->id,
                        'created_at'                => carbon::now(),
                        'updated_at'                => carbon::now(),
                        'approve_date_accounting'   => carbon::now(),
                        'approve_date_mm'           => carbon::now(),
                        'locator_id'                => $free_stock_destination->id,
                    ]);

                    Temporary::create([
                        'barcode'       => $po_detail_id,
                        'status'        => 'auto_allocation_carton',
                        'user_id'       => auth::user()->id,
                        'created_at'    => carbon::now(),
                        'updated_at'    => carbon::now(),
                    ]);


                    $app_env = Config::get('app.env');
                    if($app_env == 'live')
                    {
                        $po_detail = PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->first();
                        if(!$po_detail->user_warehouse_receive) PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true,'user_warehouse_receive' => Auth::user()->name]);
                        else PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true]);

                    }else if($app_env == 'dev')
                    {
                        $po_detail = PurchaseOrderDetailDev::where('po_detail_id',$po_detail_id)->first();

                        if(!$po_detail->user_warehouse_receive) PurchaseOrderDetailDev::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true,'user_warehouse_receive' => Auth::user()->name]);
                        else PurchaseOrderDetailDev::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true]);

                    }
                    
                }

                DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
        }
    }

    if($is_carton == true)
        {
            $list_packing_list = $po_details->pluck('no_packinglist')->toArray();
            $this->storeItemPackingList($list_packing_list);
        }
        

        }
        //return substr($po_detail->item,0,3);
        return view('accessories_barcode_supplier.barcode',compact('data_web_po','document_no'));

    }

    public function insertMaterialStock($material_arrival_ids, $warehouse_id)
    {
                $stock_accessories                              = array();
                $stock_reserved_accessories                     = array();
                $stock_reserved_handover_accessories            = array();

                
                $material_arrivals = MaterialArrival::select('id','po_detail_id','season','warehouse_id','item_id','material_subcont_id','po_buyer','supplier_name','document_no','c_bpartner_id','item_code','c_order_id','uom','qty_upload','qty_carton')
                ->whereIn('id',$material_arrival_ids)
                ->where('warehouse_id',$_warehouse_id)
                ->groupby('id','warehouse_id','document_no','po_detail_id','season','c_bpartner_id','item_id','item_code','material_subcont_id','po_buyer','supplier_name','c_order_id','uom','qty_upload','qty_carton')
                ->get();
                
                $free_stock_destination = Locator::with('area')
                ->whereHas('area',function ($query) use($_warehouse_id) 
                {
                    $query->where([
                        ['warehouse',$_warehouse_id],
                        ['name','FREE STOCK'],
                        ['is_destination',true]
                    ]);

                })
                ->first();

            $upload_date                = Carbon::now()->toDateTimeString();

            foreach ($material_arrivals as $key => $value) 
            {
                $po_detail_id           = $value->po_detail_id; 
                $po_buyer               = $value->po_buyer; 
                $material_subcont_id    = $value->material_subcont_id; 
                $material_arrival_id    = $value->id; 
                $qty_upload             = $value->qty_upload; 
                $qty_carton             = $value->qty_carton; 
                $warehouse_id           = $value->warehouse_id; 
                $season                 = $value->season; 
                $document_no            = $value->document_no; 
                $c_bpartner_id          = $value->c_bpartner_id; 
                $item_code              = strtoupper($value->item_code); 
                $c_order_id             = $value->c_order_id; 
                $uom                    = $value->uom;
                $has_many_po_buyer      = strpos($po_buyer, ",");

                $is_running_stock       = false;
                $is_integrate           = false;

                if($material_subcont_id)
                {
                    $material_subcont       = MaterialSubcont::find($material_subcont_id);
                    $source                 = 'RESERVED HANDOVER STOCK';
                    $remark_detail_stock    = 'KEDATANGAN DARI PINDAH TANGAN';
                    $is_running_stock       = true;
                    $is_integrate           = true;
                    $po_detail_id           = ($material_subcont->po_detail_id ? $material_subcont->po_detail_id : $material_subcont->barcode ); 
                }else
                {
                    if($po_buyer != '' || $po_buyer != null)
                    {
                        $source                 = 'RESERVED STOCK';
                        $is_running_stock       = true;
                        $is_integrate           = true;
                    }else
                    {
                        $source                 = 'NON RESERVED STOCK';
                    }
                    $remark_detail_stock    = 'KEDATANGAN DARI SUPPLIER';
                }

                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();

                $is_pfao         = substr($document_no,0, 4); 
                $_item           = Item::where(db::raw('upper(item_code)'),$item_code)->first();
                $item_id         = ($_item)? $_item->item_id : $value->item_id;
                $upc_item        = ($_item)? $_item->upc : 'MASTER ITEM TIDAK DITEMUKAN';
                $item_desc       = ($_item)? $_item->item_desc : 'MASTER ITEM TIDAK DITEMUKAN';
                $category        = ($_item)? $_item->category : 'MASTER ITEM TIDAK DITEMUKAN';
                $supplier        = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                
                if($c_bpartner_id == 'FREE STOCK')
                {
                    $supplier_code = 'FREE STOCK';
                    $supplier_name = 'FREE STOCK';
                }else
                {
                    $supplier_code   = ($supplier)? strtoupper($supplier->supplier_code) : 'MASTER SUPPLIER NOT FOUND';
                    $supplier_name   = ($supplier)? strtoupper($supplier->supplier_name) : $value->supplier_name;
                }
               
                if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
                {
                    $mapping_stock_id       = null;
                    $type_stock_erp_code    = '2';
                    $type_stock             = 'REGULER';
                }else
                {
                    $get_4_digit_from_beginning = substr($document_no,0, 4);
                    $_mapping_stock_4_digt = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                    if($_mapping_stock_4_digt)
                    {
                        $mapping_stock_id       = $_mapping_stock_4_digt->id;
                        $type_stock_erp_code    = $_mapping_stock_4_digt->type_stock_erp_code;
                        $type_stock             = $_mapping_stock_4_digt->type_stock;
                    }else
                    {
                        $get_5_digit_from_beginning = substr($document_no,0, 5);
                        $_mapping_stock_5_digt = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                        if($_mapping_stock_5_digt)
                        {
                            $mapping_stock_id       = $_mapping_stock_5_digt->id;
                            $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                            $type_stock             = $_mapping_stock_5_digt->type_stock;
                        }else
                        {
                            $mapping_stock_id       = null;
                            $type_stock_erp_code    = null;
                            $type_stock             = null;
                        }
                        
                    }
                }

                $conversion = UomConversion::where([
                    ['item_code',$item_code],
                    ['uom_from',$uom]
                ])
                ->first();
            
                if($conversion)
                {
                    $multiplyrate   = $conversion->multiplyrate;
                    $dividerate     = $conversion->dividerate;
                    $uom_conversion = $conversion->uom_to;
                }else
                {
                    $dividerate     = 1;
                    $multiplyrate   = 1;
                    $uom_conversion = $uom;
                }
                
                $qty_conversion = sprintf("%0.4f",$qty_upload*$dividerate);

                if(!$is_running_stock)
                {
                    $is_stock_already_created = MaterialStock::where([
                        ['document_no',$document_no],
                        ['item_code',$item_code],
                        ['warehouse_id',$warehouse_id],
                        ['is_stock_on_the_fly',true],
                    ])
                    ->whereNull('po_buyer')
                    ->exists();
                }else
                {
                    $is_stock_already_created = false;
                }
                
                $is_exists = MaterialStock::where([
                    ['po_detail_id',$po_detail_id],
                    ['document_no',$document_no],
                    ['c_bpartner_id',$c_bpartner_id],
                    ['item_code',$item_code],
                    ['type_po',2],
                    ['warehouse_id',$warehouse_id],
                    ['c_order_id',$c_order_id],
                    ['type_stock',$type_stock],
                    ['uom',$uom_conversion],
                    ['is_material_others',false],
                    ['is_stock_on_the_fly',false],
                ]);

                if(!$is_running_stock) $is_exists = $is_exists->where('locator_id',$free_stock_destination->id);
                else $is_exists= $is_exists->whereNull('locator_id');

                if($material_subcont_id) $is_exists = $is_exists->where('remark',$material_subcont_id);
                
                $is_exists = $is_exists->whereNotNull('approval_date')
                ->whereNull('po_buyer')
                ->first();

                if(!$is_exists)
                {
                    $material_stock = MaterialStock::FirstOrCreate([
                        'po_detail_id'              => $po_detail_id,
                        'locator_id'                => (!$is_running_stock ? $free_stock_destination->id : null),
                        'mapping_stock_id'          => $mapping_stock_id,
                        'type_stock_erp_code'       => $type_stock_erp_code,
                        'type_stock'                => $type_stock,
                        'document_no'               => $document_no,
                        'supplier_code'             => $supplier_code,
                        'supplier_name'             => $supplier_name,
                        'c_order_id'                => $c_order_id,
                        'c_bpartner_id'             => $c_bpartner_id,
                        'item_id'                   => $item_id,
                        'item_code'                 => $item_code,
                        'item_desc'                 => $item_desc,
                        'season'                    => $season,
                        'category'                  => $category,
                        'type_po'                   => 2,
                        'warehouse_id'              => $warehouse_id,
                        'uom'                       => $uom_conversion ,
                        'uom_source'                => $uom ,
                        'qty_carton'                => intval($qty_carton),
                        'qty_arrival'               => $qty_conversion,
                        'stock'                     => $qty_conversion,
                        'reserved_qty'              => 0,
                        'available_qty'             => $qty_conversion,
                        'is_general_item'           => true,
                        'is_material_others'        => false,
                        'is_active'                 => true,
                        'remark'                    => ($material_subcont_id  ? $material_subcont_id : null),
                        'is_running_stock'          => $is_running_stock,
                        'source'                    => $source,
                        'upc_item'                  => $upc_item,
                        'created_at'                => $upload_date,
                        'updated_at'                => $upload_date,
                        'approval_date'             => ($is_pfao == 'PFAO')? carbon::now() : NULL,
                        'approval_user_id'          => ($is_pfao == 'PFAO')? $system->id : NULL,
                        'user_id'                   => Auth::user()->id
                    ]);

                    $material_stock_id  = $material_stock->id;
                    $locator_id         = $material_stock->locator_id;

                    if($is_running_stock)
                    {
                        $is_integrate = true;
                    }else
                    {
                        if($is_stock_already_created) $is_integrate = true;
                        else $is_integrate = false;
                    }
                    
                
                    HistoryStock::approved($material_stock_id
                    ,$qty_conversion
                    ,'0'
                    ,null
                    ,null
                    ,null
                    ,null
                    ,null
                    ,$remark_detail_stock
                    ,Auth::user()->name
                    ,Auth::user()->id
                    ,$type_stock_erp_code
                    ,$type_stock
                    ,$is_integrate
                    ,true
                    );

                    
                }else
                {
                    $locator_id         = $is_exists->locator_id;
                    $material_stock_id  = $is_exists->id;
                    $stock              = $is_exists->stock;
                    $reserved_qty       = $is_exists->reserved_qty;
                    $old_available_qty  = $is_exists->available_qty;
                    $new_stock          = $stock + $qty_conversion;
                    $availability_stock = $new_stock - $reserved_qty;

                    $is_exists->stock           = sprintf("%0.4f",$new_stock);
                    $is_exists->available_qty   = sprintf("%0.4f",$availability_stock);

                    if($availability_stock > 0)
                    {
                        $is_exists->is_allocated = false;
                        $is_exists->is_active = true;
                    }

                    if($is_running_stock)
                    {
                        $is_integrate = true;
                    }else
                    {
                        if($is_stock_already_created) $is_integrate = true;
                        else $is_integrate = false;
                    }

                    HistoryStock::approved($material_stock_id
                    ,$new_stock
                    ,$old_available_qty
                    ,null
                    ,null
                    ,null
                    ,null
                    ,null
                    ,null
                    ,Auth::user()->name
                    ,Auth::user()->id
                    ,$type_stock_erp_code
                    ,$type_stock
                    ,$is_integrate
                    ,true
                    );
                    
                    $is_exists->save();
                }

                DetailMaterialStock::FirstOrCreate([
                    'material_stock_id'         => $material_stock_id,
                    'material_arrival_id'       => $material_arrival_id,
                    'remark'                    => $remark_detail_stock,
                    'uom'                       => $uom_conversion,
                    'qty'                       => $qty_conversion,
                    'user_id'                   => Auth::user()->id,
                    'mm_user_id'                => $system->id,
                    'accounting_user_id'        => $system->id,
                    'created_at'                => $upload_date,
                    'updated_at'                => $upload_date,
                    'approve_date_accounting'   => $upload_date,
                    'approve_date_mm'           => $upload_date,
                    'locator_id'                => $locator_id,
                ]);

                if(!$is_running_stock) $stock_accessories [] = $material_stock_id;
                else 
                {
                    if(!$material_subcont_id)
                    {
                        $temporary = Temporary::Create([
                            'barcode'       => $material_stock_id,
                            'status'        => 'reserved_stock',
                            'user_id'       => Auth::user()->id,
                            'created_at'    => $upload_date,
                            'updated_at'    => $upload_date,
                        ]);
                    } 
                    else
                    {
                        $temporary = Temporary::Create([
                            'barcode'       => $material_stock_id,
                            'status'        => 'reserved_handover_stock',
                            'user_id'       => Auth::user()->id,
                            'created_at'    => $upload_date,
                            'updated_at'    => $upload_date,
                        ]);
                    }
                }
            }

            SOTF::updateStockArrival($stock_accessories);
            MasterDataAutoAllocationController::insertAllocationFromMovementAccessories($stock_accessories,$_warehouse_id);
            //AutoAllocation::insertAllocationFromReservedAccessories($stock_reserved_accessories);
            //$this->copyAllocationHandoverccessories($stock_reserved_handover_accessories);

    }

    static function storeItemPackingList($packing_lists)
    {
        $data = DB::connection('web_po') //web_po //dev_web_po
        ->table('adt_item_pl')
        ->select('no_packinglist as no_packing_list',
                'kst_invoicevendor as no_invoice',
                'kst_resi as no_resi',
                'c_bpartner_id',
                'ttl_item as total_item',
                'type_po'
                )
        ->whereIn('no_packinglist',$packing_lists)
        ->get();

        try 
        {
            DB::beginTransaction();
            
            foreach ($data as $key => $value) 
            {
                $no_packing_list    = $value->no_packing_list;
                $no_resi            = $value->no_resi;
                $no_invoice         = $value->no_invoice;
                $c_bpartner_id      = $value->c_bpartner_id;
                
                $is_exists = ItemPackingList::where([
                    ['no_packing_list',$no_packing_list],
                    ['no_resi',$no_resi],
                    ['no_invoice',$no_invoice],
                    ['c_bpartner_id',$c_bpartner_id],
                    ['type_po',1]
                ])
                ->first();
                
                $count_packinglist = MaterialArrival::where([
                    ['no_packing_list',$no_packing_list],
                    ['no_resi',$no_resi],
                    ['no_invoice',$no_invoice],
                    ['c_bpartner_id',$c_bpartner_id],
                    ['type_po',1]
                ])
                ->count();

                if($is_exists)
                {
                    $is_exists->total_item          = $value->total_item;
                    $is_exists->total_item_receive  = $count_packinglist;
                    $is_exists->save();
                }else
                {
                    ItemPackingList::FirstOrCreate([
                        'no_packing_list'       => $value->no_packing_list,
                        'no_resi'               => $value->no_resi,
                        'no_invoice'            => $value->no_invoice,
                        'c_bpartner_id'         => $value->c_bpartner_id,
                        'total_item'            => $value->total_item,
                        'total_item_receive'    => $count_packinglist,
                        'type_po' => 1
                    ]);
                }
            }

            DB::commit();
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    }

    static function autoAllocationCarton($po_detail_id)
    {
        $material_stock = MaterialStock::where('po_detail_id', $po_detail_id)->first();
        if($material_stock)
        {
        $material_stock_id      = $material_stock->id;
        $c_order_id             = $material_stock->c_order_id;
        $type_stock             = $material_stock->type_stock;
        $type_stock_erp_code    = $material_stock->type_stock_erp_code;
        $c_bpartner_id          = $material_stock->c_bpartner_id;
        $supplier_name          = $material_stock->supplier_name;
        $document_no            = $material_stock->document_no;
        $item_id                = $material_stock->item_id;
        $item_code_source       = $material_stock->item_code;
        $item_code_book         = $material_stock->item_code;
        $item_desc              = $material_stock->item_desc;
        $category               = $material_stock->category;
        $uom                    = $material_stock->uom;
        $warehouse_name         = $material_stock->warehouse_id;
        $warehouse_id           = $material_stock->warehouse_id;
        $available_qty          = $material_stock->available_qty;

        //cek kebutuhan berdasarkan item untuk semua po buyer
        //ganti pakai view aja 
        $material_requirements = db::table('material_requirement_cartons_v')->where('item_id', $item_id)
        ->orderBy('psd', 'asc')
        ->orderBy('promise_date', 'asc')
        ->get();

        $movement_date = carbon::now();
        try 
        {
        DB::beginTransaction();
        //cek kebutuhan alokasinya 
        foreach($material_requirements as $key2 => $material_requirement)
        {
            $booking_number = $material_requirement->batch_code;
            $po_buyer       = $material_requirement->po_buyer;
            $article_no     = $material_requirement->article_no;
            $qty_required   = $material_requirement->qty_required;
            $promise_date   = $material_requirement->promise_date;
            $lc_date        = $material_requirement->lc_date;
            $season         = $material_requirement->season;
            $qty_in_house   = $material_requirement->qty_allocation;
            $qty_need       = $material_requirement->qty_outstanding;
            //dd($po_buyer, $item_id, $qty_need);

            $so_id          = PoBuyer::where('po_buyer', $po_buyer)->first();
            if($qty_need > 0 && $available_qty > 0)
            {
                if($available_qty > $qty_need)
                {
                    $qty_allocation = $qty_need;
                    $available_qty -= $qty_need;
                }
                else
                {
                    $qty_allocation = $available_qty;
                    $available_qty -= $available_qty;
                }
            }
            else
            {
                $qty_allocation = 0;
                $available_qty  = 0;
            }
            //dd($qty_allocation, $available_qty);
            if($qty_allocation > 0)
            {
                //jika kebutuhan ada buat alokasinya
                $auto_allocation = AutoAllocation::FirstOrCreate([
                    'document_allocation_number'        => $booking_number,
                    'lc_date'                           => $lc_date,
                    'season'                            => $season,
                    'document_no'                       => $document_no,
                    'c_order_id'                        => $c_order_id,
                    'c_bpartner_id'                     => $c_bpartner_id,
                    'supplier_name'                     => $supplier_name,
                    'po_buyer'                          => $po_buyer,
                    'old_po_buyer'                      => $po_buyer,
                    'item_code_source'                  => $item_code_source,
                    'item_id_book'                      => $item_id,
                    'item_id_source'                    => $item_id,
                    'item_code'                         => $item_code_book,
                    'item_code_book'                    => $item_code_book,
                    'article_no'                        => $article_no,
                    'item_desc'                         => $item_desc,
                    'category'                          => $category,
                    'uom'                               => $uom,
                    'warehouse_name'                    => $warehouse_name,
                    'warehouse_id'                      => $warehouse_id,
                    'qty_allocation'                    => $qty_allocation,
                    'qty_outstanding'                   => $qty_allocation,
                    'qty_allocated'                     => 0,
                    'is_fabric'                         => false,
                    'remark'                            => 'UPLOAD AUTO ALLOCATION CARTON',
                    'remark_update'                     => null,
                    'is_already_generate_form_booking'  => false,
                    'is_upload_manual'                  => true,
                    'type_stock'                        => $type_stock,
                    'type_stock_erp_code'               => $type_stock_erp_code,
                    'promise_date'                      => $promise_date,
                    'deleted_at'                        => null,
                    'status_po_buyer'                   => 'active',
                    'created_at'                        => $movement_date,
                    'updated_at'                        => $movement_date,
                    'user_id'                           => '91',
                    'so_id'                             => $so_id->so_id
                ]);

                $note = 'TERJADI PENAMBAHAN ALOKASI';
                $note_code = '1';

                HistoryAutoAllocations::FirstOrCreate([
                    'auto_allocation_id'        => $auto_allocation->id,
                    'c_bpartner_id_new'         => $c_bpartner_id,
                    'supplier_name_new'         => $supplier_name,
                    'document_no_new'           => $document_no,
                    'item_id_new'               => $item_id,
                    'item_code_new'             => $item_code_book,
                    'item_source_id_new'        => $item_id,
                    'item_code_source_new'      => $item_code_source,
                    'po_buyer_new'              => $po_buyer,
                    'uom_new'                   => $uom,
                    'qty_allocated_new'         => $qty_allocation,
                    'warehouse_id_new'          => $warehouse_id,
                    'type_stock_new'            => $type_stock,
                    'type_stock_erp_code_new'   => $type_stock_erp_code,
                    'note_code'                 => $note_code,
                    'note'                      => $note,
                    'operator'                  => $qty_allocation,
                    'is_integrate'              => true,
                    'user_id'                   => '91'//auth::user()->id
                ]);
                
            }

        }

        DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        ReportStock::doAllocation($material_stock, '91', $movement_date);

    }

    }


    static function autoAllocationFreeStockCarton()
    {
        $items = db::table('material_requirement_cartons_v')
        ->select('item_id', 'warehouse_id')
        ->groupBy('item_id', 'warehouse_id')
        ->orderBy('item_id', 'asc')
        ->get();
        //dd($items);

        foreach($items as $key => $item)
        {

            $item_id = $item->item_id;
            $warehouse_id = $item->warehouse_id;

            $auto_allocations = db::table('allocated_carton_v')
            ->where('item_id_source',$item_id)
            ->where('warehouse_id',$warehouse_id)
            ->get();

            if(count($auto_allocations) > 0)
            {
                foreach($auto_allocations as $key4 => $auto_allocation)
                {
                    $auto_allocation_id = $auto_allocation->id;
                    //dd($auto_allocation_id);
                    AllocationFabric::doCancellation($auto_allocation_id,'auto allocation carton', '91');
                }
            }

            $material_stocks = MaterialStock::select('id',
            'item_id',
            'item_code',
            'c_order_id',
            'warehouse_id',
            'type_stock',
            'type_stock_erp_code',
            'c_bpartner_id',
            'supplier_name',
            'document_no',
            'item_desc',
            'category',
            'uom',
            'available_qty')
            ->where('item_id',$item_id)
            ->where('warehouse_id',$warehouse_id)
            ->where('deleted_at',null)
            ->where('available_qty','>','0')
            ->where('is_running_stock', false)
            ->get();

            //dd($material_stocks);

            foreach($material_stocks as $key2 => $material_stock)
            {
                $material_stock_id      = $material_stock->id;
                $c_order_id             = $material_stock->c_order_id;
                $type_stock             = $material_stock->type_stock;
                $type_stock_erp_code    = $material_stock->type_stock_erp_code;
                $c_bpartner_id          = $material_stock->c_bpartner_id;
                $supplier_name          = $material_stock->supplier_name;
                $document_no            = $material_stock->document_no;
                $item_id                = $material_stock->item_id;
                $item_code_source       = $material_stock->item_code;
                $item_code_book         = $material_stock->item_code;
                $item_desc              = $material_stock->item_desc;
                $category               = $material_stock->category;
                $uom                    = $material_stock->uom;
                $warehouse_name         = $material_stock->warehouse_id;
                $warehouse_id           = $material_stock->warehouse_id;
                $available_qty          = $material_stock->available_qty;

                $material_requirements = db::table('material_requirement_cartons_v')->where('item_id', $item_id)
                ->where('qty_outstanding', '>', 0)
                ->orderBy('psd', 'asc')
                ->orderBy('promise_date', 'asc')
                ->orderBy('qty_required', 'asc')
                ->get();

                //dd($material_stock->item_id, $material_stock->id);

                $movement_date = carbon::now();

                try 
                {
                DB::beginTransaction();
                //cek kebutuhan alokasinya 
                foreach($material_requirements as $key2 => $material_requirement)
                {
                    $booking_number = $material_requirement->batch_code;
                    $po_buyer       = $material_requirement->po_buyer;
                    $article_no     = $material_requirement->article_no;
                    $qty_required   = $material_requirement->qty_required;
                    $promise_date   = $material_requirement->promise_date;
                    $lc_date        = $material_requirement->lc_date;
                    $season         = $material_requirement->season;
                    $qty_in_house   = $material_requirement->qty_allocation;
                    $qty_need       = $material_requirement->qty_outstanding;
                    //dd($po_buyer, $item_id, $qty_need);

                    $so_id          = PoBuyer::where('po_buyer', $po_buyer)->first();
                    if($qty_need > 0 && $available_qty > 0)
                    {
                        if($available_qty > $qty_need)
                        {
                            $qty_allocation = $qty_need;
                            $available_qty -= $qty_need;
                        }
                        else
                        {
                            $qty_allocation = $available_qty;
                            $available_qty -= $available_qty;
                        }
                    }
                    else
                    {
                        $qty_allocation = 0;
                        //$available_qty  = 0;
                    }
                    //dd($qty_allocation, $available_qty);
                    if($qty_allocation > 0)
                    {
                        //jika kebutuhan ada buat alokasinya
                        $auto_allocation = AutoAllocation::FirstOrCreate([
                            'document_allocation_number'        => $booking_number,
                            'lc_date'                           => $lc_date,
                            'season'                            => $season,
                            'document_no'                       => $document_no,
                            'c_order_id'                        => $c_order_id,
                            'c_bpartner_id'                     => $c_bpartner_id,
                            'supplier_name'                     => $supplier_name,
                            'po_buyer'                          => $po_buyer,
                            'old_po_buyer'                      => $po_buyer,
                            'item_code_source'                  => $item_code_source,
                            'item_id_book'                      => $item_id,
                            'item_id_source'                    => $item_id,
                            'item_code'                         => $item_code_book,
                            'item_code_book'                    => $item_code_book,
                            'article_no'                        => $article_no,
                            'item_desc'                         => $item_desc,
                            'category'                          => $category,
                            'uom'                               => $uom,
                            'warehouse_name'                    => $warehouse_name,
                            'warehouse_id'                      => $warehouse_id,
                            'qty_allocation'                    => $qty_allocation,
                            'qty_outstanding'                   => $qty_allocation,
                            'qty_allocated'                     => 0,
                            'is_fabric'                         => false,
                            'remark'                            => 'UPLOAD AUTO ALLOCATION CARTON',
                            'remark_update'                     => null,
                            'is_already_generate_form_booking'  => false,
                            'is_upload_manual'                  => true,
                            'type_stock'                        => $type_stock,
                            'type_stock_erp_code'               => $type_stock_erp_code,
                            'promise_date'                      => $promise_date,
                            'deleted_at'                        => null,
                            'status_po_buyer'                   => 'active',
                            'created_at'                        => $movement_date,
                            'updated_at'                        => $movement_date,
                            'user_id'                           => '91',//auth::user()->id,
                            'so_id'                             => $so_id->so_id
                        ]);

                        $note = 'TERJADI PENAMBAHAN ALOKASI';
                        $note_code = '1';

                        HistoryAutoAllocations::FirstOrCreate([
                            'auto_allocation_id'        => $auto_allocation->id,
                            'c_bpartner_id_new'         => $c_bpartner_id,
                            'supplier_name_new'         => $supplier_name,
                            'document_no_new'           => $document_no,
                            'item_id_new'               => $item_id,
                            'item_code_new'             => $item_code_book,
                            'item_source_id_new'        => $item_id,
                            'item_code_source_new'      => $item_code_source,
                            'po_buyer_new'              => $po_buyer,
                            'uom_new'                   => $uom,
                            'qty_allocated_new'         => $qty_allocation,
                            'warehouse_id_new'          => $warehouse_id,
                            'type_stock_new'            => $type_stock,
                            'type_stock_erp_code_new'   => $type_stock_erp_code,
                            'note_code'                 => $note_code,
                            'note'                      => $note,
                            'operator'                  => $qty_allocation,
                            'is_integrate'              => true,
                            'user_id'                   => '91'//auth::user()->id
                        ]);
                        
                    }

                }

                DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                ReportStock::doAllocation($material_stock, '91', $movement_date);

            }


        }
    }


}
