<?php namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

use App\Models\Area;
use App\Models\Locator;

class FabricBarcodeLocatorController extends Controller
{
    public function index()
    {
        $areas = Area::where([
            ['is_active',true],
            ['warehouse',auth::user()->warehouse],
            ['name','INVENTORY'],
            ['type_po','1'],
        ])
        ->get();
        
        return view('fabric_barcode_locator.index',compact('areas'));
    }

    public function barcode(request $request)
    {
        $area       = $request->area;
        $rack       = $request->rack;
        $locators   = Locator::select('barcode','rack','y_row','z_column','area_id')
        ->orderBy('rack','asc')
        ->orderBy('y_row','asc')
        ->orderBy('z_column','asc');

        if($area != 'null')  $locators = $locators->where('area_id',$area);
        if($rack != 'null') $locators = $locators->where('rack',$rack);

        $locators = $locators->where('is_active',true)
        ->get();

       return view('fabric_barcode_locator.barcode', compact('locators','rack'));
    }
}
