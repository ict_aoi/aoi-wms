<?php namespace App\Http\Controllers;

use DB;
use PDF;
use Auth;
use Excel;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Models\Area;
use App\Models\Locator;
use App\Models\MaterialStock;
use App\Models\MaterialPreparation;

class AccessoriesLocatorController extends Controller
{
    public function index(Request $request)
    {
        $warehouse_id   = ( $request->warehouse ? $request->warehouse : Auth::user()->warehouse );

        $areas = Area::where([
            ['is_active',true],
            ['warehouse',$warehouse_id],
            ['is_destination',false],
            ['type_po','2'],
        ])
        ->where(function($query)
        {
            $query->where('name','MISSING ITEM')
            ->orWhere('name','PREPARATION')
            ->orWhere('name','STICKER')
            ->orWhere('name','RECEIVING')
            ->orWhere('name','QC')
            ->orWhere('name','PRINT BARCODE')
            ->orWhere('name','FREE STOCK')
            ->orWhere('name','ATACA');
        })
        ->whereIn('id',function($query){
            $query->select('area_id')
            ->from('locators')
            ->groupby('area_id');
        })
        ->orderBy('name','asc')
        ->get();
 
        $filtering_po_buyer     = $request->po_buyer_locator;
        $filter_item_code       = $request->item_code;
        $filtering_locator      = null;

        if($filtering_po_buyer || $filter_item_code)
        {
            $filtering_movement = MaterialPreparation::select('last_locator_id')
            ->where('warehouse',$warehouse_id)
            ->where(function ($query){
                $query->where('last_status_movement','in')
                ->orWhere('last_status_movement','change');
            });

            if($filtering_po_buyer != '' && $filtering_po_buyer !=null) $filtering_movement = $filtering_movement->where('po_buyer','LIKE',"%$filtering_po_buyer%");
            if($filter_item_code !='' && $filter_item_code !=null) $filtering_movement = $filtering_movement->where('item_code','LIKE',"%$filter_item_code%");
               
            $filtering_movement = $filtering_movement->groupby('last_locator_id');

            $filtering_stock = MaterialStock::select('locator_id')
            ->where([
                ['warehouse_id',$warehouse_id],
                ['type_po',2]
            ]);

            if($filtering_po_buyer != ''  && $filtering_po_buyer !=null) $filtering_stock = $filtering_stock->where('po_buyer','LIKE',"%$filtering_po_buyer%");
            if($filter_item_code !='' && $filter_item_code !=null) $filtering_stock = $filtering_stock->where('item_code','LIKE',"%$filter_item_code%");


            $filtering_stock = $filtering_stock->groupby('locator_id');

            $filtering_locator = $filtering_movement->union($filtering_stock)
            ->get()
            ->toArray();
            
        }
        
        return view('accessories_locator.index',compact('area','areas','filtering_locator'));  
        
    }

    public function detilRack(Request $request)
    {
        $obj = new StdClass();
        $po_buyer = trim($request->po_buyer);
        $search = trim($request->q);
        $locator_id = trim($request->locator_id);
        $area = trim($request->area);

        if($area == 'FREE STOCK'){
            $lists = MaterialStock::where('locator_id',$locator_id)
            ->where(function($query) use ($search){
                $query->where('item_code','LIKE',"%$search%")
                ->Orwhere('po_buyer','LIKE',"%$search%")
                ->Orwhere('document_no','LIKE',"%$search%")
                ->Orwhere('item_desc','LIKE',"%$search%");
            })
            ->whereNull('deleted_at')
            ->where('available_qty','>','0')
            ->paginate('10');

            return view('locator._stock_in_rack_list')
            ->with('lists', $lists);

        }else
        {
            $lists = MaterialPreparation::where('last_locator_id',$locator_id);
            if($po_buyer <> 'HAS MANY PO BUYER') $lists = $lists->where('po_buyer',$po_buyer);
            $lists = $lists->paginate('10');
            
            return view('accessories_locator._item_in_rack_list',compact('lists'));
        }
       
        
    }

    public function dataRackFreeStock(Request $request)
    {
        if(request()->ajax()) 
        {
            $locator_id = $request->locator_id;
            
            $summary = MaterialStock::where([
                ['locator_id',$locator_id],
                ['available_qty','>','0'],
                ['is_allocated',false],
            ])
            ->whereNull('deleted_at');
            
            return DataTables::of($summary)
            ->editColumn('total_yard',function ($summary)
            {
                return number_format($summary->total_yard, 4, '.', ',');
            })
            ->make(true);
        }
    }

    public function dataRackNonFreeStock(Request $request)
    {
        if(request()->ajax()) 
        {
            $locator_id = $request->locator_id;
            
            $summary =  DB::table('accessories_detail_rack_preparation_v')
            ->where('last_locator_id',$locator_id);
            
            return DataTables::of($summary)
            ->make(true);
        }
    }


    public function poBuyerPickList(request $request)
    {
        $q = $request->q;
        $material_preparations = MaterialPreparation::select('po_buyer')
        ->whereNull('deleted_at')
        ->where('warehouse',auth::user()->warehouse)
        ->where(function($query){
            $query->where('last_status_movement','in')
            ->orwhere('last_status_movement','change');
        })
        ->Where('po_buyer','like',"%$q%")
        ->groupby('po_buyer');

        $material_stocks = MaterialStock::select('po_buyer')
        ->where([
            ['warehouse_id',auth::user()->warehouse],
            ['type_po',2]
        ])
        ->whereNotNull('po_buyer')
        ->where(function($query) use ($q){
            $query->where('po_buyer','like',"%$q%");
        })
        ->groupby('po_buyer');

        $paginate = 10;
        $page = ($request->page) ? $request->page : 1;
        $union = $material_preparations->union($material_stocks)->get()->toArray();

        $slice = array_slice($union, $paginate * ($page - 1), $paginate);
		$lists = new LengthAwarePaginator($slice, count($union), $paginate, $page);
        $lists->setPath(action('AccessoriesLocatorController@poBuyerPickList'));
        
        return view('accessories_locator._po_buyer_locator',compact('lists'));

        
    }
}
