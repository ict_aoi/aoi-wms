<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Config;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\PoBuyer;

class AccessoriesReportMaterialIlaController extends Controller
{
    public function index()
    {
        return view('accessories_report_material_ila.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax())
        {
            // $warehouse_id       = $request->warehouse;
            // $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            // $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();

            $po_buyer       = $request->po_buyer;
            $data      = DB::select(db::raw("SELECT * FROM get_movement_ila(
				'".$po_buyer."'
				)
                order by last_status_movement asc;"
            ));
            
            // $data = db::table('report_out_item_ila_v')
            // ->where('warehouse','LIKE',"%$warehouse_id%")
            // ->whereBetween('created_at', [$start_date, $end_date])
            // ->orderby('created_at','desc')
            // ->get();

            return DataTables::of($data)
            ->editColumn('qty_movement',function ($data)
            {
                return number_format($data->qty_movement, 4, '.', ',');
            })
            ->editColumn('qty_need',function ($data)
            {
                return number_format($data->qty_need, 4, '.', ',');
            })
            ->editColumn('last_movement_date',function ($data)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $data->last_movement_date)->format('d/M/Y H:i:s');
            })
            ->EditColumn('last_status_movement',function ($data)
            {
                if($data->last_status_movement == 'receiving' or $data->last_status_movement == 'receive') return '<span class="label label-info">BARANG DITERIMA DIGUDANG</span>';
                elseif($data->last_status_movement == 'not receive') return '<span class="label label-default">BARANG BELUM DITERIMA DIGUDANG</span>';
                elseif($data->last_status_movement == 'in') return '<span class="label label-info">MATERIAL SIAP SUPLAI</span>';
                elseif($data->last_status_movement == 'expanse') return '<span class="label label-warning">EXPENSE</span>';
                elseif($data->last_status_movement == 'out') return '<span class="label label-success">MATERIAL SUDAH DISUPLAI</span>';
                elseif($data->last_status_movement == 'out-handover') return '<span class="label label-success">MATERIAL PINDAH TANGAN</span>';
                elseif($data->last_status_movement == 'out-subcont') return '<span class="label label-success">MATERIAL SUDAH DISUPLAI KE SUBCONT</span>';
                elseif($data->last_status_movement == 'reject') return '<span class="label label-danger">REJECT</span>';
                elseif($data->last_status_movement == 'change') return '<span class="label" style="background-color:black">MATERIAL PINDAH LOKASI RAK</span>';
                elseif($data->last_status_movement == 'hold') return '<span class="label" style="background-color:#fb8d00">MATERIAL DITAHAN QC UNTUK PENGECEKAN</span>';
                elseif($data->last_status_movement == 'adjustment' || $data->last_status_movement == 'adjusment') return '<span class="label" style="background-color:#838606">PENYESUAIAN QTY</span>';
                elseif($data->last_status_movement == 'print') return '<span class="label" style="background-color:#cc00ff">PRINT BARCODE</span>';
                elseif($data->last_status_movement == 'reroute') return '<span class="label" style="background-color:#ff6666">REROUTE</span>';
                elseif($data->last_status_movement == 'reverse' || $data->last_status_movement == 'reverse-handover' ||  $data->last_status_movement == 'reverse-stock') return '<span class="label" style="background-color:#00e6e6">MATERIAL DIKEMBALIKAN KE INVENTORY</span>';    
                elseif($data->last_status_movement == 'reverse-qc' ) return '<span class="label" style="background-color:#00e6e6">MATERIAL TIDAK JADI REJECT QC</span>';    
                elseif($data->last_status_movement == 'cancel item') return '<span class="label" style="background-color:#ff6666">CANCEL ITEM</span>';
                elseif($data->last_status_movement == 'cancel order') return '<span class="label" style="background-color:#ff6666">CANCEL ORDER</span>';
                elseif($data->last_status_movement == 'check') return '<span class="label" style="background-color:#ef6a22">MATERIAL DALAM PENGECEKAN QC</span>';
                elseif($data->last_status_movement == 'out-cutting') return '<span class="label label-success">MATERIAL SUDAH DISUPLAI KE CUTTING</span>';    
                elseif($data->last_status_movement == 'relax') return '<span class="label label-info">RELAX CUTTING</span>';   
                elseif($data->last_status_movement == 'switch') return '<span class="label" style="background-color:#333300">SWITCH</span>';   
                elseif($data->last_status_movement == 'cancel backlog') return '<span class="label" style="background-color:#b30000">CANCEL BACKLOG</span>'; 
                else return '<span class="label label-default">'.$data->last_status_movement.'</span>';   
            })
            ->editColumn('warehouse',function ($data)
            {
                if($data->warehouse == '1000002') return 'Warehouse accessories AOI 1';
                elseif($data->warehouse == '1000013') return 'Warehouse accessories AOI 2';
            })
            ->rawColumns(['warehouse','last_status_movement'])
            ->make(true);

        }
    }

    public function poBuyerPicklist(Request $request)
    {
        $po_buyer = $request->q;
        $lists = PoBuyer::select('po_buyer')
        ->where(function($query) use ($po_buyer){ 
            $query->where('po_buyer','like',"%$po_buyer%");
        })
        ->groupby('po_buyer')
        ->paginate(10);

        return view('accessories_report_material_ila._po_buyer_picklist',compact('lists'));
    }


}
