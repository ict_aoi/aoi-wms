<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class FabricReportSummaryBatchSupplierController extends Controller
{
    public function index()
    {
        $months = [
            '1' => 'January',
            '2' => 'February',
            '3' => 'March',
            '4' => 'April',
            '5' => 'May',
            '6' => 'June',
            '7' => 'July',
            '8' => 'Augusts',
            '9' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        ];
        
        $years          = array();
        $first_year     = 2018;

        while($first_year <= carbon::now()->format('Y'))
        {
            $years[$first_year] = $first_year;
            $first_year++;
        }

        return view('fabric_report_summary_batch_supplier.index',compact('years','months'));
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $year           = ($request->year) ? $request->year : Carbon::today()->format('Y');
            $month          = ($request->month) ? $request->month : Carbon::now()->format('m');
            
            $summary_batch  = DB::table('summary_batch_suppliers_v')
            ->where('year_receive',$year)
            ->where('month_receive',$month)
            ->orderby('year_receive','desc')
            ->orderby('month_receive','desc');

            return DataTables::of($summary_batch)
            ->addColumn('action',function($summary_batch){
                return view('fabric_report_summary_batch_supplier._action',[
                    'model'             => $summary_batch,
                    'detail'            => route('fabricReportSummaryBatchSupplier.dataDetail',[$summary_batch->c_bpartner_id,$summary_batch->year_receive,$summary_batch->month_receive]),
                ]);
            })
            ->make(true);
        }
    }

    public function dataDetail(Request $request,$c_bpartner_id,$year,$month)
    {
        if(request()->ajax()) 
        {
            $detail_summary_batch  = DB::table('detail_batch_suppliers_v')
            ->where('c_bpartner_id',$c_bpartner_id)
            ->where('year_receive',$year)
            ->where('month_receive',$month)
            ->orderby('document_no','desc');

            return DataTables::of($detail_summary_batch)
            ->make(true);
        }
    }

    public function export(Request $request)
    {
        $year           = ($request->year) ? $request->year : Carbon::today()->format('Y');
        $month          = ($request->month) ? $request->month : Carbon::now()->format('m');
        
        $summary_batch  = DB::table('summary_batch_suppliers_v')
        ->where('year_receive',$year)
        ->where('month_receive',$month)
        ->orderby('year_receive','desc')
        ->orderby('month_receive','desc')
        ->get();

        $file_name = 'summary_batch_suppplier_periode_arrival_'.$year.'_'.$month;
        return Excel::create($file_name,function($excel) use ($summary_batch){
            $excel->sheet('active',function($sheet)use($summary_batch)
            {
                $sheet->setCellValue('A1','TAHUN PENERIMAAN');
                $sheet->setCellValue('B1','BULAN PENERIMAAN');
                $sheet->setCellValue('C1','NAMA SUPPLIER');
                $sheet->setCellValue('D1','TOTAL BATCH');
                
            $row=2;
            foreach ($summary_batch as $i) 
            {  
                $sheet->setCellValue('A'.$row,$i->year_receive);
                $sheet->setCellValue('B'.$row,$i->month_receive);
                $sheet->setCellValue('C'.$row,$i->supplier_name);
                $sheet->setCellValue('D'.$row,$i->total_batch);
                $row++;
            }
            });

        })
        ->export('xlsx');
    }
}
