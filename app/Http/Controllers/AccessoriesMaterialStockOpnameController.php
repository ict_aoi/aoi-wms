<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Area;
use App\Models\Locator;
use App\Models\PoBuyer;
use App\Models\MaterialMovement;
use App\Models\MaterialPreparation;
use App\Models\MaterialMovementLine;
use App\Models\HistoryMaterialStockOpname;

class AccessoriesMaterialStockOpnameController extends Controller
{
    public function index()
    {
        return view('accessories_material_stock_opname.index');
    }

    public function create(Request $request)
    {
        $_warehouse_id = $request->warehouse_id;
        $barcode       = trim(strtoupper($request->barcode));
        $has_dash      = strpos($barcode, "-");

        if($has_dash == false)
        {
            $_barcode = $barcode;
        }else
        {
            $split = explode('-',$barcode);
            $_barcode = $split[0];
        }
        
        $material_preparation   = MaterialPreparation::where([
            ['barcode',$_barcode],
            ['warehouse',$_warehouse_id],
        ])
        ->whereNull('deleted_at')
        ->first();

        if(!$material_preparation) return response()->json('Barcode not found.',422);

        $master_po_buyer            = PoBuyer::where('po_buyer',$material_preparation->po_buyer)->first();

        $obj                        = new stdClass();
        $obj->id                    = $material_preparation->id;
        $obj->barcode               = $material_preparation->barcode;
        $obj->statistical_date      = ($master_po_buyer)? ($master_po_buyer->statistical_date) ? $master_po_buyer->statistical_date->format('d/m/Y') : ($master_po_buyer->promise_date)? $master_po_buyer->promise_date->format('d/m/Y') : null : null;
        $obj->season                = ($master_po_buyer)? $master_po_buyer->season : null;
        $obj->po_buyer              = $material_preparation->po_buyer;
        $obj->document_no           = $material_preparation->document_no;
        $obj->item_code             = $material_preparation->item_code;
        $obj->qty_conversion        = $material_preparation->qty_conversion;
        $obj->uom_conversion        = $material_preparation->uom_conversion;
        $obj->locator               = $material_preparation->lastLocator->code;

        return response()->json($obj,200);
    }

    public function store(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'barcode_products' => 'required|not_in:[]'
        ]);

        if($validator->passes())
        {
            $items                  = json_decode($request->barcode_products);
            $_warehouse_id          = $request->warehouse_id;
            $barcode_locator        = $request->barcode_locator;
            $movement_date          = Carbon::now()->toDateTimeString();
            $new_destination_id     = $request->_to_destination_id;
            $locator                = Locator::find($new_destination_id);
            $array_1                = array();
            $array_2                = array();

            foreach($items as $i)
            {
                try 
                {
                    DB::beginTransaction();

                    $material_preparation    = MaterialPreparation::find($i->id);
                    $from_destination        = $material_preparation->last_locator_id;
                    $po_buyer                = $material_preparation->po_buyer;
                    $item_id                 = $material_preparation->item_id;
                    $item_code               = $material_preparation->item_code;
                    $material_preparation_id = $material_preparation->id;
                    $qty_movement            = $material_preparation->qty_conversion;
                    $type_po                 = $material_preparation->type_po;
                    
                    if($from_destination != $new_destination_id)
                    {
                        if(!$locator->has_many_po_buyer)
                        {
                            if($locator->last_po_buyer)
                            {
                                if($locator->last_po_buyer != $po_buyer)
                                return response()->json('Locator has booked by po buyer '.$po_buyer,422); 
                            }
                        
                        }

                        MaterialPreparation::where('id',$i->id)->update([
                            'sto_result'        => true,
                            'updated_at'        => $movement_date,
                            'sto_date'          => $movement_date,
                            'sto_user_id'       => auth::user()->id,
                            'last_locator_id'   => $new_destination_id,
                        ]);
                        
                        $change_locator = MaterialMovement::FirstOrCreate([
                            'from_location'     => $from_destination,
                            'to_destination'    => $new_destination_id,
                            'is_active'         => true,
                            'po_buyer'          => $po_buyer,
                            'status'            => 'change',
                            'created_at'        => $movement_date,
                            'updated_at'        => $movement_date,
                        ]);

                        MaterialMovementLine::firstOrCreate([
                            'material_movement_id'    => $change_locator->id,
                            'material_preparation_id' => $material_preparation_id,
                            'item_id'                 => $item_id,
                            'item_code'               => $item_code,
                            'type_po'                 => $type_po,
                            'qty_movement'            => $qty_movement,
                            'date_movement'           => $movement_date,
                            'created_at'              => $movement_date,
                            'updated_at'              => $movement_date,
                            'is_active'               => true,
                            'note'                    => 'DIPINDAHKAN MENGGUNAKAN MENU STO',
                            'user_id'                 => Auth::user()->id
                        ]);
                        
                        $locator->last_sto_date     = $movement_date;
                        $locator->last_user_sto_id  = Auth::user()->id;
                        
                        $locator->save();

                        $array_1 []                  = $new_destination_id;
                        $array_2 []                  = $from_destination;

                        
                    }

                    $material_preparation->sto_result   = true;
                    $material_preparation->updated_at   = $movement_date;
                    $material_preparation->sto_date     = $movement_date;
                    $material_preparation->sto_user_id  = auth::user()->id;
                    $material_preparation->save();

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }

            $this->updateLastCounterLocator($array_1);
            $this->updateOldCounterLocator($array_2);
            return response()->json(200);
        }else
        {
            return response()->json('Please scan barcode first.',422);
        }
    }

    static function updateLastCounterLocator($locators)
    {
        try 
        {
            DB::beginTransaction();

            foreach ($locators as $key => $locator) 
            {
                $data           = Locator::find($locator);
                $total_counter  = MaterialPreparation::where('last_locator_id',$locator)
                ->whereNull('deleted_at')
                ->where(function($query){
                    $query->where('last_status_movement','in')
                    ->orWhere('last_status_movement','change');
                })
                ->count();

                $data->counter_in                       = $total_counter;

                if($total_counter == 0) $data->last_po_buyer  = null;

                $data->save();
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function updateOldCounterLocator($locators)
    {
        try 
        {
            DB::beginTransaction();
            
            foreach ($locators as $key => $locator) 
            {
                $data           = Locator::find($locator);
                $total_counter  = MaterialPreparation::where('last_locator_id',$locator)
                ->whereNull('deleted_at')
                ->where(function($query){
                    $query->where('last_status_movement','in')
                    ->orWhere('last_status_movement','change');
                })
                ->count();

                $data->counter_in    = $total_counter;
                if($total_counter == 0) $data->last_po_buyer = null;
                $data->save();
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function locatorPicklist(Request $request)
    {
        $q              = strtoupper($request->q);
        $warehouse_id   = $request->warehouse_id;

        $lists = Locator::whereHas('area',function($query) use($warehouse_id)
        {
            $query->where([
                ['warehouse',$warehouse_id],
                ['is_active',true],
                ['is_destination',false],
                ['name','<>','ADJUSTMENT'],
                ['name','<>','RECEIVING'],
                ['name','<>','BOM'],
                ['name','<>','QC'],
                ['name','<>','FREE STOCK'],
                ['name','<>','SUPPLIER'],
                ['name','<>','SWITCH'],
            ]);
        })
        ->where('is_active',true)
        ->where(function($query) use($q)
        {
            $query->where('barcode','like',"%$q%")
            ->orWhere('code','like',"%$q%");
        })
        ->orderBy('rack','asc')
        ->orderBy('y_row','asc')
        ->orderBy('z_column','asc')
        ->paginate('15');
        
        return view('accessories_material_stock_opname._rack_accesories_list',compact('lists'));
    }
}
