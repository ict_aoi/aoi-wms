<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class FabricReportDailyMaterialArrivalController extends Controller
{
    public function index()
    {
        return view('fabric_report_daily_material_arrival.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id       = $request->warehouse;
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();

            $daily_arrivals = DB::select(db::raw("SELECT * FROM get_report_monitor_receivement_fabric(
                '".$warehouse_id."', '".$start_date."', '".$end_date."');"
               ));
            
            // $daily_arrivals     = DB::table('monitor_receivements_fabrics_v')
            // ->where('warehouse_id','LIKE',"%$warehouse_id%")
            // ->whereBetween('arrival_date',[$start_date,$end_date])
            // ->orderby('arrival_date','desc');
            
            return DataTables::of($daily_arrivals)
            ->editColumn('arrival_date',function ($daily_arrivals)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $daily_arrivals->arrival_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('total_yard',function ($daily_arrivals)
            {
                return number_format($daily_arrivals->total_yard, 4, '.', ',');
            })
            ->make(true);
        }
    }

    public function export(Request $request)
    {
        $warehouse = ($request->warehouse ? $request->warehouse : auth::user()->warehouse);
        if(auth::user()->hasRole(['admin-ict-fabric','mm-staff']))
        {
            $filename = 'report_monitoring_receiving_fabric.csv';
        }else 
        {
            if($warehouse == '1000011') $filename = 'report_monitoring_receiving_fabric_aoi2.csv';
            else if($warehouse == '1000001') $filename = 'report_monitoring_receiving_fabric_aoi1.csv';
        }

          $file = Config::get('storage.report') . '/' . $filename;
      
          if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';
          
          $resp = response()->download($file);
          $resp->headers->set('Pragma', 'no-cache');
          $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
          $resp->headers->set('X-Content-Type-Options', 'nosniff');
          return $resp;
    }
}
