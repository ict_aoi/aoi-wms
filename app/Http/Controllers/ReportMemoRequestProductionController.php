<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Item;
use App\Models\Locator;
use App\Models\PoBuyer;
use App\Models\Temporary;
use App\Models\UomConversion;
use App\Models\MaterialMovement;
use App\Models\MaterialPreparation;
use App\Models\MaterialRequirement;
use App\Models\MaterialMovementLine;

class ReportMemoRequestProductionController extends Controller
{
    public function index()
    {
        return view('report_memo_request_production.index');
    }

    public function import()
    {
        return view('report_memo_request_production.import');
    }

    public function data(Request $request)
    {
         if(request()->ajax()) 
         {
            $po_buyer    = $request->po_buyer;
            $mrp        = DB::select(db::raw("SELECT * FROM get_mrp_cte(
                '".$po_buyer."'
                ) order by category asc, item_code asc, style desc;"
            ));

            return DataTables::of($mrp)
            ->editColumn('statistical_date',function ($mrp)
            {
                if($mrp->statistical_date) return Carbon::createFromFormat('Y-m-d H:i:s', $mrp->statistical_date)->format('d/M/Y');
                else return null;

            })
            ->editColumn('last_movement_date',function ($mrp)
            {
                if($mrp->last_movement_date) return Carbon::createFromFormat('Y-m-d H:i:s', $mrp->last_movement_date)->format('d/M/Y H:i:s');
                else return null;
                    
            })
            ->editColumn('qty_need',function ($mrp)
            {
                return number_format($mrp->qty_need, 4, '.', ',');
            })
            ->editColumn('qty_available',function ($mrp)
            {
                return number_format($mrp->qty_available, 4, '.', ',');
            })
            ->editColumn('qty_supply',function ($mrp)
            {
                return number_format($mrp->qty_supply, 4, '.', ',');
            })
            ->editColumn('qty_handover_no_received_yet',function ($mrp)
            {
                return '<button type="button" class="btn btn-link legitRipple"  onclick="showDetail(\''.$mrp->po_buyer.'\',\''.$mrp->item_code.'\',\''.$mrp->article_no.'\',\''.$mrp->style.'\')">'.number_format($mrp->qty_handover_no_received_yet, 4, '.', ',').'</button>';
                
            })
            ->editColumn('last_locator_code',function ($mrp)
            {
                $concat_locator_name = '';
                $locators            = MaterialPreparation::select('last_locator_id')
                ->where([
                    ['po_buyer', $mrp->po_buyer],
                    ['item_code', $mrp->item_code],
                    ['style', $mrp->style],
                ])
                ->whereNull('cancel_planning')
                ->groupby('last_locator_id')
                ->get();

                $array = array();
                foreach($locators as $key => $value)
                {
                        $locator             = Locator::find($value->last_locator_id);
                        if($locator)
                        {
                            $locator_name        = $locator->code;
                            $concat_locator_name .= $locator_name.',';
                        }
                        else
                        {
                            $concat_locator_name = null;
                        }
                }

                return substr_replace($concat_locator_name,'',-1);
                
                
            })
            ->EditColumn('last_status_movement',function ($mrp)
            {
                if($mrp->last_status_movement == 'receiving') return '<span class="label label-info">BARANG DITERIMA DIGUDANG</span>';
                elseif($mrp->last_status_movement == 'not receive') return '<span class="label label-default">BARANG BELUM DITERIMA DIGUDANG</span>';
                elseif($mrp->last_status_movement == 'in') return '<span class="label label-info">MATERIAL SIAP SUPLAI</span>';
                elseif($mrp->last_status_movement == 'expanse') return '<span class="label label-warning">EXPENSE</span>';
                elseif($mrp->last_status_movement == 'out') return '<span class="label label-success">MATERIAL SUDAH DISUPLAI</span>';
                elseif($mrp->last_status_movement == 'out-handover') return '<span class="label label-success">MATERIAL PINDAH TANGAN</span>';
                elseif($mrp->last_status_movement == 'out-subcont') return '<span class="label label-success">MATERIAL SUDAH DISUPLAI KE SUBCONT</span>';
                elseif($mrp->last_status_movement == 'qc-release') return '<span class="label" style="background-color:#fcf403">MATERIAL SUDAH RELEASE QC</span>';
                elseif($mrp->last_status_movement == 'reject') return '<span class="label label-danger">REJECT</span>';
                elseif($mrp->last_status_movement == 'change') return '<span class="label" style="background-color:black">MATERIAL PINDAH LOKASI RAK</span>';
                elseif($mrp->last_status_movement == 'hold') return '<span class="label" style="background-color:#fb8d00">MATERIAL DITAHAN QC UNTUK PENGECEKAN</span>';
                elseif($mrp->last_status_movement == 'adjustment' || $mrp->last_status_movement == 'adjusment') return '<span class="label" style="background-color:#838606">PENYESUAIAN QTY</span>';
                elseif($mrp->last_status_movement == 'print') return '<span class="label" style="background-color:#cc00ff">PRINT BARCODE</span>';
                elseif($mrp->last_status_movement == 'reroute') return '<span class="label" style="background-color:#ff6666">REROUTE</span>';
                elseif($mrp->last_status_movement == 'reverse' || $mrp->last_status_movement == 'reverse-handover' ||  $mrp->last_status_movement == 'reverse-stock') return '<span class="label" style="background-color:#00e6e6">MATERIAL DIKEMBALIKAN KE INVENTORY</span>';    
                elseif($mrp->last_status_movement == 'reverse-qc' ) return '<span class="label" style="background-color:#00e6e6">MATERIAL TIDAK JADI REJECT QC</span>';    
                elseif($mrp->last_status_movement == 'cancel item') return '<span class="label" style="background-color:#ff6666">CANCEL ITEM</span>';
                elseif($mrp->last_status_movement == 'cancel order') return '<span class="label" style="background-color:#ff6666">CANCEL ORDER</span>';
                elseif($mrp->last_status_movement == 'check') return '<span class="label" style="background-color:#ef6a22">MATERIAL DALAM PENGECEKAN QC</span>';
                elseif($mrp->last_status_movement == 'out-cutting') return '<span class="label label-success">MATERIAL SUDAH DISUPLAI KE CUTTING</span>';    
                elseif($mrp->last_status_movement == 'relax') return '<span class="label label-info">RELAX CUTTING</span>';   
                elseif($mrp->last_status_movement == 'switch') return '<span class="label" style="background-color:#333300">SWITCH</span>';   
                elseif($mrp->last_status_movement == 'cancel backlog') return '<span class="label" style="background-color:#b30000">CANCEL BACKLOG</span>'; 
                else return '<span class="label label-default">'.$mrp->last_status_movement.'</span>';   
            })
            ->addColumn('action',function($mrp)
            {
                if(auth::user()->hasRole(['admin-ict-acc','closing-mrp']))
                {
                    $availble_qty   = ($mrp->qty_available)? $mrp->qty_available : '0';
                    $supply_qty     = ($mrp->qty_supply)? $mrp->qty_supply : '0';
                    $need_qty       = ($mrp->qty_need)? $mrp->qty_need : '0';

                    return view('report_memo_request_production._action',[
                        'model'     => $mrp,
                        'closeMrp'  => $mrp->po_buyer.'|'.$mrp->item_code.'|'.$mrp->style.'|'.$mrp->article_no.'|'.$need_qty.'|'.$availble_qty.'|'.$supply_qty.'|'.$mrp->uom.'|'.$mrp->backlog_status,
                        'movement'  => true
                    ]);
                }else
                {
                    return view('report_memo_request_production._action',[
                        'model'     => $mrp,
                        'movement'  => true
                    ]);
                }
                
            })
            ->setRowAttr([
                'style' => function($mrp) use($po_buyer) 
                {
                    $check_po_buyer_is_cancel = PoBuyer::where('po_buyer',$po_buyer)
                    ->whereNotNull('cancel_date')
                    ->exists();

                    if($check_po_buyer_is_cancel){
                        return  'background-color: #f44336;color:white';
                    }

                    if($mrp->warehouse == 'WAREHOUSE ACC AOI 2' || $mrp->warehouse == 'WAREHOUSE FAB AOI 2'){
                        return  'background-color: #ffd6fe;color:black';
                    }
                },
            ])
            ->rawColumns(['style','last_status_movement','action','qty_handover_no_received_yet','last_locator_code'])
            ->make(true);
            
         }
    }

    public function dataHistory(Request $request)
    {
        if(request()->ajax()) 
        {
            $po_buyer   = $request->po_buyer;
            $item_code  = $request->item_code;
            $article_no = $request->article_no;
            $style      = $request->style;

            $histories  = MaterialPreparation::where([
                ['po_buyer',$po_buyer],
                ['item_code',$item_code],
                ['article_no',$article_no],
                ['style',$style],
            ])
            ->whereNull('cancel_planning')
            ->orderby('last_movement_date','desc');

            return DataTables::of($histories)
            ->editColumn('qty_conversion',function ($histories)
            {
                return number_format($histories->qty_conversion, 4, '.', ',');
            })
            ->editColumn('last_movement_date',function ($histories)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $histories->last_movement_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('last_locator_id',function($histories)
            {
                if($histories->last_locator_id !=null) return $histories->lastLocator->code;
                else return '';
            })
            ->editColumn('warehouse',function($histories)
            {
                if($histories->warehouse =='1000002') 
                    return $histories->warehouse = 'Warehouse Accessories AOI 1';
                elseif($histories->warehouse =='1000013') 
                    return $histories->warehouse = 'Warehouse Accessories AOI 2';
                elseif($histories->warehouse =='1000001') 
                    return $histories->warehouse = 'Warehouse Fabric AOI 1';
                else 
                    return $histories->warehouse = 'Warehouse Fabric AOI 2';
            })
            ->editColumn('last_user_movement_id',function($histories)
            {
                if($histories->last_user_movement_id !=null) return $histories->lastUserMovement->name;
                else return '';
            })
            ->make(true);
        }
    }

    public function poBuyerPicklist(Request $request)
    {
        $po_buyer = $request->q;
        $lists = PoBuyer::select('po_buyer')
        ->whereNotIn('po_buyer', function($query) { 
            $query->select('po_buyer')
                  ->where('po_buyer','like','%-S%')
                  ->where('brand','ADIDAS');
        })
        ->where(function($query) use ($po_buyer){ 
            $query->where('po_buyer','like',"%$po_buyer%");
        })
        ->groupBy('po_buyer')
        ->paginate(10);

        return view('report_material_movement._po_buyer_picklist',compact('lists'));
    }

    public function export(Request $request)
    {
        if($request->po_buyer_id=='')
        {
            return redirect()->back()
            ->withErrors([
                'po_buyer' => 'Please select po buyer first',
            ]);
        } 

        $po_buyer   = trim($request->po_buyer);
        $mrp = DB::select(db::raw("SELECT * FROM get_mrp_cte(
            '".$po_buyer."'
            ) order by category asc, item_code asc, style desc;"
        ));;

        $file_name = 'LAPORAN_MRP_'.$po_buyer;
        return Excel::create($file_name,function ($excel) use($mrp)
        {
            $excel->sheet('main', function($sheet) use($mrp) {
                $sheet->setCellValue('A1','#');
                $sheet->setCellValue('B1','TANGGAL PO DD');
                $sheet->setCellValue('C1','STATUS BACKLOG');
                $sheet->setCellValue('D1','STYLE');
                $sheet->setCellValue('E1','ARTICLE NO');
                $sheet->setCellValue('F1','PO BUYER');
                $sheet->setCellValue('G1','KATEGORI ITEM');
                $sheet->setCellValue('H1','KODE ITEM');
                $sheet->setCellValue('I1','QTY NEED');
                $sheet->setCellValue('J1','UOM');
                $sheet->setCellValue('K1','STATUS TERAKHIR');
                $sheet->setCellValue('L1','WAREHOUSE');
                $sheet->setCellValue('M1','LOKASI TERAKHIR');
                $sheet->setCellValue('N1','TANGGAL PERPINDAHAN TERAKHIR');
                $sheet->setCellValue('O1','USER YANG MELAKUKAN PERPINDAHAN TERAKHIR');
                $sheet->setCellValue('P1','QTY AVAILABLE');
                $sheet->setCellValue('Q1','QTY SUPPLY');
                $sheet->setCellValue('R1','PO SUPPLIER');
                $sheet->setCellValue('S1','REMARK');
                $sheet->setCellValue('T1','SYSTEM LOG');

                $index = 0;
                foreach ($mrp as $key => $value) {
                    $row = $index + 2;
                    $index++;

                    if($value->last_status_movement == 'receiving') $last_status_movement = 'BARANG DITERIMA DIGUDANG';
                    else if($value->last_status_movement == 'not receive') $last_status_movement = 'BARANG BELUM DITERIMA DIGUDANG';
                    else if($value->last_status_movement == 'in') $last_status_movement = 'MATERIAL SIAP SUPLAI';
                    elseif($value->last_status_movement == 'expanse') $last_status_movement = 'EXPENSE';
                    elseif($value->last_status_movement == 'out') $last_status_movement = 'MATERIAL SUDAH DISUPLAI';
                    elseif($value->last_status_movement == 'out-handover')  $last_status_movement = 'MATERIAL PINDAH TANGAN';
                    elseif($value->last_status_movement == 'out-subcont') $last_status_movement = 'MATERIAL SUDAH DISUPLAI KE SUBCONT';
                    elseif($value->last_status_movement == 'reject') $last_status_movement = 'REJECT';
                    elseif($value->last_status_movement == 'change') $last_status_movement = 'MATERIAL PINDAH LOKASI RAK';
                    elseif($value->last_status_movement == 'hold') $last_status_movement = 'MATERIAL DITAHAN QC UNTUK PENGECEKAN';
                    elseif($value->last_status_movement == 'adjustment' || $value->last_status_movement == 'adjusment') $last_status_movement = 'PENYESUAIAN QTY';
                    elseif($value->last_status_movement == 'print') $last_status_movement = 'PRINT BARCODE';
                    elseif($value->last_status_movement == 'reroute') $last_status_movement = 'REROUTE';
                    elseif($value->last_status_movement == 'cancel item') $last_status_movement = 'CANCEL ITEM';
                    elseif($value->last_status_movement == 'cancel order') $last_status_movement = 'CANCEL ORDER';
                    elseif($value->last_status_movement == 'check') $last_status_movement = 'MATERIAL DALAM PENGECEKAN QC';
                    else if($value->last_status_movement == 'out-cutting') $last_status_movement = 'MATERIAL SUDAH DISUPLAI KE CUTTING';    
                    else if($value->last_status_movement == 'relax')  $last_status_movement = 'RELAX CUTTING';   
                    else $last_status_movement = $value->last_status_movement;
                    
                    if($value->backlog_status) $status_backlog = $value->backlog_status;
                    else $status_backlog = '-';

                    if($value->qty_available) $qty_available = $value->qty_available;
                    else $qty_available = '0';

                    if($value->qty_supply) $qty_supply = $value->qty_supply;
                    else $qty_supply = '0';

                    if($value->warehouse) $warehouse = $value->warehouse;
                    else $warehouse = '-';

                    if($value->last_locator_code) $last_locator_code = $value->last_locator_code;
                    else $last_locator_code = '-';
                    
                    if($value->remark) $remark = $value->remark;
                    else $remark = '-';

                    if($value->document_no) $document_no = $value->document_no;
                    else $document_no = '-';

                    if($value->system_log) $system_log = $value->system_log;
                    else $system_log = '-';
                    
                    if($value->last_user_movement_name) $last_user_movement_name = $value->last_user_movement_name;
                    else $last_user_movement_name = '-';

                    if($value->last_movement_date) $last_movement_date = $value->last_movement_date;
                    else $last_movement_date = '-';
                    
                    $sheet->setCellValue('A'.$row, $index);
                    $sheet->setCellValue('B'.$row, $value->statistical_date);
                    $sheet->setCellValue('C'.$row, $status_backlog);
                    $sheet->setCellValue('D'.$row, $value->style);
                    $sheet->setCellValue('E'.$row, $value->article_no);
                    $sheet->setCellValue('F'.$row, $value->po_buyer);
                    $sheet->setCellValue('G'.$row, $value->category);
                    $sheet->setCellValue('H'.$row, $value->item_code);
                    $sheet->setCellValue('I'.$row, $value->qty_need);
                    $sheet->setCellValue('J'.$row, $value->uom);
                    $sheet->setCellValue('K'.$row, $last_status_movement);
                    $sheet->setCellValue('L'.$row, $warehouse);
                    $sheet->setCellValue('M'.$row, $last_locator_code);
                    $sheet->setCellValue('N'.$row, $last_movement_date);
                    $sheet->setCellValue('O'.$row, $last_user_movement_name);
                    $sheet->setCellValue('P'.$row, $qty_available);
                    $sheet->setCellValue('Q'.$row, $qty_supply);
                    $sheet->setCellValue('R'.$row, $document_no);
                    $sheet->setCellValue('S'.$row, $remark);
                    $sheet->setCellValue('T'.$row, $system_log);
                }
            });

        })
        ->export('xls');
    }

    public function printPreview(Request $request)
    {
        $po_buyer = trim($request->_print_po_buyer);

        if($po_buyer == '')
        {
            return redirect()->back()
            ->withErrors([
                'po_buyer' => 'Please select po buyer first',
            ]);
        } 

        $mrp = DB::select(db::raw("SELECT * FROM get_mrp_cte(
            '".$po_buyer."'
            ) order by category asc, item_code asc, style desc;"
        ));;


        return view('report_memo_request_production.print', compact('mrp'));
    }

    public function downloadAll()
    {
        $filename   = 'Report_MRP.csv';
        $file       = Config::get('storage.mrp') . '/' . $filename;
        
        if(!file_exists($file)) return 'File not found.';
        
        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function closing(Request $request)
    {
        $po_buyer       = $request->po_buyer;
        $item_code      = $request->item_code;
        $style          = $request->style;
        $article_no     = $request->article_no;
        $backlog_status = $request->backlog_status;
        $warehouse_id   = $request->warehouse_closing;
        $closing_reason = strtoupper(trim($request->closing_reason));
        $qty_closing    = sprintf('%0.8f',$request->closing_qty);
        $movement_date  = carbon::now()->toDateTimeString();
        
        $this->doClosing($po_buyer,$item_code,$style,$article_no,$backlog_status,$warehouse_id,$closing_reason,$qty_closing,$movement_date);

        return response()->json(200);
    }

    public function exportClosing()
    {
        return Excel::create('upload_closing_mrp',function ($excel)
        {
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','PO_BUYER');

                $sheet->setWidth('A', 15);
                $sheet->setColumnFormat(array(
                    'A' => '@'
                ));
                $sheet->setCellValue('B1','ITEM_CODE');

                $sheet->setWidth('B', 15);
                $sheet->setColumnFormat(array(
                    'B' => '@'
                ));
                $sheet->setCellValue('C1','WAREHOUSE');

                $sheet->setWidth('C', 15);
                $sheet->setColumnFormat(array(
                    'C' => '@'
                ));
                $sheet->setCellValue('D1','REASON');

                $sheet->setWidth('D', 15);
                $sheet->setColumnFormat(array(
                    'D' => '@'
                ));

            });
            $excel->setActiveSheetIndex(0);
          
            
            
        })->export('xlsx');
    }

    public function importClosing(Request $request)
    {
        $array = array();
        //$obj = new stdClass();
        if($request->hasFile('upload_file')){
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);
          
            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                    foreach ($data as $key => $value) 
                    {
                        $mrp = DB::select(db::raw("SELECT *, qty_need - qty_supply as qty_outstanding 
                            FROM get_mrp_cte(
                            '".$value->po_buyer."'
                            ) WHERE item_code ='".$value->item_code."' AND (qty_need - qty_supply) > 0
                            order by category asc, item_code asc;"
                        ));

                        if($mrp != '')
                        {
                            $movement_date  = carbon::now()->toDateTimeString();
                            foreach ($mrp as $key1 => $value1) 
                            {
                                $po_buyer       = $value1->po_buyer;
                                $item_code      = $value1->item_code;
                                $style          = $value1->style;
                                $article_no     = $value1->article_no;
                                $qty_closing    = sprintf('%0.8f',$value1->qty_outstanding);
                                $backlog_status = false;
                                $warehouse_id   = $value->warehouse;
                                $closing_reason = strtoupper(trim($value->reason));
                                
                                $this->doClosing($po_buyer,$item_code,$style,$article_no,$backlog_status,$warehouse_id,$closing_reason,$qty_closing,$movement_date);
                                
                                $obj            = new stdClass();
                                $obj->po_buyer  = trim(strtoupper($value->po_buyer));
                                $obj->item_code = trim(strtoupper($value->item_code));
                                $obj->warehouse = $value->warehouse;
                                $obj->reason    = $value->reason;
                                $obj->result    = 'DATA BERHASIL DI CLOSE';
                                $array []       = $obj;
                            }
                        }
                        else
                        {
                            $obj            = new stdClass();
                            $obj->po_buyer  = trim(strtoupper($value->po_buyer));
                            $obj->item_code = trim(strtoupper($value->item_code));
                            $obj->warehouse = $value->warehouse;
                            $obj->reason    = $value->reason;
                            $obj->result    = 'DATA MRP TIDAK DITEMUKAN SILAHKAN CEK FILE KEMBALI';
                            $array []       = $obj;
                        }
                    }
                    return response()->json($array,200);
            }
            else
            {
                return response()->json('import gagal, silahkan cek file anda',422);
            }

        }
    }

    public function dataDetailHandover(Request $request)
    {
        if(request()->ajax()) 
        {
            $po_buyer   = $request->po_buyer;
            $item_code  = $request->item_code;
            $article_no = $request->article_no;
            $style      = $request->style;

            $handover  = MaterialPreparation::where([
                ['po_buyer',$po_buyer],
                ['item_code',$item_code],
                ['article_no',$article_no],
                ['style',$style],
                ['last_status_movement', 'out-handover']
            ])
            ->orderby('last_movement_date','desc');
            
            return DataTables::of($handover)
            ->editColumn('qty_conversion',function ($handover)
            {
                return number_format($handover->qty_conversion, 4, '.', ',');
            })
            ->editColumn('last_movement_date',function ($handover)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $handover->last_movement_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('warehouse',function($handover)
            {
                if($handover->warehouse =='1000002') 
                    return $handover->warehouse = 'Warehouse Accessories AOI 1';
                elseif($handover->warehouse =='1000013') 
                    return $handover->warehouse = 'Warehouse Accessories AOI 2';
                elseif($handover->warehouse =='1000001') 
                    return $handover->warehouse = 'Warehouse Fabric AOI 1';
                else 
                    return $handover->warehouse = 'Warehouse Fabric AOI 2';
            })
            ->editColumn('last_user_movement_id',function($handover)
            {
                if($handover->last_user_movement_id !=null) return $handover->lastUserMovement->name;
                else return '';
            })
            ->make(true);
        }
    }    


    static function doClosing($po_buyer,$item_code,$style,$article_no,$backlog_status,$warehouse_id,$closing_reason,$qty_closing,$movement_date)
    {
        $inventory_erp = Locator::with('area')
        ->where([
            ['is_active',true],
            ['rack','ERP INVENTORY'],
        ])
        ->whereHas('area',function ($query) use($warehouse_id)
        {
            $query->where('is_active',true);
            $query->where('warehouse',$warehouse_id);
            $query->where('name','ERP INVENTORY');
        })
        ->first();

        $missing_item_from = Locator::with('area')
        ->whereHas('area',function ($query) use ($warehouse_id)
        {
            $query->where([
                ['warehouse',$warehouse_id],
                ['name','MISSING ITEM'],
                ['is_destination',false]
            ]);

        })
        ->first();

        if($backlog_status != '') $is_backlog = true;
        else $is_backlog = false;
       
        $item           = Item::where('item_code',$item_code)->first();
        $item_id        = ($item)? $item->item_id : null;
        $uom_conversion = ($item)? $item->uom : null;
        $item_desc      = ($item)? $item->item_desc : null;
        $category       = ($item)? $item->category : null;

        $conversion     = UomConversion::where('item_code',$item_code)->first();

        if($conversion)$multiplyrate = $conversion->multiplyrate;
        else $multiplyrate = 1;

        $qty_reconversion = $qty_closing * $multiplyrate;

        if($warehouse_id == '1000002' || $warehouse_id == '1000013')
        {
            $locator = Locator::where([
                ['rack','DISTRIBUSI']
            ])
            ->whereHas('area',function($query) use ($warehouse_id){
                $query->where('warehouse',$warehouse_id);
            })
            ->first();
        }else if($warehouse_id == '1000011' || $warehouse_id == '1000001')
        {
            $locator = Locator::where([
                ['rack','CUTTING']
            ])
            ->whereHas('area',function($query) use ($warehouse_id){
                $query->where('warehouse',$warehouse_id);
            })
            ->first();
        }
        

        try 
        {
            DB::beginTransaction();

            $material_requirement = MaterialRequirement::where([
                ['po_buyer',$po_buyer],
                ['item_code',$item_code],
                ['style',$style],
                ['article_no',$article_no],
            ])
            ->first();

            $is_preparation_exists = MaterialPreparation::where([
                ['barcode','CLOSING '.$po_buyer],
                ['item_id',$item_id],
                ['c_order_id','CLOSING'],
                ['po_buyer',$po_buyer],
                ['uom_conversion',$uom_conversion],
                ['qty_conversion',$qty_closing],
                ['style',$style],
                ['article_no',$material_requirement->article_no],
            ])
            ->exists();

            if(!$is_preparation_exists)
            {
                $so_id          = PoBuyer::where('po_buyer', $po_buyer)->first();

                $material_preparation = MaterialPreparation::firstOrCreate([
                    'barcode'                                               => 'CLOSING_'.$po_buyer,
                    'item_id'                                               => $item_id,
                    'item_id_source'                                        => null,
                    'c_order_id'                                            => 'CLOSING',
                    'c_bpartner_id'                                         => 'CLOSING',
                    'supplier_name'                                         => 'CLOSING',
                    'document_no'                                           => 'CLOSING',
                    'po_detail_id'                                          => 'CLOSING',
                    'po_buyer'                                              => $po_buyer,
                    'uom_conversion'                                        => $uom_conversion,
                    'qty_conversion'                                        => $qty_closing,
                    'qty_reconversion'                                      => sprintf('%0.8f',$qty_reconversion),
                    'job_order'                                             => $material_requirement->job_order,
                    'style'                                                 => $style,
                    'warehouse'                                             => $warehouse_id,
                    'article_no'                                            => $material_requirement->article_no,
                    'item_code'                                             => $item_code,
                    'item_desc'                                             => $item_desc,
                    'category'                                              => $category,
                    'is_backlog'                                            => $is_backlog,
                    'total_carton'                                          => 1,
                    'type_po'                                               => 2,
                    'user_id'                                               => auth::user()->id,
                    'last_status_movement'                                  => 'out',
                    'last_locator_id'                                       => $locator->id,
                    'is_from_closing'                                       => true,
                    'is_need_inserted_to_locator_free_stock_erp'            => true,
                    'is_need_inserted_to_locator_inventory_erp'             => true,
                    'last_movement_date'                                    => $movement_date,
                    'created_at'                                            => $movement_date,
                    'updated_at'                                            => $movement_date,
                    'deleted_at'                                            => $movement_date,
                    'first_inserted_to_locator_date'                        => $movement_date,
                    'last_user_movement_id'                                 => auth::user()->id,
                    'ict_log'                                               => $closing_reason.'. QTY '.$qty_closing.' ADALAH UNTUK CLOSING',
                    'so_id'                                                 => $so_id->so_id
                ]);

                $is_movement_exists = MaterialMovement::where([
                    ['from_location',$missing_item_from->id],
                    ['to_destination',$locator->id],
                    ['from_locator_erp_id',$missing_item_from->area->erp_id],
                    ['to_locator_erp_id',$locator->area->erp_id],
                    ['po_buyer',$material_preparation->po_buyer],
                    ['is_complete_erp',false],
                    ['is_active',true],
                    ['no_packing_list','CLOSING'],
                    ['no_invoice','CLOSING'],
                    ['is_integrate',false],
                    ['status','out'],
                ])
                ->first();

                if(!$is_movement_exists)
                {
                    $movement_out = MaterialMovement::firstOrCreate([
                        'from_location'             => $missing_item_from->id,
                        'to_destination'            => $locator->id,
                        'from_locator_erp_id'       => $missing_item_from->area->erp_id,
                        'to_locator_erp_id'         => $locator->area->erp_id,
                        'po_buyer'                  => $po_buyer,
                        'is_complete_erp'           => false,
                        'is_integrate'              => false,
                        'is_active'                 => true,
                        'status'                    => 'out',
                        'no_packing_list'           => 'CLOSING',
                        'no_invoice'                => 'CLOSING',
                        'created_at'                => $movement_date,
                        'updated_at'                => $movement_date
                    ]);

                    $movement_out_id = $movement_out->id;
                }else
                {
                    $is_movement_exists->updated_at = $movement_date;
                    $is_movement_exists->save();

                    $movement_out_id = $is_movement_exists->id;
                }

                $is_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                ->where([
                    'material_movement_id'          => $movement_out_id,
                    'material_preparation_id'       => $material_preparation->id,
                    'warehouse_id'                  => $material_preparation->warehouse,
                    'item_id'                       => $material_preparation->item_id,
                    'qty_movement'                  => $material_preparation->qty_conversion,
                    'is_integrate'                  => false,
                    'is_active'                     => true,
                    'date_movement'                 => $material_preparation->last_movement_date,
                ])
                ->exists();
    
                if(!$is_line_exists)
                {
                    $new_material_movement_line = MaterialMovementLine::firstOrCreate([
                        'material_movement_id'                      => $movement_out_id,
                        'material_preparation_id'                   => $material_preparation->id,
                        'item_id'                                   => $material_preparation->item_id,
                        'item_id_source'                            => $material_preparation->item_id_source,
                        'item_code'                                 => $material_preparation->item_code,
                        'type_po'                                   => 2,
                        'c_order_id'                                => $material_preparation->c_order_id,
                        'c_bpartner_id'                             => $material_preparation->c_bpartner_id,
                        'supplier_name'                             => $material_preparation->supplier_name,
                        'c_orderline_id'                            => '-',
                        'uom_movement'                              => $material_preparation->uom_conversion,
                        'qty_movement'                              => $material_preparation->qty_conversion,
                        'date_movement'                             => $material_preparation->last_movement_date,
                        'created_at'                                => $material_preparation->last_movement_date,
                        'updated_at'                                => $material_preparation->last_movement_date,
                        'date_receive_on_destination'               => $material_preparation->last_movement_date,
                        'nomor_roll'                                => '-',
                        'warehouse_id'                              => $material_preparation->warehouse,
                        'document_no'                               => $material_preparation->document_no,
                        'is_active'                                 => true,
                        'is_integrate'                              => false,
                        'is_inserted_to_material_movement_per_size' => true,
                        'note'                                      => $closing_reason.'. QTY '.$qty_closing.' ADALAH UNTUK CLOSING',
                        'user_id'                                   => Auth::user()->id
                    ]);

                    $material_preparation->last_material_movement_line_id = $new_material_movement_line->id;
                    $material_preparation->save();
                }


                $is_movement_integration_exists = MaterialMovement::where([
                    ['from_location',$inventory_erp->id],
                    ['to_destination',$inventory_erp->id],
                    ['from_locator_erp_id',$inventory_erp->area->erp_id],
                    ['to_locator_erp_id',$inventory_erp->area->erp_id],
                    ['po_buyer',$material_preparation->po_buyer],
                    ['is_integrate',false],
                    ['is_active',true],
                    ['no_packing_list','CLOSING'],
                    ['no_invoice','CLOSING'],
                    ['status','integration-to-inventory-erp'],
                ])
                ->first();
                
                if(!$is_movement_integration_exists)
                {
                    $material_integration_movement = MaterialMovement::firstOrCreate([
                        'from_location'         => $inventory_erp->id,
                        'to_destination'        => $inventory_erp->id,
                        'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                        'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                        'po_buyer'              => $material_preparation->po_buyer,
                        'is_integrate'          => false,
                        'is_active'             => true,
                        'no_packing_list'       => 'CLOSING',
                        'no_invoice'            => 'CLOSING',
                        'status'                => 'integration-to-inventory-erp',
                        'created_at'            => $material_preparation->last_movement_date,
                        'updated_at'            => $material_preparation->last_movement_date,
                    ]);

                    $material_integration_movement_id = $material_integration_movement->id;
                }else
                {
                    $is_movement_integration_exists->updated_at = $material_preparation->last_movement_date;
                    $is_movement_integration_exists->save();

                    $material_integration_movement_id = $is_movement_integration_exists->id;
                }

                $is_material_movement_line_integration_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                ->where([
                    ['material_movement_id',$material_integration_movement_id],
                    ['material_preparation_id',$material_preparation->id],
                    ['warehouse_id',$material_preparation->warehouse],
                    ['item_id',$material_preparation->item_id],
                    ['is_active',true],
                    ['is_integrate',false],
                    ['qty_movement',$material_preparation->qty_conversion],
                    ['date_movement',$material_preparation->last_movement_date],
                ])
                ->exists();

                if(!$is_material_movement_line_integration_exists)
                {
                    $new_material_movement_line = MaterialMovementLine::Create([
                        'material_movement_id'          => $material_integration_movement_id,
                        'material_preparation_id'       => $material_preparation->id,
                        'item_code'                     => $material_preparation->item_code,
                        'item_id'                       => $material_preparation->item_id,
                        'item_id_source'                => $material_preparation->item_id_source,
                        'c_order_id'                    => $material_preparation->c_order_id,
                        'c_orderline_id'                => '-',
                        'c_bpartner_id'                 => $material_preparation->c_bpartner_id,
                        'supplier_name'                 => $material_preparation->supplier_name,
                        'type_po'                       => 2,
                        'is_active'                     => true,
                        'is_integrate'                  => false,
                        'uom_movement'                  => $material_preparation->uom_conversion,
                        'qty_movement'                  => $material_preparation->qty_conversion,
                        'date_movement'                 => $material_preparation->last_movement_date,
                        'created_at'                    => $material_preparation->last_movement_date,
                        'updated_at'                    => $material_preparation->last_movement_date,
                        'date_receive_on_destination'   => $material_preparation->last_movement_date,
                        'nomor_roll'                    => '-',
                        'warehouse_id'                  => $material_preparation->warehouse,
                        'document_no'                   => $material_preparation->document_no,
                        'note'                          => 'PROSES INTEGRASI DIKARNAKAN CLOSING DARI MENU MRP (ALASAN DI CLOSE KARNA '.$closing_reason.')',
                        'is_active'                     => true,
                        'user_id'                       => auth::user()->id,
                    ]);

                    $material_preparation->last_material_movement_line_id = $new_material_movement_line->id;
                    $material_preparation->save();
                }
            }
            
            Temporary::Create([
                'barcode'       => $po_buyer,
                'status'        => 'mrp',
                'user_id'       => Auth::user()->id,
                'created_at'    => $movement_date,
                'updated_at'    => $movement_date,
            ]);

            DB::commit();
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }
    
}
