<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

use App\Models\User;
use App\Models\Item;
use App\Models\Barcode;
use App\Models\Locator;
use App\Models\Supplier;
use App\Models\Temporary;
use App\Models\PoSupplier;
use App\Models\UomConversion;
use App\Models\MappingStocks;
use App\Models\MaterialStock;
use App\Models\AllocationItem;
use App\Models\SummaryStockFabric;
use App\Models\MaterialStockPerLot;
use App\Models\DetailMaterialStock;
use App\Models\MovementStockHistory;
use App\Models\HistoryMaterialStockOpname;

use App\Models\HistoryMaterialStocks;

use App\Http\Controllers\FabricMaterialStockApprovalController as HistoryStock;
class FabricMaterialStockController extends Controller
{
    public function index()
    {
        //$flag = Session::get('flag');
        //return view('fabric_material_stock.index',compact('flag'));

        $mapping_stocks = MappingStocks::select('type_stock_erp_code','type_stock','document_number')->get();
        return view('fabric_material_stock.create',compact('mapping_stocks'));
    }

    public function data(Request $request)
    {
        if ($request->ajax())
        { 
            $data = MaterialStock::where([
                ['warehouse_id',auth::user()->warehouse],
                ['is_material_others',true],
                ['is_roll_cancel',false],
                ['is_closing_balance',false],
            ])
            ->orderby('created_at','desc');
            
            return DataTables::of($data)
            ->editColumn('qty_order',function ($data)
            {
                return number_format($data->qty_order, 4, '.', ',');
            })
            ->editColumn('available_qty',function ($data)
            {
                return number_format($data->available_qty, 4, '.', ',');
            })
            ->editColumn('user_id',function ($data)
            {
                return $data->user->name;
            })
            ->editColumn('created_at',function ($data)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $data->created_at)->format('d/M/Y H:i:s');
            })
            ->addColumn('action', function($data) 
            {
                if($data->qty_allocated == 0)
                {
                    return view('fabric_material_stock._action', [
                        'model' => $data,
                        'delete' => route('fabricMaterialStock.delete',$data->id),
                        'print' => route('fabricMaterialStock.reprintBarcode',$data->id),
                    ]);
                }else
                {
                    return view('fabric_material_stock._action', [
                        'model' => $data,
                        'print' => route('fabricMaterialStock.reprintBarcode',$data->id),
                    ]);
                }
            })
            ->make(true);
        }
    }

    public function create()
    {
        $mapping_stocks = MappingStocks::select('type_stock_erp_code','type_stock','document_number')->get();
        return view('fabric_material_stock.create',compact('mapping_stocks'));
    }

    public function store(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'materials' => 'required|not_in:[]'
        ]);

         if($validator->passes())
         {
            $items          = json_decode($request->materials);
            $warehouse_id   = $request->warehouse_id;
           
            if(Session::has('flag')) Session::forget('flag');

            $return_print           = '';
            $summary_stock_fabrics  = array();

            $area_receive_fabric = Locator::whereHas('area',function ($query) use ($warehouse_id)
            {
                $query->where('name','RECEIVING')
                ->where('warehouse',$warehouse_id);
            })
            ->first();

            $approval_date          = carbon::now()->toDateTimeString();
            $list_approval_ids       = array();
            foreach ($items as $key => $item) 
            {
                $is_error                       = $item->is_error;

                if(!$is_error)
                {
                    $item_id                        = $item->item_id;
                    $item_desc                      = $item->item_desc;
                    $item_code                      = $item->item_code;
                    $color                          = $item->color;
                    $upc                            = $item->upc;
                    $category                       = $item->category;
                    $uom                            = $item->uom;
                    $type_stock                     = $item->type_stock;
                    $type_stock_erp_code            = $item->type_stock_erp_code;
                    $mapping_stock_id               = $item->mapping_stock_id;
                    
                    $supplier_code                  = trim(strtoupper($item->supplier_code));
                    $document_no                    = trim(strtoupper($item->document_no));
                    $supplier_name                  = trim(strtoupper($item->supplier_name));
                    $source                         = trim(strtoupper($item->source));
                    $c_order_id                     = $item->c_order_id;
                    $c_bpartner_id                  = ($c_order_id == 'FREE STOCK' ? 'FREE STOCK' : $item->c_bpartner_id);
                    $roll_number                    = $item->roll_number;
                    $batch_number                   = ($item->batch_number)? $item->batch_number : null;
                    $actual_width                   = ($item->actual_width)? $item->actual_width : null;
                    $actual_lot                     = ($item->actual_lot)? $item->actual_lot : null;
                    $qty                            = sprintf('%0.8f',$item->qty);

                    $get_barcode                    = $this->randomCode();
                    $barcode                        = $get_barcode->barcode;
                    $referral_code                  = $get_barcode->referral_code;
                    $sequence                       = $get_barcode->sequence;

                    try 
                    {
                        DB::beginTransaction();
                        
                        $material_stock = MaterialStock::FirstOrCreate([
                            'locator_id'            => $area_receive_fabric->id,
                            'barcode_supplier'      => $barcode,
                            'referral_code'         => $referral_code,
                            'sequence'              => $sequence,
                            'document_no'           => $document_no,
                            'supplier_code'         => $supplier_code,
                            'supplier_name'         => $supplier_name,
                            'c_bpartner_id'         => $c_bpartner_id,
                            'c_order_id'            => $c_order_id,
                            'item_id'               => $item_id,
                            'item_code'             => $item_code,
                            'item_desc'             => $item_desc,
                            'color'                 => $color,
                            'upc_item'              => $upc,
                            'category'              => $category,
                            'type_po'               => 1,
                            'warehouse_id'          => $warehouse_id,
                            'uom'                   => $uom,
                            'qty_carton'            => 1,
                            'stock'                 => $qty,
                            'nomor_roll'            => $roll_number,
                            'batch_number'          => $batch_number,
                            'begin_width'           => $actual_width,
                            'middle_width'          => $actual_width,
                            'end_width'             => $actual_width,
                            'actual_width'          => $actual_width,
                            'load_actual'           => $actual_lot,
                            'reserved_qty'          => 0,
                            'available_qty'         => $qty,
                            'qty_order'             => $qty,
                            'qty_arrival'           => $qty,
                            'is_active'             => true,
                            'is_material_others'    => true,
                            //'approval_date'         => $approval_date,
                            //'approval_user_id'      => Auth::user()->id,
                            'source'                => $source,
                            'user_id'               => Auth::user()->id,
                            'type_stock'            => $type_stock,
                            'mapping_stock_id'      => $mapping_stock_id,
                            'type_stock_erp_code'   => $type_stock_erp_code,
                            'created_at'            => $approval_date,
                            'updated_at'            => $approval_date,

                        ]);

                        DetailMaterialStock::FirstOrCreate([
                            'material_stock_id'     => $material_stock->id,
                            'remark'                => 'DI DAPATKAN DARI UPLOAD STOCK',
                            'uom'                   => $material_stock->uom,
                            'qty'                   => $material_stock->available_qty,
                            'user_id'               => $material_stock->user_id,
                            'created_at'            => $material_stock->created_at,
                            'updated_at'            => $material_stock->updated_at,
                        ]);
    
                        MovementStockHistory::FirstOrCreate([
                            'material_stock_id' => $material_stock->id,
                            'from_location'     => $material_stock->locator_id,
                            'to_destination'    => $material_stock->locator_id,
                            'uom'               => $material_stock->uom,
                            'qty'               => $material_stock->available_qty,
                            'movement_date'     => $material_stock->created_at,
                            'status'            => 'UPLOAD STOCK',
                            'user_id'           => $material_stock->user_id
                        ]);

                        $history_material_stock_opname = HistoryMaterialStockOpname::create([
                            'material_stock_id'     => $material_stock->id,
                            'locator_old_id'        => $material_stock->locator_id,
                            'locator_new_id'        => $material_stock->locator_id,
                            'available_stock_old'   => '0',
                            'available_stock_new'   => $material_stock->available_qty,
                            'operator'              => $material_stock->available_qty,
                            'source'                => 'new',
                            'note'                  => $material_stock->source,
                            'sto_note'              => $material_stock->source,
                            'sto_date'              => $material_stock->created_at,
                            'user_id'               => $material_stock->user_id,
                        ]);

                        $list_approval_ids [] = $history_material_stock_opname->id;

                        $return_print                   .= $material_stock->barcode_supplier.',';
                        $summary_stock_fabrics []        = $material_stock->id;
                        $counter_in                      = $area_receive_fabric->counter_in;
                        $area_receive_fabric->counter_in = $counter_in + 1;
                        $area_receive_fabric->save();

                        //$this->insertSummaryStockFabric($summary_stock_fabrics);
                        DB::commit();
                    } catch (Exception $e) 
                    {
                        DB::rollBack();
                        $message = $e->getMessage();
                        ErrorHandler::db($message);
                    }
                }
            }

            foreach($list_approval_ids as $key => $list_approval_id)
            {
                HistoryStock::itemApprove($list_approval_id);
            }

            
            Session::flash('flag', 'success');
            return response()->json(substr($return_print,0,-1),200);
        }else{
            return response()->json('Data not found.',422);
        }
    }

    public function delete(Request $request)
    {
        try 
        {
            DB::beginTransaction();
            
            $material_stock = MaterialStock::find($request->id);
            $material_stock->update([
                'reserved_qty'=> $material_stock->stock,
                'is_closing_balance'=> true,
                'is_allocated'=> true,
                'is_active'=> false,
                'available_qty'=> 0,
                'deleted_at'=> carbon::now(),
            ]);
            $summary_stock_id = $material_stock->summary_stock_fabric_id;
            
            $sum_summary_stock = MaterialStock::select(
                db::raw('sum(stock) as total_stock'),
                db::raw('sum(reserved_qty) as total_reserved'),
                db::raw('sum(available_qty) as total_available')
            )
            ->where([
                ['summary_stock_fabric_id',$summary_stock_id],
                ['is_closing_balance',false],
            ])
            ->first();
        
            SummaryStockFabric::where('id',$summary_stock_id)->update([
                'stock' => $sum_summary_stock->total_stock,
                'reserved_qty' => $sum_summary_stock->total_reserved,
                'available_qty' => $sum_summary_stock->total_available,
            ]);
            DB::commit();

            return response()->json(200);
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function itemPicklist(request $request)
    {
        $q              = $request->q;
       
        $lists = Item::where('category','FB')
        ->where(function($query) use($q){
            $query->where('item_code','like',"%$q%")
            ->orWhere('item_desc','like',"%$q%");
        })
        ->paginate(10);
        
        
        return view('fabric_material_stock._item_list',compact('lists'));
    }

    public function supplierPicklist(request $request)
    {
        $q      = strtoupper(trim($request->q));
        $lists  = DB::table('supplier_fabric_v')
        ->where(db::raw('upper(supplier_code)'),'like',"%$q%")
        ->orWhere(db::raw('upper(supplier_name)'),'like',"%$q%")
        ->paginate(10);
        
        return view('fabric_material_stock._supplier_list',compact('lists'));
        //return view('fabric_material_stock._document_no_list',compact('lists'));
    }

    public function export()
    {
        return Excel::create('upload_stock',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','TYPE_STOCK');
                $sheet->setCellValue('B1','SUPPLIER_CODE');
                $sheet->setCellValue('C1','PO_SUPPLIER');
                $sheet->setCellValue('D1','ITEM_CODE');
                $sheet->setCellValue('E1','BATCH_NUMBER');
                $sheet->setCellValue('F1','ROLL_NUMBER');
                $sheet->setCellValue('G1','ACTUAL_WIDTH');
                $sheet->setCellValue('H1','ACTUAL_LOT');
                $sheet->setCellValue('I1','UOM');
                $sheet->setCellValue('J1','QTY_INPUT');
                $sheet->setCellValue('K1','SOURCE');

                $sheet->setWidth('A', 15);
                $sheet->setWidth('B', 15);
                $sheet->setWidth('C', 15);
                $sheet->setWidth('D', 15);
                $sheet->setWidth('E', 15);
                $sheet->setWidth('F', 15);
                $sheet->setWidth('G', 15);
                $sheet->setWidth('H', 15);
                $sheet->setWidth('I', 15);
                $sheet->setWidth('J', 15);
                $sheet->setWidth('K', 15);

                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                    'C' => '@',
                    'D' => '@',
                    'E' => '@',
                    'F' => '@',
                    'G' => '@',
                    'H' => '@',
                    'I' => '@',
                    'K' => '@',
                ));

                for ($i=2; $i <100 ; $i++) {                
                    //TYPE STOCK
                    $objValidation = $sheet->getCell('A'.$i)->getDataValidation();
                    $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value tidak ada dalam list.');
                    $objValidation->setPromptTitle('Pilih Type Stock');
                    $objValidation->setPrompt('Silahkan input sesuai dengan list.');
                    $objValidation->setFormula1('type_stock');

                    $objValidation = $sheet->getCell('K'.$i)->getDataValidation();
                    $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value tidak ada dalam list.');
                    $objValidation->setPromptTitle('Pilih Type Source');
                    $objValidation->setPrompt('Silahkan input sesuai dengan list.');
                    $objValidation->setFormula1('type_source');

                }

            });

            $excel->sheet('list',function($sheet)
            {
                $sheet->SetCellValue("A1", "TYPE STOCK");
                $sheet->SetCellValue("A2", "");
                $sheet->SetCellValue("A3", "SLT");
                $sheet->SetCellValue("A4", "REGULER");
                $sheet->SetCellValue("A5", "PR/SR");
                $sheet->SetCellValue("A6", "MTFC");

                $sheet->SetCellValue("K1", "SOURCE");
                $sheet->SetCellValue("K2", "");
                $sheet->SetCellValue("K3", "SAVING");
                $sheet->SetCellValue("K4", "PLUS ROLL");
                $sheet->SetCellValue("K5", "RETURN");

                $variantsSheet = $sheet->_parent->getSheet(1); // Variants Sheet , "0" is the Index of Variants Sheet

                $sheet->_parent->addNamedRange(
                  new \PHPExcel_NamedRange(
                     'type_stock', $variantsSheet, 'A2:A6' // You can also replace 'A2:A7' with $variantsSheet ->calculateWorksheetDimension()
                  )
               );

               $sheet->_parent->addNamedRange(
                new \PHPExcel_NamedRange(
                   'type_source', $variantsSheet, 'K2:K5' // You can also replace 'A2:A7' with $variantsSheet ->calculateWorksheetDimension()
                )
             );
            });
           
            $excel->setActiveSheetIndex(0);
          
            
            
        })->export('xlsx');
    }

    public function import(Request $request)
    {
        $array = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);
          
            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();
            
            if(!empty($data) && $data->count())
            {
                foreach ($data as $key => $value) 
                {

                    $is_error       = false;
                    $remark         = '';
                    $_type_stock    = trim(strtoupper($value->type_stock));
                    $_document_no   = (trim(strtoupper($value->po_supplier)) ? trim(strtoupper($value->po_supplier)) : 'FREE STOCK') ;
                    $_supplier_code = trim(strtoupper($value->supplier_code));
                    $_item_code     = trim(strtoupper($value->item_code));
                    $_batch_number  = trim(strtoupper($value->batch_number));
                    $_roll_number   = trim(strtoupper($value->roll_number));
                    $_actual_width  = (trim(strtoupper($value->actual_width)) ? (is_numeric(trim(strtoupper($value->actual_width))) ? trim(strtoupper($value->actual_width)) : -1 ): null);
                    $_actual_lot    = trim(strtoupper($value->actual_lot));
                    $_uom           = trim(strtoupper($value->uom));
                    $_qty_input     = (is_numeric($value->qty_input) ? sprintf('%0.8f',$value->qty_input) : -1 );
                    $_source        = trim(strtoupper($value->source));

                    //dd($_document_no);

                    if($_document_no)
                    {
                        if($_item_code)
                        {
                            if($_qty_input)
                            {
                                if($_document_no == 'FREE STOCK' && $_supplier_code == 'FREE STOCK')
                                {
                                    $document_no    = 'FREE STOCK';
                                    $c_order_id     = 'FREE STOCK';
                                    $c_bpartner_id  = 'FREE STOCK';
                                    $supplier_name  = 'FREE STOCK';
                                    $supplier_code  = 'FREE STOCK';
                                }else
                                {
                                    if($_document_no == 'FREE STOCK' && $_supplier_code != 'FREE STOCK')
                                    {
                                        $po_supplier =  DB::table('supplier_fabric_v')->where('supplier_code',$_supplier_code)->first();
                                        $c_order_id     = 'FREE STOCK';
                                        $document_no    = 'FREE STOCK';
                                        $c_bpartner_id  = ($po_supplier) ? $po_supplier->c_bpartner_id: null;
                                        $supplier_name  = ($po_supplier) ? $po_supplier->supplier_name : null;
                                        $supplier_code  = ($po_supplier) ? $po_supplier->supplier_code : null;
                                    }else if($_document_no != 'FREE STOCK' && $_supplier_code == '')
                                    {
                                        $po_supplier    =  PoSupplier::where('document_no',$_document_no)->first();
                                        $c_order_id     = ($po_supplier ? $po_supplier->c_order_id : null);
                                        $document_no    = ($po_supplier ? $po_supplier->document_no : null);
                                        $c_bpartner_id  = ($po_supplier ? $po_supplier->c_bpartner_id: null);

                                        $supplier       =  DB::table('supplier_fabric_v')->where('c_bpartner_id',$c_bpartner_id)->first();

                                        $supplier_name  = ($supplier ? $supplier->supplier_name : null );
                                        $supplier_code  = ($supplier ? $supplier->supplier_code : null );
                                    }else
                                    {
                                        $c_order_id     = null;
                                        $document_no    = null;
                                        $c_bpartner_id  = null;
                                        $supplier_name  = null;
                                        $supplier_code  = null;
                                    }
                                }

                                if($c_order_id)
                                {
                                    if($_type_stock==null || $_type_stock=='')
                                    {
                                        if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
                                        {
                                            $type_stock             = 'REGULER';
                                            $type_stock_erp_code    = '2';
                                        }else
                                        {
                                            $get_4_digit_from_beginning = substr($document_no,0, 4);
                                            $mapping_stocks             = MappingStocks::select('type_stock','type_stock_erp_code')->where('document_number',$get_4_digit_from_beginning)->first();
                                            
                                            if($mapping_stocks)
                                            {
                                                $type_stock             = $mapping_stocks->type_stock;
                                                $type_stock_erp_code    = $mapping_stocks->type_stock_erp_code;
                                            }else
                                            {
                                                $get_5_digit_from_beginning = substr($document_no,0, 5);
                                                $mapping_stocks             = MappingStocks::select('type_stock','type_stock_erp_code')->where('document_number',$get_5_digit_from_beginning)->first();
                                                if($mapping_stocks)
                                                {
                                                    $type_stock             = $mapping_stocks->type_stock;
                                                    $type_stock_erp_code    = $mapping_stocks->type_stock_erp_code;
                                                }else
                                                {
                                                    $type_stock             = null;
                                                    $type_stock_erp_code    = null;
                                                }
                                            }
                                        }
                                    }else
                                    {
                                        $type_stock          = $_type_stock;
                                        $type_stock_erp_code = $this->getTypeStockErpCode($type_stock);
                                    }


                                    if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
                                    {
                                        $item = Item::where(db::raw('upper(item_code)'),trim(strtoupper($_item_code)))->first();
                                        if($item)
                                        {
                                            $item_id    = $item->item_id;
                                            $item_code  = $item->item_code;
                                            $item_desc  = $item->item_desc;
                                            $category   = 'FB';
                                            $uom        = $_uom;
                                            $color      = $item->color;
                                            $upc        = $item->upc;
                                            $width      = $item->width;
                                        }else
                                        {
                                            $item_id    = null;
                                            $item_code  = trim(strtoupper($_item_code));
                                            $item_desc  = 'data not found';
                                            $category   = 'data not found';
                                            $uom        = null;
                                            $color      = null;
                                            $upc        = null;
                                            $width      = null;
                                        }
                                    }else
                                    {
                                        $item = DB::table('purchase_item_fabric_v')
                                        ->where([
                                            ['c_order_id',$c_order_id],
                                            [db::raw('upper(item_code)'),$_item_code],
                                        ])
                                        ->first();

                                        if($item)
                                        {
                                            $item_id    = $item->item_id;
                                            $item_code  = $item->item_code;
                                            $item_desc  = $item->item_desc;
                                            $category   = 'FB';
                                            $uom        = $_uom;
                                            $color      = $item->color;
                                            $upc        = $item->upc;
                                            $width      = $item->width;
                                        }else
                                        {
                                            $item_id    = null;
                                            $item_code  = trim(strtoupper($_item_code));
                                            $item_desc  = null;
                                            $category   = null;
                                            $uom        = null;
                                            $color      = null;
                                            $upc        = null;
                                            $width      = null;
                                        }
                                    }

                                    if($_actual_width == -1 || $_actual_width == '') 
                                    {
                                        if($width != '' || $width) $_actual_width = $width;
                                        else $_actual_width = $_actual_width;
                                    }

                                    if($item_id)
                                    {
                                        if($_actual_width != -1)
                                        {
                                            if($_qty_input != -1)
                                            {
                                                $obj = new stdClass();
                                                $obj->c_bpartner_id         = $c_bpartner_id;
                                                $obj->c_order_id            = $c_order_id;
                                                $obj->supplier_code         = $supplier_code;
                                                $obj->document_no           = $document_no;
                                                $obj->supplier_name         = $supplier_name;
                                                $obj->batch_number          = $_batch_number;
                                                $obj->roll_number           = $_roll_number;
                                                $obj->actual_width          = $_actual_width;
                                                $obj->actual_lot            = $_actual_lot;
                                                $obj->item_id               = $item_id;
                                                $obj->item_code             = $item_code;
                                                $obj->item_desc             = $item_desc;
                                                $obj->category              = $category;
                                                $obj->upc                   = $upc;
                                                $obj->color                 = $color;
                                                $obj->uom                   = $uom;
                                                $obj->qty                   = $_qty_input;
                                                $obj->source                = $_source;
                                                $obj->type_stock            = $type_stock;
                                                $obj->is_error              = false;
                                                $obj->is_warning            = false;
                                                $obj->remark                = 'ready to save';
                                                $obj->mapping_stock_id      = null;
                                                $obj->type_stock_erp_code   = $type_stock_erp_code;
                                                $array [] = $obj;
                                            }else
                                            {
                                                $obj = new stdClass();
                                                $obj->c_bpartner_id         = null;
                                                $obj->c_order_id            = null;
                                                $obj->supplie_code          = null;
                                                $obj->document_no           = $_document_no;
                                                $obj->supplier_name         = null;
                                                $obj->batch_number          = $_batch_number;
                                                $obj->roll_number           = $_roll_number;
                                                $obj->actual_width          = $_actual_width;
                                                $obj->actual_lot            = $_actual_lot;
                                                $obj->item_id               = null;
                                                $obj->item_code             = $_item_code;
                                                $obj->item_desc             = null;
                                                $obj->category              = null;
                                                $obj->upc                   = null;
                                                $obj->color                 = null;
                                                $obj->uom                   = $_uom;
                                                $obj->qty                   = $_qty_input;
                                                $obj->source                = $_source;
                                                $obj->type_stock            = $_type_stock;
                                                $obj->is_error              = true;
                                                $obj->remark                = 'FORMAT WIDTH HARUS ANGKA';
                                                $obj->mapping_stock_id      = null;
                                                $obj->type_stock_erp_code   = null;
                                                $array [] = $obj;
                                            }
                                        }else
                                        {
                                            $obj = new stdClass();
                                            $obj->c_bpartner_id         = null;
                                            $obj->c_order_id            = null;
                                            $obj->supplie_code          = null;
                                            $obj->document_no           = $_document_no;
                                            $obj->supplier_name         = null;
                                            $obj->batch_number          = $_batch_number;
                                            $obj->roll_number           = $_roll_number;
                                            $obj->actual_width          = $_actual_width;
                                            $obj->actual_lot            = $_actual_lot;
                                            $obj->item_id               = null;
                                            $obj->item_code             = $_item_code;
                                            $obj->item_desc             = null;
                                            $obj->category              = null;
                                            $obj->upc                   = null;
                                            $obj->color                 = null;
                                            $obj->uom                   = $_uom;
                                            $obj->qty                   = $_qty_input;
                                            $obj->source                = $_source;
                                            $obj->type_stock            = $_type_stock;
                                            $obj->is_error              = true;
                                            $obj->remark                = 'FORMAT WIDTH HARUS ANGKA';
                                            $obj->mapping_stock_id      = null;
                                            $obj->type_stock_erp_code   = null;
                                            $array [] = $obj;
                                        }
                                    }else
                                    {
                                        $obj = new stdClass();
                                        $obj->c_bpartner_id         = null;
                                        $obj->c_order_id            = null;
                                        $obj->supplie_code          = null;
                                        $obj->document_no           = $_document_no;
                                        $obj->supplier_name         = null;
                                        $obj->batch_number          = $_batch_number;
                                        $obj->roll_number           = $_roll_number;
                                        $obj->actual_width          = $_actual_width;
                                        $obj->actual_lot            = $_actual_lot;
                                        $obj->item_id               = null;
                                        $obj->item_code             = $_item_code;
                                        $obj->item_desc             = null;
                                        $obj->category              = null;
                                        $obj->upc                   = null;
                                        $obj->color                 = null;
                                        $obj->uom                   = $_uom;
                                        $obj->qty                   = $_qty_input;
                                        $obj->source                = $_source;
                                        $obj->type_stock            = $_type_stock;
                                        $obj->is_error              = true;
                                        $obj->remark                = 'ITEM TIDAK DITEMUKAN, SILAHKAN CEK KEMBALI KODE ANDA';
                                        $obj->mapping_stock_id      = null;
                                        $obj->type_stock_erp_code   = null;
                                        $array [] = $obj;
                                    }
                                }else
                                {
                                    $obj = new stdClass();
                                    $obj->c_bpartner_id         = null;
                                    $obj->c_order_id            = null;
                                    $obj->supplie_code          = null;
                                    $obj->document_no           = $_document_no;
                                    $obj->supplier_name         = null;
                                    $obj->batch_number          = $_batch_number;
                                    $obj->roll_number           = $_roll_number;
                                    $obj->actual_width          = $_actual_width;
                                    $obj->actual_lot            = $_actual_lot;
                                    $obj->item_id               = null;
                                    $obj->item_code             = $_item_code;
                                    $obj->item_desc             = null;
                                    $obj->category              = null;
                                    $obj->upc                   = null;
                                    $obj->color                 = null;
                                    $obj->uom                   = $_uom;
                                    $obj->qty                   = $_qty_input;
                                    $obj->source                = $_source;
                                    $obj->type_stock            = $_type_stock;
                                    $obj->is_error              = true;
                                    $obj->remark                = 'PO SUPLIER TIDAK DITEMUKAN, SILAHKAN CEK KEMBALI';
                                    $obj->mapping_stock_id      = null;
                                    $obj->type_stock_erp_code   = null;
                                    $array [] = $obj;
                                }
                            }else
                            {
                                $obj = new stdClass();
                                $obj->c_bpartner_id         = null;
                                $obj->c_order_id            = null;
                                $obj->supplie_code          = null;
                                $obj->document_no           = $_document_no;
                                $obj->supplier_name         = null;
                                $obj->batch_number          = $_batch_number;
                                $obj->roll_number           = $_roll_number;
                                $obj->actual_width          = $_actual_width;
                                $obj->actual_lot            = $_actual_lot;
                                $obj->item_id               = null;
                                $obj->item_code             = $_item_code;
                                $obj->item_desc             = null;
                                $obj->category              = null;
                                $obj->upc                   = null;
                                $obj->color                 = null;
                                $obj->uom                   = $_uom;
                                $obj->qty                   = $_qty_input;
                                $obj->source                = $_source;
                                $obj->type_stock            = $_type_stock;
                                $obj->is_error              = true;
                                $obj->remark                = 'SILAHKAN MASUKAN QTY TERLEBIH DAHULU';
                                $obj->mapping_stock_id      = null;
                                $obj->type_stock_erp_code   = null;
                                $array [] = $obj;
                            }
                        }else
                        {
                            $obj = new stdClass();
                            $obj->c_bpartner_id         = null;
                            $obj->c_order_id            = null;
                            $obj->supplie_code          = null;
                            $obj->document_no           = $_document_no;
                            $obj->supplier_name         = null;
                            $obj->batch_number          = $_batch_number;
                            $obj->roll_number           = $_roll_number;
                            $obj->actual_width          = $_actual_width;
                            $obj->actual_lot            = $_actual_lot;
                            $obj->item_id               = null;
                            $obj->item_code             = $_item_code;
                            $obj->item_desc             = null;
                            $obj->category              = null;
                            $obj->upc                   = null;
                            $obj->color                 = null;
                            $obj->uom                   = $_uom;
                            $obj->qty                   = $_qty_input;
                            $obj->source                = $_source;
                            $obj->type_stock            = $_type_stock;
                            $obj->is_error              = true;
                            $obj->remark                = 'SILAHKAN MASUKAN NOMOR KODE ITEM TERLEBIH DAHULU';
                            $obj->mapping_stock_id      = null;
                            $obj->type_stock_erp_code   = null;
                            $array [] = $obj;
                        }
                    }else
                    {
                        $obj = new stdClass();
                        $obj->c_bpartner_id         = null;
                        $obj->c_order_id            = null;
                        $obj->supplie_code          = null;
                        $obj->document_no           = $_document_no;
                        $obj->supplier_name         = null;
                        $obj->batch_number          = $_batch_number;
                        $obj->roll_number           = $_roll_number;
                        $obj->actual_width          = $_actual_width;
                        $obj->actual_lot            = $_actual_lot;
                        $obj->item_id               = null;
                        $obj->item_code             = $_item_code;
                        $obj->item_desc             = null;
                        $obj->category              = null;
                        $obj->upc                   = null;
                        $obj->color                 = null;
                        $obj->uom                   = $_uom;
                        $obj->qty                   = $_qty_input;
                        $obj->source                = $_source;
                        $obj->type_stock            = $_type_stock;
                        $obj->is_error              = true;
                        $obj->remark                = 'SILAHKAN MASUKAN NOMOR PO SUPPLIER TERLEBIH DAHULU';
                        $obj->mapping_stock_id      = null;
                        $obj->type_stock_erp_code   = null;
                        $array [] = $obj;
                    }

                    
                }
                return response()->json($array,200);
            }else{
                  return response()->json('Import failed, please check again',422);
            }

            
        }
    }

    public function exportFormSplitRoll()
    {
        return Excel::create('form_split_roll',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','PO_SUPPLIER');
                $sheet->setCellValue('B1','ITEM_CODE');
                $sheet->setCellValue('C1','UOM');
                $sheet->setCellValue('D1','TOTAL_ROLL');
                $sheet->setCellValue('E1','TOTAL_YARD');
            
                $sheet->setWidth('A', 15);
                $sheet->setWidth('B', 15);
                $sheet->setWidth('C', 15);
                $sheet->setWidth('D', 15);
                $sheet->setWidth('E', 15);
                $sheet->setWidth('F', 15);

                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                    'C' => '@',
                    'D' => '@',
                    'E' => '@'
                ));
            });
           
            $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }

    public function importFormSplitRoll(Request $request)
    {
        $array = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);
          
            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();
            
            foreach ($data as $key => $value) 
            {
                $document_no    = strtoupper(trim($value->po_supplier));
                $item_code      = strtoupper(trim($value->item_code));
                $uom            = strtoupper(trim($value->uom));
                $total_roll     = $value->total_roll;
                $total_yard     = sprintf('%0.8f',$value->total_yard);
                $qty_per_roll   = sprintf('%0.8f',$total_yard/$total_roll);

                for ($i=1; $i <= $total_roll; $i++) 
                { 
                    $obj                = new stdClass();
                    $obj->document_no   = $document_no;
                    $obj->item_code     = $item_code;
                    $obj->nomor_roll    = $i;
                    $obj->uom           = $uom;
                    $obj->qty_per_roll  = $qty_per_roll;
                    $array [] = $obj;
                }
            }

            return Excel::create('upload_stock',function ($excel) use($array)
            {
                $excel->sheet('active', function($sheet) use($array)
                {
                    $sheet->setCellValue('A1','TYPE_STOCK');
                    $sheet->setCellValue('B1','SUPPLIER_CODE');
                    $sheet->setCellValue('C1','PO_SUPPLIER');
                    $sheet->setCellValue('D1','ITEM_CODE');
                    $sheet->setCellValue('E1','BATCH_NUMBER');
                    $sheet->setCellValue('F1','ROLL_NUMBER');
                    $sheet->setCellValue('G1','ACTUAL_WIDTH');
                    $sheet->setCellValue('H1','ACTUAL_LOT');
                    $sheet->setCellValue('I1','UOM');
                    $sheet->setCellValue('J1','QTY_INPUT');
                    $sheet->setCellValue('K1','SOURCE');
    
                    $sheet->setWidth('A', 15);
                    $sheet->setWidth('B', 15);
                    $sheet->setWidth('C', 15);
                    $sheet->setWidth('D', 15);
                    $sheet->setWidth('E', 15);
                    $sheet->setWidth('F', 15);
                    $sheet->setWidth('G', 15);
                    $sheet->setWidth('H', 15);
                    $sheet->setWidth('I', 15);
                    $sheet->setWidth('J', 15);
                    $sheet->setWidth('K', 15);
    
                    $row=2;
                    foreach ($array as $key => $value) 
                    {
                        $sheet->setCellValue('C'.$row,$value->document_no);
                        $sheet->setCellValue('D'.$row,$value->item_code);
                        $sheet->setCellValue('F'.$row,$value->nomor_roll);
                        $sheet->setCellValue('I'.$row,$value->uom);
                        $sheet->setCellValue('J'.$row,$value->qty_per_roll);
                        $row++;
                    }

                    $sheet->setColumnFormat(array(
                        'A' => '@',
                        'B' => '@',
                        'C' => '@',
                        'D' => '@',
                        'E' => '@',
                        'F' => '@',
                        'G' => '@',
                        'H' => '@',
                        'I' => '@',
                        'K' => '@',
                    ));
    
                    for ($i=2; $i <count($array) ; $i++) 
                    {                
                        //TYPE STOCK
                        $objValidation = $sheet->getCell('A'.$i)->getDataValidation();
                        $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                        $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                        $objValidation->setAllowBlank(false);
                        $objValidation->setShowInputMessage(true);
                        $objValidation->setShowErrorMessage(true);
                        $objValidation->setShowDropDown(true);
                        $objValidation->setErrorTitle('Input error');
                        $objValidation->setError('Value tidak ada dalam list.');
                        $objValidation->setPromptTitle('Pilih Type Stock');
                        $objValidation->setPrompt('Silahkan input sesuai dengan list.');
                        $objValidation->setFormula1('type_stock');
                    }
                });
    
    
                $excel->sheet('list',function($sheet) use($array)
                {
                    $sheet->SetCellValue("A1", "TYPE STOCK");
                    $sheet->SetCellValue("A2", "");
                    $sheet->SetCellValue("A3", "SLT");
                    $sheet->SetCellValue("A4", "REGULER");
                    $sheet->SetCellValue("A5", "PR/SR");
                    $sheet->SetCellValue("A6", "MTFC");
                    $variantsSheet = $sheet->_parent->getSheet(1); // Variants Sheet , "0" is the Index of Variants Sheet
    
                    $sheet->_parent->addNamedRange(
                      new \PHPExcel_NamedRange(
                         'type_stock', $variantsSheet, 'A2:A6' // You can also replace 'A2:A7' with $variantsSheet ->calculateWorksheetDimension()
                      )
                   );
                });
               
                $excel->setActiveSheetIndex(0);
              
                
                
            })->export('xlsx');
        }
    }

    public function showBarcode(Request $request)
    {
        $id         = trim($request->id,'"');
        $splits_id  =  explode(',',$id);
        $array      = array();
        foreach ($splits_id as $key => $value) {
            $array [] = $value;
        }

        $material_stocks = MaterialStock::whereIn('barcode_supplier',$array)->where('is_material_others',true)->get();

        return view('fabric_material_stock.barcode',compact('material_stocks'));
   
    }

    public function reprintBarcode(Request $request)
    {
        $material_stocks = MaterialStock::where('id',$request->id)->get();
        return view('fabric_material_stock.barcode',compact('material_stocks'));
    }

    static function insertSummaryStockFabric($summary_stock_fabrics)
    {
        $material_stocks = MaterialStock::select('document_no'
        ,'c_bpartner_id'
        ,'item_code'
        ,'item_id'
        ,'uom'
        ,db::raw('sum(stock) as stock')
        ,db::raw('sum(qty_order) as qty_order'))
        ->wherein('id',$summary_stock_fabrics)
        ->groupby('document_no','c_bpartner_id','item_code','item_id','uom')
        ->get();

        $summary_stocks = array();
        
        try 
        {
            DB::beginTransaction();
            
            foreach ($material_stocks as $key => $material_stock) 
            {
                $new_c_bpartner_id  = $material_stock->c_bpartner_id;
                $new_document_no    = $material_stock->document_no;
                $new_item_id        = $material_stock->item_id;
                $new_item_code      = $material_stock->item_code;
                $new_color          = $material_stock->color;
                $new_uom            = $material_stock->uom;
                $new_stock          = $material_stock->stock;
                $qty_order          = $material_stock->qty_order;
                $supplier_code      = $material_stock->supplier_code;
                $new_supplier_name  = $material_stock->supplier_name;
                

                if($new_document_no == 'FREE STOCK' || $new_document_no == 'FREE STOCK-CELUP')
                {
                    $mapping_stock_id       = null;
                    $type_stock_erp_code    = '2';
                    $type_stock             = 'REGULER';
                }else
                {
                    $get_4_digit_from_beginning = substr($new_document_no,0, 4);
                    $_mapping_stock_4_digt      = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                    
                    if($_mapping_stock_4_digt)
                    {
                        $mapping_stock_id       = $_mapping_stock_4_digt->id;
                        $type_stock_erp_code    = $_mapping_stock_4_digt->type_stock_erp_code;
                        $type_stock             = $_mapping_stock_4_digt->type_stock;
                    }else
                    {
                        $get_5_digit_from_beginning = substr($new_document_no,0, 5);
                        $_mapping_stock_5_digt      = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                        
                        if($_mapping_stock_5_digt)
                        {
                            $mapping_stock_id       = $_mapping_stock_5_digt->id;
                            $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                            $type_stock             = $_mapping_stock_5_digt->type_stock;
                        }else
                        {
                            $mapping_stock_id       = null;
                            $type_stock_erp_code    = null;
                            $type_stock             = null;
                        }
                        
                    }
                }

                $is_exists = SummaryStockFabric::where([
                    ['document_no',$new_document_no],
                    ['c_bpartner_id',$new_c_bpartner_id],
                    ['item_id',$new_item_id],
                    ['warehouse_id',auth::user()->warehouse],
                ])
                ->first();

                if($is_exists)
                {
                    $__summary_stock_fabric_id  = $is_exists->id;
                }else
                {
                    if($new_stock >= $qty_order) $_qty_order = $new_stock;
                    else $_qty_order = $qty_order;

                    $summary_stock_fabric = SummaryStockFabric::FirstOrCreate([
                        'c_bpartner_id'     => $new_c_bpartner_id,
                        'document_no'       => $new_document_no,
                        'supplier_code'     => $supplier_code,
                        'supplier_name'     => $new_supplier_name,
                        'item_code'         => $new_item_code,
                        'item_id'           => $new_item_id,
                        'color'             => $new_color,
                        'category_stock'    => $type_stock,
                        'uom'               => $new_uom,
                        'stock'             => $new_stock,
                        'reserved_qty'      => 0,
                        'available_qty'     => $new_stock,
                        'qty_order'         => sprintf('%0.8f',$_qty_order),
                        'warehouse_id'      => auth::user()->warehouse,
                    ]);

                    $__summary_stock_fabric_id = $summary_stock_fabric->id;
                }

                MaterialStock::where([
                    ['document_no',$new_document_no],
                    ['c_bpartner_id',$new_c_bpartner_id],
                    ['item_id',$new_item_id],
                    ['warehouse_id',auth::user()->warehouse],
                    ['is_material_others',true],
                ])
                ->wherenull('summary_stock_fabric_id')
                ->update(['summary_stock_fabric_id' => $__summary_stock_fabric_id]);

                $summary_stocks [] =  $__summary_stock_fabric_id;
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        try 
        {
            DB::beginTransaction();
            $sum_summary_stocks = MaterialStock::select(
                'summary_stock_fabric_id',
                db::raw('sum(stock) as total_stock'),
                db::raw('sum(reserved_qty) as total_reserved'),
                db::raw('sum(available_qty) as total_available')
            )
            ->whereIn('summary_stock_fabric_id',$summary_stocks)
            ->groupby('summary_stock_fabric_id')
            ->get();

            foreach ($sum_summary_stocks as $key => $sum_summary_stock) 
            {
                $summary_stock_fabric                       = SummaryStockFabric::find($sum_summary_stock->summary_stock_fabric_id);
                $qty_order                                  = $summary_stock_fabric->qty_order;
                $qty_order_reserved                         = $summary_stock_fabric->qty_order_reserved;
                $new_stock                                  = $sum_summary_stock->total_stock;
                $total_reserved                             = $sum_summary_stock->total_reserved;
                $total_available                            = $sum_summary_stock->total_available;
            
                $summary_stock_fabric->stock                = sprintf('%0.8f',$new_stock);
                $summary_stock_fabric->reserved_qty         = sprintf('%0.8f',$total_reserved);
                $summary_stock_fabric->available_qty        = sprintf('%0.8f',$total_available);
                $summary_stock_fabric->qty_order            = sprintf('%0.8f',$qty_order);
                $summary_stock_fabric->save();
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    function randomCode()
    {
        $referral_code  = '1S'.Carbon::now()->format('u');
        $sequence       = Barcode::where('referral_code',$referral_code)->max('sequence');

        if ($sequence == null)
            $sequence = 1;
        else
            $sequence += 1;

        Barcode::FirstOrCreate([
            'barcode'       => $referral_code.''.$sequence,
            'referral_code' => $referral_code,
            'sequence'      => $sequence,
        ]);
        
        $obj = new stdClass();
        $obj->barcode       = $referral_code.''.$sequence;
        $obj->referral_code = $referral_code;
        $obj->sequence      = $sequence;
        return $obj;
    }

    private function getTypeStockErpCode($type_stock)
    {
        $result = null;
        
        if($type_stock == 'SLT') $result = 1;
        else if($type_stock == 'REGULER') $result = 2;
        else if($type_stock == 'PR/SR') $result = 3;
        else if($type_stock == 'MTFC') $result = 4;
        else if($type_stock == 'NB') $result = 5;
        
        return $result;
    }

    static function insertStockAllocationMM()
    {
        $stock_fabrics = AllocationItem::select('c_order_id','document_no','item_code_source','c_bpartner_id','item_id_source','warehouse_inventory',db::raw("sum(qty_booking) as total_booking"))
        ->whereIn('auto_allocation_id',function($query){
            $query->select('barcode')
            ->from('temporaries')
            ->where('status','insert_stock_fabric')
            ->groupby('barcode');
        })
        ->groupby('c_order_id','c_bpartner_id','item_id_source','item_code_source','document_no','warehouse_inventory')
        ->get();

        try 
        {
            DB::beginTransaction();
            $approval_date          = carbon::now()->toDateTimeString();
            
            foreach ($stock_fabrics as $key => $stock_fabric) 
            {
                $document_no            = $stock_fabric->document_no;
                $item_code              = $stock_fabric->item_code_source;
                $c_order_id             = $stock_fabric->c_order_id;
                $c_bpartner_id          = $stock_fabric->c_bpartner_id;
                $item_id                = $stock_fabric->item_id_source;
                $warehouse_id           = $stock_fabric->warehouse_inventory;
                $total_booking          = sprintf('%0.8f',$stock_fabric->total_booking);

                if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
                {
                    $mapping_stocks             = null;
                }else
                {
                    $get_4_digit_from_beginning = substr($document_no,0, 4);
                    $mapping_stocks             = MappingStocks::select('type_stock','type_stock_erp_code')->where('document_number',$get_4_digit_from_beginning)->first();

                    if($mapping_stocks)
                    {
                        $get_5_digit_from_beginning = substr($document_no,0, 5);
                        $mapping_stocks             = MappingStocks::select('type_stock','type_stock_erp_code')->where('document_number',$get_4_digit_from_beginning)->first();
                    }
                }

                if($mapping_stocks != null)
                {
                    $type_stock          = $mapping_stocks->type_stock;
                    $type_stock_erp_code = $mapping_stocks->type_stock_erp_code;
                }else
                {
                    $type_stock          = 'REGULER';
                    $type_stock_erp_code = '2';
                }
                
                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();

                $check_stock_erp = DB::connection('erp')
                ->table('bw_master_free_stock') 
                ->where([
                    ['c_order_id',$c_order_id],
                    ['item_id',$item_id],
                    ['c_bpartner_id',$c_bpartner_id],
                    ['warehouse_id',$warehouse_id],
                    ['type_stock',$type_stock_erp_code],
                ])
                ->first();

                if($check_stock_erp)
                {
                    $available_stock_on_erp = sprintf('%0.8f',$check_stock_erp->qty_available);
                    
                    $is_stock_exists = MaterialStock::where([
                        ['c_order_id',$c_order_id],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['document_no',$document_no],
                        ['item_code',$item_code],
                        ['warehouse_id',$warehouse_id],
                        ['is_stock_on_the_fly',true],
                        ['is_stock_mm',true],
                        ['type_stock_erp_code',$type_stock_erp_code],
                    ])
                    ->first();

                    if($is_stock_exists)
                    {
                        $material_stock_id  = $is_stock_exists->id;
                        $stock_old          = $is_stock_exists->stock;
                        $available_qty_old  = sprintf('%0.8f',$is_stock_exists->available_qty);
                        $reserved_qty       = sprintf('%0.8f',$is_stock_exists->reserved_qty);
                        
                        if($available_qty_old != $available_stock_on_erp)
                        {
                            HistoryStock::approved($material_stock_id
                            ,$available_qty_old
                            ,$available_stock_on_erp
                            ,null
                            ,null
                            ,null
                            ,null
                            ,null
                            ,'ADJ STOCK ERP, BEDA DENGAN WMS'
                            ,$system->name
                            ,$system->id
                            ,$type_stock_erp_code
                            ,$type_stock
                            ,false
                            ,false);

                            $available_new      = sprintf('%0.8f',$available_qty_old);
                            $stock_new          = sprintf('%0.8f',$stock_old);

                        }else if ($available_qty_old != $total_booking)
                        {
                            $selisih = $available_qty_old - $total_booking;
                            if($selisih < 0)
                            {
                                $available_new      = sprintf('%0.8f',$available_qty_old + $total_booking);
                                $stock_new          = sprintf('%0.8f',$stock_old + $total_booking);
    
                                HistoryStock::approved($material_stock_id
                                ,$available_new
                                ,$available_qty_old
                                ,null
                                ,null
                                ,null
                                ,null
                                ,null
                                ,'ADJ STOCK FAB'
                                ,$system->name
                                ,$system->id
                                ,$type_stock_erp_code
                                ,$type_stock
                                ,false
                                ,false);
                            }else
                            {
                                $available_new      = sprintf('%0.8f',$available_qty_old);
                                $stock_new          = sprintf('%0.8f',$stock_old);
                            }
                        }else
                        {
                            $available_new      = sprintf('%0.8f',$available_qty_old);
                            $stock_new          = sprintf('%0.8f',$stock_old);
                        }

                        $allocations =  AllocationItem::where([
                            ['c_order_id',$c_order_id],
                            ['c_bpartner_id',$c_bpartner_id],
                            ['warehouse',$warehouse_id],
                            ['item_id_source',$item_id],
                        ])
                        ->whereIn('auto_allocation_id',function($query){
                            $query->select('barcode')
                            ->from('temporaries')
                            ->where('status','insert_stock_fabric')
                            ->groupby('barcode');
                        })
                        ->get();

                        foreach ($allocations as $key => $allocation) 
                        {
                            $auto_allocation_id             = $allocation->auto_allocation_id;
                            $user_id                        = $allocation->user_id;
                            $user_name                      = $allocation->user->name;
                            $po_buyer                       = $allocation->po_buyer;
                            $style                          = $allocation->style;
                            $article_no                     = $allocation->article_no;
                            $lc_date                        = $allocation->lc_date;
                            $qty_booking                    = sprintf('%0.8f',$allocation->qty_booking);

                            $old                            = $available_new;
                            $new                            = $available_new - $qty_booking;
                            
                            if($new <= 0) $new              = '0';
                            else $new                       = $new;

                            HistoryStock::approved($material_stock_id
                            ,$new
                            ,$old
                            ,$qty_booking
                            ,$po_buyer
                            ,$style
                            ,$article_no
                            ,$lc_date
                            ,'ALLOCATION WMS'
                            ,$user_name
                            ,$user_id
                            ,$type_stock_erp_code
                            ,$type_stock
                            ,false
                            ,true);
        
                            $available_new      -= $qty_booking;
                            $reserved_qty       += $qty_booking;

                            $allocation->material_stock_id  = $material_stock_id;
                            $allocation->save();

                            Temporary::where('barcode',$auto_allocation_id)->delete();
                        }

                        $is_stock_exists->stock         = sprintf('%0.8f',$stock_new);
                        $is_stock_exists->available_qty = sprintf('%0.8f',$available_new);
                        $is_stock_exists->reserved_qty  = sprintf('%0.8f',$reserved_qty);
                        $is_stock_exists->save();
                    }else
                    {
                        $new_stock              = sprintf('%0.8f',$total_booking + $available_stock_on_erp);

                        if($document_no == 'FREE STOCK')
                        {
                            $supplier_code = 'FREE STOCK';
                            $supplier_name = 'FREE STOCK';
                        }else
                        {   
                            $supplier = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                            $supplier_code = ($supplier ? $supplier->supplier_code : null);
                            $supplier_name = ($supplier ? $supplier->supplier_name : null);
                        }

                        $item       = Item::where('item_id',$item_id)->first();
                        $item_code  = ($item ? $item->item_code : null);
                        $item_desc  = ($item ? $item->item_desc : null);
                        
                        $material_stock = MaterialStock::FirstOrCreate([
                            'type_stock_erp_code'   => $type_stock_erp_code,
                            'type_stock'            => $type_stock,
                            'supplier_code'         => $supplier_code,
                            'supplier_name'         => $supplier_name,
                            'document_no'           => $document_no,
                            'c_bpartner_id'         => $c_bpartner_id,
                            'c_order_id'            => $c_order_id,
                            'item_id'               => $item_id,
                            'item_code'             => $item_code,
                            'item_desc'             => $item_desc,
                            'category'              => 'FB',
                            'type_po'               => 1,
                            'warehouse_id'          => $warehouse_id,
                            'uom'                   => 'YDS',
                            'qty_carton'            => 1,
                            'stock'                 => $new_stock,
                            'reserved_qty'          => 0,
                            'is_active'             => true,
                            'is_stock_on_the_fly'   => true,
                            'is_stock_mm'           => true,
                            'source'                => 'ADJ STOCK FAB',
                            'user_id'               => $system->id,
                            'created_at'            => $approval_date,
                            'updated_at'            => $approval_date,
                        ]);
    
                        $material_stock_id = $material_stock->id;

                        HistoryStock::approved($material_stock_id
                        ,$new_stock
                        ,$available_stock_on_erp
                        ,null
                        ,null
                        ,null
                        ,null
                        ,null
                        ,'ADJ STOCK FAB'
                        ,$system->name
                        ,$system->id
                        ,$type_stock_erp_code
                        ,$type_stock
                        ,false
                        ,false);

                        $allocations =  AllocationItem::where([
                            ['c_order_id',$c_order_id],
                            ['c_bpartner_id',$c_bpartner_id],
                            ['warehouse',$warehouse_id],
                            ['item_id_source',$item_id],
                        ])
                        ->whereIn('auto_allocation_id',function($query){
                            $query->select('barcode')
                            ->from('temporaries')
                            ->where('status','insert_stock_fabric')
                            ->groupby('barcode');
                        })
                        ->get();

                        $available_new  = $new_stock;
                        $reserved_qty   = 0;

                        foreach ($allocations as $key => $allocation) 
                        {
                            $auto_allocation_id             = $allocation->auto_allocation_id;
                            $user_id                        = $allocation->user_id;
                            $user_name                      = $allocation->user->name;
                            $po_buyer                       = $allocation->po_buyer;
                            $style                          = $allocation->style;
                            $article_no                     = $allocation->article_no;
                            $lc_date                        = $allocation->lc_date;
                            $qty_booking                    = sprintf('%0.8f',$allocation->qty_booking);

                            $old                            = $available_new;
                            $new                            = $available_new - $qty_booking;
                            
                            if($new <= 0) $new              = '0';
                            else $new                       = $new;

                            HistoryStock::approved($material_stock_id
                            ,$new
                            ,$old
                            ,$qty_booking
                            ,$po_buyer
                            ,$style
                            ,$article_no
                            ,$lc_date
                            ,'ALLOCATION WMS'
                            ,$user_name
                            ,$user_id
                            ,$type_stock_erp_code
                            ,$type_stock
                            ,false
                            ,true);
        
                            $available_new      -= $qty_booking;
                            $reserved_qty       += $qty_booking;

                            $allocation->material_stock_id  = $material_stock_id;
                            $allocation->save();

                            Temporary::where('barcode',$auto_allocation_id)->delete();
                        }

                        $material_stock->stock         = sprintf('%0.8f',$new_stock);
                        $material_stock->available_qty = sprintf('%0.8f',$available_new);
                        $material_stock->reserved_qty  = sprintf('%0.8f',$reserved_qty);
                        $material_stock->save();
                    }

                }else
                {
                    $is_stock_exists = MaterialStock::where([
                        ['c_order_id',$c_order_id],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['document_no',$document_no],
                        ['item_code',$item_code],
                        ['warehouse_id',$warehouse_id],
                        ['is_stock_on_the_fly',true],
                        ['is_stock_mm',true],
                        ['type_stock_erp_code',$type_stock_erp_code],
                    ])
                    ->first();

                    if($is_stock_exists)
                    {
                        $material_stock_id  = $is_stock_exists->id;
                        $stock_old          = $is_stock_exists->stock;
                        $available_qty_old  = $is_stock_exists->available_qty;
                        $reserved_qty       = sprintf('%0.8f',$is_stock_exists->reserved_qty);

                        $selisih            = $available_qty_old - $total_booking;

                        if($selisih < 0)
                        {
                            $available_new      = sprintf('%0.8f',$available_qty_old + $total_booking);
                            $stock_new          = sprintf('%0.8f',$stock_old + $total_booking);
    
                            HistoryStock::approved($material_stock_id
                            ,$available_new
                            ,$available_qty_old
                            ,null
                            ,null
                            ,null
                            ,null
                            ,null
                            ,'ADJ STOCK FAB'
                            ,$system->name
                            ,$system->id
                            ,$type_stock_erp_code
                            ,$type_stock
                            ,false
                            ,false);
                        }else
                        {
                            $available_new      = sprintf('%0.8f',$available_qty_old);
                            $stock_new          = sprintf('%0.8f',$stock_old);
                        }

                        $allocations =  AllocationItem::where([
                            ['c_order_id',$c_order_id],
                            ['c_bpartner_id',$c_bpartner_id],
                            ['warehouse',$warehouse_id],
                            ['item_id_source',$item_id],
                        ])
                        ->whereIn('auto_allocation_id',function($query){
                            $query->select('barcode')
                            ->from('temporaries')
                            ->where('status','insert_stock_fabric')
                            ->groupby('barcode');
                        })
                        ->get();

                        foreach ($allocations as $key => $allocation) 
                        {
                            $auto_allocation_id             = $allocation->auto_allocation_id;
                            $user_id                        = $allocation->user_id;
                            $user_name                      = $allocation->user->name;
                            $po_buyer                       = $allocation->po_buyer;
                            $style                          = $allocation->style;
                            $article_no                     = $allocation->article_no;
                            $lc_date                        = $allocation->lc_date;
                            $qty_booking                    = sprintf('%0.8f',$allocation->qty_booking);

                            $old                            = $available_new;
                            $new                            = $available_new - $qty_booking;
                            
                            if($new <= 0) $new              = '0';
                            else $new                       = $new;

                            HistoryStock::approved($material_stock_id
                            ,$new
                            ,$old
                            ,$qty_booking
                            ,$po_buyer
                            ,$style
                            ,$article_no
                            ,$lc_date
                            ,'ALLOCATION WMS'
                            ,$user_name
                            ,$user_id
                            ,$type_stock_erp_code
                            ,$type_stock
                            ,false
                            ,true);
        
                            $available_new      -= $qty_booking;
                            $reserved_qty       += $qty_booking;

                            $allocation->material_stock_id  = $material_stock_id;
                            $allocation->save();

                            Temporary::where('barcode',$auto_allocation_id)->delete();
                        }

                        $is_stock_exists->stock         = sprintf('%0.8f',$stock_new);
                        $is_stock_exists->available_qty = sprintf('%0.8f',$available_new);
                        $is_stock_exists->reserved_qty  = sprintf('%0.8f',$reserved_qty);
                        $is_stock_exists->save();

                    }else
                    {
                        if($document_no == 'FREE STOCK')
                        {
                            $supplier_code = 'FREE STOCK';
                            $supplier_name = 'FREE STOCK';
                        }else
                        {   
                            $supplier = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                            $supplier_code = ($supplier ? $supplier->supplier_code : null);
                            $supplier_name = ($supplier ? $supplier->supplier_name : null);
                        }

                        $item       = Item::where('item_id',$item_id)->first();
                        $item_code  = ($item ? $item->item_code : null);
                        $item_desc  = ($item ? $item->item_desc : null);
                        
                        $material_stock = MaterialStock::FirstOrCreate([
                            'type_stock_erp_code'   => $type_stock_erp_code,
                            'type_stock'            => $type_stock,
                            'supplier_code'         => $supplier_code,
                            'supplier_name'         => $supplier_name,
                            'document_no'           => $document_no,
                            'c_bpartner_id'         => $c_bpartner_id,
                            'c_order_id'            => $c_order_id,
                            'item_id'               => $item_id,
                            'item_code'             => $item_code,
                            'item_desc'             => $item_desc,
                            'category'              => 'FB',
                            'type_po'               => 1,
                            'warehouse_id'          => $warehouse_id,
                            'uom'                   => 'YDS',
                            'qty_carton'            => 1,
                            'stock'                 => $total_booking,
                            'reserved_qty'          => 0,
                            'is_active'             => true,
                            'is_stock_on_the_fly'   => true,
                            'is_stock_mm'           => true,
                            'source'                => 'ADJ STOCK FAB',
                            'user_id'               => $system->id,
                            'created_at'            => $approval_date,
                            'updated_at'            => $approval_date,
                        ]);
    
                        $material_stock_id = $material_stock->id;

                        HistoryStock::approved($material_stock_id
                        ,$total_booking
                        ,'0'
                        ,null
                        ,null
                        ,null
                        ,null
                        ,null
                        ,'ADJ STOCK FAB'
                        ,$system->name
                        ,$system->id
                        ,$type_stock_erp_code
                        ,$type_stock
                        ,false
                        ,false);

                        $allocations =  AllocationItem::where([
                            ['c_order_id',$c_order_id],
                            ['c_bpartner_id',$c_bpartner_id],
                            ['warehouse',$warehouse_id],
                            ['item_id_source',$item_id],
                        ])
                        ->whereIn('auto_allocation_id',function($query){
                            $query->select('barcode')
                            ->from('temporaries')
                            ->where('status','insert_stock_fabric')
                            ->groupby('barcode');
                        })
                        ->get();

                        $available_new  = $total_booking;
                        $reserved_qty   = 0;

                        foreach ($allocations as $key => $allocation) 
                        {
                            $auto_allocation_id             = $allocation->auto_allocation_id;
                            $user_id                        = $allocation->user_id;
                            $user_name                      = $allocation->user->name;
                            $po_buyer                       = $allocation->po_buyer;
                            $style                          = $allocation->style;
                            $article_no                     = $allocation->article_no;
                            $lc_date                        = $allocation->lc_date;
                            $qty_booking                    = sprintf('%0.8f',$allocation->qty_booking);

                            $old                            = $available_new;
                            $new                            = $available_new - $qty_booking;
                            
                            if($new <= 0) $new              = '0';
                            else $new                       = $new;

                            HistoryStock::approved($material_stock_id
                            ,$new
                            ,$old
                            ,$qty_booking
                            ,$po_buyer
                            ,$style
                            ,$article_no
                            ,$lc_date
                            ,'ALLOCATION WMS'
                            ,$user_name
                            ,$user_id
                            ,$type_stock_erp_code
                            ,$type_stock
                            ,false
                            ,true);
        
                            $available_new      -= $qty_booking;
                            $reserved_qty       += $qty_booking;

                            $allocation->material_stock_id  = $material_stock_id;
                            $allocation->save();

                            Temporary::where('barcode',$auto_allocation_id)->delete();
                        }

                        $material_stock->stock         = sprintf('%0.8f',$total_booking);
                        $material_stock->available_qty = sprintf('%0.8f',$available_new);
                        $material_stock->reserved_qty  = sprintf('%0.8f',$reserved_qty);
                        $material_stock->save();

                    }
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

    }

    static function historyStockPerLot($id
    ,$new_stock                 = null
    ,$old_stock                 = null
    ,$qty_allocation            = null
    ,$po_buyer_allocation       = null
    ,$style_allocation          = null
    ,$article_allocation        = null
    ,$lc_date_allocation        = null
    ,$allocation_source         = null
    ,$user_name                 = null
    ,$user_id                   = null
    ,$new_type_stock_erp_code   = null
    ,$new_type_stock            = null
    ,$is_integrate              = false
    ,$is_allocation             = false)
    {
        $date_now                   = carbon::now()->ToDateTimeString();
        $material_stock_per_lot     = MaterialStockPerLot::find($id);
        $c_order_id                 = $material_stock_per_lot->c_order_id;
        $c_bpartner_id              = $material_stock_per_lot->c_bpartner_id;
        $item_id                    = $material_stock_per_lot->item_id;
        $item_code                  = $material_stock_per_lot->item_code;
        $document_no                = $material_stock_per_lot->document_no;
        $item_desc                  = $material_stock_per_lot->item_desc;
        $actual_lot                 = $material_stock_per_lot->actual_lot;
        $warehouse_id               = $material_stock_per_lot->warehouse_id;
        $no_invoice                 = $material_stock_per_lot->no_invoice;
        $uom                        = $material_stock_per_lot->uom;
        $no_packing_list            = $material_stock_per_lot->no_packing_list;
        $user_id                    = ($user_id ? $user_id : $material_stock_per_lot->user_id);
        $user_name                  = ($user_name ? $user_name : $material_stock_per_lot->user->name);
        $is_stock_allocation        = ($document_no == 'FREE STOCK')? false : true;
        $supplier_name              = $material_stock_per_lot->supplier_name;
        $stock                      = sprintf('%0.8f',$material_stock_per_lot->qty_stock);
        $_new_stock                 = (strval($new_stock) != null) ? sprintf('%0.8f',$new_stock) : sprintf('%0.8f',$stock);
        $_old_stock                 = (strval($old_stock) != null) ? sprintf('%0.8f',$old_stock) : '0';
        $operator                   = sprintf('%0.8f',$_new_stock - $_old_stock);
        
        if($new_type_stock_erp_code != null)
        {
            $_type_stock_erp_code    = $new_type_stock_erp_code;
            $type_stock              = $new_type_stock;
        }else
        {
            $_type_stock_erp_code    = $material_stock_per_lot->type_stock_erp_code;
            $type_stock              = $material_stock_per_lot->type_stock;
        }
        
        if($_type_stock_erp_code == 'SLT') $type_stock_erp_code = '1';
        else if($_type_stock_erp_code == 'REGULER') $type_stock_erp_code = '2';
        else if ($_type_stock_erp_code == 'PR/SR') $type_stock_erp_code = '3';
        else if ($_type_stock_erp_code == 'MTFC') $type_stock_erp_code = '4';
        else if ($_type_stock_erp_code == 'NB') $type_stock_erp_code = '5';
        else if ($_type_stock_erp_code == 'ALLOWANCE') $type_stock_erp_code = '6';
        else $type_stock_erp_code = $_type_stock_erp_code;

        $is_header_exists = HistoryMaterialStocks::where('material_stock_per_lot_id',$id)->exists();
        $note             = ($is_header_exists ? 'MUTASI' : 'NEW');

        HistoryMaterialStocks::Create([
            'type_stock_erp_code'           => $type_stock_erp_code,
            'type_stock'                    => $type_stock,
            'material_stock_per_lot_id'     => $id,
            'actual_lot'                    => $actual_lot,
            'item_id'                       => $item_id,
            'item_code'                     => $item_code,
            'item_desc'                     => $item_desc,
            'uom'                           => $uom,
            'c_order_id'                    => $c_order_id,
            'document_no'                   => $document_no,
            'c_bpartner_id'                 => $c_bpartner_id,
            'supplier_name'                 => $supplier_name,
            'warehouse_id'                  => $warehouse_id,
            'stock_old'                     => sprintf('%0.8f',$_old_stock),
            'stock_new'                     => sprintf('%0.8f',$_new_stock),
            'source'                        => 'WMS',
            'user_id'                       => $user_id,
            'operator'                      => $operator,
            'created_at'                    => $date_now,
            'note'                          => $note,
            'is_integrate'                  => $is_integrate,
            'is_stock_allocation'           => $is_stock_allocation,
            'qty_allocation'                => sprintf('%0.8f',$qty_allocation),
            'po_buyer_allocation'           => $po_buyer_allocation,
            'style_po_buyer_allocation'     => $style_allocation,
            'article_po_buyer_allocation'   => $article_allocation,
            'lc_date_po_buyer_allocation'   => $lc_date_allocation,
        ]);
    }
   
}
