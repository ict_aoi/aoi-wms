<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\ReroutePoBuyer;
use App\Models\MaterialPreparation;

class AccessoriesBarcodeReRouteController extends Controller
{
    public function index()
    {
        return view('accessories_barcode_reroute.index');
    }

    public function poBuyerPickList(Request $request)
    {
        $po_buyer = trim(strtoupper($request->po_buyer));
        $page = 'reroute';

        $lists= ReroutePoBuyer::where('new_po_buyer','like',"%$po_buyer%")
        ->OrWhere('old_po_buyer','like',"%$po_buyer%")
        ->paginate('10');

        return view('accessories_barcode_reroute._buyer_list',compact('lists','page'));
    }

    public function itemPicklist(Request $request)
    {
        $array                  = array();
        $old_po_buyer           = $request->old_po_buyer;
        $po_buyer               = $request->po_buyer;
        $warehouse_id           = $request->warehouse_id;

        $material_preparations   = MaterialPreparation::where([
            ['po_buyer',$po_buyer],
            ['warehouse',$warehouse_id],
        ])
        ->get();

        foreach ($material_preparations as $key => $material_preparation) 
        {   
            $obj                    = new stdClass();
            $obj->id                = $material_preparation->id;
            $obj->old_po_buyer      = $old_po_buyer;
            $obj->new_po_buyer      = $material_preparation->po_buyer;
            $obj->document_no       = $material_preparation->document_no;
            $obj->item_code         = $material_preparation->item_code;
            $obj->style             = $material_preparation->style;
            $obj->article_no        = $material_preparation->article_no;
            $obj->uom_conversion    = trim($material_preparation->uom_conversion);
            $obj->qty_conversion    = $material_preparation->qty_conversion;
            $obj->checked           = true;
            $array [] = $obj;
        }


        return response()->json($array,200);
    }

    public function printout(Request $request)
    {
        $array      = array();
        $list_id    = array();
        $list_item  = json_decode($request->print_barcode);
       
        foreach ($list_item as $key => $value) 
        {
            if($value->checked == true)
            {
                $list_id [] = $value->id;
            }
        }
        $items = MaterialPreparation::leftjoin('reroute_po_buyers','reroute_po_buyers.new_po_buyer','material_preparations.po_buyer');
        if($list_id) $items = $items->whereIn('material_preparations.id',$list_id);
        
        $items = $items->orderby('po_buyer','item_code')->get();
        return view('accessories_barcode_reroute.barcode',compact('items'));
    }
}
