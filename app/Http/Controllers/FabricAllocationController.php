<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use Illuminate\Support\Facades\Session;


use App\Models\AllocationItem;


class FabricAllocationController extends Controller
{
    public function index()
    {
        $flag       = Session::get('flag');
        $active_tab = 'reguler';
        return view('fabric_allocation.index',compact('active_tab','flag'));
    }

    public function dataReguler(Request $request)
    {
        if ($request->ajax()) 
        {
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
            
            $allocations = db::table('allocation_item_fabrics_v')
            ->where('is_additional','!=',true)
            ->whereBetween('lc_date',[$start_date,$end_date])
            ->orderby('created_at','desc');

            return DataTables::of($allocations)
            ->editColumn('lc_date',function ($allocations)
            {
                if($allocations->lc_date) return  Carbon::createFromFormat('Y-m-d', $allocations->lc_date)->format('d/M/Y');
                else return null;
                
            })
            ->editColumn('created_at',function ($allocations)
            {
                if($allocations->created_at != '-') return  Carbon::createFromFormat('Y-m-d H:i:s', $allocations->created_at)->format('d/M/Y H:i:s');
                else return null;
            })
            ->editColumn('qty_booking',function ($allocations)
            {
                if($allocations->qty_booking != '-') return   number_format($allocations->qty_booking, 4, '.', ',');
                else return null;
            })
            ->editColumn('qty_need',function ($allocations)
            {
                if($allocations->qty_need != '-') return   number_format($allocations->qty_need, 4, '.', ',');
                else return null;
            })
            ->make(true);
        }
    }

    public function dataAdditional(Request $request)
    {
        if ($request->ajax()) 
        {
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
            
            $allocations = db::table('allocation_item_fabrics_v')
            ->where('is_additional', 'true')
            ->whereBetween('lc_date',[$start_date,$end_date])
            ->orderby('created_at','desc');

            return DataTables::of($allocations)
            ->editColumn('lc_date',function ($allocations)
            {
                if($allocations->lc_date) return  Carbon::createFromFormat('Y-m-d', $allocations->lc_date)->format('d/M/Y');
                else return null;
                
            })
            ->editColumn('created_at',function ($allocations)
            {
                if($allocations->created_at != '-') return  Carbon::createFromFormat('Y-m-d H:i:s', $allocations->created_at)->format('d/M/Y H:i:s');
                else return null;
            })
            ->editColumn('qty_booking',function ($allocations)
            {
                if($allocations->qty_booking != '-') return   number_format($allocations->qty_booking, 4, '.', ',');
                else return null;
            })
            ->editColumn('qty_need',function ($allocations)
            {
                if($allocations->qty_need != '-') return   number_format($allocations->qty_need, 4, '.', ',');
                else return null;
            })
            ->make(true);
        }
    }
    
    public function export()
    {
        $allocations =  db::table('allocation_item_fabrics_v')->orderby('created_at','desc')->get();
        
        $filename = 'report_allocation_item_fabric';
        $file = Config::get('storage.report') . '/' . e($filename).'.csv';

        if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';
        
        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }
}
