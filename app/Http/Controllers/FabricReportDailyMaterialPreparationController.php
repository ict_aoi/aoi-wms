<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\User;
use App\Models\Item;
use App\Models\Locator;
use App\Models\Barcode;
use App\Models\AutoAllocation;
use App\Models\Temporary;
use App\Models\PoSupplier;
use App\Models\MaterialStock;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\SummaryStockFabric;
use App\Models\MaterialPreparation;
use App\Models\MovementStockHistory;
use App\Models\MaterialMovementLine;
use App\Models\DetailPoBuyerPerRoll;
use App\Models\MaterialRequirement;
use App\Models\MaterialPlanningFabric;
use App\Models\HistoryPreparationFabric;
use App\Models\MaterialPreparationFabric;
use App\Models\MaterialRequirementPerPart;
use App\Models\DetailMaterialPreparation;
use App\Models\DetailMaterialPlanningFabric;
use App\Models\DetailMaterialPreparationFabric;
use App\Models\DetailMaterialPlanningFabricPerPart;

use App\Http\Controllers\AccessoriesMaterialReverseOutController as ReverseOut;
use App\Http\Controllers\FabricMaterialCancelPreparationController as CancelPreparation;

class FabricReportDailyMaterialPreparationController extends Controller
{
    public function index()
    {
        $active_tab = 'reguler';
        return view('fabric_report_daily_material_preparation.index',compact('active_tab'));
    }

    public function dataReguler(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id           = $request->warehouse;
            $start_date             = ($request->start_date) ? Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d') : Carbon::today()->subDays(30);
            $end_date               = ($request->end_date) ? Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d') : Carbon::now();
            
            $material_preparations = DB::select(db::raw("SELECT * FROM get_report_material_preparation_fabrics(
                '".$start_date."', '".$end_date."', '".$warehouse_id."') WHERE is_from_additional = false ;"
                ));
                    
            // $material_preparations = db::table('gn_fabric_report_daily_material_preparation_v')->where([
            //     ['warehouse_id',$warehouse_id],
            //     ['is_from_additional',false],
            // ])
            // ->whereNull('deleted_at')
            // ->whereBetween('planning_date', [$start_date, $end_date]);
            
            return DataTables::of($material_preparations)
            ->editColumn('is_piping',function ($material_preparations)
            {
                if($material_preparations->is_piping) return '<span class="label label-info">Piping</span>';
                else return '<span class="label label-default">Non Piping</span>';
            })
            ->editColumn('planning_date',function ($material_preparations)
            {
                if($material_preparations->planning_date) return Carbon::createFromFormat('Y-m-d', $material_preparations->planning_date)->format('d/M/Y'); 
                else return null;
            })
            ->editColumn('total_reserved_qty',function ($material_preparations)
            {
                return number_format($material_preparations->total_reserved_qty, 4, '.', ',');
            })
            ->editColumn('total_qty_rilex',function ($material_preparations)
            {
                return number_format($material_preparations->total_qty_rilex, 4, '.', ',');
            })
            ->editColumn('total_qty_outstanding',function ($material_preparations)
            {
                return number_format($material_preparations->total_qty_outstanding, 4, '.', ',');
            })
            ->editColumn('warehouse_id',function ($material_preparations)
            {
                if($material_preparations->warehouse_id == '1000001') return 'Warehouse fabric AOI 1';
                elseif($material_preparations->warehouse_id == '1000011') return 'Warehouse fabric AOI 2';
            })
            ->editColumn('status',function ($material_preparations)
            {
                if($material_preparations->status == false)
                {
                    return '<span class="label label-default">OPEN</span>';
                }
                else
                {
                    return '<span class="label label-info">CLOSED</span>';
                }
            })
            ->editColumn('po_buyer',function ($material_preparations)
            {
                $list_po_buyer = explode(',', $material_preparations->po_buyer);
                $po_buyer      = array_unique($list_po_buyer);
                $po_buyer      = implode(',', $po_buyer);
                return $po_buyer;
            })
            ->addColumn('action',function($material_preparations){
                return view('fabric_report_daily_material_preparation._action',[
                    'model'         => $material_preparations,
                    'history'       => route('fabricReportDailyMaterialPreparation.dataHistory',$material_preparations->id),
                    'detail'        => route('fabricReportDailyMaterialPreparation.detail',$material_preparations->id),
                    'cancel'        => route('fabricReportDailyMaterialPreparation.cancel',$material_preparations->id),
                    'change'        => route('fabricReportDailyMaterialPreparation.showChangePlan',$material_preparations->id),
                    'recalculate'   => route('fabricReportDailyMaterialPreparation.recalculate',$material_preparations->id),
                    // 'destroy'       => route('fabricReportDailyMaterialPreparation.delete',$material_preparations->id),
                    'deleteClosing' => route('fabricReportDailyMaterialPreparation.deleteClosing',$material_preparations->id),
                    'remark'        => route('fabricReportDailyMaterialPreparation.remark',$material_preparations->id),
                    'reprepare'     => route('fabricReportDailyMaterialPreparation.rePrepareRollToBuyer',$material_preparations->id),
                    'unlock'        => route('fabricReportDailyMaterialPreparation.unlock',$material_preparations->id),
                    'close'         => route('fabricReportDailyMaterialPreparation.close',$material_preparations->id)
                ]);
              })
            ->setRowAttr([
                'style' => function($material_preparations)
                {

                    if(round($material_preparations->total_qty_outstanding, 4) != 0) return  'background-color: #fab1b1;';
                    // if($material_preparations->remark_planning == 'please cancel this data due allocation is not longer exists') return  'background-color: #ffd400;';
                    if ($material_preparations->remark_planning == 'please cancel this data due allocation is not longer exists') {
                        return  'background-color: #ffd400;';
                    } else if($material_preparations->remark_planning == '') {
                        return  'background-color: #ffffff;';
                    } else {
                        return  'background-color: #80b3ff;';
                    }
                },
            ])
            ->rawColumns(['is_piping','action','style','status'])
            ->make(true);
        }
    }

    public function dataAdditional(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id           = $request->warehouse;
            $start_date             = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date               = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
            
            // $material_preparations = MaterialPreparationFabric::where([
            //     ['warehouse_id',$warehouse_id],
            //     ['is_from_additional',true],
            //     ['auto_allocation_id', '<>',null]
            // ])
            // ->whereNull('deleted_at')
            // ->whereBetween('created_at', [$start_date, $end_date])
            // ->orderBy('created_at','desc');

            $material_preparations = db::table('mna_daily_report_material_preparation')
            ->where([
                ['warehouse_id',$warehouse_id],
                ['is_from_additional',true],
                ['auto_allocation_id', '<>',null],
                ['qty_allocated', '>', 0]
            ])
            ->whereNull('deleted_at')
            ->whereBetween('created_at', [$start_date, $end_date])
            ->orderBy('created_at','desc');
            
            
            return DataTables::of($material_preparations)
            ->editColumn('is_piping',function ($material_preparations)
            {
                if($material_preparations->is_piping) return '<span class="label label-info">Piping</span>';
                else return '<span class="label label-default">Non Piping</span>';
            })
            ->editColumn('total_reserved_qty',function ($material_preparations)
            {
                return number_format($material_preparations->total_reserved_qty, 4, '.', ',');
            })
            ->editColumn('total_qty_rilex',function ($material_preparations)
            {
                return number_format($material_preparations->total_qty_rilex, 4, '.', ',');
            })
            ->editColumn('total_qty_outstanding',function ($material_preparations)
            {
                return number_format($material_preparations->total_qty_outstanding, 4, '.', ',');
            })
            ->editColumn('warehouse_id',function ($material_preparations)
            {
                if($material_preparations->warehouse_id == '1000001') return 'Warehouse fabric AOI 1';
                elseif($material_preparations->warehouse_id == '1000011') return 'Warehouse fabric AOI 2';
            })
            ->editColumn('auto_allocation_id',function ($material_preparations)
            {
               $auto_allocation = AutoAllocation::find($material_preparations->auto_allocation_id);
               if($auto_allocation)
               {
                return $auto_allocation->document_allocation_number;
               }
            })
            ->addColumn('action',function($material_preparations){
                return view('fabric_report_daily_material_preparation._action',[
                    'model'         => $material_preparations,
                    'detail'        => route('fabricReportDailyMaterialPreparation.detail',$material_preparations->id),
                    'recalculate'   => route('fabricReportDailyMaterialPreparation.recalculate',$material_preparations->id),                    
                    'destroy'       => route('fabricReportDailyMaterialPreparation.delete',$material_preparations->id),
                ]);
              })
            ->rawColumns(['is_piping','action'])
            ->make(true);
        }
    }

    public function dataBalanceMarker(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id           = $request->warehouse;
            $start_date             = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date               = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
            
            $material_preparations = MaterialPreparationFabric::where([
                ['warehouse_id',$warehouse_id],
                ['is_balance_marker',true],
            ])
            ->whereNull('deleted_at')
            ->whereBetween('created_at', [$start_date, $end_date])
            ->orderBy('created_at','desc');
            
            
            return DataTables::of($material_preparations)
            ->editColumn('is_piping',function ($material_preparations)
            {
                if($material_preparations->is_piping) return '<span class="label label-info">Piping</span>';
                else return '<span class="label label-default">Non Piping</span>';
            })
            ->editColumn('total_reserved_qty',function ($material_preparations)
            {
                return number_format($material_preparations->total_reserved_qty, 4, '.', ',');
            })
            ->editColumn('total_qty_rilex',function ($material_preparations)
            {
                return number_format($material_preparations->total_qty_rilex, 4, '.', ',');
            })
            ->editColumn('total_qty_outstanding',function ($material_preparations)
            {
                return number_format($material_preparations->total_qty_outstanding, 4, '.', ',');
            })
            ->editColumn('warehouse_id',function ($material_preparations)
            {
                if($material_preparations->warehouse_id == '1000001') return 'Warehouse fabric AOI 1';
                elseif($material_preparations->warehouse_id == '1000011') return 'Warehouse fabric AOI 2';
            })
            ->editColumn('auto_allocation_id',function ($material_preparations)
            {
               $auto_allocation = AutoAllocation::find($material_preparations->auto_allocation_id);
               if($auto_allocation)
               {
                    return $auto_allocation->document_allocation_number;
               }
            })
            ->addColumn('action',function($material_preparations){
                return view('fabric_report_daily_material_preparation._action',[
                    'model'         => $material_preparations,
                    'detail'        => route('fabricReportDailyMaterialPreparation.detail',$material_preparations->id),
                    'recalculate'   => route('fabricReportDailyMaterialPreparation.recalculate',$material_preparations->id),                    
                    'destroy'       => route('fabricReportDailyMaterialPreparation.delete',$material_preparations->id),
                ]);
              })
            ->rawColumns(['is_piping','action'])
            ->make(true);
        }
    }

    public function dataHistory(Request $request,$id)
    {
        if(request()->ajax()) 
        {
            $histories  = HistoryPreparationFabric::where('material_preparation_fabric_id',$id)
            ->orderby('created_at','desc');

            return DataTables::of($histories)
            ->editColumn('created_at',function ($histories)
            {
                if($histories->created_at) return Carbon::createFromFormat('Y-m-d H:i:s', $histories->created_at)->format('d/M/Y H:i:s'); 
                else return null;
            })
            ->make(true);
        }
    }
    
    public function detail($id)
    {
        $material_preparation = MaterialPreparationFabric::find($id);

        if($material_preparation)
        {
            $details              =  DB::table('detail_preparation_fabric_v')
            ->where('material_preparation_fabric_id',$id)
            ->orderby('created_at','asc')
            ->orderby('barcode','asc')
            ->get();

            // dd($details);
    
            return view('fabric_report_daily_material_preparation.detail',compact('material_preparation','details'));
        }else
        {
            return redirect()->action('FabricReportDailyMaterialPreparationController@index'); 
        }
        
    }

    public function dataDetail(Request $request,$id)
    {
        if(request()->ajax()) 
        {
            $detail_material_preparations = DB::table('detail_preparation_fabric_v')
            ->where('material_preparation_fabric_id',$id)
            ->orderby('created_at','asc')
            ->orderby('barcode','asc');
            
            return DataTables::of($detail_material_preparations)
            ->editColumn('created_at',function ($detail_material_preparations)
            {
                if($detail_material_preparations->created_at) return Carbon::createFromFormat('Y-m-d H:i:s', $detail_material_preparations->created_at)->format('d/M/Y H:i:s'); 
                else return null;
            })
            ->editColumn('movement_out_date',function ($detail_material_preparations)
            {
                if($detail_material_preparations->movement_out_date) return Carbon::createFromFormat('Y-m-d H:i:s', $detail_material_preparations->movement_out_date)->format('d/M/Y H:i:s'); 
                else return null;
            })
            ->editColumn('checkbox',function($detail_material_preparations){
                return view('fabric_report_daily_material_preparation._action',[
                    'model'     => $detail_material_preparations,
                    'checkbox'  => true
                ]);
           })
            ->editColumn('reserved_qty',function ($detail_material_preparations)
            {
                return number_format($detail_material_preparations->reserved_qty, 4, '.', ',');
            })
            ->addColumn('action',function($detail_material_preparations) use($id){
                return view('fabric_report_daily_material_preparation._action',[
                    'model' => $detail_material_preparations,
                    'print' => route('fabricReportDailyMaterialPreparation.printBarcode',[$id,$detail_material_preparations->id])
                ]);
            })
            ->rawColumns(['action','checkbox'])
            ->make(true);
        }
    }

    public function printBarcode($summary_id,$id)
    {
        $barcode_preparations = DetailMaterialPreparationFabric::where('id',$id)->get();

        foreach ($barcode_preparations as $key => $barcode_preparation_fabric) 
        {
           if($barcode_preparation_fabric->barcode =='BELUM DIPRINT')
           {
                $get_barcode            = $this->randomCode($barcode_preparation_fabric->materialStock->barcode_supplier);
                $barcode                = $get_barcode->barcode;
                $referral_code          = $get_barcode->referral_code;
                $sequence               = $get_barcode->sequence;
                
                $warehouse_id           = $barcode_preparation_fabric->materialStock->warehouse_id;
                $is_additional          = $barcode_preparation_fabric->materialPreparationFabric->is_from_additional;
                $planning_date          = $barcode_preparation_fabric->materialPreparationFabric->planning_date;
                $qty_booking            = $barcode_preparation_fabric->reserved_qty;
                

                $inventory  = Locator::where('rack','INVENTORY')
                ->whereHas('area',function ($query) use ($warehouse_id)
                {
                    $query->where('name','INVENTORY')
                    ->where('warehouse',$warehouse_id);
                })
                ->first();

                $relax = Locator::whereHas('area',function ($query) use ($warehouse_id)
                {
                    $query->where('name','RELAX')
                    ->where('warehouse',$warehouse_id);
                })
                ->first();

                if($is_additional == true) $note = 'DIGUNAKAN SEBANYAK '.$qty_booking.' UNTUK ADDITIONAL OLEH '.strtoupper($barcode_preparation_fabric->user->name);
                else $note = ' DIGUNAKAN SEBANYAK '.$qty_booking.' UNTUK PCD '.$planning_date.' OLEH '.strtoupper($barcode_preparation_fabric->user->name);


                MovementStockHistory::create([
                    'material_stock_id' => $barcode_preparation_fabric->material_stock_id,
                    'from_location'     => $inventory->id,
                    'to_destination'    => $relax->id,
                    'status'            => 'relax',
                    'uom'               => $barcode_preparation_fabric->uom,
                    'qty'               => $barcode_preparation_fabric->reserved_qty,
                    'movement_date'     => $barcode_preparation_fabric->created_at,
                    'note'              => $note.' ( NOMOR SERI BARCODE '.$barcode.')',
                    'user_id'           => $barcode_preparation_fabric->user_id
                ]);

                $barcode_preparation_fabric->barcode        = $barcode;
                $barcode_preparation_fabric->referral_code  = $referral_code;
                $barcode_preparation_fabric->sequence       = $sequence;
                $barcode_preparation_fabric->save();
           }
        }

        return view('fabric_report_daily_material_preparation.barcode',compact('barcode_preparations'));
    }

    public function exportAll(Request $request)
    {
        $warehouse_id       = ($request->warehouse ? $request->warehouse : auth::user()->warehouse);
        $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
        $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
        
        $_start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d') : Carbon::today()->subDays(30)->format('Y-m-d');
        $_end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d') : Carbon::now()->format('Y-m-d');
        $active_tab          = $request->active_tab_export;
        
        $warehouse_name     = ($warehouse_id == '1000001' ?'AOI-1':'AOI-2' );
        
        if($active_tab == 'reguler')
        {
            $daily_preparation  =  db::table('fabric_report_daily_material_preparation_v')->where([
                ['warehouse_id',$warehouse_id],
                ['is_from_additional', false],
            ])
            ->whereNull('deleted_at')
            ->whereBetween('planning_date', [$start_date, $end_date])
            ->orderBy('planning_date','desc')
            ->get();
            $file_name = 'MATERIAL_PREPARATION_FABRIC_REGULER'.$warehouse_name.'_FROM_'.$_start_date.'_TO_'.$_end_date;
        }
        else
        {
            $daily_preparation  =  db::table('fabric_report_daily_material_preparation_v')->where([
                ['warehouse_id',$warehouse_id],
                ['is_from_additional', true],
            ])
            ->whereNull('deleted_at')
            ->get();
            $file_name = 'MATERIAL_PREPARATION_FABRIC_ADDITIONAL'.$warehouse_name.'_FROM_'.$_start_date.'_TO_'.$_end_date;
        }

        return Excel::create($file_name,function($excel) use ($daily_preparation)
        {
            $excel->sheet('ACTIVE',function($sheet)use($daily_preparation)
            {
                $sheet->setCellValue('A1','ID');
                $sheet->setCellValue('B1','WAREHOUSE_PREPARATION');
                $sheet->setCellValue('C1','PLANNING_DATE');
                $sheet->setCellValue('D1','PIPING');
                $sheet->setCellValue('E1','PO_SUPPLIER');
                $sheet->setCellValue('F1','SUPPLIER_NAME');
                $sheet->setCellValue('G1','ITEM_CODE');
                $sheet->setCellValue('H1','ITEM_CODE_SOURCE');
                $sheet->setCellValue('I1','PO_BUYER');
                $sheet->setCellValue('J1','STYLE');
                $sheet->setCellValue('K1','ARTICLE_NO');
                $sheet->setCellValue('L1','UOM');
                $sheet->setCellValue('M1','TOTAL_QTY_PREPARED');
                $sheet->setCellValue('N1','TOTAL_QTY_OUTSTANDING');
                $sheet->setCellValue('O1','TOTAL_QTY_RELAX');
                $sheet->setCellValue('P1','LOT');
                $sheet->setCellValue('Q1','MACHINE');
                //$sheet->setCellValue('P1','SCAN_DATE');
                //$sheet->setCellValue('Q1','TOTAL_QTY_SCAN_PER_DATE');
                $sheet->setCellValue('R1','REMARK');
                $sheet->setCellValue('S1','REMARK_PLANNING');

            
            $row=2;
            foreach ($daily_preparation as $i) 
            { 
                if($i->is_piping) $piping = 'Piping';
                else  $piping = 'Non Piping';

                if($i->warehouse_id == '1000001') $warehouse_name =  'Warehouse fabric AOI 1';
                elseif($i->warehouse_id == '1000011') $warehouse_name =  'Warehouse fabric AOI 2';

                $sheet->setCellValue('A'.$row,$i->id);
                $sheet->setCellValue('B'.$row,$warehouse_name);
                $sheet->setCellValue('C'.$row,$i->planning_date);
                $sheet->setCellValue('D'.$row,$piping);
                $sheet->setCellValue('E'.$row,$i->document_no);
                $sheet->setCellValue('F'.$row,$i->supplier_name);
                $sheet->setCellValue('G'.$row,$i->item_code);
                $sheet->setCellValue('H'.$row,$i->item_code_source);
                $sheet->setCellValue('I'.$row,$i->po_buyer);
                $sheet->setCellValue('J'.$row,$i->_style);
                $sheet->setCellValue('K'.$row,$i->article_no);
                $sheet->setCellValue('L'.$row,$i->uom);
                $sheet->setCellValue('M'.$row,$i->total_reserved_qty);
                $sheet->setCellValue('N'.$row,$i->total_qty_outstanding);
                $sheet->setCellValue('O'.$row,$i->total_qty_rilex);
                $sheet->setCellValue('P'.$row,$i->actual_lot);
                $sheet->setCellValue('Q'.$row,$i->machine);
                //$sheet->setCellValue('P'.$row,$i->scan_date);
                //$sheet->setCellValue('Q'.$row,$i->total_qty_scan_per_date);
                $sheet->setCellValue('R'.$row,$i->remark);
                $sheet->setCellValue('S'.$row,$i->remark_planning);
                $row++;
            }
            });

        })
        ->export('xlsx');
    }

    public function exportDetail(Request $request,$id)
    {
        $material_preparation           = MaterialPreparationFabric::find($id);
        $detail_material_preparations   = DB::table('detail_preparation_fabric_v')->where('material_preparation_fabric_id',$id)->get();
    
    
        $file_name = 'material_preparation_fabric_detail';
        return Excel::create($file_name,function($excel) use ($detail_material_preparations,$material_preparation)
        {
            $excel->sheet('active',function($sheet)use($detail_material_preparations,$material_preparation){
                //Header
                $sheet->setCellValue('A1','PLANNING DATE');
                $sheet->setCellValue('A2','SUPPLIER NAME');
                $sheet->setCellValue('B1',$material_preparation->planning_date);
                $sheet->setCellValue('B2',$material_preparation->supplier_name);

                $sheet->setCellValue('D1','PO SUPPLIER');
                $sheet->setCellValue('D2','ITEM CODE');
                $sheet->setCellValue('E1',$material_preparation->document_no);
                $sheet->setCellValue('E2',$material_preparation->item_code);

                $sheet->setCellValue('G1','TOTAL QTY RELAX');
                $sheet->setCellValue('G2','TOTAL QTY OUTSTANDING');
                $sheet->setCellValue('H1',$material_preparation->total_qty_rilex);
                $sheet->setCellValue('H2',$material_preparation->total_qty_outstanding);
                
                $row=5;
                $sheet->setCellValue('A'.$row,'TANGGAL PREPARE');
                $sheet->setCellValue('B'.$row,'MOVEMENT OUT DATE');
                $sheet->setCellValue('C'.$row,'NOMOR ROLL');
                $sheet->setCellValue('D'.$row,'BATCH NUMBER');
                $sheet->setCellValue('E'.$row,'ACTUAL LOT');
                $sheet->setCellValue('F'.$row,'BEGIN WIDTH');
                $sheet->setCellValue('G'.$row,'MIDDLE WIDTH');
                $sheet->setCellValue('H'.$row,'END WIDTH');
                $sheet->setCellValue('I'.$row,'ACTUAL WIDTH');
                $sheet->setCellValue('J'.$row,'ACTUAL LENGTH');
                $sheet->setCellValue('K'.$row,'RESERVED QTY');
                $sheet->setCellValue('L'.$row,'BARCODE');

                $row+=1;
                foreach ($detail_material_preparations as $i) 
                {  
                    $sheet->setCellValue('A'.$row,$i->created_at);
                    $sheet->setCellValue('B'.$row,$i->movement_out_date);
                    $sheet->setCellValue('C'.$row,$i->nomor_roll);
                    $sheet->setCellValue('D'.$row,$i->batch_number);
                    $sheet->setCellValue('E'.$row,$i->load_actual);
                    $sheet->setCellValue('F'.$row,$i->begin_width);
                    $sheet->setCellValue('G'.$row,$i->middle_width);
                    $sheet->setCellValue('H'.$row,$i->end_width);
                    $sheet->setCellValue('I'.$row,$i->actual_width);
                    $sheet->setCellValue('J'.$row,$i->actual_length);
                    $sheet->setCellValue('K'.$row,$i->reserved_qty);
                    $sheet->setCellValue('L'.$row,$i->barcode);

                    $row++;
                }
            });

        })->export('xlsx');
    }

    public function cancel(Request $request,$id)
    {
        $material_preparation_fabric            = MaterialPreparationFabric::find($id);
        $warehouse_id                           = $material_preparation_fabric->warehouse_id;
        $detail_material_preparation_fabrics    = $material_preparation_fabric->detailMaterialPreparationFabric()->get();
        $array                                  = array();
        $movement_date                          = Carbon::now()->toDateTimeString();
       
        foreach ($detail_material_preparation_fabrics as $key => $detail_material_preparation_fabric) 
        {
            if(!$detail_material_preparation_fabric->is_closing)
            {
                $detail_material_preparation_fabric_id  = $detail_material_preparation_fabric->id;
                $material_stock_id                      = $detail_material_preparation_fabric->material_stock_id;
                $material_preparation_fabric_id         = $detail_material_preparation_fabric->material_preparation_fabric_id;
                $summary_stock_fabric_id                = $detail_material_preparation_fabric->materialStock->summary_stock_fabric_id;
                $document_no                            = strtoupper($detail_material_preparation_fabric->materialStock->document_no);
                $batch_number                           = $detail_material_preparation_fabric->materialStock->batch_number;
                $barcode                                = $detail_material_preparation_fabric->barcode;
                $reserved_qty                           = $detail_material_preparation_fabric->reserved_qty;
                $warehouse_id                           = $detail_material_preparation_fabric->warehouse_id;
                $nomor_roll                             = $detail_material_preparation_fabric->materialStock->nomor_roll;
                $item_code                              = strtoupper($detail_material_preparation_fabric->materialStock->item_code);
                $supplier_name                          = $detail_material_preparation_fabric->materialStock->supplier_name;
                $c_bpartner_id                          = $detail_material_preparation_fabric->materialStock->c_bpartner_id;
                $color                                  = $detail_material_preparation_fabric->materialStock->color;
                $begin_width                            = $detail_material_preparation_fabric->begin_width;
                $middle_width                           = $detail_material_preparation_fabric->middle_width;
                $end_width                              = $detail_material_preparation_fabric->end_width;
                $actual_width                           = $detail_material_preparation_fabric->actual_width;
                $actual_lot                             = $detail_material_preparation_fabric->materialStock->load_actual;
                $uom                                    = trim($detail_material_preparation_fabric->uom);
                $last_locator_id                        = $detail_material_preparation_fabric->last_locator_id;
                $article_no                             = $detail_material_preparation_fabric->materialPreparationFabric->article_no;
                
                $obj                                    = new stdClass();
                $obj->id                                = $detail_material_preparation_fabric_id;
                $obj->material_stock_id                 = $material_stock_id;
                $obj->summary_stock_fabric_id           = $summary_stock_fabric_id;
                $obj->material_preparation_fabric_id    = $material_preparation_fabric_id;
                $obj->last_locator_id                   = $last_locator_id;
                $obj->barcode                           = $barcode;
                $obj->total_qty_preparation             = $reserved_qty;
                $obj->warehouse_id                      = $warehouse_id;
                $obj->nomor_roll                        = $nomor_roll;
                $obj->batch_number                      = $batch_number;
                $obj->document_no                       = $document_no;
                $obj->item_code                         = $item_code;
                $obj->supplier_name                     = $supplier_name;
                $obj->c_bpartner_id                     = $c_bpartner_id;
                $obj->article_no                        = $article_no;
                $obj->color                             = $color;
                $obj->actual_width                      = $actual_width;
                $obj->begin_width                       = $begin_width;
                $obj->middle_width                      = $middle_width;
                $obj->end_width                         = $end_width;
                $obj->actual_lot                        = $actual_lot;
                $obj->uom                               = $uom;
                $obj->movement_date                     = $movement_date;
    
                $array []  = $obj;
            }else
            {
                $detail_material_preparation_fabric->delete();
            }
            
        }
       
        CancelPreparation::doStore($warehouse_id,$array);
        return response()->json('success',200);
    }

    public function delete(Request $request,$id)
    {
        $reason                         = trim(strtolower($request->reason));
        $material_preparation_fabric    = MaterialPreparationFabric::find($id);
        $warehouse_id                   = $material_preparation_fabric->warehouse_id;
        $movement_date                  = carbon::now();
        
        if($material_preparation_fabric->is_from_additional)
        {
            try 
            {
                
            DB::beginTransaction();

            $detail_material_preparation_fabrics    = $material_preparation_fabric->detailMaterialPreparationFabric()->get();
            $array                                  = array();

            foreach ($detail_material_preparation_fabrics as $key => $detail_material_preparation_fabric) 
            {
                $detail_material_preparation_fabric_id  = $detail_material_preparation_fabric->id;
                $material_stock_id                      = $detail_material_preparation_fabric->material_stock_id;
                $material_preparation_fabric_id         = $detail_material_preparation_fabric->material_preparation_fabric_id;
                $summary_stock_fabric_id                = $detail_material_preparation_fabric->materialStock->summary_stock_fabric_id;
                $document_no                            = strtoupper($detail_material_preparation_fabric->materialStock->document_no);
                $batch_number                           = $detail_material_preparation_fabric->materialStock->batch_number;
                $barcode                                = $detail_material_preparation_fabric->barcode;
                $reserved_qty                           = $detail_material_preparation_fabric->reserved_qty;
                $warehouse_id                           = $detail_material_preparation_fabric->warehouse_id;
                $nomor_roll                             = $detail_material_preparation_fabric->materialStock->nomor_roll;
                $item_code                              = strtoupper($detail_material_preparation_fabric->materialStock->item_code);
                $supplier_name                          = $detail_material_preparation_fabric->materialStock->supplier_name;
                $c_bpartner_id                          = $detail_material_preparation_fabric->materialStock->c_bpartner_id;
                $color                                  = $detail_material_preparation_fabric->materialStock->color;
                $begin_width                            = $detail_material_preparation_fabric->begin_width;
                $middle_width                           = $detail_material_preparation_fabric->middle_width;
                $end_width                              = $detail_material_preparation_fabric->end_width;
                $actual_width                           = $detail_material_preparation_fabric->actual_width;
                $actual_lot                             = $detail_material_preparation_fabric->materialStock->load_actual;
                $uom                                    = trim($detail_material_preparation_fabric->uom);
                $last_locator_id                        = $detail_material_preparation_fabric->last_locator_id;
                $article_no                             = $detail_material_preparation_fabric->materialPreparationFabric->article_no;
                
                $obj                                    = new stdClass();
                $obj->id                                = $detail_material_preparation_fabric_id;
                $obj->material_stock_id                 = $material_stock_id;
                $obj->summary_stock_fabric_id           = $summary_stock_fabric_id;
                $obj->material_preparation_fabric_id    = $material_preparation_fabric_id;
                $obj->last_locator_id                   = $last_locator_id;
                $obj->barcode                           = $barcode;
                $obj->total_qty_preparation             = $reserved_qty;
                $obj->warehouse_id                      = $warehouse_id;
                $obj->nomor_roll                        = $nomor_roll;
                $obj->batch_number                      = $batch_number;
                $obj->document_no                       = $document_no;
                $obj->item_code                         = $item_code;
                $obj->supplier_name                     = $supplier_name;
                $obj->c_bpartner_id                     = $c_bpartner_id;
                $obj->article_no                        = $article_no;
                $obj->color                             = $color;
                $obj->actual_width                      = $actual_width;
                $obj->begin_width                       = $begin_width;
                $obj->middle_width                      = $middle_width;
                $obj->end_width                         = $end_width;
                $obj->actual_lot                        = $actual_lot;
                $obj->uom                               = $uom;
                $obj->movement_date                     = $movement_date;

                $array []  = $obj;
            }
            
            CancelPreparation::doStore($warehouse_id,$array);
    
                $material_preparation_fabric->delete_reason             = $reason;
                $material_preparation_fabric->delete_user_id            = auth::user()->id;
                $material_preparation_fabric->deleted_at                = $movement_date;
                $material_preparation_fabric->save();
                
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

        }else
        {
            if($material_preparation_fabric->total_qty_rilex)
            {
                return response()->json('This data cannot be deleted, because this data is still have roll on it',422);
            }
    
            try 
            {
                DB::beginTransaction();
    
                $material_preparation_fabric->delete_reason             = $reason;
                $material_preparation_fabric->delete_user_id            = auth::user()->id;
                $material_preparation_fabric->deleted_at                = $movement_date;
                $material_preparation_fabric->save();
                
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
       

        return response()->json('success',200);
    }

    public function deleteClosing(Request $request,$id)
    {
        try 
        {
            DB::beginTransaction();

            DetailMaterialPreparationFabric::where([
                ['material_preparation_fabric_id',$id],
                ['is_closing',true],
            ])
            ->delete();

            DB::commit();

            $this->updateTotalPreparation($id);
            $this->checkMaterialPreparationFabric($id);
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
        }
    }

    public function remark(Request $request,$id)
    {
        try 
        {
            DB::beginTransaction();

            $material_preparation_fabric            = MaterialPreparationFabric::find($id);
            $material_preparation_fabric->remark    = $material_preparation_fabric->remark. '_'.trim(strtolower($request->remark));
            $material_preparation_fabric->save();

            DB::commit();

            return response()->json('success',200);
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function recalculate(Request $request,$id)
    {
        $this->updateTotalPreparation($id);
        return response()->json(200);
    }

    public function unlock(Request $request,$id)
    {
        try 
        {
            DB::beginTransaction();

            $material_preparation_fabric                    = MaterialPreparationFabric::find($id);
            $material_preparation_fabric->preparation_date  = null;
            $material_preparation_fabric->preparation_by    = null;
            $material_preparation_fabric->save();

            DB::commit();

            return response()->json('success',200);
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function rePrepareRollToBuyer(Request $request,$id)
    {
        MaterialPreparation::whereIn('id',function($query) use($id)
        {
            $query->select('material_preparation_id')
            ->from('detail_material_preparations')
            ->whereNotNull('detail_material_preparation_fabric_id')
            ->whereIn('detail_material_preparation_fabric_id',function($query2) use ($id)
            {
                $query2->select('id')
                ->from('detail_material_preparation_fabrics')
                ->where('material_preparation_fabric_id',$id);
            })
            ->groupby('material_preparation_id');
        })
        ->delete();
        
        DetailPoBuyerPerRoll::whereIn('detail_material_preparation_fabric_id',function($query) use($id)
        {
            $query->select('id')
            ->from('detail_material_preparation_fabrics')
            ->where('material_preparation_fabric_id',$id);
        })
        ->delete();
        
        $this->insertRePrepareRollToBuyer($id,1);
    }

    public function close(Request $request, $id)
    {
        $this->doClose($id,strtoupper(trim($request->remark)),auth::user()->id);
    }

    public function getPlanningPerArticle(Request $request)
    {
        $is_piping                      = $request->is_piping;
        $material_preparation_fabric    = MaterialPreparationFabric::find($request->id);
        $item_id                        = $material_preparation_fabric->item_id_source;
        $c_order_id                     = $material_preparation_fabric->c_order_id;
        $warehouse_id                   = $material_preparation_fabric->warehouse_id;
        $planning_date                  = Carbon::createFromFormat('d/m/Y', $request->planning_date)->format('Y-m-d');
       
        $articles       = MaterialPreparationFabric::select('article_no')
        ->where([
            ['is_piping',$is_piping],
            ['item_id_source',$item_id],
            ['warehouse_id',$warehouse_id],
            ['planning_date',$planning_date],
            ['c_order_id', $c_order_id],
        ])
        ->pluck('article_no','article_no')
        ->all();

        if(count($articles) > 0)
        {
            return response()->json([
                'articles'  => $articles,
                'total'     => count($articles),
                
            ],200);
        }else
        {
            return response()->json('Planning not found',422);
        }
    }

    public function getPlanningPerStyle(Request $request)
    {
        $is_piping      = $request->is_piping;
        $article_no     = $request->article_no;
        $warehouse_id   = $request->warehouse_id;
        $planning_date  = Carbon::createFromFormat('d/m/Y', $request->planning_date)->format('Y-m-d');

        $styles         = MaterialRequirement::select('_style')
        ->whereIn('po_buyer',function($query) use($is_piping,$article_no,$planning_date,$warehouse_id)
        {
            $query->select('po_buyer')
            ->from('material_planning_fabrics')
            ->where([
                ['is_piping',$is_piping],
                ['article_no',$article_no],
                ['planning_date',$planning_date],
                ['warehouse_id',$warehouse_id],
            ])
            ->groupby('po_buyer');
        })
        ->whereNotNull('_style')
        ->pluck('_style','_style')
        ->all();

        if(count($styles) > 0)
        {
            return response()->json([
                'styles'    => $styles,
                'total'     => count($styles),
                
            ],200);
        }else
        {
            return response()->json('Planning not found',422);
        }
    }
    
    public function getPlanningPerDate(Request $request)
    {
        try 
        {
            DB::beginTransaction();

            $material_preparation_fabric    = MaterialPreparationFabric::find($request->id);
            $_is_piping                     = $request->is_piping;
            $c_order_id                     = $material_preparation_fabric->c_order_id;
            $item_id_source                 = $material_preparation_fabric->item_id_source;
            $warehouse_id                   = $material_preparation_fabric->warehouse_id;
            $style                          = $request->style;
            $article_no                     = $request->article_no;
            $planning_date                  = Carbon::createFromFormat('d/m/Y', $request->planning_date)->format('Y-m-d');
            
            if($_is_piping == 1) $is_piping = true;
            else $is_piping = false;

            $material_preparation_fabric = MaterialPreparationFabric::where([
                ['c_order_id',$c_order_id],
                ['item_id_source',$item_id_source],
                ['planning_date',$planning_date],
                ['is_piping',$is_piping],
                ['article_no',$article_no],
                ['_style',$style],
                ['warehouse_id',$warehouse_id],
                ['id','!=',$request->id],
            ])
            ->whereNull('deleted_at')
            ->orderby('planning_date','asc')
            ->first();

            //dd($material_preparation_fabric);

            if($material_preparation_fabric)
            {
                $allocation = DetailMaterialPlanningFabric::select('document_no'
                    ,'c_order_id'
                    ,'_style'
                    ,'article_no'
                    ,'item_code'
                    ,'item_code_source'
                    ,'item_id_book'
                    ,'item_id_source'
                    ,'planning_date'
                    ,'is_piping'
                    ,db::raw("sum(qty_booking) as qty_prepare"))
                ->where([
                    ['c_order_id',$c_order_id],
                    ['item_id_source',$item_id_source],
                    ['planning_date',$planning_date],
                    ['is_piping',$is_piping],
                    ['_style',$style],
                    ['article_no',$article_no],
                    ['warehouse_id',$warehouse_id],
                ])
                ->groupby('document_no'
                    ,'c_order_id'
                    ,'_style'
                    ,'article_no'
                    ,'item_code'
                    ,'item_code_source'
                    ,'item_id_book'
                    ,'item_id_source'
                    ,'planning_date'
                    ,'is_piping')
                ->first();

                if($allocation)
                {
                    $qty_prepare        = $allocation->qty_prepare;
                    $total_reserved_qty = sprintf('%0.8f',$material_preparation_fabric->total_reserved_qty);
                    $total_qty_relax    = sprintf('%0.8f',$material_preparation_fabric->total_qty_rilex);
                    if($qty_prepare != $total_reserved_qty)
                    {
                        $new_qty_oustanding = sprintf('%0.8f',$qty_prepare - $total_qty_relax);
                        
                        $material_preparation_fabric->total_reserved_qty    = $qty_prepare;
                        $material_preparation_fabric->total_qty_outstanding = $new_qty_oustanding;
                        $material_preparation_fabric->save();
                    }
                    
                    $update_preparation = MaterialPreparationFabric::find($material_preparation_fabric->id);
                    return response()->json($update_preparation,200);

                }else return response()->json($material_preparation_fabric,200);
            }else
            {
                $allocation = DetailMaterialPlanningFabric::select('document_no'
                    ,'c_order_id'
                    ,'_style'
                    ,'article_no'
                    ,'item_code'
                    ,'item_code_source'
                    ,'item_id_book'
                    ,'item_id_source'
                    ,'planning_date'
                    ,'c_bpartner_id'
                    ,'is_piping'
                    ,db::raw("sum(qty_booking) as qty_prepare"))
                ->where([
                    ['c_order_id',$c_order_id],
                    ['item_id_source',$item_id_source],
                    ['planning_date',$planning_date],
                    ['is_piping',$is_piping],
                    ['article_no',$article_no],
                    ['_style',$style],
                    ['warehouse_id',$warehouse_id],
                ])
                ->groupby('document_no'
                    ,'c_order_id'
                    ,'_style'
                    ,'article_no'
                    ,'item_code'
                    ,'item_code_source'
                    ,'item_id_book'
                    ,'item_id_source'
                    ,'planning_date'
                    ,'c_bpartner_id'
                    ,'is_piping')
                ->first();

                if($allocation)
                {
                    $obj                            = new stdClass();
                    $obj->id                        = null;
                    $obj->document_no               = $allocation->document_no;
                    $obj->item_code                 = $allocation->item_code;
                    $obj->article_no                = $allocation->article_no;
                    $obj->style                     = $allocation->style;
                    $obj->item_code_source          = $allocation->item_code_source;
                    $obj->item_id_book              = $allocation->item_id_book;
                    $obj->item_id_source            = $allocation->item_id_source;
                    $obj->planning_date             = $allocation->planning_date;
                    $obj->warehouse_id              = auth::user()->warehouse;
                    $obj->c_bpartner_id             = $allocation->c_bpartner_id;
                    $obj->is_piping                 = $allocation->is_piping;
                    $obj->total_reserved_qty        = $allocation->qty_prepare;
                    $obj->total_qty_rilex           = 0;
                    $obj->total_qty_outstanding     = $allocation->qty_prepare;

                }else
                {
                    return response()->json('Allocation not found',422);
                } 

                return response()->json($obj,200);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function storeChangePlan(Request $request,$id)
    {
        $return_print                   = array();
        $delete_material_movement_lines = array();
        $insert_date                    = carbon::now();

        try 
        {
            DB::beginTransaction();
            
            $new_planning_date                  = Carbon::createFromFormat('d/m/Y', $request->start_date)->format('d/M/Y');
            // old
            $material_preparation_fabric_id     = $id;
            $total_qty_rilex                    = sprintf('%0.8f',$request->total_qty_rilex);
            $total_qty_outstanding              = sprintf('%0.8f',$request->total_reserved_qty - $request->total_qty_rilex);
            $old_remark                         = '';
           
            $old                                = MaterialPreparationFabric::find($material_preparation_fabric_id);
            
            if($old)
            {
                $old_planning_date                  = $old->planning_date->format('d/M/Y');
                $old->total_qty_rilex               = $total_qty_rilex;
                $old->total_qty_outstanding         = $total_qty_outstanding;
                $old_remark = ($old->remark == NULL || '') ? '' : $old->remark ;
                
                $relax = Locator::whereHas('area',function ($query) use($old)
                {
                    $query->where('name','RELAX')
                    ->where('warehouse',$old->warehouse_id);
                })
                ->first();
    
                $inventory  = Locator::where('rack','INVENTORY') 
                ->whereHas('area',function ($query) use($old)
                {
                    $query->where('name','INVENTORY')
                    ->where('warehouse',$old->warehouse_id);
                })
                ->first();
    
                $inventory_erp = Locator::with('area')
                ->where([
                    ['is_active',true],
                    ['rack','ERP INVENTORY'],
                ])
                ->whereHas('area',function ($query) use($old)
                {
                    $query->where('is_active',true);
                    $query->where('warehouse',$old->warehouse_id);
                    $query->where('name','ERP INVENTORY');
                })
                ->first();
    
                if($total_qty_outstanding == 0.000)
                {
                    $old->relax_date            = carbon::now(); //gana
                    $old->remark_planning   = 'All moved to new plan '.$new_planning_date;

                } else {
                    $old->relax_date            = carbon::now();

                    if ($old->remark_planning == NULL || '') {
                        $old->remark_planning   = 'Partially moved to new plan '.$new_planning_date;
                    } else {
                        $old->remark_planning   = $old->remark_planning.','.$new_planning_date;
                    }
                }
    
                $old->preparation_date              = null; 
                $old->preparation_by                = null;
                $old->save();
    
                $old_materials                  = json_decode($request->list_preparation_material);
    
                foreach ($old_materials as $key => $old_material) 
                {
                    $old_id_roll    = $old_material->id;
                    $reserved_qty   = $old_material->reserved_qty;
                    $old_roll       = DetailMaterialPreparationFabric::where([
                        ['material_preparation_fabric_id',$material_preparation_fabric_id],
                        ['id',$old_id_roll],
                    ])
                    ->first();
                    
                    if($old_roll)
                    {
                        $old_last_status_movement = $old_roll->last_status_movement;
                        if(!$old_roll->is_closing)
                        {
                            if($old_roll->detailPoBuyerPerRoll()->count() == 0)
                            {
                                $movement_out_date  = $old_roll->movement_out_date;
                                $po_detail_id       = $old_roll->materialStock->po_detail_id;
                                $item_code          = $old_roll->materialStock->item_code;
                                $item_id            = $old_roll->materialStock->item_id;
                                $c_order_id         = $old_roll->materialStock->c_order_id;
                                $uom                = $old_roll->materialStock->uom;
                                $c_bpartner_id      = $old_roll->materialStock->c_bpartner_id;
                                $supplier_name      = $old_roll->materialStock->supplier_name;
                                $nomor_roll         = $old_roll->materialStock->nomor_roll;
                                $warehouse_id       = $old_roll->materialStock->warehouse_id;
                                $document_no        = $old_roll->materialStock->document_no;
                                $material_stock_id  = $old_roll->material_stock_id;
    
                                if($movement_out_date)
                                {
                                    if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                                    {
                                        $no_packing_list        = '-';
                                        $no_invoice             = '-';
                                        $c_orderline_id         = '-';
                                    }else
                                    {
                                        $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                        ->whereNull('material_roll_handover_fabric_id')
                                        ->where('po_detail_id',$po_detail_id)
                                        ->first();
                                        
                                        $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                        $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                        $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                                    }
    
                                    $is_movement_exists = MaterialMovement::where([
                                        ['from_location',$inventory_erp->id],
                                        ['to_destination',$inventory_erp->id],
                                        ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                        ['to_locator_erp_id',$inventory_erp->area->erp_id],
                                        ['is_integrate',false],
                                        ['is_active',true],
                                        ['no_packing_list',$no_packing_list],
                                        ['no_invoice',$no_invoice],
                                        ['status','integration-to-inventory-erp'],
                                    ])
                                    ->first();
                                    
                                    if(!$is_movement_exists)
                                    {
                                        $material_movement = MaterialMovement::firstOrCreate([
                                            'from_location'         => $inventory_erp->id,
                                            'to_destination'        => $inventory_erp->id,
                                            'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                            'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                            'is_integrate'          => false,
                                            'is_active'             => true,
                                            'no_packing_list'       => $no_packing_list,
                                            'no_invoice'            => $no_invoice,
                                            'status'                => 'integration-to-inventory-erp',
                                            'created_at'            => $insert_date,
                                            'updated_at'            => $insert_date,
                                        ]);
    
                                        $material_movement_id = $material_movement->id;
                                    }else
                                    {
                                        $is_movement_exists->updated_at = $insert_date;
                                        $is_movement_exists->save();
    
                                        $material_movement_id = $is_movement_exists->id;
                                    }
    
                                    $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                                    ->where([
                                        ['material_movement_id',$material_movement_id],
                                        ['material_stock_id',$material_stock_id],
                                        ['qty_movement',$old_roll->reserved_qty],
                                        ['warehouse_id',$warehouse_id],
                                        ['item_id',$item_id],
                                        ['date_movement',$insert_date],
                                        ['is_integrate',false],
                                        ['is_active',true],
                                    ])
                                    ->exists();
    
                                    if(!$is_material_movement_line_exists)
                                    {
                                        MaterialMovementLine::Create([
                                            'material_movement_id'          => $material_movement_id,
                                            'material_stock_id'             => $material_stock_id,
                                            'item_code'                     => $item_code,
                                            'item_id'                       => $item_id,
                                            'c_order_id'                    => $c_order_id,
                                            'c_orderline_id'                => $c_orderline_id,
                                            'c_bpartner_id'                 => $c_bpartner_id,
                                            'supplier_name'                 => $supplier_name,
                                            'type_po'                       => 1,
                                            'is_integrate'                  => false,
                                            'uom_movement'                  => $uom,
                                            'qty_movement'                  => $old_roll->reserved_qty,
                                            'date_movement'                 => $insert_date,
                                            'created_at'                    => $insert_date,
                                            'updated_at'                    => $insert_date,
                                            'date_receive_on_destination'   => $insert_date,
                                            'nomor_roll'                    => $nomor_roll,
                                            'warehouse_id'                  => $warehouse_id,
                                            'document_no'                   => $document_no,
                                            'note'                          => 'PROSES INTEGRASI UNTUK MOVE PLAN '.auth::user()->name,
                                            'is_active'                     => true,
                                            'user_id'                       => auth::user()->id,
                                        ]);
                                    }
                                }
                            }else
                            {
                                 // check status movement fabric
                                $material_preparations = MaterialPreparation::whereIn('id',function($query) use($old_id_roll)
                                {
                                    $query->select('material_preparation_id')
                                    ->from('detail_material_preparations')
                                    ->whereNotNull('detail_material_preparation_fabric_id')
                                    ->where('detail_material_preparation_fabric_id',$old_id_roll)
                                    ->groupby('material_preparation_id');
                                })
                                ->get();
            
                                foreach ($material_preparations as $key => $material_preparation) 
                                {
                                    if($material_preparation->last_status_movement == 'out-cutting' || $material_preparation->last_status_movement == 'out-distribution')
                                    {
                                        $last_material_movement_line_id = $material_preparation->last_material_movement_line_id;
                                        
                                        if($material_preparation->po_detail_id == 'FREE STOCK' || $material_preparation->po_detail_id == 'FREE STOCK-CELUP')
                                        {
                                            $c_order_id             = 'FREE STOCK';
                                            $no_packing_list        = '-';
                                            $no_invoice             = '-';
                                            $c_orderline_id         = '-';
                                        }else
                                        {
                                            $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                            ->whereNull('material_roll_handover_fabric_id')
                                            ->where('po_detail_id',$material_preparation->po_detail_id)
                                            ->first();

                                            $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                            $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                            $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                                        }
                                        
                                        $_item_id_source = ($material_preparation->item_id_book != $material_preparation->item_id_source ? $material_preparation->item_id_source : null);
            
                                        $is_movement_reverse_exists = MaterialMovement::where([
                                            ['from_location',$material_preparation->last_locator_id],
                                            ['to_destination',$inventory->id],
                                            ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                            ['to_locator_erp_id',$inventory_erp->area->erp_id],
                                            ['po_buyer',$material_preparation->po_buyer],
                                            ['is_integrate',false],
                                            ['is_complete_erp',false],
                                            ['is_active',true],
                                            ['no_packing_list',$no_packing_list],
                                            ['no_invoice',$no_invoice],
                                            ['status','reverse'],
                                        ])
                                        ->first();
            
                                        if($is_movement_reverse_exists)
                                        {
                                            $is_movement_reverse_exists->updated_at     = $insert_date;
                                            $is_movement_reverse_exists->save();
                                            
                                            $movement_reverse_id                        = $is_movement_reverse_exists->id;
            
                                        }else
                                        {
                                            $movement_reverse = MaterialMovement::FirstOrCreate([
                                                'from_location'             => $material_preparation->last_locator_id,
                                                'to_destination'            => $inventory->id,
                                                'from_locator_erp_id'       => $inventory_erp->area->erp_id,
                                                'to_locator_erp_id'         => $inventory_erp->area->erp_id,
                                                'po_buyer'                  => $material_preparation->po_buyer,
                                                'is_complete_erp'           => false,
                                                'is_integrate'              => false ,
                                                'is_active'                 => true,
                                                'status'                    => 'reverse',
                                                'no_packing_list'           => $no_packing_list,
                                                'no_invoice'                => $no_invoice,
                                                'created_at'                => $insert_date,
                                                'updated_at'                => $insert_date,
                                            ]);
            
                                            $movement_reverse_id = $movement_reverse->id;
                                        }
            
                                        $is_line_reverse_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                        ->where([
                                            ['material_movement_id',$movement_reverse_id],
                                            ['material_preparation_id',$material_preparation->id],
                                            ['qty_movement',$material_preparation->qty_conversion],
                                            ['item_id',$material_preparation->item_id],
                                            ['warehouse_id',$material_preparation->warehouse],
                                            ['is_integrate',false],
                                            ['is_active',true],
                                            ['date_movement',$insert_date],
                                        ]) 
                                        ->exists();
            
                                        if(!$is_line_reverse_exists)
                                        {
                                            $new_material_movement_line_reverse = MaterialMovementLine::Create([
                                                'material_movement_id'                      => $movement_reverse_id,
                                                'material_preparation_id'                   => $material_preparation->id,
                                                'item_code'                                 => $material_preparation->item_code,
                                                'item_id'                                   => $material_preparation->item_id,
                                                'c_order_id'                                => $material_preparation->c_order_id,
                                                'c_orderline_id'                            => $c_orderline_id,
                                                'c_bpartner_id'                             => $material_preparation->c_bpartner_id,
                                                'supplier_name'                             => $material_preparation->supplier_name,
                                                'type_po'                                   => 1,
                                                'is_integrate'                              => false,
                                                'is_inserted_to_material_movement_per_size' => false,
                                                'uom_movement'                              => $material_preparation->uom_conversion,
                                                'qty_movement'                              => $material_preparation->qty_conversion,
                                                'date_movement'                             => $insert_date,
                                                'created_at'                                => $insert_date,
                                                'updated_at'                                => $insert_date,
                                                'date_receive_on_destination'               => $insert_date,
                                                'nomor_roll'                                => '-',
                                                'warehouse_id'                              => $material_preparation->warehouse,
                                                'document_no'                               => $material_preparation->document_no,
                                                'note'                                      => 'MOVEMENT DI KEMBALIKAN KE INVENTORY',
                                                'is_active'                                 => true,
                                                'user_id'                                   => auth::user()->id,
                                            ]);
            
                                            $material_preparation->last_material_movement_line_id    = $new_material_movement_line_reverse->id;
                                        }
            
                                        Temporary::Create([
                                            'barcode'       => $material_preparation->po_buyer,
                                            'status'        => 'mrp',
                                            'user_id'       => Auth::user()->id,
                                            'created_at'    => $insert_date,
                                            'updated_at'    => $insert_date,
                                        ]);
            
                                        $material_preparation->last_user_movement_id    = Auth::user()->id;
                                        $material_preparation->last_locator_id          = $inventory->id;
                                        $material_preparation->last_status_movement     = 'reverse';
                                        $material_preparation->last_movement_date       = $insert_date;
                                        $material_preparation->cancel_planning          = $insert_date;
                                        $material_preparation->save();
            
                                        $obj_line                               = new stdClass();
                                        $obj_line->material_movement_line_id    = $last_material_movement_line_id;
                                        $obj_line->movement_date                = $insert_date;
                                        $delete_material_movement_lines []      = $obj_line;
            
                                    }else
                                    {
                                        Temporary::Create([
                                            'barcode'       => $material_preparation->po_buyer,
                                            'status'        => 'mrp',
                                            'user_id'       => Auth::user()->id,
                                            'created_at'    => $insert_date,
                                            'updated_at'    => $insert_date,
                                        ]);
            
                                        $material_preparation->delete();                            
                                    }
                                }
                            }
    
                            if($reserved_qty == 0)
                            {
                                $old_roll->material_preparation_fabric_id   = $request->new_material_preparation_fabric_id;
                                
                                if($old_last_status_movement == 'out')
                                {
                                    MovementStockHistory::create([
                                        'material_stock_id' => $old_roll->material_stock_id,
                                        'from_location'     => $relax->id,
                                        'to_destination'    => $old_roll->last_locator_id,
                                        'status'            => 'out',
                                        'uom'               => $old_roll->uom,
                                        'qty'               => $old_roll->reserved_qty,
                                        'movement_date'     => $insert_date,
                                        'note'              => 'qty prepare dipindahkan semua dari plan '.$old_planning_date.' ke tanggal '.$new_planning_date,
                                        'user_id'           => auth::user()->id
                                    ]);
                                }else
                                {
                                    MovementStockHistory::create([
                                        'material_stock_id' => $old_roll->material_stock_id,
                                        'from_location'     => $relax->id,
                                        'to_destination'    => $relax->id,
                                        'status'            => 'relax',
                                        'uom'               => $old_roll->uom,
                                        'qty'               => $old_roll->reserved_qty,
                                        'movement_date'     => $insert_date,
                                        'note'              => 'qty prepare dipindahkan semua dari plan '.$old_planning_date.' ke tanggal '.$new_planning_date,
                                        'user_id'           => auth::user()->id
                                    ]);
                                }
                                
                            }else
                            {
                                $qty_move                                                       = sprintf('%0.8f',$old_roll->reserved_qty - $old_material->reserved_qty);
                                $old_roll->reserved_qty                                         = sprintf('%0.8f',$old_material->reserved_qty);
                                $old_roll->is_status_out_inserted_to_material_preparation       = false;
                                $old_roll->is_status_prepare_inserted_to_material_preparation   = false;
            
                                if($old_last_status_movement == 'out')
                                {
                                    MovementStockHistory::create([
                                        'material_stock_id' => $old_roll->material_stock_id,
                                        'from_location'     => $relax->id,
                                        'to_destination'    => $old_roll->last_locator_id,
                                        'status'            => 'relax',
                                        'uom'               => $old_roll->uom,
                                        'qty'               => $old_material->reserved_qty,
                                        'movement_date'     => $insert_date,
                                        'note'              => 'qty prepare dipindahkan sebagian ('.$qty_move.') dari plan '.$old_planning_date.' ke tanggal '.$new_planning_date,
                                        'user_id'           => auth::user()->id
                                    ]);
    
                                    if($old_roll->detailPoBuyerPerRoll()->count() == 0)
                                    {
                                        $po_detail_id       = $old_roll->materialStock->po_detail_id;
                                        $item_code          = $old_roll->materialStock->item_code;
                                        $item_id            = $old_roll->materialStock->item_id;
                                        $c_order_id         = $old_roll->materialStock->c_order_id;
                                        $uom                = $old_roll->materialStock->uom;
                                        $c_bpartner_id      = $old_roll->materialStock->c_bpartner_id;
                                        $supplier_name      = $old_roll->materialStock->supplier_name;
                                        $nomor_roll         = $old_roll->materialStock->nomor_roll;
                                        $warehouse_id       = $old_roll->materialStock->warehouse_id;
                                        $document_no        = $old_roll->materialStock->document_no;
                                        $material_stock_id  = $old_roll->material_stock_id;
    
                                        if($old_roll->po_detail_id == 'FREE STOCK' || $old_roll->po_detail_id == 'FREE STOCK-CELUP')
                                        {
                                            $no_packing_list        = '-';
                                            $no_invoice             = '-';
                                            $c_orderline_id         = '-';
                                        }else
                                        {
                                            $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                            ->whereNull('material_roll_handover_fabric_id')
                                            ->where('po_detail_id',$old_roll->po_detail_id)
                                            ->first();

                                            $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                            $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                            $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                                        }
    
                                        $is_movement_exists = MaterialMovement::where([
                                            ['from_location',$inventory_erp->id],
                                            ['to_destination',$inventory_erp->id],
                                            ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                            ['to_locator_erp_id',$inventory_erp->area->erp_id],
                                            ['is_integrate',false],
                                            ['is_active',true],
                                            ['no_packing_list',$no_packing_list],
                                            ['no_invoice',$no_invoice],
                                            ['status','integration-to-inventory-erp'],
                                        ])
                                        ->first();
                                        
                                        if(!$is_movement_exists)
                                        {
                                            $material_movement = MaterialMovement::firstOrCreate([
                                                'from_location'         => $inventory_erp->id,
                                                'to_destination'        => $inventory_erp->id,
                                                'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                                'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                                'is_integrate'          => false,
                                                'is_active'             => true,
                                                'no_packing_list'       => $no_packing_list,
                                                'no_invoice'            => $no_invoice,
                                                'status'                => 'integration-to-inventory-erp',
                                                'created_at'            => $insert_date,
                                                'updated_at'            => $insert_date,
                                            ]);
                
                                            $material_movement_id = $material_movement->id;
                                        }else
                                        {
                                            $is_movement_exists->updated_at = $insert_date;
                                            $is_movement_exists->save();
                
                                            $material_movement_id = $is_movement_exists->id;
                                        }
    
                                        $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                                        ->where([
                                            ['material_movement_id',$material_movement_id],
                                            ['item_id',$item_id],
                                            ['warehouse_id',$warehouse_id],
                                            ['material_stock_id',$material_stock_id],
                                            ['qty_movement',-1*$old_material->reserved_qty],
                                            ['is_integrate',false],
                                            ['is_active',true],
                                            ['date_movement',$insert_date],
                                        ])
                                        ->exists();
    
                                        if(!$is_material_movement_line_exists)
                                        {
                                            MaterialMovementLine::Create([
                                                'material_movement_id'          => $material_movement_id,
                                                'material_stock_id'             => $material_stock_id,
                                                'item_code'                     => $item_code,
                                                'item_id'                       => $item_id,
                                                'c_order_id'                    => $c_order_id,
                                                'c_orderline_id'                => $c_orderline_id,
                                                'c_bpartner_id'                 => $c_bpartner_id,
                                                'supplier_name'                 => $supplier_name,
                                                'type_po'                       => 1,
                                                'is_integrate'                  => false,
                                                'uom_movement'                  => $uom,
                                                'qty_movement'                  => -1*$old_material->reserved_qty,
                                                'date_movement'                 => $insert_date,
                                                'created_at'                    => $insert_date,
                                                'updated_at'                    => $insert_date,
                                                'date_receive_on_destination'   => $insert_date,
                                                'nomor_roll'                    => $nomor_roll,
                                                'warehouse_id'                  => $warehouse_id,
                                                'document_no'                   => $document_no,
                                                'note'                          => 'PROSES INTEGRASI UNTUK MOVE PLAN TAPI UDAH OUT DAN GA ADA PO BUYER DI TABEL PO BUYER PER ROLL',
                                                'is_active'                     => true,
                                                'user_id'                       => auth::user()->id,
                                            ]);
                                        }
                                    }
                                }else
                                {
                                    MovementStockHistory::create([
                                        'material_stock_id' => $old_roll->material_stock_id,
                                        'from_location'     => $relax->id,
                                        'to_destination'    => $relax->id,
                                        'status'            => 'relax',
                                        'uom'               => $old_roll->uom,
                                        'qty'               => $old_material->reserved_qty,
                                        'movement_date'     => $insert_date,
                                        'note'              => 'qty prepare dipindahkan sebagian ('.$qty_move.') dari plan '.$old_planning_date.' ke tanggal '.$new_planning_date,
                                        'user_id'           => auth::user()->id
                                    ]);
                                }
                                
                                $old_roll->save();
                            }
                        }
    
                        DetailPoBuyerPerRoll::where('detail_material_preparation_fabric_id',$old_id_roll)->delete();
                        $old_roll->save();
                       
                    }
                }
            }
            

            // new
            $new_material_preparation_fabric_id = $request->new_material_preparation_fabric_id;
            $new                                = MaterialPreparationFabric::find($new_material_preparation_fabric_id);
            
            if($new)
            {
                $new_qty_rilex                      = sprintf('%0.8f',$request->qty_relax);
                $new_qty_outstanding                = sprintf('%0.8f',$request->qty_outstanding);
            
                $new->total_qty_rilex               = $new_qty_rilex;
                $new->total_qty_outstanding         = $new_qty_outstanding;
                $new->remark                        = $old_remark;
                $new->remark_planning               = 'planning from '.$old_planning_date.' change to '.$new_planning_date; //gana
                
                if ($new->remark_planning == NULL || '') {
                    $new->remark_planning           = 'planning from '.$old_planning_date.' change to '.$new_planning_date;
                } else {
                    $new->remark_planning           = $new->remark_planning.', change again to '.$new_planning_date;
                }

                $new->save();


                $relax = Locator::whereHas('area',function ($query) use($new)
                {
                    $query->where('name','RELAX')
                    ->where('warehouse',$new->warehouse_id);
                })
                ->first();

                $inventory  = Locator::where('rack','INVENTORY') 
                ->whereHas('area',function ($query) use($new)
                {
                    $query->where('name','INVENTORY')
                    ->where('warehouse',$new->warehouse_id);
                })
                ->first();

                $new_materials                      = json_decode($request->list_material);

                foreach ($new_materials as $key => $new_material) 
                {
                    $old_id_roll                            = $new_material->old_id;
                    $old_material_preparation_fabric_id     = $new_material->material_preparation_fabric_id;
                    $new_reserved_qty                       = sprintf('%0.8f',$new_material->reserved_qty);
                    $new_roll                               = DetailMaterialPreparationFabric::where([
                        ['id',$old_id_roll],
                        ['material_preparation_fabric_id',$old_material_preparation_fabric_id]
                    ])
                    ->first();

                    if($new_roll)
                    {
                        $get_barcode                = $this->randomCode($new_roll->barcode.'MP');
                        $barcode                    = $get_barcode->barcode;
                        $referral_code              = $get_barcode->referral_code;
                        $sequence                   = $get_barcode->sequence;
                        $new_last_status_movement   = $new_roll->last_status_movement;

                        $is_exists = DetailMaterialPreparationFabric::where([
                            ['material_preparation_fabric_id',$request->new_material_preparation_fabric_id],
                            ['material_stock_id',$new_roll->material_stock_id],
                            ['reserved_qty',$new_reserved_qty],
                        ])
                        ->whereNull('deleted_at')
                        ->exists();

                        if(!$is_exists)
                        {
                            if($new_reserved_qty > 0)
                            {
                                $new_detail_material_preparation_fabric = DetailMaterialPreparationFabric::FirstOrCreate([
                                    'material_preparation_fabric_id'        => $request->new_material_preparation_fabric_id,
                                    'material_stock_id'                     => $new_roll->material_stock_id,
                                    'barcode'                               => $barcode,
                                    'referral_code'                         => $referral_code,
                                    'sequence'                              => $sequence,
                                    'uom'                                   => $new_roll->uom,
                                    'item_id'                               => $new_roll->item_id,
                                    'item_code'                             => $new_roll->item_code,
                                    'color'                                 => $new_roll->color,
                                    'upc_item'                              => $new_roll->upc_item,
                                    'begin_width'                           => $new_roll->begin_width,
                                    'middle_width'                          => $new_roll->middle_width,
                                    'end_width'                             => $new_roll->end_width,
                                    'actual_width'                          => $new_roll->actual_width,
                                    'actual_length'                         => $new_roll->actual_length,
                                    'actual_lot'                            => $new_roll->actual_lot,
                                    'actual_preparation_date'               => $new_roll->actual_preparation_date,
                                    'last_movement_date'                    => ($new_last_status_movement == 'out' ? $new_roll->last_movement_date : $insert_date),
                                    'preparation_date'                      => ($new_last_status_movement == 'out' ? $new_roll->preparation_date : $insert_date),
                                    'movement_out_date'                     => ($new_last_status_movement == 'out' ? $new_roll->movement_out_date : null),
                                    'remark'                                => ($new_last_status_movement == 'out' ? 'SUDAH SUPPLAI KE CUTTING-CUTTING':null),
                                    'last_locator_id'                       => ($new_last_status_movement == 'out' ? $new_roll->last_locator_id:$relax->id),
                                    'last_status_movement'                  => ($new_last_status_movement == 'out' ? 'out':'relax'),
                                    'last_user_movement_id'                 => ($new_last_status_movement == 'out' ? $new_roll->last_user_movement_id:auth::user()->id),
                                    'warehouse_id'                          => $new_roll->warehouse_id,
                                    'reserved_qty'                          => $new_reserved_qty,
                                    'deleted_at'                            => null,
                                    'user_id'                               => $new_roll->user_id,
                                ]);

                                if($new_last_status_movement == 'out')
                                {
                                    MovementStockHistory::create([
                                        'material_stock_id' => $new_detail_material_preparation_fabric->material_stock_id,
                                        'from_location'     => $relax->id,
                                        'to_destination'    => $new_detail_material_preparation_fabric->last_locator_id,
                                        'status'            => 'out',
                                        'uom'               => $new_detail_material_preparation_fabric->uom,
                                        'qty'               => $new_detail_material_preparation_fabric->reserved_qty,
                                        'movement_date'     => $new_detail_material_preparation_fabric->last_movement_date,
                                        'note'              => 'digunakan untuk plan tanggal '.$new_planning_date.', yang di dapatkan dari perpindahan plan tanggal '.$old_planning_date.' ( NOMOR SERI BARCODE '.$new_detail_material_preparation_fabric->barcode.')',
                                        'user_id'           => auth::user()->id
                                    ]);
                                }else
                                {
                                    
                                    MovementStockHistory::create([
                                        'material_stock_id' => $new_detail_material_preparation_fabric->material_stock_id,
                                        'from_location'     => $relax->id,
                                        'to_destination'    => $relax->id,
                                        'status'            => 'relax',
                                        'uom'               => $new_detail_material_preparation_fabric->uom,
                                        'qty'               => $new_detail_material_preparation_fabric->reserved_qty,
                                        'movement_date'     => $new_detail_material_preparation_fabric->last_movement_date,
                                        'note'              => 'digunakan untuk plan tanggal '.$new_planning_date.', yang di dapatkan dari perpindahan plan tanggal '.$old_planning_date.' ( NOMOR SERI BARCODE '.$new_detail_material_preparation_fabric->barcode.')',
                                        'user_id'           => auth::user()->id
                                    ]);
        
                                    $return_print [] = $new_detail_material_preparation_fabric->id;
                                }
                            }
                        }
                    
                    }
                }
            }

            if($old)
            {
                if(
                    sprintf('%0.8f',$old->total_reserved_qty) == 0.0000 
                    && sprintf('%0.8f',$old->total_qty_rilex) == 0.0000
                    && $old->detailMaterialPreparationFabric()->where('is_closing',false)->count() == 0
                )
                {
                    $this->updateTotalPreparation($old->id);
                }else
                {
                    MaterialPreparation::whereIn('id',function($query) use($material_preparation_fabric_id)
                    {
                        $query->select('material_preparation_id')
                        ->from('detail_material_preparations')
                        ->whereNotNull('detail_material_preparation_fabric_id')
                        ->whereIn('detail_material_preparation_fabric_id',function($query2) use ($material_preparation_fabric_id)
                        {
                            $query2->select('id')
                            ->from('detail_material_preparation_fabrics')
                            ->where('material_preparation_fabric_id',$material_preparation_fabric_id);
                        })
                        ->groupby('material_preparation_id');
                    })
                    ->delete();

                    DetailPoBuyerPerRoll::whereIn('detail_material_preparation_fabric_id',function($query) use($material_preparation_fabric_id)
                    {
                        $query->select('id')
                        ->from('detail_material_preparation_fabrics')
                        ->where('material_preparation_fabric_id',$material_preparation_fabric_id);
                    })
                    ->delete();
                    
                    $this->insertRePrepareRollToBuyer($material_preparation_fabric_id,0);
                }

            }
            
            MaterialPreparation::whereIn('id',function($query) use($new_material_preparation_fabric_id)
            {
                $query->select('material_preparation_id')
                ->from('detail_material_preparations')
                ->whereNotNull('detail_material_preparation_fabric_id')
                ->whereIn('detail_material_preparation_fabric_id',function($query2) use ($new_material_preparation_fabric_id)
                {
                    $query2->select('id')
                    ->from('detail_material_preparation_fabrics')
                    ->where('material_preparation_fabric_id',$new_material_preparation_fabric_id);
                })
                ->groupby('material_preparation_id');
            })
            ->delete();
            
            DetailPoBuyerPerRoll::whereIn('detail_material_preparation_fabric_id',function($query) use($new_material_preparation_fabric_id)
            {
                $query->select('id')
                ->from('detail_material_preparation_fabrics')
                ->where('material_preparation_fabric_id',$new_material_preparation_fabric_id);
            })
            ->delete();
            
            $this->insertRePrepareRollToBuyer($new_material_preparation_fabric_id,0);
            if(count($delete_material_movement_lines) > 0)  ReverseOut::deleteMaterialMovementLines($delete_material_movement_lines);
            DB::commit();

            if($old)
            {
                $this->checkMaterialPreparationFabric($old->id);
                $this->updateTotalPreparation($old->id);
            }

            if($new)
            {
                $this->checkMaterialPreparationFabric($new->id);
                $this->updateTotalPreparation($new->id);
            }
           
           
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
        }

        if(count($return_print) > 0) return response()->json($return_print,200);
        else return response()->json('-1',200);
    }
    
    public function printBarcodeMovePrepare(Request $request)
    {   

        $list_barcodes          = json_decode($request->list_barcodes);
        $barcode_preparations   = DetailMaterialPreparationFabric::whereIn('id',$list_barcodes)->get();
        
        
        return view('fabric_report_daily_material_preparation.barcode',compact('barcode_preparations'));
    }

    static function doClose($id,$remark,$user_id)
    {
        try 
        {
            DB::beginTransaction();
            $actual_planning_date                   = carbon::now()->toDateTimeString();

            FabricReportDailyMaterialPreparationController::updateTotalPreparation($id);

            $material_preparation_fabric            = MaterialPreparationFabric::find($id);
            $total_qty_rilex                        = sprintf("%.8f",$material_preparation_fabric->total_qty_rilex);
            $total_qty_outstanding                  = sprintf("%.8f",$material_preparation_fabric->total_qty_outstanding);
            
            $warehouse_id                           = $material_preparation_fabric->warehouse_id;

            if($material_preparation_fabric->is_piping)
            {
                $distribution = Locator::whereHas('area',function($query) use ($warehouse_id)
                {
                    $query->where([
                        ['name','DISTRIBUTION'],
                        ['warehouse',$warehouse_id],
                    ]);
                })
                ->first();

                $last_locator_id    = $distribution->id;
                $last_locator_name   = $distribution->code;
            }else 
            {
                $cutting = Locator::whereHas('area',function($query) use ($warehouse_id)
                {
                    $query->where([
                        ['name','CUTTING'],
                        ['warehouse',$warehouse_id],
                    ]);
                })
                ->first();

                $last_locator_id     = $cutting->id;
                $last_locator_name   = $cutting->code;
            }

            if($total_qty_outstanding > 0)
            {
                $master_item                        = Item::where('item_id',$material_preparation_fabric->item_id_source)->first();

                DetailMaterialPreparationFabric::FirstOrCreate([
                    'material_preparation_fabric_id'        => $id,
                    'material_stock_id'                     => null,
                    'barcode'                               => 'CLOSINGAN-'.$id,
                    'referral_code'                         => '-1',
                    'sequence'                              => '-1',
                    'uom'                                   => 'YDS',
                    'item_id'                               => $material_preparation_fabric->item_id_source,
                    'item_code'                             => $material_preparation_fabric->item_code_source,
                    'color'                                 => $master_item->color,
                    'upc_item'                              => $master_item->upc_item,
                    'begin_width'                           => 0,
                    'middle_width'                          => 0,
                    'end_width'                             => 0,
                    'actual_width'                          => 0,
                    'actual_length'                         => $total_qty_outstanding,
                    'actual_lot'                            => 'CLOSING',
                    'actual_preparation_date'               => $actual_planning_date,
                    'last_movement_date'                    => $actual_planning_date,
                    'preparation_date'                      => $actual_planning_date,
                    'movement_out_date'                     => $actual_planning_date,
                    'last_locator_id'                       => $last_locator_id,
                    'last_status_movement'                  => 'out',
                    'last_user_movement_id'                 => $user_id,
                    'warehouse_id'                          => $material_preparation_fabric->warehouse_id,
                    'reserved_qty'                          => $total_qty_outstanding,
                    'deleted_at'                            => $actual_planning_date,
                    'remark'                                => 'SUDAH SUPPLAI KE '.$last_locator_name,
                    'remark_closing'                        => $remark,
                    'is_closing'                            => true,
                    'user_id'                               => $user_id,
                ]);

                $material_preparation_fabric->remark                = $remark.", qty ".$total_qty_outstanding." adalah untuk closing";
                $material_preparation_fabric->total_qty_rilex       = sprintf("%.8f",$total_qty_rilex + $total_qty_outstanding);
                $material_preparation_fabric->total_qty_outstanding = 0;
                $material_preparation_fabric->save();
                
                DetailPoBuyerPerRoll::whereIn('detail_material_preparation_fabric_id',function($query) use($material_preparation_fabric)
                {
                    $query->select('id')
                    ->from('detail_material_preparation_fabrics')
                    ->where('material_preparation_fabric_id',$material_preparation_fabric->id);
                })
                ->delete();

                FabricReportDailyMaterialPreparationController::insertRePrepareRollToBuyer($id,1);
    
            }else
            {
                return response()->json('Total qty relax has bigger than total qty prepared',422);
            }
            
           
            DB::commit();

            return response()->json('success',200);
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function insertRePrepareRollToBuyer($id,$flag)
    {
        $material_preparation_fabric = MaterialPreparationFabric::find($id);

        //if($material_preparation_fabric->total_qty_outstanding != '0') return response()->json('This prepare is still outstanding, please complete it first',422);

       if($material_preparation_fabric)
       {
            $__detail_material_planning_per_part    = new DetailMaterialPlanningFabricPerPart();
            $material_preparation_fabric_id         = $material_preparation_fabric->id;
            $c_order_id                             = $material_preparation_fabric->c_order_id;
            $document_no                            = $material_preparation_fabric->document_no;
            $c_bpartner_id                          = $material_preparation_fabric->c_bpartner_id;
            $planning_date                          = $material_preparation_fabric->planning_date;
            $warehouse_id                           = $material_preparation_fabric->warehouse_id;
            $article_no                             = $material_preparation_fabric->article_no;
            $item_id_book                           = $material_preparation_fabric->item_id_book;
            $item_id_source                         = $material_preparation_fabric->item_id_source;
            $is_piping                              = $material_preparation_fabric->is_piping;
            $_style                                 = $material_preparation_fabric->_style;
            $is_from_additional                     = $material_preparation_fabric->is_from_additional;
            $item_code                              = strtoupper(trim($material_preparation_fabric->item_code));
            $movement_date                          = carbon::now()->toDateTimeString();
            $detail_material_preparation_ids        = array();
            $delete_material_movement_lines         = array();
            
            $inventory_erp = Locator::with('area')
            ->where([
                ['is_active',true],
                ['rack','ERP INVENTORY'],
            ])
            ->whereHas('area',function ($query) use($warehouse_id)
            {
                $query->where('is_active',true);
                $query->where('warehouse',$warehouse_id);
                $query->where('name','ERP INVENTORY');
            })
            ->first();

            $inventory = Locator::where('rack','INVENTORY')
            ->whereHas('area',function ($query) use ($warehouse_id)
            {
                $query->where('name','INVENTORY')
                ->where('warehouse',$warehouse_id);
            })
            ->first();
            
            
            try 
            {
                DB::beginTransaction();

                /*if($flag == 1)
                {
                    $detail_preparation_fabrics = $material_preparation_fabric->detailMaterialPreparationFabric()->get();
                    
                    foreach ($detail_preparation_fabrics as $key => $detail_preparation_fabric) 
                    {
                        if(!$detail_preparation_fabric->is_closing)
                        {
                            $user_id                        = $detail_preparation_fabric->user_id;
                            $detail_preparation_fabric_id   = $detail_preparation_fabric->id;
                            $movement_out_date              = $detail_preparation_fabric->movement_out_date;
                            $po_detail_id                   = $detail_preparation_fabric->materialStock->po_detail_id;
                            $item_code                      = $detail_preparation_fabric->materialStock->item_code;
                            $uom                            = $detail_preparation_fabric->materialStock->uom;
                            $item_id                        = $detail_preparation_fabric->materialStock->item_id;
                            $c_order_id                     = $detail_preparation_fabric->materialStock->c_order_id;
                            $c_bpartner_id                  = $detail_preparation_fabric->materialStock->c_bpartner_id;
                            $supplier_name                  = $detail_preparation_fabric->materialStock->supplier_name;
                            $nomor_roll                     = $detail_preparation_fabric->materialStock->nomor_roll;
                            $warehouse_id                   = $detail_preparation_fabric->materialStock->warehouse_id;
                            $document_no                    = $detail_preparation_fabric->materialStock->document_no;
                            $material_stock_id              = $detail_preparation_fabric->material_stock_id;
                            
                            $system = User::where([
                                ['name','system'],
                                ['warehouse',$warehouse_id]
                            ])
                            ->first();

                            if($detail_preparation_fabric->detailPoBuyerPerRoll()->count() == 0)
                            {
                                if($movement_out_date)
                                {
                                    if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                                    {
                                        $no_packing_list        = '-';
                                        $no_invoice             = '-';
                                        $c_orderline_id         = '-';
                                    }else
                                    {
                                        $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                        ->whereNull('material_roll_handover_fabric_id')
                                        ->where('po_detail_id',$po_detail_id)
                                        ->first();

                                        $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                        $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                        $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                                    }
        
                                    $is_movement_exists = MaterialMovement::where([
                                        ['from_location',$inventory_erp->id],
                                        ['to_destination',$inventory_erp->id],
                                        ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                        ['to_locator_erp_id',$inventory_erp->area->erp_id],
                                        ['is_integrate',false],
                                        ['is_active',true],
                                        ['no_packing_list',$no_packing_list],
                                        ['no_invoice',$no_invoice],
                                        ['status','integration-to-inventory-erp'],
                                    ])
                                    ->first();
                                    
                                    if(!$is_movement_exists)
                                    {
                                        $material_movement = MaterialMovement::firstOrCreate([
                                            'from_location'         => $inventory_erp->id,
                                            'to_destination'        => $inventory_erp->id,
                                            'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                            'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                            'is_integrate'          => false,
                                            'is_active'             => true,
                                            'no_packing_list'       => $no_packing_list,
                                            'no_invoice'            => $no_invoice,
                                            'status'                => 'integration-to-inventory-erp',
                                            'created_at'            => $movement_date,
                                            'updated_at'            => $movement_date,
                                        ]);
        
                                        $material_movement_id = $material_movement->id;
                                    }else
                                    {
                                        $is_movement_exists->updated_at = $movement_date;
                                        $is_movement_exists->save();
        
                                        $material_movement_id = $is_movement_exists->id;
                                    }
        
                                    $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                                    ->where([
                                        ['material_movement_id',$material_movement_id],
                                        ['item_id',$item_id],
                                        ['warehouse_id',$warehouse_id],
                                        ['material_stock_id',$material_stock_id],
                                        ['qty_movement',$detail_preparation_fabric->reserved_qty],
                                        ['date_movement',$movement_date],
                                        ['is_integrate',false],
                                        ['is_active',true],
                                    ])
                                    ->exists();
        
                                    if(!$is_material_movement_line_exists)
                                    {
                                        MaterialMovementLine::Create([
                                            'material_movement_id'          => $material_movement_id,
                                            'material_stock_id'             => $material_stock_id,
                                            'item_code'                     => $item_code,
                                            'item_id'                       => $item_id,
                                            'c_order_id'                    => $c_order_id,
                                            'c_orderline_id'                => $c_orderline_id,
                                            'c_bpartner_id'                 => $c_bpartner_id,
                                            'supplier_name'                 => $supplier_name,
                                            'type_po'                       => 1,
                                            'is_integrate'                  => false,
                                            'uom_movement'                  => $uom,
                                            'qty_movement'                  => $detail_preparation_fabric->reserved_qty,
                                            'date_movement'                 => $movement_date,
                                            'created_at'                    => $movement_date,
                                            'updated_at'                    => $movement_date,
                                            'date_receive_on_destination'   => $movement_date,
                                            'nomor_roll'                    => $nomor_roll,
                                            'warehouse_id'                  => $warehouse_id,
                                            'document_no'                   => $document_no,
                                            'note'                          => 'PROSES INTEGRASI UNTUK RE PREPRARE ROLL PER BARCODE BY'.$system->name,
                                            'is_active'                     => true,
                                            'user_id'                       => $system->id,
                                        ]);
                                    }
                                }
                            }else
                            {
                                    // check status movement fabric
                                $material_preparations = MaterialPreparation::whereIn('id',function($query) use($detail_preparation_fabric_id)
                                {
                                    $query->select('material_preparation_id')
                                    ->from('detail_material_preparations')
                                    ->whereNotNull('detail_material_preparation_fabric_id')
                                    ->where('detail_material_preparation_fabric_id',$detail_preparation_fabric_id)
                                    ->groupby('material_preparation_id');
                                })
                                ->get();
            
                                foreach ($material_preparations as $key => $material_preparation) 
                                {
                                    if($material_preparation->last_status_movement == 'out-cutting' || $material_preparation->last_status_movement == 'out-distribution')
                                    {
                                        $last_material_movement_line_id = $material_preparation->last_material_movement_line_id;
                                        
                                        if($material_preparation->po_detail_id == 'FREE STOCK' || $material_preparation->po_detail_id == 'FREE STOCK-CELUP')
                                        {
                                            $c_order_id             = 'FREE STOCK';
                                            $no_packing_list        = '-';
                                            $no_invoice             = '-';
                                            $c_orderline_id         = '-';
                                        }else
                                        {
                                            $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                            ->whereNull('material_roll_handover_fabric_id')
                                            ->where('po_detail_id',$material_preparation->po_detail_id)
                                            ->first();

                                            $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                            $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                            $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                                        }
                                        
                                        $_item_id_source = ($material_preparation->item_id_book != $material_preparation->item_id_source ? $material_preparation->item_id_source : null);
            
                                        $is_movement_reverse_exists = MaterialMovement::where([
                                            ['from_location',$material_preparation->last_locator_id],
                                            ['to_destination',$inventory->id],
                                            ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                            ['to_locator_erp_id',$inventory_erp->area->erp_id],
                                            ['po_buyer',$material_preparation->po_buyer],
                                            ['is_integrate',false],
                                            ['is_complete_erp',false],
                                            ['is_active',true],
                                            ['no_packing_list',$no_packing_list],
                                            ['no_invoice',$no_invoice],
                                            ['status','reverse'],
                                        ])
                                        ->first();
            
                                        if($is_movement_reverse_exists)
                                        {
                                            $is_movement_reverse_exists->updated_at     = $movement_date;
                                            $is_movement_reverse_exists->save();
                                            
                                            $movement_reverse_id                        = $is_movement_reverse_exists->id;
            
                                        }else
                                        {
                                            $movement_reverse = MaterialMovement::FirstOrCreate([
                                                'from_location'             => $material_preparation->last_locator_id,
                                                'to_destination'            => $inventory->id,
                                                'from_locator_erp_id'       => $inventory_erp->area->erp_id,
                                                'to_locator_erp_id'         => $inventory_erp->area->erp_id,
                                                'po_buyer'                  => $material_preparation->po_buyer,
                                                'is_complete_erp'           => false,
                                                'is_integrate'              => false ,
                                                'is_active'                 => true,
                                                'status'                    => 'reverse',
                                                'no_packing_list'           => $no_packing_list,
                                                'no_invoice'                => $no_invoice,
                                                'created_at'                => $movement_date,
                                                'updated_at'                => $movement_date,
                                            ]);
            
                                            $movement_reverse_id = $movement_reverse->id;
                                        }
            
                                        $is_line_reverse_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                        ->where([
                                            ['material_movement_id',$movement_reverse_id],
                                            ['material_preparation_id',$material_preparation->id],
                                            ['qty_movement',$material_preparation->qty_conversion],
                                            ['item_id',$material_preparation->item_id],
                                            ['warehouse_id',$material_preparation->warehouse],
                                            ['date_movement',$movement_date],
                                            ['is_integrate',false],
                                            ['is_active',true],
                                        ]) 
                                        ->exists();
            
                                        if(!$is_line_reverse_exists)
                                        {
                                            $new_material_movement_line_reverse = MaterialMovementLine::Create([
                                                'material_movement_id'                      => $movement_reverse_id,
                                                'material_preparation_id'                   => $material_preparation->id,
                                                'item_code'                                 => $material_preparation->item_code,
                                                'item_id'                                   => $material_preparation->item_id,
                                                'c_order_id'                                => $material_preparation->c_order_id,
                                                'c_orderline_id'                            => $c_orderline_id,
                                                'c_bpartner_id'                             => $material_preparation->c_bpartner_id,
                                                'supplier_name'                             => $material_preparation->supplier_name,
                                                'type_po'                                   => 1,
                                                'is_integrate'                              => false,
                                                'is_inserted_to_material_movement_per_size' => false,
                                                'uom_movement'                              => $material_preparation->uom_conversion,
                                                'qty_movement'                              => $material_preparation->qty_conversion,
                                                'date_movement'                             => $movement_date,
                                                'created_at'                                => $movement_date,
                                                'updated_at'                                => $movement_date,
                                                'date_receive_on_destination'               => $movement_date,
                                                'nomor_roll'                                => '-',
                                                'warehouse_id'                              => $material_preparation->warehouse,
                                                'document_no'                               => $material_preparation->document_no,
                                                'note'                                      => 'MOVEMENT DI KEMBALIKAN KE INVENTORY',
                                                'is_active'                                 => true,
                                                'user_id'                                   => $material_preparation->last_user_movement_id,
                                            ]);
            
                                            $material_preparation->last_material_movement_line_id    = $new_material_movement_line_reverse->id;
                                        }
            
                                        Temporary::Create([
                                            'barcode'       => $material_preparation->po_buyer,
                                            'status'        => 'mrp',
                                            'user_id'       => $material_preparation->last_user_movement_id,
                                            'created_at'    => $movement_date,
                                            'updated_at'    => $movement_date,
                                        ]);
            
                                        $material_preparation->last_user_movement_id    = $material_preparation->last_user_movement_id;
                                        $material_preparation->last_locator_id          = $inventory->id;
                                        $material_preparation->last_status_movement     = 'reverse';
                                        $material_preparation->last_movement_date       = $movement_date;
                                        $material_preparation->cancel_planning          = $movement_date;
                                        $material_preparation->save();
            
                                        $obj_line                               = new stdClass();
                                        $obj_line->material_movement_line_id    = $last_material_movement_line_id;
                                        $obj_line->movement_date                = $movement_date;
                                        $delete_material_movement_lines []      = $obj_line;
            
                                    }else
                                    {
                                        Temporary::Create([
                                            'barcode'       => $material_preparation->po_buyer,
                                            'status'        => 'mrp',
                                            'user_id'       => $material_preparation->last_user_movement_id,
                                            'created_at'    => $movement_date,
                                            'updated_at'    => $movement_date,
                                        ]);
            
                                        $material_preparation->delete();                            
                                    }
                                }
                            }
                        }else
                        {
                            $detail_preparation_fabric_id   = $detail_preparation_fabric->id;
                            $movement_out_date              = $detail_preparation_fabric->movement_out_date;
                            
                            $material_preparations = MaterialPreparation::whereIn('id',function($query) use($detail_preparation_fabric_id)
                            {
                                $query->select('material_preparation_id')
                                ->from('detail_material_preparations')
                                ->whereNotNull('detail_material_preparation_fabric_id')
                                ->where('detail_material_preparation_fabric_id',$detail_preparation_fabric_id)
                                ->groupby('material_preparation_id');
                            })
                            ->get();

                            foreach ($material_preparations as $key => $material_preparation) 
                            {
                                $material_preparation->delete();
                            }
                        }
                    }

                    if(count($delete_material_movement_lines) > 0)  ReverseOut::deleteMaterialMovementLines($delete_material_movement_lines);
                }*/
                
                /**/

                if(!$is_from_additional)
                {
                    $detail_preparation_fabrics = DetailMaterialPreparationFabric::where('material_preparation_fabric_id',$material_preparation_fabric_id)
                    ->orderby('actual_lot','asc')
                    ->get();

                    foreach ($detail_preparation_fabrics as $key => $detail_preparation_fabric) 
                    {
                        $is_closing                             = $detail_preparation_fabric->is_closing;
                        $user_id                                = $detail_preparation_fabric->user_id;
                        $_reserved_qty                          = sprintf('%0.8f',$detail_preparation_fabric->reserved_qty);
                        $detail_material_preparation_fabric_id  = $detail_preparation_fabric->id;
                        // $material_stock_id  = $detail_preparation_fabric->material_stock_id;
                        // $actual_qty                             = MaterialStock::find($material_stock_id);
                        // $_reserved_qty                          = $actual_qty->actual_length;
                        $total_prepare                          = DetailPoBuyerPerRoll::where('detail_material_preparation_fabric_id',$detail_material_preparation_fabric_id)->sum('qty_booking');
                        $reserved_qty                           =  sprintf('%0.8f',$_reserved_qty - $total_prepare);

                        //cek kebutuhan detail plan
                        $detail_material_plannings = DetailMaterialPlanningFabric::where([
                            ['_style',$_style],
                            ['article_no',$article_no],
                            ['c_order_id',$c_order_id],
                            ['item_id_book',$item_id_book],
                            ['item_id_source',$item_id_source],
                            ['planning_date',$planning_date],
                            ['is_piping',$is_piping],
                            ['warehouse_id',$warehouse_id],
                        ])
                        ->whereNull('deleted_at')
                        ->orderby('po_buyer','asc')
                        ->get();

                        foreach ($detail_material_plannings as $key => $detail_material_planning) 
                        {
                            $material_planning_fabric_id        = $detail_material_planning->material_planning_fabric_id;
                            $po_buyer                           = $detail_material_planning->po_buyer;
                            $style                              = $detail_material_planning->style;
                            $job_order                          = $detail_material_planning->materialPlanningFabric->job_order;
                            $style_in_job_order                 = explode('::',$job_order)[0];
                            $check_saldo                        = $detail_material_planning->checkSaldo($_style,$article_no,$c_order_id,$item_id_book,$item_id_source,$planning_date,$is_piping,$warehouse_id,$po_buyer)[0]->exists;
                                                        
                            if($check_saldo) $qty_consumtion    = sprintf('%0.8f',$detail_material_planning->qty_booking - $detail_material_planning->qtyAllocated($detail_material_planning->id));
                            else $qty_consumtion = $reserved_qty;
                            //echo 'reserved'.$reserved_qty.PHP_EOL;
                            if($qty_consumtion > 0 && $reserved_qty > 0)
                            {
                                
                                if ($reserved_qty/$qty_consumtion >= 1) $_supplied1 = $qty_consumtion;
                                else  $_supplied1 = $reserved_qty;

                                $material_requirement_per_parts = DetailMaterialPlanningFabricPerPart::where('material_planning_fabric_id',$material_planning_fabric_id)
                                ->orderby('part_no','asc')
                                ->get();
                                
                                foreach ($material_requirement_per_parts as $key => $material_requirement_per_part) 
                                {
                                    $qty_need_per_part                  = $material_requirement_per_part->qty_per_part;
                                    $part_no                            = $material_requirement_per_part->part_no;
                                    $checkSaldoPerPart                 = $material_requirement_per_part->checkSaldo($material_planning_fabric_id)[0]->exists;
                    
                                    if($checkSaldoPerPart)
                                    {
                                        $total_preparation_per_part         = $material_requirement_per_part->getTotalPreparationFabric($material_requirement_per_part->id);
                                        $qty_required_per_part              = sprintf('%0.8f',$qty_need_per_part - $total_preparation_per_part);
                                    }else
                                    {
                                        $total_preparation_per_part         = 1;
                                        $qty_required_per_part              = $qty_consumtion;//harus dikurangi per po supplier
                                    } 
                                    
                                    //echo $check_saldo.' '.$detail_material_preparation_fabric_id.' '.$po_buyer.' id planning '. $material_requirement_per_part->material_planning_fabric_id. ' part'.  $part_no .' kebutuhan '.$qty_required_per_part .' total prepare '.$total_preparation_per_part.' supply'.$__supplied.PHP_EOL;
                                    ////echo $check_saldo.' '.$detail_material_preparation_fabric_id.' '.$po_buyer.' '.$qty_consumtion.' '.$part_no.' '.$reserved_qty.' '.$total_preparation_per_part.' '.$qty_required_per_part.' '.$__supplied.PHP_EOL;
                                    //echo $__supplied.PHP_EOL;
                                    if($qty_required_per_part > 0)
                                    {
                                        if ($_supplied1/$qty_required_per_part >= 1) $_supplied_per_part = $qty_required_per_part;
                                        else  $_supplied_per_part = $_supplied1;
                                        
                                        

                                        DetailPoBuyerPerRoll::FirstorCreate([
                                            'material_planning_fabric_id'           => $material_planning_fabric_id,
                                            'detail_material_preparation_fabric_id' => $detail_material_preparation_fabric_id,
                                            'detail_material_planning_fabric_id'    => $detail_material_planning->id,
                                            'detail_material_planning_fabric_per_part_id' => $material_requirement_per_part->id,
                                            'planning_date'                         => $planning_date,
                                            'part_no'                               => $part_no,
                                            'po_buyer'                              => $po_buyer,
                                            'article_no'                            => $article_no,
                                            'style'                                 => $style,
                                            'style_in_job_order'                    => $style_in_job_order,
                                            'job_order'                             => $job_order,
                                            'qty_booking'                           => sprintf('%0.8f',$_supplied_per_part),
                                            'warehouse_id'                          => $warehouse_id,
                                            'user_id'                               => $user_id,
                                            'is_closing'                            => $is_closing,
                                            'created_at'                            => $movement_date,
                                            'updated_at'                            => $movement_date,
                                        ]);

                                        $qty_required_per_part -= $_supplied_per_part;
                                        $_supplied1 -= $_supplied1;
                                        $qty_consumtion -= $_supplied_per_part;
                                        $reserved_qty   -= $_supplied_per_part;
                                    }
                                }

                                

                                
                            }

                            $detail_material_preparation_ids [] = $detail_material_preparation_fabric_id;
                        }

                        
                    }   
                }
                
                DetailMaterialPreparationFabric::where('material_preparation_fabric_id',$id)
                ->update([
                    'is_status_prepare_inserted_to_material_preparation'    => false,
                    'is_status_out_inserted_to_material_preparation'        => false,
                    'repreparation_roll_to_buyer_date'                      => $movement_date,
                ]);

                // posisi jangan di rubah
                /*if($flag == 1)
                {
                    $detail_preparation_fabrics = $material_preparation_fabric->detailMaterialPreparationFabric()->get();

                    foreach ($detail_preparation_fabrics as $key => $detail_preparation_fabric) 
                    {
                        if(!$detail_preparation_fabric->is_closing)
                        {
                            if($detail_preparation_fabric->detailPoBuyerPerRoll()->count() == 0)
                            {
                                $po_detail_id       = $detail_preparation_fabric->materialStock->po_detail_id;
                                $item_code          = $detail_preparation_fabric->materialStock->item_code;
                                $item_id            = $detail_preparation_fabric->materialStock->item_id;
                                $c_order_id         = $detail_preparation_fabric->materialStock->c_order_id;
                                $uom                = $detail_preparation_fabric->materialStock->uom;
                                $c_bpartner_id      = $detail_preparation_fabric->materialStock->c_bpartner_id;
                                $supplier_name      = $detail_preparation_fabric->materialStock->supplier_name;
                                $nomor_roll         = $detail_preparation_fabric->materialStock->nomor_roll;
                                $warehouse_id       = $detail_preparation_fabric->materialStock->warehouse_id;
                                $document_no        = $detail_preparation_fabric->materialStock->document_no;
                                $material_stock_id  = $detail_preparation_fabric->material_stock_id;

                                $system = User::where([
                                    ['name','system'],
                                    ['warehouse',$warehouse_id]
                                ])
                                ->first();
                                
                                if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                                {
                                    $no_packing_list        = '-';
                                    $no_invoice             = '-';
                                    $c_orderline_id         = '-';
                                }else
                                {
                                    $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                    ->whereNull('material_roll_handover_fabric_id')
                                    ->where('po_detail_id',$po_detail_id)
                                    ->first();

                                    $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                    $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                    $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                                }

                                $is_movement_exists = MaterialMovement::where([
                                    ['from_location',$inventory_erp->id],
                                    ['to_destination',$inventory_erp->id],
                                    ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                    ['to_locator_erp_id',$inventory_erp->area->erp_id],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                    ['no_packing_list',$no_packing_list],
                                    ['no_invoice',$no_invoice],
                                    ['status','integration-to-inventory-erp'],
                                ])
                                ->first();
                                
                                if(!$is_movement_exists)
                                {
                                    $material_movement = MaterialMovement::firstOrCreate([
                                        'from_location'         => $inventory_erp->id,
                                        'to_destination'        => $inventory_erp->id,
                                        'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                        'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                        'is_integrate'          => false,
                                        'is_active'             => true,
                                        'no_packing_list'       => $no_packing_list,
                                        'no_invoice'            => $no_invoice,
                                        'status'                => 'integration-to-inventory-erp',
                                        'created_at'            => $movement_date,
                                        'updated_at'            => $movement_date,
                                    ]);

                                    $material_movement_id = $material_movement->id;
                                }else
                                {
                                    $is_movement_exists->updated_at = $movement_date;
                                    $is_movement_exists->save();

                                    $material_movement_id = $is_movement_exists->id;
                                }

                                $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                                ->where([
                                    ['material_movement_id',$material_movement_id],
                                    ['material_stock_id',$material_stock_id],
                                    ['qty_movement',-1*$detail_preparation_fabric->reserved_qty],
                                    ['date_movement',$movement_date],
                                    ['item_id',$item_id],
                                    ['warehouse_id',$warehouse_id],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                ])
                                ->exists();

                                if(!$is_material_movement_line_exists)
                                {
                                    MaterialMovementLine::Create([
                                        'material_movement_id'          => $material_movement_id,
                                        'material_stock_id'             => $material_stock_id,
                                        'item_code'                     => $item_code,
                                        'item_id'                       => $item_id,
                                        'c_order_id'                    => $c_order_id,
                                        'c_orderline_id'                => $c_orderline_id,
                                        'c_bpartner_id'                 => $c_bpartner_id,
                                        'supplier_name'                 => $supplier_name,
                                        'type_po'                       => 1,
                                        'is_integrate'                  => false,
                                        'uom_movement'                  => $uom,
                                        'qty_movement'                  => -1*$detail_preparation_fabric->reserved_qty,
                                        'date_movement'                 => $movement_date,
                                        'created_at'                    => $movement_date,
                                        'updated_at'                    => $movement_date,
                                        'date_receive_on_destination'   => $movement_date,
                                        'nomor_roll'                    => $nomor_roll,
                                        'warehouse_id'                  => $warehouse_id,
                                        'document_no'                   => $document_no,
                                        'note'                          => 'PROSES INTEGRASI UNTUK REPREPARE BUYER PER ROLL TAPI UDAH OUT DAN GA ADA PO BUYER DI TABEL PO BUYER PER ROLL',
                                        'is_active'                     => true,
                                        'user_id'                       => $system->id,
                                    ]);
                                }
                            }
                        }
                    }
                }*/

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
       }

        /*$data           = db::select(db::raw("
            select material_preparation_fabric_id from detail_preparation_vs_detail_roll_per_buyer_fabric
            where total_relax::numeric != total_qty_booking_per_roll::numeric
            and material_preparation_fabric_id = '".$id."'
            GROUP BY material_preparation_fabric_id
            order by count(0) desc
        "));

        foreach ($data as $key => $value) 
        {
            $__id                           = $value->material_preparation_fabric_id;
            $_material_prepration_fabric    =  MaterialPreparationFabric::find($__id);

            $is_planning_exists = DetailMaterialPlanningFabricPerPart::whereIn('material_planning_fabric_id',function($query) use ($_material_prepration_fabric)
            {
                $query->select('material_planning_fabric_id')
                ->from('detail_material_planning_fabrics')
                ->where([
                    ['_style',$_material_prepration_fabric->_style],
                    ['article_no',$_material_prepration_fabric->article_no],
                    ['c_order_id',$_material_prepration_fabric->c_order_id],
                    ['item_id_book',$_material_prepration_fabric->item_id_book],
                    ['item_id_source',$_material_prepration_fabric->item_id_source],
                    ['planning_date',$_material_prepration_fabric->planning_date],
                    ['is_piping',$_material_prepration_fabric->is_piping],
                    ['warehouse_id',$_material_prepration_fabric->warehouse_id],
                ])
                ->groupby('material_planning_fabric_id');
            })
            ->exists();

            if($is_planning_exists) FabricReportDailyMaterialPreparationController::insertRePrepareRollToBuyer($__id,0);
        }*/
    }
    
    static function updateSummaryStockFabric($summary_stock_fabric_ids)
    {
        $summary_stock_fabrics = DB::table('summary_vs_stock_v')
        ->whereIn('id',$summary_stock_fabric_ids)
        ->get();
        
        try 
        {
            DB::beginTransaction();
            
            foreach ($summary_stock_fabrics as $key => $value) 
            {
                $summary_stock_fabric_id  = $value->id;
                $total_stock                          = sprintf('%0.8f',$value->detail_total_stock);
                $total_reserved_qty                   = sprintf('%0.8f',$value->detail_total_reserved_qty);
                $total_available_qty                  = sprintf('%0.8f',$value->detail_total_available_qty);
                
                $summary_stock_fabric                 = SummaryStockFabric::find($summary_stock_fabric_id);
                $summary_stock_fabric->stock          = $total_stock; 
                $summary_stock_fabric->reserved_qty   = $total_reserved_qty; 
                $summary_stock_fabric->available_qty  = $total_available_qty; 
                $summary_stock_fabric->save();
            }
        
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function randomCode($barcode)
    {
        $sequence = Barcode::where('referral_code',$barcode)->max('sequence');

        if ($sequence == null) $sequence = 1;
        else $sequence += 1;
        
        Barcode::FirstOrCreate([
            'barcode'           => $barcode.'-'.$sequence,
            'referral_code'     => $barcode,
            'sequence'          => $sequence,
        ]);
        
        $obj                = new stdClass();
        $obj->barcode       = $barcode.'-'.$sequence;
        $obj->referral_code = $barcode;
        $obj->sequence      = $sequence;

        return $obj;

    }

    static function updateTotalPreparation($id)
    {
        try 
        {
            DB::beginTransaction();

            $material_preparation_fabric                        = MaterialPreparationFabric::find($id);
            if($material_preparation_fabric)
            {
                $total_reserved_qty                                 = sprintf('%0.8f',$material_preparation_fabric->total_reserved_qty);
            
                if($total_reserved_qty > 0)
                {
                    $total_qty_relax                                    = sprintf('%0.8f',DetailMaterialPreparationFabric::where('material_preparation_fabric_id',$id)->sum('reserved_qty'));
                    $total_qty_outstanding                              = sprintf('%0.8f',$total_reserved_qty - $total_qty_relax);
                    
                }else
                {
                    $total_qty_relax                                    = sprintf('%0.8f',DetailMaterialPreparationFabric::where('material_preparation_fabric_id',$id)->sum('reserved_qty'));
                    $total_qty_outstanding                              = '0';
                }
                
                $material_preparation_fabric->total_qty_rilex       = $total_qty_relax;
                $material_preparation_fabric->total_qty_outstanding = $total_qty_outstanding;
                $material_preparation_fabric->remark_planning       = null;
                $material_preparation_fabric->save();
    
                DB::commit();

            }
            
            $_material_preparation_fabric                        = MaterialPreparationFabric::find($id);
            if($_material_preparation_fabric)
            {
                if($_material_preparation_fabric->total_reserved_qty == 0)
                {
                    if($_material_preparation_fabric->total_qty_rilex == 0 && $_material_preparation_fabric->detailMaterialPreparationFabric()->where('is_closing',false)->count() == 0)
                    {
                        $_material_preparation_fabric->delete();
                    }
                }
            }
            
            
            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

    }

    static function checkMaterialPreparationFabric($id)
    {
        $material_preparation_fabric = MaterialPreparationFabric::find($id);

        try 
        {
            DB::beginTransaction();

            if($material_preparation_fabric)
            {
                $material_preparation_fabric_id = $material_preparation_fabric->id;
                $c_order_id                     = $material_preparation_fabric->c_order_id;
                $item_id_book                   = $material_preparation_fabric->item_id_book;
                $item_id_source                 = $material_preparation_fabric->item_id_source;
                $_style                         = $material_preparation_fabric->_style;
                $warehouse_id                   = $material_preparation_fabric->warehouse_id;
                $article_no                     = $material_preparation_fabric->article_no;
                $planning_date                  = $material_preparation_fabric->planning_date;
                $total_reserved_qty             = sprintf("%.8f",$material_preparation_fabric->total_reserved_qty);
    
                $is_allocation_exists           = DetailMaterialPlanningFabric::where([
                    ['warehouse_id',$warehouse_id],
                    ['article_no',$article_no],
                    ['_style',$_style],
                    ['planning_date',$planning_date],
                    ['c_order_id',$c_order_id],
                    ['item_id_book',$item_id_book],
                    ['item_id_source',$item_id_source],
                ])
                ->exists();
    
                if(!$is_allocation_exists)
                {
                    $is_detail_exists = DetailMaterialPreparationFabric::where([
                        ['material_preparation_fabric_id',$material_preparation_fabric_id],
                        ['is_closing',false],
                    ])
                    ->exists();
    
                    $total_roll = $material_preparation_fabric->detailMaterialPreparationFabric()
                    ->where('is_closing',false)
                    ->count();

                    if(!$is_detail_exists && $total_roll == 0)
                    {
                        $material_preparation_fabric->delete();
                    }else
                    {
                        $material_preparation_fabric->total_reserved_qty    = 0;
                        $material_preparation_fabric->total_qty_outstanding = 0;
                        if ($material_preparation_fabric->remark_planning == NULL || '') {
                            $material_preparation_fabric->remark_planning       = 'please cancel this data due allocation is not longer exists';
                        }
                        $material_preparation_fabric->save();
                    }
                }
            }
            
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }
}
