<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Config;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\MaterialCheck;

class FabricReportDailyMaterialQualityControlController extends Controller
{
    public function index()
    {
        return view('fabric_report_daily_material_quality_control.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id           = $request->warehouse;
            $user_id                = ($request->user_id ? $request->user_id : auth::user()->id) ;
            $start_date             = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date               = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
            
            $daily_quality_control = DB::table('inspect_lab_qc_report_v')
            ->where(function($query) use($warehouse_id,$user_id){
                $query->where('warehouse_id','LIKE',"%$warehouse_id%")
                ->Where('user_id','LIKE',"%$user_id%");
            })
            ->whereBetween('inspection_date',[$start_date,$end_date])
            ->orderby('inspection_date','desc');
            
            return DataTables::of($daily_quality_control)
            ->editColumn('inspection_date',function ($daily_quality_control)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $daily_quality_control->inspection_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('receiving_date',function ($daily_quality_control)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $daily_quality_control->receiving_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('warehouse_id',function ($daily_quality_control)
            {
                if($daily_quality_control->warehouse_id == '1000001') return 'Warehouse fabric AOI 1';
                elseif($daily_quality_control->warehouse_id == '1000011') return 'Warehouse fabric AOI 2';
            })
            ->addColumn('action',function ($daily_quality_control)
            {
                return view('fabric_report_daily_material_quality_control._action',[
                    'model' => $daily_quality_control,
                    'detail' => route('fabricReportDailyMaterialQualityControl.detail',$daily_quality_control->id)
                ]);
            })
            
            ->make(true);
        }
    }

    public function detail(Request $request,$id)
    {
        echo 'detail belum dibuat';
    }

    public function export(Request $request)
    {
        $filename   = 'report_material_receivement';
        $file       = Config::get('storage.report') . '/' . e($filename).'.csv';
        
        if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';

        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function getUserPicklist(Request $request)
    {
        $user_id          = $request->user_id;
        $warehouse_id    = $request->warehouse_id;

        $users = MaterialCheck::join('users','users.id','material_checks.user_id')
        ->where('warehouse_id',$warehouse_id)
        ->groupby('users.name','users.id')
        ->pluck('users.name','users.id')
        ->all();
        
        return response()->json(['users' => $users,'user_id' => $user_id]);
    }
}
