<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;

use App\Models\User;
use App\Models\Item;
use App\Models\PoBuyer;
use App\Models\Temporary;
use App\Models\PoSupplier;
use App\Models\PurchaseItem;
use App\Models\MaterialStock;
use App\Models\AutoAllocation as AutoAllocations;
use App\Models\AllocationItem;
use App\Models\CuttingInstruction;
use App\Models\SummaryStockFabric;
use App\Models\MaterialRequirement;
use App\Models\MaterialPlanningFabric;
use App\Models\HistoryAutoAllocations;
use App\Models\HistoryPreparationFabric;
use App\Models\MaterialPreparationFabric;
use App\Models\DetailMaterialRequirement;
use App\Models\MaterialRequirementPerPart;
use App\Models\DetailMaterialPlanningFabric;
use App\Models\MaterialPlanningFabricPiping;
use App\Models\DetailMaterialPreparationFabric;

use App\Http\Controllers\MasterDataAutoAllocationController as AutoAllocation;
use App\Http\Controllers\AccessoriesMaterialStockApprovalController as HistoryStock;

class PoBuyerController extends Controller
{
    public function index()
    {
        return view('master_data_po_buyer.index');
    }

    public function data(Request $request)
    {
        if ($request->ajax())
        {
            $po_buyers = PoBuyer::select('po_buyer','lc_date','statistical_date','brand','promise_date'
            ,'type_stock','cancel_date','cancel_reason','cancel_user_id')
            ->orderby('lc_date','desc')
            ->groupBy('po_buyer','lc_date','statistical_date','brand','promise_date'
            ,'type_stock','cancel_date','cancel_reason','cancel_user_id');
            
            return DataTables::of($po_buyers)
            ->editColumn('cancel_user_id',function($po_buyers){
                return ($po_buyers->cancel_user_id)? $po_buyers->cancelUser->name : null;
            })
            ->editColumn('lc_date',function($po_buyers)
            {
                if($po_buyers->lc_date) return Carbon::parse($po_buyers->lc_date)->format('d/M/y');
                else null;
            })
            ->editColumn('statistical_date',function($po_buyers)
            {
                if($po_buyers->statistical_date) return Carbon::parse($po_buyers->statistical_date)->format('d/M/y');
                else null;
            })
            ->editColumn('promise_date',function($po_buyers)
            {
                if($po_buyers->promise_date) return Carbon::parse($po_buyers->promise_date)->format('d/M/y');
                else null;
            })
            ->editColumn('cancel_date',function($po_buyers)
            {
                if($po_buyers->cancel_date) return Carbon::parse($po_buyers->cancel_date)->format('d/M/y H:i:s');
                else null;
            })
            ->addColumn('action',function($po_buyers){
                return view('master_data_po_buyer._action', [
                    'model'     => $po_buyers,
                    'cancel'    => route('masterDataPoBuyer.cancel',$po_buyers->po_buyer)
                ]);
            })
            ->rawColumns(['is_selected','action'])
            ->make(true);
        }
    }

    public function cancelBulk()
    {
        return view('master_data_po_buyer.cancel');
    }

    public function uploadPSD()
    {
        return view('master_data_po_buyer.upload_psd');
    }

    public function exportCancelForm()
    {
        return Excel::create('upload_cancel_po_buyer',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','PO_BUYER');
                $sheet->setCellValue('B1','REASON');
 
                $sheet->setWidth('A', 15);
                $sheet->setWidth('B', 15);
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@'
                ));
            });
            $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }

    public function exportPSD()
    {
        return Excel::create('upload_psd_po_buyer',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','PO_BUYER');
                $sheet->setCellValue('B1','PSD');
 
                $sheet->setWidth('A', 15);
                $sheet->setWidth('B', 15);
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@'
                ));
            });
            $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }

    public function importCancelForm(Request $request)
    {
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
              'upload_file' => 'required|mimes:xls'
            ]);
  
            $path = $request->file('upload_file')->getRealPath();
            $result=[];
            $sheet = Excel::selectSheets('active')->load($path,function($render){return $render;})->getExcel()->getSheet();
            $row=2;
            
            try 
            {
                DB::beginTransaction();
                
                while(true)
                {
                    $_po_buyer            = $sheet->getCell('A'.$row)->getValue();
                    $cancel_reason        = $sheet->getCell('B'.$row)->getValue();
                    
                    if($_po_buyer==null && $cancel_reason==null)
                    {
                        break;
                    }else
                    {

                        $obj              =   new stdClass;
                        $obj->po_buyer    =   $_po_buyer;
                        $obj->reason      =   $cancel_reason;
                        
                        if($_po_buyer==null) $obj->status = 'PO BUYER TIDAK BOLEH KOSONG !';
                        else if ($cancel_reason==null) $obj->status = 'CANCEL REASON TIDAK BOLEH KOSONG';
                        else
                        {
                            if(PoBuyer::select('po_buyer')->where('po_buyer',$_po_buyer)->first()!=null)
                            {
                                Temporary::FirstOrCreate([
                                    'barcode'       => $_po_buyer,
                                    'status'        => 'sync_cancel_po_buyer',
                                    'text_1'        => $cancel_reason,
                                    'user_id'       => 1,
                                    'created_at'    => carbon::now(),
                                    'updated_at'    => carbon::now(),
                                ]);
                                $obj->status = 'BERHASIL';
                            }else
                            {
                                $obj->status = 'PO BUYER TIDAK DI TEMUKAN !';
                            }
                        }
                        
                        $result[]=$obj;
                    
                    }
                    $row++;
                }

                DB::commit();

            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            return response()->json($result,200);
        }
    }

    
    public function importPSD(Request $request)
    {
        if($request->hasFile('upload_file_psd'))
        {
            $validator = Validator::make($request->all(), [
              'upload_file_psd' => 'required|mimes:xls'
            ]);
  
            $path = $request->file('upload_file_psd')->getRealPath();
            $result=[];
            $sheet = Excel::selectSheets('active')->load($path,function($render){return $render;})->getExcel()->getSheet();
            $row=2;
            
            try 
            {
                DB::beginTransaction();
                
                while(true)
                {
                    $_po_buyer  = $sheet->getCell('A'.$row)->getValue();
                    $psd        = $sheet->getCell('B'.$row)->getValue();
                    $psd         = ( $psd ? Carbon::parse($psd)->format('Y-m-d') : null);
                    
                    if($_po_buyer==null && $psd==null)
                    {
                        break;
                    }else
                    {

                        $obj           =   new stdClass;
                        $obj->po_buyer =   $_po_buyer;
                        $obj->psd      =   $psd;
                        
                        if($_po_buyer==null) $obj->status = 'PO BUYER TIDAK BOLEH KOSONG';
                        else if ($psd==null) $obj->status = 'PSD TIDAK BOLEH KOSONG';
                        else
                        {
                            $po_buyer      = PoBuyer::where('po_buyer', $_po_buyer )->first();
                            $last_update   = carbon::parse($po_buyer->updated_at)->format('Y-m-d');
                            if($po_buyer)
                            {
                                $po_buyer->psd          = $psd;
                                PoBuyer::where('po_buyer', $_po_buyer)->update(['psd' => $psd]);
                                //$po_buyer->save();
                                //nggak kepakai di auto allokasi kali aja masih butuh
                                // $auto_allocations       = AutoAllocations::where('po_buyer', $_po_buyer)
                                //                           ->where('is_fabric', false)
                                //                           ->where('category', 'CT')
                                //                           ->whereNull('deleted_at')
                                //                           ->update(['psd' => $psd]);
                                $obj->status            = 'BERHASIL';
                                $obj->last_update       = $last_update;
                            }
                            else
                            {
                                $obj->status = 'PO BUYER TIDAK DITEMUKAN';
                            }

                        }
                        
                        $result[]=$obj;
                    
                    }
                    $row++;
                }

                DB::commit();

            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            return response()->json($result,200);
        }
    }

    public function cancelPoBuyer(Request $request,$po_buyer)
    {
        $_po_buyer          = $po_buyer;
        $cancel_reason      = strtoupper($request->cancel_reason);
        $start_date         = MaterialPlanningFabric::where('po_buyer',$_po_buyer)->min('planning_date');
        $end_date           = MaterialPlanningFabric::where('po_buyer',$_po_buyer)->max('planning_date');
       
        try 
        {
            DB::beginTransaction();

            $this->deletePoBuyer($cancel_reason,$_po_buyer);
            $this->deletePurchaseItem($_po_buyer,$cancel_reason);
            $this->deletePlanningFabrics($_po_buyer);
            $this->deleteAllocationItemFabrics($_po_buyer,$cancel_reason);
            AutoAllocation::cancelPoBuyer($cancel_reason,$_po_buyer);
            $this->storeCuttingInstructionHeaderReport($start_date,$end_date);
           
            DB::commit();

        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function syncPoCancel()
    {
        $movement_date  = carbon::now()->toDateTimeString();
        $data           = Temporary::where('status','sync_cancel_po_buyer')->take(1)->get();

        foreach ($data as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();
                $_po_buyer          = $value->barcode;
                $cancel_reason      = $value->text_1;

                $start_date         = MaterialPlanningFabric::where('po_buyer',$_po_buyer)->min('planning_date');
                $end_date           = MaterialPlanningFabric::where('po_buyer',$_po_buyer)->max('planning_date');
                
                PoBuyerController::deletePoBuyer($cancel_reason,$_po_buyer);
                PoBuyerController::deletePurchaseItem($_po_buyer,$cancel_reason);
                PoBuyerController::deletePlanningFabrics($_po_buyer);
                PoBuyerController::deleteAllocationItemFabrics($_po_buyer,$cancel_reason);
                AutoAllocation::cancelPoBuyer($cancel_reason,$_po_buyer);
                PoBuyerController::storeCuttingInstructionHeaderReport($start_date,$end_date);

                $value->delete();
                DB::commit();

            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
        
    }

    static function deletePoBuyer($cancel_reason,$_po_buyer)
    {
        $_po_buyer      = $_po_buyer;
        $cancel_reason  = $cancel_reason;

        try 
        {
            DB::beginTransaction();

            MaterialRequirement::where('po_buyer',$_po_buyer)->update(['qty_required' => 0]);
            MaterialRequirementPerPart::where('po_buyer',$_po_buyer)->update(['qty_required' => 0]);
            DetailMaterialRequirement::where('po_buyer',$_po_buyer)->update(['qty_required' => 0]);
            
            $user           = User::where('name','system')->first();


            PoBuyer::where('po_buyer',$_po_buyer)
            ->whereNull('cancel_date')
            ->update([
                'cancel_reason'     => $cancel_reason,
                'cancel_user_id'    => $user->id,
                'cancel_date'       => carbon::now()
            ]);

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function deleteAllocationItemFabrics($po_buyer,$cancel_reason)
    {
        /*$allocation_items = AllocationItem::where('po_buyer',$po_buyer)
        ->whereIn('warehouse',['1000011','1000001'])
        ->where(function($query){
            $query->where('confirm_by_warehouse','approved')
            ->OrWhereNull('confirm_by_warehouse');
        })
        ->whereNull('deleted_at')
        ->get();

        foreach ($allocation_items as $key => $allocation_item) 
        {
            // next buat update stock nya
            $allocation_item_id     = $allocation_item->id;
            DetailMaterialPlanningFabric::where('allocation_item_id',$allocation_item_id)->delete();

            $material_stock_id                  = $allocation_item->material_stock_id;
            $po_buyer                           = $allocation_item->po_buyer;
            $style                              = $allocation_item->style;
            $article_no                         = $allocation_item->article_no;
            $lc_date                            = $allocation_item->lc_date;
            $qty_booking                        = sprintf('%0.8f',$allocation_item->qty_booking);

            if($material_stock_id)
            {
                $material_stock                     = MaterialStock::find($material_stock_id);
                $stock                              = sprintf('%0.8f',$material_stock->stock);
                $reserved_qty                       = sprintf('%0.8f',$material_stock->reserved_qty);
                $old_available_qty                  = sprintf('%0.8f',$material_stock->available_qty);
                $_new_available_qty                 = sprintf('%0.8f',$old_available_qty + $qty_booking);
                $new_reserverd_qty                  = $reserved_qty - $qty_booking;
                $new_available_qty                  = $stock - $new_reserverd_qty;
                $curr_type_stock_erp_code           = $material_stock->type_stock_erp_code;
                $curr_type_stock                    = $material_stock->type_stock;

                $material_stock->reserved_qty       = sprintf('%0.8f',$new_reserverd_qty);
                $material_stock->available_qty      = sprintf('%0.8f',$new_available_qty);

                if($new_available_qty > 0)
                {
                    $material_stock->is_allocated   = false;
                    $material_stock->is_active      = true;
                }

                HistoryStock::approved($material_stock_id
                ,$_new_available_qty
                ,$old_available_qty
                ,(-1*$qty_booking)
                ,$po_buyer
                ,$style
                ,$article_no
                ,$lc_date
                ,'PO CANCEL, '.$cancel_reason
                ,auth::user()->name
                ,auth::user()->id
                ,$curr_type_stock_erp_code
                ,$curr_type_stock
                ,false
                ,true );

                $material_stock->save();
            }

            $allocation_item->delete();
        }*/
    }

    static function deletePurchaseItem($po_buyer,$cancel_reason)
    {
        /*$purchase_items =  PurchaseItem::where([
            ['po_buyer',$po_buyer],
        ])
        ->whereNull('cancel_user_id')
        ->get();

        try 
        {
            DB::beginTransaction();
            
            foreach ($purchase_items as $key => $purchase_item) 
            {
                if($purchase_item->counter_out == 0 && $purchase_item->counter_handover == 0)
                {
                    $item           = Item::where('item_code',$purchase_item->item_code)->first();
                    $_po_buyer      = PoBuyer::where('po_buyer',$purchase_item->po_buyer)->first();
                    $po_supplier    = PoSupplier::where([
                        [db::raw('upper(document_no)'),strtoupper($purchase_item->document_no)],
                        ['c_bpartner_id',$purchase_item->c_bpartner_id],
                    ])
                    ->first();
                    
                    HistoryAutoAllocations::Create([
                        'purchase_item_id'          => $purchase_item->id,
                        'document_no_old'           => $purchase_item->document_no,
                        'c_bpartner_id_old'         => $purchase_item->c_bpartner_id,
                        'item_code_old'             => $purchase_item->item_code,
                        'uom_old'                   => $purchase_item->uom,
                        'item_id_old'               => ($item) ? $item->item_id : null,
                        'item_source_id_old'        => ($item) ? $item->item_id : null,
                        'item_code_source_old'      => $purchase_item->item_code,
                        'po_buyer_old'              => $purchase_item->po_buyer,
                        'supplier_name_old'         => ($po_supplier) ? ($po_supplier->supplier_id)? $po_supplier->supplier->supplier_name :null : null,
                        'qty_allocated_old'         => $purchase_item->qty,
                        'warehouse_id_old'          => $purchase_item->warehouse,
                        'type_stock_erp_code_old'   => ($_po_buyer)? $_po_buyer->type_stock_erp_code : null,
                        'type_stock_old'            => ($_po_buyer)? $_po_buyer->type_stock : null,
                        'note_code'                 => '2',
                        'note'                      => 'CANCEL PO BUYER DUE MATERIAL NOT RECEIVE YET',
                        'operator'                  => -1*$purchase_item->qty,
                        'remark'                    => strtoupper($cancel_reason),
                        'is_integrate'              => false,
                        'lc_date'                   => ($_po_buyer)? $_po_buyer->lc_date : null,
                        'c_order_id'                => ($po_supplier) ? $po_supplier->c_order_id : null,
                        'user_id'                   => auth::user()->id
                    ]);

                    //$purchase_item->is_stock_already_created = true;
                }

                $purchase_item->cancel_user_id = auth::user()->id;
                $purchase_item->status_po_buyer = 'cancel';
                $purchase_item->cancel_date = carbon::now();
                $purchase_item->save();
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }*/
    }

    static function deletePlanningFabrics($_po_buyer)
    {
        try 
        {
            DB::beginTransaction();

            $detail_material_plannings = DetailMaterialPlanningFabric::select('c_order_id'
                ,'article_no'
                ,'warehouse_id'
                ,'item_id_book'
                ,'item_id_source'
                ,'_style'
                ,'uom'
                ,'planning_date'
                ,'is_piping'
                ,db::raw("sum(qty_booking) as total_booking"))
            ->where('po_buyer','!=',$_po_buyer)
            ->groupby('c_order_id'
                ,'warehouse_id'
                ,'uom'
                ,'item_id_book'
                ,'item_id_source'
                ,'_style'
                ,'article_no'
                ,'planning_date'
                ,'is_piping')
            ->get();

            $movement_date  = carbon::now()->toDateTimeString();
            
            foreach ($detail_material_plannings as $key => $detail_material_planning) 
            {
                
                $c_order_id_preparation         = $detail_material_planning->c_order_id;
                $_style_preparation             = $detail_material_planning->_style;
                $_article_no_preparation        = $detail_material_planning->article_no;
                $_document_no_preparation       = $detail_material_planning->document_no;
                $_warehouse_id_preparation      = $detail_material_planning->warehouse_id;
                $_item_id_book                  = $detail_material_planning->item_id_book;
                $_item_id_source_preparation    = $detail_material_planning->item_id_source;
                $_item_code                     = $detail_material_planning->item_code;
                $_item_code_source_preparation  = $detail_material_planning->item_code_source;
                $_planning_date_preparation     = $detail_material_planning->planning_date->format('d/M/y');
                $_is_piping_preparation         = $detail_material_planning->is_piping;
                $_uom                           = $detail_material_planning->uom;
                $_total_booking_preparation     = sprintf('%0.8f',$detail_material_planning->total_booking);

                $is_material_preparation_exists = MaterialPreparationFabric::where([
                    ['c_order_id',$c_order_id_preparation],
                    ['item_id_book',$_item_id_book],
                    ['item_id_source',$_item_id_source_preparation],
                    ['_style',$_style_preparation],
                    ['article_no',$_article_no_preparation],
                    ['warehouse_id',$_warehouse_id_preparation],
                    ['planning_date',$_planning_date_preparation],
                    ['is_piping',$_is_piping_preparation],
                ])
                ->first();

                if($is_material_preparation_exists)
                {
                    $total_reserved_qty     = sprintf('%0.8f',$is_material_preparation_exists->total_reserved_qty);
                    $total_qty_relax        = sprintf('%0.8f',$is_material_preparation_exists->total_qty_rilex);
                    $curr_remark_planning   = $is_material_preparation_exists->remark_planning;

                    if($_total_booking_preparation != $total_reserved_qty)
                    {
                        $new_qty_oustanding = sprintf('%0.8f',$_total_booking_preparation - $total_qty_relax);
                       
                        if($new_qty_oustanding < 0) $remark_planning = 'pengurangan qty alokasi';
                        else $remark_planning = 'penambahan qty alokasi';

                        HistoryPreparationFabric::create([
                            'material_preparation_fabric_id'    => $is_material_preparation_exists->id,
                            'remark'                            => $remark_planning.'. Po buyer '.$_po_buyer.' Cancel',
                            'qty_before'                        => $total_reserved_qty,
                            'qty_after'                         => $_total_booking_preparation,
                            'created_at'                        => $movement_date,
                            'updated_at'                        => $movement_date,
                        ]);

                        $is_material_preparation_exists->total_reserved_qty     = $_total_booking_preparation;
                        $is_material_preparation_exists->total_qty_outstanding  = $new_qty_oustanding;
                        $is_material_preparation_exists->remark_planning        = ( $curr_remark_planning ? $curr_remark_planning .', Po Buyer '.$_po_buyer.' cancel please reduce qty '.(-1*$new_qty_oustanding).'('.$_uom.') from preparation and do re-preparation roll to buyer' : 'Po Buyer '.$_po_buyer.' cancel please reduce qty '.(-1*$new_qty_oustanding).'('.$_uom.') from preparation and do re-preparation roll to buyer' );
                        $is_material_preparation_exists->save();
                    }
                }
            }
            

            MaterialPlanningFabric::where('po_buyer',$_po_buyer)->delete();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function storeCuttingInstructionHeaderReport($start_date,$end_date)
    {

        $material_planning_fabrics = MaterialPlanningFabric::select(db::raw("substr(job_order,0,POSITION ( ':' IN job_order )) AS style")
            ,'article_no'
            ,'planning_date'
            ,'uom'
            ,'warehouse_id'
            ,db::raw("string_agg ( distinct item_code, ',' ) AS item_code")
            ,db::raw("string_agg ( distinct po_buyer, ',' ) AS po_buyer")
            ,db::raw("sum(qty_consumtion) as qty_need"))
        ->whereBetween('planning_date',[$start_date,$end_date])
        ->groupby(db::raw("substr(job_order,0,POSITION ( ':' IN job_order ))")
            ,'uom'
            ,'article_no'
            ,'warehouse_id'
            ,'planning_date')
        ->get();
        
        try 
        {
            DB::beginTransaction();

            CuttingInstruction::whereBetween('planning_date',[$start_date,$end_date])->delete();

            $movement_date = carbon::now()->toDateTimeString();
            foreach ($material_planning_fabrics as $key => $material_planning_fabric) 
            {
                $qty_need       = sprintf("%.8f",$material_planning_fabric->qty_need);
                $style          = $material_planning_fabric->style;
                $article_no     = $material_planning_fabric->article_no;
                $uom            = $material_planning_fabric->uom;
                $po_buyer       = $material_planning_fabric->po_buyer;
                $item_code      = $material_planning_fabric->item_code;
                $warehouse_id   = $material_planning_fabric->warehouse_id;
                $planning_date  = $material_planning_fabric->planning_date;
                
                $is_exists = CuttingInstruction::where([
                    ['article_no',$article_no],
                    ['style',$style],
                    ['uom',$uom],
                    ['item_code',$item_code],
                    ['planning_date',$planning_date],
                    ['po_buyer',$po_buyer],
                    ['qty_need',$qty_need],
                    ['warehouse_id',$warehouse_id],
                ])
                ->exists();

                if(!$is_exists)
                {
                    CuttingInstruction::FirstOrCreate([
                        'created_at'        => $movement_date,
                        'updated_at'        => $movement_date,
                        'article_no'        => $article_no,
                        'uom'               => $uom,
                        'style'             => $style,
                        'item_code'         => $item_code,
                        'planning_date'     => $planning_date,
                        'po_buyer'          => $po_buyer,
                        'qty_need'          => $qty_need,
                        'warehouse_id'      => $warehouse_id,
                    ]);
                }
                
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }
}
