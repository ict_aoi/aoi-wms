<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use File;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Models\Mrp;
use App\Models\WmsOld;
use App\Models\Locator;
use App\Models\PoBuyer;
use App\Models\Temporary;
use App\Models\PoBuyerLog;
use App\Models\ReroutePoBuyer;
use App\Models\RackAutomation;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\MaterialRequirement;
use App\Models\MaterialPreparation;
use App\Models\MaterialMovementLine;
use App\Models\MaterialReadyPreparation;
use App\Models\DetailMaterialPreparation;

class AccessoriesMaterialInController extends Controller
{
    public function index()
    {
       /* $temporaries    = Temporary::where('user_id',Auth::user()->id)->where('status','in')->get();
        $return_array   = array();

        if($temporaries->count() != 0)
        {
            foreach ($temporaries as $key => $temporary)
            {
                $return_array [] = $this->getDataBarcode($temporary->barcode,$temporary->barcode);;
            }
        }*/

        $return_array = '[]';
        return view('accessories_material_in.index',compact('return_array'));
        // return view('errors.migration');
    }

    public function create(Request $request)
    {
        $barcode        = trim($request->barcode);
        $_warehouse_id  = $request->warehouse_id;
        $has_dash       = strpos($barcode, "-");
        if($has_dash == false)
        {
            $_barcode = $barcode;
            $flag = 1;
        }else
        {
            $split = explode('-',$barcode);
            $_barcode = $split[0];
            $flag = 0;
        }

        $array = array();
        $array = $this->getDataBarcode($_barcode,$barcode,$flag,$_warehouse_id);

        if(isset($array->original)) return response()->json($array->original,422);
        return response()->json($array,200);
    }

    public function rackPicklist(Request $request)
    {
        $q              = strtoupper($request->q);
        $_warehouse_id  = $request->warehouse_id;

        $lists = Locator::
        select(
            'id',
            'area_id',
            'rack',
            'y_row',
            'z_column',
            'has_many_po_buyer',
            'barcode',
            'counter_in',
            'last_po_buyer'
        )
        ->whereHas('area',function($query) use ($_warehouse_id)
        {
            $query->where([
                ['warehouse',$_warehouse_id],
                ['is_active',true],
                ['is_destination',false],
                ['name','<>','ADJUSTMENT'],
                ['name','<>','RECEIVING'],
                ['name','<>','BOM'],
                ['name','<>','QC'],
                ['name','<>','FREE STOCK'],
                ['name','<>','SUPPLIER'],
                ['name','<>','SWITCH'],
            ]);
        })
        ->where([
            ['locators.is_active',true],
            ['locators.rack','<>','RJC'],
            ['locators.rack','<>','ERP INVENTORY'],
        ])
        ->where(function($query) use($q){
            $query->where('barcode','like',"%$q%")
            ->orWhere('locators.code','like',"%$q%");
        })
        ->orderBy('rack','asc')
        ->orderBy('y_row','asc')
        ->orderBy('z_column','asc')
        ->paginate(10);

        return view('accessories_material_in._rack_list',compact('lists'));
    }

    private function getDataBarcode($barcode,$scanned_barcode,$has_dash,$_warehouse_id)
    {
        if($barcode =='BELUM DI PRINT') return response()->json('Barcode not printed yet.',422);
        
        $material_preparation = MaterialPreparation::where([
            ['barcode',strtoupper($barcode)],
            ['warehouse',$_warehouse_id]
        ])
        ->whereNull('deleted_at')
        ->first();

        if(!$material_preparation) return response()->json('Barcode not found.',422);
        if($material_preparation->qc_status == null && $material_preparation->is_additional == false && $material_preparation->is_from_handover == false && $material_preparation->is_from_barcode_bom == false)
        {
            return response()->json('This barcode has not been checked QC .( Barcode ini belum dicek oleh QC ) ',422);   
        }
        if($material_preparation->qc_status == null && $material_preparation->is_additional == false && $material_preparation->is_from_barcode_bom = true && $material_preparation->category != 'ST')
        {
            if(substr($material_preparation->item_code, 0, 8) != '62002417')
            {
                return response()->json('This barcode has not been checked QC .( Barcode ini belum dicek oleh QC ) ',422);
            }
        }
        if($material_preparation->is_need_to_be_switch) return response()->json('Item is book to another Po buyer, please give it to admin to be prepared again(Item ini dipinjamkan untuk po buyer lain, silahkan berikan ke admin untuk di prepare ulang)..',422);
        if($material_preparation->is_moq) return response()->json('Item has MOQ on it, please make adjustments first(Item ini terkena moq, silahkan lakukan adjustment terlebih dahulu).',422);
        if($material_preparation->is_reduce) return response()->json('Qty for this item need REDUCE due reduce qty on bom, please make adjustments first(Item ini terkena reduce, silahkan lakukan adjustment terlebih dahulu).',422);
        if($material_preparation->qc_status == 'HOLD') return response()->json('The QC results for this item are HOLD, please contact QC to find out the actual results (Hasil qc item ini HOLD, silahkan hubungi QC untuk mengetahui hasil aktualnya).',422);
        if($material_preparation->qc_status == 'FULL REJECT') return response()->json('The QC results of this item are reject, the item is not used, please wait for a replacement (Hasil QC item ini ditolak, sudah tidak digunakan silahkan tunggu pengantinya).',422);

        if($material_preparation->warehouse != $_warehouse_id)
        {
            if($material_preparation->warehouse == 1000002) $_temp = 'Accessories AOI 1';
            else if($material_preparation->warehouse == 1000013) $_temp = 'Accessories AOI 2';

            return response()->json('Warehouse on barcode '.$_temp.', not same with your warehouse.',422);
        }

        if($material_preparation->total_carton > 1)
        {
            if($has_dash == 1) return response()->json('Wrong barcode, Total carton more than 1',422);
        }

        if($material_preparation->last_status_movement == 'out' 
            || $material_preparation->last_status_movement == 'reroute' 
            || $material_preparation->last_status_movement == 'cancel order' 
            || $material_preparation->last_status_movement == 'cancel item') 

            return response()->json('Item already supplied to '.$material_preparation->lastLocator->code,422);

        $data_locator = $material_preparation->Locator($material_preparation->po_buyer,$material_preparation->warehouse);
        
        if($data_locator)
        {
            if(isset($data_locator->original)) return response()->json($data_locator->original,422);

            if($data_locator)
            {
                $locator_id     = $data_locator->id;
                $locator_code   = $data_locator->code;
            }else
            {
                $locator_id     = null;
                $locator_code   = null;
            }
            //$locator_name_automation = ($material_preparation->Locator($material_preparation->po_buyer))? $material_preparation->Locator($material_preparation->po_buyer)->locator_name_automation: null;
        }

        $detail_array[] = [
            'barcode_supplier' => $scanned_barcode
        ];

        $document_no                = $material_preparation->document_no;
        $material_preparation_id    = $material_preparation->id;
        $po_buyer                   = $material_preparation->po_buyer;
        $item_id                    = $material_preparation->item_id;
        $item_code                  = $material_preparation->item_code;
        $item_desc                  = $material_preparation->item_desc;
        $category                   = $material_preparation->category;
        $type_po                    = $material_preparation->type_po;
        $job_order                  = $material_preparation->job_order;
        $total_carton               = $material_preparation->total_carton;
        $is_backlog                 = $material_preparation->is_backlog;
        $uom_conversion             = $material_preparation->uom_conversion;
        $qty_conversion             = $material_preparation->qty_conversion;
        $last_locator_id            = $material_preparation->last_locator_id;
        $last_status_movement       = $material_preparation->last_status_movement;
        $last_locator_code          = $material_preparation->lastLocator->code;
        $last_locator_erp_id        = $material_preparation->lastLocator->area->erp_id;
        $warehouse_user             = $_warehouse_id;
        $style                      = $material_preparation->style;
        $article_no                 = $material_preparation->article_no;

        $is_out_subcont = MaterialPreparation::where([
            ['po_buyer', $po_buyer],
            ['last_status_movement', 'out-subcont'],
        ])
        ->exists();
        
        $material_requirement = MaterialRequirement::where([
            ['po_buyer',$po_buyer],
            ['item_id',$item_id],
            ['style',$style],
            ['article_no',$article_no]
        ])
        ->first();
//ketemu
        $_warehouse_place       = $_warehouse_id;
        $is_need_to_handover    = false;

        if($material_requirement)
        {
            if($material_requirement->warehouse_id)
            {
                $warehouse_place = $material_requirement->warehouse_id;
                if($warehouse_place == '1000007') $_warehouse_place = '1000002';
                else if($warehouse_place == '1000010') $_warehouse_place = '1000013';
                else $_warehouse_place = $_warehouse_id;

                if($_warehouse_place != $_warehouse_id) $is_need_to_handover = true;
                else $is_need_to_handover = false;
            } 
            else $is_need_to_handover = false;
        }

        $_is_exist_prepare = MaterialPreparation::where([
            ['po_buyer',$po_buyer],
            ['warehouse',$_warehouse_id]
        ]);

        if($_warehouse_id == '1000013')
        {
            $_is_exist_prepare = $_is_exist_prepare->where(function($query){
                $query->where('last_status_movement','out')
                ->OrWhere('last_status_movement','out-handover');
            })
            ->whereIn('last_locator_id',function($query){
                $query->select('id')
                ->from('locators')
                ->where([
                    ['rack','LINE'],
                    ['warehouse','1000013']
                ]);
            });
        }else
        {
            $_is_exist_prepare = $_is_exist_prepare->where(function($query){
                $query->where('last_status_movement','out')
                ->OrWhere('last_status_movement','out-handover');
            })
            ->whereIn('last_locator_id',function($query){
                $query->select('id')
                ->from('locators')
                ->where([
                    ['rack','LINE'],
                    ['warehouse','1000002']
                ]);
            });
        }

        $_is_exist_prepare = $_is_exist_prepare->exists();

        $_tujuan_out = '';
        if($_is_exist_prepare == true &&  $_warehouse_id == '1000013')
        {
            $_tujuan_out =  MaterialPreparation::select('item_code')
            ->where([['po_buyer',$po_buyer],['warehouse','1000013'],['last_status_movement','out']])
            ->leftJoin('locators','material_preparations.last_locator_id','locators.id')
            ->whereIn('last_locator_id',function($query){
                $query->select('id')
                ->from('locators')
                ->where([
                    ['rack','LINE'],
                    ['warehouse','1000013']
                ]);
            })
            ->first();
            $_tujuan_out = $_tujuan_out['item_code'];
        }

        if($is_out_subcont == true) 
        {
            $_is_exist_prepare = true;
            $is_need_to_out_subcont = true;
            $_tujuan_out =  MaterialPreparation::select('item_code')
            ->where([['po_buyer',$po_buyer],['warehouse','1000013'],['last_status_movement','out-subcont']])
            ->leftJoin('locators','material_preparations.last_locator_id','locators.id')
            ->whereIn('last_locator_id',function($query){
                $query->select('id')
                ->from('locators')
                ->where([
                    ['rack','BBI SEMARANG'],
                ]);
            })
            ->first();
            $_tujuan_out = $_tujuan_out['item_code'];
        }
        //dd($_tujuan_out);

        //alokasi subcont / beda whs 
        $erp = DB::connection('erp');
        $sewing_place = $erp->table('bw_report_material_requirement_new_v1')->where([
            ['poreference',$po_buyer],
            ['componentid',$item_id]
        ])
        ->select('warehouse_place')
        ->first();

        $sp = null;
        if($sewing_place != null)
        {
            $sp = $sewing_place->warehouse_place;
        }else{
            $sp = null;
        }


        $master_po_buyer        = PoBuyer::where('po_buyer',$po_buyer)->first();
        $is_po_buyer_cancel     = ($master_po_buyer ? ($master_po_buyer->cancel_reason? true :false) :false);
        
        $obj                            = new StdClass();
        $obj->material_preparation_id   = $material_preparation_id;
        $obj->code                      = $item_code;
        $obj->desc                      = $item_desc;
        $obj->category                  = $category;
        $obj->type_po                   = $type_po;
        $obj->uom_conversion            = $uom_conversion;
        $obj->job                       = $job_order;
        $obj->material_out              = ($_is_exist_prepare==false)?null:'true_wms_new';
        $obj->style                     = $style;
        $obj->article_no                = $article_no;
        $obj->po_buyer                  = $po_buyer;
        $obj->locator_id                = ($is_po_buyer_cancel)?'':$locator_id;
        $obj->locator                   = ($is_po_buyer_cancel)?'':$locator_code;
        $obj->is_reroute                = false;
        $obj->is_others_already_out     = $_is_exist_prepare;
        $obj->material_movement_id      = null;
        $obj->material_movement_line_id = null;
        $obj->source_rak_id             = $last_locator_id;
        $obj->source_rak_code           = $last_locator_code;
        $obj->source_rak_erp_id         = $last_locator_erp_id;
        $obj->qty_need                  = ($material_requirement) ? $material_requirement->qty_need : 0;
        $obj->qty_receive               = $qty_conversion;
        $obj->barcode                   = $barcode;
        $obj->statistical_date          = ($master_po_buyer)? ($master_po_buyer->statistical_date)? $master_po_buyer->statistical_date->format('d-M-Y') : null : null;
        $obj->total_carton              = $total_carton;
        $obj->is_po_buyer_cancel        = $is_po_buyer_cancel;
        $obj->last_status_movement      = $last_status_movement;
        $obj->is_backlog                = $is_backlog;
        $obj->warehouse_place           = $_warehouse_place;
        $obj->is_need_to_handover       = $is_need_to_handover;
        $obj->counter                   = 1;
        $obj->details                   = $detail_array;
        $obj->season                    = ($material_requirement) ? $material_requirement->season : null;
        $obj->ict_log                   = $obj->is_backlog==true? $material_preparation->ict_log.' '.strtoupper('Qty '.$qty_conversion.' ('.$uom_conversion.') yang berasal dari po '.$document_no.' adalah backlog'):$material_preparation->ict_log;
        $obj->movement_date             = Carbon::now()->toDateTimeString();
        $obj->error_note                = null;
        $obj->tujuan_out                = $_tujuan_out;
        $obj->sewing_place              = $sp;
        
        /*if($locator_name_automation != null && auth::user()->warehouse == '1000013'){
            $ip_address = auth::user()->ip_address;
            
            if($ip_address == '192.168.72.159')
                $group_id = 1;
            elseif($ip_address == '192.168.72.111')
                $group_id = 2;
            elseif($ip_address == '192.168.72.151')
                $group_id = 3;
            else
                $group_id = 4;

            $obj_automation = new stdClass();
            $obj_automation->locator_name = $locator_name_automation;
            $obj_automation->po_buyer = $material_preparation->po_buyer;
            $obj_automation->group_id = $group_id;
            $insert_automations [] = $obj_automation;

            //$this->insertIntoAutomationRack($insert_automations);
        }*/
       
        
        return $obj;
    }

    public function store(Request $request)
    {
        $_warehouse_id      = $request->warehouse_id;
        $po_buyer           = $request->__po_buyer;
        $to_destination     = $request->_from_rackin_id;
        $has_many_po_buyer  = trim($request->has_many_po_buyer);
        $items              = json_decode($request->barcode_products);
        $insert_automations = array();
        $array_move         = array();
        $flag_move          = 0;
        $concatenate        = '';   

        $from_location = Locator::with('area')
        ->where('is_active',true)
        ->whereHas('area',function ($query) use($_warehouse_id)
        {
            $query->where('is_destination',false);
            $query->where('is_active',true);
            $query->where('warehouse',$_warehouse_id);
            $query->where('name','RECEIVING');
        })
        ->first();

        $validator = Validator::make($request->all(), [
            'barcode_products' => 'required|not_in:[]'
        ]);

        if($validator->passes())
        {
            $counter = 0;

            //return response()->json($items);
            $return_error = array();
            foreach ($items as $key => $item) 
            {
                try 
                {
                    DB::beginTransaction();
                    $counter++;
                    $curr_po_buyer              = $item->po_buyer;
                    $material_preparation_id    = $item->material_preparation_id;
                    $locator_destination        = Locator::find($to_destination);
                    $locator_destination_name   = $locator_destination->rack.'.'.$locator_destination->y_row.'.'.$locator_destination->z_column;
    
                    if($locator_destination)
                    {
                        $has_po_buyer = $locator_destination->has_many_po_buyer;
                       
                        if(!$has_po_buyer)
                        {
                            $active_po_buyer = $locator_destination->last_po_buyer;
                           
                            if($active_po_buyer)
                            {
                                if($active_po_buyer == $curr_po_buyer)
                                {
                                    $this->insertMovement($item,$locator_destination,$from_location);
                                }else
                                {
                                    $item->error_note = 'RAK '.$locator_destination_name.' SUDAH DIISI OLEH PO BUYER '.$active_po_buyer;
                                    $return_error [] = $item;
                                }
                            }else
                            {
                                $locator_destination->last_po_buyer = $curr_po_buyer;
                                $locator_destination->save();
    
                                $this->insertMovement($item,$locator_destination,$from_location);
                            }
                        }else
                        {
                           $this->insertMovement($item,$locator_destination,$from_location);
                        } 
    
                    
                    }
    
                    $concatenate .= "'" .$material_preparation_id."',";
                    DB::commit();
                   
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }

            try 
            {
                DB::beginTransaction();
                $concatenate = substr_replace($concatenate, '', -1);
                if($concatenate !='')
                {
                    DB::select(db::raw("SELECT * FROM delete_duplicate_movement(array[".$concatenate."]);"));
                }
                
                $total_counter = MaterialPreparation::where('last_locator_id',$to_destination)
                ->whereNull('deleted_at')
                ->where(function($query){
                    $query->where('last_status_movement','in')
                    ->orWhere('last_status_movement','change');
                })
                ->count();


                $locator                = Locator::find($to_destination);
                $locator->counter_in    = $total_counter;
                
                if($total_counter == 0) $locator->last_po_buyer = null;
                $locator->save();
                
                /*if($locator_destination){
                    $counter_in = $locator->counter_in;
                    if ($counter_in == null)
                        $counter_in = $counter;
                    else
                        $counter_in += $counter;

                    $locator->counter_in = $counter_in;
                    $locator->save();
                }*/
            

                //$this->insertIntoAutomationRack($insert_automations);
                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
            
            
                
            return response()->json($return_error,200);
        }else
        {
            return response()->json('data tidak ditemukan.',422);
        }
    }

    static function insertMovement($item,$to_destination,$from_location)
    {
        $from_location_id               = $from_location->id;
        $from_location_erp_id           = $from_location->area->erp_id;
        $to_location_id                 = $to_destination->id;
        $to_location_erp_id             = $to_destination->area->erp_id;
        $material_movement_line_id      = $item->material_movement_line_id;
        $source_rack_id                 = $item->source_rak_id;
        $material_preparation_id        = $item->material_preparation_id;
        $po_buyer                       = $item->po_buyer;
        $item_code                      = $item->code;
        $last_status_movement           = $item->last_status_movement;
        $type_po                        = $item->type_po;
        $qty_receive                    = $item->qty_receive;
        $ict_log                        = $item->ict_log;
        $movement_date                  = ($item->movement_date) ? $item->movement_date : Carbon::now()->toDateTimeString();
        $material_preparation           = MaterialPreparation::find($material_preparation_id);
            
        if($material_preparation->po_detail_id == 'FREE STOCK' || $material_preparation->po_detail_id == 'FREE STOCK-CELUP')
        {
            $c_order_id             = 'FREE STOCK';
            $no_packing_list        = '-';
            $no_invoice             = '-';
            $c_orderline_id         = '-';
        }else
        {
            $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
            ->whereNull('material_roll_handover_fabric_id')
            ->where('po_detail_id',$material_preparation->po_detail_id)
            ->first();

            $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
            $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
            $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
        }

        if(($last_status_movement == 'in' || $last_status_movement == 'change') && $to_destination != $source_rack_id)
        {
            $last_status_movement                   = 'change';

            $is_movement_in_exists = MaterialMovement::where([
                ['from_location',$source_rack_id],
                ['to_destination',$to_location_id],
                ['from_locator_erp_id',$from_location_erp_id],
                ['to_locator_erp_id',$to_location_erp_id],
                ['po_buyer',$po_buyer],
                ['is_integrate',false],
                ['is_active',true],
                ['no_packing_list',$no_packing_list],
                ['no_invoice',$no_invoice],
                ['status','change'],
            ])
            ->first();

            if(!$is_movement_in_exists)
            {
                $material_in_movement = MaterialMovement::firstorcreate([
                    'from_location'         => $source_rack_id,
                    'to_destination'        => $to_location_id,
                    'from_locator_erp_id'   => $from_location_erp_id,
                    'to_locator_erp_id'     => $to_location_erp_id,
                    'is_integrate'          => false,
                    'is_active'             => true,
                    'po_buyer'              => $po_buyer,
                    'status'                => 'change',
                    'no_packing_list'       => $no_packing_list,
                    'no_invoice'            => $no_invoice,
                    'created_at'            => $movement_date,
                    'updated_at'            => $movement_date,

                ]);
                $material_in_movement_id = $material_in_movement->id;
            }else
            {
                $is_movement_in_exists->updated_at = $movement_date;
                $is_movement_in_exists->save();

                $material_in_movement_id = $is_movement_in_exists->id;
            }

            $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
            ->where([
                ['material_movement_id',$material_in_movement_id],
                ['material_preparation_id',$material_preparation->id],
                ['qty_movement',$material_preparation->qty_conversion],
                ['item_id',$material_preparation->item_id],
                ['warehouse_id',$material_preparation->warehouse],
                ['date_movement',$movement_date],
                ['is_active',true],
                ['is_integrate',false],
            ])
            ->exists();

            if(!$is_material_movement_line_exists)
            {
                $material_in_movement_line = MaterialMovementLine::firstorcreate([
                    'material_movement_id'          => $material_in_movement_id,
                    'material_preparation_id'       => $material_preparation->id,
                    'item_id'                       => $material_preparation->item_id,
                    'item_id_source'                => $material_preparation->item_id_source,
                    'item_code'                     => $material_preparation->item_code,
                    'type_po'                       => $material_preparation->type_po,
                    'c_order_id'                    => $material_preparation->c_order_id,
                    'c_bpartner_id'                 => $material_preparation->c_bpartner_id,
                    'supplier_name'                 => $material_preparation->supplier_name,
                    'c_orderline_id'                => $c_orderline_id,
                    'uom_movement'                  => $material_preparation->uom_conversion,
                    'qty_movement'                  => $material_preparation->qty_conversion,
                    'date_receive_on_destination'   => $movement_date,
                    'date_movement'                 => $movement_date,
                    'created_at'                    => $movement_date,
                    'updated_at'                    => $movement_date,
                    'nomor_roll'                    => '-',
                    'is_active'                     => true,
                    'is_integrate'                  => false,
                    'warehouse_id'                  => $material_preparation->warehouse,
                    'document_no'                   => $material_preparation->document_no,
                    'user_id'                       => auth::user()->id
                ]);
            }

            $locator_change_rack = Locator::find($source_rack_id);

            if($locator_change_rack)
            {
                $counter_minus = $locator_change_rack->counter_in;
                if($counter_minus > 0)
                {
                    $_conter_minus = ($counter_minus - 1);
                    $locator_change_rack->counter_in = $_conter_minus;
                    if($_conter_minus == 0) $locator_change_rack->last_po_buyer = null;
                } 
                else
                {
                    $locator_change_rack->counter_in    = 0;
                    $locator_change_rack->last_po_buyer = null;
                } 

                $locator_change_rack->save();
            }
        
        }else
        {
            $last_status_movement = 'in';

            $is_movement_in_exists = MaterialMovement::where([
                ['from_location',$source_rack_id],
                ['to_destination',$to_location_id],
                ['from_locator_erp_id',$from_location_erp_id],
                ['to_locator_erp_id',$to_location_erp_id],
                ['po_buyer',$po_buyer],
                ['is_integrate',false],
                ['is_active',true],
                ['no_packing_list',$no_packing_list],
                ['no_invoice',$no_invoice],
                ['status','in'],
            ])
            ->first();

            if(!$is_movement_in_exists)
            {
                $material_in_movement = MaterialMovement::firstorcreate([
                    'from_location'         => $source_rack_id,
                    'to_destination'        => $to_location_id,
                    'from_locator_erp_id'   => $from_location_erp_id,
                    'to_locator_erp_id'     => $to_location_erp_id,
                    'is_integrate'          => false,
                    'is_active'             => true,
                    'po_buyer'              => $po_buyer,
                    'status'                => 'in',
                    'no_packing_list'       => $no_packing_list,
                    'no_invoice'            => $no_invoice,
                    'created_at'            => $movement_date,
                    'updated_at'            => $movement_date,

                ]);
                $material_in_movement_id = $material_in_movement->id;
            }else
            {
                $is_movement_in_exists->updated_at = $movement_date;
                $is_movement_in_exists->save();

                $material_in_movement_id = $is_movement_in_exists->id;
            }

            $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
            ->where([
                ['material_movement_id',$material_in_movement_id],
                ['material_preparation_id',$material_preparation->id],
                ['qty_movement',$material_preparation->qty_conversion],
                ['item_id',$material_preparation->item_id],
                ['warehouse_id',$material_preparation->warehouse],
                ['date_movement',$movement_date],
                ['is_active',true],
                ['is_integrate',false],
            ])
            ->exists();

            if(!$is_material_movement_line_exists)
            {
                $material_in_movement_line = MaterialMovementLine::firstorcreate([
                    'material_movement_id'          => $material_in_movement_id,
                    'material_preparation_id'       => $material_preparation->id,
                    'item_id'                       => $material_preparation->item_id,
                    'item_id_source'                => $material_preparation->item_id_source,
                    'item_code'                     => $material_preparation->item_code,
                    'type_po'                       => $material_preparation->type_po,
                    'c_order_id'                    => $material_preparation->c_order_id,
                    'c_bpartner_id'                 => $material_preparation->c_bpartner_id,
                    'supplier_name'                 => $material_preparation->supplier_name,
                    'c_orderline_id'                => $c_orderline_id,
                    'uom_movement'                  => $material_preparation->uom_conversion,
                    'qty_movement'                  => $material_preparation->qty_conversion,
                    'date_receive_on_destination'   => $movement_date,
                    'date_movement'                 => $movement_date,
                    'created_at'                    => $movement_date,
                    'updated_at'                    => $movement_date,
                    'nomor_roll'                    => '-',
                    'is_active'                     => true,
                    'is_integrate'                  => false,
                    'warehouse_id'                  => $material_preparation->warehouse,
                    'document_no'                   => $material_preparation->document_no,
                    'user_id'                       => auth::user()->id
                ]);
            }
                
        }

        /*if($locator_destination->rack == 'Z' && auth::user()->warehouse == '1000013'){
            $ip_address = auth::user()->ip_address;

            if($ip_address == '192.168.72.159')
                $group_id = 1;
            elseif($ip_address == '192.168.72.111')
                $group_id = 2;
            elseif($ip_address == '192.168.72.151')
                $group_id = 3;
            else
                $group_id = 4;
            
            $obj_automation = new stdClass();
            $obj_automation->locator_name = $locator_destination_name;
            $obj_automation->po_buyer = $material_preparation->po_buyer;
            $obj_automation->group_id = $group_id;
            $insert_automations [] = $obj_automation;
        }*/
    
        $first_inserted_to_locator_date                 = $material_preparation->first_inserted_to_locator_date;
        
        if(!$first_inserted_to_locator_date) $material_preparation->first_inserted_to_locator_date       = $movement_date;

        $material_preparation->last_material_movement_line_id   = $material_in_movement_line->id;
        $material_preparation->last_status_movement             = $last_status_movement;
        $material_preparation->last_locator_id                  = $to_location_id;
        $material_preparation->updated_at                       = $movement_date;
        $material_preparation->last_movement_date               = $movement_date;
        $material_preparation->last_user_movement_id            = Auth::user()->id;
        $material_preparation->ict_log                          = $ict_log;
        $material_preparation->save();


        $temporary = Temporary::Create([
            'barcode'   => $po_buyer,
            'status'    => 'mrp',
            'user_id'   => Auth::user()->id,
            'created_at'=> $movement_date,
            'updated_at'=> $movement_date,
        ]);
    }

    static function insertIntoAutomationRack($insert_automations)
    {
       
        foreach ($insert_automations as $key => $insert_automation) 
        {
            $po_buyer       = $insert_automation->po_buyer;
            $group_id       = $insert_automation->group_id;
            $locator_name   = $insert_automation->locator_name;

            RackAutomation::Create([
                'locator_name'  => $locator_name,
                'group_id'      => $group_id,
                'po_buyer'      => $po_buyer,
            ]);

        }
    }

    static function mrp()
    {
        $po_buyers = Temporary::select('barcode as po_buyer')
        ->where('status','mrp')
        ->groupby('barcode')
        ->get();

        try
        {
            DB::beginTransaction();
        
            foreach ($po_buyers as $key => $value) 
            {
                $po_buyer = PoBuyer::where('po_buyer',$value->po_buyer)->first();
                Mrp::where('po_buyer',$value->po_buyer)->delete();

                $mrp = DB::select(db::raw("SELECT * FROM get_mrp_cte(
                    '".$value->po_buyer."'
                    );"
                ));

                foreach ($mrp as $kmr => $mrp_val) 
                {
                    if($mrp_val->last_status_movement == 'receiving') $last_status_movement = 'MATERIAL DITERIMA DIGUDANG';
                    else if($mrp_val->last_status_movement == 'not receive') $last_status_movement = 'MATERIAL BELUM DITERIMA DIGUDANG';
                    else if($mrp_val->last_status_movement == 'in') $last_status_movement = 'MATERIAL SIAP SUPLAI';
                    else if($mrp_val->last_status_movement == 'expanse') $last_status_movement = 'EXPENSE';
                    else if($mrp_val->last_status_movement == 'out') $last_status_movement = 'MATERIAL SUDAH DISUPLAI';
                    else if($mrp_val->last_status_movement == 'out-handover') $last_status_movement = 'MATERIAL PINDAH TANGAN';
                    else if($mrp_val->last_status_movement == 'out-subcont') $last_status_movement = 'MATERIAL SUDAH DISUPLAI KE SUBCONT';
                    else if($mrp_val->last_status_movement == 'reject') $last_status_movement = 'REJECT';
                    else if($mrp_val->last_status_movement == 'change') $last_status_movement = 'MATERIAL PINDAH LOKASI RAK';
                    else if($mrp_val->last_status_movement == 'hold') $last_status_movement = 'MATERIAL DITAHAN QC UNTUK PENGECEKAN';
                    else if($mrp_val->last_status_movement == 'adjustment' || $mrp_val->last_status_movement == 'adjusment') $last_status_movement = 'PENYESUAIAN QTY';
                    else if($mrp_val->last_status_movement == 'print') $last_status_movement = 'CETAK BARCODE';
                    else if($mrp_val->last_status_movement == 'reroute') $last_status_movement = 'REROUTE';
                    else if($mrp_val->last_status_movement == 'cancel item') $last_status_movement = 'CANCEL ITEM';
                    else if($mrp_val->last_status_movement == 'cancel order') $last_status_movement = 'CANCEL ORDER';
                    else if($mrp_val->last_status_movement == 'check') $last_status_movement = 'MATERIAL DALAM PENGECEKAN QC';
                    else if($mrp_val->last_status_movement == 'out-cutting') $last_status_movement = 'MATERIAL SUDAH DISUPLAI KE CUTTING';
                    else if($mrp_val->last_status_movement == 'relax') $last_status_movement = 'RELAX FABRIC';
                    else $last_status_movement = $mrp_val->last_status_movement;

                    if($po_buyer)
                    {
                        if($po_buyer->statistical_date) $statistical_date = $po_buyer->statistical_date;
                        else $statistical_date = $po_buyer->promise_date;
                    }else
                    {
                        $statistical_date   = null;
                    }

                    Mrp::Create([
                        'lc_date'                       => ($po_buyer? $po_buyer->lc_date : null),
                        'backlog_status'                => $mrp_val->backlog_status,
                        'statistical_date'              => $statistical_date,
                        'style'                         => $mrp_val->style,
                        'season'                        => $mrp_val->season,
                        'article_no'                    => $mrp_val->article_no,
                        'po_buyer'                      => $mrp_val->po_buyer,
                        'category'                      => $mrp_val->category,
                        'item_code'                     => $mrp_val->item_code,
                        'item_desc'                     => $mrp_val->item_desc,
                        'qty_need'                      => $mrp_val->qty_need,
                        'warehouse'                     => $mrp_val->warehouse,
                        'uom'                           => $mrp_val->uom,
                        'status'                        => $last_status_movement,
                        'location'                      => $mrp_val->last_locator_code,
                        'movement_date'                 => $mrp_val->last_movement_date,
                        'user_name'                     => $mrp_val->last_user_movement_name,
                        'qty_in'                        => $mrp_val->qty_available,
                        'qty_supply'                    => $mrp_val->qty_supply,
                        'qty_handover_not_received_yet' => $mrp_val->qty_handover_no_received_yet,
                        'supplier_name'                 => $mrp_val->supplier_name,
                        'document_no'                   => $mrp_val->document_no,
                        'remark'                        => $mrp_val->remark,
                        'system_log'                    => $mrp_val->system_log
                    ]);
                }

                Temporary::where([
                    ['barcode',$value->po_buyer],
                    ['status','mrp'],
                ])
                ->delete();
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

    }


}
