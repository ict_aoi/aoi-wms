<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use Illuminate\Support\Facades\Session;

use App\Models\User;
use App\Models\Locator;
use App\Models\Supplier;
use App\Models\UomConversion;
use App\Models\MaterialStock;
use App\Models\MappingStocks;
use App\Models\AllocationItem;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\MaterialMovementLine;

use App\Http\Controllers\AccessoriesMaterialStockApprovalController as HistoryStock;

class AccessoriesAllocationNonBuyerController extends Controller
{
    public function index()
    {
        //return view('errors.503');
        
        $flag = Session::get('flag');
        return view('accessories_allocation_non_buyer.index',compact('flag'));
    }

    public function data(Request $request)
    {

        if(request()->ajax()) 
        {
            $warehouse_id       = $request->warehouse;
            $allocations = AllocationItem::whereNull('deleted_at')
            ->whereNull('purchase_item_id')
            ->where([
                ['warehouse','LIKE',"%$warehouse_id%"],
                ['is_from_allocation_buyer',false],
                ['is_not_allocation',false]
            ])
            ->orderby('created_at','desc');

            return DataTables::of($allocations)
            ->editColumn('created_at',function ($allocations)
            {
                return $allocations->created_at->format('d/M/Y H:i:s');
            })
            ->addColumn('qty_booking',function ($allocations)
            {
                return number_format($allocations->qty_booking, 4, '.', ',');
            })
            ->editColumn('user_id',function ($allocations)
            {
                return strtoupper($allocations->user->name);
            })
            ->addColumn('action', function($allocations) 
            {
                return view('_action', [
                    'model'     => $allocations,
                    'delete'    => route('accessoriesAllocationNonBuyer.delete',$allocations->id)
                ]);
            })
            
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function upload()
    {
        return view('accessories_allocation_non_buyer.upload');
    }

    public function exportFileUpload()
    {
         return Excel::create('upload_alokasi_non_buyer',function ($excel){
            $excel->sheet('active', function($sheet){
                // $sheet->setCellValue('A1','SUPPLIER_CODE');
                // $sheet->setCellValue('B1','PO_SUPPLIER_SOURCE');
                // $sheet->setCellValue('C1','PO_BUYER_SOURCE');
                // $sheet->setCellValue('D1','LOCATOR_SOURCE');
                // $sheet->setCellValue('E1','ITEM_CODE_SOURCE');
                // $sheet->setCellValue('F1','UOM_BOOKING');
                $sheet->setCellValue('A1','MATERIAL_STOCK_ID');
                $sheet->setCellValue('B1','QTY_BOOKING');
                $sheet->setCellValue('C1','NOTE');

                $sheet->setWidth('A', 15);
                $sheet->setColumnFormat(array(
                    'C' => '@',
                ));
            
            });

            /*$excel->sheet('conversion', function($sheet){
                $conversions = UomConversion::get();

                $sheet->setCellValue('A1','#');
                $sheet->setCellValue('B1','ITEM_CODE');
                $sheet->setCellValue('C1','UOM_FROM');
                $sheet->setCellValue('D1','UOM_TO');
                $sheet->setCellValue('E1','MULTIPLICATION');

                $index = 0;
                foreach ($conversions as $key => $conversion) {
                    $row = $index + 2;
                    $sheet->setCellValue('A'.$row, ($index+1));
                    $sheet->setCellValue('B'.$row, $conversion->item_code);
                    $sheet->setCellValue('C'.$row, $conversion->uom_from);
                    $sheet->setCellValue('D'.$row, $conversion->uom_to);
                    $sheet->setCellValue('E'.$row, $conversion->dividerate);
                    $index++;
                }
            });*/

            $excel->setActiveSheetIndex(0);
          
            
            
        })->export('xlsx');
    }

    public function import(Request $request)
    {
        $array = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);
          
            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();
            
            if(!empty($data) && $data->count())
            {
                //return response()->json($data);
                $array              = array();
                $array_allocations  = array();
                $array_data_stock   = array();
                $created_at         = carbon::now();

                $warehouse_id   = $request->warehouse_id;
                if($warehouse_id == '1000002') $warehouse_name = 'ACCESSORIES AOI 1';
                else if($warehouse_id == '1000013') $warehouse_name = 'ACCESSORIES AOI 2';

                
                foreach ($data as $key => $value) 
                {
                    $id      = $value->material_stock_id;
                    // $supplier_code      = trim(strtoupper($value->supplier_code));
                    // $document_no        = trim(strtoupper($value->po_supplier_source));
                    // $item_code_source   = trim(strtoupper($value->item_code_source));
                    // $po_buyer_source    = trim(strtoupper($value->po_buyer_source));
                    // $uom_booking        = trim(strtoupper($value->uom_booking));
                    $qty_booking        = sprintf('%0.8f',$value->qty_booking);
                    // $locator_source     = trim(strtoupper($value->locator_source));
                    $is_additional      = false;//trim(strtoupper($value->is_additional));
                    $remark_additional  = trim(strtoupper($value->remark_additional));
                    $note               = trim(strtoupper($value->note));
                    
                    if($id != null
                        && $qty_booking != null && $note != null)
                    {
                        if($is_additional == false || $is_additional == null || $is_additional == '' || $is_additional == 'FALSE' )
                        {
                            $material_stock = MaterialStock::where('id',$id)
                                              ->where('is_allocated',false)
                                              ->where('is_closing_balance',false)
                                              ->first();

                            if($material_stock)
                            {
                                // foreach ($material_stocks as $key_2 => $material_stock) 
                                // {
                                    $supplier_code      = $material_stock->supplier_code;
                                    $document_no        = $material_stock->document_no;
                                    $item_code_source   = $material_stock->item_code;
                                    $po_buyer_source    = $material_stock->po_buyer;
                                    $uom_booking        = $material_stock->uom;
                                    $locator_source     = ($material_stock->locator_id) ? $material_stock->locator->code : '';
                                    $available_qty = sprintf('%0.8f',$material_stock->available_qty);
    
                                    if($qty_booking > 0)
                                    {
                                        if($available_qty > 0)
                                        {
                                            if ($qty_booking/$available_qty >= 1) $_supply = $available_qty;
                                            else $_supply = $qty_booking;
    
                                            if($_supply > 0)
                                            {
                                                $obj                    = new stdClass();
                                                $obj->material_stock_id = $material_stock->id;
                                                $obj->supplier_code     = $supplier_code;
                                                $obj->document_no       = $document_no;
                                                $obj->item_code_source  = $item_code_source;
                                                $obj->po_buyer_source   = $po_buyer_source;
                                                $obj->locator_source    = $locator_source;
                                                $obj->note              = $note;
                                                $obj->uom               = $uom_booking;
                                                $obj->qty_booking       = $qty_booking;
                                                $obj->is_additional     = false;
                                                $obj->remark_additional = $remark_additional;
                                                $array_allocations []   = $obj;
                                                $array_data_stock []    = $material_stock->id;
                                            }
                                            $available_qty -= $_supply;
                                            $qty_booking -= $_supply;
                                        }
    
                                    }
                                //}
                            }else
                            {
                                $obj                        = new stdClass();
                                $obj->supplier_code         = '-';
                                $obj->document_no           = '-';
                                $obj->item_code_source      = '-';
                                $obj->po_buyer_source       = '-';
                                $obj->locator_source        = '-';
                                $obj->note                  = $note;
                                $obj->uom                   = '-';
                                $obj->qty_booking           = $qty_booking;
                                $obj->result                = 'Failed,'.$id.' stock not found.';
                                $obj->is_additional         = $is_additional;
                                $obj->remark_additional     = $remark_additional;
                                $array []                   = $obj;
                            }
                            
                        }else
                        {
                            if($remark_additional)
                            {
                                $material_stocks = MaterialStock::where([
                                    ['document_no',$document_no],
                                    ['item_code',$item_code_source],
                                    ['warehouse_id',$warehouse_id],
                                    ['uom',$uom_booking],
                                    ['is_allocated',false],
                                    ['is_closing_balance',false],
                                    ['is_running_stock',false]
                                ]);
    
                                if($po_buyer_source && $po_buyer_source != '' && $po_buyer_source != '-') $material_stocks = $material_stocks->where('po_buyer',$po_buyer_source);
                                else $material_stocks = $material_stocks->whereNull('po_buyer');
    
                                if($supplier_code == 'FREE STOCK')
                                {
                                    $material_stocks = $material_stocks->where(function($query){
                                        $query->whereNull('c_bpartner_id')
                                        ->orWhere('c_bpartner_id','')
                                        ->orWhere('c_bpartner_id','FREE STOCK');
                                    });
                                } else{
                                    $supplier = Supplier::where(db::raw('upper(supplier_code)'),$supplier_code)->first();
                                    $material_stocks = $material_stocks->where('c_bpartner_id',$supplier->c_bpartner_id);
                                }
    
                                if($locator_source)
                                {
                                    $locator = Locator::where(db::raw('trim(upper(code))'),$locator_source)
                                    ->whereHas('area',function($query) use ($warehouse_id)
                                    {
                                        $query->where('warehouse',$warehouse_id);
                                    })
                                    ->where('is_active',true)
                                    ->first();
    
                                    if($locator)
                                        $material_stocks = $material_stocks->where('locator_id',$locator->id);
    
                                }else
                                {
                                    $material_stocks = $material_stocks->where(function($query){
                                        $query->whereNull('locator_id')
                                        ->orWhere('locator_id','');
                                    });
                                }
    
                                $material_stocks = $material_stocks->orderby('available_qty','asc')
                                ->get();
    
                                if($material_stocks->count() > 0)
                                {
                                    foreach ($material_stocks as $key_2 => $material_stock) 
                                    {
                                        $available_qty = sprintf('%0.8f',$material_stock->available_qty);
        
                                        if($qty_booking > 0)
                                        {
                                            if($available_qty > 0)
                                            {
                                                if ($qty_booking/$available_qty >= 1) $_supply = $available_qty;
                                                else $_supply = $qty_booking;
        
                                                if($_supply > 0)
                                                {
                                                    $obj                    = new stdClass();
                                                    $obj->material_stock_id = $material_stock->id;
                                                    $obj->supplier_code     = $supplier_code;
                                                    $obj->document_no       = $document_no;
                                                    $obj->item_code_source  = $item_code_source;
                                                    $obj->po_buyer_source   = $po_buyer_source;
                                                    $obj->locator_source    = $locator_source;
                                                    $obj->note              = $note;
                                                    $obj->uom               = $uom_booking;
                                                    $obj->qty_booking       = $qty_booking;
                                                    $obj->is_additional     = true;
                                                    $obj->remark_additional = $remark_additional;
                                                   
                                                    $array_allocations []   = $obj;
                                                    $array_data_stock []    = $material_stock->id;
                                                }

                                                $available_qty  -= $_supply;
                                                $qty_booking    -= $_supply;
                                            }
        
                                        }
                                    }
                                }else
                                {
                                    $obj                        = new stdClass();
                                    $obj->supplier_code         = $supplier_code;
                                    $obj->document_no           = $document_no;
                                    $obj->item_code_source      = $item_code_source;
                                    $obj->po_buyer_source       = $po_buyer_source;
                                    $obj->locator_source        = $locator_source;
                                    $obj->note                  = $note;
                                    $obj->uom                   = $uom_booking;
                                    $obj->qty_booking           = $qty_booking;
                                    $obj->result                = 'Failed, stock not found';
                                    $obj->is_additional         = $is_additional;
                                    $obj->remark_additional     = $remark_additional;
                                    $array []                   = $obj;
                                } 
                            }else
                            {
                                $obj                            = new stdClass();
                                $obj->supplier_code             = $supplier_code;
                                $obj->document_no               = $document_no;
                                $obj->item_code_source          = $item_code_source;
                                $obj->po_buyer_source           = $po_buyer_source;
                                $obj->locator_source            = $locator_source;
                                $obj->note                      = $note;
                                $obj->uom                       = $uom_booking;
                                $obj->qty_booking               = $qty_booking;
                                $obj->result                    = 'Failed, please insert remark additional first.';
                                $obj->is_additional             = $is_additional;
                                $obj->remark_additional         = $remark_additional;
                                $array [] = $obj;
                            }
                            
                        }
                        

                        
                    }else
                    {
                        $obj                        = new stdClass();
                        $obj->supplier_code         = $supplier_code;
                        $obj->document_no           = $document_no;
                        $obj->item_code_source      = $item_code_source;
                        $obj->po_buyer_source       = $po_buyer_source;
                        $obj->locator_source        = $locator_source;
                        $obj->note                  = $note;
                        $obj->uom                   = $uom_booking;
                        $obj->qty_booking           = $qty_booking;
                        $obj->result                = 'Failed, please check id note';
                        $obj->is_additional         = $is_additional;
                        $obj->remark_additional     = $remark_additional;
                        $array [] = $obj;
                    }
                }

                //die();
                //dd($array_data_stock);
                //die();
                //return response()->json($array_allocations);
                //return response()->json(array_unique($array_data_stock));
                //return response()->json($array_data_stock);
                //dd($available_qty);
                
                //die();
                usort($array_allocations, function($a, $b) {
                    return strcmp($a->material_stock_id,$b->material_stock_id);
                });

                $material_stocks = MaterialStock::wherein('id',array_unique($array_data_stock))
                ->where([
                    ['is_allocated',false],
                    ['is_closing_balance',false],
                    ['is_running_stock',false]
                ])
                ->whereNull('last_status')
                ->orderby('id','asc')
                ->get();
               
                try 
                {
                    DB::beginTransaction();

                    foreach ($material_stocks as $key_1 => $material_stock) 
                    {
                        if($material_stock->last_status != 'prepared')
                        {
                            $material_stock->last_status = 'prepared';
                            $material_stock->save();

                            $material_stock_id      = $material_stock->id;
                            $c_bpartner_id          = $material_stock->c_bpartner_id;
                            $supplier_code          = $material_stock->supplier_code;
                            $supplier_name          = $material_stock->supplier_name;
                            $c_order_id             = $material_stock->c_order_id;
                            $document_no            = $material_stock->document_no;
                            $locator_id             = $material_stock->locator_id;
                            $po_buyer               = $material_stock->po_buyer;
                            $item_id                = $material_stock->item_id;
                            $item_code              = $material_stock->item_code;
                            $item_desc              = $material_stock->item_desc;
                            $uom                    = $material_stock->uom;
                            $category               = $material_stock->category;
                            $type_stock             = $material_stock->type_stock;
                            $type_stock_erp_code    = $material_stock->type_stock_erp_code;
                            $old_available_qty      = sprintf('%0.8f',$material_stock->available_qty);
                            $available_qty          = sprintf('%0.8f',$material_stock->available_qty);

                            $po_detail_id           = $material_stock->po_detail_id;
                            $warehouse_id           = $material_stock->warehouse_id;
                            $nomor_roll             = '-';
                           
                            if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                            {
                                $c_order_id             = 'FREE STOCK';
                                $no_packing_list        = '-';
                                $no_invoice             = '-';
                                $c_orderline_id         = '-';
                            }else
                            {
                                $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                ->whereNull('material_roll_handover_fabric_id')
                                ->where('po_detail_id',$po_detail_id)
                                ->first();

                                $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                                $c_order_id             = ($material_arrival ? $material_arrival->c_order_id : 'FREE STOCK');
                            }
                            
                            $system = User::where([
                                ['name','system'],
                                ['warehouse',$warehouse_id]
                            ])
                            ->first();

                            $inventory_erp = Locator::with('area')
                            ->where([
                                ['is_active',true],
                                ['rack','ERP INVENTORY'],
                            ])
                            ->whereHas('area',function ($query) use($warehouse_id)
                            {
                                $query->where('is_active',true);
                                $query->where('warehouse',$warehouse_id);
                                $query->where('name','ERP INVENTORY');
                            })
                            ->first();

                            
                            foreach ($array_allocations as $key_2 => $array_allocation) 
                            {
                                $supplier_code                  = $array_allocation->supplier_code;
                                $material_stock_id_allocation   = $array_allocation->material_stock_id;
                                $po_buyer_source                = $array_allocation->po_buyer_source;
                                $is_additional                  = $array_allocation->is_additional;
                                $qty_booking                    = sprintf('%0.8f',$array_allocation->qty_booking);
                                $note                           = $array_allocation->note;
                            
                                if($material_stock_id == $material_stock_id_allocation)
                                {
                                    if ($available_qty/$qty_booking >= 1) $_supply = $qty_booking;
                                    else $_supply = $available_qty;

                                    if($available_qty > 0 && $_supply > 0)
                                    {
                                        AllocationItem::Create([
                                            'material_stock_id'         => $material_stock->id,
                                            'c_bpartner_id'             => $c_bpartner_id,
                                            'supplier_code'             => $supplier_code,
                                            'supplier_name'             => $supplier_name,
                                            'document_no'               => $document_no,
                                            'c_order_id'                => $c_order_id,
                                            'item_code'                 => $item_code,
                                            'item_id_book'              => $item_id,
                                            'item_code_source'          => $item_code,
                                            'item_id_source'            => $item_id,
                                            'item_desc'                 => $item_desc,
                                            'category'                  => $category,
                                            'remark'                    => $note,
                                            'uom'                       => $uom,
                                            'is_from_allocation_buyer'  => false,
                                            'mm_approval_date'          => $created_at,
                                            'accounting_approval_date'  => $created_at,
                                            'created_at'                => $created_at,
                                            'updated_at'                => $created_at,
                                            'accounting_user_id'        => $system->id,
                                            'mm_user_id'                => $system->id,
                                            'warehouse'                 => $material_stock->warehouse_id,
                                            'qty_booking'               => $_supply,
                                            'is_additional'             => $is_additional,
                                            'user_id'                   => auth::user()->id,
                                            'confirm_by_warehouse'      => 'approved',
                                            'confirm_date'              => $created_at,
                                            'confirm_user_id'           => auth::user()->id,
                                            'deleted_at'                => null
                                        ]);

                                        $qty_booking -= $_supply;
                                        $new = $available_qty - $_supply;
                                        if($new <= 0) $new = '0';
                                        else $new = $new;
                                        
                                        $operator   = sprintf('%0.8f',$new - $old_available_qty);

                                        HistoryStock::approved($material_stock_id
                                        ,$new
                                        ,$old_available_qty
                                        ,$_supply
                                        ,null
                                        ,null
                                        ,null
                                        ,null
                                        ,'ALLOCATION NON BUYER, '.$note
                                        ,auth::user()->name
                                        ,auth::user()->id
                                        ,$type_stock_erp_code
                                        ,$type_stock
                                        ,false
                                        ,true);
                                        
                                        $is_movement_exists = MaterialMovement::where([
                                            ['from_location',$inventory_erp->id],
                                            ['to_destination',$inventory_erp->id],
                                            ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                            ['to_locator_erp_id',$inventory_erp->area->erp_id],
                                            ['is_integrate',false],
                                            ['is_active',true],
                                            ['no_packing_list',$no_packing_list],
                                            ['no_invoice',$no_invoice],
                                            ['status','integration-to-inventory-erp'],
                                        ])
                                        ->first();
                                        
                                        if(!$is_movement_exists)
                                        {
                                            $material_movement = MaterialMovement::firstOrCreate([
                                                'from_location'         => $inventory_erp->id,
                                                'to_destination'        => $inventory_erp->id,
                                                'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                                'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                                'is_integrate'          => false,
                                                'is_active'             => true,
                                                'no_packing_list'       => $no_packing_list,
                                                'no_invoice'            => $no_invoice,
                                                'status'                => 'integration-to-inventory-erp',
                                                'created_at'            => $created_at,
                                                'updated_at'            => $created_at,
                                            ]);
                    
                                            $material_movement_id = $material_movement->id;
                                        }else
                                        {
                                            $is_movement_exists->updated_at = $created_at;
                                            $is_movement_exists->save();
                    
                                            $material_movement_id = $is_movement_exists->id;
                                        }
                    
                    
                                        $is_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                                        ->where([
                                            ['material_movement_id',$material_movement_id],
                                            ['material_stock_id',$material_stock_id],
                                            ['warehouse_id',$warehouse_id],
                                            ['item_id',$item_id],
                                            ['date_movement',$created_at],
                                            ['is_integrate',false],
                                            ['is_active',true],
                                            ['qty_movement',$operator],
                                        ])
                                        ->exists();

                                        if(!$is_line_exists)
                                        {
                                            MaterialMovementLine::Create([
                                                'material_movement_id'          => $material_movement_id,
                                                'material_stock_id'             => $material_stock_id,
                                                'item_code'                     => $item_code,
                                                'item_id'                       => $item_id,
                                                'c_order_id'                    => $c_order_id,
                                                'c_orderline_id'                => $c_orderline_id,
                                                'c_bpartner_id'                 => $c_bpartner_id,
                                                'supplier_name'                 => $supplier_name,
                                                'type_po'                       => 1,
                                                'is_integrate'                  => false,
                                                'uom_movement'                  => $uom,
                                                'qty_movement'                  => $operator,
                                                'date_movement'                 => $created_at,
                                                'created_at'                    => $created_at,
                                                'updated_at'                    => $created_at,
                                                'date_receive_on_destination'   => $created_at,
                                                'nomor_roll'                    => $nomor_roll,
                                                'warehouse_id'                  => $warehouse_id,
                                                'document_no'                   => $document_no,
                                                'note'                          => 'PENGURANGAN STOCK MENGUNAKAN MENU ALOKASI NON BUYER KARENA '.$note,
                                                'is_active'                     => true,
                                                'user_id'                       => auth::user()->id,
                                            ]);
                                        }
                                        
                                        
                                        $available_qty      -= $_supply;
                                        $reserved_qty       = $material_stock->reserved_qty;
                                        $new_reserverd_qty  = sprintf('%0.8f',$reserved_qty + $_supply);
                                    
                                        $material_stock->available_qty = sprintf('%0.8f',$available_qty);
                                        $material_stock->reserved_qty = sprintf('%0.8f',$new_reserverd_qty);

                                        if(sprintf('%0.8f',$available_qty) <= 0.0000)
                                        {
                                            $material_stock->is_allocated   = true;
                                            $material_stock->is_active      = false;

                                            if($material_stock->locator_id)
                                            {
                                                $locator = Locator::find($material_stock->locator_id);
                                                if($locator)
                                                {
                                                    $counter = $locator->counter_in;
                                                    $locator->counter_in = $counter -1 ;
                                                    $locator->save();
                                                }
                                            }
                                        }

                                        $material_stock->last_status = null;
                                        $material_stock->save();

                                        $obj = new stdClass();
                                        $obj->supplier_code         = $supplier_code;
                                        $obj->document_no           = $document_no;
                                        $obj->item_code_source      = $item_code;
                                        $obj->po_buyer_source       = $po_buyer_source;
                                        $obj->locator_source        = $locator_source;
                                        $obj->note                  = $note;
                                        $obj->uom                   = $uom_booking;
                                        $obj->qty_booking           = $_supply;
                                        $obj->result                = 'SUCCESS';
                                        $obj->is_additional         = $is_additional;
                                        $obj->remark_additional     = $remark_additional;
                                        $array []                   = $obj;
                                    }else
                                    {
                                        $obj                        = new stdClass();
                                        $obj->supplier_code         = $supplier_code;
                                        $obj->document_no           = $document_no;
                                        $obj->item_code_source      = $item_code;
                                        $obj->po_buyer_source       = $po_buyer_source;
                                        $obj->locator_source        = $locator_source;
                                        $obj->note                  = $note;
                                        $obj->uom                   = $uom_booking;
                                        $obj->qty_booking           = $qty_booking;
                                        $obj->result                = 'Failed, you don\'t have stock anymore';
                                        $obj->is_additional         = $is_additional;
                                        $obj->remark_additional     = $remark_additional;
                                        $array []                   = $obj;
                                    }
                                    
                                }
                            }
                        }
                        
                    }

                    DB::commit();
                    
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                usort($array, function($a, $b) {
                    return strcmp($a->document_no,$b->document_no);
                });
                
                return response()->json($array,200);
            }else{
                  return response()->json('Upload faild, please check your file.',422);
            }

            
        }
        
    }

    public function materialStockPickList(Request $request)
    {
        $warehouse_id   = $request->warehouse_id;
        $document_no    = trim(strtoupper($request->document_no));
        $item_code      = trim(strtoupper($request->item_code));
        $po_buyer       = trim(strtoupper($request->po_buyer));
        
        $lists = MaterialStock::where([
            ['warehouse_id',$warehouse_id],
            ['is_allocated',false],
            ['is_stock_on_the_fly',false],
            ['is_closing_balance',false],
            ['is_running_stock',false],
            ['available_qty','>','0']
        ])
        ->whereNotNull('approval_date');

        if($document_no)
            $lists = $lists->where(function($query) use ($document_no){ 
                $query->where('document_no','like',"%$document_no%");
            });
        
        if($item_code)
            $lists = $lists->where(function($query) use ($item_code){ 
                $query->where('item_code','like',"%$item_code%");
            });
        
         if($po_buyer)
            $lists = $lists->where(function($query) use ($po_buyer){ 
                $query->where('po_buyer','like',"%$po_buyer%");
            });
        
        $lists = $lists->paginate('10');

        return view('accessories_allocation_non_buyer._material_stock_list',compact('lists'));
            
    }

    public function store(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'item_allocation_id'    => 'required',
            'qty_booking'           => 'required',
            'remark'                => 'required',
        ]);

        if ($validator->passes()) 
        {
            //return response()->json($request->all());
           
            $created_at         = carbon::now();
            $material_stock_id  = $request->item_allocation_id;   
            $is_additional      = $request->is_additional;   
            $qty_booking        = sprintf('%0.8f',$request->qty_booking); 
            $remark             = $request->remark; 
            
            $material_stock = MaterialStock::where([
                ['id',$material_stock_id],
                ['is_running_stock',false],
            ])
            ->whereNull('last_status')
            ->first();

            if(!$material_stock) return response()->json('Data in used, please try again later.',422);

            $old_available_qty  = sprintf('%0.8f',$material_stock->available_qty);
            $available_qty      = sprintf('%0.8f',$material_stock->available_qty);
            $po_detail_id       = $material_stock->po_detail_id;
            $item_code          = $material_stock->item_code;
            $item_id            = $material_stock->item_id;
            $c_bpartner_id      = $material_stock->c_bpartner_id;
            $supplier_name      = $material_stock->supplier_name;
            $document_no        = $material_stock->document_no;
            $warehouse_id       = $material_stock->warehouse_id;
            $nomor_roll         = '-';
            $uom                = $material_stock->uom;

            if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
            {
                $c_order_id             = 'FREE STOCK';
                $no_packing_list        = '-';
                $no_invoice             = '-';
                $c_orderline_id         = '-';
            }else
            {
                $material_arrival       = MaterialArrival::where('po_detail_id',$po_detail_id)->first();                $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                ->whereNull('material_roll_handover_fabric_id')
                ->where('po_detail_id',$po_detail_id)
                ->first();
                
                $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                $c_order_id             = ($material_arrival ? $material_arrival->c_order_id : 'FREE STOCK');
            }
            
            $system = User::where([
                ['name','system'],
                ['warehouse',$material_stock->warehouse_id]
            ])
            ->first();

            
            if($available_qty - $qty_booking >= 0)
            {
                $material_stock->last_status = 'prepared';
                $material_stock->save();

                $type_stock             = $material_stock->type_stock;
                $type_stock_erp_code    = $material_stock->type_stock_erp_code;

                if ($qty_booking/$available_qty >= 1) $_supply = $available_qty;
                else $_supply = $qty_booking;
                
                    AllocationItem::Create([
                        'material_stock_id'         => $material_stock->id,
                        'document_no'               => $material_stock->document_no,
                        'c_order_id'                => $material_stock->c_order_id,
                        'supplier_code'             => $material_stock->supplier_code,
                        'supplier_name'             => $material_stock->supplier_name,
                        'item_code'                 => $material_stock->item_code,
                        'item_id_book'              => $material_stock->item_id,
                        'item_code_source'          => $material_stock->item_code,
                        'item_id_source'            => $material_stock->item_id,
                        'item_desc'                 => $material_stock->item_desc,
                        'category'                  => $material_stock->category,
                        'uom'                       => $material_stock->uom,
                        'warehouse'                 => $material_stock->warehouse_id,
                        'qty_booking'               => $_supply,
                        'is_additional'             => $is_additional,
                        'is_from_allocation_buyer'  => false,
                        'mm_approval_date'          => $created_at,
                        'accounting_approval_date'  => $created_at,
                        'accounting_user_id'        => $system->id,
                        'mm_user_id'                => $system->id,
                        'remark'                    => $remark,
                        'confirm_by_warehouse'      => 'approved',
                        'confirm_date'              => $created_at,
                        'confirm_user_id'           => auth::user()->id,
                        'user_id'                   => auth::user()->id
                    ]);

                    $new = $available_qty - $_supply;
                    if($new <= 0) $new = '0';
                    else $new = $new;
                    
                    $operator   = sprintf('%0.8f',$new - $old_available_qty);

                    HistoryStock::approved($material_stock_id
                        ,$new
                        ,$old_available_qty
                        ,$_supply
                        ,null
                        ,null
                        ,null
                        ,null
                        ,'ALLOCATION NON BUYER, '.$remark
                        ,auth::user()->name
                        ,auth::user()->id
                        ,$type_stock_erp_code
                        ,$type_stock
                        ,false
                        ,true
                    );

                    $inventory_erp = Locator::with('area')
                    ->where([
                        ['is_active',true],
                        ['rack','ERP INVENTORY'],
                    ])
                    ->whereHas('area',function ($query) use($warehouse_id)
                    {
                        $query->where('is_active',true);
                        $query->where('warehouse',$warehouse_id);
                        $query->where('name','ERP INVENTORY');
                    })
                    ->first();

                    $is_movement_exists = MaterialMovement::where([
                        ['from_location',$inventory_erp->id],
                        ['to_destination',$inventory_erp->id],
                        ['from_locator_erp_id',$inventory_erp->area->erp_id],
                        ['to_locator_erp_id',$inventory_erp->area->erp_id],
                        ['is_integrate',false],
                        ['is_active',true],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        ['status','integration-to-inventory-erp'],
                    ])
                    ->first();
                    
                    if(!$is_movement_exists)
                    {
                        $material_movement = MaterialMovement::firstOrCreate([
                            'from_location'         => $inventory_erp->id,
                            'to_destination'        => $inventory_erp->id,
                            'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                            'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                            'is_integrate'          => false,
                            'is_active'             => true,
                            'no_packing_list'       => $no_packing_list,
                            'no_invoice'            => $no_invoice,
                            'status'                => 'integration-to-inventory-erp',
                            'created_at'            => $created_at,
                            'updated_at'            => $created_at,
                        ]);

                        $material_movement_id = $material_movement->id;
                    }else
                    {
                        $is_movement_exists->updated_at = $created_at;
                        $is_movement_exists->save();

                        $material_movement_id = $is_movement_exists->id;
                    }

                    $is_line_integration_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                    ->where([
                        ['material_movement_id',$material_movement_id],
                        ['material_stock_id',$material_stock_id],
                        ['item_id',$item_id],
                        ['warehouse_id',$warehouse_id],
                        ['qty_movement',$operator],
                        ['date_movement',$created_at],
                    ])
                    ->exists();

                    if(!$is_line_integration_exists)
                    {
                        MaterialMovementLine::Create([
                            'material_movement_id'          => $material_movement_id,
                            'material_stock_id'             => $material_stock_id,
                            'item_code'                     => $item_code,
                            'item_id'                       => $item_id,
                            'c_order_id'                    => $c_order_id,
                            'c_orderline_id'                => $c_orderline_id,
                            'c_bpartner_id'                 => $c_bpartner_id,
                            'supplier_name'                 => $supplier_name,
                            'type_po'                       => 1,
                            'is_integrate'                  => false,
                            'uom_movement'                  => $uom,
                            'qty_movement'                  => $operator,
                            'date_movement'                 => $created_at,
                            'created_at'                    => $created_at,
                            'updated_at'                    => $created_at,
                            'date_receive_on_destination'   => $created_at,
                            'nomor_roll'                    => $nomor_roll,
                            'warehouse_id'                  => $warehouse_id,
                            'document_no'                   => $document_no,
                            'note'                          => 'PENGURANGAN STOCK MENGUNAKAN MENU ALOKASI NON BUYER KARENA '.$remark,
                            'is_active'                     => true,
                            'user_id'                       => auth::user()->id,
                        ]);
                    }
                    
                    $available_qty  -= $_supply;
                    $reserved_qty                   = sprintf('%0.8f',$material_stock->reserved_qty);
                    $material_stock->available_qty  = sprintf('%0.8f',$available_qty);
                    $material_stock->reserved_qty   = sprintf('%0.8f',$reserved_qty + $_supply);

                    if($available_qty <= 0.0000)
                    {
                        $material_stock->is_allocated = true;
                        $material_stock->is_active = false;

                        if($material_stock->locator_id)
                        {
                            $locator = Locator::find($material_stock->locator_id);
                            if($locator)
                            {
                                $counter = $locator->counter_in;
                                $locator->counter_in = $counter -1 ;
                                $locator->save();
                            }
                        }
                        
                        
                    }

                    $material_stock->last_status = null;
                    $material_stock->save();
            }else{
                return response()->json('Qty book must be less than available qty.', 422);
            }
            
            return response()->json('success', 200);
            
        }else{
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ],400); 
        }

    }

    public function destroy(Request $request,$id)
    {
        $allocation_item        = AllocationItem::find($id);
        $material_stock         = MaterialStock::find($allocation_item->material_stock_id);

        if($material_stock)
        {
            try 
            {
            DB::beginTransaction();

            $old_available_qty      = $material_stock->available_qty;
            $stock                  = sprintf('%0.8f',$material_stock->stock);
            $reserved_qty           = sprintf('%0.8f',$material_stock->reserved_qty);
            $type_stock             = sprintf('%0.8f',$material_stock->type_stock);
            $type_stock_erp_code    = $material_stock->type_stock_erp_code;
            
            $new_reserverd_qty      = sprintf('%0.8f',$reserved_qty - $allocation_item->qty_booking);
            $new_available_qty      = sprintf('%0.8f',$stock - $new_reserverd_qty);
            $_new_available_qty     = sprintf('%0.8f',$old_available_qty + $allocation_item->qty_booking);

        
            $material_stock->reserved_qty   = $new_reserverd_qty;
            $material_stock->available_qty  = $new_available_qty;

            if($new_available_qty > 0)
            {
                $material_stock->is_allocated   = false;
                $material_stock->is_active      = true;
            }
            //$material_stock->save();

            $allocation_item->deleted_at = carbon::now();

            HistoryStock::approved($allocation_item->material_stock_id
            ,$_new_available_qty
            ,$old_available_qty
            ,(-1*$allocation_item->qty_booking)
            ,$allocation_item->po_buyer
            ,$allocation_item->style
            ,$allocation_item->article_no
            ,$allocation_item->lc_date
            ,'CANCEL ALLOCATION NON BUYER, ('.$allocation_item->remark.')'
            ,auth::user()->name
            ,auth::user()->id
            ,$type_stock_erp_code
            ,$type_stock
            ,false
            ,true);

            $material_stock->save();
            $allocation_item->save();

            DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

           
        }

        return response()->json(200);
    }

    public function export()
    {
        $warehouse_id       = Auth::user()->warehouse;
            $allocations = AllocationItem::whereNull('deleted_at')
            ->whereNull('purchase_item_id')
            ->where([
                ['warehouse','LIKE',"%$warehouse_id%"],
                ['is_from_allocation_buyer',false],
                ['is_not_allocation',false]
            ])
            ->orderby('created_at','desc')
            ->get();

            $file_name = 'report alokasi non po buyer';
            return Excel::create($file_name,function($excel) use ($allocations)
            {
                $excel->sheet('active',function($sheet)use($allocations)
                {
                    $sheet->setCellValue('A1','MATERIAL_STOCK_ID');
                    $sheet->setCellValue('B1','CREATED_AT');
                    $sheet->setCellValue('C1','USERNAME');
                    $sheet->setCellValue('D1','SUPPLIER CODE');
                    $sheet->setCellValue('E1','SUPPLIER NAME');
                    $sheet->setCellValue('F1','PO SUPPLIER');
                    $sheet->setCellValue('G1','ITEM CODE');
                    $sheet->setCellValue('H1','CATEGORY');
                    $sheet->setCellValue('I1','UOM');
                    $sheet->setCellValue('J1','QTY_ALLOCATED');
                    $sheet->setCellValue('K1','REMARK');
    
                $row=2;
                foreach ($allocations as $i) 
                {  
                    $sheet->setCellValue('A'.$row,$i->material_stock_id);
                    $sheet->setCellValue('B'.$row,$i->created_at);
                    $sheet->setCellValue('C'.$row,$i->user->name);
                    $sheet->setCellValue('D'.$row,$i->supplier_code);
                    $sheet->setCellValue('E'.$row,$i->supplier_name);
                    $sheet->setCellValue('F'.$row,$i->document_no);
                    $sheet->setCellValue('G'.$row,$i->item_code);
                    $sheet->setCellValue('H'.$row,$i->category);
                    $sheet->setCellValue('I'.$row,$i->uom);
                    $sheet->setCellValue('J'.$row,$i->qty_booking);
                    $sheet->setCellValue('K'.$row,$i->remark);
                    $row++;
                }
                });
    
            })
            ->export('csv');

    }
}
