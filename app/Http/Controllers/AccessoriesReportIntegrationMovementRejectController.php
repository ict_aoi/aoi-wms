<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;


use App\Models\User;
use App\Models\Locator;
use App\Models\MaterialMovement;
use App\Models\MaterialMovementLine;

class AccessoriesReportIntegrationMovementRejectController extends Controller
{
    public function index()
    {
        return view('accessories_report_integration_movement_reject.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id       = $request->warehouse;
            
            $data               = DB::table('integration_movement_inventory_to_reject')
            ->where('warehouse_id','LIKE',"%$warehouse_id%")
            ->wherenotnull('material_preparation_id');

            return DataTables::of($data)
           
            ->editColumn('created_at',function ($data)
            {
                if($data->created_at) return Carbon::createFromFormat('Y-m-d H:i:s', $data->created_at)->format('d/M/Y H:i:s');
                else return null;
            })
            ->editColumn('integration_date',function ($data)
            {
                if($data->integration_date) return  Carbon::createFromFormat('Y-m-d H:i:s', $data->integration_date)->format('d/M/Y H:i:s');
                else return null;
            })
            ->editColumn('qty',function ($data)
            {
                return number_format($data->qty, 4, '.', ',').'('.trim($data->uom).')';
            })
            ->editColumn('warehouse_id',function ($data)
            {
                if( $data->warehouse_id == '1000002') return 'Warehouse Accessories Aoi 1';
                else if ( $data->warehouse_id == '1000013') return 'Warehouse Accessories Aoi 2';
                else return '-';
            })
            ->setRowAttr([
                'style' => function($data)
                {
                    if($data->is_integrate)  return 'background-color: #d9ffde';
                },
            ])
            ->rawColumns(['style'])
            ->make(true);
        }
    }

    public function store(Request $request)
    {
        $document_movement_id   = $request->document_movement_id;

        $data                   = DB::table('integration_movement_inventory_to_reject')
        ->select('item_id','item_code','uom','warehouse_id',db::raw("sum(qty) as total_qty"))
        ->where('material_movement_id',$document_movement_id)
        ->groupby('item_id','item_code','uom','warehouse_id')
        ->first();

        if($data)
        {
            $item_code              = $data->item_code;
            $uom                    = $data->uom;
            $item_id                = $data->item_id;
            $warehouse_id           = $data->warehouse_id;
            $total_qty              = $data->total_qty;
            $movement_date          = carbon::now();
    
            $inventory_erp = Locator::with('area')
            ->where([
                ['is_active',true],
                ['rack','ERP INVENTORY'],
            ])
            ->whereHas('area',function ($query) use($warehouse_id)
            {
                $query->where('is_active',true);
                $query->where('warehouse',$warehouse_id);
                $query->where('name','ERP INVENTORY');
            })
            ->first();
    
            $system = User::where([
                ['name','system'],
                ['warehouse',$warehouse_id]
            ])
            ->first();
    
            try 
            {
                DB::beginTransaction();
                
                $is_movement_integration_exists = MaterialMovement::where([
                    ['from_location',$inventory_erp->id],
                    ['to_destination',$inventory_erp->id],
                    ['from_locator_erp_id',$inventory_erp->area->erp_id],
                    ['to_locator_erp_id',$inventory_erp->area->erp_id],
                    ['is_integrate',false],
                    ['is_active',true],
                    ['no_packing_list','-'],
                    ['no_invoice','-'],
                    ['status','integration-to-inventory-erp'],
                ])
                ->first();
                
                if(!$is_movement_integration_exists)
                {
                    $movement_integration = MaterialMovement::firstOrCreate([
                        'from_location'         => $inventory_erp->id,
                        'to_destination'        => $inventory_erp->id,
                        'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                        'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                        'po_buyer'              => '-',
                        'is_integrate'          => false,
                        'is_active'             => true,
                        'no_packing_list'       => '-',
                        'no_invoice'            => '-',
                        'status'                => 'integration-to-inventory-erp',
                        'created_at'            => $movement_date,
                        'updated_at'            => $movement_date,
                    ]);
    
                    $movement_integration_id = $movement_integration->id;
                }else
                {
                    $is_movement_integration_exists->updated_at = $movement_date;
                    $is_movement_integration_exists->save();
    
                    $movement_integration_id = $is_movement_integration_exists->id;
                }
    
                $is_material_movement_line_integration_exists = MaterialMovementLine::whereNull('material_preparation_id')
                ->whereNull('material_stock_id')
                ->where([
                    ['material_movement_id',$movement_integration_id],
                    ['qty_movement',$total_qty],
                    ['item_id',$item_id],
                    ['warehouse_id',$warehouse_id],
                    ['is_integrate',false],
                    ['is_active',true],
                    ['date_movement',$movement_date],
                ])
                ->exists();
    
                if(!$is_material_movement_line_integration_exists)
                {
                    MaterialMovementLine::Create([
                        'material_movement_id'          => $movement_integration_id,
                        'item_code'                     => $item_code,
                        'item_id'                       => $item_id,
                        'item_id_source'                => null,
                        'c_order_id'                    => '-',
                        'c_orderline_id'                => '-',
                        'c_bpartner_id'                 => '-',
                        'supplier_name'                 => '-',
                        'type_po'                       => 2,
                        'is_integrate'                  => false,
                        'is_active'                     => true,
                        'uom_movement'                  => $uom,
                        'qty_movement'                  => $total_qty,
                        'date_movement'                 => $movement_date,
                        'created_at'                    => $movement_date,
                        'updated_at'                    => $movement_date,
                        'date_receive_on_destination'   => $movement_date,
                        'nomor_roll'                    => '-',
                        'warehouse_id'                  => $warehouse_id,
                        'document_no'                   => '-',
                        'note'                          => 'RE-INTERGRATION MOVEMENT REJECT DOCUMENT MOVEMENT '.$document_movement_id,
                        'is_active'                     => true,
                        'user_id'                       => $system->id,
                    ]);
                }
    
                MaterialMovement::find($document_movement_id)
                ->update([
                    'is_integrate'      => false,
                    'integration_date'  => null,
                ]);
    
                DB::commit();
    
                return response()->json(200);
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }else
        {
            return response()->json('Document movement not found',422);
        }
        
    }
}
