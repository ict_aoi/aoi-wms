<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class AccessoriesReportMaterialOutSubcontController extends Controller
{
    
    public function index()
    {
        return view('accessories_report_material_out_subcont.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id           = $request->warehouse;
            $start_date             = ($request->start_date) ? Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d') : Carbon::today()->subDays(30);
            $end_date               = ($request->end_date) ? Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d') : Carbon::now();
            
            $material_out_subcont = db::table('mna_report_material_out_subcont')
            ->where([
                ['warehouse',$warehouse_id]
            ])
            ->whereBetween('movement_date', [$start_date, $end_date])
            ->orderBy('movement_date','desc');
            
            
            return DataTables::of($material_out_subcont)
           
            // ->editColumn('created_at',function ($material_in)
            // {
            //     if($material_in->created_at) return Carbon::createFromFormat('Y-m-d', $material_in->created_at)->format('d/M/Y'); 
            //     else return null;
            // })
            ->editColumn('qty',function ($material_out_subcont)
            {
                return number_format($material_out_subcont->qty, 4, '.', ',');
            })
            ->editColumn('warehouse',function ($material_out_subcont)
            {

                if($material_out_subcont->warehouse == '1000002') return 'Warehouse ACC AOI 1';
                else if($material_out_subcont->warehouse == '1000013') return 'Warehouse ACC AOI 2';
            })
          
            // ->rawColumns(['is_piping','action','style','status'])
            ->make(true);
        }
    }
}
