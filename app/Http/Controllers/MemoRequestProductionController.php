<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\PoBuyer;
use App\Models\MaterialPreparation;

class MemoRequestProductionController extends Controller
{
    public function index()
    {
        return view('memo_request_production.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $po_buyer    = $request->po_buyer;
            $mrp        = DB::select(db::raw("SELECT * FROM get_mrp_cte(
                '".$po_buyer."'
                );"
            ));

            return DataTables::of($mrp)
            ->editColumn('statistical_date',function ($mrp)
            {
                if($mrp->statistical_date) return Carbon::createFromFormat('Y-m-d H:i:s', $mrp->statistical_date)->format('d/M/Y');
                else return null;

            })
            ->editColumn('last_movement_date',function ($mrp)
            {
                if($mrp->last_movement_date) return Carbon::createFromFormat('Y-m-d H:i:s', $mrp->last_movement_date)->format('d/M/Y H:i:s');
                else return null;
                    
            })
            ->editColumn('qty_need',function ($mrp)
            {
                return number_format($mrp->qty_need, 4, '.', ',');
            })
            ->editColumn('qty_available',function ($mrp)
            {
                return number_format($mrp->qty_available, 4, '.', ',');
            })
            ->editColumn('qty_supply',function ($mrp)
            {
                return number_format($mrp->qty_supply, 4, '.', ',');
            })
            ->EditColumn('last_status_movement',function ($mrp)
            {
                if($mrp->last_status_movement == 'receiving' or $mrp->last_status_movement == 'receive') return '<span class="label label-info">BARANG DITERIMA DIGUDANG</span>';
                elseif($mrp->last_status_movement == 'not receive') return '<span class="label label-default">BARANG BELUM DITERIMA DIGUDANG</span>';
                elseif($mrp->last_status_movement == 'in') return '<span class="label label-info">MATERIAL SIAP SUPLAI</span>';
                elseif($mrp->last_status_movement == 'expanse') return '<span class="label label-warning">EXPENSE</span>';
                elseif($mrp->last_status_movement == 'out') return '<span class="label label-success">MATERIAL SUDAH DISUPLAI</span>';
                elseif($mrp->last_status_movement == 'out-handover') return '<span class="label label-success">MATERIAL PINDAH TANGAN</span>';
                elseif($mrp->last_status_movement == 'out-subcont') return '<span class="label label-success">MATERIAL SUDAH DISUPLAI KE SUBCONT</span>';
                elseif($mrp->last_status_movement == 'reject') return '<span class="label label-danger">REJECT</span>';
                elseif($mrp->last_status_movement == 'change') return '<span class="label" style="background-color:black">MATERIAL PINDAH LOKASI RAK</span>';
                elseif($mrp->last_status_movement == 'hold') return '<span class="label" style="background-color:#fb8d00">MATERIAL DITAHAN QC UNTUK PENGECEKAN</span>';
                elseif($mrp->last_status_movement == 'adjustment' || $mrp->last_status_movement == 'adjusment') return '<span class="label" style="background-color:#838606">PENYESUAIAN QTY</span>';
                elseif($mrp->last_status_movement == 'print') return '<span class="label" style="background-color:#cc00ff">PRINT BARCODE</span>';
                elseif($mrp->last_status_movement == 'reroute') return '<span class="label" style="background-color:#ff6666">REROUTE</span>';
                elseif($mrp->last_status_movement == 'reverse' || $mrp->last_status_movement == 'reverse-handover' ||  $mrp->last_status_movement == 'reverse-stock') return '<span class="label" style="background-color:#00e6e6">MATERIAL DIKEMBALIKAN KE INVENTORY</span>';    
                elseif($mrp->last_status_movement == 'reverse-qc' ) return '<span class="label" style="background-color:#00e6e6">MATERIAL TIDAK JADI REJECT QC</span>';    
                elseif($mrp->last_status_movement == 'cancel item') return '<span class="label" style="background-color:#ff6666">CANCEL ITEM</span>';
                elseif($mrp->last_status_movement == 'cancel order') return '<span class="label" style="background-color:#ff6666">CANCEL ORDER</span>';
                elseif($mrp->last_status_movement == 'check') return '<span class="label" style="background-color:#ef6a22">MATERIAL DALAM PENGECEKAN QC</span>';
                elseif($mrp->last_status_movement == 'out-cutting') return '<span class="label label-success">MATERIAL SUDAH DISUPLAI KE CUTTING</span>';    
                elseif($mrp->last_status_movement == 'relax') return '<span class="label label-info">RELAX CUTTING</span>';   
                elseif($mrp->last_status_movement == 'switch') return '<span class="label" style="background-color:#333300">SWITCH</span>';   
                elseif($mrp->last_status_movement == 'cancel backlog') return '<span class="label" style="background-color:#b30000">CANCEL BACKLOG</span>'; 
                else return '<span class="label">'.$mrp->last_status_movement.'</span>';   
            })
            ->addColumn('action',function($mrp)
            {
                return view('report_memo_request_production._action',[
                    'model' => $mrp,
                    'movement' => $mrp->po_buyer.'|'.$mrp->item_code.'|'.$mrp->style.'|'.$mrp->article_no
                ]);
                
            })
            ->setRowAttr([
                'style' => function($mrp) use($po_buyer) 
                {
                    $check_po_buyer_is_cancel = PoBuyer::where('po_buyer',$po_buyer)
                    ->whereNotNull('cancel_date')
                    ->exists();

                    if($check_po_buyer_is_cancel){
                        return  'background-color: #f44336;color:white';
                    }

                    if($mrp->warehouse == 'WAREHOUSE ACC AOI 2' || $mrp->warehouse == 'WAREHOUSE FAB AOI 2'){
                        return  'background-color: #ffd6fe;color:black';
                    }
                },
            ])
            ->rawColumns(['style','last_status_movement','action'])
            ->make(true);
            
        }
    }

    public function poBuyerPicklist(Request $request)
    {
        $po_buyer = $request->q;
        $lists = PoBuyer::select('po_buyer')
        ->where(function($query) use ($po_buyer){ 
            $query->where('po_buyer','like',"%$po_buyer%");
        })
        ->groupby('po_buyer')
        ->paginate(10);

        return view('memo_request_production._po_buyer_picklist',compact('lists'));
    }

    public function export(Request $request)
    {
        if($request->po_buyer_id=='')
        {
            return redirect()->back()
            ->withErrors([
                'po_buyer' => 'Please select po buyer first',
            ]);
        } 

        $po_buyer   = trim($request->po_buyer);
        $mrp = DB::select(db::raw("SELECT * FROM get_mrp_cte(
            '".$po_buyer."'
            );"
        ));;

        $file_name = 'LAPORAN_MRP_'.$po_buyer;
        return Excel::create($file_name,function ($excel) use($mrp)
        {
            $excel->sheet('main', function($sheet) use($mrp) {
                $sheet->setCellValue('A1','#');
                $sheet->setCellValue('B1','TANGGAL PO DD');
                $sheet->setCellValue('C1','STATUS BACKLOG');
                $sheet->setCellValue('D1','STYLE');
                $sheet->setCellValue('E1','ARTICLE NO');
                $sheet->setCellValue('F1','PO BUYER');
                $sheet->setCellValue('G1','KATEGORI ITEM');
                $sheet->setCellValue('H1','KODE ITEM');
                $sheet->setCellValue('I1','QTY NEED');
                $sheet->setCellValue('J1','UOM');
                $sheet->setCellValue('K1','STATUS TERAKHIR');
                $sheet->setCellValue('L1','WAREHOUSE');
                $sheet->setCellValue('M1','LOKASI TERAKHIR');
                $sheet->setCellValue('N1','TANGGAL PERPINDAHAN TERAKHIR');
                $sheet->setCellValue('O1','USER YANG MELAKUKAN PERPINDAHAN TERAKHIR');
                $sheet->setCellValue('P1','QTY AVAILABLE');
                $sheet->setCellValue('Q1','QTY SUPPLY');
                $sheet->setCellValue('R1','PO SUPPLIER');
                $sheet->setCellValue('S1','REMARK');
                $sheet->setCellValue('T1','SYSTEM LOG');

                $index = 0;
                foreach ($mrp as $key => $value) {
                    $row = $index + 2;
                    $index++;

                    if($value->last_status_movement == 'receiving') $last_status_movement = 'BARANG DITERIMA DIGUDANG';
                    else if($value->last_status_movement == 'not receive') $last_status_movement = 'BARANG BELUM DITERIMA DIGUDANG';
                    else if($value->last_status_movement == 'in') $last_status_movement = 'MATERIAL SIAP SUPLAI';
                    elseif($value->last_status_movement == 'expanse') $last_status_movement = 'EXPENSE';
                    elseif($value->last_status_movement == 'out') $last_status_movement = 'MATERIAL SUDAH DISUPLAI';
                    elseif($value->last_status_movement == 'out-handover')  $last_status_movement = 'MATERIAL PINDAH TANGAN';
                    elseif($value->last_status_movement == 'out-subcont') $last_status_movement = 'MATERIAL SUDAH DISUPLAI KE SUBCONT';
                    elseif($value->last_status_movement == 'reject') $last_status_movement = 'REJECT';
                    elseif($value->last_status_movement == 'change') $last_status_movement = 'MATERIAL PINDAH LOKASI RAK';
                    elseif($value->last_status_movement == 'hold') $last_status_movement = 'MATERIAL DITAHAN QC UNTUK PENGECEKAN';
                    elseif($value->last_status_movement == 'adjustment' || $value->last_status_movement == 'adjusment') $last_status_movement = 'PENYESUAIAN QTY';
                    elseif($value->last_status_movement == 'print') $last_status_movement = 'PRINT BARCODE';
                    elseif($value->last_status_movement == 'reroute') $last_status_movement = 'REROUTE';
                    elseif($value->last_status_movement == 'cancel item') $last_status_movement = 'CANCEL ITEM';
                    elseif($value->last_status_movement == 'cancel order') $last_status_movement = 'CANCEL ORDER';
                    elseif($value->last_status_movement == 'check') $last_status_movement = 'MATERIAL DALAM PENGECEKAN QC';
                    else if($value->last_status_movement == 'out-cutting') $last_status_movement = 'MATERIAL SUDAH DISUPLAI KE CUTTING';    
                    else if($value->last_status_movement == 'relax')  $last_status_movement = 'RELAX CUTTING';   
                    else $last_status_movement = $value->last_status_movement;
                    
                    if($value->backlog_status) $status_backlog = $value->backlog_status;
                    else $status_backlog = '-';

                    if($value->qty_available) $qty_available = $value->qty_available;
                    else $qty_available = '0';

                    if($value->qty_supply) $qty_supply = $value->qty_supply;
                    else $qty_supply = '0';

                    if($value->warehouse) $warehouse = $value->warehouse;
                    else $warehouse = '-';

                    if($value->last_locator_code) $last_locator_code = $value->last_locator_code;
                    else $last_locator_code = '-';
                    
                    if($value->remark) $remark = $value->remark;
                    else $remark = '-';

                    if($value->document_no) $document_no = $value->document_no;
                    else $document_no = '-';

                    if($value->system_log) $system_log = $value->system_log;
                    else $system_log = '-';
                    
                    if($value->last_user_movement_name) $last_user_movement_name = $value->last_user_movement_name;
                    else $last_user_movement_name = '-';

                    if($value->last_movement_date) $last_movement_date = $value->last_movement_date;
                    else $last_movement_date = '-';
                    
                    $sheet->setCellValue('A'.$row, $index);
                    $sheet->setCellValue('B'.$row, $value->statistical_date);
                    $sheet->setCellValue('C'.$row, $status_backlog);
                    $sheet->setCellValue('D'.$row, $value->style);
                    $sheet->setCellValue('E'.$row, $value->article_no);
                    $sheet->setCellValue('F'.$row, $value->po_buyer);
                    $sheet->setCellValue('G'.$row, $value->category);
                    $sheet->setCellValue('H'.$row, $value->item_code);
                    $sheet->setCellValue('I'.$row, $value->qty_need);
                    $sheet->setCellValue('J'.$row, $value->uom);
                    $sheet->setCellValue('K'.$row, $last_status_movement);
                    $sheet->setCellValue('L'.$row, $warehouse);
                    $sheet->setCellValue('M'.$row, $last_locator_code);
                    $sheet->setCellValue('N'.$row, $last_movement_date);
                    $sheet->setCellValue('O'.$row, $last_user_movement_name);
                    $sheet->setCellValue('P'.$row, $qty_available);
                    $sheet->setCellValue('Q'.$row, $qty_supply);
                    $sheet->setCellValue('R'.$row, $document_no);
                    $sheet->setCellValue('S'.$row, $remark);
                    $sheet->setCellValue('T'.$row, $system_log);
                }
            });

        })
        ->export('xls');
    }

    public function printPreview(Request $request)
    {
        $po_buyer = trim($request->_print_po_buyer);

        if($po_buyer == '')
        {
            return redirect()->back()
            ->withErrors([
                'po_buyer' => 'Please select po buyer first',
            ]);
        } 

        $mrp = DB::select(db::raw("SELECT * FROM get_mrp_cte(
            '".$po_buyer."'
            );"
        ));;


        return view('memo_request_production.print', compact('mrp'));
    }

    public function downloadAll()
    {
        $filename   = 'Report_MRP.csv';
        $file       = Config::get('storage.mrp') . '/' . $filename;
        
        if(!file_exists($file)) return 'File not found.';
        
        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function dataHistory(Request $request)
    {
        if(request()->ajax()) 
        {
            $po_buyer   = $request->po_buyer;
            $item_code  = $request->item_code;
            $article_no = $request->article_no;
            $style      = $request->style;

            $histories  = MaterialPreparation::where([
                ['po_buyer',$po_buyer],
                ['item_code',$item_code],
                ['article_no',$article_no],
                ['style',$style],
            ])
            ->orderby('last_movement_date','desc');

            return DataTables::of($histories)
            ->editColumn('qty_conversion',function ($histories)
            {
                return number_format($histories->qty_conversion, 4, '.', ',');
            })
            ->editColumn('last_movement_date',function ($histories)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $histories->last_movement_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('last_locator_id',function($histories)
            {
                if($histories->last_locator_id !=null) return $histories->lastLocator->code;
                else return '';
            })
            ->editColumn('last_user_movement_id',function($histories)
            {
                if($histories->last_user_movement_id !=null) return $histories->lastUserMovement->name;
                else return '';
            })
            ->make(true);
        }
    }
    
}
