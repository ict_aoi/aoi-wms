<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Models\Rma;
use App\Models\Item;
use App\Models\Defect;
use App\Models\Locator;
use App\Models\DetailMasterDataFabricTesting;
use App\Models\MasterDataFabricTesting;
use App\Models\AllocationItem;
use App\Models\UomConversion;
use App\Models\MaterialStock;
use App\Models\MaterialCheck;
use App\Models\MaterialArrival;
use App\Models\DetailMaterialCheck;
use App\Models\MovementStockHistory;

class FabricMaterialTestingController extends Controller
{
    public function index()
    {
      return view('fabric_material_testing.index');
    }

    public function create(request $request)
    {
        $_warehouse_id  = $request->warehouse_id;
        $_barcode       = strtoupper(trim($request->barcode));

        $material = MaterialStock::where([
            ['barcode_supplier', $_barcode],
            ['warehouse_id', $_warehouse_id],
        ])
        ->first();

        if(!$material) return response()->json('Barcode not found', 422);
        if($material->po_detail_id = null || $material->po_detail_id = '') return response()->json('Please contact ICT',422);

        if($material->is_reject_by_lot) return response()->json('Roll cannot be used due of lot is not match',422);
        
        if($material->is_reject)
        {
            if($material->is_reject_by_rma) return response()->json('Roll cannot be used due of material is wrong',422);
            else return response()->json('Roll cannot be used due of reject result by qc',422);
        } 
        //if($material->inspect_lot_result == 'HOLD') return response()->json('Roll is in hold by qc due of lot inspection.',422);
        

        $is_material_check = MaterialCheck::where('material_stock_id',$material->id)
        ->whereNull('deleted_at')
        ->exists();

        if($is_material_check) return response()->json('Material already scanned', 422);

        $total = 0;
        
        $_options = MasterDataFabricTesting::select('testing_name','id')->orderBy('created_at', 'desc')
        ->get();
        $options = array();
        $no = 0;
        foreach($_options as $key => $list)
        {
          $options [] = [
                        'id'   => $list->id,
                        'name' => $list->testing_name,
          ];

          if($no == 0)
          {
            $_detail_options = DetailMasterDataFabricTesting::select('subtesting_name','id')
                                                       ->where('master_data_fabric_testing_id', $list->id)
                                                       ->get();
            foreach($_detail_options as $key => $list)
            {
              $detail_options [] = [
                            'id'   => $list->id,
                            'name' => $list->subtesting_name,
              ];
            }
          }
          $no += 1;
        }

        $_item                          = Item::where('item_code',$material->item_code)->first();
        $obj                            = new StdClass();
        $obj->material_stock_id         = $material->id;
        $obj->item_id                   = $material->item_id;
        $obj->barcode                   = $material->barcode_supplier;
        $obj->material_arrival_id       = $material->material_arrival_id;
        $obj->c_bpartner_id             = $material->c_bpartner_id;
        $obj->supplier_name             = $material->supplier_name;
        $obj->document_no               = $material->document_no;
        $obj->item_code                 = $material->item_code;
        $obj->color                     = ($_item)? $_item->color : 'COLOR NOT FOUND';
        $obj->no_invoice                = $material->no_invoice;
        $obj->category                  = $material->category;
        $obj->nomor_roll                = $material->nomor_roll;
        $obj->batch_number              = $material->batch_number;
        $obj->uom                       = $material->uom;
        $obj->_qty                      = $material->qty_arrival;
        $obj->qty                       = $material->qty_arrival;
        $obj->status_options            = $options;
        $obj->detail_status_options     = $detail_options;
        $obj->status                    = 'PILIH';
        $obj->movement_date             = Carbon::now()->toDateTimeString();
        $obj->warehouse_id              = $material->warehouse_id;
        $allocation_items               = AllocationItem::select(db::raw("string_agg(distinct _style, ',') as style"))
                                                          ->where('item_id_source', $material->item_id)
                                                          ->where('c_order_id', $material->c_order_id)
                                                          ->groupBy('_style')
                                                          ->first();
        $obj->style                     = ($allocation_items)? $allocation_items->style : 'STYLE NOT FOUND';
        $obj->composition              = $_item->composition;
        $obj->detail                    = [];
        //dd($obj);

        return response()->json($obj,200);
    }

    public function getSubtesting($id)
    {
      $_detail_options = DetailMasterDataFabricTesting::where('master_data_fabric_testing_id', $id)
                                                      ->pluck('subtesting_name','id')
                                                       ->all();

      return response()->json(
        ['detail_options'    => $_detail_options],200);
    }

  }



