<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Models\User;
use App\Models\Item;
use App\Models\Locator;
use App\Models\OrderErp;
use App\Models\Category;
use App\Models\PoBuyer;
use App\Models\Supplier;
use App\Models\Temporary;
use App\Models\MaterialMoq;
use App\Models\PurchaseItem;
use App\Models\MaterialStock;
use App\Models\UomConversion;
use App\Models\MappingStocks;
use App\Models\ReroutePoBuyer;
use App\Models\AutoAllocation;
use App\Models\AllocationItem;
use App\Models\MaterialArrival;
use App\Models\MaterialSubcont;
use App\Models\ItemPackingList;
use App\Models\MaterialExclude;
use App\Models\MaterialBacklog;
use App\Models\MaterialMovement;
use App\Models\DetailMaterialStock;
use App\Models\MaterialRequirement;
use App\Models\MaterialPreparation;
use App\Models\PurchaseOrderDetail;
use App\Models\PurchaseOrderDetailDev;
use App\Models\MaterialMovementLine;
use App\Models\DetailMaterialArrival;
use App\Models\DetailMaterialSubcont;
use App\Models\MaterialReadyPreparation;
use App\Models\DetailMaterialPreparation;

use App\Http\Controllers\ReroutePoBuyerController;
use App\Http\Controllers\AccessoriesReportMaterialStockController as ReportStock;
use App\Http\Controllers\AccessoriesMaterialStockApprovalController as HistoryStock;
use App\Http\Controllers\AccessoriesMaterialStockOnTheFlyController as SOTF;

class AccessoriesMaterialArrivalController extends Controller
{
    public function index()
    {
        // return view('errors.migration');
        return view('accessories_material_arrival.index');
    }

    public function create(request $request)
    {
        $warehouse_id           = $request->warehouse_id;
        $movement_date          = Carbon::now()->toDateTimeString();
        $has_dash               = strpos($request->po_detail_id, "-");
        $_flag                  = 0;
        $array                  = array();

        if($has_dash == false)
        {
            $po_detail_id   = strtoupper($request->po_detail_id);
            $flag           = 1;
        }else
        {
            $split          = explode('-',$request->po_detail_id);
            $po_detail_id   = strtoupper($split[0]);
            $flag           = 0;
        }

        $material_arrival = MaterialArrival::where([
            ['po_detail_id',$po_detail_id],
            ['warehouse_id',$warehouse_id],
        ])
        ->whereNull('deleted_at')
        ->first();

        if($material_arrival)
        {
            $material_subcont = MaterialSubcont::where([
                ['barcode',$po_detail_id],
                ['warehouse_id',$warehouse_id],
                ['is_active',true],
            ])
            ->whereNull('date_receive')
            ->first();

            if(!$material_subcont) if($material_arrival->is_ready_to_prepare) return response()->json('Material already received',422);
            if($material_arrival->qty_carton > 1) if($flag == 1) return response()->json('Total carton more than 1, wrong barcode.',422);
            

            $material_arrival_id = $material_arrival->id;
            $po_detail_id        = $material_arrival->po_detail_id;
            $item_code           = $material_arrival->item_code;
            $category            = $material_arrival->category;
            $uom                 = $material_arrival->uom;
            $po_buyer            = $material_arrival->po_buyer;
            $replace_po_buyer    = trim(str_replace('-S', '',$po_buyer));
            $list_po_buyer       = explode(', ', $replace_po_buyer);
            $po_buyer            = array_unique($list_po_buyer);
            $po_buyer            = implode(',', $po_buyer);
            $qty_upload          = sprintf('%0.8f',$material_arrival->qty_upload);
            $foc                 = sprintf('%0.8f',$material_arrival->foc);
            $document_no         = strtoupper(trim($material_arrival->document_no));
            $item_id             = $material_arrival->item_id;
            $c_order_id          = $material_arrival->c_order_id;
            $c_orderline_id      = $material_arrival->c_orderline_id;
            $c_bpartner_id       = $material_arrival->c_bpartner_id;
            $type_po             = $material_arrival->type_po;
            $no_resi             = $material_arrival->no_packing_list;
            $no_packing_list     = $material_arrival->no_resi;
            $no_surat_jalan      = $material_arrival->no_surat_jalan;
            $no_invoice          = $material_arrival->no_invoice;
            $warehouse_id        = $material_arrival->warehouse_id;
            $etd_date            = $material_arrival->etd_date;
            $eta_date            = $material_arrival->eta_date;
            $season              = $material_arrival->season;
            $po_buyer            = trim($material_arrival->po_buyer);
            $item_desc           = strtoupper($material_arrival->item_desc);
            $supplier_name       = strtoupper($material_arrival->supplier_name);
            $qty_entered         = sprintf('%0.8f', $material_arrival->qty_entered);
            $qty_ordered         = sprintf('%0.8f', $material_arrival->qty_ordered);
            $qty_carton          = sprintf('%0.8f', $material_arrival->qty_carton);
            $qty_upload          = sprintf('%0.8f', $material_arrival->qty_upload);
            $qty_delivered       = sprintf('%0.8f', $material_arrival->qty_delivered);
            $counter             = $material_arrival->counter($material_arrival_id);
            $is_subcont          = $material_arrival->is_subcont;
            $is_po_cancel        = PoBuyer::where('po_buyer',$po_buyer)->whereNotNull('cancel_date')->exists();

            $reroute                            = ReroutePoBuyer::where('old_po_buyer',$po_buyer)->first();
            if($reroute)
            {
                $show_po_buyer  = $reroute->new_po_buyer.' (po buyer ini reroute dari po '.$po_buyer.')';
                $po_buyer       = $reroute->new_po_buyer;
            }else
            {
                $show_po_buyer  = $po_buyer;
            }

            $is_stock_already_created = MaterialStock::where([
                ['document_no',$document_no],
                ['po_buyer',$po_buyer],
                ['item_code',$item_code],
                ['warehouse_id',$warehouse_id],
                ['is_stock_on_the_fly',true],
                ['is_running_stock',false],
            ])
            ->whereNotNull('po_buyer')
            ->exists();
            
            if($is_stock_already_created)
            {
                $is_po_cancel   = true;
                $is_sotf        = true;
            } 
            else
            {
                $is_po_cancel   = $is_po_cancel;
                $is_sotf        = false;
            }

            $conversion = UomConversion::where([
                ['item_code',$item_code],
                ['uom_from',strtoupper($uom)]
            ])
            ->first();

            if($conversion)
            {
                $dividerate     = $conversion->dividerate;
                $uom_conversion = $conversion->uom_to;
            }else
            {
                $dividerate     = 1;
                $uom_conversion = $uom;
            }

            $qty_conversion = sprintf('%0.8f', $dividerate * $qty_upload);

            // //untuk menampilkan alokasi warehouse place
            // $erp = DB::connection('erp');
            // $warehouse_place = $erp->data;

            /*
                info dari mba hani udah ga terlalu penting
                $is_backlog = $this->checkIsBacklog($po_buyer,$item_code);
            */
            
            $flag_is_exists     = false;
            $_po_buyer          = '';
            $_barcode_supplier  = '';

            $array = 
            [
                'id'                        => $material_arrival_id,
                'po_detail_id'              => $po_detail_id,
                'material_subcont_id'       => ($material_subcont) ? $material_subcont->id : null,
                'item_id'                   => $item_id,
                'item_code'                 => $item_code,
                'item_desc'                 => $item_desc,
                'supplier_name'             => $supplier_name,
                'document_no'               => $document_no,
                'c_order_id'                => $c_order_id ,
                'c_orderline_id'            => $c_orderline_id,
                'c_bpartner_id'             => $c_bpartner_id,
                'type_po'                   => $type_po,
                'uom'                       => $uom,
                'uom_conversion'            => $uom_conversion,
                'warehouse_id'              => $warehouse_id,
                'category'                  => $category,
                'no_packing_list'           => $no_packing_list,
                'season'                    => $season,
                'no_resi'                   => $no_resi,
                'no_surat_jalan'            => $no_surat_jalan,
                'no_invoice'                => $no_invoice,
                'etd_date'                  => $etd_date,
                'eta_date'                  => $eta_date,
                'po_buyer'                  => $po_buyer,
                'show_po_buyer'             => $show_po_buyer,
                'qty_entered'               => $qty_entered,
                'qty_ordered'               => $qty_ordered,
                'qty_carton'                => $qty_carton,
                'qty_upload'                => $qty_upload,
                'qty_conversion'            => $qty_conversion,
                'flag_moq'                  => false,
                'qty_moq'                   => 0,
                'uom_moq'                   => null,
                'is_moq'                    => false,
                'is_po_cancel'              => $is_po_cancel,
                'is_stof'                   => $is_sotf,
                'is_backlog'                => false,
                'foc'                       => $foc,
                'counter'                   => $counter,
                'prepared_status'           => $this->getNeedPrepareStatus($po_buyer,$item_code,$category,$uom,$is_subcont,$qty_conversion),
                'is_other_buyer_checkout'   => $this->getOthersIsCheckout($po_buyer,$item_code,$warehouse_id),
                'movement_date'             => $movement_date,
            ];

            $detail_material_arrivals   = $material_arrival->detailMaterialArrivals()->get();
            $array['is_exists']         = 0;
            $array['counter']           = $counter;
            
            foreach ($detail_material_arrivals as $key => $detail_material_arrival) 
            {
                $barcode_supplier           = $detail_material_arrival->barcode_supplier;
                $array['po_buyer']          = $po_buyer;
                $array['qty_delivered']     = $qty_delivered;
                $array['qty_upload']        = $qty_upload;
                
                $obj                        = new StdClass();
                $obj->material_arrival_id   = $material_arrival_id;
                $obj->po_buyer              = $po_buyer;
                $obj->barcode_supplier      = $barcode_supplier;
                $obj->qty_delivered         = $qty_delivered;
                $obj->qty_upload            = $qty_upload;
                $array['details'] []        = $obj;

                if($request->po_detail_id == $barcode_supplier)
                {
                    $array['is_exists']     = 1;
                    $flag_is_exists         = true;
                }
            }

            $index = count($array['details']);
            if($flag_is_exists == false)
            {
                $array['counter']           = $counter+1;
                $obj                        = new StdClass();
                $obj->material_arrival_id   = -1;
                $obj->po_buyer              = $po_buyer;
                $obj->barcode_supplier      = $request->po_detail_id;
                $obj->qty_delivered         = $qty_delivered;
                $obj->qty_upload            = $qty_upload;
                $array['details'] [$index]  = $obj;
                $index++;
            }
        }else
        {

            $material_subcont = MaterialSubcont::where([
                ['barcode',$po_detail_id],
                ['warehouse_id',$warehouse_id],
            ])
            ->whereNull('date_receive')
            ->first();

            if($material_subcont)
            {
                if($material_subcont->date_receive) return response()->json('Material already received.',422);
                if($material_subcont->qty_carton > 1) if($flag == 1) return response()->json('total carton lebih dari satu, barcode salah.',422);
                

                $category                   = $material_subcont->category;
                $uom                        = trim($material_subcont->uom);
                $qty_upload                 = sprintf('%0.8f',$material_subcont->qty_upload);
                $qty_entered                = sprintf('%0.8f', $material_subcont->qty_entered);
                $qty_ordered                = sprintf('%0.8f', $material_subcont->qty_ordered);
                $qty_carton                 = sprintf('%0.8f', $material_subcont->qty_carton);
                $is_backlog                 = ($material_subcont->material_movement_line_id)? $material_subcont->materialMovementLine->materialPreparation->is_backlog : null;
                $_po_detail_id              = $po_detail_id;
                $material_subcont_id        = $material_subcont->id;
                $item_id                    = $material_subcont->item_id;
                $c_order_id                 = $material_subcont->c_order_id;
                $type_po                    = $material_subcont->type_po;
                $c_orderline_id             = $material_subcont->c_orderline_id;
                $c_bpartner_id              = $material_subcont->c_bpartner_id;
                $category                   = $material_subcont->category;
                $season                     = $material_subcont->season;
                $warehouse_id               = $material_subcont->warehouse_id;
                $no_packing_list            = $material_subcont->no_packing_list;
                $no_resi                    = $material_subcont->no_resi;
                $no_invoice                 = $material_subcont->no_invoice;
                $no_surat_jalan             = $material_subcont->no_surat_jalan;
                $etd_date                   = $material_subcont->etd_date;
                $eta_date                   = $material_subcont->eta_date;
                $item_code                  = strtoupper(trim($material_subcont->item_code));
                $po_buyer                   = trim($material_subcont->po_buyer);
                $item_desc                  = strtoupper(trim($material_subcont->item_desc));
                $supplier_name              = strtoupper(trim($material_subcont->supplier_name));
                $document_no                = strtoupper(trim($material_subcont->document_no));
                $is_po_cancel               = PoBuyer::where('po_buyer',$po_buyer)->whereNotNull('cancel_date')->exists();

                $reroute                            = ReroutePoBuyer::where('old_po_buyer',$po_buyer)->first();
                if($reroute)
                {
                    $show_po_buyer  = $reroute->new_po_buyer.' (po buyer ini reroute dari po '.$po_buyer.')';
                    $po_buyer       = $reroute->new_po_buyer;
                }else
                {
                    $show_po_buyer  = $po_buyer;
                }
                
                $is_stock_already_created = MaterialStock::where([
                    ['document_no',$document_no],
                    ['po_buyer',$po_buyer],
                    ['item_code',$item_code],
                    ['warehouse_id',$warehouse_id],
                    ['is_stock_on_the_fly',true],
                    ['is_running_stock',false],
                ])
                ->whereNull('deleted_at')
                ->whereNotNull('po_buyer')
                ->exists();
                
                if($is_stock_already_created)
                {
                    $is_po_cancel   = true;
                    $is_sotf        = true;
                } 
                else
                {
                    $is_po_cancel   = $is_po_cancel;
                    $is_sotf        = false;
                }
            
                
                $conversion = UomConversion::where([
                    ['item_code',$item_code],
                    ['category',$category],
                    ['uom_from',strtoupper($uom)]
                ])
                ->first();

                if($conversion)
                {
                    $dividerate     = $conversion->dividerate;
                    $uom_conversion = $conversion->uom_to;
                }else
                {
                    $dividerate     = 1;
                    $uom_conversion = $material_subcont->uom;
                }

                $qty_conversion = sprintf('%0.8f', $dividerate * $qty_upload);
                $array = [
                    'id'                        => -1,
                    'po_detail_id'              => $_po_detail_id,
                    'material_subcont_id'       => $material_subcont_id,
                    'item_id'                   => $item_id,
                    'item_code'                 => $item_code,
                    'po_buyer'                  => $po_buyer,
                    'show_po_buyer'             => $show_po_buyer,
                    'item_desc'                 => $item_desc,
                    'supplier_name'             => $supplier_name,
                    'document_no'               => $document_no,
                    'c_order_id'                => $c_order_id ,
                    'type_po'                   => $type_po ,
                    'c_orderline_id'            => $c_orderline_id,
                    'c_bpartner_id'             => $c_bpartner_id,
                    'uom'                       => $uom,
                    'season'                    => $season,
                    'uom_conversion'            => $uom_conversion,
                    'warehouse_id'              => $warehouse_id,
                    'category'                  => $category,
                    'no_packing_list'           => $no_packing_list,
                    'no_resi'                   => $no_resi,
                    'no_surat_jalan'            => $no_surat_jalan,
                    'no_invoice'                => $no_invoice,
                    'etd_date'                  => $etd_date,
                    'eta_date'                  => $eta_date,
                    'qty_entered'               => $qty_entered,
                    'qty_ordered'               => $qty_ordered,
                    'qty_carton'                => $qty_carton,
                    'qty_delivered'             => 0,
                    'qty_upload'                => $qty_upload,
                    'qty_conversion'            => $qty_conversion,
                    'foc'                       => 0,
                    'counter'                   => 1,
                    'is_exists'                 => 0,
                    'flag_moq'                  => false,
                    'is_backlog'                => $is_backlog,
                    'is_po_cancel'              => $is_po_cancel,
                    'is_sotf'                   => $is_sotf,
                    'qty_moq'                   => 0,
                    'uom_moq'                   => null,
                    'po_buyer'                  => $po_buyer,
                    'prepared_status'           => $this->getNeedPrepareStatus($po_buyer,$item_code,$category,$uom,true,$qty_conversion),
                    'is_other_buyer_checkout'   => $this->getOthersIsCheckout($po_buyer,$item_code,$warehouse_id),
                    'movement_date'             => $movement_date

                ];

                $array['details'] [] = [
                    'material_arrival_id'       => -1,
                    'po_buyer'                  => $po_buyer,
                    'barcode_supplier'          => trim($po_detail_id),
                    'qty_delivered'             => 0,
                    'qty_upload'                => $qty_upload,
                ];


                return response()->json($array);
            }else
            {
                $app_env = Config::get('app.env');
                if($app_env == 'live')
                {
                    $web_po = PurchaseOrderDetail::where([
                        ['po_detail_id',$po_detail_id],
                        ['type_po',2],
                    ])
                    ->first();
                }else if ($app_env == 'dev')
                {
                    $web_po = PurchaseOrderDetailDev::where([
                        ['po_detail_id',$po_detail_id],
                        ['type_po',2],
                    ])
                    ->first();
                }else
                {
                    $web_po = PurchaseOrderDetail::where([
                        ['po_detail_id',$po_detail_id],
                        ['type_po',2],
                    ])
                    ->first();
                }
                
                
                if(!$web_po) return response()->json('Barcode not found !.',422);
                if($web_po->isactive == false) return response()->json('Barcode is inactive, please contact ict for further information.',422);
                if($web_po->is_locked == false || $web_po->flag_erp == false) return response()->json('Barcode is unlock, please contact ict for further information.',422);
                if($web_po->flag_wms == true) return response()->json('Barcode already scanned.',422);

                if($web_po->m_warehouse_id != $warehouse_id)
                {
                    if($web_po->m_warehouse_id == 1000002) $_temp = 'Accessories AOI 1';
                    else if($web_po->m_warehouse_id == 1000013) $_temp = 'Accessories AOI 2';

                    return response()->json('Warehouse destination is '.$_temp.', is not equals with your warehouse.',422);
                }

                if($web_po->qty_carton > 1) if($flag == 1) return response()->json('Total carton more than 1, barcode is wrong.',422);
                

                $po_detail_id                       = $web_po->po_detail_id;
                $item_code                          = $web_po->item;
                $category                           = $web_po->category;
                $uom                                = trim($web_po->uomsymbol);
                $qty_upload                         = sprintf('%0.8f', $web_po->qty_upload);
                $po_buyer                           = trim($web_po->pobuyer);
                $document_no                        = trim(strtoupper($web_po->documentno));
                $warehouse_id                       = trim($web_po->m_warehouse_id);
                $c_bpartner_id                      = trim($web_po->c_bpartner_id);
                $supplier                           = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                $item_id                            = $web_po->m_product_id;
                $item_desc                          = trim(strtoupper($web_po->desc_product));
                $supplier_name                      = ($supplier ? $supplier->supplier_name : trim(strtoupper($web_po->supplier)) );
                $c_order_id                         = $web_po->c_order_id;
                $type_po                            = $web_po->type_po;
                $c_orderline_id                     = $web_po->c_orderline_id;
                $no_packinglist                     = $web_po->no_packinglist;
                $no_resi                            = $web_po->kst_resi;
                $no_surat_jalan                     = $web_po->kst_suratjalanvendor;
                $no_invoice                         = $web_po->kst_invoicevendor;
                $etd_date                           = $web_po->kst_etddate;
                $eta_date                           = $web_po->kst_etadate;
                $season                             = $web_po->kst_season;
                $qty_entered                        = sprintf('%0.8f', $web_po->qtyentered);
                $qty_ordered                        = sprintf('%0.8f', $web_po->qtyordered);
                $qty_carton                         = sprintf('%0.8f', $web_po->qty_carton);
                $qty_delivered                      = sprintf('%0.8f', $web_po->qtydelivered);
                $foc                                = sprintf('%0.8f', $web_po->foc);

                $conversion = UomConversion::where([
                    ['item_code',$item_code],
                    ['category',$category],
                    ['uom_from',strtoupper($uom)]
                ])
                ->first();

                if($conversion)
                {
                    $dividerate     = $conversion->dividerate;
                    $uom_conversion = $conversion->uom_to;
                }else
                {
                    $dividerate     = 1;
                    $uom_conversion = $web_po->uomsymbol;
                }
                
                $qty_conversion     = sprintf('%0.8f', $dividerate * $qty_upload);
                $prepared_status    = $this->getNeedPrepareStatus($po_buyer,$item_code,$category,$uom,false,$qty_conversion);

                $reroute                            = ReroutePoBuyer::where('old_po_buyer',$po_buyer)->first();
                if($reroute)
                {
                    $show_po_buyer  = $reroute->new_po_buyer.' (po buyer ini reroute dari po '.$po_buyer.')';
                    $po_buyer       = $reroute->new_po_buyer;
                }else
                {
                    $show_po_buyer  = $po_buyer;
                }

                if($prepared_status == 'NO NEED PREPARED')
                {
                    Temporary::FirstOrCreate([
                        'barcode'       => $po_detail_id,
                        'string_1'      => $po_buyer,
                        'string_2'      => $c_order_id,
                        'string_3'      => $c_bpartner_id,
                        'string_4'      => $item_id,
                        'double_1'      => $qty_conversion,
                        'status'        => 'material_arrival',
                        'user_id'       => Auth::user()->id,
                        'created_at'    => $movement_date,
                        'updated_at'    => $movement_date,
                    ]);

                    $moq                = $this->getMoq($po_detail_id,$c_order_id,$item_id,$document_no,$item_code, $po_buyer,$warehouse_id,$qty_conversion,$category,$uom,$c_bpartner_id);
                
                    if($moq)
                    {
                        $uom_moq    = $moq->uom;
                        $qty_moq    = sprintf('%0.8f',$moq->qty_book);
                        $flag_moq   = $moq->is_moq ?'Pembelian item ini terkena MOQ, silahkan kembalikan qty sejumlah '.$qty_moq.'('.$uom_moq.') ke bagian free stock' : false;
                        $is_moq     = true;
                    }else
                    {
                        $uom_moq    = null;
                        $qty_moq    = 0;
                        $flag_moq   = null;
                        $is_moq     = false;
                    }
                }else
                {
                    $uom_moq    = null;
                    $qty_moq    = 0;
                    $flag_moq   = null;
                    $is_moq     = false;
                }
                

                $is_po_cancel               = PoBuyer::where('po_buyer',$po_buyer)->whereNotNull('cancel_date')->exists();
                $is_stock_already_created   = MaterialStock::where([
                    ['document_no',$document_no],
                    ['po_buyer',$po_buyer],
                    ['item_code',$item_code],
                    ['warehouse_id',$warehouse_id],
                    ['is_stock_on_the_fly',true],
                    ['is_running_stock',false],
                ])
                ->whereNull('deleted_at')
                ->whereNotNull('po_buyer')
                ->exists();

                
                if($is_stock_already_created)
                {
                    $is_po_cancel   = true;
                    $is_sotf        = true;
                } 
                else
                {
                    $is_po_cancel   = $is_po_cancel;
                    $is_sotf        = false;
                }

                $material_requirement = MaterialRequirement::whereIn('po_buyer',explode(",",$po_buyer))
                ->where('item_code', $item_code)
                ->first();
        
                if($material_requirement)
                {
                    if($material_requirement->warehouse_id)
                    {
                        $warehouse_place = $material_requirement->warehouse_id;
                        if($warehouse_place == '1000007') $_warehouse_place = '1000002';
                        else if($warehouse_place == '1000010') $_warehouse_place = '1000013';
                        else $_warehouse_place = $warehouse_id;
        
                        if($_warehouse_place != $warehouse_id) $is_handover = true;
                        else $is_handover = false;
                    } else
                    {
                        $is_handover = false;
                        $_warehouse_place = $warehouse_id;
                    }
                }
                else
                {
                    $is_handover = false;
                    $_warehouse_place = $warehouse_id;
                }

                  //alokasi subcont / beda whs 
                    $erp = DB::connection('erp');
                    $sewing_place = $erp->table('bw_report_material_requirement_new_v1')->where([
                        ['poreference',$po_buyer],
                        ['componentid',$item_id]
                    ])
                    ->select('warehouse_place')
                    ->first();
                    $sp = null;
                    if($sewing_place != null)
                    {
                        $sp = $sewing_place->warehouse_place;
                    }else{
                        $sp = null;
                    }


                $subconts = Pobuyer::whereNotNull('note')
                ->whereIn('po_buyer',explode(",",$po_buyer))
                ->first();

                if($subconts)
                {
                    $is_subcont = true;
                    $note       = $subconts->note;
                }
                else
                {
                    $is_subcont = false;
                    $note       = null;
                }
            
                $array = [
                    'id'                        => -1,
                    'po_detail_id'              => $po_detail_id,
                    'material_subcont_id'       => null,
                    'item_id'                   => $item_id,
                    'item_code'                 => $item_code,
                    'po_buyer'                  => $po_buyer,
                    'show_po_buyer'             => $show_po_buyer,
                    'item_desc'                 => $item_desc,
                    'supplier_name'             => $supplier_name,
                    'document_no'               => $document_no,
                    'c_order_id'                => $c_order_id ,
                    'type_po'                   => $type_po ,
                    'c_orderline_id'            => $c_orderline_id,
                    'c_bpartner_id'             => $c_bpartner_id,
                    'uom'                       => $uom,
                    'season'                    => $season,
                    'uom_conversion'            => $uom_conversion,
                    'warehouse_id'              => $warehouse_id,
                    'category'                  => $category,
                    'no_packing_list'           => $no_packinglist,
                    'no_resi'                   => $no_resi,
                    'no_surat_jalan'            => $no_surat_jalan,
                    'no_invoice'                => $no_invoice,
                    'etd_date'                  => $etd_date,
                    'eta_date'                  => $eta_date,
                    'qty_entered'               => $qty_entered,
                    'qty_ordered'               => $qty_ordered,
                    'qty_carton'                => $qty_carton,
                    'qty_delivered'             => $qty_delivered,
                    'qty_conversion'            => $qty_conversion,
                    'qty_upload'                => $qty_upload,
                    'foc'                       => $foc,
                    'counter'                   => 1,
                    'is_exists'                 => 0,
                    'is_moq'                    => $is_moq,
                    'is_backlog'                => false,
                    'is_po_cancel'              => $is_po_cancel,
                    'is_sotf'                   => $is_sotf,
                    'flag_moq'                  => $flag_moq,
                    'qty_moq'                   => $qty_moq,
                    'uom_moq'                   => $uom_moq,
                    'prepared_status'           => $this->getNeedPrepareStatus($po_buyer,$item_code,$category,$uom,false,$qty_conversion),
                    'is_other_buyer_checkout'   => ($is_moq)? false : $this->getOthersIsCheckout($po_buyer,$item_code,$warehouse_id),
                    'movement_date'             => $movement_date,
                    'warehouse_place'           => $_warehouse_place,
                    'is_handover'               => $is_handover,
                    'is_subcont'                => $is_subcont,
                    'note'                      => $note,
                    'sewing_place'              => $sp 
                ];

                $array['details'] [] = [
                    'material_arrival_id'       => -1,
                    'po_buyer'                  => $po_buyer,
                    'barcode_supplier'          => $po_detail_id,
                    'qty_delivered'             => $qty_delivered,
                    'qty_upload'                => $qty_upload,
                ];
            }

        }


        return response()->json($array,200);
    }

    public function store(request $request)
    {
        $packing_list           = array();
        $material_stocks        = array();
        $barcode_arr            = array();
        $list_material_arrivals = array();
        $flag_no_subcont        = 0;
        $flag_insert_stock      = 0;
        $_warehouse_id          = $request->warehouse_id;

        $handover_location = Locator::with('area')
        ->whereHas('area',function ($query) use($_warehouse_id)
        {
            $query->where('is_destination',false);
            $query->where('is_active',true);
            $query->where('warehouse',$_warehouse_id);
            $query->where('name','HANDOVER');
        })
        ->first();

        $receiving_location     = Locator::with('area')
        ->whereHas('area',function ($query) use($_warehouse_id)
        {
            $query->where('is_destination',false);
            $query->where('is_active',true);
            $query->where('warehouse',$_warehouse_id);
            $query->where('name','RECEIVING');
        })
        ->first();


        $inventory_erp = Locator::with('area')
        ->where([
            ['is_active',true],
            ['rack','ERP INVENTORY'],
        ])
        ->whereHas('area',function ($query) use($_warehouse_id)
        {
            $query->where('is_active',true);
            $query->where('warehouse',$_warehouse_id);
            $query->where('name','ERP INVENTORY');
        })
        ->first();

        $validator = Validator::make($request->all(), [
            'material_arrival' => 'required|not_in:[]'
        ]);

        if ($validator->passes())
        {
            $barcodes       = json_decode($request->material_arrival);
            $concatenate    = '';

            try 
            {
                DB::beginTransaction();
                
                foreach ($barcodes as $key => $barcode_header) 
                {
                    $c_order_id             = trim(strtoupper($barcode_header->c_order_id));
                    $c_orderline_id         = trim(strtoupper($barcode_header->c_orderline_id));
                    $c_bpartner_id          = trim(strtoupper($barcode_header->c_bpartner_id));
                    $supplier_name          = trim(strtoupper($barcode_header->supplier_name));
                    $document_no            = trim(strtoupper($barcode_header->document_no));
                    $item_code              = trim(strtoupper($barcode_header->item_code));
                    $_item                  = Item::where('item_code',$item_code)->first();
                    $item_id                = ($barcode_header->item_id)? trim(strtoupper($barcode_header->item_id)) : $_item->item_id;
                    $item_desc              = trim(strtoupper($barcode_header->item_desc));
                    $category               = $barcode_header->category;
                    $no_packing_list        = $barcode_header->no_packing_list;
                    $uom                    = $barcode_header->uom;
                    $no_surat_jalan         = $barcode_header->no_surat_jalan;
                    $no_resi                = $barcode_header->no_resi;
                    $no_invoice             = $barcode_header->no_invoice;
                    $season                 = $barcode_header->season;
                    $po_buyer               = trim(str_replace('-S', '',$barcode_header->po_buyer));
                    $type_po                = $barcode_header->type_po;
                    $is_moq                 = (isset($barcode_header->is_moq)) ? $barcode_header->is_moq : false;
                    $is_backlog             = (isset($barcode_header->is_backlog)) ? $barcode_header->is_backlog : false;
                    $qty_carton             = $barcode_header->qty_carton;
                    $qty_upload             = $barcode_header->qty_upload;
                    $etd_date               = $barcode_header->etd_date;
                    $eta_date               = $barcode_header->eta_date;
                    $material_subcont_id    = $barcode_header->material_subcont_id;
                    $material_subcont       = MaterialSubcont::find($material_subcont_id);
                    $po_detail_id           = ($material_subcont ? ($material_subcont->po_detail_id ? $material_subcont->po_detail_id : $material_subcont->barcode) : $barcode_header->po_detail_id);
                    $barcode                = ($material_subcont ? $material_subcont->barcode : $po_detail_id );
                    $is_subcont             = ($material_subcont_id)? true:false;
                    $counter                = $barcode_header->counter;
                    $qty_conversion         = $barcode_header->qty_conversion;
                    $qty_entered            = $barcode_header->qty_entered;
                    $qty_ordered            = $barcode_header->qty_ordered;
                    $foc                    = $barcode_header->foc;
                    if($uom == 'YDS' or $uom == 'M' or $uom=='CNS')
                    {
                        $prepared_status        = 'NEED PREPARED';
                    }
                    else
                    {
                        $prepared_status        = $barcode_header->prepared_status;
                    }
                    $movement_date          = ($barcode_header->movement_date) ? $barcode_header->movement_date : Carbon::now()->toDateTimeString();
                    $barcode_details        = $barcode_header->details;
                    $warehouse_id           = $barcode_header->warehouse_id;
                    $barcode_header_id      = $barcode_header->id;
                    $uom_conversion         = $barcode_header->uom_conversion;
                    $packing_list[]         = $no_packing_list;
                    $barcode_arr[]          = $po_detail_id;
                    $concatenate            .= "'".$po_detail_id."',";
                    $material_arrival       = null;

                    if($counter == $qty_carton)
                    {
                        $is_ready_to_prepare = 'true';
                        if($material_subcont_id && $material_subcont)
                        {
                            $receive_on_destination         = carbon::now();
                            $material_subcont->is_active    = false;
                            $material_subcont->deleted_at   = $receive_on_destination;
                            $material_subcont->date_receive = $receive_on_destination;
                            $material_subcont->save();

                            if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                            {
                                $c_order_id             = 'FREE STOCK';
                                $no_packing_list        = '-';
                                $no_invoice             = '-';
                            }else
                            {
                                $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                ->whereNull('material_roll_handover_fabric_id')
                                ->whereNull('deleted_at')
                                ->where('po_detail_id',$po_detail_id)
                                ->first();
                                
                                $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                            }
                            
                            $is_movement_handover_exists = MaterialMovement::where([
                                ['from_location',$inventory_erp->id],
                                ['to_destination',$inventory_erp->id],
                                ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                ['to_locator_erp_id',$inventory_erp->area->erp_id],
                                ['po_buyer',$po_buyer],
                                ['is_integrate',false],
                                ['is_active',true],
                                ['no_packing_list',$no_packing_list],
                                ['no_invoice',$no_invoice],
                                ['status','integration-to-inventory-erp'],
                            ])
                            ->first();

                            if(!$is_movement_handover_exists)
                            {
                                $material_handover_movement = MaterialMovement::firstOrCreate([
                                    'from_location'         => $inventory_erp->id,
                                    'to_destination'        => $inventory_erp->id,
                                    'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                    'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                    'po_buyer'              => $po_buyer,
                                    'is_integrate'          => false,
                                    'is_active'             => true,
                                    'no_packing_list'       => $no_packing_list,
                                    'no_invoice'            => $no_invoice,
                                    'status'                => 'integration-to-inventory-erp',
                                    'created_at'            => $receive_on_destination,
                                    'updated_at'            => $receive_on_destination,
                                ]);
        
                                $material_handover_movement_id = $material_handover_movement->id;
                            }else
                            {
                                $is_movement_handover_exists->updated_at = $receive_on_destination;
                                $is_movement_handover_exists->save();
        
                                $material_handover_movement_id = $is_movement_handover_exists->id;
                            }

                            if($_warehouse_id == '1000013') $_whs_handover  = 'AOI 1';
                            else if($_warehouse_id == '1000002') $_whs_handover  = 'AOI 2';

                            $integration_note = 'BARANG PINDAH TANGAN DITERIMA DARI '.$_whs_handover;
                            

                            $is_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                            ->where([
                                ['material_movement_id',$material_handover_movement_id],
                                ['material_preparation_id',$material_subcont->materialMovementLine->material_preparation_id],
                                ['qty_movement',$material_subcont->qty_upload],
                                ['item_id',$item_id],
                                ['is_integrate',false],
                                ['is_active',true],
                                ['warehouse_id',$_warehouse_id],
                                ['date_movement',$receive_on_destination],
                            ])
                            ->exists();

                            if(!$is_line_exists)
                            {
                                MaterialMovementLine::Create([
                                    'material_movement_id'          => $material_handover_movement_id,
                                    'material_preparation_id'       => $material_subcont->materialMovementLine->material_preparation_id,
                                    'item_code'                     => $item_code,
                                    'item_id'                       => $item_id,
                                    'c_order_id'                    => $c_order_id,
                                    'c_orderline_id'                => $c_orderline_id,
                                    'c_bpartner_id'                 => $c_bpartner_id,
                                    'supplier_name'                 => $supplier_name,
                                    'type_po'                       => 2,
                                    'is_integrate'                  => false,
                                    'is_active'                     => true,
                                    'uom_movement'                  => $uom,
                                    'qty_movement'                  => $material_subcont->qty_upload,
                                    'date_movement'                 => $receive_on_destination,
                                    'created_at'                    => $receive_on_destination,
                                    'updated_at'                    => $receive_on_destination,
                                    'date_receive_on_destination'   => $receive_on_destination,
                                    'nomor_roll'                    => '-',
                                    'warehouse_id'                  => $_warehouse_id,
                                    'document_no'                   => $document_no,
                                    'note'                          => 'INTEGRASI PINDAH TANGAN, '.$integration_note,
                                    'is_active'                     => true,
                                    'user_id'                       => auth::user()->id,
                                ]);
                            }
                            
                            $material_movement_line_id      = $material_subcont->material_movement_line_id;

                            if($material_movement_line_id)
                            {
                                $update_material_movement_line                              = MaterialMovementLine::find($material_movement_line_id);
                                $update_material_movement_line->date_receive_on_destination = $receive_on_destination;
                                $update_material_movement_line->save();
                            }
                            
                        }

                    }else
                    {
                        $is_ready_to_prepare = 'false';
                    }

                    
                    $is_exists = MaterialArrival::where([
                        ['po_detail_id',$po_detail_id],
                        ['warehouse_id',$warehouse_id],
                        ['deleted_at', null]
                    ]);

                    if($material_subcont_id) $is_exists = $is_exists->where('material_subcont_id',$material_subcont_id);
                    
                    $is_exists = $is_exists->first();
                    
                    if($barcode_header_id == -1 && !$is_exists)
                    {
                        if($material_subcont_id)
                        {
                            // $copy_preparation   = MaterialPreparation::whereIn('id',function($query) use($material_subcont_id)
                            $is_exists   = MaterialPreparation::whereIn('id',function($query) use($material_subcont_id)  
                            {
                                $query->select('material_preparation_id')
                                ->from('material_movement_lines')
                                ->whereIn('id',function($query2) use ($material_subcont_id)
                                {
                                    $query2->select('material_movement_line_id')
                                    ->from('material_subconts')
                                    ->where('id',$material_subcont_id);
                                })
                                ->groupby('material_preparation_id');
                            })
                            ->first();
        
                            // $is_exists = MaterialPreparation::where([
                            //     'po_detail_id'          => $copy_preparation->po_detail_id,
                            //     'item_id'               => $copy_preparation->item_id,
                            //     'c_order_id'            => $copy_preparation->c_order_id,
                            //     'barcode'               => $copy_preparation->barcode,
                            //     'po_buyer'              => $copy_preparation->po_buyer,
                            //     'qty_conversion'        => $copy_preparation->qty_conversion,
                            //     'style'                 => $copy_preparation->style,
                            //     'article_no'            => $copy_preparation->article_no,
                            //     'warehouse'             => $warehouse_id,
                            //     'item_code'             => $copy_preparation->item_code,
                            //     'type_po'               => 2,
                            // ])
                            // ->first();
                            
                            if($is_exists)
                            {
                                $is_exists->last_status_movement        = 'receiving';
                                $is_exists->last_locator_id             = $receiving_location->id;
                                $is_exists->last_movement_date          = $movement_date;
                                $is_exists->deleted_at                  = null;
                                $is_exists->warehouse                   = $warehouse_id;
                                $is_exists->last_user_movement_id       = auth::user()->id;
                                $is_exists->save();
    
                                if($is_exists->po_detail_id == 'FREE STOCK' || $is_exists->po_detail_id == 'FREE STOCK-CELUP')
                                {
                                    $c_order_id             = 'FREE STOCK';
                                    $no_packing_list        = '-';
                                    $no_invoice             = '-';
                                    $c_orderline_receive_id  = '-';
                                }else
                                {
                                    $material_arrival       = MaterialArrival::where('po_detail_id',$is_exists->po_detail_id)->first();
                                    $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                    $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                    $c_orderline_receive_id = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                                }
                                
                                $is_movement_receive_exists = MaterialMovement::where([
                                    ['from_location',$handover_location->id],
                                    ['to_destination',$receiving_location->id],
                                    ['from_locator_erp_id',$handover_location->area->erp_id],
                                    ['to_locator_erp_id',$receiving_location->area->erp_id],
                                    ['po_buyer',$is_exists->po_buyer],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                    ['no_packing_list',$no_packing_list],
                                    ['no_invoice',$no_invoice],
                                    ['status','receive'],
                                ])
                                ->first();
                                
                                if(!$is_movement_receive_exists)
                                {
                                    $material_receive_movement = MaterialMovement::firstorcreate([
                                        'from_location'         => $handover_location->id,
                                        'to_destination'        => $receiving_location->id,
                                        'from_locator_erp_id'   => $handover_location->area->erp_id,
                                        'to_locator_erp_id'     => $receiving_location->area->erp_id,
                                        'is_integrate'          => false,
                                        'is_active'             => true,
                                        'po_buyer'              => $is_exists->po_buyer,
                                        'status'                => 'receive',
                                        'no_packing_list'       => $no_packing_list,
                                        'no_invoice'            => $no_invoice,
                                        'created_at'            => $movement_date,
                                        'updated_at'            => $movement_date,
        
                                    ]);
                                    $material_receive_movement_id = $material_receive_movement->id;
                                }else
                                {
                                    $is_movement_receive_exists->updated_at = $movement_date;
                                    $is_movement_receive_exists->save();
            
                                    $material_receive_movement_id = $is_movement_receive_exists->id;
                                }
                                
                                $is_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                ->where([
                                    ['material_movement_id',$material_receive_movement_id],
                                    ['material_preparation_id',$is_exists->id],
                                    ['item_id',$is_exists->item_id],
                                    ['warehouse_id',$is_exists->warehouse],
                                    ['is_active',true],
                                    ['is_integrate',false],
                                    ['date_movement',$movement_date],
                                    ['qty_movement',$is_exists->qty_conversion],
                                ])
                                ->exists();

                                if(!$is_line_exists)
                                {
                                    $material_movement_line_receive = MaterialMovementLine::firstorcreate([
                                        'material_movement_id'      => $material_receive_movement_id,
                                        'material_preparation_id'   => $is_exists->id,
                                        'item_id'                   => $is_exists->item_id,
                                        'item_code'                 => $is_exists->item_code,
                                        'type_po'                   => $is_exists->type_po,
                                        'c_order_id'                => $is_exists->c_order_id,
                                        'c_bpartner_id'             => $is_exists->c_bpartner_id,
                                        'supplier_name'             => $is_exists->supplier_name,
                                        'c_orderline_id'            => $c_orderline_id,
                                        'uom_movement'              => $is_exists->uom_conversion,
                                        'qty_movement'              => $is_exists->qty_conversion,
                                        'date_movement'             => $movement_date,
                                        'created_at'                => $movement_date,
                                        'updated_at'                => $movement_date,
                                        'is_active'                 => true,
                                        'is_integrate'              => false,
                                        'warehouse_id'              => $is_exists->warehouse,
                                        'document_no'               => $is_exists->document_no,
                                        'nomor_roll'                => '-',
                                        'user_id'                   => Auth::user()->id
                                    ]);
    
                                    $is_exists->last_material_movement_line_id  = $material_movement_line_receive->id;
                                    $is_exists->save();
                                }
                                
    
                                $temporary = Temporary::Create([
                                    'barcode'       => $is_exists->po_buyer,
                                    'status'        => 'mrp',
                                    'user_id'       => Auth::user()->id,
                                    'created_at'    => $movement_date,
                                    'updated_at'    => $movement_date,
                                ]);
                            }else
                            {
                                $replace_po_buyer = trim(str_replace('-S', '',$po_buyer));
                                $list_po_buyer    = explode(', ', $replace_po_buyer);
                                $po_buyer         = array_unique($list_po_buyer);
                                $po_buyer         = implode(',', $po_buyer);
                                $has_coma         = strpos($po_buyer, ",");
                            
                                $material_arrival = MaterialArrival::FirstOrCreate([
                                    'po_detail_id'          => $po_detail_id,
                                    'material_subcont_id'   => $material_subcont_id,
                                    'item_id'               => $item_id,
                                    'item_code'             => $item_code,
                                    'item_desc'             => $item_desc,
                                    'supplier_name'         => $supplier_name,
                                    'document_no'           => $document_no,
                                    'type_po'               => $type_po,
                                    'c_order_id'            => $c_order_id,
                                    'c_orderline_id'        => $c_orderline_id,
                                    'c_bpartner_id'         => $c_bpartner_id,
                                    'warehouse_id'          => $warehouse_id,
                                    'po_buyer'              => $po_buyer,
                                    'category'              => $category,
                                    'uom'                   => $uom,
                                    'no_packing_list'       => $no_packing_list,
                                    'no_resi'               => $no_resi,
                                    'no_surat_jalan'        => $no_surat_jalan,
                                    'no_invoice'            => $no_invoice,
                                    'etd_date'              => $etd_date,
                                    'eta_date'              => $eta_date,
                                    'qty_entered'           => $qty_entered,
                                    'qty_ordered'           => $qty_ordered,
                                    'qty_carton'            => $qty_carton,
                                    'qty_upload'            => $qty_upload,
                                    'foc_available_rma'     => $foc,
                                    'qty_available_rma'     => $qty_upload,
                                    'qty_reserved_rma'      => 0,
                                    'foc_reserved_rma'      => 0,
                                    'foc'                   => $foc,
                                    'is_subcont'            => $is_subcont,
                                    'prepared_status'       => $prepared_status,
                                    'is_ready_to_prepare'   => $is_ready_to_prepare,
                                    'is_active'             => true,
                                    'is_moq'                => $is_moq,
                                    'user_id'               => Auth::user()->id,
                                    'deleted_at'            => null
                                ]);
                                
                                $material_arrival_id = $material_arrival->id;

                                $material_stocks [] = $material_arrival_id;
                                $flag_insert_stock++;

                                foreach ($barcode_details as $key => $detail) 
                                {
                                    $detail_material_arrival_id     = $detail->material_arrival_id;
                                    $detail_barcode_supplier        = $detail->barcode_supplier;
                                    $detail_qty_delivered           = $detail->qty_delivered;
                                    $detail_qty_upload              = $detail->qty_upload;

                                    if($detail_material_arrival_id == -1)
                                    {
                                        $has_coma = strpos($po_buyer, ",");
                                        
                                        DetailMaterialArrival::firstOrCreate([
                                            'material_arrival_id'   => $material_arrival_id,
                                            'po_buyer'              => $po_buyer,
                                            'barcode_supplier'      => $detail_barcode_supplier,
                                            'qty_delivered'         => $detail_qty_delivered,
                                            'qty_upload'            => $detail_qty_upload,
                                            'user_id'               =>  Auth::user()->id,
                                            'is_active'             =>  true
                                        ]);
                                    }
                                }
                            }
                            
                        }else
                        {

                            $replace_po_buyer = trim(str_replace('-S', '',$po_buyer));
                            $list_po_buyer    = explode(', ', $replace_po_buyer);
                            $po_buyer         = array_unique($list_po_buyer);
                            $po_buyer         = implode(',', $po_buyer);
                            $has_coma = strpos($po_buyer, ",");
                            
                            $material_arrival = MaterialArrival::FirstOrCreate([
                                'po_detail_id'          => $po_detail_id,
                                'material_subcont_id'   => $material_subcont_id,
                                'item_id'               => $item_id,
                                'item_code'             => $item_code,
                                'item_desc'             => $item_desc,
                                'supplier_name'         => $supplier_name,
                                'document_no'           => $document_no,
                                'type_po'               => $type_po,
                                'c_order_id'            => $c_order_id,
                                'c_orderline_id'        => $c_orderline_id,
                                'c_bpartner_id'         => $c_bpartner_id,
                                'warehouse_id'          => $warehouse_id,
                                'po_buyer'              => $po_buyer,
                                'category'              => $category,
                                'uom'                   => $uom,
                                'no_packing_list'       => $no_packing_list,
                                'no_resi'               => $no_resi,
                                'no_surat_jalan'        => $no_surat_jalan,
                                'no_invoice'            => $no_invoice,
                                'etd_date'              => $etd_date,
                                'eta_date'              => $eta_date,
                                'season'                => $season,
                                'qty_entered'           => $qty_entered,
                                'qty_ordered'           => $qty_ordered,
                                'qty_carton'            => $qty_carton,
                                'qty_upload'            => $qty_upload,
                                'foc_available_rma'     => $foc,
                                'qty_available_rma'     => $qty_upload,
                                'qty_reserved_rma'      => 0,
                                'foc_reserved_rma'      => 0,
                                'foc'                   => $foc,
                                'is_subcont'            => $is_subcont,
                                'prepared_status'       => $prepared_status,
                                'is_ready_to_prepare'   => $is_ready_to_prepare,
                                'is_active'             => true,
                                'is_moq'                => $is_moq,
                                'user_id'               => Auth::user()->id,
                                'deleted_at'            => null
                            ]);
                            $material_arrival_id = $material_arrival->id;

                            $material_stocks [] = $material_arrival_id;
                            $flag_insert_stock++;

                            foreach ($barcode_details as $key => $detail) 
                            {
                                $detail_material_arrival_id     = $detail->material_arrival_id;
                                $detail_barcode_supplier        = $detail->barcode_supplier;
                                $detail_qty_delivered           = $detail->qty_delivered;
                                $detail_qty_upload              = $detail->qty_upload;

                                if($detail_material_arrival_id == -1)
                                {
                                    $has_coma = strpos($po_buyer, ",");
                                    
                                    DetailMaterialArrival::firstOrCreate([
                                        'material_arrival_id'   => $material_arrival_id,
                                        'po_buyer'              => $po_buyer,
                                        'barcode_supplier'      => $detail_barcode_supplier,
                                        'qty_delivered'         => $detail_qty_delivered,
                                        'qty_upload'            => $detail_qty_upload,
                                        'user_id'               =>  Auth::user()->id,
                                        'is_active'             =>  true
                                    ]);
                                }
                            }
                        }
                        
                    }else
                    {
                        if($material_subcont_id)
                        {
                            // $copy_preparation   = MaterialPreparation::whereIn('id',function($query) use($material_subcont_id) 
                            $is_exists   = MaterialPreparation::whereIn('id',function($query) use($material_subcont_id) 
                            {
                                $query->select('material_preparation_id')
                                ->from('material_movement_lines')
                                ->whereIn('id',function($query2) use ($material_subcont_id)
                                {
                                    $query2->select('material_movement_line_id')
                                    ->from('material_subconts')
                                    ->where('id',$material_subcont_id);
                                })
                                ->groupby('material_preparation_id');
                            })
                            ->first();
        
                            // $is_exists = MaterialPreparation::where([
                            //     'po_detail_id'          => $copy_preparation->po_detail_id,
                            //     'item_id'               => $copy_preparation->item_id,
                            //     'c_order_id'            => $copy_preparation->c_order_id,
                            //     'barcode'               => $copy_preparation->barcode,
                            //     'po_buyer'              => $copy_preparation->po_buyer,
                            //     'qty_conversion'        => $copy_preparation->qty_conversion,
                            //     'style'                 => $copy_preparation->style,
                            //     'article_no'            => $copy_preparation->article_no,
                            //     'warehouse'             => $warehouse_id,
                            //     'item_code'             => $copy_preparation->item_code,
                            //     'type_po'               => 2,
                            // ])
                            // ->first();
                            
                            if($is_exists)
                            {
                                $is_exists->last_status_movement        = 'receiving';
                                $is_exists->last_locator_id             = $receiving_location->id;
                                $is_exists->last_movement_date          = $movement_date;
                                $is_exists->deleted_at                  = null;
                                $is_exists->warehouse                   = $warehouse_id;
                                $is_exists->last_user_movement_id       = auth::user()->id;
                                $is_exists->save();

                                if($is_exists->po_detail_id == 'FREE STOCK' || $is_exists->po_detail_id == 'FREE STOCK-CELUP')
                                {
                                    $c_order_id             = 'FREE STOCK';
                                    $no_packing_list        = '-';
                                    $no_invoice             = '-';
                                    $c_orderline_receive_id  = '-';
                                }else
                                {
                                    $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                    ->whereNull('material_roll_handover_fabric_id')
                                    ->where('po_detail_id',$is_exists->po_detail_id)
                                    ->first();

                                    $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                    $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                    $c_orderline_receive_id = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                                }
                                
                                $is_movement_receive_exists = MaterialMovement::where([
                                    ['from_location',$handover_location->id],
                                    ['to_destination',$receiving_location->id],
                                    ['from_locator_erp_id',$handover_location->area->erp_id],
                                    ['to_locator_erp_id',$receiving_location->area->erp_id],
                                    ['po_buyer',$is_exists->po_buyer],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                    ['no_packing_list',$no_packing_list],
                                    ['no_invoice',$no_invoice],
                                    ['status','receive'],
                                ])
                                ->first();
                                
                                if(!$is_movement_receive_exists)
                                {
                                    $material_receive_movement = MaterialMovement::firstorcreate([
                                        'from_location'         => $handover_location->id,
                                        'to_destination'        => $receiving_location->id,
                                        'from_locator_erp_id'   => $handover_location->area->erp_id,
                                        'to_locator_erp_id'     => $receiving_location->area->erp_id,
                                        'is_integrate'          => false,
                                        'is_active'             => true,
                                        'po_buyer'              => $is_exists->po_buyer,
                                        'status'                => 'receive',
                                        'no_packing_list'       => $no_packing_list,
                                        'no_invoice'            => $no_invoice,
                                        'created_at'            => $movement_date,
                                        'updated_at'            => $movement_date,
        
                                    ]);
                                    $material_receive_movement_id = $material_receive_movement->id;
                                }else
                                {
                                    $is_movement_receive_exists->updated_at = $movement_date;
                                    $is_movement_receive_exists->save();
            
                                    $material_receive_movement_id = $is_movement_receive_exists->id;
                                }

                                $_is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                ->where([
                                    ['material_movement_id',$material_receive_movement_id],
                                    ['material_preparation_id',$is_exists->id],
                                    ['item_id',$is_exists->item_id],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                    ['warehouse_id',$is_exists->warehouse],
                                    ['qty_movement',$is_exists->qty_conversion],
                                    ['date_movement',$movement_date],
                                ])
                                ->exists();

                                if(!$_is_material_movement_line_exists)
                                {
                                    $material_movement_line_receive = MaterialMovementLine::firstorcreate([
                                        'material_movement_id'      => $material_receive_movement_id,
                                        'material_preparation_id'   => $is_exists->id,
                                        'item_id'                   => $is_exists->item_id,
                                        'item_code'                 => $is_exists->item_code,
                                        'type_po'                   => $is_exists->type_po,
                                        'c_order_id'                => $is_exists->c_order_id,
                                        'c_bpartner_id'             => $is_exists->c_bpartner_id,
                                        'supplier_name'             => $is_exists->supplier_name,
                                        'c_orderline_id'            => $c_orderline_id,
                                        'uom_movement'              => $is_exists->uom_conversion,
                                        'qty_movement'              => $is_exists->qty_conversion,
                                        'date_movement'             => $movement_date,
                                        'created_at'                => $movement_date,
                                        'updated_at'                => $movement_date,
                                        'is_active'                 => true,
                                        'is_integrate'              => false,
                                        'warehouse_id'              => $is_exists->warehouse,
                                        'document_no'               => $is_exists->document_no,
                                        'nomor_roll'                => '-',
                                        'user_id'                   => Auth::user()->id
                                    ]);
    
                                    $is_exists->last_material_movement_line_id  = $material_movement_line_receive->id;
                                    $is_exists->save();
                                    
                                }
                                
                                
                                $temporary = Temporary::Create([
                                    'barcode'       => $is_exists->po_buyer,
                                    'status'        => 'mrp',
                                    'user_id'       => Auth::user()->id,
                                    'created_at'    => $movement_date,
                                    'updated_at'    => $movement_date,
                                ]);
                            }
                        }else
                        {
                            $material_arrival_id            = $is_exists->id;
                            $is_exists->is_ready_to_prepare = $is_ready_to_prepare;
                            $is_exists->save();

                            foreach ($barcode_details as $key => $detail) 
                            {
                                $detail_material_arrival_id     = $detail->material_arrival_id;
                                $detail_barcode_supplier        = $detail->barcode_supplier;
                                $detail_qty_delivered           = $detail->qty_delivered;
                                $detail_qty_upload              = $detail->qty_upload;

                                if($detail_material_arrival_id == -1)
                                {
                                    $has_coma = strpos($po_buyer, ",");
                                    
                                    DetailMaterialArrival::firstOrCreate([
                                        'material_arrival_id'   => $material_arrival_id,
                                        'po_buyer'              => $po_buyer,
                                        'barcode_supplier'      => $detail_barcode_supplier,
                                        'qty_delivered'         => $detail_qty_delivered,
                                        'qty_upload'            => $detail_qty_upload,
                                        'user_id'               =>  Auth::user()->id,
                                        'is_active'             =>  true
                                    ]);
                                }
                            }
                        }
                        
                    }
                    if(!$material_subcont_id)
                    {
                        //tampung material_arrivals 
                        if($material_arrival != null)
                        {
                            $list_material_arrivals[] = $material_arrival->po_detail_id;

                            $total_carton_in = DetailMaterialArrival::where('material_arrival_id',$material_arrival_id)->count();
                            MaterialArrival::find($material_arrival_id)->update(['total_carton_in'=> $total_carton_in]);
                            $flag_no_subcont++;
                        }

                        // if($material_arrival != null)
                        // {
                        //     $po_detail_id = $material_arrival->po_detail_id;
                        //     //dd($po_detail_id );
                        //     $total_carton_in = DetailMaterialArrival::where('material_arrival_id',$material_arrival_id)->count();
                        //     MaterialArrival::find($material_arrival_id)->update(['total_carton_in'=> $total_carton_in]);

                        //     $app_env = Config::get('app.env');
                        //     if($app_env == 'live')
                        //     {
                        //         $po_detail = PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->first();
                        //         if(!$po_detail->user_warehouse_receive) PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true,'user_warehouse_receive' => Auth::user()->name]);
                        //         else PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true]);

                        //     }else if($app_env == 'dev')
                        //     {
                        //         $po_detail = PurchaseOrderDetailDev::where('po_detail_id',$po_detail_id)->first();

                        //         if(!$po_detail->user_warehouse_receive) PurchaseOrderDetailDev::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true,'user_warehouse_receive' => Auth::user()->name]);
                        //         else PurchaseOrderDetailDev::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true]);

                        //     }else
                        //     {
                        //         $po_detail = PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->first();

                        //         if(!$po_detail->user_warehouse_receive) PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true,'user_warehouse_receive' => Auth::user()->name]);
                        //         else PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true]);
                        //     }
                            
                        //     $flag_no_subcont++;
                        // }
                        // else
                        // {
                        //     return response()->json('Failed Save Barcode '.$barcode,422);
                        // }
                    }

                    Temporary::where([
                        ['status','material_arrival'],
                        ['barcode',$po_detail_id],
                    ])
                    ->delete();


                }
                
                if($concatenate != '')
                {
                    $concatenate = substr_replace($concatenate, '', -1);
                    DB::select(db::raw("SELECT *  FROM delete_duplicate_arrival(array[".$concatenate ."]);" ));
                }
                
                if($flag_no_subcont > 0)
                {
                    $this->getItemPackingList($packing_list);
                    $po_buyer = $this->getReadyPrepare($barcode_arr,$_warehouse_id);
                }
                
                if($flag_insert_stock > 0) $this->insertMaterialStock($material_stocks,$_warehouse_id);

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
            $this->updatePoDetail($list_material_arrivals);

            
        }else
        {
            return response()->json('silahkan scan barcode terlebih dahulu.',422);
        }

        return response()->json('success',200);
    }

    static function updatePoDetail($list_material_arrivals)
    {
        foreach ($list_material_arrivals as $key => $list_material_arrival)
        {
            if($list_material_arrival != null)
            {
                $po_detail_id = $list_material_arrival;
                //dd($po_detail_id );

                $app_env = Config::get('app.env');
                if($app_env == 'live')
                {
                    $po_detail = PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->first();
                    if(!$po_detail->user_warehouse_receive) PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true,'user_warehouse_receive' => Auth::user()->name]);
                    else PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true]);

                }else if($app_env == 'dev')
                {
                    $po_detail = PurchaseOrderDetailDev::where('po_detail_id',$po_detail_id)->first();

                    if(!$po_detail->user_warehouse_receive) PurchaseOrderDetailDev::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true,'user_warehouse_receive' => Auth::user()->name]);
                    else PurchaseOrderDetailDev::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true]);

                }else
                {
                    $po_detail = PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->first();

                    if(!$po_detail->user_warehouse_receive) PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true,'user_warehouse_receive' => Auth::user()->name]);
                    else PurchaseOrderDetail::where('po_detail_id',$po_detail_id)->update(['flag_wms' => true]);
                }
                
                //$flag_no_subcont++;
            }
        }
    }

    private function getMoq($po_detail,$c_order_id,$item_id,$document_no,$item_code,$po_buyer,$warehouse,$qty_upload,$category,$uom,$c_bpartner_id)
    {
      $has_many_po_buyer = strpos($po_buyer, ",");
      $flag_many_po = ($has_many_po_buyer) ? true : false;
      
      if(!$flag_many_po)
      {
        $is_po_buyer_cancel = PoBuyer::where('po_buyer',$po_buyer)
        ->whereNotNull('cancel_date')
        ->exists();

        if($is_po_buyer_cancel)  return false;
        else if($category == 'EL' || $category == 'TP' || $category == 'TH') return false;
        else if($c_bpartner_id == '1001054' || $c_bpartner_id == '1001164') return false;
        else if($category == 'LB')
        {
            $array = [
                '62752352-001A-00',
                '62752352.BT-001A-00',
                '62752352.TP-001A-00',
                '62002447-001A-00',
                '62002447.BT-001A-00',
                '62002447.TP-001A-00',
                '62550266-001A-00',
                '62550266.BT-001A-00',
                '62550266.TP-001A-00',
                '62752351-001A-00',
                '62752351.BT-001A-00',
                '62752351.TP-001A-00'
            ];
            if(!in_array($item_code, $array)) return false;
        }

        
        $check_qty_moq = $this->getQtyUploadAndNeed($po_buyer,$item_id,$qty_upload,$c_order_id,$c_bpartner_id,$uom,$po_detail,$warehouse);
        if($check_qty_moq)
        {
            $obj            = new StdClass;
            $obj->uom       = $check_qty_moq->uom;
            $obj->qty_book  = $check_qty_moq->qty_moq;
            $obj->flag_moq  = 'Pembelian item ini terkena MOQ, silahkan print barcode adjustment !';
            $obj->is_moq    = true;

            return $obj;
        }else{
            return false;
        }

        /*$is_moq = PurchaseItem::where([
            ['document_no',$document_no],
            ['item_code',$item_code],
            ['warehouse',$warehouse],
            ['c_bpartner_id',$c_bpartner_id],
            ['po_buyer',$po_buyer],
            ['is_moq',true],
        ])
        ->first();

        if($is_moq)
        {
            if($is_moq->_qty_moq <= $is_moq->qty_moq)
            {
                $conversion = UomConversion::where([
                    ['item_code',$item_code],
                    ['uom_to',$is_moq->uom],
                    ['uom_from',$uom]
                ])
                ->first();
            
                if($conversion){
                    $multiplyrate = $conversion->multiplyrate;
                    $dividerate = $conversion->dividerate;
                    $uom_conversion = $conversion->uom_to;
                }else{
                    $dividerate = 1;
                    $multiplyrate = 1;
                    $uom_conversion = $uom;
                }
                    
                $qty_conversion = $qty_upload * $multiplyrate;
    
                $curr_moq = $is_moq->_qty_moq;
                $new_moq = $curr_moq + $qty_conversion;
                $is_moq->_qty_moq = $new_moq;
                $is_moq->save();

                $obj = new StdClass;
                $obj->uom = $is_moq->uom;
                $obj->qty_book = $is_moq->qty_moq;
                $obj->flag_moq = 'Pembelian item ini terkena MOQ, silahkan print barcode adjustment !';
                $obj->is_moq = true;

                return $obj;
            }else{
                return false;
            }
            
        }else{
            return false;
        }*/

        
        
      }else{
            return false;
      }
    }

    private function insertMaterialStock($material_arrival_ids,$_warehouse_id)
    {
        $stock_accessories                              = array();
        $stock_reserved_accessories                     = array();
        $stock_reserved_handover_accessories            = array();
        
        $material_arrivals = MaterialArrival::select('id','po_detail_id','season','warehouse_id','item_id','material_subcont_id','po_buyer','supplier_name','document_no','c_bpartner_id','item_code','c_order_id','uom','qty_upload','qty_carton')
        ->whereIn('id',$material_arrival_ids)
        ->where('warehouse_id',$_warehouse_id)
        ->groupby('id','warehouse_id','document_no','po_detail_id','season','c_bpartner_id','item_id','item_code','material_subcont_id','po_buyer','supplier_name','c_order_id','uom','qty_upload','qty_carton')
        ->get();
        
        $free_stock_destination = Locator::with('area')
        ->whereHas('area',function ($query) use($_warehouse_id) 
        {
            $query->where([
                ['warehouse',$_warehouse_id],
                ['name','FREE STOCK'],
                ['is_destination',true]
            ]);

        })
        ->first();
        $movement_date = Carbon::now();

        try 
        {
            DB::beginTransaction();
            $upload_date                = Carbon::now()->toDateTimeString();

            foreach ($material_arrivals as $key => $value) 
            {
                $po_detail_id           = $value->po_detail_id; 
                $po_buyer               = $value->po_buyer; 
                $material_subcont_id    = $value->material_subcont_id; 
                $material_arrival_id    = $value->id; 
                $qty_upload             = $value->qty_upload; 
                $qty_carton             = $value->qty_carton; 
                $warehouse_id           = $value->warehouse_id; 
                $season                 = $value->season; 
                $document_no            = $value->document_no; 
                $c_bpartner_id          = $value->c_bpartner_id; 
                $item_code              = strtoupper($value->item_code); 
                $c_order_id             = $value->c_order_id; 
                $item_id                = $value->item_id; 
                $uom                    = $value->uom;
                $has_many_po_buyer      = strpos($po_buyer, ",");

                $is_running_stock       = false;
                $is_integrate           = false;

                if($material_subcont_id)
                {
                    $material_subcont       = MaterialSubcont::find($material_subcont_id);
                    $source                 = 'RESERVED HANDOVER STOCK';
                    $remark_detail_stock    = 'KEDATANGAN DARI PINDAH TANGAN';
                    $is_running_stock       = true;
                    $is_integrate           = true;
                    $po_detail_id           = ($material_subcont->po_detail_id ? $material_subcont->po_detail_id : $material_subcont->barcode ); 
                }else
                {
                    if($po_buyer != '' || $po_buyer != null)
                    {
                        $source                 = 'RESERVED STOCK';
                        $is_running_stock       = true;
                        $is_integrate           = true;
                    }else
                    {
                        $source                 = 'NON RESERVED STOCK';
                    }
                    $remark_detail_stock    = 'KEDATANGAN DARI SUPPLIER';
                }

                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();

                $is_pfao         = substr($document_no,0, 4); 
                $_item           = Item::where('item_id',$item_id)->first();
                $upc_item        = ($_item)? $_item->upc : 'MASTER ITEM TIDAK DITEMUKAN';
                $item_desc       = ($_item)? $_item->item_desc : 'MASTER ITEM TIDAK DITEMUKAN';
                $category        = ($_item)? $_item->category : 'MASTER ITEM TIDAK DITEMUKAN';
                $supplier        = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                
                if($c_bpartner_id == 'FREE STOCK')
                {
                    $supplier_code = 'FREE STOCK';
                    $supplier_name = 'FREE STOCK';
                }else
                {
                    $supplier_code   = ($supplier)? strtoupper($supplier->supplier_code) : 'MASTER SUPPLIER NOT FOUND';
                    $supplier_name   = ($supplier)? strtoupper($supplier->supplier_name) : $value->supplier_name;
                }
               
                if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
                {
                    $mapping_stock_id       = null;
                    $type_stock_erp_code    = '2';
                    $type_stock             = 'REGULER';
                }else
                {
                    $get_4_digit_from_beginning = substr($document_no,0, 4);
                    $_mapping_stock_4_digt = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                    if($_mapping_stock_4_digt)
                    {
                        $mapping_stock_id       = $_mapping_stock_4_digt->id;
                        $type_stock_erp_code    = $_mapping_stock_4_digt->type_stock_erp_code;
                        $type_stock             = $_mapping_stock_4_digt->type_stock;
                    }else
                    {
                        $get_5_digit_from_beginning = substr($document_no,0, 5);
                        $_mapping_stock_5_digt = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                        if($_mapping_stock_5_digt)
                        {
                            $mapping_stock_id       = $_mapping_stock_5_digt->id;
                            $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                            $type_stock             = $_mapping_stock_5_digt->type_stock;
                        }else
                        {
                            $mapping_stock_id       = null;
                            $type_stock_erp_code    = null;
                            $type_stock             = null;
                        }
                        
                    }
                }

                $conversion = UomConversion::where([
                    ['item_id',$item_id],
                    ['uom_from',$uom]
                ])
                ->first();
            
                if($conversion)
                {
                    $multiplyrate   = $conversion->multiplyrate;
                    $dividerate     = $conversion->dividerate;
                    $uom_conversion = $conversion->uom_to;
                }else
                {
                    $dividerate     = 1;
                    $multiplyrate   = 1;
                    $uom_conversion = $uom;
                }
                
                $qty_conversion = sprintf('%0.8f',$qty_upload*$dividerate);

                if(!$is_running_stock)
                {
                    $is_stock_already_created = MaterialStock::where([
                        ['c_order_id',$c_order_id],
                        ['item_id',$item_id],
                        ['warehouse_id',$warehouse_id],
                        ['is_stock_on_the_fly',true],
                        ['is_closing_balance', false],
                        ['deleted_at', null]
                    ])
                    ->whereNull('po_buyer')
                    ->exists();
                }else
                {
                    $is_stock_already_created = false;
                }
                
                $is_exists = MaterialStock::where([
                    ['po_detail_id',$po_detail_id],
                    ['c_order_id',$c_order_id],
                    ['item_id',$item_id],
                    ['type_po',2],
                    ['warehouse_id',$warehouse_id],
                    ['type_stock',$type_stock],
                    ['uom',$uom_conversion],
                    ['is_material_others',false],
                    ['is_closing_balance',false],
                    ['is_stock_on_the_fly',false],
                ])
                ->whereNull('deleted_at');

                if(!$is_running_stock) $is_exists = $is_exists->where('locator_id',$free_stock_destination->id);
                else $is_exists= $is_exists->whereNull('locator_id');

                if($material_subcont_id) $is_exists = $is_exists->where('remark',$material_subcont_id);
                
                $is_exists = $is_exists->whereNotNull('approval_date')
                ->whereNull('po_buyer')
                ->exists();

                if(!$is_exists)
                {
                    $material_stock = MaterialStock::FirstOrCreate([
                        'po_detail_id'              => $po_detail_id,
                        'locator_id'                => (!$is_running_stock ? $free_stock_destination->id : null),
                        'mapping_stock_id'          => $mapping_stock_id,
                        'type_stock_erp_code'       => $type_stock_erp_code,
                        'type_stock'                => $type_stock,
                        'document_no'               => $document_no,
                        'supplier_code'             => $supplier_code,
                        'supplier_name'             => $supplier_name,
                        'c_order_id'                => $c_order_id,
                        'c_bpartner_id'             => $c_bpartner_id,
                        'item_id'                   => $item_id,
                        'item_code'                 => $item_code,
                        'item_desc'                 => $item_desc,
                        'season'                    => $season,
                        'category'                  => $category,
                        'type_po'                   => 2,
                        'warehouse_id'              => $warehouse_id,
                        'uom'                       => $uom_conversion ,
                        'uom_source'                => $uom ,
                        'qty_carton'                => intval($qty_carton),
                        'qty_arrival'               => $qty_conversion,
                        'stock'                     => $qty_conversion,
                        'reserved_qty'              => 0,
                        'available_qty'             => $qty_conversion,
                        'is_general_item'           => true,
                        'is_material_others'        => false,
                        'is_active'                 => true,
                        'remark'                    => ($material_subcont_id  ? $material_subcont_id : null),
                        'is_running_stock'          => $is_running_stock,
                        'source'                    => $source,
                        'upc_item'                  => $upc_item,
                        'created_at'                => $movement_date,
                        'updated_at'                => $movement_date,
                        'approval_date'             => ($is_pfao == 'PFAO')? $movement_date : NULL,
                        'approval_user_id'          => ($is_pfao == 'PFAO')? $system->id : NULL,
                        'user_id'                   => Auth::user()->id,
                        'is_closing_balance'        => false,
                        'deleted_at'                => null
                    ]);

                    $material_stock_id  = $material_stock->id;
                    $locator_id         = $material_stock->locator_id;

                    if($is_running_stock)
                    {
                        $is_integrate = true;
                    }else
                    {
                        if($is_stock_already_created) $is_integrate = true;
                        else $is_integrate = false;
                    }
                    
                
                    HistoryStock::approved($material_stock_id
                    ,$qty_conversion
                    ,'0'
                    ,null
                    ,null
                    ,null
                    ,null
                    ,null
                    ,$remark_detail_stock
                    ,Auth::user()->name
                    ,Auth::user()->id
                    ,$type_stock_erp_code
                    ,$type_stock
                    ,$is_integrate
                    ,true
                    );

                    DetailMaterialStock::FirstOrCreate([
                        'material_stock_id'         => $material_stock_id,
                        'material_arrival_id'       => $material_arrival_id,
                        'remark'                    => $remark_detail_stock,
                        'uom'                       => $uom_conversion,
                        'qty'                       => $qty_conversion,
                        'user_id'                   => Auth::user()->id,
                        'mm_user_id'                => $system->id,
                        'accounting_user_id'        => $system->id,
                        'created_at'                => $upload_date,
                        'updated_at'                => $upload_date,
                        'approve_date_accounting'   => $upload_date,
                        'approve_date_mm'           => $upload_date,
                        'locator_id'                => $locator_id,
                    ]);

                    
                }else
                {
                    /*$locator_id         = $is_exists->locator_id;
                    $material_stock_id  = $is_exists->id;
                    $stock              = $is_exists->stock;
                    $reserved_qty       = $is_exists->reserved_qty;
                    $old_available_qty  = $is_exists->available_qty;
                    $new_stock          = $stock + $qty_conversion;
                    $availability_stock = $new_stock - $reserved_qty;

                    $is_exists->stock           = sprintf('%0.8f',$new_stock);
                    $is_exists->available_qty   = sprintf('%0.8f',$availability_stock);

                    if($availability_stock > 0)
                    {
                        $is_exists->is_allocated = false;
                        $is_exists->is_active = true;
                    }

                    if($is_running_stock)
                    {
                        $is_integrate = true;
                    }else
                    {
                        if($is_stock_already_created) $is_integrate = true;
                        else $is_integrate = false;
                    }

                    HistoryStock::approved($material_stock_id
                    ,$new_stock
                    ,$old_available_qty
                    ,null
                    ,null
                    ,null
                    ,null
                    ,null
                    ,null
                    ,Auth::user()->name
                    ,Auth::user()->id
                    ,$type_stock_erp_code
                    ,$type_stock
                    ,$is_integrate
                    ,true
                    );
                    
                    $is_exists->save();*/
                }

                if(!$is_running_stock) $stock_accessories [] = $material_stock_id;
                else 
                {
                    if(!$material_subcont_id)
                    {
                        $temporary = Temporary::Create([
                            'barcode'       => $material_stock_id,
                            'status'        => 'reserved_stock',
                            'warehouse'     => $warehouse_id,
                            'user_id'       => Auth::user()->id,
                            'created_at'    => $upload_date,
                            'updated_at'    => $upload_date,
                        ]);
                    } 
                    else
                    {
                        $temporary = Temporary::Create([
                            'barcode'       => $material_stock_id,
                            'status'        => 'reserved_handover_stock',
                            'warehouse'     => $warehouse_id,
                            'user_id'       => Auth::user()->id,
                            'created_at'    => $upload_date,
                            'updated_at'    => $upload_date,
                        ]);
                    }
                }
            }

            SOTF::updateStockArrival($stock_accessories);
            if(count($stock_accessories) > 0)
            {
                $material_stocks = MaterialStock::whereIn('id',$stock_accessories)->get();

                foreach ($material_stocks as $key => $material_stock )
                {
                    if(!$material_stock->last_status) ReportStock::doAllocation($material_stock, auth::user()->id, $movement_date);
                }
            }

            //AutoAllocation::insertAllocationFromReservedAccessories($stock_reserved_accessories);
            //$this->copyAllocationHandoverccessories($stock_reserved_handover_accessories);
            
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    
    static function getQtyUploadAndNeed($po_buyer,$item_id,$qty_upload,$c_order_id,$c_bpartner_id,$uom_source,$po_detail_id,$warehouse)
    {
        /*$material_requirement = MaterialRequirement::where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code],
        ])
        ->first();

        if($material_requirement)
        {
            $uom_need       = $material_requirement->uom;
            $qty_required   = sprintf('%0.8f',$material_requirement->qty_required);

            $conversion = UomConversion::where([
                ['item_code',$item_code],
                ['uom_to',$uom_need],
                ['uom_from',$uom]
            ])
            ->first();
        
            if($conversion)
            {
                $multiplyrate       = $conversion->multiplyrate;
                $dividerate         = $conversion->dividerate;
                $uom_conversion     = $conversion->uom_to;
            }else
            {
                $dividerate         = 1;
                $multiplyrate       = 1;
                $uom_conversion     = $uom;
            }
                
            $qty_upload         = $qty_upload * $multiplyrate;
            $qty_conversion     = $qty_required * $multiplyrate;
            $diff               = sprintf('%0.8f',$qty_upload - $qty_conversion);

            if($diff > 10)
            {
                $obj            = new stdClass();
                $obj->uom       = $uom_conversion;
                $obj->qty_moq   = $diff;
                return $obj;
            }
        }*/

        $allocation = AutoAllocation::where([
            ['c_order_id',$c_order_id],
            ['po_buyer',$po_buyer],
            ['item_id_source',$item_id],
            ['warehouse_id',$warehouse], 
        ])
        ->first();

        if($allocation)
        {
            $uom            = $allocation->uom;
            $qty_required   = sprintf('%0.8f',$allocation->qty_outstanding);

            $conversion = UomConversion::where([
                ['item_id',$item_id],
                ['uom_to',$uom],
                ['uom_from',$uom_source]
            ])
            ->first();
        
            if($conversion)
            {
                $multiplyrate   = $conversion->multiplyrate;
                $dividerate     = $conversion->dividerate;
                $uom_conversion = $conversion->uom_to;
            }else
            {
                $dividerate     = 1;
                $multiplyrate   = 1;
                $uom_conversion = $uom_source;
            }
             
            $qty_arrival_other = Temporary::where([
                ['status','material_arrival'],
                ['string_1',$po_buyer],
                ['string_2',$c_order_id],
                ['string_3',$c_bpartner_id],
                ['string_4',$item_id],
                ['barcode','!=',$po_detail_id],
            ])
            ->sum('double_1');

            $diff                   = sprintf('%0.8f',($qty_upload+$qty_arrival_other) - $qty_required);
            if($diff > 0)
            {
                if(($diff * $multiplyrate) > 10)
                {
                    $obj            = new stdClass();
                    $obj->uom       = $uom_conversion;
                    $obj->qty_moq   = $diff;
                    return $obj;
                }
            }
           
            
            
        }

        return false;
    }

    static function getNeedPrepareStatus($po_buyer,$item_code,$category,$uom,$is_subcont,$qty_rcv)
    {
        $replace_po_buyer = trim(str_replace('-S', '',$po_buyer));
        $list_po_buyer    = explode(', ', $replace_po_buyer);
        $po_buyer         = array_unique($list_po_buyer);
        $po_buyer         = implode(',', $po_buyer);
        $has_many_po_buyer = strpos($po_buyer, ",");
        if($uom == 'YDS' || $uom == 'M' || $uom=='CNS')
        {
            $flag_1        = true;
        }
        else
        {
            $flag_1 = ($has_many_po_buyer) ? true : false;
        }

        if($is_subcont == true) return 'NO NEED PREPARED';
        else if($flag_1 == true) return 'NEED PREPARED';
        else if($po_buyer == '' || $po_buyer == null) return 'NEED PREPARED ALLOCATION';

        if($flag_1 == false)
        {
            $material_requirements = MaterialRequirement::select('po_buyer','item_code','style','article_no')
            ->where([
                ['po_buyer',$po_buyer],
                ['item_code',$item_code]
            ])
            ->groupby('po_buyer','item_code','style','article_no')
            ->get();

            $flag = 0;
            foreach ($material_requirements as $key => $value) 
            {
                $flag++;
            }

            if($flag > 1) return 'NEED PREPARED TOP & BOTTOM';
            else return 'NO NEED PREPARED';
        }
    }

    static function getOthersIsCheckout($po_buyer,$item_code,$_warehouse_id)
    {
        $has_many_po_buyer = strpos($po_buyer, ",");
        $flag_1 = ($has_many_po_buyer) ? true : false;

        if($flag_1 == true || ($po_buyer == '' || $po_buyer == null)) return false;

        if($flag_1 == false)
        {
            $_is_exist_prepare = MaterialPreparation::where([
                ['po_buyer',$po_buyer],
                ['warehouse',$_warehouse_id]
            ]);
            if($_warehouse_id=='1000013')
            {
                $_is_exist_prepare = $_is_exist_prepare->where(function($query)
                {
                    $query->where('last_status_movement','out');
                })
                ->whereIn('last_locator_id',function($query){
                    $query->select('id')
                    ->from('locators')
                    ->where([
                        ['rack','LINE'],
                        ['warehouse','1000013']
                    ]);
                });
            }else
            {
                $_is_exist_prepare = $_is_exist_prepare->where(function($query)
                {
                    $query->where('last_status_movement','out')
                    ->orWhere('last_status_movement','out-handover');
                });
            }
    
            $_is_exist_prepare = $_is_exist_prepare->exists();
    
            /*$is_exists = MaterialPreparation::where([
                ['po_buyer',$po_buyer],
                ['warehouse',auth::user()->warehouse],
            ])
            ->where(function($q){
              $q->Where('last_status_movement','out')
              ->orWhere('last_status_movement','out-handover');
            })
            ->exists();*/

            return $_is_exist_prepare;
        }
    }

    static function getReadyPrepare($barcode_arr,$_warehouse_id)
    {
        $_po_buyer = array();
        $material_arrival = MaterialArrival::whereIn('po_detail_id',$barcode_arr)
        ->where('warehouse_id',$_warehouse_id)
        ->get();

        try 
        {
            DB::beginTransaction();
            
            foreach ($material_arrival as $key => $value)
            {
                $po_buyer = $value->po_buyer;

                if($po_buyer != '' || $po_buyer != null)
                {
                    $has_many_po_buyer = strpos($po_buyer, ",");
                    if($has_many_po_buyer == false)
                    {
                        $reroute        = ReroutePoBuyer::where('old_po_buyer',trim($po_buyer))->first();
                        $new_po_buyer   = ($reroute ? $reroute->new_po_buyer : trim($po_buyer));
                        
                        // hanya satu po buyer
                        $is_exists = MaterialReadyPreparation::where([
                            ['material_arrival_id',$value->id],
                            ['item_code',$value->item_code],
                            ['category',$value->category],
                            ['document_no',$value->document_no],
                            ['c_order_id',($value->c_order_id)? $value->c_order_id : null],
                            ['c_orderline_id',($value->c_orderline_id)? $value->c_orderline_id : null],
                            ['c_bpartner_id',$value->c_bpartner_id],
                            ['type_po',$value->type_po],
                            ['po_buyer',$new_po_buyer]
                        ])
                        ->exists();

                        if(!$is_exists)
                        {
                            MaterialReadyPreparation::FirstOrCreate([
                                'material_arrival_id'   => $value->id,
                                'c_order_id'            => ($value->c_order_id)? $value->c_order_id : null,
                                'c_orderline_id'        => ($value->c_orderline_id)? $value->c_orderline_id : null,
                                'c_bpartner_id'         => $value->c_bpartner_id,
                                'supplier_name'         => $value->supplier_name,
                                'item_id'               => $value->item_id,
                                'item_code'             => $value->item_code,
                                'item_desc'             => $value->item_desc,
                                'category'              => $value->category,
                                'po_buyer'              => $new_po_buyer,
                                'document_no'           => $value->document_no,
                                'type_po'               => $value->type_po,
                                'user_id'               => $value->user_id
                            ]);
                            $_po_buyer[] = trim($new_po_buyer);
                        }

                    }else
                    {
                        // banyak po buyer
                        $split = explode(',',$po_buyer);
                        foreach ($split as $key_po_buyer => $split_po_buyer) 
                        {
                            $reroute        = ReroutePoBuyer::where('old_po_buyer',trim($split_po_buyer))->first();
                            $new_po_buyer   = ($reroute ? $reroute->new_po_buyer : trim($split_po_buyer));

                            $is_exists = MaterialReadyPreparation::where([
                                ['material_arrival_id',$value->id],
                                ['item_code',$value->item_code],
                                ['category',$value->category],
                                ['document_no',$value->document_no],
                                ['c_order_id',$value->c_order_id],
                                ['c_orderline_id',$value->c_orderline_id],
                                ['c_bpartner_id',$value->c_bpartner_id],
                                ['type_po',$value->type_po],
                                ['po_buyer',$new_po_buyer]
                            ])
                            ->exists();

                            if(!$is_exists)
                            {
                                MaterialReadyPreparation::FirstOrCreate([
                                    'material_arrival_id'   => $value->id,
                                    'c_order_id'            => $value->c_order_id,
                                    'c_orderline_id'        => $value->c_orderline_id,
                                    'c_bpartner_id'         => $value->c_bpartner_id,
                                    'supplier_name'         => $value->supplier_name,
                                    'item_id'               => $value->item_id,
                                    'item_code'             => $value->item_code,
                                    'item_desc'             => $value->item_desc,
                                    'category'              => $value->category,
                                    'po_buyer'              => trim($new_po_buyer),
                                    'document_no'           => $value->document_no,
                                    'type_po'               => $value->type_po,
                                    'user_id'               => Auth::user()->id
                                ]);

                                $_po_buyer[] = trim($new_po_buyer);
                            }
                        }
                    }
                }
            }

            DB::commit();
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }


        return $_po_buyer;
    }

    static function getItemPackingList($packing_lists)
    {
        $data = DB::connection('web_po') //web_po //dev_web_po
        ->table('adt_item_pl')
        ->select('no_packinglist as no_packing_list',
                'kst_invoicevendor as no_invoice',
                'kst_resi as no_resi',
                'c_bpartner_id',
                'ttl_item as total_item',
                'type_po'
                )
        ->whereIn('no_packinglist',$packing_lists)
        ->get();

        try 
        {
            DB::beginTransaction();
            
            foreach ($data as $key => $value) 
            {
                $no_packing_list    = $value->no_packing_list;
                $no_resi            = $value->no_resi;
                $no_invoice         = $value->no_invoice;
                $c_bpartner_id      = $value->c_bpartner_id;
                
                $is_exists = ItemPackingList::where([
                    ['no_packing_list',$no_packing_list],
                    ['no_resi',$no_resi],
                    ['no_invoice',$no_invoice],
                    ['c_bpartner_id',$c_bpartner_id],
                    ['type_po',2]
                ])
                ->first();
                
                $count_packinglist = MaterialArrival::whereNotNull('no_packing_list')
                ->whereNotNull('no_resi')
                ->whereNotNull('no_invoice')
                ->whereNotNull('c_bpartner_id')
                ->whereNull('material_subcont_id')
                ->whereNull('material_roll_handover_fabric_id')
                ->where([
                    ['no_packing_list',$no_packing_list],
                    ['no_resi',$no_resi],
                    ['no_invoice',$no_invoice],
                    ['c_bpartner_id',$c_bpartner_id],
                    ['type_po',2]
                ])
                ->count();

                if($is_exists)
                {
                    $is_exists->total_item          = $value->total_item;
                    $is_exists->total_item_receive  = $count_packinglist;
                    $is_exists->save();
                }else
                {
                    ItemPackingList::FirstOrCreate([
                        'no_packing_list'           => $value->no_packing_list,
                        'no_resi'                   => $value->no_resi,
                        'no_invoice'                => $value->no_invoice,
                        'c_bpartner_id'             => $value->c_bpartner_id,
                        'total_item'                => $value->total_item,
                        'total_item_receive'        => $count_packinglist,
                        'type_po'                   => 2
                    ]);
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }
}
