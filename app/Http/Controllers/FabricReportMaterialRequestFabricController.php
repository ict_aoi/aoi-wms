<?php
namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class FabricReportMaterialRequestFabricController extends Controller
{
    public function index()
    {
        return view('fabric_report_material_request_fabric.index');
    }
}
