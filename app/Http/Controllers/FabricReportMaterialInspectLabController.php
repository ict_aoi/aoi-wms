<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Config;
use Excel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Temporary;
use App\Models\MaterialStock;
use App\Models\MaterialStockPerLot;
use App\Models\DetailMaterialPreparationFabric;

use App\Http\Controllers\FabricMaterialStockController as HistoryStockPerLot;

class FabricReportMaterialInspectLabController extends Controller
{
    public function index(Request $request)
    {
        return view('fabric_report_material_inspect_lab.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id       = $request->warehouse;
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
            $status                 = $request->status;
            
            if($status == '')
            {
              $material_inspect_lab = DB::select(db::raw("SELECT * FROM get_report_inspect_lab(
                 '".$warehouse_id."', '".$start_date."', '".$end_date."');"
                ));
              // $material_inspect_lab   = db::table('inspect_lab_report_v')
              // ->whereBetween('receive_date',[$start_date,$end_date])
              // ->where('warehouse_id','LIKE',"%$warehouse_id%")
              // ->orderby('receive_date','desc')
              // ->take(50);
            }
            elseif($status == 'hold')
            {
              $material_inspect_lab = DB::select(db::raw("SELECT * FROM get_report_inspect_lab(
                '".$warehouse_id."', '".$start_date."', '".$end_date."') where is_hold = true ;"
               ));
              // $material_inspect_lab   = db::table('inspect_lab_report_v')
              // ->whereBetween('receive_date',[$start_date,$end_date])
              // ->where('is_hold', true)
              // ->where('warehouse_id','LIKE',"%$warehouse_id%")
              // ->orderby('receive_date','desc')
              // ->take(50);
            }
            
            return DataTables::of($material_inspect_lab)
            ->editColumn('receive_date',function ($material_inspect_lab)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $material_inspect_lab->receive_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('warehouse_id',function ($material_inspect_lab)
            {
                if($material_inspect_lab->warehouse_id == '1000001') return 'Warehouse fabric AOI 1';
                elseif($material_inspect_lab->warehouse_id == '1000011') return 'Warehouse fabric AOI 2';
            })
            ->editColumn('is_handover',function ($material_inspect_lab)
            {
              if ($material_inspect_lab->is_handover) return '<div class="checker border-info-600 text-info-800">
              <span class="checked"><input type="checkbox" class="control-info" checked="checked"></span>
              </div>';
              else return null;
            })
            ->addColumn('action',function($material_inspect_lab){
                    return view('fabric_report_material_inspect_lab._action',[
                    'model' => $material_inspect_lab,
                    'detail' => route('fabricReportMaterialInspectLab.detail',$material_inspect_lab->id),
                    'export' => route('fabricReportMaterialInspectLab.exportDetail',$material_inspect_lab->id)
                ]);
            })
            ->setRowAttr([
                'style' => function($material_inspect_lab) 
                {
                    if($material_inspect_lab->total_inspect_lab_roll >= $material_inspect_lab->total_roll && $material_inspect_lab->is_hold == false)
                    {
                        return  'background-color: #d9ffde;';
                    }
                    elseif($material_inspect_lab->is_hold == true)
                    {
                      return  'background-color: #FFB74D;';
                    }
                },
            ])
            ->rawColumns(['action','style', 'is_handover'])
            ->make(true);
        }
    }

    public function dataDetail(Request $request,$id)
    {
        if(request()->ajax()) 
        {
            $monitoring_receving_fabric_id  = $id;
          
            $detail_stocks   = MaterialStock::where('monitoring_receiving_fabric_id',$monitoring_receving_fabric_id)
            ->where('is_master_roll',true)
            ->orderby('batch_number','asc')
            ->orderby('nomor_roll','asc');

            return DataTables::of($detail_stocks)
            ->editColumn('inspect_lab_date',function ($detail_stocks)
            {
                if($detail_stocks->inspect_lab_date) return  $detail_stocks->inspect_lab_date->format('d/M/Y H:i:s');
                else return null;
            })
            ->editColumn('user_lab_id',function ($detail_stocks)
            {
                if($detail_stocks->user_lab_id) return strtoupper($detail_stocks->userInspectLab->name);
                else return null;
            })
            ->editColumn('load_actual',function ($detail_stocks)
            {
                if($detail_stocks->load_actual)
                {
                  if($detail_stocks->inspect_lot_result == 'hold' || $detail_stocks->inspect_lot_result == 'HOLD') return '<input type="hidden" class="form-control" value="'.$detail_stocks->load_actual.'" id="hidden_load_'.$detail_stocks->id.'"> <input type="text" class="form-control" onChange="changelot(\''.$detail_stocks->id.'\')" value="'.$detail_stocks->load_actual.'" id="load_'.$detail_stocks->id.'">';
                  else return '<input type="hidden" class="form-control" value="'.$detail_stocks->load_actual.'" id="hidden_load_'.$detail_stocks->id.'"> <input type="text" class="form-control" readonly="readonly" value="'.$detail_stocks->load_actual.'" id="load_'.$detail_stocks->id.'">';
                } else return null;
            })
            ->editColumn('inspect_lab_remark',function ($detail_stocks)
            {
                if($detail_stocks->inspect_lab_remark) return '<input type="hidden" class="form-control" value="'.$detail_stocks->inspect_lab_remark.'" id="hidden_remark_'.$detail_stocks->id.'"> <input type="text" class="form-control" onChange="changelot(\''.$detail_stocks->id.'\')" value="'.$detail_stocks->inspect_lab_remark.'" id="remark_'.$detail_stocks->id.'">';
                else return '<input type="hidden" class="form-control" value="'.$detail_stocks->inspect_lab_remark.'" id="hidden_remark_'.$detail_stocks->id.'"> <input type="text" class="form-control" onChange="changelot(\''.$detail_stocks->id.'\')" value="'.$detail_stocks->inspect_lab_remark.'" id="remark_'.$detail_stocks->id.'">';
            })
            ->editColumn('inspect_lot_result',function ($detail_stocks)
            {
                if($detail_stocks->load_actual)
                {
                  return view('fabric_report_material_inspect_lab._action',[
                    'model'            => $detail_stocks,
                    'action_view'      => 'final_result'
                  ]);
                }else return null;
            })
            ->editColumn('locator_id',function ($detail_stocks)
            {
                if($detail_stocks->locator_id) return  strtoupper($detail_stocks->locator->code);
                else return null;
            })
            ->setRowAttr([
                'style' => function($detail_stocks) 
                {
                    if($detail_stocks->load_actual)
                    {
                        return  'background-color: #d9ffde;';
                    }
                },
            ])
            ->rawColumns(['style','load_actual','inspect_lot_result', 'inspect_lab_remark'])
            ->make(true);
        }
    }

    public function exportSummary(Request $request)
    {
      $warehouse  = ($request->warehouse) ? $request->warehouse : auth::user()->warehouse;
      if($warehouse == '1000011') $filename = 'report_inspect_lab_fab_header_aoi2.csv';
      else if($warehouse == '1000001') $filename = 'report_inspect_lab_fab_header_aoi1.csv';
    
        $file = Config::get('storage.report') . '/' . $filename;
    
        if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';
        
        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
        // $warehouse  = ($request->warehouse) ? $request->warehouse : auth::user()->warehouse();
        // $start_date = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date." 00:00:00") : Carbon::now()->subDays(30);
        // $end_date   = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date." 23:59:59") : Carbon::now();
        // $data       = db::table('inspect_lab_report_v')
        // ->whereBetween('receive_date',[$start_date,$end_date])
        // ->where('warehouse_id','LIKE',"%$warehouse%")
        // ->get();

        // return Excel::create('report_summary_inspect_lab',function($excel) use($data){
        //   $excel->sheet('active',function($sheet) use ($data){
        //     $sheet->setWidth(array(
        //       'A'=>5,
        //       'B'=>20,
        //       'B'=>23,
        //       'C'=>27,
        //       'D'=>18,
        //       'E'=>30,
        //       'F'=>21,
        //       'G'=>20,
        //     ));
        //     $sheet->setHeight(array(
        //       '1'=>20,
        //     ));
        //     $sheet->setCellValue('A1','No.');
        //     $sheet->setCellValue('B1','TANGGAL TERIMA BARANG');
        //     $sheet->setCellValue('C1','NO INVOICE');
        //     $sheet->setCellValue('D1','NO PO SUPPLIER');
        //     $sheet->setCellValue('E1','NAMA SUPLILER');
        //     $sheet->setCellValue('F1','KODE ITEM');
        //     $sheet->setCellValue('G1','STYLE');
        //     $sheet->setCellValue('H1','WARNA');
        //     $sheet->setCellValue('I1','TOTAL ROLL');
        //     $sheet->setCellValue('J1','TOTAL YARD');
        //     $sheet->setCellValue('K1','TOTAL ROLL INSPECT');
        //     $sheet->setCellValue('L1','TOTAL BATCH NUMBER');
        //     $sheet->setCellValue('M1','TOTAL LOT ACTUAL');
        //     $sheet->setCellValue('N1','BATCH NUMBER');
        //     $sheet->setCellValue('O1','LOT ACTUAL');
    
        //     $row=2;
        //     foreach ($data as $key => $i)
        //     {
        //         $sheet->setHeight(array(
        //             $row    =>  20,
        //         ));
        //         $sheet->cell('A'.$row,function($cell){
        //             $cell->setAlignment('center');
        //         });
        //         $sheet->cell('I'.$row,function($cell){
        //             $cell->setAlignment('center');
        //         });
        //         $sheet->cell('J'.$row,function($cell){
        //             $cell->setAlignment('center');
        //         });
                
        //         $sheet->setCellValue('A'.$row,$key+1);
        //         $sheet->setCellValue('B'.$row,Carbon::parse($i->receive_date)->format('d/M/Y h:i:s'));
        //         $sheet->setCellValue('C'.$row,'\''.$i->no_invoice);
        //         $sheet->setCellValue('D'.$row,$i->document_no);
        //         $sheet->setCellValue('E'.$row,$i->supplier_name);
        //         $sheet->setCellValue('F'.$row,$i->item_code);
        //         $sheet->setCellValue('G'.$row,$i->style);
        //         $sheet->setCellValue('H'.$row,$i->color);
        //         $sheet->setCellValue('I'.$row,$i->total_roll);
        //         $sheet->setCellValue('J'.$row,$i->total_yard);
        //         $sheet->setCellValue('K'.$row,$i->total_inspect_lab_roll);
        //         $sheet->setCellValue('L'.$row,$i->jumlah_batch_number);
        //         $sheet->setCellValue('M'.$row,$i->jumlah_lot_actual);
        //         $sheet->setCellValue('N'.$row,$i->batch_number);
        //         $sheet->setCellValue('O'.$row,$i->lot_actual);
        //         $row++;
        //     }
        //   });
        // })->export('xlsx');

    }

    public function detail($id)
    {
        $summary = db::table('inspect_lab_report_v')->where('id',$id)->first();
        $data_total_roll = MaterialStock::select('uom',db::raw('count(0) as total_roll'),db::raw('sum(qty_arrival) as qty_arrival'))
        ->where([
          ['monitoring_receiving_fabric_id',$id],
          ['is_master_roll',true],
        ])
        ->groupby('uom')
        ->first();

        $data_inspect_roll = MaterialStock::select('uom',db::raw('count(0) as total_roll'),db::raw('sum(qty_arrival) as qty_arrival'))
        ->where([
          ['monitoring_receiving_fabric_id',$id],
          ['is_master_roll',true],
        ])
        ->whereNotNull('inspect_lab_date')
        ->groupby('uom')
        ->first();
        
        return view('fabric_report_material_inspect_lab.detail',compact('summary','data_total_roll','data_inspect_roll'));
    }

    public function exportDetail(Request $request,$id)
    {
        $summary            = db::table('inspect_lab_report_v')->where('id',$id)->first();
        $data_total_roll    = MaterialStock::select('uom',db::raw('count(0) as total_roll'),db::raw('sum(qty_arrival) as qty_arrival'))
        ->where([
          ['monitoring_receiving_fabric_id',$id],
          ['is_master_roll',true],
        ])
        ->groupby('uom')
        ->first();

        $data_inspect_roll  = MaterialStock::select('uom',db::raw('count(0) as total_roll'),db::raw('sum(qty_arrival) as qty_arrival'))
        ->where([
          ['monitoring_receiving_fabric_id',$id],
          ['is_master_roll',true],
        ])
        ->whereNotNull('inspect_lab_date')
        ->groupby('uom')
        ->first();
        
        $data               = MaterialStock::where([
          ['monitoring_receiving_fabric_id',$id],
          ['is_master_roll',true],
        ])
        ->orderby('load_actual','asc')
        ->orderby('batch_number','asc')
        ->orderby('nomor_roll','asc')
        ->get();

        return Excel::create('SHADE_BAND_AND_BLANKET_TEST',function($excel) use($summary,$data_total_roll,$data_inspect_roll,$data)
        {
            $excel->sheet('active',function($sheet) use($summary,$data_total_roll,$data_inspect_roll,$data)
            {
              $sheet->setWidth(array(
                'A'=>10,
                'B'=>15,
                'C'=>10,
                'D'=>10,
                'E'=>10,
                'F'=>10,
                'G'=>20,
              ));
      
              $sheet->mergeCells('A1:H3');
              $sheet->cell('A1:H3',function($cell)
              {
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '25',
                  'bold'     => true
                ));
                $cell->setAlignment('center');
                $cell->setBorder('thin','thin','thin','thin');
              });
      
              $sheet->mergeCells('A4:H4');
              $sheet->cell('A4:H4',function($cell)
              {
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '11',
                  'bold'     => true
                ));
                $cell->setBackground('#bdc3c7');
                $cell->setFontColor('#ffffff');
                $cell->setAlignment('center');
                $cell->setBorder('thin','thin','thin','thin');
              });
              $sheet->setCellValue('A1','adidas');
              $sheet->setCellValue('A4','SHADE BAND and BLANKET TEST');
      
              //Item
              $sheet->mergeCells('A6:B6');
              $sheet->cell('A6:B6',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                ));
                $cell->setAlignment('left');
              });
              $sheet->mergeCells('C6:D6');
              $sheet->cell('C6:D6',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                ));
                $cell->setAlignment('left');
              });
      
              //POBB
              $sheet->mergeCells('A7:B7');
              $sheet->cell('A7:B7',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                ));
                $cell->setAlignment('left');
              });
      
              $sheet->mergeCells('C7:D7');
              $sheet->cell('C7:D7',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                ));
                $cell->setAlignment('left');
              });
      
              //Style
              $sheet->mergeCells('A8:B8');
              $sheet->cell('A8:B8',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                ));
                $cell->setAlignment('left');
              });
      
              $sheet->mergeCells('C8:G8');
              $sheet->cell('C8:G8',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                ));
                $cell->setAlignment('left');
              });
      
              //NO INVOICE
              $sheet->mergeCells('A9:B9');
              $sheet->cell('A9:B9',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                ));
                $cell->setAlignment('left');
              });
      
              $sheet->mergeCells('C9:D9');
              $sheet->cell('C9:D9',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                ));
                $cell->setAlignment('left');
              });
      
              //Color
              $sheet->mergeCells('A10:B10');
              $sheet->cell('A10:B10',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                ));
                $cell->setAlignment('left');
              });
      
              $sheet->mergeCells('C10:D10');
              $sheet->cell('C10:D10',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                ));
                $cell->setAlignment('left');
              });
      
              //Supplier
              $sheet->mergeCells('A11:B11');
              $sheet->cell('A11:B11',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                ));
                $cell->setAlignment('left');
              });
              $sheet->mergeCells('C11:G11');
              $sheet->cell('C11:G11',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                ));
                $cell->setAlignment('left');
              });
      
              //Date Receivd
              $sheet->mergeCells('A12:B12');
              $sheet->cell('A12:B12',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                ));
                $cell->setAlignment('left');
              });
              $sheet->cell('C12',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                ));
                $cell->setAlignment('left');
              });
      
              //Roll
              $sheet->mergeCells('D12:E12');
              $sheet->cell('D12:E12',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                ));
                $cell->setAlignment('center');
              });
      
              $sheet->mergeCells('D13:E13');
              $sheet->cell('D13:E13',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                ));
                $cell->setAlignment('center');
              });
      
              //YARD
              $sheet->mergeCells('F12:G12');
              $sheet->cell('F12:G12',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                ));
                $cell->setAlignment('center');
              });
      
              $sheet->mergeCells('F13:G13');
              $sheet->cell('F13:G13',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                ));
                $cell->setAlignment('center');
              });
      
      
              //Header Data
              $sheet->setCellValue('A1','adidas');
              $sheet->setCellValue('A4','SHADE BAND and BLANKET TEST');
      
              $sheet->setCellValue('A6','Item');
              $sheet->setCellValue('A7','POBB');
              $sheet->setCellValue('A8','Style');
              $sheet->setCellValue('A9','No Invoice');
              $sheet->setCellValue('A10','Color');
              $sheet->setCellValue('A11','Supplier');
              $sheet->setCellValue('A12','Date recvd warehouse');
      
              $sheet->setCellValue('C6',': '.$summary->item_code);
              $sheet->setCellValue('C7',': '.$summary->document_no);
              $sheet->setCellValue('C8',': '.$summary->style);
              $sheet->setCellValue('C9',': '.$summary->no_invoice);
              $sheet->setCellValue('C10',': '.$summary->color);
              $sheet->setCellValue('C11',': '.$summary->supplier_name);
              $sheet->setCellValue('C12',': '.Carbon::parse($summary->receive_date)->format('d-M-y'));
      
              $sheet->setCellValue('D12',$data_total_roll->total_roll .' ROLL');
              $sheet->setCellValue('D13',($data_inspect_roll ? $data_inspect_roll->total_roll : '0' ).' ROLL');
      
      
              //Body Data
      
              //No
              $sheet->mergeCells('A16:A17');
              $sheet->cell('A16:B16',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                  'bold'     => true
                ));
                $cell->setAlignment('center');
                $cell->setBackground('#bdc3c7');
                $cell->setBorder('thin','thin','thin','thin');
              });
              //RCVD LAB
              $sheet->mergeCells('B16:B17');
              $sheet->cell('B16:B16',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                  'bold'     => true
                ));
                $cell->setAlignment('center');
                $cell->setBackground('#bdc3c7');
                $cell->setBorder('thin','thin','thin','thin');
              });
              //No ROLL
              $sheet->mergeCells('C16:C17');
              $sheet->cell('C16:C17',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                  'bold'     => true
                ));
                $cell->setAlignment('center');
                $cell->setBackground('#bdc3c7');
                $cell->setBorder('thin','thin','thin','thin');
              });
              //BATCH NUMBER
              $sheet->mergeCells('D16:D17');
              $sheet->cell('D16:D17',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                  'bold'     => true
                ));
                $cell->setAlignment('center');
                $cell->setBackground('#bdc3c7');
                $cell->setBorder('thin','thin','thin','thin');
              });
              $sheet->getStyle('D16:D17')
              ->getAlignment()->setWrapText(true); 
              //LOT
              $sheet->mergeCells('E16:F16');
              $sheet->cell('E16:F16',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                  'bold'     => true
                ));
                $cell->setAlignment('center');
                $cell->setBackground('#bdc3c7');
                $cell->setBorder('thin','thin','thin','thin');
              });
              //AOI 1
              $sheet->cell('E17',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                  'bold'     => true
                ));
                $cell->setAlignment('center');
                $cell->setBackground('#bdc3c7');
                $cell->setBorder('thin','thin','thin','thin');
              });
              //AOI 2
              $sheet->cell('F17',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                  'bold'     => true
                ));
                $cell->setAlignment('center');
                $cell->setBackground('#bdc3c7');
                $cell->setBorder('thin','thin','thin','thin');
              });
              //YARD
              $sheet->mergeCells('G16:G17');
              $sheet->cell('G16:G17',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                  'bold'     => true
                ));
                $cell->setAlignment('center');
                $cell->setBackground('#bdc3c7');
                $cell->setBorder('thin','thin','thin','thin');
              });

              //Remark
              $sheet->mergeCells('H16:H17');
              $sheet->cell('H16:H17',function($cell){
                $cell->setFont(array(
                  'family'   => 'Calibri',
                  'size'     => '10',
                  'bold'     => true
                ));
                $cell->setAlignment('center');
                $cell->setBackground('#bdc3c7');
                $cell->setBorder('thin','thin','thin','thin');
              });
      
              $sheet->setCellValue('A16','No.');
              $sheet->setCellValue('B16','RCVD LAB');
              $sheet->setCellValue('C16','No. Roll');
              $sheet->setCellValue('D16','Batch Number');
              $sheet->setCellValue('E16','Lot');
              $sheet->setCellValue('E17','AOI 1');
              $sheet->setCellValue('F17','AOI 2');
              $sheet->setCellValue('G16','Yard');
              $sheet->setCellValue('H16','Remark');
      
              $no=1;
              $row=18;
              foreach ($data as $i)
              {
                //Nomer
                $sheet->cell('A'.$row,function($cell){
                  $cell->setAlignment('center');
                  $cell->setBorder('thin','thin','thin','thin');
                });
                $sheet->setCellValue('A'.$row,$no);
      
                $sheet->cell('B'.$row,function($cell){
                  $cell->setAlignment('center');
                  $cell->setBorder('thin','thin','thin','thin');
                });
                $sheet->setCellValue('B'.$row,Carbon::parse($i->inspect_lab_date)->format('d-M-Y'));
               
                $sheet->cell('C'.$row,function($cell){
                  $cell->setAlignment('center');
                  $cell->setBorder('thin','thin','thin','thin');
                });
                $sheet->setCellValue('C'.$row,$i->nomor_roll);
               
                $sheet->cell('D'.$row,function($cell){
                  $cell->setAlignment('center');
                  $cell->setBorder('thin','thin','thin','thin');
                });

                $sheet->setCellValue('D'.$row,$i->batch_number);

                $sheet->cell('E'.$row,function($cell){
                  $cell->setAlignment('center');
                  $cell->setBorder('thin','thin','thin','thin');
                });
                $sheet->cell('F'.$row,function($cell){
                  $cell->setAlignment('center');
                  $cell->setBorder('thin','thin','thin','thin');
              });
                if(Auth::user()->warehouse=='1000001')
                {
                    $sheet->cell('E'.$row,function($cell){
                        $cell->setAlignment('center');
                    });
                    $sheet->setCellValue('E'.$row,$i->load_actual);
                }else if(Auth::user()->warehouse=='1000011')
                {
                    $sheet->cell('F'.$row,function($cell){
                        $cell->setAlignment('center');
                    });
                    $sheet->setCellValue('F'.$row,$i->load_actual);
                }
                //Yard
                $sheet->cell('G'.$row,function($cell){
                  $cell->setAlignment('center');
                  $cell->setBorder('thin','thin','thin','thin');
                });
                $sheet->setCellValue('G'.$row,$i->qty_arrival);
                $sheet->cell('H'.$row,function($cell){
                  $cell->setAlignment('center');
                  $cell->setBorder('thin','thin','thin','thin');
              });
                $sheet->setCellValue('H'.$row,$i->inspect_lab_remark);
      
                $no++;
                $row++;
              }
              $sheet->mergeCells('A'.$row.':'.'H'.$row);
              $row+=1;
              //Total
              $sheet->cell('A'.$row,function($cell){
                $cell->setAlignment('center');
                $cell->setFont(array(
                  'bold'=>true
                ));
                $cell->setBorder('thin','thin','thin','thin');
              });
              $sheet->setCellValue('A'.$row,'Total');
              $sheet->cell('A'.$row,function($cell){
                $cell->setBorder('thin','thin','thin','thin');
              });
              $sheet->cell('B'.$row,function($cell){
                $cell->setBorder('thin','thin','thin','thin');
              });
              $sheet->cell('C'.$row,function($cell){
                $cell->setBorder('thin','thin','thin','thin');
              });
              $sheet->cell('D'.$row,function($cell){
                $cell->setBorder('thin','thin','thin','thin');
              });
              $sheet->cell('E'.$row,function($cell){
                $cell->setBorder('thin','thin','thin','thin');
              });
              $sheet->cell('F'.$row,function($cell){
                $cell->setBorder('thin','thin','thin','thin');
              });

              //Yard
              $sheet->setCellValue('G'.$row,($data_inspect_roll ? $data_inspect_roll->qty_arrival :'0') );
              $sheet->cell('G'.$row,function($cell){
                $cell->setAlignment('center');
                $cell->setFont(array(
                  'bold'=>true
                ));
                $cell->setBorder('thin','thin','thin','thin');
              });
      
              //Remark
              $sheet->cell('H'.$row,function($cell){
                $cell->setAlignment('center');
                $cell->setFont(array(
                  'bold'=>true
                ));
                $cell->setBorder('thin','thin','thin','thin');
              });
              $sheet->setCellValue('H'.$row,'yards');
      
              $sheet->setCellValue('F12',$data_total_roll->qty_arrival.' Yards');
              $sheet->setCellValue('F13',($data_inspect_roll ? $data_inspect_roll->qty_arrival :'0') .' Yards');
      
              // Set border for range

              //$sheet->setAllBorders('thin');
              // $sheet->setBorder('A1:H4', 'thin');
              // $sheet->setBorder('A4:H4', 'thin');
              // $sheet->setBorder('A16:H17', 'thin');
      
              // $sheet->setBorder('A17:H'.$row,'thin');
      
            });
          })
          ->export('xlsx');
    
    }

    public function updateLot(Request $request)
    {
      $list_lots            = json_decode($request->list_lot);
      $movement_date        = carbon::now()->todatetimestring();
      $insert_stock_per_lots= array();

      try 
      {
        DB::beginTransaction();
        
        foreach ($list_lots as $key => $list_lot) 
        {
          $material_stock                     = MaterialStock::find($list_lot->id);
          $material_stock->load_actual        = trim(strtoupper($list_lot->lot_actual));
          $material_stock->user_lab_id        = auth::user()->id;
          $material_stock->inspect_lot_result = $list_lot->result;
          $material_stock->inspect_lab_remark = $list_lot->inspect_lab_remark;
          $monitoring_receiving_fabric_ids [] = $material_stock->monitoring_receiving_fabric_id;

          if($list_lot->result == 'REJECT')
          {
              $material_stock->qc_result                  = 'REJECT BY LOT NOT MATCH';
              $material_stock->is_reject                  = false;
              $material_stock->is_reject_by_lot           = true;
              $material_stock->qty_reject_non_by_inspect  = $material_stock->available_qty;
              $material_stock->reject_by_lot_date         = $movement_date;
          }

          if($list_lot->result == 'RELEASE') $insert_stock_per_lots [] = $material_stock->monitoring_receiving_fabric_id;
          
          $material_stock->save();

          DetailMaterialPreparationFabric::where('material_stock_id',$list_lot->id)
          ->update([
              'actual_lot' => trim(strtoupper($list_lot->lot_actual))
          ]);
        }

        //insert stock per lot
        foreach (array_unique($insert_stock_per_lots) as $key => $value) 
        {
          // $data = DB::select(db::raw("SELECT * FROM get_stock_per_lot(
          //     '".$value."'
          //     );"
          // ));

          $data = MaterialStock::select(
                  'monitoring_receiving_fabric_id',
                  'c_order_id',
                  'c_bpartner_id',
                  'warehouse_id',
                  'item_id',
                  'item_code',
                  'no_invoice',
                  'no_packing_list',
                  'document_no',
                  'supplier_code',
                  'supplier_name',
                  'type_stock',
                  'type_stock_erp_code',
                  'load_actual',
                  'uom',
                  db::raw("sum(qty_arrival) as total_arrival"))
                  ->where('inspect_lot_result', 'RELEASE')
                  ->where('monitoring_receiving_fabric_id', $value)
                  ->whereNull('deleted_at')
                  ->groupBy('monitoring_receiving_fabric_id',
                  'c_order_id',
                  'c_bpartner_id',
                  'warehouse_id',
                  'item_id',
                  'item_code',
                  'no_invoice',
                  'no_packing_list',
                  'document_no',
                  'supplier_code',
                  'supplier_name',
                  'type_stock',
                  'type_stock_erp_code',
                  'load_actual',
                  'uom')
                  ->get();


          foreach ($data as $key_2 => $datum) 
          {
            $monitoring_receiving_fabric_id = $datum->monitoring_receiving_fabric_id;
            $c_order_id                     = $datum->c_order_id;
            $c_bpartner_id                  = $datum->c_bpartner_id;
            $warehouse_id                   = $datum->warehouse_id;
            $item_id                        = $datum->item_id;
            $no_invoice                     = $datum->no_invoice;
            $no_packing_list                = $datum->no_packing_list;
            $document_no                    = $datum->document_no;
            $supplier_code                  = $datum->supplier_code;
            $supplier_name                  = $datum->supplier_name;
            $type_stock                     = $datum->type_stock;
            $type_stock_erp_code            = $datum->type_stock_erp_code;
            $actual_lot                     = $datum->load_actual;
            $uom                            = $datum->uom;
            $total_arrival                  = sprintf('%0.8f',$datum->total_arrival);

            $is_exists = MaterialStockPerLot::whereNull('deleted_at')
            ->where([
                ['c_order_id',$c_order_id],
                ['c_bpartner_id',$c_bpartner_id],
                ['item_id',$item_id],
                ['warehouse_id',$warehouse_id],
                ['actual_lot',$actual_lot],
                ['type_stock',$type_stock],
                ['no_invoice',$no_invoice],
                ['no_packing_list',$no_packing_list],
            ])
            ->first();

            if(!$is_exists)
            {
              $material_stock_per_lot = MaterialStockPerLot::FirstOrCreate([
                  'c_order_id'          => $c_order_id,
                  'c_bpartner_id'       => $c_bpartner_id,
                  'warehouse_id'        => $warehouse_id,
                  'item_id'             => $item_id,
                  'no_invoice'          => $no_invoice,
                  'no_packing_list'     => $no_packing_list,
                  'document_no'         => $document_no,
                  'supplier_code'       => $supplier_code,
                  'supplier_name'       => $supplier_name,
                  'type_stock'          => $type_stock,
                  'type_stock_erp_code' => $type_stock_erp_code,
                  'actual_lot'          => $actual_lot,
                  'uom'                 => $uom,
                  'qty_stock'           => $total_arrival,
                  'qty_available'       => $total_arrival,
                  'created_at'          => $movement_date,
                  'updated_at'          => $movement_date,
              ]);

              $material_stock_per_lot_id  = $material_stock_per_lot->id;
              $old_available              = '0';
              $new_available              = $material_stock_per_lot->qty_available;
              $curr_type_stock_erp_code   = $material_stock_per_lot->type_stock_erp_code;
              $curr_type_stock            = $material_stock_per_lot->type_stock;
              $curr_actual_lot            = $material_stock_per_lot->actual_lot;
              
              
            }else
            {
              $material_stock_per_lot_id  = $is_exists->id;
              $reserved_qty               = $is_exists->qty_reserved;
              $new_stock                  = $total_arrival;
              $availabilty_qty            = $new_stock - $reserved_qty;

              $new_available              = $availabilty_qty;
              $old_available              = $is_exists->qty_available;
              $curr_type_stock_erp_code   = $is_exists->type_stock_erp_code;
              $curr_type_stock            = $is_exists->type_stock;
              
              $is_exists->qty_stock       = sprintf('%0.8f',$new_stock);
              $is_exists->qty_available   = sprintf('%0.8f',$availabilty_qty);
              $is_exists->save();
            }
          
            HistoryStockPerLot::historyStockPerLot($material_stock_per_lot_id
            ,$new_available
            ,$old_available
            ,null
            ,null
            ,null
            ,null
            ,null
            ,'TAMBAH STOCK'
            ,auth::user()->name
            ,auth::user()->id
            ,$curr_type_stock_erp_code
            ,$curr_type_stock
            ,false
            ,false);

            MaterialStock::where([
              ['monitoring_receiving_fabric_id',$monitoring_receiving_fabric_id],
              ['load_actual',$actual_lot],
              ['c_order_id',$c_order_id],
              ['c_bpartner_id',$c_bpartner_id],
              ['item_id',$item_id],
              ['warehouse_id',$warehouse_id],
              ['no_invoice',$no_invoice],
              ['no_packing_list',$no_packing_list],
              ['inspect_lot_result', 'RELEASE'],
            ])
            ->update([
              'material_stock_per_lot_id' => $material_stock_per_lot_id
            ]);
          }

          foreach (array_unique($monitoring_receiving_fabric_ids) as $key => $monitoring_receiving_fabric_id) 
          {
              //cek lot sudah ada semua apa belum dalam satu penerimaan 
              $is_complete_lot = $this->getLotComplete($monitoring_receiving_fabric_id);
              if($is_complete_lot == true)
              {
                  //schedule untuk jalanin alokasi per lot
                  Temporary::create([
                      'barcode'       => $monitoring_receiving_fabric_id,
                      'status'        => 'complete_lot',
                      'user_id'       => auth::user()->id,
                      'created_at'    => $movement_date,
                      'updated_at'    => $movement_date,
                  ]);
              }
          }
          
        }

        DB::commit();
      } catch (Exception $e) 
      {
          DB::rollBack();
          $message = $e->getMessage();
          ErrorHandler::db($message);
      }
    }

    public function exportAll(Request $request)
    {
      $warehouse  = ($request->warehouse) ? $request->warehouse : auth::user()->warehouse;
      if($warehouse == '1000011') $filename = 'report_inspect_lab_fab_all_aoi2.csv';
      else if($warehouse == '1000001') $filename = 'report_inspect_lab_fab_all_aoi1.csv';
    
        $file = Config::get('storage.report') . '/' . $filename;
    
        if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';
        
        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;      
    }

    public function remarkDetail(Request $request,$id)
    {
          try 
          {
              DB::beginTransaction();
  
              $material_stock                        = MaterialStock::find($id);
              $material_stock->inspect_lab_remark    = trim(strtolower($request->remark));
              $material_stock->save();
  
              DB::commit();
  
              return response()->json('success',200);
              
          } catch (Exception $e) 
          {
              DB::rollBack();
              $message = $e->getMessage();
              ErrorHandler::db($message);
          }
    }

    public function getLotComplete($monitoring_receiving_fabric_id)
    {
        $material_stocks = MaterialStock::where('monitoring_receiving_fabric_id', $monitoring_receiving_fabric_id)
                           ->whereNull('load_actual')
                           ->exists();
        if($material_stocks)
        {
            return false;
        }
        else
        {
           return true;

        }


    }
}
