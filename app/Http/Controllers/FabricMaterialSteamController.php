<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Models\User;
use App\Models\MaterialStock;
use App\Models\MaterialPreparationFabric;
use App\Models\DetailMaterialPreparation;
use App\Models\DetailMaterialPreparationFabric;

class FabricMaterialSteamController extends Controller
{
    public function index(Request $request)
    {
        return view('fabric_material_steam.index');
    }

    public function create(Request $request)
    {
        $warehouse_id                       = $request->warehouse_id;
        $barcode                            = strtoupper(trim($request->barcode));

        $detail_material_preparation_fabric = DetailMaterialPreparationFabric::where([
            ['barcode',$barcode],
            ['warehouse_id',$warehouse_id],
        ])
        ->orderby('created_at','desc')
        ->whereNull('deleted_at')
        ->first();

        if($detail_material_preparation_fabric)
        {
            $detail_material_preparation_fabric_id = $detail_material_preparation_fabric->id;
            if($detail_material_preparation_fabric->last_status_movement =='out') return response()->json('Barcode already supplied.', 422);
            if($detail_material_preparation_fabric->materialPreparationFabric->steam_date != null ) return response()->json('Barcode already scan.', 422);
            
            $detail_material_preparation_fabric_id  = $detail_material_preparation_fabric_id;
            $material_preparation_fabric_id         = $detail_material_preparation_fabric->material_preparation_fabric_id;
            $material_stock_id                      = $detail_material_preparation_fabric->material_stock_id;
            $document_no                            = strtoupper($detail_material_preparation_fabric->materialStock->document_no);
            $batch_number                           = $detail_material_preparation_fabric->materialStock->batch_number;
            $barcode                                = $detail_material_preparation_fabric->barcode;
            $reserved_qty                           = sprintf('%0.8f',$detail_material_preparation_fabric->reserved_qty);
            $warehouse_id                           = $detail_material_preparation_fabric->warehouse_id;
            $nomor_roll                             = $detail_material_preparation_fabric->materialStock->nomor_roll;
            $item_code                              = strtoupper($detail_material_preparation_fabric->materialStock->item_code);
            $supplier_name                          = $detail_material_preparation_fabric->materialStock->supplier_name;
            $c_bpartner_id                          = $detail_material_preparation_fabric->materialStock->c_bpartner_id;
            $color                                  = $detail_material_preparation_fabric->materialStock->color;
            $actual_width                           = $detail_material_preparation_fabric->materialStock->actual_width;
            $is_additional                          = $detail_material_preparation_fabric->materialPreparationFabric->is_from_additional;
            $style                                  = $detail_material_preparation_fabric->materialPreparationFabric->_style;
            $article_no                             = $detail_material_preparation_fabric->materialPreparationFabric->article_no;

            $uom                                    = trim($detail_material_preparation_fabric->materialStock->uom);
            $_planning_date                         = ($detail_material_preparation_fabric->materialPreparationFabric->planning_date) ? $detail_material_preparation_fabric->materialPreparationFabric->planning_date->todateTimeString() : $detail_material_preparation_fabric->materialPreparationFabric->planning_date;
            $planning_date                          = ($detail_material_preparation_fabric->materialPreparationFabric->planning_date ? $detail_material_preparation_fabric->materialPreparationFabric->planning_date->format('d/M/Y') : $detail_material_preparation_fabric->materialPreparationFabric->planning_date);

            $obj                                    = new stdClass();
            $obj->id                                = $detail_material_preparation_fabric_id;
            $obj->material_preparation_fabric_id    = $material_preparation_fabric_id;
            $obj->_planning_date                    = $_planning_date;
            $obj->planning_date                     = $planning_date;
            $obj->material_stock_id                 = $material_stock_id;
            $obj->barcode                           = $barcode;
            $obj->reserved_qty                      = $reserved_qty;
            $obj->warehouse_id                      = $warehouse_id;
            $obj->nomor_roll                        = $nomor_roll;
            $obj->batch_number                      = $batch_number;
            $obj->document_no                       = $document_no;
            $obj->item_code                         = $item_code;
            $obj->supplier_name                     = $supplier_name;
            $obj->c_bpartner_id                     = $c_bpartner_id;
            $obj->article_no                        = $article_no;
            $obj->style                             = $style;
            $obj->color                             = $color;
            $obj->actual_width                      = $actual_width;
            $obj->uom                               = $uom;
            $obj->movement_date                     = Carbon::now()->toDateTimeString();
            $obj->is_additional                     = $is_additional;
           
            return response()->json($obj, 200);
        }else{
            return response()->json('Barcode not found', 422);
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fabric_material_steams' => 'required|not_in:[]'
        ]);

        if ($validator->passes())
        {
            $warehouse_id   = $request->warehouse_id;
            $barcodes       = json_decode($request->fabric_material_steams);

            try
            {
                db::beginTransaction();

                foreach ($barcodes as $key => $value) 
                {
                    $material_preparation_fabric_id                       = $value->material_preparation_fabric_id;
                    $material_preparation_fabric                          = MaterialPreparationFabric::find($material_preparation_fabric_id);
                    $material_preparation_fabric->machine                 = 'STEAM';
                    $material_preparation_fabric->steam_date              = carbon::now();
                    $material_preparation_fabric->save();

                    db::commit();
                }
            }catch (Exception $ex)
            {
                db::rollback();
                $message = $ex->getMessage();
            }
        }

    }
}
