<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use View;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;

use App\Models\User;
use App\Models\Item;
use App\Models\Locator;
use App\Models\Supplier;
use App\Models\MaterialStock;
use App\Models\MappingStocks;
use App\Models\AllocationItem;
use App\Models\AutoAllocation;
use App\Models\DetailMaterialStock;
use App\Models\MaterialRequirement;
use App\Http\Controllers\AccessoriesMaterialStockApprovalController as HistoryStock;

class FabricReportMaterialStockMaterialManagementController extends Controller
{
    public function index(Request $request)
    {
        $warehouse_id                   = ($request->has('warehouse_id'))?$request->warehouse_id:auth::user()->warehouse;
        $_mapping_stock                 = ($request->has('mapping_stock_id'))?$request->mapping_stock_id:'ALL';
        $active_tab                     = ($request->has('active_tab'))?'inactive':'active';
        return view('fabric_report_material_stock_material_management.index',compact('warehouse_id','array_mapping_stocks','_mapping_stock','active_tab'));
    }

    public function dataActive(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id           = $request->warehouse;
            $type_stock_erp_code    = $request->type_stock_erp_code;
            $material_stocks        = db::table('report_material_stock_material_management_fabric_v')->where('warehouse_id','LIKE',"%$warehouse_id%");
            if($type_stock_erp_code) $material_stocks = $material_stocks->where('type_stock_erp_code',$type_stock_erp_code);
            
            $material_stocks  = $material_stocks->orderby('available_qty','desc');
            
            return DataTables::of($material_stocks)
            ->addColumn('source',function ($material_stocks){
                return '<b>Po Buyer Origin</b><br/> '.$material_stocks->po_buyer.
                '<br><b>Origin Stock</b><br/> '.$material_stocks->source;

            })
            ->addColumn('supplier',function ($material_stocks){
                return'<br><b>Supplier Code</b><br/> '.$material_stocks->supplier_code.
                '<br><b>Supplier Name</b><br/> '.$material_stocks->supplier_name;
            })
            ->addColumn('item',function ($material_stocks){
                return '<b>Item Code</b><br/> '.$material_stocks->item_code.
                '<br><b>Item Desc</b><br/> '.$material_stocks->item_desc.
                '<br><b>Category</b><br/> '.$material_stocks->category;
            })
            ->editColumn('stock',function ($material_stocks){
                return number_format($material_stocks->stock, 4, '.', ',');
            })
            ->editColumn('reserved_qty',function ($material_stocks){
                return number_format($material_stocks->reserved_qty, 4, '.', ',');
            })
            ->addColumn('_available_qty',function ($material_stocks){
                return number_format($material_stocks->available_qty, 4, '.', ',');
            })
            ->addColumn('action', function($material_stocks) {
                if(auth::user()->hasRole('admin-ict-fabric'))
                {
                    return view('_action',[
                        'model'                 => $material_stocks,
                        'historyModal'          => route('fabricReportMaterialStockMaterialManagement.detailAllocation',$material_stocks->material_stock_id),
                        'edit_modal'            => route('fabricReportMaterialStockMaterialManagement.editStock',$material_stocks->material_stock_id),
                        'delete'                => route('fabricReportMaterialStockMaterialManagement.deleteStock',$material_stocks->material_stock_id),
                        'recalculate'           => route('fabricReportMaterialStockMaterialManagement.recalculate',$material_stocks->material_stock_id),
                        'allocatingStock'       => route('fabricReportMaterialStockMaterialManagement.allocatingStock',$material_stocks->material_stock_id),
                        'change_type'           => route('fabricReportMaterialStockMaterialManagement.changeTypeStock',$material_stocks->material_stock_id),
                    ]);
                }else{
                    if(auth::user()->hasRole(['mm-staff','free-stock']))
                    {
                        return view('_action',[
                            'historyModal'          => route('fabricReportMaterialStockMaterialManagement.detailAllocation',$material_stocks->material_stock_id),
                            'edit_modal'            => route('fabricReportMaterialStockMaterialManagement.editStock',$material_stocks->material_stock_id),
                            'delete'                => route('fabricReportMaterialStockMaterialManagement.deleteStock',$material_stocks->material_stock_id),
                            'recalculate'           => route('fabricReportMaterialStockMaterialManagement.recalculate',$material_stocks->material_stock_id),
                            'allocatingStock'       => route('fabricReportMaterialStockMaterialManagement.allocatingStock',$material_stocks->material_stock_id),
                        ]);
                    }else{
                        return view('_action',[
                            'model'                 => $material_stocks,
                            'historyModal'          => route('fabricReportMaterialStockMaterialManagement.detailAllocation',$material_stocks->material_stock_id),
                        ]);
                    }
                    
                }
            })
            ->setRowAttr([
                'style' => function($material_stocks) {
                    if($material_stocks->available_qty > 0) return  'background-color: #d9ffde';
                },
            ])
            ->rawColumns(['source','quantity','item','action','style','supplier']) 
            ->make(true);
        }
    }

    public function dataInactive(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id           = $request->warehouse;
            $type_stock_erp_code    = $request->type_stock_erp_code;
            $material_stocks = db::table('report_material_stock_material_management_incative_fabric_v')->where('warehouse_id','LIKE',"%$warehouse_id%");
            if($type_stock_erp_code) $material_stocks = $material_stocks->where('type_stock_erp_code',$type_stock_erp_code);
            
            return DataTables::of($material_stocks)
            ->addColumn('source',function ($material_stocks){
                return '<b>Po Buyer Origin</b><br/> '.$material_stocks->po_buyer.
                '<br><b>Origin Stock</b><br/> '.$material_stocks->source;

            })
            ->addColumn('supplier',function ($material_stocks){
                return'<br><b>Supplier Code</b><br/> '.$material_stocks->supplier_code.
                '<br><b>Supplier Name</b><br/> '.$material_stocks->supplier_name;
            })
            ->addColumn('item',function ($material_stocks){
                return '<b>Item Code</b><br/> '.$material_stocks->item_code.
                '<br><b>Item Desc</b><br/> '.$material_stocks->item_desc.
                '<br><b>Category</b><br/> '.$material_stocks->category;
            })
            ->editColumn('stock',function ($material_stocks){
                return number_format($material_stocks->stock, 4, '.', ',');
            })
            ->editColumn('reserved_qty',function ($material_stocks){
                return number_format($material_stocks->reserved_qty, 4, '.', ',');
            })
            ->addColumn('_available_qty',function ($material_stocks){
                return number_format($material_stocks->available_qty, 4, '.', ',');
            })
            ->addColumn('action', function($material_stocks) 
            {
                if(auth::user()->hasRole('admin-ict-fabric'))
                {
                    return view('_action',[
                        'model'                 => $material_stocks,
                        'historyModal'          => route('fabricReportMaterialStockMaterialManagement.detailAllocation',$material_stocks->material_stock_id),
                    ]);
                }else
                {
                    return view('_action',[
                        'model'                 => $material_stocks,
                        'historyModal'          => route('fabricReportMaterialStockMaterialManagement.detailAllocation',$material_stocks->material_stock_id),
                    ]);
                }
            })
            ->setRowAttr([
                'style' => function($material_stocks) {
                    if($material_stocks->available_qty > 0) return  'background-color: green;color:white';
                },
            ])
            ->rawColumns(['source','quantity','item','action','style','supplier']) 
            ->make(true);
        }
    }

    public function editStock($id)
    {
        $material_stock         = MaterialStock::find($id);
        $obj                    = new stdClass();
        $obj->id                = $id;
        $obj->supplier_name     = $material_stock->supplier;
        $obj->document_no       = $material_stock->document_no;
        $obj->item_code         = $material_stock->item_code;
        $obj->po_buyer          = $material_stock->po_buyer;
        $obj->available_qty     = $material_stock->available_qty;
        $obj->uom               = $material_stock->uom;
        
        if(!auth::user()->hasRole(['admin-ict-fabric']))
        {
            $warehouse_user = auth::user()->warehouse;
            if($warehouse_user != $material_stock->warehouse_id)
            {
                return response()->json('You dont have permission to update this data',422);
            }

        }

        return response()->json($obj,200);
    }

    public function storeEditStock(Request $request)
    {
        $new_available_qty_stock    = sprintf('%0.8f',$request->new_available_qty_stock);

        $material_stock             = MaterialStock::find($request->id);
        $curr_type_stock            = $material_stock->type_stock;
        $curr_type_stock_erp_code   = $material_stock->type_stock_erp_code;
        $curr_available_qty         = sprintf('%0.8f',$material_stock->available_qty);
        $locator_id                 = $material_stock->locator_id;
        $operator                   = $new_available_qty_stock - $curr_available_qty;
        $remark                     = trim(strtoupper($request->update_reason));
        $upload_date                = Carbon::now()->toDateTimeString();

        try 
        {
            DB::beginTransaction();
            
            if($operator > 0)
            {
                $new_stock = ($material_stock->stock + $operator);
                $material_stock->stock = sprintf('%0.8f',$new_stock);

                DetailMaterialStock::FirstOrCreate([
                    'material_stock_id'     => $material_stock->id,
                    'remark'                => $remark,
                    'uom'                   => $material_stock->uom,
                    'qty'                   => $operator,
                    'locator_id'            => $material_stock->locator_id,
                    'user_id'               => Auth::user()->id,
                    'created_at'            => $upload_date,
                    'updated_at'            => $upload_date,
                ]);
            }else
            {
                $new_reserved_qty = $material_stock->reserved_qty + (-1*$operator);
                AllocationItem::FirstOrCreate([
                    'material_stock_id'         => $material_stock->id,
                    'c_order_id'                => $material_stock->c_order_id,
                    'document_no'               => $material_stock->document_no,
                    'item_id_source'            => $material_stock->item_id_source,
                    'item_id_book'              => $material_stock->item_id,
                    'item_code'                 => $material_stock->item_code,
                    'item_code_source'          => $material_stock->item_code,
                    'item_desc'                 => $material_stock->item_desc,
                    'category'                  => $material_stock->category,
                    'uom'                       => $material_stock->uom,
                    'warehouse'                 => auth::user()->warehouse,
                    'qty_booking'               => sprintf('%0.8f',(-1*$operator)),
                    'is_not_allocation'         => true,
                    'user_id'                   => auth::user()->id,
                    'confirm_user_id'           => auth::user()->id,
                    'confirm_by_warehouse'      => 'approved',
                    'confirm_date'              => carbon::now(),
                    'remark'                    => $remark,
                ]);

                $material_stock->reserved_qty   = sprintf('%0.8f',$new_reserved_qty);
                
            }

            $material_stock->available_qty      = sprintf('%0.8f',$new_available_qty_stock);
            if($new_available_qty_stock <= 0)
            {
                $material_stock->is_allocated   = true;
                $material_stock->is_active      = false;
            }else
            {
                $material_stock->is_allocated   = false;
                $material_stock->is_active      = true;
            }

            HistoryStock::approved($request->id
            ,$new_available_qty_stock
            ,$curr_available_qty
            ,null
            ,null
            ,null
            ,null
            ,null
            ,null
            ,auth::user()->name
            ,auth::user()->id
            ,$curr_type_stock_erp_code
            ,$curr_type_stock
            ,false
            ,false);
                        
            $count_item_on_locator = MaterialStock::where([
                ['locator_id',$locator_id],
                ['is_allocated',false],
                ['is_closing_balance',false],
            ])
            ->whereNull('deleted_at')
            ->count();

            $locator = Locator::find($locator_id);
            if($locator)
            {
                $locator->counter_in = $count_item_on_locator;
                $locator->save();
            }

            $material_stock->save();
        
            DB::commit();
            
            return response()->json(200);
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
    	}
    }

    public function deleteStock($id)
    {
        $material_stock         = MaterialStock::find($id);
        $obj                    = new stdClass();
        $obj->id                = $id;
        $obj->supplier_name     = $material_stock->supplier;
        $obj->document_no       = $material_stock->document_no;
        $obj->item_code         = $material_stock->item_code;
        $obj->po_buyer          = $material_stock->po_buyer;
        $obj->locator_id        = $material_stock->locator_id;
        $obj->locator_name      = ($material_stock->locator_id) ? $material_stock->locator->code : null;
        $obj->available_qty     = $material_stock->available_qty;
        $obj->uom               = $material_stock->uom;
        
        if(!auth::user()->hasRole(['admin-ict-fabric']))
        {
            $warehouse_user = auth::user()->warehouse;
            if($warehouse_user != $material_stock->warehouse_id)
            {
                return response()->json('You dont have permission to update this data',422);
            }

        }

        return response()->json($obj,200);
    }

    public function storeDeleteStock(Request $request)
    {
        $id                             = $request->id;
        $delete_type                    = $request->delete_type;
        $uom                            = $request->delete_uom;
        $delete_qty                     = $request->delete_qty;
        $delete_reason                  = strtoupper($request->delete_reason);

        $material_stock                 = MaterialStock::find($id);
        $locator_id                     = $material_stock->locator_id;
        $locator                        = Locator::find($locator_id);
        $old_available_qty              = $material_stock->available_qty;
        $curr_available_qty             = $material_stock->available_qty;
        $curr_reserved_qty              = $material_stock->reserved_qty;
        $curr_type_stock                = $material_stock->type_stock;
        $curr_type_stock_erp_code       = $material_stock->type_stock_erp_code;

        try 
        {
    		DB::beginTransaction();
            
            if($material_stock)
            {
                if($material_stock->last_status != 'prepared')
                {
                    $material_stock->last_status = 'prepared';
                    $material_stock->save();

                    if ($delete_qty/$curr_available_qty >= 1) $_supplied = $curr_available_qty;
                    else $_supplied = $delete_qty;

                    //echo $_supplied.' '.$curr_available_qty;
                    if($_supplied > 0 && $curr_available_qty > 0)
                    {
                        AllocationItem::Create([
                            'material_stock_id'             => $material_stock->id,
                            'c_order_id'                    => $material_stock->c_order_id,
                            'document_no'                   => $material_stock->document_no,
                            'item_id_source'                => $material_stock->item_id_source,
                            'item_id_book'                  => $material_stock->item_id,
                            'item_code'                     => $material_stock->item_code,
                            'item_code_source'              => $material_stock->item_code,
                            'item_desc'                     => $material_stock->item_desc,
                            'category'                      => $material_stock->category,
                            'uom'                           => $material_stock->uom,
                            'warehouse'                     => auth::user()->warehouse,
                            'qty_booking'                   => sprintf('%0.8f',$_supplied),
                            'is_not_allocation'             => true,
                            'user_id'                       => auth::user()->id,
                            'confirm_user_id'               => auth::user()->id,
                            'confirm_by_warehouse'          => 'approved',
                            'confirm_date'                  => carbon::now(),
                            'remark'                        => 'STOCK DI HAPUS, '.$delete_reason,
                        ]);

                        $curr_available_qty            -= $_supplied;
                        $material_stock->reserved_qty   = sprintf('%0.8f',$_supplied + $curr_reserved_qty);
                        $material_stock->available_qty  = sprintf('%0.8f',$curr_available_qty);
                        
                        if($curr_available_qty <=0)
                        {
                            $material_stock->is_allocated       = true;
                            $material_stock->is_closing_balance = true;
                            $material_stock->is_active          = false;
                        }
                        
                        HistoryStock::approved($id
                        ,$curr_available_qty
                        ,$old_available_qty
                        ,null
                        ,null
                        ,null
                        ,null
                        ,null
                        ,'DELETE STOCK'
                        ,auth::user()->name
                        ,auth::user()->id
                        ,$curr_type_stock_erp_code
                        ,$curr_type_stock
                        ,false
                        ,false);

                        $material_stock->last_status = null;
                        $material_stock->save();
                    }
                } 
            }

            if($locator)
            {
                $count_item_on_locator = MaterialStock::where([
                    ['locator_id',$locator->id],
                    ['is_allocated',false],
                    ['is_closing_balance',false],
                ])
                ->whereNull('deleted_at')
                ->count();

                $locator->counter_in = $count_item_on_locator;
                $locator->save();
            }

            DB::commit();
            return response()->json(200);
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    }

    public function detailAllocation(Request $request,$id)
    {
        //return view('errors.503');
        $po_buyer           = $request->q;
        $material_stock     = MaterialStock::find($id);
        $obj                = new stdClass();
        $obj->supplier_name = $material_stock->supplier_name;
        $obj->document_no   = $material_stock->document_no;
        $obj->item_code     = $material_stock->item_code;
        $obj->po_buyer      = ($material_stock->po_buyer) ? $material_stock->po_buyer : '';
        $obj->locator       = ($material_stock->locator_id) ? $material_stock->locator->code : '';

        $detail_stock = DetailMaterialStock::where('material_stock_id',$id)->orderBy('created_at','desc')->get();

        $allocation_per_pobuyer = AllocationItem::where([
            ['material_stock_id',$id],
            ['is_from_allocation_buyer',true],
            ['is_not_allocation',false],
            ['po_buyer','LIKE',"%$po_buyer%"]
        ])
        ->whereNull('deleted_at')
        ->where(function($query){
            $query->where('confirm_by_warehouse','approved')
            ->orWhereNull('confirm_by_warehouse');
        })
        ->orderBy('created_at','desc')
        ->get();

        $allocation_non_per_pobuyer = AllocationItem::where([
            ['material_stock_id',$id],
            ['is_from_allocation_buyer',false],
            ['is_not_allocation',false]
        ])
        ->whereNull('deleted_at')
        ->where(function($query){
            $query->where('confirm_by_warehouse','approved')
            ->orWhereNull('confirm_by_warehouse');
        })
        ->orderBy('created_at','desc')
        ->get();

        $non_allocation = AllocationItem::where([
            ['material_stock_id',$id],
            ['is_not_allocation',true],
        ])
        ->whereNull('deleted_at')
        ->where(function($query){
            $query->where('confirm_by_warehouse','approved')
            ->orWhereNull('confirm_by_warehouse');
        })
        ->orderBy('created_at','desc')
        ->get();

        $data = [
            'view_detail_stock'                 => View::make('fabric_report_material_stock_material_management._detail_material_stock',compact('detail_stock'))->render(),
            'view_allocation_per_pobuyer'       => View::make('fabric_report_material_stock_material_management._booking_material_stock',compact('allocation_per_pobuyer'))->render(),
            'view_allocation_non_per_pobuyer'   => View::make('fabric_report_material_stock_material_management._booking_non_buyer_material_stock',compact('allocation_non_per_pobuyer'))->render(),
            'view_non_allocation'               => View::make('fabric_report_material_stock_material_management._booking_non_allocation',compact('non_allocation'))->render(),
            'material_stock'                    => $obj
        ];

        return $data;
    }

    public function recalculate($id)
    {
        try
        {
            DB::beginTransaction();
            //return view('errors.503');
            $material_stock         = MaterialStock::find($id);
            $_stock                 = sprintf('%0.8f',$material_stock->stock);
            $type_stock             = $material_stock->type_stock;
            $type_stock_erp_code    = $material_stock->type_stock_erp_code;
            $old_available_qty      = sprintf('%0.8f',$material_stock->available_qty);
            $total_detail_stock     = sprintf('%0.8f',DetailMaterialStock::where('material_stock_id',$id)->sum('qty'));


            if(!auth::user()->hasRole(['admin-ict-fabric']))
            {
                $warehouse_user = auth::user()->warehouse;
                if($warehouse_user != $material_stock->warehouse_id)
                {
                    return response()->json('You dont have permission to update this data',422);
                }

            }

            if($total_detail_stock != $_stock) $stock = $total_detail_stock;
            else $stock = $_stock;

            $total_allocation_item  = AllocationItem::where('material_stock_id',$id)
            ->whereNull('deleted_at')
            ->where(function($query){
                $query->where('confirm_by_warehouse','approved')
                ->orWhereNull('confirm_by_warehouse');
            })
            ->sum('qty_booking');

            $new_available_qty = sprintf('%0.8f',$stock - $total_allocation_item);

            if($new_available_qty <=0)
            {
                $material_stock->is_allocated   = true;
                $material_stock->is_active      = false;
            }else
            {
                $material_stock->is_allocated   = false;
                $material_stock->is_active      = true;
            }

            $locator_id = $material_stock->locator_id;

            if($locator_id)
            {
                $count_item_on_locator = MaterialStock::where([
                    ['locator_id',$locator_id],
                    ['is_allocated',false],
                    ['is_closing_balance',false],
                ])
                ->whereNull('deleted_at')
                ->count();

                $locator = Locator::find($locator_id);
                if($locator)
                {
                    $locator->counter_in = $count_item_on_locator ;
                    $locator->save();
                }
            }

            if ($new_available_qty == 0) $new_available_qty = '0';
            else $new_available_qty = sprintf('%0.8f',$new_available_qty);

            if($new_available_qty != $old_available_qty)
            {
                HistoryStock::approved($id
                ,$new_available_qty
                ,$old_available_qty
                ,null
                ,null
                ,null
                ,null
                ,null
                ,'RECALCULATE STOCK'
                ,auth::user()->name
                ,auth::user()->id
                ,$type_stock_erp_code
                ,$type_stock
                ,false
                ,false);
                
            } 

            $material_stock->stock          = sprintf('%0.8f',$stock);
            $material_stock->reserved_qty   = sprintf('%0.8f',$total_allocation_item);
            $material_stock->available_qty  = sprintf('%0.8f',$new_available_qty);
            $material_stock->save();

            DB::commit();
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function changeTypeStock($id)
    {
        $material_stock             = MaterialStock::find($id);
        $obj                        = new stdClass();
        $obj->id                    = $id;
        $obj->supplier_name         = $material_stock->supplier;
        $obj->document_no           = $material_stock->document_no;
        $obj->item_code             = $material_stock->item_code;
        $obj->po_buyer              = $material_stock->po_buyer;
        $obj->available_qty         = $material_stock->available_qty;
        $obj->type_stock_erp_code   = $material_stock->type_stock_erp_code;
        $obj->type_stock            = $material_stock->type_stock;
        $obj->uom                   = $material_stock->uom;
        $obj->locator_name          = ($material_stock->locator_id) ? $material_stock->locator->code : null;
        
        if(!auth::user()->hasRole(['admin-ict-fab','mm-staff']))
        {
            $warehouse_user = auth::user()->warehouse;
            if($warehouse_user != $material_stock->warehouse_id)
            {
                return response()->json('You dont have permission to update this data',422);
            }

        }
        
        return response()->json($obj,200);
    }

    public function storeChangeTypeStock(Request $request)
    {
        $id                         = $request->id;
        $move_type                  = $request->change_stock_type_option;
        $new_type_stock             = $request->mapping_type_stock_option;
        $qty_change                 = $request->qty_change_stock;

        $material_stock             = MaterialStock::find($id);
        $old_type_stock             = $material_stock->type_stock;
        $old_type_stock_erp_code    = $material_stock->type_stock_erp_code;
        $locator_id                 = $material_stock->locator_id;
        $upload_date                = Carbon::now()->toDateTimeString();

        try 
        {
            DB::beginTransaction();
            
            if($old_type_stock != $new_type_stock)
            {
                $old_available          = sprintf('%0.8f',$material_stock->available_qty);
                $curr_available_qty     = sprintf('%0.8f',$material_stock->available_qty);
                $cur_reserved_qty       = sprintf('%0.8f',$material_stock->reserved_qty);

                if ($qty_change/$curr_available_qty >= 1) $_supplied = $curr_available_qty;
                else $_supplied = $qty_change;

                $mapping_stock              = MappingStocks::where('type_stock',$new_type_stock)->first();
                $new_type_stock_erp_code    = ($mapping_stock)? $mapping_stock->type_stock_erp_code : null;

                if($_supplied > 0 && $curr_available_qty > 0)
                {
                    $date_now = carbon::now();
                    //insert new allocataion
                    AllocationItem::FirstOrCreate([
                        'material_stock_id'         => $material_stock->id,
                        'c_order_id'                => $material_stock->c_order_id,
                        'document_no'               => $material_stock->document_no,
                        'item_id_source'            => $material_stock->item_id_source,
                        'item_id_book'              => $material_stock->item_id,
                        'item_code'                 => $material_stock->item_code,
                        'item_code_source'          => $material_stock->item_code,
                        'item_desc'                 => $material_stock->item_desc,
                        'category'                  => $material_stock->category,
                        'uom'                       => $material_stock->uom,
                        'warehouse'                 => auth::user()->warehouse,
                        'qty_booking'               => sprintf('%0.8f',$_supplied),
                        'is_not_allocation'         => true,
                        'user_id'                   => auth::user()->id,
                        'confirm_user_id'           => auth::user()->id,
                        'confirm_by_warehouse'      => 'approved',
                        'confirm_date'              => carbon::now(),
                        'created_at'                => $date_now,
                        'remark'                    => 'DIPINDAHKAN KE TIPE STOCK '.$new_type_stock,
                    ]);

                
                    //insert new stock
                    $is_exists = MaterialStock::whereNull('deleted_at')
                    ->where([
                        [db::raw("upper(document_no)"),$material_stock->document_no],
                        [db::raw('upper(item_code)'),strtoupper($material_stock->item_code)],
                        ['type_po', 2],
                        ['uom',$material_stock->uom],
                        ['locator_id',$locator_id],
                        ['warehouse_id',$material_stock->warehouse_id],
                        ['is_material_others',$material_stock->is_material_others],
                        ['is_closing_balance',false],
                        ['type_stock',$new_type_stock],
                    ])
                    ->whereNotNull('approval_date');

                    if($material_stock->po_detail_id) $is_exists = $is_exists->where('po_detail_id',$material_stock->po_detail_id);
                    else $is_exists = $is_exists->whereNull('po_detail_id');

                    if($material_stock->c_bpartner_id || $material_stock->c_bpartner_id != '') $is_exists = $is_exists->where('c_bpartner_id',$material_stock->c_bpartner_id);
                    else $is_exists = $is_exists->whereNull('c_bpartner_id');

                    if($material_stock->c_order_id || $material_stock->c_order_id != '') $is_exists = $is_exists->where('c_order_id',$material_stock->c_order_id);
                    else $is_exists = $is_exists->whereNull('c_order_id');

                    if($material_stock->po_buyer || $material_stock->po_buyer != '') $is_exists = $is_exists->where('po_buyer',$material_stock->po_buyer);
                    else $is_exists = $is_exists->whereNull('po_buyer');

                    if($material_stock->source || $material_stock->source != '') $is_exists = $is_exists->where('source',$material_stock->source);
                    else $is_exists = $is_exists->whereNull('source');

                    $is_exists = $is_exists->first();
                    
                    if(!$is_exists)
                    {
                        $new_material_stock = MaterialStock::FirstOrCreate([
                            'po_detail_id'          => $material_stock->po_detail_id,
                            'document_no'           => $material_stock->document_no,
                            'supplier_code'         => $material_stock->supplier_code,
                            'supplier_name'         => $material_stock->supplier_name,
                            'c_bpartner_id'         => $material_stock->c_bpartner_id,
                            'c_order_id'            => $material_stock->c_order_id,
                            'locator_id'            => $locator_id,
                            'item_id'               => $material_stock->item_id,
                            'item_code'             => $material_stock->item_code,
                            'item_desc'             => $material_stock->item_desc,
                            'category'              => $material_stock->category,
                            'type_po'               => $material_stock->type_po,
                            'warehouse_id'          => $material_stock->warehouse_id,
                            'uom'                   => $material_stock->uom,
                            'po_buyer'              => $material_stock->po_buyer,
                            'qty_carton'            => $material_stock->qty_carton,
                            'stock'                 => sprintf('%0.8f',$_supplied),
                            'reserved_qty'          => 0,
                            'available_qty'         => sprintf('%0.8f',$_supplied),
                            'is_active'             =>  $material_stock->is_active,
                            'is_material_others'    => $material_stock->is_material_others,
                            'upc_item'              => $material_stock->upc_item,
                            'source'                => $material_stock->source,
                            'type_stock_erp_code'   => $new_type_stock_erp_code,
                            'type_stock'            => $new_type_stock,
                            'source'                => $material_stock->source,
                            'user_id'               => $material_stock->user_id
                        ]);

                        $new_material_stock_id      = $new_material_stock->id;
                        $_old_stock                 = ($old_type_stock)? $old_type_stock : '(-)';
                        $_new_type_stock            = ($new_type_stock)? $new_type_stock : '(-)';
                        $remark_mutasi              = 'DIPINDAHKAN DARI DARI TIPE '.$_old_stock.' KE TIPE '.$_new_type_stock;
                       
                        HistoryStock::approved($new_material_stock_id
                        ,$_supplied
                        ,'0'
                        ,null
                        ,null
                        ,null
                        ,null
                        ,null
                        ,$remark_mutasi
                        ,auth::user()->name
                        ,auth::user()->id
                        ,$new_type_stock_erp_code
                        ,$new_type_stock
                        ,false
                        ,true);
                        
                        DetailMaterialStock::FirstOrCreate([
                            'material_stock_id'     => $new_material_stock->id,
                            'remark'                => $remark_mutasi,
                            'uom'                   => $new_material_stock->uom,
                            'qty'                   => $_supplied,
                            'locator_id'            => $material_stock->locator_id,
                            'user_id'               => Auth::user()->id,
                            'created_at'            => $upload_date,
                            'updated_at'            => $upload_date,
                        ]);

                    }else
                    {
                        $new_material_stock_id      = $is_exists->id;
                        $new_stock                  = sprintf('%0.8f',$is_exists->stock);
                        $old_available_qty_curr     = sprintf('%0.8f',$is_exists->available_qty);
                        $new_reserved_qty           = sprintf('%0.8f',$is_exists->reserved_qty);
                        $_new_stock                 = sprintf('%0.8f',$new_stock + $_supplied);
                        $new_availabilty_qty        = sprintf('%0.8f',$_new_stock - $new_reserved_qty);

                        $is_exists->stock           = sprintf('%0.8f',$_new_stock);
                        $is_exists->available_qty   = sprintf('%0.8f',$new_availabilty_qty);
                        
                    
                        if($new_availabilty_qty > 0)
                        {
                            $is_exists->is_allocated    = false;
                            $is_exists->is_active       = true;
                        }

                        $_old_stock                 = ($old_type_stock)? $old_type_stock : '(-)';
                        $_new_type_stock            = ($new_type_stock)? $new_type_stock : '(-)';
                        $remark_mutasi              = 'DIPINDAHKAN DARI DARI TIPE '.$_old_stock.' KE TIPE '.$_new_type_stock;
                        
                        HistoryStock::approved($new_material_stock_id
                        ,$new_availabilty_qty
                        ,$old_available_qty_curr
                        ,null
                        ,null
                        ,null
                        ,null
                        ,null
                        ,$remark_mutasi
                        ,auth::user()->name
                        ,auth::user()->id
                        ,$new_type_stock_erp_code
                        ,$new_type_stock
                        ,false
                        ,true);
                        
                        $is_exists->save();

                        DetailMaterialStock::FirstOrCreate([
                            'material_stock_id'     => $is_exists->id,
                            'remark'                => $remark_mutasi,
                            'uom'                   => $is_exists->uom,
                            'qty'                   => $_supplied,
                            'locator_id'            => $is_exists->locator_id,
                            'user_id'               => Auth::user()->id,
                            'created_at'            => $upload_date,
                            'updated_at'            => $upload_date,
                        ]);
                    }

                    $curr_available_qty -= $_supplied;
                    
                    $new = $old_available - $_supplied;
                    if($new <= 0) $new = '0';
                    else $new = $new;

                    $_new_type_stock            = ($new_type_stock)? $new_type_stock : '(-)';
                    $remark_mutasi              = 'DI PINDAHKAN KE TIPE '. $_new_type_stock;
                    
                    HistoryStock::approved($id
                    ,$new
                    ,$old_available
                    ,null
                    ,null
                    ,null
                    ,null
                    ,null
                    ,$remark_mutasi
                    ,auth::user()->name
                    ,auth::user()->id
                    ,$old_type_stock_erp_code
                    ,$old_type_stock
                    ,false
                    ,true);
                    
                    $material_stock->reserved_qty   = sprintf('%0.8f',$cur_reserved_qty + $_supplied);
                    $material_stock->available_qty  = sprintf('%0.8f',$curr_available_qty);
                    if($curr_available_qty <= 0)
                    {
                        $material_stock->is_allocated   = true;
                        $material_stock->is_active      = false;
                    }

                    $material_stock->save();
                    
                }

                $locator = Locator::find($locator_id);
                if($locator)
                {
                    $counter_locator = MaterialStock::where([
                        ['locator_id',$locator_id],
                        ['is_allocated',false],
                        ['is_closing_balance',false],
                    ])
                    ->whereNull('deleted_at')
                    ->count();

                    $locator->counter_in = $counter_locator ;
                    $locator->save();
                }
                

            }

            DB::commit();
            return response()->json(200);
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    }

    public function allocatingStock($id)
    {
        //return view('errors.503');
        $material_stock_id      = $id;
        $material_stock         = MaterialStock::where([
            ['id',$material_stock_id],
            ['is_allocated',false],
            ['is_closing_balance',false],
            ['is_stock_on_the_fly',false],
        ])
        ->whereNull('last_status')
        ->whereNull('deleted_at')
        ->first();

        if(!auth::user()->hasRole(['admin-ict-fabric']))
        {
            $warehouse_user = auth::user()->warehouse;
            if($warehouse_user != $material_stock->warehouse_id)
            {
                return response()->json('You dont have permission to update this data',422);
            }

        }

        if($material_stock)
        {
            $material_stock->last_status    = 'prepared';
            $material_stock->save();

            $type_stock                     = strtoupper($material_stock->type_stock);
            $type_stock_erp_code            = $material_stock->type_stock_erp_code;
            $document_no                    = strtoupper($material_stock->document_no);
            $item_code                      = strtoupper($material_stock->item_code);
            $po_buyer_source                = $material_stock->po_buyer;
            $c_order_id                     = $material_stock->c_order_id;
            $supplier_name                  = strtoupper($material_stock->supplier_name);
            $c_bpartner_id                  = $material_stock->c_bpartner_id;
            $_supplier                      = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
            $supplier_code                  = ($_supplier)? $_supplier->supplier_code : 'FREE STOCK';
            $warehouse_id                   = $material_stock->warehouse_id;
            $reserved_qty                   = sprintf('%0.8f',$material_stock->reserved_qty);
            $available_qty                  = sprintf('%0.8f',$material_stock->available_qty);
            $is_bom_exists                  = 0;

            $system                         = User::where([
                ['name','system'],
                ['warehouse',$warehouse_id]
            ])
            ->first();
            
            $auto_allocations = AutoAllocation::where([
                [db::raw('upper(document_no)'),$document_no],
                [db::raw('upper(item_code_source)'),$item_code],
                ['c_bpartner_id',$c_bpartner_id],
                ['warehouse_id',$warehouse_id],
                ['is_fabric',false],
                ['status_po_buyer','active'],
                ['qty_outstanding','>',0],
            ]);
            //if($type_stock) $auto_allocations = $auto_allocations->where('type_stock',$type_stock);
            //else  $auto_allocations = $auto_allocations->whereNull('type_stock');

            $auto_allocations = $auto_allocations->orderby('promise_date','asc')
            ->orderby('qty_allocation','asc')
            ->whereNull('deleted_at')
            ->get();
        
            
            if($available_qty > 0)
            {
                //dd($auto_allocations).'<br/>';
                foreach ($auto_allocations as $key_2 => $auto_allocation) 
                {
                    $auto_allocation_id     = $auto_allocation->id;
                    $item_id_book           = $auto_allocation->item_id_book;
                    $item_id_source         = $auto_allocation->item_id_source;
                    $item_code_source       = $auto_allocation->item_code_source;
                    $_item_code_book        = $auto_allocation->item_code;
                    
                    if($auto_allocation->is_upload_manual)
                    {
                        $allocation_source  = 'ALLOCATION MANUAL';
                        $user               = $auto_allocation->user_id;
                        $username           = strtoupper($auto_allocation->user->name);
                        $is_integrate       = false;
                    }else
                    {
                        $allocation_source  = ($auto_allocation->is_allocation_purchase)? 'ALLOCATION PURCHASE' : 'ALLOCATION ERP';
                        $user               = $system->id;
                        $username           = strtoupper($auto_allocation->user->name);
                        $is_integrate       = true;
                        
                    }

                    $pos                    = strpos($_item_code_book, '|');
                    if ($pos === false)
                    {
                        $item_code_book     = $_item_code_book;
                        $_article_no        = null;
                    }else 
                    {
                        $split              = explode('|',$_item_code_book);
                        $item_code_book     = $split[0];
                        $_article_no        = $split[1];
                    }
                    
                    $po_buyer               = $auto_allocation->po_buyer;
                    $qty_outstanding        = sprintf('%0.8f',$auto_allocation->qty_outstanding);
                    $qty_allocated          = sprintf('%0.8f',$auto_allocation->qty_allocated);
                        
                    $material_requirements  = MaterialRequirement::where([
                        ['po_buyer',$po_buyer],
                        [db::raw('upper(item_code)'),$item_code_book],
                    ]);

                    if($_article_no) $material_requirements  = $material_requirements->where('article_no',$_article_no);

                    $material_requirements  = $material_requirements->orderby('article_no','asc')
                    ->orderby('style','asc')
                    ->get();

                    $count_mr               = count($material_requirements);
                    echo $po_buyer.' '.$item_code_book.' '.$count_mr.'<br/>';
                    if($count_mr > 0)
                    {
                        if ($available_qty/$qty_outstanding >= 1) $_supply  = $qty_outstanding;
                        else $_supply   = $available_qty;
                        
                        
                        $qty_supply     = $_supply;
                        if($qty_outstanding > 0)
                        {
                            foreach ($material_requirements as $key => $material_requirement) 
                            {
                                $lc_date        = $material_requirement->lc_date;
                                $month_lc       = $material_requirement->lc_date->format('m');
                                $item_desc      = strtoupper($material_requirement->item_desc);
                                $uom            = $material_requirement->uom;
                                $category       = $material_requirement->category;
                                $style          = $material_requirement->style;
                                $job_order      = $material_requirement->job_order;
                                $article_no     = $material_requirement->article_no;
                                $qty_required   = sprintf('%0.8f',$material_requirement->qty_required - $material_requirement->qtyAllocated($po_buyer,$item_code,$style,$article_no));
                                
                                //if($month_lc <= '02') $qty_required = sprintf('%0.8f',$material_requirement->qty_required);
                                //else $qty_required = sprintf('%0.8f',$material_requirement->qty_required);
    
                                //echo $po_buyer.' '.$style.' '.$qty_supply.' '.$material_requirement->qty_required.' '.$qty_required.'<br/>';
                                //echo $po_buyer.' '.$item_code.' '.$qty_supply.' '.$qty_required.'</br>';
                                
                                if(($key+1) != $count_mr) $__supplied = sprintf('%0.8f',$qty_required);
                                else $__supplied = sprintf('%0.8f',$qty_supply);
                                
                                if($__supplied > 0 && $qty_supply > 0)
                                {
                                    if ($qty_supply/$__supplied >= 1) $_supplied = $__supplied;
                                    else $_supplied = $qty_supply;
    
                                    $__supplied     = sprintf('%0.8f',$_supplied);
                                 
                                    $is_exists = AllocationItem::where([
                                        ['material_stock_id',$material_stock_id],
                                        ['auto_allocation_id',$auto_allocation_id],
                                        ['item_code',$item_code_book],
                                        ['article_no',$article_no],
                                        ['style',$style],
                                        ['po_buyer',$po_buyer],
                                        ['qty_booking',$__supplied],
                                        ['is_additional',false],
                                        ['warehouse',$warehouse_id]
                                    ])
                                    ->where(function($query){
                                        $query->where('confirm_by_warehouse','approved')
                                        ->OrWhereNull('confirm_by_warehouse');
                                    })
                                    ->whereNull('deleted_at')
                                    ->exists();
                                    //echo $po_buyer.' '.$item_code.' '.$is_exists.'</br>';
                            
                                    if(!$is_exists && $_supplied > 0)
                                    {
                                        AllocationItem::FirstOrCreate([
                                            'material_stock_id'         => $material_stock_id,
                                            'auto_allocation_id'        => $auto_allocation_id,
                                            'lc_date'                   => $lc_date,
                                            'c_order_id'                => $c_order_id,
                                            'item_id_book'              => $item_id_book,
                                            'item_id_source'            => $item_id_source,
                                            'c_bpartner_id'             => $c_bpartner_id,
                                            'supplier_code'             => $supplier_code,
                                            'supplier_name'             => $supplier_name,
                                            'document_no'               => $document_no,
                                            'item_code'                 => $item_code_book,
                                            'item_code_source'          => $item_code_source,
                                            'item_desc'                 => $item_desc,
                                            'category'                  => $category,
                                            'po_buyer_source'           => $po_buyer_source,
                                            'uom'                       => $uom,
                                            'job'                       => $job_order,
                                            'style'                     => $style,
                                            'article_no'                => $article_no,
                                            'po_buyer'                  => $po_buyer,
                                            'warehouse'                 => $warehouse_id, //  JIKA ACC INI ADALAH WAREHOUSE INVENTORY
                                            'qty_booking'               => $__supplied,
                                            'is_additional'             => false,
                                            'is_backlog'                => false,
                                            'user_id'                   => $user,
                                            'confirm_by_warehouse'      => 'approved',
                                            'confirm_date'              => Carbon::now(),
                                            'remark'                    => 'AUTO ALLOCATION',
                                            'confirm_user_id'           => $user,
                                            'deleted_at'                => null
                                        ]);
    
                                        $qty_supply         -= $_supplied;
                                        $qty_required       -= $_supplied;
                                        $qty_outstanding    -= $_supplied;
                                        $qty_allocated      += $_supplied;

                                        $old = $available_qty;
                                        $new = $available_qty - $_supplied;

                                        if($new <= 0) $new = '0';
                                        else $new = $new;

                                        HistoryStock::approved($material_stock_id
                                        ,$new
                                        ,$old
                                        ,$__supplied
                                        ,$po_buyer
                                        ,$style
                                        ,$article_no
                                        ,$lc_date
                                        ,$allocation_source
                                        ,$username
                                        ,$user
                                        ,$type_stock_erp_code
                                        ,$type_stock
                                        ,$is_integrate,
                                        true);

                                        $available_qty      -= $_supplied;
                                        $reserved_qty       += $_supplied;
                                        $is_bom_exists++;
                                    }
                                }
    
                            }
                        }
                        
                        //$available_qty -= $_supply;
                        //$reserved_qty += $_supply;
    
                        if($qty_outstanding<=0)
                        {
                            $auto_allocation->is_already_generate_form_booking  = true;
                            $auto_allocation->generate_form_booking             = carbon::now();
                        }
                        
                        $auto_allocation->qty_outstanding                       = sprintf('%0.8f',$qty_outstanding);
                        $auto_allocation->qty_allocated                         = sprintf('%0.8f',$qty_allocated);
                        $auto_allocation->save();
                    }
                }
                
            }

            if($is_bom_exists > 0)
            {
                if($available_qty <=0 )
                {
                    $material_stock->is_allocated       = true;
                    $material_stock->is_active          = false;
                    $locator                            = Locator::find($material_stock->locator_id);
                    
                    if($locator)
                    {
                        $counter                        = $locator->counter_in;
                        $locator->counter_in            = $counter -1 ;
                        $locator->save();
                    }

                }
                
                $material_stock->available_qty          = sprintf('%0.8f',$available_qty);
                $material_stock->reserved_qty           = sprintf('%0.8f',$reserved_qty);
               
            }

            $material_stock->last_status                = null;
            $material_stock->save();
        }

        //return response()->json(200);
    }

    public function changeTypeBulk()
    {
        return view('fabric_report_material_stock_material_management.change_type_bulk');
    }

    public function exportFormImportChangeTypeBulk()
    {
        return Excel::create('change_material_stock',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','MATERIAL_STOCK_ID');
                $sheet->setCellValue('B1','TYPE_STOCK_NEW');
                $sheet->setCellValue('C1','QTY_CHANGE');
                $sheet->setCellValue('D1','REASON');
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                    'C' => '@',
                ));
            });
            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }

    public function storeChangeTypeBulk(request $request)
    {
        $array = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                $upload_date                = Carbon::now()->toDateTimeString();
                try 
                {
                    DB::beginTransaction();
                    
                    foreach ($data as $key => $value) 
                    {
                        $material_stock_id  = $value->material_stock_id;
                        $type_stock_new     = strtoupper($value->type_stock_new);
                        $reason             = strtoupper($value->reason);
                        $qty_change         =  sprintf('%0.8f',$value->qty_change);
                        
                        if($type_stock_new == 'SLT') $type_stock_erp_code_new = '1';
                        else if($type_stock_new == 'REGULER') $type_stock_erp_code_new = '2';
                        else if($type_stock_new == 'PR/SR' || $type_stock_new == 'PR') $type_stock_erp_code_new = '3';
                        else if($type_stock_new == 'MTFC') $type_stock_erp_code_new = '4';
                        else if($type_stock_new == 'NB') $type_stock_erp_code_new = '5';
                        else if($type_stock_new == 'ALLOWANCE') $type_stock_erp_code_new = '6';
                        else $type_stock_erp_code_new = null;
                        
                        if($material_stock_id)
                        {
                            if($reason)
                            {
                                if($type_stock_erp_code_new)
                                {
                                    $id                         =  $material_stock_id; 
                                    $material_stock             = MaterialStock::find($id);
                                
                                    $new_type_stock             = $type_stock_new;
                                    $qty_change                 = $qty_change;

                                    $old_type_stock             = $material_stock->type_stock;
                                    $old_type_stock_erp_code    = $material_stock->type_stock_erp_code;
                                    $locator_id                 = $material_stock->locator_id;
                                    
                                    
                                        
                                    if($old_type_stock != $new_type_stock)
                                    {
                                        $old_available          = sprintf('%0.8f',$material_stock->available_qty);
                                        $curr_available_qty     = sprintf('%0.8f',$material_stock->available_qty);
                                        $cur_reserved_qty       = sprintf('%0.8f',$material_stock->reserved_qty);

                                        if ($qty_change/$curr_available_qty >= 1) $_supplied = $curr_available_qty;
                                        else $_supplied = $qty_change;

                                        $mapping_stock              = MappingStocks::where('type_stock',$new_type_stock)->first();
                                        $new_type_stock_erp_code    = ($mapping_stock)? $mapping_stock->type_stock_erp_code : null;

                                        if($_supplied > 0 && $curr_available_qty > 0)
                                        {
                                            $date_now = carbon::now();
                                            //insert new allocataion
                                            AllocationItem::FirstOrCreate([
                                                'material_stock_id'         => $material_stock->id,
                                                'c_order_id'                => $material_stock->c_order_id,
                                                'document_no'               => $material_stock->document_no,
                                                'item_id_source'            => $material_stock->item_id_source,
                                                'item_id_book'              => $material_stock->item_id,
                                                'item_code'                 => $material_stock->item_code,
                                                'item_code_source'          => $material_stock->item_code,
                                                'item_desc'                 => $material_stock->item_desc,
                                                'category'                  => $material_stock->category,
                                                'uom'                       => $material_stock->uom,
                                                'warehouse'                 => auth::user()->warehouse,
                                                'qty_booking'               => sprintf('%0.8f',$_supplied),
                                                'is_not_allocation'         => true,
                                                'user_id'                   => auth::user()->id,
                                                'confirm_user_id'           => auth::user()->id,
                                                'confirm_by_warehouse'      => 'approved',
                                                'confirm_date'              => carbon::now(),
                                                'created_at'                => $date_now,
                                                'remark'                    => 'DIPINDAHKAN KE TIPE STOCK '.$new_type_stock.' '.$reason,
                                            ]);

                                        
                                            //insert new stock
                                            $is_exists = MaterialStock::whereNull('deleted_at')
                                            ->where([
                                                [db::raw("upper(document_no)"),$material_stock->document_no],
                                                [db::raw('upper(item_code)'),strtoupper($material_stock->item_code)],
                                                ['type_po', 2],
                                                ['uom',$material_stock->uom],
                                                ['locator_id',$locator_id],
                                                ['warehouse_id',$material_stock->warehouse_id],
                                                ['is_material_others',$material_stock->is_material_others],
                                                ['is_closing_balance',false],
                                                ['type_stock',$new_type_stock],
                                            ])
                                            ->whereNotNull('approval_date');

                                            if($material_stock->po_detail_id) $is_exists = $is_exists->where('po_detail_id',$material_stock->po_detail_id);
                                            else $is_exists = $is_exists->whereNull('po_detail_id');

                                            if($material_stock->c_bpartner_id || $material_stock->c_bpartner_id != '') $is_exists = $is_exists->where('c_bpartner_id',$material_stock->c_bpartner_id);
                                            else $is_exists = $is_exists->whereNull('c_bpartner_id');

                                            if($material_stock->c_order_id || $material_stock->c_order_id != '') $is_exists = $is_exists->where('c_order_id',$material_stock->c_order_id);
                                            else $is_exists = $is_exists->whereNull('c_order_id');

                                            if($material_stock->po_buyer || $material_stock->po_buyer != '') $is_exists = $is_exists->where('po_buyer',$material_stock->po_buyer);
                                            else $is_exists = $is_exists->whereNull('po_buyer');

                                            if($material_stock->source || $material_stock->source != '') $is_exists = $is_exists->where('source',$material_stock->source);
                                            else $is_exists = $is_exists->whereNull('source');

                                            $is_exists = $is_exists->first();
                                            
                                            if(!$is_exists)
                                            {
                                                $new_material_stock = MaterialStock::FirstOrCreate([
                                                    'po_detail_id'          => $material_stock->po_detail_id,
                                                    'document_no'           => $material_stock->document_no,
                                                    'supplier_code'         => $material_stock->supplier_code,
                                                    'supplier_name'         => $material_stock->supplier_name,
                                                    'c_bpartner_id'         => $material_stock->c_bpartner_id,
                                                    'c_order_id'            => $material_stock->c_order_id,
                                                    'locator_id'            => $locator_id,
                                                    'item_id'               => $material_stock->item_id,
                                                    'item_code'             => $material_stock->item_code,
                                                    'item_desc'             => $material_stock->item_desc,
                                                    'category'              => $material_stock->category,
                                                    'type_po'               => $material_stock->type_po,
                                                    'warehouse_id'          => $material_stock->warehouse_id,
                                                    'uom'                   => $material_stock->uom,
                                                    'po_buyer'              => $material_stock->po_buyer,
                                                    'qty_carton'            => $material_stock->qty_carton,
                                                    'stock'                 => sprintf('%0.8f',$_supplied),
                                                    'reserved_qty'          => 0,
                                                    'available_qty'         => sprintf('%0.8f',$_supplied),
                                                    'is_active'             =>  $material_stock->is_active,
                                                    'is_material_others'    => $material_stock->is_material_others,
                                                    'upc_item'              => $material_stock->upc_item,
                                                    'source'                => $material_stock->source,
                                                    'type_stock_erp_code'   => $new_type_stock_erp_code,
                                                    'type_stock'            => $new_type_stock,
                                                    'source'                => $material_stock->source,
                                                    'user_id'               => $material_stock->user_id
                                                ]);

                                                $new_material_stock_id      = $new_material_stock->id;
                                                $_old_stock                 = ($old_type_stock)? $old_type_stock : '(-)';
                                                $_new_type_stock            = ($new_type_stock)? $new_type_stock : '(-)';
                                                $remark_mutasi              = 'DIPINDAHKAN DARI DARI TIPE '.$_old_stock.' KE TIPE '.$_new_type_stock.' '.$reason;
                                            
                                                HistoryStock::approved($new_material_stock_id
                                                ,$_supplied
                                                ,'0'
                                                ,null
                                                ,null
                                                ,null
                                                ,null
                                                ,null
                                                ,$remark_mutasi
                                                ,auth::user()->name
                                                ,auth::user()->id
                                                ,$new_type_stock_erp_code
                                                ,$new_type_stock
                                                ,false
                                                ,true);
                                                
                                                DetailMaterialStock::FirstOrCreate([
                                                    'material_stock_id'     => $new_material_stock->id,
                                                    'remark'                => $remark_mutasi,
                                                    'uom'                   => $new_material_stock->uom,
                                                    'qty'                   => $_supplied,
                                                    'locator_id'            => $material_stock->locator_id,
                                                    'user_id'               => Auth::user()->id,
                                                    'created_at'            => $upload_date,
                                                    'updated_at'            => $upload_date,
                                                ]);

                                            }else
                                            {
                                                $new_material_stock_id      = $is_exists->id;
                                                $new_stock                  = sprintf('%0.8f',$is_exists->stock);
                                                $old_available_qty_curr     = sprintf('%0.8f',$is_exists->available_qty);
                                                $new_reserved_qty           = sprintf('%0.8f',$is_exists->reserved_qty);
                                                $_new_stock                 = sprintf('%0.8f',$new_stock + $_supplied);
                                                $new_availabilty_qty        = sprintf('%0.8f',$_new_stock - $new_reserved_qty);

                                                $is_exists->stock           = sprintf('%0.8f',$_new_stock);
                                                $is_exists->available_qty   = sprintf('%0.8f',$new_availabilty_qty);
                                                
                                            
                                                if($new_availabilty_qty > 0)
                                                {
                                                    $is_exists->is_allocated    = false;
                                                    $is_exists->is_active       = true;
                                                }

                                                $_old_stock                 = ($old_type_stock)? $old_type_stock : '(-)';
                                                $_new_type_stock            = ($new_type_stock)? $new_type_stock : '(-)';
                                                $remark_mutasi              = 'DIPINDAHKAN DARI DARI TIPE '.$_old_stock.' KE TIPE '.$_new_type_stock.' '.$reason;
                                                
                                                HistoryStock::approved($new_material_stock_id
                                                ,$new_availabilty_qty
                                                ,$old_available_qty_curr
                                                ,null
                                                ,null
                                                ,null
                                                ,null
                                                ,null
                                                ,$remark_mutasi
                                                ,auth::user()->name
                                                ,auth::user()->id
                                                ,$new_type_stock_erp_code
                                                ,$new_type_stock
                                                ,false
                                                ,true);
                                                
                                                $is_exists->save();

                                                DetailMaterialStock::FirstOrCreate([
                                                    'material_stock_id'     => $is_exists->id,
                                                    'remark'                => $remark_mutasi,
                                                    'uom'                   => $is_exists->uom,
                                                    'qty'                   => $_supplied,
                                                    'locator_id'            => $is_exists->locator_id,
                                                    'user_id'               => Auth::user()->id,
                                                    'created_at'            => $upload_date,
                                                    'updated_at'            => $upload_date,
                                                ]);
                                            }

                                            $curr_available_qty -= $_supplied;
                                            
                                            $new = $old_available - $_supplied;
                                            if($new <= 0) $new = '0';
                                            else $new = $new;

                                            $_new_type_stock            = ($new_type_stock)? $new_type_stock : '(-)';
                                            $remark_mutasi              = 'DI PINDAHKAN KE TIPE '. $_new_type_stock.' '.$reason;
                                            
                                            HistoryStock::approved($id
                                            ,$new
                                            ,$old_available
                                            ,null
                                            ,null
                                            ,null
                                            ,null
                                            ,null
                                            ,$remark_mutasi
                                            ,auth::user()->name
                                            ,auth::user()->id
                                            ,$old_type_stock_erp_code
                                            ,$old_type_stock
                                            ,false
                                            ,true);
                                            
                                            $material_stock->reserved_qty   = sprintf('%0.8f',$cur_reserved_qty + $_supplied);
                                            $material_stock->available_qty  = sprintf('%0.8f',$curr_available_qty);
                                            if($curr_available_qty <= 0)
                                            {
                                                $material_stock->is_allocated   = true;
                                                $material_stock->is_active      = false;
                                            }

                                            $material_stock->save();
                                            
                                        }

                                        $locator = Locator::find($locator_id);
                                        if($locator)
                                        {
                                            $counter_locator = MaterialStock::where([
                                                ['locator_id',$locator_id],
                                                ['is_allocated',false],
                                                ['is_closing_balance',false],
                                            ])
                                            ->whereNull('deleted_at')
                                            ->count();

                                            $locator->counter_in = $counter_locator ;
                                            $locator->save();
                                        }
                                        

                                    }

                                    $obj                    = new stdClass();
                                    $obj->supplier_name     = $material_stock->supplier_name;
                                    $obj->document_no       = $material_stock->document_no;
                                    $obj->item_code         = $material_stock->item_code;
                                    $obj->uom               = $material_stock->uom;
                                    $obj->qty_available     = $qty_change;
                                    $obj->warehouse_name    = $material_stock->warehouse_id;
                                    $obj->reason            = $reason;
                                    $obj->error_upload      = false;
                                    $obj->remark            = 'success';
                                    $array [] = $obj;
                                        
                                        
                                }else
                                {
                                    $obj                    = new stdClass();
                                    $obj->supplier_name     = null;
                                    $obj->document_no       = null;
                                    $obj->item_code         = null;
                                    $obj->uom               = null;
                                    $obj->qty_available     = null;
                                    $obj->warehouse_name    = null;
                                    $obj->reason            = $reason;
                                    $obj->error_upload      = true;
                                    $obj->remark            = 'ERROR, TYPE STOCK NOT FOUND';
                                    $array [] = $obj;
                                }
                            }else
                            {
                                $obj                    = new stdClass();
                                $obj->supplier_name     = null;
                                $obj->document_no       = null;
                                $obj->item_code         = null;
                                $obj->uom               = null;
                                $obj->qty_available     = null;
                                $obj->warehouse_name    = null;
                                $obj->reason            = $reason;
                                $obj->error_upload      = true;
                                $obj->remark            = 'ERROR, SILAHKAN MASUKAN ALASANNYA';
                                $array [] = $obj;
                            }
                            
                        }else
                        {
                            $obj                    = new stdClass();
                            $obj->supplier_name     = null;
                            $obj->document_no       = null;
                            $obj->item_code         = null;
                            $obj->uom               = null;
                            $obj->qty_available     = null;
                            $obj->warehouse_name    = null;
                            $obj->reason            = $reason;
                            $obj->error_upload      = true;
                            $obj->remark            = 'ERROR, ID TIDAK TIDAK DITEMUKAN';
                            $array [] = $obj;
                        }
                        
                    }

                DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
                
            }
        }

        return response()->json($array,'200');
    }

    public function editBulk()
    {
        return view('fabric_report_material_stock_material_management.edit_bulk');
    }

    public function exportFormImportEditBulk()
    {
        return Excel::create('edit_material_stock',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','MATERIAL_STOCK_ID');
                $sheet->setCellValue('B1','NEW_AVAILABLE_QTY_STOCK');
                $sheet->setCellValue('C1','UPDATE_REASON');
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                    'C' => '@',
                ));
            });
            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }

    public function storeEditBulk(request $request)
    {
        $array = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                try 
                {
                    DB::beginTransaction();

                    foreach ($data as $key => $value) 
                    {
                        $material_stock_id       = $value->material_stock_id;
                        $new_available_qty_stock = strtoupper($value->new_available_qty_stock);
                        $reason                  = strtoupper($value->update_reason);
                        
                        if($material_stock_id)
                        {
                            
                            if($reason)
                            {
                                    $material_stock             = MaterialStock::find($material_stock_id);
                                    $curr_type_stock            = $material_stock->type_stock;
                                    $curr_type_stock_erp_code   = $material_stock->type_stock_erp_code;
                                    $curr_available_qty         = sprintf('%0.8f',$material_stock->available_qty);
                                    $locator_id                 = $material_stock->locator_id;
                                    $operator                   = $new_available_qty_stock - $curr_available_qty;
                                    $remark                     = trim(strtoupper($request->update_reason));
                                    $upload_date                = Carbon::now()->toDateTimeString();



                                    if($operator > 0)
                                    {
                                        $new_stock = ($material_stock->stock + $operator);
                                        $material_stock->stock = sprintf('%0.8f',$new_stock);

                                        DetailMaterialStock::FirstOrCreate([
                                            'material_stock_id'     => $material_stock->id,
                                            'remark'                => $remark,
                                            'uom'                   => $material_stock->uom,
                                            'qty'                   => $operator,
                                            'locator_id'            => $material_stock->locator_id,
                                            'user_id'               => Auth::user()->id,
                                            'created_at'            => $upload_date,
                                            'updated_at'            => $upload_date,
                                        ]);
                                    }else
                                    {
                                        $new_reserved_qty = $material_stock->reserved_qty + (-1*$operator);
                                        AllocationItem::FirstOrCreate([
                                            'material_stock_id'         => $material_stock->id,
                                            'c_order_id'                => $material_stock->c_order_id,
                                            'document_no'               => $material_stock->document_no,
                                            'item_id_source'            => $material_stock->item_id_source,
                                            'item_id_book'              => $material_stock->item_id,
                                            'item_code'                 => $material_stock->item_code,
                                            'item_code_source'          => $material_stock->item_code,
                                            'item_desc'                 => $material_stock->item_desc,
                                            'category'                  => $material_stock->category,
                                            'uom'                       => $material_stock->uom,
                                            'warehouse'                 => auth::user()->warehouse,
                                            'qty_booking'               => sprintf('%0.8f',(-1*$operator)),
                                            'is_not_allocation'         => true,
                                            'user_id'                   => auth::user()->id,
                                            'confirm_user_id'           => auth::user()->id,
                                            'confirm_by_warehouse'      => 'approved',
                                            'confirm_date'              => carbon::now(),
                                            'remark'                    => $remark,
                                        ]);

                                        $material_stock->reserved_qty   = sprintf('%0.8f',$new_reserved_qty);
                                        
                                    }

                                    $material_stock->available_qty      = sprintf('%0.8f',$new_available_qty_stock);
                                    if($new_available_qty_stock <= 0)
                                    {
                                        $material_stock->is_allocated   = true;
                                        $material_stock->is_active      = false;
                                    }else
                                    {
                                        $material_stock->is_allocated   = false;
                                        $material_stock->is_active      = true;
                                    }

                                    HistoryStock::approved($material_stock_id
                                    ,$new_available_qty_stock
                                    ,$curr_available_qty
                                    ,null
                                    ,null
                                    ,null
                                    ,null
                                    ,null
                                    ,null
                                    ,auth::user()->name
                                    ,auth::user()->id
                                    ,$curr_type_stock_erp_code
                                    ,$curr_type_stock
                                    ,false
                                    ,false);
                                                
                                    $count_item_on_locator = MaterialStock::where([
                                        ['locator_id',$locator_id],
                                        ['is_allocated',false],
                                        ['is_closing_balance',false],
                                    ])
                                    ->whereNull('deleted_at')
                                    ->count();

                                    $locator = Locator::find($locator_id);
                                    if($locator)
                                    {
                                        $locator->counter_in = $count_item_on_locator;
                                        $locator->save();
                                    }

                                    $material_stock->save();
                                
                                        $obj                    = new stdClass();
                                        $obj->supplier_name     = $material_stock->supplier_name;
                                        $obj->document_no       = $material_stock->document_no;
                                        $obj->item_code         = $material_stock->item_code;
                                        $obj->uom               = $material_stock->uom;
                                        $obj->qty_available     = $curr_available_qty;
                                        $obj->warehouse_name    = ($material_stock->warehouse_id == '1000013') ? 'WAREHOUSE ACC AOI 2': 'WAREHOUSE ACC AOI 1';
                                        $obj->reason            = $reason;
                                        $obj->error_upload      = false;
                                        $obj->remark            = 'DATA BERHASIL DI EDIT';
                                        $array [] = $obj;
                            }else
                            {
                                $obj                    = new stdClass();
                                $obj->supplier_name     = null;
                                $obj->document_no       = null;
                                $obj->item_code         = null;
                                $obj->uom               = null;
                                $obj->qty_available     = null;
                                $obj->warehouse_name    = null;
                                $obj->reason            = $reason;
                                $obj->error_upload      = true;
                                $obj->remark            = 'ERROR, SILAHKAN ISI ALASANNYA TERLEBIH DAHULU';
                                $array [] = $obj;
                            }
                        }else
                        {
                            $obj                    = new stdClass();
                            $obj->supplier_name     = null;
                            $obj->document_no       = null;
                            $obj->item_code         = null;
                            $obj->uom               = null;
                            $obj->qty_available     = null;
                            $obj->warehouse_name    = null;
                            $obj->reason            = $reason;
                            $obj->error_upload      = true;
                            $obj->remark            = 'ERROR, ID TIDAK TIDAK DITEMUKAN';
                            $array [] = $obj;
                        }
                        
                    }
                
                    DB::commit();
                }catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
                    
            }
        }

        return response()->json($array,'200');
    }

    public function deleteBulk()
    {
        return view('fabric_report_material_stock_material_management.delete_bulk');
    }

    public function exportFormImportDeleteBulk()
    {
        return Excel::create('delete_material_stock',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','MATERIAL_STOCK_ID');
                $sheet->setCellValue('B1','REASON');
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                ));
            });
            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }

    public function storeDeleteBulk(request $request)
    {
        $array = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                try 
                {
                    DB::beginTransaction();

                    foreach ($data as $key => $value) 
                    {
                        $material_stock_id  = $value->material_stock_id;
                        $reason             = strtoupper($value->reason);
                        
                        if($material_stock_id)
                        {
                            
                            if($reason)
                            {
                                try
                                {
                                    db::beginTransaction();
                                    $material_stock     = MaterialStock::find($material_stock_id);
                                    $old_available_qty  = $material_stock->available_qty;

                                    if($material_stock->is_closing_balance == false)
                                    {
                                        AllocationItem::FirstOrCreate([
                                            'material_stock_id'         => $material_stock->id,
                                            'c_order_id'                => $material_stock->c_order_id,
                                            'document_no'               => $material_stock->document_no,
                                            'item_id_source'            => $material_stock->item_id_source,
                                            'item_id_book'              => $material_stock->item_id,
                                            'item_code'                 => $material_stock->item_code,
                                            'item_code_source'          => $material_stock->item_code,
                                            'item_desc'                 => $material_stock->item_desc,
                                            'category'                  => $material_stock->category,
                                            'uom'                       => $material_stock->uom,
                                            'warehouse'                 => auth::user()->warehouse,
                                            'qty_booking'               => sprintf('%0.8f',$material_stock->available_qty),
                                            'is_not_allocation'         => true,
                                            'user_id'                   => auth::user()->id,
                                            'confirm_user_id'           => auth::user()->id,
                                            'confirm_by_warehouse'      => 'approved',
                                            'confirm_date'              => carbon::now(),
                                            'remark'                    => 'STOCK DI HAPUS, '.$reason,
                                        ]);
                        
                                        $material_stock->reserved_qty       = sprintf('%0.8f',$material_stock->available_qty + $material_stock->reserved_qty);
                                        $material_stock->available_qty      = 0;
                                        $material_stock->is_allocated       = true;
                                        $material_stock->is_closing_balance = true;
                                        $material_stock->is_active          = false;
                        
                                        HistoryStock::approved($material_stock->id
                                        ,'0'
                                        ,$old_available_qty
                                        ,null
                                        ,null
                                        ,null
                                        ,null
                                        ,null
                                        ,'DELETE STOCK'
                                        ,auth::user()->name
                                        ,auth::user()->id
                                        ,$material_stock->type_stock_erp_code
                                        ,$material_stock->type_stock
                                        ,false
                                        ,true);
                        
                                        $material_stock->save();

                                        $obj                    = new stdClass();
                                        $obj->supplier_name     = $material_stock->supplier_name;
                                        $obj->document_no       = $material_stock->document_no;
                                        $obj->item_code         = $material_stock->item_code;
                                        $obj->uom               = $material_stock->uom;
                                        $obj->qty_available     = $old_available_qty;
                                        $obj->warehouse_name    = ($material_stock->warehouse_id == '1000013') ? 'WAREHOUSE ACC AOI 2': 'WAREHOUSE ACC AOI 1';
                                        $obj->reason            = $reason;
                                        $obj->error_upload      = false;
                                        $obj->remark            = 'DATA BERHASIL DI HAPUS';
                                        $array [] = $obj;
                                    }else
                                    {
                                        $obj                    = new stdClass();
                                        $obj->supplier_name     = $material_stock->supplier_name;
                                        $obj->document_no       = $material_stock->document_no;
                                        $obj->item_code         = $material_stock->item_code;
                                        $obj->uom               = $material_stock->uom;
                                        $obj->qty_available     = $material_stock->qty_available;
                                        $obj->warehouse_name    = ($material_stock->warehouse_id == '1000013') ? 'WAREHOUSE ACC AOI 2': 'WAREHOUSE ACC AOI 1';
                                        $obj->reason            = $reason;
                                        $obj->error_upload      = true;
                                        $obj->remark            = 'ERROR, ID SUDAH TIDAK AKTIF';
                                        $array [] = $obj;
                                    }
                                    db::commit();
                                }catch (Exception $ex){
                                    db::rollback();
                                    $message = $ex->getMessage();
                                    ErrorHandler::db($message);
                                }
                            }else
                            {
                                $obj                    = new stdClass();
                                $obj->supplier_name     = null;
                                $obj->document_no       = null;
                                $obj->item_code         = null;
                                $obj->uom               = null;
                                $obj->qty_available     = null;
                                $obj->warehouse_name    = null;
                                $obj->reason            = $reason;
                                $obj->error_upload      = true;
                                $obj->remark            = 'ERROR, SILAHKAN ISI ALASANNYA TERLEBIH DAHULU';
                                $array [] = $obj;
                            }
                        }else
                        {
                            $obj                    = new stdClass();
                            $obj->supplier_name     = null;
                            $obj->document_no       = null;
                            $obj->item_code         = null;
                            $obj->uom               = null;
                            $obj->qty_available     = null;
                            $obj->warehouse_name    = null;
                            $obj->reason            = $reason;
                            $obj->error_upload      = true;
                            $obj->remark            = 'ERROR, ID TIDAK TIDAK DITEMUKAN';
                            $array [] = $obj;
                        }
                        
                    }
                
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
                    
            }
        }

        return response()->json($array,'200');
    }

    public function export(Request $request)
    {
        if(auth::user()->hasRole(['admin-ict-fabric','mm-staff']))
        {
            $filename = 'report_stock_fabric.csv';
        }else
        {
            $warehouse = ($request->_warehouse_id ? $request->_warehouse_id : auth::user()->warehouse);
            if($warehouse == '1000011') $filename = 'report_stock_fabric_aoi2.csv';
            else if($warehouse == '1000001') $filename = 'report_stock_fabric_aoi1.csv';
        }
    
        $file = Config::get('storage.report') . '/' . $filename;
        if(!file_exists($file)) return 'File not found';
        
        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function exportAllocation(Request $request)
    {
        $allocations =  db::table('allocation_item_fabrics_v')->orderby('created_at','desc')->get();
        
        $filename = 'report_allocation_item_fabric';
        $file = Config::get('storage.report') . '/' . e($filename).'.csv';

        if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';
        
        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

}
