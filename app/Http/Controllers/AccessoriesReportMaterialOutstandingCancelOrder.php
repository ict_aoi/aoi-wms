<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;


class AccessoriesReportMaterialOutstandingCancelOrder extends Controller
{
    public function index()
    {
        return view('accessories_report_material_outstanding_cancel_order.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax())
        {
            $material_cancel_order = db::table('outstanding_scan_cancel_order_v')
            ->get();

            return DataTables::of($material_cancel_order)
            ->make(true);
        }
    }

    public function export(Request $request)
    { 

        $material_cancel_order = db::table('outstanding_scan_cancel_order_v')
                                ->get();

        $file_name = 'accessories_report_scan_cancel_order_v ';
        return Excel::create($file_name,function($excel) use ($material_cancel_order)
        {
            $excel->sheet('active',function($sheet)use($material_cancel_order)
            {
                $sheet->setCellValue('A1','BARCODE');
                $sheet->setCellValue('B1','PO_BUYER');
                $sheet->setCellValue('C1','ITEM_CODE');
                $sheet->setCellValue('D1','DOCUMENT_NO');
                $sheet->setCellValue('E1','STYLE');
                $sheet->setCellValue('F1','ARTICLE_NO');
                $sheet->setCellValue('G1','JOB_ORDER');
                $sheet->setCellValue('H1','UOM_CONVERTION');
                $sheet->setCellValue('I1','QTY');
                $sheet->setCellValue('J1','WAREHOUSE');
                $sheet->setCellValue('K1','LAST_STATUS_MOVEMENT');
                $sheet->setCellValue('L1','LOCATOR');

            $row=2;
            foreach ($material_cancel_order as $i) 
            {  
                $sheet->setCellValue('A'.$row,$i->barcode);
                $sheet->setCellValue('B'.$row,$i->po_buyer);
                $sheet->setCellValue('C'.$row,$i->item_code);
                $sheet->setCellValue('D'.$row,$i->document_no);
                $sheet->setCellValue('E'.$row,$i->style);
                $sheet->setCellValue('F'.$row,$i->article_no);
                $sheet->setCellValue('G'.$row,$i->job_order);
                $sheet->setCellValue('H'.$row,$i->uom_conversion);
                $sheet->setCellValue('I'.$row,$i->qty);
                $sheet->setCellValue('J'.$row,$i->warehouse);
                $sheet->setCellValue('K'.$row,$i->last_status_movement);
                $sheet->setCellValue('L'.$row,$i->locator);
                $row++;
            }
            });

        })
        ->export('xlsx');

    }

}
