<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Models\Temporary;
use App\Models\MaterialStock;
use App\Models\AllocationItem;
use App\Models\AutoAllocation;
use App\Models\MaterialStockPerLot;
use App\Models\MonitoringReceivingFabric;
use App\Models\MovementStockHistory;
use App\Models\DetailMaterialPlanningFabric;

use App\Http\Controllers\FabricMaterialStockController as HistoryStockPerLot;
use App\Http\Controllers\FabricMaterialPlanningController as PlanningFabric;

class FabricMaterialInspectLabController extends Controller
{
    public function index()
    {
      return view('fabric_material_inspect_lab.index');
    }

    public function create(request $request)
    {
      $warehouse_id     = $request->warehouse_id;
      $_barcode         = strtoupper(trim($request->barcode));
      $total            = 0;

      $material_stock   = MaterialStock::where([
          ['barcode_supplier', $_barcode],
          ['warehouse_id', $warehouse_id],
      ])
      ->whereNull('deleted_at')
      ->first();

      if($material_stock)
      {
        if($material_stock->inspect_lot_result == 'REJECT') return response()->json('This barcode '.$material_stock->barcode_supplier.' is rejected if you want to change the status, please contact ICT', 422);
        if($material_stock->inspect_lot_result == 'RELEASE') return response()->json('This barcode '.$material_stock->barcode_supplier.' is release if you want to change the status, please contact ICT', 422);
        //if($material_stock->user_lab_id) return response()->json('This barcode already inspected by '.$material_stock->userInspectLab->name.' at '.$material_stock->inspect_lab_date->format('d/m/Y h:i:s'), 422);
        if($material_stock->po_detail_id = null || $material_stock->po_detail_id = '') return response()->json('Please contact ICT',422);
        
        $obj                        = new StdClass();
        $obj->check_all             = false;
        $obj->material_stock_id     = $material_stock->id;
        $obj->material_arrival_id   = $material_stock->material_arrival_id;
        $obj->barcode               = $material_stock->barcode_supplier;
        $obj->c_bpartner_id         = $material_stock->c_bpartner_id;
        $obj->supplier_name         = $material_stock->supplier_name;
        $obj->document_no           = $material_stock->document_no;
        $obj->item_code             = $material_stock->item_code;
        $obj->item_desc             = $material_stock->item_desc;
        $obj->category              = $material_stock->category;
        $obj->nomor_roll            = $material_stock->nomor_roll;
        $obj->batch_number          = $material_stock->batch_number;
        $obj->c_order_id            = $material_stock->c_order_id;
        $obj->load_actual           = $material_stock->load_actual;
        $obj->inspect_lot_result    = $material_stock->inspect_lot_result;
        $obj->inspect_lab_remark    = $material_stock->inspect_lab_remark;
        $obj->movement_date         = Carbon::now()->toDateTimeString();
        $obj->qty_check             = 0;
        $obj->remark                = null;
        
        return response()->json($obj,200);
      }else
      {
        $material_stock   = MaterialStock::where([
            ['barcode_supplier', $_barcode],
            ['warehouse_id', $warehouse_id],
        ])
        ->first();
        
        if($material_stock)
        {
            $history_material_stock = MovementStockHistory::where('material_stock_id', $material_stock->id)
                                       ->orderBy('created_at', 'desc')
                                       ->first();
                                       
            return response()->json('QTY TERSEDIA '.$material_stock->available_qty. ' TERAKHIR DIGUNAKAN UNTUK '.$history_material_stock->note.' SEBESAR '. $history_material_stock->qty, 422);
        }
        else
        {
            return response()->json('Barcode not found.', 422);
        }
      }

    }

    public function store(request $request)
    {
        $validator = Validator::make($request->all(), [
            'barcodes' => 'required|not_in:[]',
        ]);

        if($validator->passes())
        {
            $warehouse_id           = $request->warehouse_id;
            $barcodes               = json_decode($request->barcodes);
            $insert_stock_per_lots  = array();
            $material_stock_ids     = array();
            $movement_date          = carbon::now()->todatetimestring();

            try 
            {
                DB::beginTransaction();

                foreach($barcodes as $key => $value)
                {
        
                    $load_actual                        = trim(strtoupper($value->load_actual));
                    $inspect_lot_result                 = trim(strtoupper($value->inspect_lot_result));
                   
                    $material_stock                     = MaterialStock::find($value->material_stock_id);
                    $curr_load_actual                   = strtoupper($material_stock->load_actual);
                    
                    $material_stock->load_actual        = $load_actual;
                    $material_stock->inspect_lot_result = $inspect_lot_result;
                    $material_stock->inspect_lab_date   = $value->movement_date;
                    $material_stock->user_lab_id        = Auth::user()->id;
                    $material_stock->inspect_lab_remark = strtoupper(trim($value->inspect_lab_remark));
                    $monitoring_receiving_fabric_ids [] = $material_stock->monitoring_receiving_fabric_id;
        
                    if($inspect_lot_result == 'REJECT')
                    {
                        $material_stock->qc_result                  = 'REJECT BY LOT NOT MATCH';
                        $material_stock->is_reject_by_lot           = true;
                        $material_stock->is_reject                  = false;
                        $material_stock->qty_reject_non_by_inspect  = $material_stock->available_qty;
                        $material_stock->reject_by_lot_date         = $value->movement_date;
                    }

                    if($inspect_lot_result == 'RELEASE') 
                    {
                        $insert_stock_per_lots [] = $material_stock->monitoring_receiving_fabric_id;
                    }

                    if($curr_load_actual == null || $curr_load_actual == '')
                    {
                        $monitoring_receiving_fabric_id   = $material_stock->monitoring_receiving_fabric_id;
                        $monitoring_receiving_fabric      = MonitoringReceivingFabric::find($monitoring_receiving_fabric_id);
                        if($monitoring_receiving_fabric)
                        {
                            $total_roll_inspected                               = $monitoring_receiving_fabric->total_roll_inspected;
                            $monitoring_receiving_fabric->total_roll_inspected  = $total_roll_inspected+1;
                            $monitoring_receiving_fabric->save();
                        }
                    }
                    
                    $material_stock->save();
                }

                 //insert stock per lot JANGAN DI HAPUS
                $list_material_stock_per_lot_id = array();
                foreach (array_unique($insert_stock_per_lots) as $key => $value) 
                {
                    // $data = DB::select(db::raw("SELECT * FROM get_stock_per_lot(
                    //     '".$value."'
                    //     );"
                    // ));
                    $data = MaterialStock::select(
                            'monitoring_receiving_fabric_id',
                            'c_order_id',
                            'c_bpartner_id',
                            'warehouse_id',
                            'item_id',
                            'item_code',
                            'no_invoice',
                            'no_packing_list',
                            'document_no',
                            'supplier_code',
                            'supplier_name',
                            'type_stock',
                            'type_stock_erp_code',
                            'load_actual',
                            'uom',
                            db::raw("sum(qty_arrival) as total_arrival"))
                            //->where('id', $material_stock->id)
                            ->where('inspect_lot_result', 'RELEASE')
                            ->where('monitoring_receiving_fabric_id', $value)
                            ->whereNull('deleted_at')
                            ->groupBy('monitoring_receiving_fabric_id',
                            'c_order_id',
                            'c_bpartner_id',
                            'warehouse_id',
                            'item_id',
                            'item_code',
                            'no_invoice',
                            'no_packing_list',
                            'document_no',
                            'supplier_code',
                            'supplier_name',
                            'type_stock',
                            'type_stock_erp_code',
                            'load_actual',
                            'uom')
                            ->get();

                    foreach ($data as $key_2 => $datum) 
                    {
                        $monitoring_receiving_fabric_id = $datum->monitoring_receiving_fabric_id;
                        $c_order_id                     = $datum->c_order_id;
                        $c_bpartner_id                  = $datum->c_bpartner_id;
                        $warehouse_id                   = $datum->warehouse_id;
                        $item_id                        = $datum->item_id;
                        $item_code                      = $datum->item_code;
                        $no_invoice                     = $datum->no_invoice;
                        $no_packing_list                = $datum->no_packing_list;
                        $document_no                    = $datum->document_no;
                        $supplier_code                  = $datum->supplier_code;
                        $supplier_name                  = $datum->supplier_name;
                        $type_stock                     = $datum->type_stock;
                        $type_stock_erp_code            = $datum->type_stock_erp_code;
                        $actual_lot                     = $datum->load_actual;
                        $uom                            = $datum->uom;
                        $total_arrival                  = sprintf('%0.8f',$datum->total_arrival);

                        $is_exists = MaterialStockPerLot::whereNull('deleted_at')
                        ->where([
                            ['c_order_id',$c_order_id],
                            ['c_bpartner_id',$c_bpartner_id],
                            ['item_id',$item_id],
                            ['warehouse_id',$warehouse_id],
                            ['actual_lot',$actual_lot],
                            ['type_stock',$type_stock],
                            ['no_invoice',$no_invoice],
                            ['no_packing_list',$no_packing_list],
                        ])
                        ->first();

                        if(!$is_exists)
                        {
                            $material_stock_per_lot = MaterialStockPerLot::FirstOrCreate([
                                'c_order_id'          => $c_order_id,
                                'c_bpartner_id'       => $c_bpartner_id,
                                'warehouse_id'        => $warehouse_id,
                                'item_id'             => $item_id,
                                'item_code'           => $item_code,  
                                'no_invoice'          => $no_invoice,
                                'no_packing_list'     => $no_packing_list,
                                'document_no'         => $document_no,
                                'supplier_code'       => $supplier_code,
                                'supplier_name'       => $supplier_name,
                                'type_stock'          => $type_stock,
                                'type_stock_erp_code' => $type_stock_erp_code,
                                'actual_lot'          => $actual_lot,
                                'uom'                 => $uom,
                                'qty_stock'           => $total_arrival,
                                'qty_available'       => $total_arrival,
                                'created_at'          => $movement_date,
                                'updated_at'          => $movement_date,
                            ]);
    
                            $material_stock_per_lot_id  = $material_stock_per_lot->id;
                            $old_available              = '0';
                            $new_available              = $material_stock_per_lot->qty_available;
                            $curr_type_stock_erp_code   = $material_stock_per_lot->type_stock_erp_code;
                            $curr_type_stock            = $material_stock_per_lot->type_stock;
                            $curr_actual_lot            = $material_stock_per_lot->actual_lot;
                        }else
                        {
                            $material_stock_per_lot_id  = $is_exists->id;
                            $reserved_qty               = $is_exists->qty_reserved;
                            $new_stock                  = $total_arrival;
                            $availabilty_qty            = $new_stock - $reserved_qty;

                            $new_available              = $availabilty_qty;
                            $old_available              = $is_exists->qty_available;
                            $curr_type_stock_erp_code   = $is_exists->type_stock_erp_code;
                            $curr_type_stock            = $is_exists->type_stock;
                            
                            $is_exists->qty_stock       = sprintf('%0.8f',$new_stock);
                            $is_exists->qty_available   = sprintf('%0.8f',$availabilty_qty);
                            $is_exists->save();
                        
                        }
                        
                        HistoryStockPerLot::historyStockPerLot($material_stock_per_lot_id
                        ,$new_available
                        ,$old_available
                        ,null
                        ,null
                        ,null
                        ,null
                        ,null
                        ,'TAMBAH STOCK'
                        ,auth::user()->name
                        ,auth::user()->id
                        ,$curr_type_stock_erp_code
                        ,$curr_type_stock
                        ,false
                        ,false);

                        MaterialStock::where([
                        ['monitoring_receiving_fabric_id',$monitoring_receiving_fabric_id],
                        ['load_actual',$actual_lot],
                        ['c_order_id',$c_order_id],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['item_id',$item_id],
                        ['warehouse_id',$warehouse_id],
                        ['no_invoice',$no_invoice],
                        ['no_packing_list',$no_packing_list],
                        ['inspect_lot_result', 'RELEASE'],
                        ])
                        ->update([
                        'material_stock_per_lot_id' => $material_stock_per_lot_id
                        ]);

                        //$list_material_stock_per_lot_id [] = $material_stock_per_lot_id;
                    }
                
                }
                foreach (array_unique($monitoring_receiving_fabric_ids) as $key => $monitoring_receiving_fabric_id) 
                {
                    //cek lot sudah ada semua apa belum dalam satu penerimaan 
                    $is_complete_lot = $this->getLotComplete($monitoring_receiving_fabric_id);
                    if($is_complete_lot == true)
                    {
                        //schedule untuk jalanin alokasi per lot
                        Temporary::create([
                            'barcode'       => $monitoring_receiving_fabric_id,
                            'status'        => 'complete_lot',
                            'user_id'       => auth::user()->id,
                            'created_at'    => $movement_date,
                            'updated_at'    => $movement_date,
                        ]);
                    }
                }
                
                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

        }
    }

    //cek alokasi yang lotnya masih kosong
    static function allocationLot($auto_allocation_id)
    {
        //dd($auto_allocation_id);
        $allocation_items = AllocationItem::select('id')
        ->where('auto_allocation_id', $auto_allocation_id)
        ->whereNull('material_stock_per_lot_id')
        ->whereNotIn('id', function($query)
                {
                    $query->select('allocation_item_id')
                    ->from('detail_material_planning_fabrics')    
                    ->whereIn('material_planning_fabric_id', function($query)
                    {
                        $query->select('material_planning_fabric_id')
                        ->from('detail_po_buyer_per_rolls')                
                        ->groupby('material_planning_fabric_id');
                    })               
                    ->groupby('allocation_item_id');
                })
        ->groupBy('id')
        ->get();

        $material_stock_per_lots = array();
        $buyers                  = array();
        if(count($allocation_items) > 0)
        {
            foreach($allocation_items as $key => $allocation_item)
            {
                $allocation_item_id      = $allocation_item->id;
                $allocation_item_old         = AllocationItem::where('id', $allocation_item_id)->first();
                if($allocation_item_old)
                {
                    $qty_required            = $allocation_item_old->qty_booking;
                    $c_order_id              = $allocation_item_old->c_order_id;
                    $warehouse_id            = $allocation_item_old->warehouse_inventory;
                    $auto_allocation_id      = $allocation_item_old->auto_allocation_id;
                    $c_bpartner_id           = $allocation_item_old->c_bpartner_id;
                    $supplier_code           = $allocation_item_old->supplier_code;
                    $supplier_name           = $allocation_item_old->supplier_name;
                    $document_no             = $allocation_item_old->document_no;
                    $item_code               = $allocation_item_old->item_code;
                    $item_id_book            = $allocation_item_old->item_id_book;
                    $item_code_source        = $allocation_item_old->item_code_source;
                    $item_id_source          = $allocation_item_old->item_id_source;
                    $item_desc               = $allocation_item_old->item_desc;
                    $category                = $allocation_item_old->category;
                    $uom                     = $allocation_item_old->uom;
                    $job_order               = $allocation_item_old->job_order;
                    $_style                  = $allocation_item_old->_style;
                    $style                   = $allocation_item_old->style;
                    $article_no              = $allocation_item_old->article_no;
                    $new_po_buyer            = $allocation_item_old->po_buyer;
                    $old_po_buyer            = $allocation_item_old->po_buyer;
                    $warehouse_production_id = $allocation_item_old->warehouse;
                    $qty_booking             = $allocation_item_old->qty_booking;
                    $is_additional           = $allocation_item_old->is_additional;
                    $user_id                 = $allocation_item_old->user_id;
                    $type_stock              = $allocation_item_old->type_stock;
                    $remark                  = $allocation_item_old->remark;
                    $confirm_user_id         = $allocation_item_old->confirm_user_id;

                    $auto_allocation = AutoAllocation::find($auto_allocation_id);
                    $lc_date = $auto_allocation->lc_date;
                    //dd($auto_allocation_id);
                    //jika jepang cina dimaksimalkan 2 lot
                    if($auto_allocation->is_ordering_for_japan_china == 1)
                    {
                        //ambil lot dengan qty paling gede
                        $max_qty_stock_per_Lot = MaterialStockPerLot::select('actual_lot', db::raw("sum(qty_available) as total_qty"))
                        ->whereNull('deleted_at')
                        ->whereNotNull('actual_lot')
                        ->where([
                            ['c_order_id',$c_order_id],
                            ['item_id',$item_id_source],
                            ['warehouse_id',$warehouse_id],
                            ['qty_available', '>', 0],
                        ])
                        ->groupBy('actual_lot')
                        ->orderBy('total_qty', 'desc')
                        ->first();

                        //dd($max_qty_stock_per_Lot);
                        $lot_max = ($max_qty_stock_per_Lot)? $max_qty_stock_per_Lot->actual_lot : null;
                        $qty_lot_max = ($max_qty_stock_per_Lot)? $max_qty_stock_per_Lot->total_qty : 0;

                        if($lot_max != null)
                        {

                            //cari lot sebelumnya 
                            $before_max_stock_per_Lot = MaterialStockPerLot::select('actual_lot', db::raw("sum(qty_available) as total_qty"))
                            ->whereNull('deleted_at')
                            ->whereNotNull('actual_lot')
                            ->where([
                                ['c_order_id',$c_order_id],
                                ['item_id',$item_id_source],
                                ['warehouse_id',$warehouse_id],
                                ['actual_lot', '<', $lot_max],
                            ])
                            ->groupBy('actual_lot')
                            ->orderBy('total_qty', 'desc')
                            ->first();

                            $lot_before = ($before_max_stock_per_Lot) ? $before_max_stock_per_Lot->actual_lot : null;
                            $qty_lot_before = ($before_max_stock_per_Lot) ? $before_max_stock_per_Lot->total_qty : 0;

                            //cari lot setelahnya
                            $after_max_stock_per_Lot = MaterialStockPerLot::select('actual_lot', db::raw("sum(qty_available) as total_qty"))
                            ->whereNull('deleted_at')
                            ->whereNotNull('actual_lot')
                            ->where([
                                ['c_order_id',$c_order_id],
                                ['item_id',$item_id_source],
                                ['warehouse_id',$warehouse_id],
                                ['actual_lot', '>', $lot_max],
                            ])
                            ->groupBy('actual_lot')
                            ->orderBy('total_qty', 'desc')
                            ->first();

                            $lot_after = ($after_max_stock_per_Lot) ? $after_max_stock_per_Lot->actual_lot : null;
                            $qty_lot_after = ($after_max_stock_per_Lot) ? $after_max_stock_per_Lot->total_qty : 0;

                            //list urutan lotnya 
                            $material_stock_per_lots = MaterialStockPerLot::whereNull('deleted_at')
                            ->whereNotNull('actual_lot')
                            ->where([
                                ['c_order_id',$c_order_id],
                                ['item_id',$item_id_source],
                                ['warehouse_id',$warehouse_id],
                            ])
                            ->orderBy(db::raw(" actual_lot ='".$lot_max."'"), 'desc')
                            ->orderBy(db::raw("case when '".$qty_lot_before."' < '".$qty_lot_after."'  then
                                actual_lot ='".$lot_before."'
                                else
                                actual_lot ='".$lot_after."'
                                end"), 'desc')
                            ->orderBy(db::raw("case when '".$qty_lot_before."' < '".$qty_lot_after."'  then
                                actual_lot >'".$lot_before."'
                                else
                                actual_lot >'".$lot_after."'
                                end"), 'desc')
                            ->get();    
                        }
                        else
                        {
                            break;
                        }
                        //dd($material_stock_per_lots);

                    }
                    else
                    {
                        $material_stock_per_lots = MaterialStockPerLot::whereNull('deleted_at')
                        ->whereNotNull('actual_lot')
                        ->where([
                            ['c_order_id',$c_order_id],
                            ['item_id',$item_id_source],
                            ['warehouse_id',$warehouse_id],
                            ['qty_available', '>', 0],
                        ])
                        ->orderBy('actual_lot', 'asc')
                        ->orderBy('qty_available', 'desc')
                        ->get();

                    }



                    if(count($material_stock_per_lots) > 0)
                    {
                            foreach ($material_stock_per_lots as $key_1 => $material_stock_per_lot) 
                            {
                                if($qty_required > 0.0000)
                                {
                                    //dd($material_stock_per_lot);
                                    $material_stock_per_lot_id = $material_stock_per_lot->id;
                                    $qty_available_lot         = sprintf('%0.8f',$material_stock_per_lot->qty_available);
                                    $qty_reserved_lot          = sprintf('%0.8f',$material_stock_per_lot->qty_reserved);

                                    //cek apakah stock per lot cukup atau tidak 
                                    if($qty_available_lot >= $qty_required )
                                    {
                                        $qty_booking = $qty_required;
                                        $allocation_item = AllocationItem::FirstOrCreate([
                                            'auto_allocation_id'        => $auto_allocation_id,
                                            'lc_date'                   => $lc_date,
                                            'c_bpartner_id'             => $c_bpartner_id,
                                            'c_order_id'                => $c_order_id,
                                            'supplier_code'             => $supplier_code,
                                            'supplier_name'             => $supplier_name,
                                            'document_no'               => $document_no,
                                            'item_code'                 => $item_code,
                                            'item_id_book'              => $item_id_book,
                                            'item_code_source'          => $item_code_source,
                                            'item_id_source'            => $item_id_source,
                                            'item_desc'                 => $item_desc,
                                            'category'                  => $category,
                                            'uom'                       => $uom,
                                            'job'                       => $job_order,
                                            '_style'                    => $_style,
                                            'style'                     => $style,
                                            'article_no'                => $article_no,
                                            'po_buyer'                  => $new_po_buyer,
                                            'old_po_buyer_reroute'      => $old_po_buyer,
                                            'warehouse'                 => $warehouse_production_id,
                                            'warehouse_inventory'       => $warehouse_id,
                                            'is_need_to_handover'       => false,
                                            'qty_booking'               => $qty_booking,
                                            'is_from_allocation_fabric' => true,
                                            'is_additional'             => $is_additional,
                                            'user_id'                   => $user_id,
                                            'confirm_by_warehouse'      => 'approved',
                                            'type_stock_buyer'          => $type_stock,
                                            'confirm_date'              => Carbon::now(),
                                            'remark'                    => null,
                                            'confirm_user_id'           => '107',
                                            'deleted_at'                => null,
                                            'material_stock_per_lot_id' => $material_stock_per_lot_id,
                                        ]);

                                        //update available stock per lot
                                        $qty_available_lot                     -= $qty_booking;
                                        $qty_reserved_lot                      += $qty_booking;
                                        $material_stock_per_lot->qty_available  = $qty_available_lot;
                                        $material_stock_per_lot->qty_reserved   = $qty_reserved_lot;
                                        $material_stock_per_lot->save();
                                    }
                                    else if(($qty_available_lot < $qty_required) && $qty_available_lot > 0)
                                    {
                                        //jika kurang dari qty booking pakai available yang tersedia
                                        $qty_booking = $qty_available_lot;

                                        $allocation_item = AllocationItem::FirstOrCreate([
                                            'auto_allocation_id'        => $auto_allocation_id,
                                            'lc_date'                   => $lc_date,
                                            'c_bpartner_id'             => $c_bpartner_id,
                                            'c_order_id'                => $c_order_id,
                                            'supplier_code'             => $supplier_code,
                                            'supplier_name'             => $supplier_name,
                                            'document_no'               => $document_no,
                                            'item_code'                 => $item_code,
                                            'item_id_book'              => $item_id_book,
                                            'item_code_source'          => $item_code_source,
                                            'item_id_source'            => $item_id_source,
                                            'item_desc'                 => $item_desc,
                                            'category'                  => $category,
                                            'uom'                       => $uom,
                                            'job'                       => $job_order,
                                            '_style'                    => $_style,
                                            'style'                     => $style,
                                            'article_no'                => $article_no,
                                            'po_buyer'                  => $new_po_buyer,
                                            'old_po_buyer_reroute'      => $old_po_buyer,
                                            'warehouse'                 => $warehouse_production_id,
                                            'warehouse_inventory'       => $warehouse_id,
                                            'is_need_to_handover'       => false,
                                            'qty_booking'               => $qty_booking,
                                            'is_from_allocation_fabric' => true,
                                            'is_additional'             => $is_additional,
                                            'user_id'                   => $user_id,
                                            'confirm_by_warehouse'      => 'approved',
                                            'type_stock_buyer'          => $type_stock,
                                            'confirm_date'              => Carbon::now(),
                                            'remark'                    => null,
                                            'confirm_user_id'           => '107',
                                            'deleted_at'                => null,
                                            'material_stock_per_lot_id' => $material_stock_per_lot_id,
                                        ]);

                                        //update available stock per lot
                                        $qty_available_lot                     -= $qty_booking;
                                        $qty_reserved_lot                      += $qty_booking;
                                        $material_stock_per_lot->qty_available  = $qty_available_lot;
                                        $material_stock_per_lot->qty_reserved   = $qty_reserved_lot;
                                        $material_stock_per_lot->save();
                                    }

                                    $qty_required       -= $qty_booking;
                                    $buyers []           = $new_po_buyer;
                                }
                                else
                                {
                                    break;
                                }

                            }

                    }
                    
                    if($qty_required > 0.0000)
                    {
                        $__supply = $qty_required;

                        $allocation_item = AllocationItem::FirstOrCreate([
                            'auto_allocation_id'        => $auto_allocation_id,
                            'lc_date'                   => $lc_date,
                            'c_bpartner_id'             => $c_bpartner_id,
                            'c_order_id'                => $c_order_id,
                            'supplier_code'             => $supplier_code,
                            'supplier_name'             => $supplier_name,
                            'document_no'               => $document_no,
                            'item_code'                 => $item_code,
                            'item_id_book'              => $item_id_book,
                            'item_code_source'          => $item_code_source,
                            'item_id_source'            => $item_id_source,
                            'item_desc'                 => $item_desc,
                            'category'                  => $category,
                            'uom'                       => $uom,
                            'job'                       => $job_order,
                            '_style'                    => $_style,
                            'style'                     => $style,
                            'article_no'                => $article_no,
                            'po_buyer'                  => $new_po_buyer,
                            'old_po_buyer_reroute'      => $old_po_buyer,
                            'warehouse'                 => $warehouse_production_id,
                            'warehouse_inventory'       => $warehouse_id,
                            'is_need_to_handover'       => false,
                            'qty_booking'               => $__supply,
                            'is_from_allocation_fabric' => true,
                            'is_additional'             => $is_additional,
                            'user_id'                   => $user_id,
                            'confirm_by_warehouse'      => 'approved',
                            'type_stock_buyer'          => $type_stock,
                            'confirm_date'              => Carbon::now(),
                            'remark'                    => null,
                            'confirm_user_id'           => '107',
                            'deleted_at'                => null
                    ]);

                    $qty_required       -= $__supply;
                    $buyers []           = $new_po_buyer;

                    }
                    $allocation_item_old->delete();
                }
            }
            PlanningFabric::getAllocationFabricPerBuyer($buyers);
        }

    }

    public function getLotComplete($monitoring_receiving_fabric_id)
    {
        $material_stocks = MaterialStock::where('monitoring_receiving_fabric_id', $monitoring_receiving_fabric_id)
                           ->whereNull('load_actual')
                           ->exists();
        if($material_stocks)
        {
            return false;
        }
        else
        {
           return true;

        }


    }
}
