<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use File;
use Auth;
use Excel;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;


class FabricReportMaterialRejectController extends Controller
{
    public function index()
    {
        return view('fabric_report_material_reject.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id       = $request->warehouse;
            $user_id            = $request->user_id ;
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
            
            $material_reject    = DB::table('report_fabric_reject_v')
            ->where('warehouse_id','LIKE',"%$warehouse_id%")
            ->orderby('reject_date','desc');

            return DataTables::of($material_reject)
           
            ->editColumn('qty_reject',function ($material_reject)
            {
                return '-'.number_format($material_reject->qty_reject, 4, '.', ',');
            })
            ->editColumn('scan_reject',function ($material_reject)
            {
                if ($material_reject->scan_reject) return '<div class="checker border-info-600 text-info-800">
                    <span class="checked"><input type="checkbox" class="control-info" checked="checked"></span>
                </div>';
                else return null;
                
            })
            ->rawColumns(['scan_reject']) 
            ->make(true);
        }
    }

    static function sendEmailMoveLocatorReject()
    {
        try
        {
            $emails         = ['indra.setyawan@aoi.co.id', 'niam.alfiyan@aoi.co.id', 'whs.fabric3@aoi.co.id'];
            $cc             = ['numa.ryan@aoi.co.id','mega.antika@aoi.co.id','iin.sugiarti@aoi.co.id','diah.priatiningsih@aoi.co.id','dino.wicaksono@aoi.co.id','istriana.lab@aoi.co.id'];
            //$cc = ['niam.alfiyan@aoi.co.id'];
            //$emails = ['niam.alfiyan@aoi.co.id'];
            
            $data           = DB::select(db::raw("SELECT *  FROM report_fabric_reject_v where scan_reject = false")); //where material_check_id in( '816f19a0-099e-11ea-b515-9ffe6c395ca1','0b690ff0-09a0-11ea-8b1c-bf76fbc2001c')
            $data_limit_100 = DB::select(db::raw("SELECT *  FROM report_fabric_reject_v where scan_reject = false limit 100;"));
           

            $location = Config::get('storage.email_attachment_qc');
            if (!File::exists($location)) File::makeDirectory($location, 0777, true);
            
            
            $send_date = carbon::now()->format('Ymd');
            $file_name = 'outstanding_move_locator_reject_'.$send_date;
            
            $file = Excel::create($file_name,function($excel) use($data){
                $excel->sheet('data',function($sheet) use($data){
                    $sheet->setCellValue('A1','#');
                    $sheet->setCellValue('B1','BARCODE');
                    $sheet->setCellValue('C1','NOMOR_ROLL');
                    $sheet->setCellValue('D1','DOCUMENT_NO');
                    $sheet->setCellValue('E1','SUPPLIER_NAME');
                    $sheet->setCellValue('F1','ITEM_CODE');
                    $sheet->setCellValue('G1','ITEM_DESC');
                    $sheet->setCellValue('H1','BATCH_NUMBER');
                    $sheet->setCellValue('I1','COLOR');
                    $sheet->setCellValue('J1','NO_PACKING_LIST');
                    $sheet->setCellValue('K1','NO_INVOICE');
                    $sheet->setCellValue('L1','QTY_REJECT');
                    $sheet->setCellValue('M1','WAREHOUSE');
                    $sheet->setCellValue('N1','REMARK');
                    $sheet->setCellValue('O1','LOCATOR');

                    $sheet->setAutoSize(true);
                    $row=2;
                    foreach ($data as $key => $datum) 
                    {  
                        $sheet->setCellValue('A'.$row,($key+1));
                        $sheet->setCellValue('B'.$row,$datum->barcode_supplier);
                        $sheet->setCellValue('C'.$row,$datum->nomor_roll);
                        $sheet->setCellValue('D'.$row,$datum->document_no);
                        $sheet->setCellValue('E'.$row,$datum->supplier_name);
                        $sheet->setCellValue('F'.$row,$datum->item_code);
                        $sheet->setCellValue('G'.$row,$datum->item_desc);
                        $sheet->setCellValue('H'.$row,$datum->batch_number);
                        $sheet->setCellValue('I'.$row,$datum->color);
                        $sheet->setCellValue('J'.$row,$datum->no_packing_list);
                        $sheet->setCellValue('K'.$row,$datum->no_invoice);
                        $sheet->setCellValue('L'.$row,$datum->qty_reject);
                        $sheet->setCellValue('M'.$row,$datum->warehouse_name);
                        $sheet->setCellValue('N'.$row,$datum->remark);
                        $sheet->setCellValue('O'.$row,$datum->locator);
                        $row++;
                    }
                    
                    
                });
            })
            ->save('xlsx', $location);
            
            $file = $location.'/'.$file_name.'.xlsx';
            Mail::send('fabric_report_material_reject.email', ['total_outstanding' => count($data),'data' => $data_limit_100], function ($message) use ($emails,$cc,$file,$file_name)
            {
                $message->subject('OUTSTANDING MOVE LOCATOR REJECT');
                $message->from('no-reply@wms.aoi.co.id', 'WMS');
                $message->cc($cc);
                $message->to($emails);
                $message->attach($file, array('as' => $file_name.'.xlsx', 'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'));
            });

            File::delete($file);
            
        }catch (Exception $e)
        {
            return response (['status' => false,'errors' => $e->getMessage()]);
        }
    }
    
    public function export()
    {
        $warehouse_id       = Auth::user()->warehouse;

        if ($warehouse_id == '1000001' ? $warehouse_name = 'warehouse_fab_aoi_1': $warehouse_name = 'warehouse_fab_aoi_2' );

        $material_stocks      = DB::table('report_fabric_reject_v');
        if($warehouse_id != 'all') $material_stocks = $material_stocks->where('warehouse_id','LIKE',"%$warehouse_id%");

        $material_stocks = $material_stocks->orderby('item_code','asc')
        ->get();

        $file_name = 'report_material_reject_'.$warehouse_name;
        return Excel::create($file_name,function($excel) use ($material_stocks)
        {
            $excel->sheet('active',function($sheet)use($material_stocks)
            {
                $sheet->setCellValue('A1','#');
                $sheet->setCellValue('B1','BARCODE');
                $sheet->setCellValue('C1','NOMOR_ROLL');
                $sheet->setCellValue('D1','DOCUMENT_NO');
                $sheet->setCellValue('E1','SUPPLIER_NAME');
                $sheet->setCellValue('F1','ITEM_CODE');
                $sheet->setCellValue('G1','ITEM_DESC');
                $sheet->setCellValue('H1','BATCH_NUMBER');
                $sheet->setCellValue('I1','COLOR');
                $sheet->setCellValue('J1','NO_PACKING_LIST');
                $sheet->setCellValue('K1','NO_INVOICE');
                $sheet->setCellValue('L1','QTY_REJECT');
                $sheet->setCellValue('M1','WAREHOUSE');
                $sheet->setCellValue('N1','SCAN_REJECT');
                $sheet->setCellValue('O1','REMARK');
                $sheet->setCellValue('P1','LOCATOR');
            
            $row=2;
            foreach ($material_stocks as $key => $datum) 
            {  
                $sheet->setCellValue('A'.$row,($key+1));
                $sheet->setCellValue('B'.$row,$datum->barcode_supplier);
                $sheet->setCellValue('C'.$row,$datum->nomor_roll);
                $sheet->setCellValue('D'.$row,$datum->document_no);
                $sheet->setCellValue('E'.$row,$datum->supplier_name);
                $sheet->setCellValue('F'.$row,$datum->item_code);
                $sheet->setCellValue('G'.$row,$datum->item_desc);
                $sheet->setCellValue('H'.$row,$datum->batch_number);
                $sheet->setCellValue('I'.$row,$datum->color);
                $sheet->setCellValue('J'.$row,$datum->no_packing_list);
                $sheet->setCellValue('K'.$row,$datum->no_invoice);
                $sheet->setCellValue('L'.$row,$datum->qty_reject);
                $sheet->setCellValue('M'.$row,$datum->warehouse_name);
                $sheet->setCellValue('N'.$row,$datum->scan_reject);
                $sheet->setCellValue('O'.$row,$datum->remark);
                $sheet->setCellValue('P'.$row,$datum->locator);
                $row++;
            }
            });

        })
        ->export('xlsx');
        
    }
}
