<?php
namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class AccessoriesReportMaterialStockOpnameController extends Controller
{
    public function index(Request $request)
    {
        return view('accessories_report_material_stock_opname.index');
    }
    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id       = $request->warehouse;
            $user_id            = $request->user_id ;
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
            
            $material_stock_opname = DB::table('material_stock_opname_acc_v')
            ->where('warehouse','LIKE',"%$warehouse_id%")
            ->whereBetween('sto_date', [$start_date, $end_date])
            ->orderby('sto_date','desc');

            return DataTables::of($material_stock_opname)
           
            ->editColumn('sto_date',function ($material_stock_opname)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $material_stock_opname->sto_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('qty_conversion',function ($material_stock_opname)
            {
                return number_format($material_stock_opname->qty_conversion, 4, '.', ',');
            })
            ->make(true);
        }

    }
    public function export(Request $request)
    {
        $warehouse_id       = ($request->warehouse ? $request->warehouse : auth::user()->warehouse);
        $warehouse_name     = ($warehouse_id == '1000002' ?'warehouse_acc_aoi_1':'warehouse_acc_aoi_2' );
        
        $material_bapb      = DB::table('material_stock_opname_acc_v')
        ->where('warehouse','LIKE',"%$warehouse_id%")
        ->orderby('sto_date','desc')
        ->get();

        $file_name = 'material_stock_opname_acc'.$warehouse_name;
        return Excel::create($file_name,function($excel) use ($material_bapb){
            $excel->sheet('active',function($sheet)use($material_bapb)
            {
                $sheet->setCellValue('A1','STO DATE');
                $sheet->setCellValue('B1','STO BY');
                $sheet->setCellValue('C1','LOCATOR');
                $sheet->setCellValue('D1','PO SUPPLIER');
                $sheet->setCellValue('E1','KODE SUPPLIER');
                $sheet->setCellValue('F1','NAMA SUPPLIER');
                $sheet->setCellValue('G1','ITEM CODE');
                $sheet->setCellValue('H1','UOM');
                $sheet->setCellValue('I1','QTY');

            
            $row=2;
            foreach ($material_bapb as $i) 
            {  
                $sheet->setCellValue('A'.$row,$i->sto_date);
                $sheet->setCellValue('B'.$row,$i->name);
                $sheet->setCellValue('C'.$row,$i->locator);
                $sheet->setCellValue('D'.$row,$i->document_no);
                $sheet->setCellValue('E'.$row,$i->c_bpartner_id);
                $sheet->setCellValue('F'.$row,$i->supplier_name);
                $sheet->setCellValue('G'.$row,$i->item_code);
                $sheet->setCellValue('H'.$row,$i->uom_conversion);
                $sheet->setCellValue('I'.$row,$i->qty_conversion);
                $row++;
            }
            });

        })
        ->export('xlsx');
    }
}
