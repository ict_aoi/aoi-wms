<?php
namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use File;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use Illuminate\Support\Facades\Session;

use App\Models\User;
use App\Models\Item;
use App\Models\Locator;
use App\Models\PoBuyer;
use App\Models\Supplier;
use App\Models\Temporary;
use App\Models\PoSupplier;
use App\Models\UomConversion;
use App\Models\MaterialStock;
use App\Models\MappingStocks;
use App\Models\ReroutePoBuyer;
use App\Models\AutoAllocation;
use App\Models\AllocationItem;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\MaterialPreparation;
use App\Models\MaterialRequirement;
use App\Models\MaterialMovementLine;
use App\Models\HistoryAutoAllocations;
use App\Models\HistoryPreparationFabric;
use App\Models\MaterialPreparationFabric;
use App\Models\DetailMaterialPreparation;
use App\Models\DetailMaterialPlanningFabric;
use App\Models\DashboardErp;

class FabricReportMaterialMonitoringController extends Controller
{
    public function index(Request $request)
    {
        return view('fabric_report_material_monitoring.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            //$warehouse_id = $request->warehouse;
            $start_date             = ( $request->start_date ? Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d') : null);
            $end_date               = ( $request->start_date ? Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d') : null);
            $start_statistical_date = ( $request->start_statistical_date ? Carbon::createFromFormat('d/m/Y', $request->start_statistical_date)->format('Y-m-d') : null);
            $end_statistical_date   = ( $request->end_statistical_date ? Carbon::createFromFormat('d/m/Y', $request->end_statistical_date)->format('Y-m-d') : null);

            if(auth::user()->hasRole(['mm-staff','admin-ict-fabric']))
            {
                $warehouse_id = ['1000011','1000001'];
                if(($start_date != null and $end_date != null) && ($start_statistical_date == null and $end_statistical_date == null))
                {
                    $auto_allocations = db::table('monitoring_allocations_v')
                    ->where('is_additional',false)
                    ->whereBetween('lc_date', [$start_date, $end_date])
                    ->whereIn('warehouse_id',$warehouse_id)
                    ->orderBy('statistical_date', 'asc')
                    ->orderBy('promise_date', 'asc')
                    ->orderBy('qty_allocation', 'asc')
                    ->get();
                }
                else if (($start_statistical_date != null and $end_statistical_date != null) and ($start_date == null and $end_date == null))
                {
                    $auto_allocations = db::table('monitoring_allocations_v')
                    ->where('is_additional',false)
                    ->whereBetween('statistical_date', [$start_statistical_date, $end_statistical_date])
                    ->whereIn('warehouse_id',$warehouse_id)
                    ->orderBy('statistical_date', 'asc')
                    ->orderBy('promise_date', 'asc')
                    ->orderBy('qty_allocation', 'asc')
                    ->get();
                }
                else
                {
                    $auto_allocations = db::table('monitoring_allocations_v')
                    ->where('is_additional',false)
                    ->whereBetween('lc_date', [$start_date, $end_date])
                    ->whereBetween('statistical_date', [$start_statistical_date, $end_statistical_date])
                    ->whereIn('warehouse_id',$warehouse_id)
                    ->orderBy('statistical_date', 'asc')
                    ->orderBy('promise_date', 'asc')
                    ->orderBy('qty_allocation', 'asc')
                    ->get();
                }
            }
            else
            {
                $warehouse_id = ['1000013','1000002'];
                if(($start_date != null and $end_date != null) && ($start_statistical_date == null and $end_statistical_date == null))
                {
                    $auto_allocations = db::table('monitoring_allocations_v')
                    ->where('is_additional',false)
                    ->whereBetween('lc_date', [$start_date, $end_date])
                    ->whereIn('warehouse_id',$warehouse_id)
                    ->orderBy('statistical_date', 'asc')
                    ->orderBy('promise_date', 'asc')
                    ->orderBy('qty_allocation', 'asc')
                    ->get();
                }
                else if (($start_statistical_date != null and $end_statistical_date != null) and ($start_date == null and $end_date == null))
                {
                    $auto_allocations = db::table('monitoring_allocations_v')
                    ->where('is_additional',false)
                    ->whereBetween('statistical_date', [$start_statistical_date, $end_statistical_date])
                    ->whereIn('warehouse_id',$warehouse_id)
                    ->orderBy('statistical_date', 'asc')
                    ->orderBy('promise_date', 'asc')
                    ->orderBy('qty_allocation', 'asc')
                    ->get();
                }
                else
                {
                    $auto_allocations = db::table('monitoring_allocations_v')
                    ->where('is_additional',false)
                    ->whereBetween('lc_date', [$start_date, $end_date])
                    ->whereBetween('statistical_date', [$start_statistical_date, $end_statistical_date])
                    ->whereIn('warehouse_id',$warehouse_id)
                    ->orderBy('statistical_date', 'asc')
                    ->orderBy('promise_date', 'asc')
                    ->orderBy('qty_allocation', 'asc')
                    ->get();
                }

            }

            //dd($auto_allocations);

            return DataTables::of($auto_allocations)
            ->editColumn('warehouse_id',function ($auto_allocations)
            {
                if($auto_allocations->warehouse_id == '1000001') return 'Warehouse fabric AOI 1';
                elseif($auto_allocations->warehouse_id == '1000011') return 'Warehouse fabric AOI 2';
            })
            ->addColumn('qty_open',function($auto_allocations)
            {
                $qty_open = sprintf('%0.4f',$auto_allocations->qty_allocation) - sprintf('%0.4f',$auto_allocations->qty_in_house) - sprintf('%0.4f',$auto_allocations->qty_on_ship);
                return number_format($qty_open, 4, '.', ',');
            })
            // ->addColumn('status',function($auto_allocations){
            //     if($auto_allocations->is_closed == true)
            //     {
            //         return '<span class="label label-info">CLOSED</span>';
            //     }
            //     elseif($auto_allocations->qty_allocation == $auto_allocations->qty_in_house)
            //     {
            //         return '<span class="label label-info">CLOSED</span>';
            //     }
            //     elseif(($auto_allocations->qty_allocation - $auto_allocations->qty_in_house) - $auto_allocations->qty_on_ship > 0)
            //     {
            //         return '<span class="label label-default">OPEN</span>';
            //     }
            //     else
            //     {
            //         return '<span class="label label-default">ON SHIP</span>';
            //     }
            // })
            ->rawColumns(['qty_open'])
            ->make(true);
        
        }
    }



    static function updateMonitoringMaterial()
    {
        $warehouse_id = ['1000011','1000001'];

        $ids = db::table('mna_test')->pluck('id')->toArray();

        $refresh = DB::select(db::raw("
        refresh materialized view monitoring_allocations_mv;"
        ));

        //list kalkulasi
        $auto_allocations = db::table('monitoring_allocations_mv')
        ->where('is_additional',false)
        ->where('lc_date', '>=', '2020-08-01')
        //->where('lc_date', '<', '2020-08-01')
        // ->where('qty_allocated', '>', '0')
        // ->where('c_order_id','1926979')
        // ->where('item_id_source','2137928')
        ->wherein('id',$ids)
        ->where(db::raw("qty_allocated - COALESCE(qty_in_house, 0)"), '>' ,'0')
        ->whereIn('warehouse_id',$warehouse_id)
        ->orderBy('statistical_date', 'asc')
        ->orderBy('promise_date', 'asc')
        ->orderBy('qty_allocation', 'asc')
        ->get();

        $change_date = carbon::now()->format('Y-m-d');

        $app_env = Config::get('app.env');
        if($app_env == 'live')
        {
            $data_erp = DB::connection('erp');
            $data     = DB::connection('web_po');
            $webpo_new = DB::connection('webpo_new');
        }

        // dd($auto_allocations);
        try 
            {
            DB::beginTransaction();

            $reset_alocation = AutoAllocation::where('is_additional',false)
            //->whereBetween('lc_date', [$start_date, $end_date])
            ->where('lc_date', '>=', '2020-08-01')
            // ->where('c_order_id','1926979')
            // ->where('item_id_source','2137928')
            //->where(db::raw("to_char(lc_date,'YYYYMM')"),['202006'])
            ->wherein('id',$ids)
            ->where(db::raw("qty_allocated - COALESCE(qty_in_house, 0)"), '>' ,'0')
            ->whereIn('warehouse_id',$warehouse_id)
            ->whereNull('deleted_at')
            ->update([
                'qty_in_house' => null,
                'qty_on_ship'  => null,
                'qty_mrd'      => null,
                // 'eta_actual'   => null,
                // 'eta_actual'   => null,
                // 'no_invoice'   => null,
                // 'receive_date' => null,
                // 'mrd'          => null,
                // 'dd_pi'        => null,
                // 'eta_date'     => null,
                // 'etd_date'     => null,
            ]);

            foreach($auto_allocations as $key => $auto_allocation)
            {   
                $eta_date     = null;
                $etd_date     = null;
                $receive_date = null;
                $no_invoice   = null;
                $qty_in_house = 0;
                $qty_on_ship  = 0;
                $qty_mrd      = 0;
                $mrd          = null;
                $dd_pi        = null;
                $eta_pi       = null;
                $etd_pi       = null;

                $auto_allocation_id = $auto_allocation->id;
                $item_id            = $auto_allocation->item_id_source;
                $warehouse_id       = $auto_allocation->warehouse_id;
                $c_order_id         = $auto_allocation->c_order_id;
                $lc_date             = $auto_allocation->lc_date;
                $qty_need           = sprintf('%0.4f',$auto_allocation->qty_allocation);
                $qty_on_ship_alocation = sprintf('%0.4f',$auto_allocation->qty_on_ship);
                echo $key . ' - '. $auto_allocation_id . "\n";
                if($c_order_id == 'FREE STOCK')
                {
                    $qty_in_house = $qty_need;
                    $eta_actual   = $lc_date;
                    $etd_actual   = $lc_date;
                    $receive_date = $lc_date;
                    $no_invoice   = null;

                }
                else
                {

                $material_stock = MaterialStock::select(db::raw('sum(stock) as stock'), db::raw('max(created_at) as created_at'))
                ->where([
                ['item_id', $item_id],
                ['c_order_id', $c_order_id],
                ['is_stock_mm', false],
                ['is_closing_balance', false],
                ['approval_date', '!=', null],
                ['is_from_handover', false],
                ['is_master_roll', true],
                //['warehouse_id', $warehouse_id], tidak cek warehouse
                ])
                ->whereNull('case')
                ->first();
                //->whereNull('deleted_at')
                //->sum('stock');
                // dd($material_stock);
                $sum_qty_stock = $material_stock->stock;
                $receive_date    = $material_stock->created_at;
                //$no_invoice    = $material_stock->no_invoice;

                $sum_qty_in_house = AutoAllocation::where([
                    ['item_id_source', $item_id],
                    ['c_order_id', $c_order_id],
                    //['warehouse_id', $warehouse_id],
                    ])
                    ->whereNull('deleted_at')
                    ->sum('qty_in_house');

                //sisa qty in house 
                $material_in_house = sprintf('%0.4f',$sum_qty_stock) - sprintf('%0.4f',$sum_qty_in_house);
                // dd($material_in_house);
                //jika material yang tersdia > 0
                
                if($material_in_house > 0)
                {
                    //dd($material_in_house - $qty_need);
                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                    //dd($material_in_house/$qty_need);
                    if(sprintf('%0.4f',$material_in_house)/$qty_need >= 1)
                    {
                        $qty_in_house = $qty_need;
                        $qty_on_ship  = 0;

                        //$app_env = Config::get('app.env');
                        // if($app_env == 'live')
                        // {
                            //$data = DB::connection('web_po');
                            $list_on_ships = $webpo_new->select("SELECT sum(qty_upload) as qty_upload, kst_etadate, kst_etddate, kst_invoicevendor,wms_receiving_date
                                                        FROM material_on_ship_f('$c_order_id','$item_id','t') GROUP BY kst_etadate,kst_etddate,kst_invoicevendor,wms_receiving_date ORDER BY kst_etadate asc , kst_etddate asc");
                        

                            if(count($list_on_ships) > 0)
                            {
                                $sum_on_ship_web_po = 0;
                                foreach ($list_on_ships as $item) {
                                    $sum_on_ship_web_po += $item->qty_upload;
                                }
                                $temp_qty_on_ship = 0;
                                foreach($list_on_ships as $key => $list_on_ship)
                                {
                                    $sisa = 0;
                                    $no_invoice           = $list_on_ship->kst_invoicevendor;
                                    $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
                                    $temp_qty_on_ship += $total_on_ship_web_po;

                                    $sum_all_qty_on_ship = AutoAllocation::where([
                                        ['item_id_source', $item_id],
                                        ['c_order_id', $c_order_id],
                                        //['warehouse_id', $warehouse_id],
                                        ])
                                        ->whereNull('deleted_at')
                                        ->sum('qty_on_ship');

                                   
                                       
                                    if(sprintf('%0.4f', $sum_all_qty_on_ship) - $temp_qty_on_ship > 0 )
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        $outstanding_on_ship =  $temp_qty_on_ship - sprintf('%0.4f', $sum_all_qty_on_ship);
                                    }

                                    //$outstanding_on_ship = sprintf('%0.4f',$total_on_ship_web_po) - sprintf('%0.4f',$sum_qty_on_ship);
                                    
                                    //echo $no_invoice. ' total  '. $total_on_ship_web_po. ' sisa '.$outstanding_on_ship. PHP_EOL;

                                    //echo (sprintf('%0.4f',$sum_on_ship_web_po) - sprintf('%0.4f', $sum_all_qty_on_ship)). PHP_EOL;
                                    if(sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship) != 0)
                                    {
                                        if($outstanding_on_ship > 0)
                                        {
                                            //jika material yang tersedia lebih dari atau sama dengan alokasi
                                            echo "\n" .$outstanding_on_ship >= ($qty_need - $qty_in_house). "\n";
                                            if($outstanding_on_ship >= ($qty_need - $qty_in_house))
                                            {
                                                
                                                    $qty_on_ship              = $qty_need - $qty_in_house;
                                                    $eta_date                 = $list_on_ship->kst_etadate;
                                                    $etd_date                 = $list_on_ship->kst_etddate;
                                                
                                               
                                                //$receive_date             = null;
                                                //echo $qty_on_ship. PHP_EOL;
                                                break;
                                            }
                                            else
                                            {
                                                $qty_on_ship       += $outstanding_on_ship;
                                                if($qty_on_ship >= ($qty_need - $qty_in_house))
                                                {
                                                    $qty_on_ship  = $qty_need - $qty_in_house;
                                                    $eta_date     = $list_on_ship->kst_etadate;
                                                    $etd_date     = $list_on_ship->kst_etddate;
                                                    //$receive_date = null;
                                                    //echo $qty_on_ship. PHP_EOL;
                                                    break;
                                                    
                                                }
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                //$receive_date             = $receive_date;
                                                //echo $qty_on_ship. PHP_EOL;
                                            }
                                            

                                        }
                                    }
                                }
                                //get mrd
                                $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();
                                    
                                    //dd($list_on_ship_mrds);
                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                            $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                ['status_po_buyer', 'active'] //permintaan elva karena kemakan po cancel 
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            
                                                
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }

                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        //$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_mrd. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                        //$receive_date = $receive_date;
                                                        //echo $qty_mrd. PHP_EOL;
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }
                            }
                        
                                    $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();
                                    
                                    //dd($list_on_ship_mrds);
                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                            $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            
                                                
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }

                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        //$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        $receive_date = $receive_date;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_mrd. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                        //echo $qty_mrd. PHP_EOL;
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }

                    }
                    else
                    {
                        $qty_in_house = $material_in_house;
                        //cek kekurangan di web po
                        // $app_env = Config::get('app.env');
                        // if($app_env == 'live')
                        // {
                        //     $data = DB::connection('web_po');
                        $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                        ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                        ->where([
                            ['m_product_id', $item_id],
                            ['c_order_id', $c_order_id],
                        ])
                        ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                        ->orderBy('mrd', 'asc')
                        ->orderBy('dd_pi', 'asc')
                        ->get();
                        
                        //dd($list_on_ship_mrds);
                        $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                        if(count($list_on_ship_mrds) > 0)
                        {
                            $temp_qty_on_ship_mrd = 0;
                            foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                            {
                                $sisa = 0;
                                $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                    ['item_id_source', $item_id],
                                    ['c_order_id', $c_order_id],
                                    ['warehouse_id', $warehouse_id],
                                    ])
                                    ->whereNull('deleted_at')
                                    ->sum('qty_mrd');
                                
                                    
                                if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                {
                                    continue;
                                }
                                else
                                {
                                    $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                }

                                if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                {
                                    if($outstanding_on_ship_mrd > 0)
                                    {
                                        //jika material yang tersedia lebih dari atau sama dengan alokasi
                                        if($outstanding_on_ship_mrd >= $qty_need)
                                        {
                                            $qty_mrd      = $qty_need;
                                            $mrd          = $list_on_ship_mrd->mrd;
                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                            $eta_pi       = $list_on_ship_mrd->eta;
                                            $etd_pi       = $list_on_ship_mrd->etd;
                                            //$receive_date = null;
                                            //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                            break;
                                        }
                                        else
                                        {
                                            $qty_mrd       += $outstanding_on_ship_mrd;
                                            $mrd          = $list_on_ship_mrd->mrd;
                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                            $eta_pi       = $list_on_ship_mrd->eta;
                                            $etd_pi       = $list_on_ship_mrd->etd;
                                            $receive_date = $receive_date;
                                            //echo $qty_mrd. PHP_EOL;

                                            if($qty_mrd >= $qty_need)
                                            {
                                                $qty_mrd      = $qty_need;
                                                $mrd          = $list_on_ship_mrd->mrd;
                                                $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                $eta_pi       = $list_on_ship_mrd->eta;
                                                $etd_pi       = $list_on_ship_mrd->etd;
                                                //$receive_date = null;
                                                //echo $qty_mrd. PHP_EOL;
                                                break;
                                                
                                            }
                                        }
                                        

                                    }
                                }
                            }
                        }

                            $list_on_ships = $webpo_new->select("SELECT sum(qty_upload) as qty_upload, kst_etadate, kst_etddate, kst_invoicevendor
                                    FROM material_on_ship_f('$c_order_id','$item_id','f') GROUP BY kst_etadate,kst_etddate,kst_invoicevendor ORDER BY kst_etadate asc , kst_etddate asc");
                            // $list_on_ships = $webpo_new->table('material_on_ship_v')
                            // ->select(db::raw('sum(qty_upload) as qty_upload'), 'kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            // ->where([
                            //     ['item_id', $item_id],
                            //     ['c_order_id', $c_order_id],
                            //     // ['isactive', true],
                            //     ['flag_wms', false]
                            // ])
                            // ->groupBy('kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            // ->orderBy('kst_etadate', 'asc')
                            // ->orderBy('kst_etddate', 'asc')
                            // ->get();

                            //dd($list_on_ships);
                         
                            if(count($list_on_ships) > 0)
                            {
                                $sum_on_ship_web_po = 0;
                                foreach ($list_on_ships as $item) {
                                    $sum_on_ship_web_po += $item->qty_upload;
                                }
                                $temp_qty_on_ship = 0;
                                $outstanding_on_ship = 0;
                                foreach($list_on_ships as $key => $list_on_ship)
                                {
                                    $no_invoice           = $list_on_ship->kst_invoicevendor;
                                    $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
                                    $temp_qty_on_ship += $total_on_ship_web_po;

                                    $sum_all_qty_on_ship = AutoAllocation::where([
                                        ['item_id_source', $item_id],
                                        ['c_order_id', $c_order_id],
                                        //['warehouse_id', $warehouse_id],
                                        ])
                                        ->whereNull('deleted_at')
                                        ->sum('qty_on_ship');

                                    // $sum_qty_on_ship = AutoAllocation::where([
                                    //     ['item_id_source', $item_id],
                                    //     ['c_order_id', $c_order_id],
                                    //     ['warehouse_id', $warehouse_id],
                                    //     ['no_invoice', $no_invoice],
                                    //     ])
                                    //     ->whereNull('deleted_at')
                                    //     ->sum('qty_on_ship');
        
                                    // $qty_on_ship_web_po += $sisa_qty_on_ship_web_po;
                                    
                                    //sisa qty on ship
                                    if(sprintf('%0.4f', $sum_all_qty_on_ship) - $temp_qty_on_ship > 0)
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        $outstanding_on_ship =  $temp_qty_on_ship - sprintf('%0.4f', $sum_all_qty_on_ship);
                                    }
                                    
                                    //echo $no_invoice. ' total  '. $total_on_ship_web_po. ' sisa '.$outstanding_on_ship. PHP_EOL;

                                    //echo (sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship)). PHP_EOL;
                                    if(sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship) != 0)
                                    {
                                        if($outstanding_on_ship > 0)
                                        {
                                            //jika material yang tersedia lebih dari atau sama dengan alokasi
                                            if($outstanding_on_ship >= ($qty_need - $qty_in_house))
                                            {
                                                $qty_on_ship              = $qty_need - $qty_in_house;
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                $receive_date             = $receive_date;
                                                //echo $qty_on_ship. PHP_EOL;
                                                break;
                                            }
                                            else
                                            {
                                                $qty_on_ship       += $outstanding_on_ship;
                                                if($qty_on_ship >= ($qty_need - $qty_in_house))
                                                {
                                                    $qty_on_ship  = $qty_need - $qty_in_house;
                                                    $eta_date     = $list_on_ship->kst_etadate;
                                                    $etd_date     = $list_on_ship->kst_etddate;
                                                    $receive_date = $receive_date;
                                                    //echo $qty_on_ship. PHP_EOL;
                                                    break;
                                                    
                                                }
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                $receive_date             = $receive_date;
                                                //echo $qty_on_ship. PHP_EOL;
                                            }
                                            

                                        }
                                    }
                                    // else
                                    // {
                                    //     $qty_on_ship  = 0;
                                    //     $eta_date     = null;
                                    //     $etd_date     = null;
                                    //     $receive_date = null;
                                    // }    
                                }
                            }
                            else
                            {
                                $list_on_ships = $webpo_new->select("SELECT sum(qty_upload) as qty_upload, kst_etadate, kst_etddate, kst_invoicevendor, wms_receiving_date
                                    FROM material_on_ship_f('$c_order_id','$item_id','t') GROUP BY kst_etadate, kst_etddate, kst_invoicevendor, wms_receiving_date ORDER BY kst_etadate asc , kst_etddate asc");
                                    
                                        $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                        ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                        ->where([
                                            ['m_product_id', $item_id],
                                            ['c_order_id', $c_order_id],
                                        ])
                                        ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                        ->orderBy('mrd', 'asc')
                                        ->orderBy('dd_pi', 'asc')
                                        ->get();
                                        
                                        //dd($list_on_ship_mrds);
                                        $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                        if(count($list_on_ship_mrds) > 0)
                                        {
                                            $temp_qty_on_ship_mrd = 0;
                                            foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                            {
                                                $sisa = 0;
                                                $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                                $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                                $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                                    ['item_id_source', $item_id],
                                                    ['c_order_id', $c_order_id],
                                                    //['warehouse_id', $warehouse_id],
                                                    ])
                                                    ->whereNull('deleted_at')
                                                    ->sum('qty_mrd');
                                                
                                                    
                                                if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                                {
                                                    continue;
                                                }
                                                else
                                                {
                                                    $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                                }

                                                if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                                {
                                                    if($outstanding_on_ship_mrd > 0)
                                                    {
                                                        //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                        if($outstanding_on_ship_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                            break;
                                                        }
                                                        else
                                                        {
                                                            $qty_mrd       += $outstanding_on_ship_mrd;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            $receive_date = $receive_date;

                                                            if($qty_mrd >= $qty_need)
                                                            {
                                                                $qty_mrd      = $qty_need;
                                                                $mrd          = $list_on_ship_mrd->mrd;
                                                                $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                                $eta_pi       = $list_on_ship_mrd->eta;
                                                                $etd_pi       = $list_on_ship_mrd->etd;
                                                                //$receive_date = null;
                                                                //echo $qty_mrd. PHP_EOL;
                                                                break;
                                                                
                                                            }
                                                            //echo $qty_mrd. PHP_EOL;
                                                        }
                                                        

                                                    }
                                                }
                                            }
                                        }

                                    

                                    
                                //}
                            }

                        //}

                    }

                }
                else
                {
                    $qty_in_house = 0;
                    //cek qty on ship di web po 
                    // $app_env = Config::get('app.env');
                    // if($app_env == 'live')
                    //     {
                            //$data = DB::connection('web_po');
                            // dd($item_id, $c_order_id);
                            $list_on_ships = $webpo_new->select("SELECT sum(qty_upload) as qty_upload, kst_etadate, kst_etddate, kst_invoicevendor
                            FROM material_on_ship_f('$c_order_id','$item_id','f') GROUP BY kst_etadate,kst_etddate,kst_invoicevendor ORDER BY kst_etadate asc , kst_etddate asc");
                            // $list_on_ships = $webpo_new->table('material_on_ship_v')
                            // ->select(db::raw('sum(qty_upload) as qty_upload'), 'kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            // ->where([
                            //     ['item_id', $item_id],
                            //     ['c_order_id', $c_order_id],
                            //     // ['isactive', true],//ganti dulu 
                            //     ['flag_wms', false]
                            // ])
                            // ->groupBy('kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            // ->orderBy('kst_etadate', 'asc')
                            // ->orderBy('kst_etddate', 'asc')
                            // ->get();

                           
                         
                            if(count($list_on_ships) > 0)
                            {
                                $sum_on_ship_web_po = 0;
                                foreach ($list_on_ships as $item) {
                                    $sum_on_ship_web_po += $item->qty_upload;
                                }
                                $temp_qty_on_ship = 0;
                                foreach($list_on_ships as $key => $list_on_ship)
                                {
                                    $sisa = 0;
                                    $no_invoice           = $list_on_ship->kst_invoicevendor;
                                    $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
                                    $temp_qty_on_ship += $total_on_ship_web_po;

                                    $sum_all_qty_on_ship = AutoAllocation::where([
                                        ['item_id_source', $item_id],
                                        ['c_order_id', $c_order_id],
                                        //['warehouse_id', $warehouse_id],
                                        ])
                                        ->whereNull('deleted_at')
                                        ->sum('qty_on_ship');

                                    // $sum_qty_on_ship = AutoAllocation::where([
                                    //     ['item_id_source', $item_id],
                                    //     ['c_order_id', $c_order_id],
                                    //     ['warehouse_id', $warehouse_id],
                                    //     ['no_invoice', $no_invoice],
                                    //     ])
                                    //     ->whereNull('deleted_at')
                                    //     ->sum('qty_on_ship');
                                       
                                    if(sprintf('%0.4f', $sum_all_qty_on_ship) - $temp_qty_on_ship > 0)
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        $outstanding_on_ship =  $temp_qty_on_ship - sprintf('%0.4f', $sum_all_qty_on_ship);
                                    }

                                    //$outstanding_on_ship = sprintf('%0.4f',$total_on_ship_web_po) - sprintf('%0.4f',$sum_qty_on_ship);
                                    
                                    //echo $no_invoice. ' total  '. $total_on_ship_web_po. ' sisa '.$outstanding_on_ship. PHP_EOL;

                                    //echo (sprintf('%0.4f',$sum_on_ship_web_po) - sprintf('%0.4f', $sum_all_qty_on_ship)). PHP_EOL;
                                    if(sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship) != 0)
                                    {
                                        if($outstanding_on_ship > 0)
                                        {
                                            //jika material yang tersedia lebih dari atau sama dengan alokasi
                                            if($outstanding_on_ship >= ($qty_need - $qty_in_house))
                                            {
                                                $qty_on_ship              = $qty_need - $qty_in_house;
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                $receive_date             = null;
                                                //echo $qty_on_ship. PHP_EOL;
                                                break;
                                            }
                                            else
                                            {
                                                $qty_on_ship       += $outstanding_on_ship;
                                                if($qty_on_ship >= ($qty_need - $qty_in_house))
                                                {
                                                    $qty_on_ship  = $qty_need - $qty_in_house;
                                                    $eta_date     = $list_on_ship->kst_etadate;
                                                    $etd_date     = $list_on_ship->kst_etddate;
                                                    //$receive_date = null;
                                                    //echo $qty_on_ship. PHP_EOL;
                                                    break;
                                                    
                                                }
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                $receive_date             = $receive_date;
                                                //echo $qty_on_ship. PHP_EOL;
                                            }
                                            

                                        }
                                    }
                                    // else
                                    // {
                                    //     $qty_on_ship  = 0;
                                    //     $eta_date     = null;
                                    //     $etd_date     = null;
                                    //     $receive_date = null;
                                    // }    
                                }
                                //get mrd
                                $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();
                                    
                                    // dd($list_on_ship_mrds);
                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                            $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            
                                                
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }

                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        //$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        $receive_date = $receive_date;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_mrd. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                        //echo $qty_mrd. PHP_EOL;
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }
                            }
                            else
                            {
                                $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();
                            

                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');
                                    // dd($sum_on_ship_mrds);
                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_ship_web_po;
                                        
                                            $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                ['status_po_buyer', 'active'] //permintaan elva karena kemakan po cancel 
                                            //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            
                                            // dd(sprintf('%0.4f', $sum_all_qty_on_ship_mrd)- $temp_qty_on_ship_mrd);
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }
                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        ////$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        $receive_date = $receive_date;
                                                        //echo $qty_on_ship. PHP_EOL;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_on_ship. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }
                                
                                //}
                            }

                    //}
                }

            }
                // dd($qty_need , $qty_in_house , $qty_on_ship);
                //kalau parsial masih ada qty open
                if(sprintf('%0.4f', $qty_need - $qty_in_house - $qty_on_ship) > 0.001)
                {
                    $eta_date     = null;
                    $etd_date     = null;
                    $receive_date = null;
                    $no_invoice   = null;
                }
                
                $auto_allocation_update               = AutoAllocation::find($auto_allocation_id);
                $auto_allocation_update->qty_in_house = $qty_in_house;
                $auto_allocation_update->qty_on_ship  = $qty_on_ship;
                if($auto_allocation_update->eta_actual != null)
                {
                    if($auto_allocation_update->eta_actual != $eta_date)
                    {
                        $auto_allocation_update->remark_delay = $auto_allocation_update->remark_delay.' Delay Pengiriman '.$change_date;
                    }
                }
                $auto_allocation_update->eta_actual   = $eta_date;
                $auto_allocation_update->etd_actual   = $etd_date;
                $auto_allocation_update->eta_date     = $eta_pi;
                $auto_allocation_update->etd_date     = $etd_pi;
                $auto_allocation_update->mrd          = $mrd;
                $auto_allocation_update->dd_pi        = $dd_pi;
                $auto_allocation_update->qty_mrd      = $qty_mrd;
                $auto_allocation_update->receive_date = $receive_date;
                $auto_allocation_update->no_invoice   = $no_invoice;
                $auto_allocation_update->save();

            }

                DB::commit();

                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                }

    } 


    static function dailyUpdateMonitoringMaterial($c_order_id, $item_id, $warehouse_id)
    {
        //$warehouse_id = ['1000011','1000001'];

        //list kalkulasi
        $auto_allocations = db::table('recalculate_dashboard_material_readinees_v')
        ->where('is_additional',false)
        ->where('lc_date', '>=', '2020-06-01')
        ->where('qty_allocated', '>', '0')
        ->where(db::raw("qty_allocated - COALESCE(qty_in_house, 0)"), '>' ,'0')
        ->where('c_order_id', $c_order_id)  
        ->where('item_id_source', $item_id)
        ->where('warehouse_id',$warehouse_id)
        ->orderBy('statistical_date', 'asc')
        ->orderBy('promise_date', 'asc')
        ->orderBy('qty_allocation', 'asc')
        ->get();

        $app_env = Config::get('app.env');
        if($app_env == 'live')
        {
            $data_erp = DB::connection('erp');
            $data     = DB::connection('web_po');
            $webpo_new = DB::connection('webpo_new');
        }

        try 
        {
            DB::beginTransaction();

            $reset_alocation = AutoAllocation::where('is_additional',false)
            //->whereBetween('lc_date', [$start_date, $end_date])
            ->where('lc_date', '>=', '2020-06-01')
            //->where('document_no', 'POA22007A/E0396')
            ->where('qty_allocated', '>', '0')
            ->where(db::raw("qty_allocated - COALESCE(qty_in_house, 0)"), '>' ,'0')
            ->where('c_order_id', $c_order_id)  
            ->where('item_id_source', $item_id)
            ->where('warehouse_id',$warehouse_id)
            ->whereNull('deleted_at')
            ->update([
                'qty_in_house' => null,
                'qty_on_ship'  => null,
                'qty_mrd'      => null,
                // 'eta_actual'   => null,
                // 'eta_actual'   => null,
                // 'no_invoice'   => null,
                // 'receive_date' => null,
                // 'mrd'          => null,
                // 'dd_pi'        => null,
                // 'eta_date'     => null,
                // 'etd_date'     => null,
            ]);

            foreach($auto_allocations as $key => $auto_allocation)
            {   
                $eta_date     = null;
                $etd_date     = null;
                $receive_date = null;
                $no_invoice   = null;
                $qty_in_house = 0;
                $qty_on_ship  = 0;
                $qty_mrd      = 0;
                $mrd          = null;
                $dd_pi        = null;
                $eta_pi       = null;
                $etd_pi       = null;

                $auto_allocation_id = $auto_allocation->id;
                $item_id            = $auto_allocation->item_id_source;
                $warehouse_id       = $auto_allocation->warehouse_id;
                $c_order_id         = $auto_allocation->c_order_id;
                $lc_date             = $auto_allocation->lc_date;
                $qty_need           = sprintf('%0.4f',$auto_allocation->qty_allocation);
                $qty_on_ship_alocation = sprintf('%0.4f',$auto_allocation->qty_on_ship);
                echo $auto_allocation_id . "\n";
                if($c_order_id == 'FREE STOCK')
                {
                    $qty_in_house = $qty_need;
                    $eta_actual   = $lc_date;
                    $etd_actual   = $lc_date;
                    $receive_date = $lc_date;
                    $no_invoice   = null;

                }
                else
                {

                $material_stock = MaterialStock::select(db::raw('sum(stock) as stock'), db::raw('max(created_at) as created_at'))
                ->where([
                ['item_id', $item_id],
                ['c_order_id', $c_order_id],
                ['is_stock_mm', false],
                ['is_closing_balance', false],
                ['approval_date', '!=', null],
                ['is_from_handover', false],
                ['is_master_roll', true],
                //['warehouse_id', $warehouse_id], tidak cek warehouse
                ])
                ->whereNull('case')
                ->first();
                //->whereNull('deleted_at')
                //->sum('stock');
                // dd($material_stock);
                $sum_qty_stock = $material_stock->stock;
                $receive_date    = $material_stock->created_at;
                //$no_invoice    = $material_stock->no_invoice;

                $sum_qty_in_house = AutoAllocation::where([
                    ['item_id_source', $item_id],
                    ['c_order_id', $c_order_id],
                    //['warehouse_id', $warehouse_id],
                    ])
                    ->whereNull('deleted_at')
                    ->sum('qty_in_house');

                //sisa qty in house 
                $material_in_house = sprintf('%0.4f',$sum_qty_stock) - sprintf('%0.4f',$sum_qty_in_house);
                // dd($material_in_house);
                //jika material yang tersdia > 0
                
                if($material_in_house > 0)
                {
                    //dd($material_in_house - $qty_need);
                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                    //dd($material_in_house/$qty_need);
                    if(sprintf('%0.4f',$material_in_house)/$qty_need >= 1)
                    {
                        $qty_in_house = $qty_need;
                        $qty_on_ship  = 0;

                        //$app_env = Config::get('app.env');
                        // if($app_env == 'live')
                        // {
                            //$data = DB::connection('web_po');
                            $list_on_ships = $webpo_new->select("SELECT sum(qty_upload) as qty_upload, kst_etadate, kst_etddate, kst_invoicevendor
                                                        FROM material_on_ship_f('$c_order_id','$item_id','t') GROUP BY kst_etadate,kst_etddate,kst_invoicevendor ORDER BY kst_etadate asc , kst_etddate asc");
                            // $list_on_ships = $webpo_new->table('material_on_ship_v')
                            // ->select(db::raw('sum(qty_upload) as qty_upload'), 'kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            // ->where([
                            //     ['item_id', $item_id],
                            //     ['c_order_id', $c_order_id],
                            //     // ['isactive', true],//ganti dulu 
                            //     ['flag_wms', true]
                            // ])
                            // ->groupBy('kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            // ->orderBy('kst_etadate', 'asc')
                            // ->orderBy('kst_etddate', 'asc')
                            // ->get();

                           
                            // dd($list_on_ships);

                            if(count($list_on_ships) > 0)
                            {
                                $sum_on_ship_web_po = 0;
                                foreach ($list_on_ships as $item) {
                                    $sum_on_ship_web_po += $item->qty_upload;
                                }
                                $temp_qty_on_ship = 0;
                                foreach($list_on_ships as $key => $list_on_ship)
                                {
                                    $sisa = 0;
                                    $no_invoice           = $list_on_ship->kst_invoicevendor;
                                    $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
                                    $temp_qty_on_ship += $total_on_ship_web_po;

                                    $sum_all_qty_on_ship = AutoAllocation::where([
                                        ['item_id_source', $item_id],
                                        ['c_order_id', $c_order_id],
                                        //['warehouse_id', $warehouse_id],
                                        ])
                                        ->whereNull('deleted_at')
                                        ->sum('qty_on_ship');

                                   
                                       
                                    if(sprintf('%0.4f', $sum_all_qty_on_ship) - $temp_qty_on_ship > 0 )
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        $outstanding_on_ship =  $temp_qty_on_ship - sprintf('%0.4f', $sum_all_qty_on_ship);
                                    }

                                    //$outstanding_on_ship = sprintf('%0.4f',$total_on_ship_web_po) - sprintf('%0.4f',$sum_qty_on_ship);
                                    
                                    //echo $no_invoice. ' total  '. $total_on_ship_web_po. ' sisa '.$outstanding_on_ship. PHP_EOL;

                                    //echo (sprintf('%0.4f',$sum_on_ship_web_po) - sprintf('%0.4f', $sum_all_qty_on_ship)). PHP_EOL;
                                    if(sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship) != 0)
                                    {
                                        if($outstanding_on_ship > 0)
                                        {
                                            //jika material yang tersedia lebih dari atau sama dengan alokasi
                                            if($outstanding_on_ship >= ($qty_need - $qty_in_house))
                                            {
                                                $qty_on_ship              = $qty_need - $qty_in_house;
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                //$receive_date             = null;
                                                //echo $qty_on_ship. PHP_EOL;
                                                break;
                                            }
                                            else
                                            {
                                                $qty_on_ship       += $outstanding_on_ship;
                                                if($qty_on_ship >= ($qty_need - $qty_in_house))
                                                {
                                                    $qty_on_ship  = $qty_need - $qty_in_house;
                                                    $eta_date     = $list_on_ship->kst_etadate;
                                                    $etd_date     = $list_on_ship->kst_etddate;
                                                    //$receive_date = null;
                                                    //echo $qty_on_ship. PHP_EOL;
                                                    break;
                                                    
                                                }
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                //$receive_date             = $receive_date;
                                                //echo $qty_on_ship. PHP_EOL;
                                            }
                                            

                                        }
                                    }
                                    // else
                                    // {
                                    //     $qty_on_ship  = 0;
                                    //     $eta_date     = null;
                                    //     $etd_date     = null;
                                    //     $receive_date = null;
                                    // }    
                                }
                                //get mrd
                                $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();
                                    
                                    //dd($list_on_ship_mrds);
                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                            $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                ['status_po_buyer', 'active'] //permintaan elva karena kemakan po cancel 
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            
                                                
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }

                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        //$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_mrd. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                        //$receive_date = $receive_date;
                                                        //echo $qty_mrd. PHP_EOL;
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }
                            }
                        //dd($no_invoice);

                        //$data_erp = DB::connection('erp');

                        // $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                        // ->select(db::raw('max(mrd) as mrd, max(dd_pi) as dd_pi, max(eta) as eta, max(etd) as etd'))
                        // ->where([
                        //     ['m_product_id', $item_id],
                        //     ['c_order_id', $c_order_id],
                        // ])
                        // ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                        // ->orderBy('eta', 'desc')
                        // ->orderBy('etd', 'desc')
                        // ->first();

                        // //dd($list_on_ship_mrds);
                        // if($list_on_ship_mrds)
                        // {
                        //     $mrd          = $list_on_ship_mrds->mrd;
                        //     $dd_pi        = $list_on_ship_mrds->dd_pi;
                        //     $eta_pi       = $list_on_ship_mrds->eta;
                        //     $etd_pi       = $list_on_ship_mrds->etd;
                        // }

                                    $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();
                                    
                                    //dd($list_on_ship_mrds);
                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                            $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            
                                                
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }

                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        //$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        $receive_date = $receive_date;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_mrd. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                        //echo $qty_mrd. PHP_EOL;
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }

                    }
                    else
                    {
                        $qty_in_house = $material_in_house;
                        //cek kekurangan di web po
                        // $app_env = Config::get('app.env');
                        // if($app_env == 'live')
                        // {
                        //     $data = DB::connection('web_po');
                        $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                        ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                        ->where([
                            ['m_product_id', $item_id],
                            ['c_order_id', $c_order_id],
                        ])
                        ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                        ->orderBy('mrd', 'asc')
                        ->orderBy('dd_pi', 'asc')
                        ->get();
                        
                        //dd($list_on_ship_mrds);
                        $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                        if(count($list_on_ship_mrds) > 0)
                        {
                            $temp_qty_on_ship_mrd = 0;
                            foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                            {
                                $sisa = 0;
                                $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                    ['item_id_source', $item_id],
                                    ['c_order_id', $c_order_id],
                                    ['warehouse_id', $warehouse_id],
                                    ])
                                    ->whereNull('deleted_at')
                                    ->sum('qty_mrd');
                                
                                    
                                if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                {
                                    continue;
                                }
                                else
                                {
                                    $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                }

                                if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                {
                                    if($outstanding_on_ship_mrd > 0)
                                    {
                                        //jika material yang tersedia lebih dari atau sama dengan alokasi
                                        if($outstanding_on_ship_mrd >= $qty_need)
                                        {
                                            $qty_mrd      = $qty_need;
                                            $mrd          = $list_on_ship_mrd->mrd;
                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                            $eta_pi       = $list_on_ship_mrd->eta;
                                            $etd_pi       = $list_on_ship_mrd->etd;
                                            //$receive_date = null;
                                            //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                            break;
                                        }
                                        else
                                        {
                                            $qty_mrd       += $outstanding_on_ship_mrd;
                                            $mrd          = $list_on_ship_mrd->mrd;
                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                            $eta_pi       = $list_on_ship_mrd->eta;
                                            $etd_pi       = $list_on_ship_mrd->etd;
                                            $receive_date = $receive_date;
                                            //echo $qty_mrd. PHP_EOL;

                                            if($qty_mrd >= $qty_need)
                                            {
                                                $qty_mrd      = $qty_need;
                                                $mrd          = $list_on_ship_mrd->mrd;
                                                $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                $eta_pi       = $list_on_ship_mrd->eta;
                                                $etd_pi       = $list_on_ship_mrd->etd;
                                                //$receive_date = null;
                                                //echo $qty_mrd. PHP_EOL;
                                                break;
                                                
                                            }
                                        }
                                        

                                    }
                                }
                            }
                        }

                            $list_on_ships = $webpo_new->select("SELECT sum(qty_upload) as qty_upload, kst_etadate, kst_etddate, kst_invoicevendor
                                    FROM material_on_ship_f('$c_order_id','$item_id','f') GROUP BY kst_etadate,kst_etddate,kst_invoicevendor ORDER BY kst_etadate asc , kst_etddate asc");
                            // $list_on_ships = $webpo_new->table('material_on_ship_v')
                            // ->select(db::raw('sum(qty_upload) as qty_upload'), 'kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            // ->where([
                            //     ['item_id', $item_id],
                            //     ['c_order_id', $c_order_id],
                            //     // ['isactive', true],
                            //     ['flag_wms', false]
                            // ])
                            // ->groupBy('kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            // ->orderBy('kst_etadate', 'asc')
                            // ->orderBy('kst_etddate', 'asc')
                            // ->get();

                            //dd($list_on_ships);
                         
                            if(count($list_on_ships) > 0)
                            {
                                $sum_on_ship_web_po = 0;
                                foreach ($list_on_ships as $item) {
                                    $sum_on_ship_web_po += $item->qty_upload;
                                }
                                $temp_qty_on_ship = 0;
                                $outstanding_on_ship = 0;
                                foreach($list_on_ships as $key => $list_on_ship)
                                {
                                    $no_invoice           = $list_on_ship->kst_invoicevendor;
                                    $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
                                    $temp_qty_on_ship += $total_on_ship_web_po;

                                    $sum_all_qty_on_ship = AutoAllocation::where([
                                        ['item_id_source', $item_id],
                                        ['c_order_id', $c_order_id],
                                        //['warehouse_id', $warehouse_id],
                                        ])
                                        ->whereNull('deleted_at')
                                        ->sum('qty_on_ship');

                                    // $sum_qty_on_ship = AutoAllocation::where([
                                    //     ['item_id_source', $item_id],
                                    //     ['c_order_id', $c_order_id],
                                    //     ['warehouse_id', $warehouse_id],
                                    //     ['no_invoice', $no_invoice],
                                    //     ])
                                    //     ->whereNull('deleted_at')
                                    //     ->sum('qty_on_ship');
        
                                    // $qty_on_ship_web_po += $sisa_qty_on_ship_web_po;
                                    
                                    //sisa qty on ship
                                    if(sprintf('%0.4f', $sum_all_qty_on_ship) - $temp_qty_on_ship > 0)
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        $outstanding_on_ship =  $temp_qty_on_ship - sprintf('%0.4f', $sum_all_qty_on_ship);
                                    }
                                    
                                    //echo $no_invoice. ' total  '. $total_on_ship_web_po. ' sisa '.$outstanding_on_ship. PHP_EOL;

                                    //echo (sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship)). PHP_EOL;
                                    if(sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship) != 0)
                                    {
                                        if($outstanding_on_ship > 0)
                                        {
                                            //jika material yang tersedia lebih dari atau sama dengan alokasi
                                            if($outstanding_on_ship >= ($qty_need - $qty_in_house))
                                            {
                                                $qty_on_ship              = $qty_need - $qty_in_house;
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                $receive_date             = $receive_date;
                                                //echo $qty_on_ship. PHP_EOL;
                                                break;
                                            }
                                            else
                                            {
                                                $qty_on_ship       += $outstanding_on_ship;
                                                if($qty_on_ship >= ($qty_need - $qty_in_house))
                                                {
                                                    $qty_on_ship  = $qty_need - $qty_in_house;
                                                    $eta_date     = $list_on_ship->kst_etadate;
                                                    $etd_date     = $list_on_ship->kst_etddate;
                                                    $receive_date = $receive_date;
                                                    //echo $qty_on_ship. PHP_EOL;
                                                    break;
                                                    
                                                }
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                $receive_date             = $receive_date;
                                                //echo $qty_on_ship. PHP_EOL;
                                            }
                                            

                                        }
                                    }
                                    // else
                                    // {
                                    //     $qty_on_ship  = 0;
                                    //     $eta_date     = null;
                                    //     $etd_date     = null;
                                    //     $receive_date = null;
                                    // }    
                                }
                            }
                            else
                            {
                                $list_on_ships = $webpo_new->select("SELECT sum(qty_upload) as qty_upload, kst_etadate, kst_etddate, kst_invoicevendor, wms_receiving_date
                                    FROM material_on_ship_f('$c_order_id','$item_id','t') GROUP BY kst_etadate, kst_etddate, kst_invoicevendor, wms_receiving_date,qtyordered ORDER BY kst_etadate asc , kst_etddate asc");
                                    // $qty_in_house = $qty_need;
                                    if(count($list_on_ships) > 0)
                                    {
                                        $sum_on_ship_web_po = 0;
                                        foreach ($list_on_ships as $item) {
                                            $sum_on_ship_web_po += $item->qty_upload;
                                        }
                                        $temp_qty_on_ship = 0;
                                        
                                        foreach($list_on_ships as $key => $list_on_ship)
                                        {
                                            

                                            $sisa = 0;
                                            $no_invoice           = $list_on_ship->kst_invoicevendor;
                                            $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
                                            $temp_qty_on_ship += $total_on_ship_web_po;

                                            $sum_all_qty_on_ship = AutoAllocation::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_on_ship');

                                            if(sprintf('%0.4f', $sum_all_qty_on_ship) - $temp_qty_on_ship > 0 )
                                            {
                                                
                                                continue;

                                            }
                                            else
                                            {
                                                $outstanding_on_ship =  $temp_qty_on_ship - sprintf('%0.4f', $sum_all_qty_on_ship);
                                            }

                                            //$outstanding_on_ship = sprintf('%0.4f',$total_on_ship_web_po) - sprintf('%0.4f',$sum_qty_on_ship);
                                            
                                            //echo $no_invoice. ' total  '. $total_on_ship_web_po. ' sisa '.$outstanding_on_ship. PHP_EOL;

                                            // echo (sprintf('%0.4f',$sum_on_ship_web_po) - sprintf('%0.4f', $sum_all_qty_on_ship)). PHP_EOL;
                                            if(sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship) != 0)
                                            {
                                                if($outstanding_on_ship > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship >= ($qty_need - $qty_in_house))
                                                    {
                                                       
                                                            $qty_on_ship              = $qty_need - $qty_in_house;
                                                            $eta_date                 = $list_on_ship->kst_etadate;
                                                            $etd_date                 = $list_on_ship->kst_etddate;
                                                            
                                                            $receive_date             = null;
                                                        
                                                       
                                                        
                                                        //echo $qty_on_ship. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_on_ship       += $outstanding_on_ship;
                                                        if($qty_on_ship >= ($qty_need - $qty_in_house))
                                                        {
                                                            $qty_on_ship  = $qty_need - $qty_in_house;
                                                            $eta_date     = $list_on_ship->kst_etadate;
                                                            $etd_date     = $list_on_ship->kst_etddate;
                                                            //$receive_date = null;
                                                            //echo $qty_on_ship. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                        $eta_date                 = $list_on_ship->kst_etadate;
                                                        $etd_date                 = $list_on_ship->kst_etddate;
                                                        //$receive_date             = $receive_date;
                                                        //echo $qty_on_ship. PHP_EOL;
                                                    }
                                                    

                                                }
                                            }
                                            // else
                                            // {
                                            //     $qty_on_ship  = 0;
                                            //     $eta_date     = null;
                                            //     $etd_date     = null;
                                            //     $receive_date = null;
                                            // }    
                                        }
                                        
                                        // foreach($list_on_ships as $key => $list_on_ship)
                                        // {
                                        //     $qty_in_house = $qty_need;
                                        //     $no_invoice   = $list_on_ship->kst_invoicevendor;
                                        //     $eta_date                 = $list_on_ship->kst_etadate;
                                        //     $etd_date                 = $list_on_ship->kst_etddate;
                                        //     $sisa = 0;
                                        //     $no_invoice           = $list_on_ship->kst_invoicevendor;
                                        //     $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
                                        //     // $temp_qty_on_ship += $total_on_ship_web_po;
                                        //     $receive_date = $list_on_ship->wms_receiving_date;
    
                                        // }
                                        //get mrd
                                        $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                            ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                            ->where([
                                                ['m_product_id', $item_id],
                                                ['c_order_id', $c_order_id],
                                            ])
                                            ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                            ->orderBy('mrd', 'asc')
                                            ->orderBy('dd_pi', 'asc')
                                            ->get();
                                            
                                            // dd($list_on_ship_mrds);
                                            $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                            if(count($list_on_ship_mrds) > 0)
                                            {
                                                $temp_qty_on_ship_mrd = 0;
                                                foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                                {
                                                    $sisa = 0;
                                                    $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                                    $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                                    $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                                        ['item_id_source', $item_id],
                                                        ['c_order_id', $c_order_id],
                                                        //['warehouse_id', $warehouse_id],
                                                        ])
                                                        ->whereNull('deleted_at')
                                                        ->sum('qty_mrd');
                                                    
                                                        
                                                    if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                                    {
                                                        continue;
                                                    }
                                                    else
                                                    {
                                                        $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                                    }

                                                    if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                                    {
                                                        if($outstanding_on_ship_mrd > 0)
                                                        {
                                                            //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                            if($outstanding_on_ship_mrd >= $qty_need)
                                                            {
                                                                $qty_mrd      = $qty_need;
                                                                $mrd          = $list_on_ship_mrd->mrd;
                                                                $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                                $eta_pi       = $list_on_ship_mrd->eta;
                                                                $etd_pi       = $list_on_ship_mrd->etd;
                                                                //$receive_date = null;
                                                                //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                                break;
                                                            }
                                                            else
                                                            {
                                                                $qty_mrd       += $outstanding_on_ship_mrd;
                                                                $mrd          = $list_on_ship_mrd->mrd;
                                                                $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                                $eta_pi       = $list_on_ship_mrd->eta;
                                                                $etd_pi       = $list_on_ship_mrd->etd;
                                                                $receive_date = $receive_date;

                                                                if($qty_mrd >= $qty_need)
                                                                {
                                                                    $qty_mrd      = $qty_need;
                                                                    $mrd          = $list_on_ship_mrd->mrd;
                                                                    $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                                    $eta_pi       = $list_on_ship_mrd->eta;
                                                                    $etd_pi       = $list_on_ship_mrd->etd;
                                                                    //$receive_date = null;
                                                                    //echo $qty_mrd. PHP_EOL;
                                                                    break;
                                                                    
                                                                }
                                                                //echo $qty_mrd. PHP_EOL;
                                                            }
                                                            

                                                        }
                                                    }
                                                }
                                            }
                                    }else{
                                        $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                        ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                        ->where([
                                            ['m_product_id', $item_id],
                                            ['c_order_id', $c_order_id],
                                        ])
                                        ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                        ->orderBy('mrd', 'asc')
                                        ->orderBy('dd_pi', 'asc')
                                        ->get();
                                        
                                        //dd($list_on_ship_mrds);
                                        $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                        if(count($list_on_ship_mrds) > 0)
                                        {
                                            $temp_qty_on_ship_mrd = 0;
                                            foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                            {
                                                $sisa = 0;
                                                $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                                $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                                $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                                    ['item_id_source', $item_id],
                                                    ['c_order_id', $c_order_id],
                                                    //['warehouse_id', $warehouse_id],
                                                    ])
                                                    ->whereNull('deleted_at')
                                                    ->sum('qty_mrd');
                                                
                                                    
                                                if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                                {
                                                    continue;
                                                }
                                                else
                                                {
                                                    $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                                }

                                                if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                                {
                                                    if($outstanding_on_ship_mrd > 0)
                                                    {
                                                        //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                        if($outstanding_on_ship_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                            break;
                                                        }
                                                        else
                                                        {
                                                            $qty_mrd       += $outstanding_on_ship_mrd;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            $receive_date = $receive_date;

                                                            if($qty_mrd >= $qty_need)
                                                            {
                                                                $qty_mrd      = $qty_need;
                                                                $mrd          = $list_on_ship_mrd->mrd;
                                                                $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                                $eta_pi       = $list_on_ship_mrd->eta;
                                                                $etd_pi       = $list_on_ship_mrd->etd;
                                                                //$receive_date = null;
                                                                //echo $qty_mrd. PHP_EOL;
                                                                break;
                                                                
                                                            }
                                                            //echo $qty_mrd. PHP_EOL;
                                                        }
                                                        

                                                    }
                                                }
                                            }
                                        }

                                    }

                                    
                                //}
                            }

                        //}

                    }

                }
                else
                {
                    $qty_in_house = 0;
                    //cek qty on ship di web po 
                    // $app_env = Config::get('app.env');
                    // if($app_env == 'live')
                    //     {
                            //$data = DB::connection('web_po');
                            // dd($item_id, $c_order_id);
                            $list_on_ships = $webpo_new->select("SELECT sum(qty_upload) as qty_upload, kst_etadate, kst_etddate, kst_invoicevendor
                            FROM material_on_ship_f('$c_order_id','$item_id','f') GROUP BY kst_etadate,kst_etddate,kst_invoicevendor ORDER BY kst_etadate asc , kst_etddate asc");
                            // $list_on_ships = $webpo_new->table('material_on_ship_v')
                            // ->select(db::raw('sum(qty_upload) as qty_upload'), 'kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            // ->where([
                            //     ['item_id', $item_id],
                            //     ['c_order_id', $c_order_id],
                            //     // ['isactive', true],//ganti dulu 
                            //     ['flag_wms', false]
                            // ])
                            // ->groupBy('kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            // ->orderBy('kst_etadate', 'asc')
                            // ->orderBy('kst_etddate', 'asc')
                            // ->get();

                           
                         
                            if(count($list_on_ships) > 0)
                            {
                                $sum_on_ship_web_po = 0;
                                foreach ($list_on_ships as $item) {
                                    $sum_on_ship_web_po += $item->qty_upload;
                                }
                                $temp_qty_on_ship = 0;
                                foreach($list_on_ships as $key => $list_on_ship)
                                {
                                    $sisa = 0;
                                    $no_invoice           = $list_on_ship->kst_invoicevendor;
                                    $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
                                    $temp_qty_on_ship += $total_on_ship_web_po;

                                    $sum_all_qty_on_ship = AutoAllocation::where([
                                        ['item_id_source', $item_id],
                                        ['c_order_id', $c_order_id],
                                        //['warehouse_id', $warehouse_id],
                                        ])
                                        ->whereNull('deleted_at')
                                        ->sum('qty_on_ship');

                                    // $sum_qty_on_ship = AutoAllocation::where([
                                    //     ['item_id_source', $item_id],
                                    //     ['c_order_id', $c_order_id],
                                    //     ['warehouse_id', $warehouse_id],
                                    //     ['no_invoice', $no_invoice],
                                    //     ])
                                    //     ->whereNull('deleted_at')
                                    //     ->sum('qty_on_ship');
                                       
                                    if(sprintf('%0.4f', $sum_all_qty_on_ship) - $temp_qty_on_ship > 0)
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        $outstanding_on_ship =  $temp_qty_on_ship - sprintf('%0.4f', $sum_all_qty_on_ship);
                                    }

                                    //$outstanding_on_ship = sprintf('%0.4f',$total_on_ship_web_po) - sprintf('%0.4f',$sum_qty_on_ship);
                                    
                                    //echo $no_invoice. ' total  '. $total_on_ship_web_po. ' sisa '.$outstanding_on_ship. PHP_EOL;

                                    //echo (sprintf('%0.4f',$sum_on_ship_web_po) - sprintf('%0.4f', $sum_all_qty_on_ship)). PHP_EOL;
                                    if(sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship) != 0)
                                    {
                                        if($outstanding_on_ship > 0)
                                        {
                                            //jika material yang tersedia lebih dari atau sama dengan alokasi
                                            if($outstanding_on_ship >= ($qty_need - $qty_in_house))
                                            {
                                                $qty_on_ship              = $qty_need - $qty_in_house;
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                $receive_date             = null;
                                                //echo $qty_on_ship. PHP_EOL;
                                                break;
                                            }
                                            else
                                            {
                                                $qty_on_ship       += $outstanding_on_ship;
                                                if($qty_on_ship >= ($qty_need - $qty_in_house))
                                                {
                                                    $qty_on_ship  = $qty_need - $qty_in_house;
                                                    $eta_date     = $list_on_ship->kst_etadate;
                                                    $etd_date     = $list_on_ship->kst_etddate;
                                                    //$receive_date = null;
                                                    //echo $qty_on_ship. PHP_EOL;
                                                    break;
                                                    
                                                }
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                $receive_date             = $receive_date;
                                                //echo $qty_on_ship. PHP_EOL;
                                            }
                                            

                                        }
                                    }
                                    // else
                                    // {
                                    //     $qty_on_ship  = 0;
                                    //     $eta_date     = null;
                                    //     $etd_date     = null;
                                    //     $receive_date = null;
                                    // }    
                                }
                                //get mrd
                                $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();
                                    
                                    // dd($list_on_ship_mrds);
                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                            $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            
                                                
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }

                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        //$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        $receive_date = $receive_date;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_mrd. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                        //echo $qty_mrd. PHP_EOL;
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }
                            }
                            else
                            {
                                $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                ->where([
                                    ['m_product_id', $item_id],
                                    ['c_order_id', $c_order_id],
                                ])
                                ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                ->orderBy('mrd', 'asc')
                                ->orderBy('dd_pi', 'asc')
                                ->get();
                            

                                $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');
                                // dd($sum_on_ship_mrds);
                                if(count($list_on_ship_mrds) > 0)
                                {
                                    $temp_qty_on_ship_mrd = 0;
                                    foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                    {
                                        $sisa = 0;
                                        $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                        $temp_qty_on_ship_mrd += $total_on_ship_web_po;
                                    
                                        $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                            ['item_id_source', $item_id],
                                            ['c_order_id', $c_order_id],
                                            ['status_po_buyer', 'active'] //permintaan elva karena kemakan po cancel 
                                         //['warehouse_id', $warehouse_id],
                                            ])
                                            ->whereNull('deleted_at')
                                            ->sum('qty_mrd');
                                        
                                        // dd(sprintf('%0.4f', $sum_all_qty_on_ship_mrd)- $temp_qty_on_ship_mrd);
                                        if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                        {
                                            continue;
                                        }
                                        else
                                        {
                                            $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                        }
                                        if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                        {
                                            if($outstanding_on_ship_mrd > 0)
                                            {
                                                //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                if($outstanding_on_ship_mrd >= $qty_need)
                                                {
                                                    $qty_mrd      = $qty_need;
                                                    $mrd          = $list_on_ship_mrd->mrd;
                                                    $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                    $eta_pi       = $list_on_ship_mrd->eta;
                                                    $etd_pi       = $list_on_ship_mrd->etd;
                                                    ////$receive_date = null;
                                                    //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                    break;
                                                }
                                                else
                                                {
                                                    $qty_mrd       += $outstanding_on_ship_mrd;
                                                    $mrd          = $list_on_ship_mrd->mrd;
                                                    $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                    $eta_pi       = $list_on_ship_mrd->eta;
                                                    $etd_pi       = $list_on_ship_mrd->etd;
                                                    $receive_date = $receive_date;
                                                    //echo $qty_on_ship. PHP_EOL;

                                                    if($qty_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        //$receive_date = null;
                                                        //echo $qty_on_ship. PHP_EOL;
                                                        break;
                                                        
                                                    }
                                                }
                                                

                                            }
                                        }
                                    }
                                }
                                //}
                            }

                    //}
                }

            }
                //kalau parsial masih ada qty open
                if(sprintf('%0.4f', $qty_need - $qty_in_house - $qty_on_ship) > 0.001)
                {
                    $eta_date     = null;
                    $etd_date     = null;
                    $receive_date = null;
                    $no_invoice   = null;
                }
                
                $auto_allocation_update               = AutoAllocation::find($auto_allocation_id);
                $auto_allocation_update->qty_in_house = $qty_in_house;
                $auto_allocation_update->qty_on_ship  = $qty_on_ship;
                if($auto_allocation_update->eta_actual != null)
                {
                    //dd($eta_actual, $eta_date);
                    if($auto_allocation_update->eta_actual != $eta_date)
                    {
                        $auto_allocation_update->remark_delay = $auto_allocation_update->remark_delay.' "; Delay Pengiriman '.$eta_date. ' " ';
                    }
                }
                $auto_allocation_update->eta_actual   = $eta_date;
                $auto_allocation_update->etd_actual   = $etd_date;
                $auto_allocation_update->eta_date     = $eta_pi;
                $auto_allocation_update->etd_date     = $etd_pi;
                $auto_allocation_update->mrd          = $mrd;
                $auto_allocation_update->dd_pi        = $dd_pi;
                $auto_allocation_update->qty_mrd      = $qty_mrd;
                $auto_allocation_update->receive_date = $receive_date;
                $auto_allocation_update->no_invoice   = $no_invoice;
                $auto_allocation_update->save();

            }

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
        }

    } 

    public function export(Request $request)
    {
            $start_date           = ( $request->lc_date_from ? Carbon::createFromFormat('d/m/Y', $request->lc_date_from)->format('Y-m-d') : null);
            $end_date             = ( $request->lc_date_to ? Carbon::createFromFormat('d/m/Y', $request->lc_date_to)->format('Y-m-d') : null);
            $start_statistical_date = ( $request->_start_statistical_date ? Carbon::createFromFormat('d/m/Y', $request->_start_statistical_date)->format('Y-m-d') : null);
            $end_statistical_date   = ( $request->_end_statistical_date ? Carbon::createFromFormat('d/m/Y', $request->_end_statistical_date)->format('Y-m-d') : null);

            // $refresh = DB::select(db::raw("
            // refresh materialized view monitoring_allocations_mv;"
            // ));

            if(auth::user()->hasRole(['mm-staff','admin-ict-fabric']))
            {
                $warehouse_id = ['1000011','1000001'];
                if(($start_date != null and $end_date != null) && ($start_statistical_date == null and $end_statistical_date == null))
                {
                    $auto_allocations = db::table('monitoring_allocations_v')
                    ->where('is_additional',false)
                    ->whereBetween('lc_date', [$start_date, $end_date])
                    ->whereIn('warehouse_id',$warehouse_id)
                    ->orderBy('statistical_date', 'asc')
                    ->orderBy('promise_date', 'asc')
                    ->orderBy('qty_allocation', 'asc')
                    ->get();
                }
                else if (($start_statistical_date != null and $end_statistical_date != null) and ($start_date == null and $end_date == null))
                {
                    $auto_allocations = db::table('monitoring_allocations_v')
                    ->where('is_additional',false)
                    ->whereBetween('statistical_date', [$start_statistical_date, $end_statistical_date])
                    ->whereIn('warehouse_id',$warehouse_id)
                    ->orderBy('statistical_date', 'asc')
                    ->orderBy('promise_date', 'asc')
                    ->orderBy('qty_allocation', 'asc')
                    ->get();
                }
                else
                {
                    $auto_allocations = db::table('monitoring_allocations_v')
                    ->where('is_additional',false)
                    ->whereBetween('lc_date', [$start_date, $end_date])
                    ->whereBetween('statistical_date', [$start_statistical_date, $end_statistical_date])
                    ->whereIn('warehouse_id',$warehouse_id)
                    ->orderBy('statistical_date', 'asc')
                    ->orderBy('promise_date', 'asc')
                    ->orderBy('qty_allocation', 'asc')
                    ->get();
                }
            }
            else
            {
                $warehouse_id = ['1000013','1000002'];
                $app_env = Config::get('app.env');
                if($app_env == 'live')
                {
                    $dashboard_wms = DB::connection('dashboard_wms');
                }
                
                if(($start_date != null and $end_date != null) && ($start_statistical_date == null and $end_statistical_date == null))
                {
                    $auto_allocations = $dashboard_wms->table('auto_allocations')
                    // $auto_allocations = db::table('monitoring_allocations_v')
                    ->where('is_additional',false)
                    ->whereBetween('lc_date', [$start_date, $end_date])
                    ->whereIn('warehouse_id',$warehouse_id)
                    ->orderBy('statistical_date', 'asc')
                    ->orderBy('promise_date', 'asc')
                    ->orderBy('qty_allocation', 'asc')
                    ->get();
                }
                else if (($start_statistical_date != null and $end_statistical_date != null) and ($start_date == null and $end_date == null))
                {
                    $auto_allocations = $dashboard_wms->table('auto_allocations')
                    // $auto_allocations = db::table('monitoring_allocations_v')
                    ->where('is_additional',false)
                    ->whereBetween('statistical_date', [$start_statistical_date, $end_statistical_date])
                    ->whereIn('warehouse_id',$warehouse_id)
                    ->orderBy('statistical_date', 'asc')
                    ->orderBy('promise_date', 'asc')
                    ->orderBy('qty_allocation', 'asc')
                    ->get();
                }
                else
                {
                    $auto_allocations = $dashboard_wms->table('auto_allocations')
                    // $auto_allocations = db::table('monitoring_allocations_v')
                    ->where('is_additional',false)
                    ->whereBetween('lc_date', [$start_date, $end_date])
                    ->whereBetween('statistical_date', [$start_statistical_date, $end_statistical_date])
                    ->whereIn('warehouse_id',$warehouse_id)
                    ->orderBy('statistical_date', 'asc')
                    ->orderBy('promise_date', 'asc')
                    ->orderBy('qty_allocation', 'asc')
                    ->get();
                }

            }

            $file_name = 'DASHBOARD_MONITORING_MATERIAL ';
        return Excel::create($file_name,function($excel) use ($auto_allocations)
        {
            $excel->sheet('active',function($sheet)use($auto_allocations)
            {
                $sheet->setColumnFormat(array(
                    'B' => 'yyyy-mm-dd',
                ));
                
                $sheet->setCellValue('A1','AUTO_ALLOCATION_ID');
                $sheet->setCellValue('B1','LC_DATE');
                $sheet->setCellValue('C1','PO_BUYER');
                $sheet->setCellValue('D1','STYLE');
                $sheet->setCellValue('E1','BRAND');
                $sheet->setCellValue('F1','WAREHOUSE');
                $sheet->setCellValue('G1','STATUS');
                $sheet->setCellValue('H1','STATISTICAL_DATE');
                $sheet->setCellValue('I1','PODD_CHECK');
                $sheet->setCellValue('J1','PROMISE_DATE');
                $sheet->setCellValue('K1','CATEGORY');
                $sheet->setCellValue('L1','ITEM_CODE');
                $sheet->setCellValue('M1','ITEM_CODE_SOURCE');
                $sheet->setCellValue('N1','ITEM_DESC');
                $sheet->setCellValue('O1','DOCUMENT_NO');
                $sheet->setCellValue('P1','SUPPLIER_NAME_STOCK');
                $sheet->setCellValue('Q1','SUPPLIER_NAME_BOM');
                $sheet->setCellValue('R1','IS_PR');
                $sheet->setCellValue('S1','QTY_ALLOCATION');
                $sheet->setCellValue('T1','QTY_INHOUSE');
                $sheet->setCellValue('U1','QTY_ON_SHIP');
                $sheet->setCellValue('V1','QTY_OPEN');
                $sheet->setCellValue('W1','UOM');
                $sheet->setCellValue('X1','MRD');
                $sheet->setCellValue('Y1','DD PI');
                $sheet->setCellValue('Z1','ETD PI');
                $sheet->setCellValue('AA1','ETA PI');
                $sheet->setCellValue('AB1','ETD ACTUAL');
                $sheet->setCellValue('AC1','ETA ACTUAL');
                $sheet->setCellValue('AD1','ETA DELAY');
                $sheet->setCellValue('AE1','ETA MAX');
                $sheet->setCellValue('AF1','NO INVOICE');
                $sheet->setCellValue('AG1','DATE RECEIVE');
                $sheet->setCellValue('AH1','REMARK_ALOKASI');
                $sheet->setCellValue('AI1','REMARK_DELAY');
                $sheet->setCellValue('AJ1','STATUS');

            $row=2;
            foreach ($auto_allocations as $i) 
            {  
                $sheet->setCellValue('A'.$row,$i->id);
                $sheet->setCellValue('B'.$row,$i->lc_date);
                $sheet->setCellValue('C'.$row,$i->po_buyer);
                $sheet->setCellValue('D'.$row,$i->_style);
                $sheet->setCellValue('E'.$row,$i->brand);
                $sheet->setCellValue('F'.$row,$i->warehouse_name);
                $sheet->setCellValue('G'.$row,$i->status_po_buyer);
                $sheet->setCellValue('H'.$row,$i->podd);
                $sheet->setCellValue('I'.$row,$i->statistical_date);
                $sheet->setCellValue('J'.$row,$i->promise_date);
                $sheet->setCellValue('K'.$row,$i->category);
                $sheet->setCellValue('L'.$row,$i->item_code);
                $sheet->setCellValue('M'.$row,$i->item_code_source);
                $sheet->setCellValue('N'.$row,$i->item_desc);
                $sheet->setCellValue('O'.$row,$i->document_no);
                $sheet->setCellValue('P'.$row,$i->supplier_name);
                $sheet->setCellValue('Q'.$row,$i->supplier_name_bom);
                $sheet->setCellValue('R'.$row,$i->is_allocation_purchase);
                $sheet->setCellValue('S'.$row,$i->qty_allocation);
                $sheet->setCellValue('T'.$row,$i->qty_in_house);
                $sheet->setCellValue('U'.$row,$i->qty_on_ship);
                $qty_open = sprintf('%0.4f',$i->qty_allocation) - sprintf('%0.4f',$i->qty_in_house) - sprintf('%0.4f',$i->qty_on_ship);
                $sheet->setCellValue('V'.$row,$qty_open);
                $sheet->setCellValue('W'.$row,$i->uom);
                $sheet->setCellValue('X'.$row,$i->mrd);
                $sheet->setCellValue('Y'.$row,$i->dd_pi);
                $sheet->setCellValue('Z'.$row,$i->etd_date);
                $sheet->setCellValue('AA'.$row,$i->eta_date);
                $sheet->setCellValue('AB'.$row,$i->etd_actual);
                $sheet->setCellValue('AC'.$row,$i->eta_actual);
                $sheet->setCellValue('AD'.$row,$i->eta_delay);
                $sheet->setCellValue('AE'.$row,$i->eta_max);
                $sheet->setCellValue('AF'.$row,$i->no_invoice);
                $sheet->setCellValue('AG'.$row,$i->receive_date);
                $sheet->setCellValue('AH'.$row,$i->remark_update);
                $sheet->setCellValue('AI'.$row,$i->remark_delay);
                $sheet->setCellValue('AJ'.$row,$i->status);

                $row++;
            }
            });

        })
        ->export('csv');


    }

    static function updateMonitoringMaterialACC()
    {
        $warehouse_id = ['1000002','1000013'];

        $refresh = DB::select(db::raw("
        refresh materialized view monitoring_allocations_acc_mv;"
        ));

        //list kalkulasi
        $auto_allocations = db::table('monitoring_allocations_acc_mv')
        ->where('is_additional',false)
        ->whereIn(db::raw("to_char(lc_date,'YYYYMM')"),['202010','202011','202012', '202101', '202102', '202103'])
        ->where(db::raw("qty_allocation - COALESCE(qty_in_house, 0)"), '>' ,'0')
        //->where('document_no', 'POA22012A/E1111')
        ->whereIn('warehouse_id',$warehouse_id)
        ->orderBy('statistical_date', 'asc')
        ->orderBy('promise_date', 'asc')
        ->orderBy('qty_allocation', 'asc')
        ->get();

        $app_env = Config::get('app.env');
        if($app_env == 'live')
        {
            $data_erp = DB::connection('erp');
            $data     = DB::connection('web_po');
        }

        //dd($auto_allocations);
        try 
        {
            DB::beginTransaction();

            $reset_alocation = AutoAllocation::where('is_additional',false)
            //->whereBetween('lc_date', [$start_date, $end_date])
            ->whereIn(db::raw("to_char(lc_date,'YYYYMM')"),['202010','202011','202012', '202101', '202102', '202103'])
            //->where('document_no', 'POA22012A/E1111')
            ->where(db::raw("qty_allocation - COALESCE(qty_in_house, 0)"), '>' ,'0')
            ->whereIn('warehouse_id',$warehouse_id)
            ->whereNull('deleted_at')
            ->update([
                'qty_in_house' => null,
                'qty_on_ship'  => null,
                //'mrd'          => null,
                'qty_mrd'      => null,
                // 'eta_actual'   => null,
                // 'eta_actual'   => null,
                // 'no_invoice'   => null,
                // 'receive_date' => null,
                // 'dd_pi'        => null,
            ]);

            foreach($auto_allocations as $key => $auto_allocation)
            {   
                $eta_date     = null;
                $etd_date     = null;
                $receive_date = null;
                $no_invoice   = null;
                $qty_in_house = 0;
                $qty_on_ship  = 0;
                $qty_mrd      = 0;
                $mrd          = null;
                $dd_pi        = null;
                $eta_pi       = null;
                $etd_pi       = null;

                $auto_allocation_id    = $auto_allocation->id;
                $item_id               = $auto_allocation->item_id_source;
                $warehouse_id          = $auto_allocation->warehouse_id;
                $c_order_id            = $auto_allocation->c_order_id;
                $lc_date               = $auto_allocation->lc_date;
                $qty_need              = sprintf('%0.4f',$auto_allocation->qty_allocation);
                $material_in_house     = sprintf('%0.4f',$auto_allocation->qty_allocated);
                $qty_on_ship_alocation = sprintf('%0.4f',$auto_allocation->qty_on_ship);

                if($c_order_id == 'FREE STOCK')
                {
                    $qty_in_house = $qty_need;
                    $eta_actual   = $lc_date;
                    $etd_actual   = $lc_date;
                    $receive_date = $lc_date;
                    $no_invoice   = null;

                }
                else
                {

                //get receive date
                if($material_in_house > 0)
                {
                    $material_preparation = MaterialPreparation::where('auto_allocation_id', $auto_allocation_id)
                                            ->orderBy('created_at', 'desc')
                                            ->first();
                    $material_stock_id    =($material_preparation) ? $material_preparation->material_stock_id : null;
                    $material_stock       = MaterialStock::where('id', $material_stock_id)->first();         
                    $receive_date         = ($material_stock ) ? $material_stock->created_at : null;
                }
                
                //jika material yang tersdia > 0
                if($material_in_house > 0)
                {
                    //dd($material_in_house - $qty_need);
                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                    //dd($material_in_house/$qty_need);
                    if(sprintf('%0.4f',$material_in_house)/$qty_need >= 1)
                    {
                        $qty_in_house = $qty_need;
                        $qty_on_ship  = 0;

                        //$app_env = Config::get('app.env');
                        // if($app_env == 'live')
                        // {
                            //$data = DB::connection('web_po');
                            $list_on_ships = $data->table('material_on_ship_v')
                            ->select(db::raw('sum(qty_upload) as qty_upload'), 'kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            ->where([
                                ['m_product_id', $item_id],
                                ['c_order_id', $c_order_id],
                                ['isactive', true],//ganti dulu 
                                ['flag_wms', true]
                            ])
                            ->groupBy('kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            ->orderBy('kst_etadate', 'asc')
                            ->orderBy('kst_etddate', 'asc')
                            ->get();

                            // $sum_on_ship_web_po = $list_on_ships->sum('qty_upload');

                            //dd($list_on_ships);

                            if(count($list_on_ships) > 0)
                            {
                                $sum_on_ship_web_po = 0;
                                foreach ($list_on_ships as $item) {
                                    $sum_on_ship_web_po += $item->qty_upload;
                                }
                                $temp_qty_on_ship = 0;
                                foreach($list_on_ships as $key => $list_on_ship)
                                {
                                    $sisa = 0;
                                    $no_invoice           = $list_on_ship->kst_invoicevendor;
                                    $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
                                    $temp_qty_on_ship += $total_on_ship_web_po;

                                    $sum_all_qty_on_ship = AutoAllocation::where([
                                        ['item_id_source', $item_id],
                                        ['c_order_id', $c_order_id],
                                        //['warehouse_id', $warehouse_id],
                                        ])
                                        ->whereNull('deleted_at')
                                        ->sum('qty_on_ship');

                                    // $sum_qty_on_ship = AutoAllocation::where([
                                    //     ['item_id_source', $item_id],
                                    //     ['c_order_id', $c_order_id],
                                    //     ['warehouse_id', $warehouse_id],
                                    //     ['no_invoice', $no_invoice],
                                    //     ])
                                    //     ->whereNull('deleted_at')
                                    //     ->sum('qty_on_ship');
                                    
                                       
                                    if(sprintf('%0.4f', $sum_all_qty_on_ship) - $temp_qty_on_ship > 0 )
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        $outstanding_on_ship =  $temp_qty_on_ship - sprintf('%0.4f', $sum_all_qty_on_ship);
                                    }

                                    //$outstanding_on_ship = sprintf('%0.4f',$total_on_ship_web_po) - sprintf('%0.4f',$sum_qty_on_ship);
                                    
                                    //echo $no_invoice. ' total  '. $total_on_ship_web_po. ' sisa '.$outstanding_on_ship. PHP_EOL;

                                    //echo (sprintf('%0.4f',$sum_on_ship_web_po) - sprintf('%0.4f', $sum_all_qty_on_ship)). PHP_EOL;
                                    if(sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship) != 0)
                                    {
                                        if($outstanding_on_ship > 0)
                                        {
                                            //jika material yang tersedia lebih dari atau sama dengan alokasi
                                            if($outstanding_on_ship >= ($qty_need - $qty_in_house))
                                            {
                                                $qty_on_ship              = $qty_need - $qty_in_house;
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                //$receive_date             = null;
                                                //echo $qty_on_ship. PHP_EOL;
                                                break;
                                            }
                                            else
                                            {
                                                $qty_on_ship       += $outstanding_on_ship;
                                                if($qty_on_ship >= ($qty_need - $qty_in_house))
                                                {
                                                    $qty_on_ship  = $qty_need - $qty_in_house;
                                                    $eta_date     = $list_on_ship->kst_etadate;
                                                    $etd_date     = $list_on_ship->kst_etddate;
                                                    //$receive_date = null;
                                                    //echo $qty_on_ship. PHP_EOL;
                                                    break;
                                                    
                                                }
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                //$receive_date             = $receive_date;
                                                //echo $qty_on_ship. PHP_EOL;
                                            }
                                            

                                        }
                                    }
                                    // else
                                    // {
                                    //     $qty_on_ship  = 0;
                                    //     $eta_date     = null;
                                    //     $etd_date     = null;
                                    //     $receive_date = null;
                                    // }    
                                }
                                //get mrd
                                $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();
                                    
                                    //dd($list_on_ship_mrds);
                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                            $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            
                                                
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }

                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        //$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_mrd. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                        //$receive_date = $receive_date;
                                                        //echo $qty_mrd. PHP_EOL;
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }
                            }
                        //dd($no_invoice);

                        //$data_erp = DB::connection('erp');

                        // $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                        // ->select(db::raw('max(mrd) as mrd, max(dd_pi) as dd_pi, max(eta) as eta, max(etd) as etd'))
                        // ->where([
                        //     ['m_product_id', $item_id],
                        //     ['c_order_id', $c_order_id],
                        // ])
                        // ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                        // ->orderBy('eta', 'desc')
                        // ->orderBy('etd', 'desc')
                        // ->first();

                        // //dd($list_on_ship_mrds);
                        // if($list_on_ship_mrds)
                        // {
                        //     $mrd          = $list_on_ship_mrds->mrd;
                        //     $dd_pi        = $list_on_ship_mrds->dd_pi;
                        //     $eta_pi       = $list_on_ship_mrds->eta;
                        //     $etd_pi       = $list_on_ship_mrds->etd;
                        // }

                        $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();
                                    
                                    //dd($list_on_ship_mrds);
                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                            $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            
                                                
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }

                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        //$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        $receive_date = $receive_date;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_mrd. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                        //echo $qty_mrd. PHP_EOL;
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }

                    }
                    else
                    {
                        $qty_in_house = $material_in_house;
                        //cek kekurangan di web po
                        // $app_env = Config::get('app.env');
                        // if($app_env == 'live')
                        // {
                        //     $data = DB::connection('web_po');
                        $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                        ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                        ->where([
                            ['m_product_id', $item_id],
                            ['c_order_id', $c_order_id],
                        ])
                        ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                        ->orderBy('mrd', 'asc')
                        ->orderBy('dd_pi', 'asc')
                        ->get();
                        
                        //dd($list_on_ship_mrds);
                        $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                        if(count($list_on_ship_mrds) > 0)
                        {
                            $temp_qty_on_ship_mrd = 0;
                            foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                            {
                                $sisa = 0;
                                $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                    ['item_id_source', $item_id],
                                    ['c_order_id', $c_order_id],
                                    ['warehouse_id', $warehouse_id],
                                    ])
                                    ->whereNull('deleted_at')
                                    ->sum('qty_mrd');
                                
                                    
                                if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                {
                                    continue;
                                }
                                else
                                {
                                    $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                }

                                if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                {
                                    if($outstanding_on_ship_mrd > 0)
                                    {
                                        //jika material yang tersedia lebih dari atau sama dengan alokasi
                                        if($outstanding_on_ship_mrd >= $qty_need)
                                        {
                                            $qty_mrd      = $qty_need;
                                            $mrd          = $list_on_ship_mrd->mrd;
                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                            $eta_pi       = $list_on_ship_mrd->eta;
                                            $etd_pi       = $list_on_ship_mrd->etd;
                                            //$receive_date = null;
                                            //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                            break;
                                        }
                                        else
                                        {
                                            $qty_mrd       += $outstanding_on_ship_mrd;
                                            $mrd          = $list_on_ship_mrd->mrd;
                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                            $eta_pi       = $list_on_ship_mrd->eta;
                                            $etd_pi       = $list_on_ship_mrd->etd;
                                            $receive_date = $receive_date;
                                            //echo $qty_mrd. PHP_EOL;

                                            if($qty_mrd >= $qty_need)
                                            {
                                                $qty_mrd      = $qty_need;
                                                $mrd          = $list_on_ship_mrd->mrd;
                                                $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                $eta_pi       = $list_on_ship_mrd->eta;
                                                $etd_pi       = $list_on_ship_mrd->etd;
                                                //$receive_date = null;
                                                //echo $qty_mrd. PHP_EOL;
                                                break;
                                                
                                            }
                                        }
                                        

                                    }
                                }
                            }
                        }

                            $list_on_ships = $data->table('material_on_ship_v')
                            ->select(db::raw('sum(qty_upload) as qty_upload'), 'kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            ->where([
                                ['m_product_id', $item_id],
                                ['c_order_id', $c_order_id],
                                ['isactive', true],
                                ['flag_wms', false]
                            ])
                            ->groupBy('kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            ->orderBy('kst_etadate', 'asc')
                            ->orderBy('kst_etddate', 'asc')
                            ->get();

                            //dd($list_on_ships);
                            // $sum_on_ship_web_po = $list_on_ships->sum('qty_upload');

                            if(count($list_on_ships) > 0)
                            {
                                $sum_on_ship_web_po = 0;
                                foreach ($list_on_ships as $item) {
                                    $sum_on_ship_web_po += $item->qty_upload;
                                }
                                $temp_qty_on_ship = 0;
                                $outstanding_on_ship = 0;
                                foreach($list_on_ships as $key => $list_on_ship)
                                {
                                    $no_invoice           = $list_on_ship->kst_invoicevendor;
                                    $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
                                    $temp_qty_on_ship += $total_on_ship_web_po;

                                    $sum_all_qty_on_ship = AutoAllocation::where([
                                        ['item_id_source', $item_id],
                                        ['c_order_id', $c_order_id],
                                        //['warehouse_id', $warehouse_id],
                                        ])
                                        ->whereNull('deleted_at')
                                        ->sum('qty_on_ship');

                                    // $sum_qty_on_ship = AutoAllocation::where([
                                    //     ['item_id_source', $item_id],
                                    //     ['c_order_id', $c_order_id],
                                    //     ['warehouse_id', $warehouse_id],
                                    //     ['no_invoice', $no_invoice],
                                    //     ])
                                    //     ->whereNull('deleted_at')
                                    //     ->sum('qty_on_ship');
        
                                    // $qty_on_ship_web_po += $sisa_qty_on_ship_web_po;
                                    
                                    //sisa qty on ship
                                    if(sprintf('%0.4f', $sum_all_qty_on_ship) - $temp_qty_on_ship > 0)
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        $outstanding_on_ship =  $temp_qty_on_ship - sprintf('%0.4f', $sum_all_qty_on_ship);
                                    }
                                    
                                    //echo $no_invoice. ' total  '. $total_on_ship_web_po. ' sisa '.$outstanding_on_ship. PHP_EOL;

                                    //echo (sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship)). PHP_EOL;
                                    if(sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship) != 0)
                                    {
                                        if($outstanding_on_ship > 0)
                                        {
                                            //jika material yang tersedia lebih dari atau sama dengan alokasi
                                            if($outstanding_on_ship >= ($qty_need - $qty_in_house))
                                            {
                                                $qty_on_ship              = $qty_need - $qty_in_house;
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                $receive_date             = $receive_date;
                                                //echo $qty_on_ship. PHP_EOL;
                                                break;
                                            }
                                            else
                                            {
                                                $qty_on_ship       += $outstanding_on_ship;
                                                if($qty_on_ship >= ($qty_need - $qty_in_house))
                                                {
                                                    $qty_on_ship  = $qty_need - $qty_in_house;
                                                    $eta_date     = $list_on_ship->kst_etadate;
                                                    $etd_date     = $list_on_ship->kst_etddate;
                                                    $receive_date = $receive_date;
                                                    //echo $qty_on_ship. PHP_EOL;
                                                    break;
                                                    
                                                }
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                $receive_date             = $receive_date;
                                                //echo $qty_on_ship. PHP_EOL;
                                            }
                                            

                                        }
                                    }
                                    // else
                                    // {
                                    //     $qty_on_ship  = 0;
                                    //     $eta_date     = null;
                                    //     $etd_date     = null;
                                    //     $receive_date = null;
                                    // }    
                                }
                            }
                            else
                            {
                                // $qty_on_ship  = 0;
                                // $eta_date     = null;
                                // $etd_date     = null;
                                // $receive_date = null;
                                // $no_invoice   = null;
                                //dd('sini');
                                //get MRD

                                // //cek web po yang sudah diterima 
                                // $app_env = Config::get('app.env');
                                // if($app_env == 'live')
                                // {
                                //     $data = DB::connection('web_po');
                                //     $list_eta_etd_in_house = $data->table('material_on_ship_v')
                                //     ->select(db::raw('max(kst_etadate) as etadate , max(kst_etddate) as etddate, kst_invoicevendor'))
                                //     ->where([
                                //         ['m_product_id', $item_id],
                                //         ['c_order_id', $c_order_id],
                                //         ['isactive', true],
                                //         ['flag_wms', true]
                                //     ])
                                //     ->groupBy('kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                                //     ->orderBy('kst_etadate', 'asc')
                                //     ->orderBy('kst_etddate', 'asc')
                                //     ->first();
                                // }
                                //dd($list_eta_etd_in_house);
                                // if($list_eta_etd_in_house)
                                // {
                                //     $eta_date     = $list_eta_etd_in_house->etadate;
                                //     $etd_date     = $list_eta_etd_in_house->etddate;
                                //     $receive_date = $receive_date;
                                //     $no_invoice   = $list_eta_etd_in_house->kst_invoicevendor;
                                // }

                                // if($app_env == 'live')
                                // {
                                    //$data_erp = DB::connection('erp');

                                    $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();
                                    
                                    //dd($list_on_ship_mrds);
                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                            $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            
                                                
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }

                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        //$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        $receive_date = $receive_date;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_mrd. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                        //echo $qty_mrd. PHP_EOL;
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }
                                //}
                            }

                        //}

                    }

                }
                else
                {
                    $qty_in_house = 0;
                    //cek qty on ship di web po 
                    // $app_env = Config::get('app.env');
                    // if($app_env == 'live')
                    //     {
                            //$data = DB::connection('web_po');

                            $list_on_ships = $data->table('material_on_ship_v')
                            ->select(db::raw('sum(qty_upload) as qty_upload'), 'kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            ->where([
                                ['m_product_id', $item_id],
                                ['c_order_id', $c_order_id],
                                ['isactive', true],//ganti dulu 
                                ['flag_wms', false]
                            ])
                            ->groupBy('kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            ->orderBy('kst_etadate', 'asc')
                            ->orderBy('kst_etddate', 'asc')
                            ->get();

                            // $sum_on_ship_web_po = $list_on_sh/ips->sum('qty_upload');

                            //dd($list_on_ships);

                            if(count($list_on_ships) > 0)
                            {
                                $sum_on_ship_web_po = 0;
                                foreach ($list_on_ships as $item) {
                                    $sum_on_ship_web_po += $item->qty_upload;
                                }
                                $temp_qty_on_ship = 0;
                                foreach($list_on_ships as $key => $list_on_ship)
                                {
                                    $sisa = 0;
                                    $no_invoice           = $list_on_ship->kst_invoicevendor;
                                    $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
                                    $temp_qty_on_ship += $total_on_ship_web_po;

                                    $sum_all_qty_on_ship = AutoAllocation::where([
                                        ['item_id_source', $item_id],
                                        ['c_order_id', $c_order_id],
                                        //['warehouse_id', $warehouse_id],
                                        ])
                                        ->whereNull('deleted_at')
                                        ->sum('qty_on_ship');

                                    // $sum_qty_on_ship = AutoAllocation::where([
                                    //     ['item_id_source', $item_id],
                                    //     ['c_order_id', $c_order_id],
                                    //     ['warehouse_id', $warehouse_id],
                                    //     ['no_invoice', $no_invoice],
                                    //     ])
                                    //     ->whereNull('deleted_at')
                                    //     ->sum('qty_on_ship');
                                    
                                       
                                    if(sprintf('%0.4f', $sum_all_qty_on_ship) - $temp_qty_on_ship > 0)
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        $outstanding_on_ship =  $temp_qty_on_ship - sprintf('%0.4f', $sum_all_qty_on_ship);
                                    }

                                    //$outstanding_on_ship = sprintf('%0.4f',$total_on_ship_web_po) - sprintf('%0.4f',$sum_qty_on_ship);
                                    
                                    //echo $no_invoice. ' total  '. $total_on_ship_web_po. ' sisa '.$outstanding_on_ship. PHP_EOL;

                                    //echo (sprintf('%0.4f',$sum_on_ship_web_po) - sprintf('%0.4f', $sum_all_qty_on_ship)). PHP_EOL;
                                    if(sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship) != 0)
                                    {
                                        if($outstanding_on_ship > 0)
                                        {
                                            //jika material yang tersedia lebih dari atau sama dengan alokasi
                                            if($outstanding_on_ship >= ($qty_need - $qty_in_house))
                                            {
                                                $qty_on_ship              = $qty_need - $qty_in_house;
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                $receive_date             = null;
                                                //echo $qty_on_ship. PHP_EOL;
                                                break;
                                            }
                                            else
                                            {
                                                $qty_on_ship       += $outstanding_on_ship;
                                                if($qty_on_ship >= ($qty_need - $qty_in_house))
                                                {
                                                    $qty_on_ship  = $qty_need - $qty_in_house;
                                                    $eta_date     = $list_on_ship->kst_etadate;
                                                    $etd_date     = $list_on_ship->kst_etddate;
                                                    //$receive_date = null;
                                                    //echo $qty_on_ship. PHP_EOL;
                                                    break;
                                                    
                                                }
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                $receive_date             = $receive_date;
                                                //echo $qty_on_ship. PHP_EOL;
                                            }
                                            

                                        }
                                    }
                                    // else
                                    // {
                                    //     $qty_on_ship  = 0;
                                    //     $eta_date     = null;
                                    //     $etd_date     = null;
                                    //     $receive_date = null;
                                    // }    
                                }
                                //get mrd
                                $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();
                                    
                                    //dd($list_on_ship_mrds);
                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                            $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            
                                                
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }

                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        //$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        $receive_date = $receive_date;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_mrd. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                        //echo $qty_mrd. PHP_EOL;
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }
                            }
                            else
                            {
                                // $qty_on_ship  = 0;
                                // $eta_date     = null;
                                // $etd_date     = null;
                                // $receive_date = null;
                                // $no_invoice   = null;

                                //get MRD
                                // if($app_env == 'live')
                                // {
                                    //$data_erp = DB::connection('erp');

                                    $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();

                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_ship_web_po;

                                            $sum_all_qty_on_ship_mrd = AutoAllocation::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            
                                                
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }

                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        ////$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        $receive_date = $receive_date;
                                                        //echo $qty_on_ship. PHP_EOL;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_on_ship. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }
                                //}
                            }

                    //}
                }

            }
                //kalau parsial masih ada qty open
                if(sprintf('%0.4f', $qty_need - $qty_in_house - $qty_on_ship) > 0.001)
                {
                    $eta_date     = null;
                    $etd_date     = null;
                    $receive_date = null;
                    $no_invoice   = null;
                }
                
                $auto_allocation_update               = AutoAllocation::find($auto_allocation_id);
                $auto_allocation_update->qty_in_house = $qty_in_house;
                $auto_allocation_update->qty_on_ship  = $qty_on_ship;
                if($auto_allocation_update->eta_actual != null)
                {
                    if($auto_allocation_update->eta_actual != $eta_date)
                    {
                        $auto_allocation_update->remark_delay = $auto_allocation_update->remark_delay.' " Delay Pengiriman '.$eta_date. ' " ';
                    }
                }
                $auto_allocation_update->eta_actual   = $eta_date;
                $auto_allocation_update->etd_actual   = $etd_date;
                $auto_allocation_update->eta_date     = $eta_pi;
                $auto_allocation_update->etd_date     = $etd_pi;
                $auto_allocation_update->mrd          = $mrd;
                $auto_allocation_update->dd_pi        = $dd_pi;
                $auto_allocation_update->qty_mrd      = $qty_mrd;
                $auto_allocation_update->receive_date = $receive_date;
                $auto_allocation_update->no_invoice   = $no_invoice;
                $auto_allocation_update->save();

            }

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
        }


    }

    static function insertToDashboardErp()
    {
        $warehouse_id = ['1000011','1000001'];
        $auto_allocations = db::table('monitoring_allocations_v')
                ->where('is_additional',false)
                ->where('lc_date', '>=', '2020-06-01')
                ->whereIn('warehouse_id',$warehouse_id)
                ->orderBy('statistical_date', 'asc')
                ->orderBy('promise_date', 'asc')
                ->orderBy('qty_allocation', 'asc')
                ->get();

        //insertToErp

        $app_env = Config::get('app.env');
        if($app_env == 'live')
        {
            $data_erp = DB::connection('erp');
        }

        //kosongin dulu tabelnya 
        $delete = DashboardErp::truncate();
        //isiin lagi pakai data terbaru
        foreach($auto_allocations as $key => $auto_allocation)
        {
            $dashboard = DashboardErp::insert([
                        'id'                     => $auto_allocation->id,
                        'type_stock'             => $auto_allocation->type_stock,
                        'promise_date'           => $auto_allocation->promise_date,
                        'statistical_date'       => $auto_allocation->podd,
                        'booking_number'         => $auto_allocation->booking_number,
                        'created_at'             => $auto_allocation->created_at,
                        'user_pic'               => $auto_allocation->user_pic,
                        'update_user'            => $auto_allocation->update_user,
                        'lc_date'                => $auto_allocation->lc_date,
                        'season'                 => $auto_allocation->season,
                        'document_no'            => $auto_allocation->document_no,
                        'supplier_code'          => $auto_allocation->supplier_code,
                        'supplier_name'          => $auto_allocation->supplier_name,
                        'po_buyer'               => $auto_allocation->po_buyer,
                        'old_po_buyer'           => $auto_allocation->old_po_buyer,
                        'item_code_source'       => $auto_allocation->item_code_source,
                        'item_code'              => $auto_allocation->item_code,
                        'item_desc'              => $auto_allocation->item_desc,
                        'category'               => $auto_allocation->category,
                        'warehouse_id'           => $auto_allocation->warehouse_id,
                        'warehouse_name'         => $auto_allocation->warehouse_name,
                        'uom'                    => $auto_allocation->uom,
                        'qty_allocation'         => $auto_allocation->qty_allocation,
                        'qty_outstanding'        => $auto_allocation->qty_outstanding,
                        'qty_allocated'          => $auto_allocation->qty_allocated,
                        'qty_in_house'           => $auto_allocation->qty_in_house,
                        'qty_on_ship'            => $auto_allocation->qty_on_ship,
                        'is_fabric'              => $auto_allocation->is_fabric,
                        'is_additional'          => $auto_allocation->is_additional,
                        'remark_additional'      => $auto_allocation->remark_additional,
                        'status_po_buyer'        => $auto_allocation->status_po_buyer,
                        'cancel_date'            => $auto_allocation->cancel_date,
                        'remark_update'          => $auto_allocation->remark_update,
                        'is_allocation_purchase' => $auto_allocation->is_allocation_purchase,
                        'item_id_source'         => $auto_allocation->item_id_source,
                        'c_order_id'             => $auto_allocation->c_order_id,
                        'eta_date'               => $auto_allocation->eta_date,
                        'etd_date'               => $auto_allocation->etd_date,
                        'eta_actual'             => $auto_allocation->eta_actual,
                        'etd_actual'             => $auto_allocation->etd_actual,
                        'receive_date'           => $auto_allocation->receive_date,
                        'no_invoice'             => $auto_allocation->no_invoice,
                        'mrd'                    => $auto_allocation->mrd,
                        'dd_pi'                  => $auto_allocation->dd_pi,
                        'is_closed'              => $auto_allocation->is_closed,
                        'eta_delay'              => $auto_allocation->eta_delay,
                        'eta_max'                => $auto_allocation->eta_max,
                        'item_id_book'           => $auto_allocation->item_id_book,
                        'status'                 => $auto_allocation->status,
                        'brand'                  => $auto_allocation->brand,
                        '_style'                 => $auto_allocation->_style,
                        'remark_delay_new'           => $auto_allocation->remark_delay,
                        'supplier_code_bom'      => $auto_allocation->supplier_code_bom,
                        'supplier_name_bom'      => $auto_allocation->supplier_name_bom,
                        'qtyordered_garment'      => $auto_allocation->qty_ordered_garment,
                ]);
        }






    }




}
