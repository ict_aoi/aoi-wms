<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Models\Rma;
use App\Models\Item;
use App\Models\Defect;
use App\Models\Locator;
use App\Models\UomConversion;
use App\Models\MaterialStock;
use App\Models\MaterialCheck;
use App\Models\MaterialArrival;
use App\Models\DetailMaterialCheck;
use App\Models\MovementStockHistory;


class FabricMaterialQualityControlController extends Controller
{
    public function index()
    {
        $defect_codes = Defect::orderBy('code','ASC')->get();
        return view('fabric_material_quality_control.index',compact('defect_codes'));
    }

    
    public function create(request $request)
    {
        $_warehouse_id  = $request->warehouse_id;
        $_barcode       = strtoupper(trim($request->barcode));

        $material = MaterialStock::where([
            ['barcode_supplier', $_barcode],
            ['warehouse_id', $_warehouse_id],
        ])
        ->first();

        if(!$material) return response()->json('Barcode not found', 422);
        if($material->po_detail_id = null || $material->po_detail_id = '') return response()->json('Please contact ICT',422);

        if($material->is_reject_by_lot) return response()->json('Roll cannot be used due of lot is not match',422);
        
        if($material->is_reject)
        {
            if($material->is_reject_by_rma) return response()->json('Roll cannot be used due of material is wrong',422);
            else return response()->json('Roll cannot be used due of reject result by qc',422);
        } 
        //if($material->inspect_lot_result == 'HOLD') return response()->json('Roll is in hold by qc due of lot inspection.',422);
        

        $is_material_check = MaterialCheck::where('material_stock_id',$material->id)
        ->whereNull('deleted_at')
        ->exists();

        if($is_material_check) return response()->json('Material already scanned', 422);

        $total = 0;
        
        $options = [
            [
                'id' => 'QUARANTINE',
                'name' => 'QUARANTINE'
            ],[
                'id' => 'HOLD',
                'name' => 'HOLD'
            ],[
                'id' => 'RELEASE',
                'name' => 'RELEASE'
            ],[
                'id' => 'REJECT',
                'name' => 'REJECT'
            ]
        ];

        $_item                          = Item::where('item_code',$material->item_code)->first();
        $obj                            = new StdClass();
        $obj->material_stock_id         = $material->id;
        $obj->item_id                   = $material->item_id;
        $obj->barcode                   = $material->barcode_supplier;
        $obj->material_arrival_id       = $material->material_arrival_id;
        $obj->c_bpartner_id             = $material->c_bpartner_id;
        $obj->supplier_name             = $material->supplier_name;
        $obj->document_no               = $material->document_no;
        $obj->item_code                 = $material->item_code;
        $obj->color                     = ($_item)? $_item->color : 'COLOR NOT FOUND';
        $obj->no_invoice                = $material->no_invoice;
        $obj->category                  = $material->category;
        $obj->nomor_roll                = $material->nomor_roll;
        $obj->batch_number              = $material->batch_number;
        $obj->uom                       = $material->uom;
        $obj->_qty                      = $material->qty_arrival;
        $obj->qty                       = $material->qty_arrival;
        $obj->status_options            = $options;
        $obj->status                    = 'QUARANTINE';
        $obj->movement_date             = Carbon::now()->toDateTimeString();
        $obj->actual_length             = ($material)? $material->actual_length : null;
        $obj->remark                    = null;
        $obj->begin_width               = ($material)? $material->begin_width : null;
        $obj->middle_width              = ($material)? $material->middle_width : null;
        $obj->end_width                 = ($material)? $material->end_width : null;
        $obj->actual_width              = ($material)? $material->actual_width : null;
        $obj->kg                        = null;
        $obj->different_yard            = null;
        $obj->warehouse_id              = $material->warehouse_id;
        $obj->point_1                   = 0;
        $obj->point_2                   = 0;
        $obj->point_3                   = 0;
        $obj->point_4                   = 0;
        $obj->total_all_point           = null;
        $obj->percentage                = null;
        $obj->detail                    = [];

        return response()->json($obj,200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barcodes' => 'required|not_in:[]'
        ]);

        if ($validator->passes())
        {
            $barcodes           = json_decode($request->barcodes);
            $status_release     = $request->status_release;
            $_warehouse_id      = $request->warehouse_id;
            $material_checks    = array();
            $material_checks    = array();
            $concatenate        = '';
            $movement_date      = Carbon::now()->toDateTimeString();

            try 
            {
                DB::beginTransaction();
                

                foreach ($barcodes as $key => $value) 
                {
                    $item               = Item::where('item_id', $value->item_id)->first();
                    $width_on_barcode   = ($item) ? $item->width : 'MASTER ITEM NOT FOUND';
                    
                    $is_exists = MaterialCheck::where('material_stock_id',$value->material_stock_id)->exists();
                    if(!$is_exists)
                    {
                        $concatenate    .= "'".$value->material_stock_id."',";
                        $actual_length  = ($value->actual_length)? $value->actual_length : $value->qty;
                        $qty_short      = $value->qty - $actual_length;
                        $is_short_roll  = ( $qty_short > 0.5 ? true : false);
                        
                        if($value->percentage == null || $value->percentage =='')
                        {
                                $final_result = 'RELEASE';
                                $confirm_user_id = auth::user()->id;
                                $confirm_date    = carbon::now();
                        } else
                        {
                            if($value->percentage <= 15.00 && $status_release == 'release')
                            {
                                $final_result       = 'RELEASE';
                                $confirm_user_id    = auth::user()->id;
                                $confirm_date       = carbon::now();
                            }
                            else
                            {
                                $final_result    = null;
                                $confirm_user_id = null;
                                $confirm_date    = null;
                            }
                        }
                        $material_check = MaterialCheck::FirstOrCreate([
                            'material_stock_id'         => $value->material_stock_id,
                            'barcode_preparation'       => $value->barcode,
                            'c_bpartner_id'             => $value->c_bpartner_id,
                            'supplier_name'             => $value->supplier_name,
                            'document_no'               => $value->document_no,
                            'type_po'                   => 1,
                            'item_code'                 => $value->item_code,
                            'color'                     => $value->color,
                            'no_invoice'                => $value->no_invoice,
                            'category'                  => $value->category,
                            'nomor_roll'                => $value->nomor_roll,
                            'batch_number'              => $value->batch_number,
                            'uom_conversion'            => $value->uom,
                            'status'                    => $value->status,
                            'qty_on_barcode'            => $value->qty,
                            'actual_length'             => $actual_length,
                            'begin_width'               => $value->begin_width,
                            'middle_width'              => $value->middle_width,
                            'end_width'                 => $value->end_width,
                            'actual_width'              => $value->actual_width,
                            'width_on_barcode'          => $width_on_barcode,
                            'kg'                        => ($value->kg)? $value->kg : null,
                            'point_1'                   => $value->point_1,
                            'point_2'                   => $value->point_2,
                            'point_3'                   => $value->point_3,
                            'point_4'                   => $value->point_4,
                            'total_point'               => $value->total_all_point,
                            'percentage'                => $value->percentage,
                            'user_id'                   => auth::user()->id,
                            'different_yard'            => $actual_length - $value->qty,
                            'warehouse_id'              => $value->warehouse_id,
                            'final_result'              => $final_result,
                            'confirm_user_id'           => $confirm_user_id,
                            'confirm_date'              => $confirm_date,
                            'created_at'                => $movement_date,
                            'updated_at'                => $movement_date,
                        ]);

                        $material_stock                             = MaterialStock::find($value->material_stock_id);
                        $material_stock->begin_width                = $value->begin_width;
                        $material_stock->middle_width               = $value->middle_width;
                        $material_stock->end_width                  = $value->end_width;
                        $material_stock->actual_width               = $value->actual_width;
                        $material_stock->actual_length              = $value->actual_length;
                        $material_stock->is_already_inspect         = true;
                        $material_stock->is_short_roll              = $is_short_roll;
                        $material_stock->qty_reject_by_short_roll   = ($is_short_roll ? $qty_short : '0');
                        $material_stock->save();

                        $material_checks[]  = $material_check->id;
                        $details            = $value->detail;
                        foreach ($details as $key_2 => $detail) 
                        {
                            $detail_material_check = DetailMaterialCheck::FirstOrCreate([
                                'material_check_id'     => $material_check->id,
                                'start_point_check'     => $detail->point_check_from,
                                'end_point_check'       => $detail->point_check_to,
                                'defect_code'           => $detail->defect_code,
                                'defect_value'          => $detail->defect_value,
                                'is_selected_1'         => $detail->is_selected_1,
                                'is_selected_2'         => $detail->is_selected_2,
                                'is_selected_3'         => $detail->is_selected_3,
                                'is_selected_4'         => $detail->is_selected_4,
                                'remark'                => trim($detail->remark),
                                'multiply'              => $detail->multiply,
                                'total_point_defect'    => $detail->total_point_defect,
                                'user_id'               => auth::user()->id
                            ]);
                        }

                    }
                }

                if($concatenate != '')
                {
                    $concatenate = substr_replace($concatenate, '', -1);
                    DB::select(db::raw("SELECT *  FROM delete_duplicate_qc_fabric(array[".$concatenate ."]);" ));
                }

                if(count($material_checks) > 0)
                {
                    $this->countDefectCode($material_checks);
                    $this->calculateFIR($material_checks);
                }
                
                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            

            return response()->json(200);
        }else{
            return response()->json('Please scan barcode first.',422);
        }
    }

    static function countDefectCode($material_checks)
    {
        try 
        {
            DB::beginTransaction();

            $count_defect_codes = DetailMaterialCheck::select('material_check_id','defect_code',db::raw('sum(total_point_defect) as total_defect'))
            ->whereIn('material_check_id',$material_checks)
            ->groupby('material_check_id','defect_code')
            ->get();

            foreach ($count_defect_codes as $key => $count_defect_code) 
            {
                $material_check = MaterialCheck::find($count_defect_code->material_check_id);

                if($count_defect_code->defect_code == 'A') $material_check->jumlah_defect_a = $count_defect_code->total_defect;
                else if($count_defect_code->defect_code == 'B') $material_check->jumlah_defect_b = $count_defect_code->total_defect;
                else if($count_defect_code->defect_code == 'C') $material_check->jumlah_defect_c = $count_defect_code->total_defect;
                else if($count_defect_code->defect_code == 'D') $material_check->jumlah_defect_d = $count_defect_code->total_defect;
                else if($count_defect_code->defect_code == 'E') $material_check->jumlah_defect_e = $count_defect_code->total_defect;
                else if($count_defect_code->defect_code == 'F') $material_check->jumlah_defect_f = $count_defect_code->total_defect;
                else if($count_defect_code->defect_code == 'G') $material_check->jumlah_defect_g = $count_defect_code->total_defect;
                else if($count_defect_code->defect_code == 'H') $material_check->jumlah_defect_h = $count_defect_code->total_defect;
                else if($count_defect_code->defect_code == 'I') $material_check->jumlah_defect_i = $count_defect_code->total_defect;
                else if($count_defect_code->defect_code == 'J') $material_check->jumlah_defect_j = $count_defect_code->total_defect;
                else if($count_defect_code->defect_code == 'K') $material_check->jumlah_defect_k = $count_defect_code->total_defect;
                else if($count_defect_code->defect_code == 'L') $material_check->jumlah_defect_l = $count_defect_code->total_defect;
                else if($count_defect_code->defect_code == 'M') $material_check->jumlah_defect_m = $count_defect_code->total_defect;
                else if($count_defect_code->defect_code == 'N') $material_check->jumlah_defect_n = $count_defect_code->total_defect;
                else if($count_defect_code->defect_code == 'O') $material_check->jumlah_defect_o = $count_defect_code->total_defect;
                else if($count_defect_code->defect_code == 'P') $material_check->jumlah_defect_p = $count_defect_code->total_defect;
                else if($count_defect_code->defect_code == 'Q') $material_check->jumlah_defect_q = $count_defect_code->total_defect;
                else if($count_defect_code->defect_code == 'R') $material_check->jumlah_defect_r = $count_defect_code->total_defect;
                else if($count_defect_code->defect_code == 'S') $material_check->jumlah_defect_s = $count_defect_code->total_defect;
                else if($count_defect_code->defect_code == 'T') $material_check->jumlah_defect_t = $count_defect_code->total_defect;

                $material_check->save();
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function calculateFIR($material_checks)
    {
        try 
        {
            DB::beginTransaction();

            foreach ($material_checks as $key => $value) 
            {
                $calculates = DB::select(db::raw("SELECT * FROM get_fir_calculation(
                    '".$value."'
                    );"
                ));

                foreach ($calculates as $key_2 => $calculate) 
                {
                    $material_check                     = MaterialCheck::find($calculate->id);
                    
                    if($calculate->formula_1 > $material_check->qty_on_barcode) $formula_1 = $material_check->qty_on_barcode;
                    else $formula_1 = $calculate->formula_1;

                    if($calculate->formula_2 > $material_check->qty_on_barcode) $formula_2 = $material_check->qty_on_barcode;
                    else $formula_2 = $calculate->formula_2;
                    

                    $material_check->total_linear_point = $calculate->total_linear_point;
                    $material_check->total_yds          = $calculate->total_yds;
                    $material_check->total_formula_1    = $formula_1;
                    $material_check->total_formula_2    = $formula_2;
                    if($formula_2 > 0)
                    {
                        $material_check->confirm_user_id = null;
                        $material_check->confirm_date    = null;
                    }
                    $material_check->final_result       = $calculate->final_result;
                    $material_check->save();
                }
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }
}
