<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

class AccessoriesReportMaterialMonitoringAllocation extends Controller
{
    public function index()
    {
        return view('accessories_report_material_monitoring_allocation.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id       = $request->warehouse;
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
            
            $data     = DB::table('material_monitoring_allocation_v')
            ->where('warehouse','LIKE',"%$warehouse_id%")
            ->whereBetween('lc_date',[$start_date,$end_date])
            ->orderby('lc_date')
            ->take(100);

            
            return DataTables::of($data)
            ->editColumn('warehouse',function ($data)
            {
                if($data->warehouse == '1000002') return 'Warehouse fabric AOI 1';
                elseif($data->warehouse == '1000013') return 'Warehouse fabric AOI 2';
            })
            ->addColumn('action',function ($data)
            {
                return view('accessories_report_material_monitoring_allocation._action',[
                    'model'=> $data,
                    'detail' => route('accessoriesReportMaterialMonitoringAllocation.detail',$data->id),
                ]);
                // return '-';
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    
    public function detail(Request $request, $id)
    {
        // $material_preparation_id          = $id;

        $obj             = new StdClass();
        $obj->id         = $id;
        return response()->json($obj,200);
        // return view('accessories_report_material_monitoring_allocation._detail',compact('material_preparation_id'));
    }

    public function detailData(Request $request)
    {
        if(request()->ajax()) 
        {
            $material_preparation_id      = $request->material_preparation_id;
            
            // $detail             = DB::select("SELECT material_preparations.barcode, users.id, users.name, users.nik, users.department, material_movements_1.status as status_akhir, COALESCE((SELECT material_movements.status from material_movement_lines LEFT JOIN material_movements on material_movements.id = material_movement_lines.material_movement_id WHERE material_movement_lines.created_at < material_movement_lines_1.created_at and material_preparation_id = '$material_preparation_id' ORDER BY material_movement_lines.created_at desc limit 1),'-') status_awal, locators_1.code as from_locator, locators_2.code as to_locator, material_movement_lines_1.qty_movement as qty from material_movement_lines material_movement_lines_1
            // LEFT JOIN material_movements material_movements_1 on material_movements_1.id = material_movement_lines_1.material_movement_id
            // LEFT JOIN locators locators_1 on material_movements_1.from_location = locators_1.id
            // LEFT JOIN locators locators_2 on material_movements_1.to_destination= locators_2.id
            // LEFT JOIN users on users.id = material_movement_lines_1.user_id
            // LEFT JOIN material_preparations on material_preparations.id = material_movement_lines_1.material_preparation_id
            // where material_preparation_id = '$material_preparation_id'
            // ORDER BY material_movement_lines_1.created_at");

            
            $detail = DB::select(db::raw("SELECT * FROM history_material_monitoring(
                '".$material_preparation_id."'
                );"
            ));           
            // select('history_material_monitoring($material_preparation_id)');


            
            return DataTables::of($detail)
            ->editColumn('name',function ($detail)
            {
                return ucwords($detail->name);
                // return number_format($detail->total_qty, 4, '.', ',').' ('.$detail->uom.')';
            })
            // ->editColumn('total_qty',function ($detail)
            // {
            //     return number_format($detail->total_qty, 4, '.', ',').' ('.$detail->uom.')';
            // })
            ->make(true);
        }
    }

    public function export(Request $request)
    {
        $warehouse_id       = $request->warehouse;
        $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
        $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
        
        $warehouse_name     = ($warehouse_id == '1000002' ?'AOI-1':'AOI-2' );
        
        $data     = DB::table('material_monitoring_allocation_v')
        ->where('warehouse','LIKE',"%$warehouse_id%")
        ->whereBetween('lc_date',[$start_date,$end_date])
        ->orderby('lc_date')
        ->get();

        $file_name = 'MATERIAL_MONITORING_ALLOCATION_'.$warehouse_name.'_FROM_LC_DATE_'.$start_date.'_TO_'.$end_date;
        
        return Excel::create($file_name,function($excel) use ($data)
        {
            $excel->setCreator('ICT')
            ->setCompany('AOI');
            
            $excel->sheet('ACTIVE',function($sheet)use($data)
            {
                
                $sheet->setCellValue('A1','AUTO_ALLOCATION_ID');
                $sheet->setCellValue('B1','LC_DATE');
                $sheet->setCellValue('C1','BARCODE');
                $sheet->setCellValue('D1','DOCUMENT_NO');
                $sheet->setCellValue('E1','SUPPLIER');
                $sheet->setCellValue('F1','PO_BUYER');
                $sheet->setCellValue('G1','ITEM_CODE');
                $sheet->setCellValue('H1','CATEGORY');
                $sheet->setCellValue('I1','STYLE');
                $sheet->setCellValue('J1','ARTICLE');
                $sheet->setCellValue('K1','UOM');
                $sheet->setCellValue('L1','QTY');
                $sheet->setCellValue('M1','PIC');
                $sheet->setCellValue('N1','LAST_MOVEMENT_STATUS');
                $sheet->setCellValue('O1','LAST_MOVEMENT_DATE');
                $sheet->setCellValue('P1','LOCATOR');
                $sheet->setCellValue('Q1','WAREHOUSE');

            
            $row=2;
            foreach ($data as $i) 
            {  
                $sheet->setCellValue('A'.$row,$i->auto_allocation_id);
                $sheet->setCellValue('B'.$row,$i->lc_date);
                $sheet->setCellValue('C'.$row,$i->barcode);
                $sheet->setCellValue('D'.$row,$i->document_no);
                $sheet->setCellValue('E'.$row,$i->supplier_name);
                $sheet->setCellValue('F'.$row,$i->po_buyer);
                $sheet->setCellValue('G'.$row,$i->item_code);
                $sheet->setCellValue('H'.$row,$i->category);
                $sheet->setCellValue('I'.$row,$i->style);
                $sheet->setCellValue('J'.$row,$i->article_no);
                $sheet->setCellValue('K'.$row,$i->uom_conversion);
                $sheet->setCellValue('L'.$row,$i->qty_conversion);
                $sheet->setCellValue('M'.$row,$i->pic);
                $sheet->setCellValue('N'.$row,$i->last_status_movement);
                $sheet->setCellValue('O'.$row,$i->last_movement_date);
                $sheet->setCellValue('P'.$row,$i->locator);
                $warehouse_name     = ($i->warehouse == '1000002' ?'AOI-1':'AOI-2' );
                $sheet->setCellValue('Q'.$row,$warehouse_name);
                $row++;
            }
            });

        })
        ->export('xlsx');
    }

}
