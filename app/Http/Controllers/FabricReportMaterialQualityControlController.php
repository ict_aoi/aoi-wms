<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Mail;
use Excel;
use Config;
use stdClass;
use Validator;
use Carbon\Carbon;
use IntlDateFormatter;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Defect;
use App\Models\Locator;
use App\Models\MaterialCheck;
use App\Models\MaterialStock;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\MaterialRequirement;
use App\Models\DetailMaterialCheck;
use App\Models\MaterialMovementLine;
use App\Models\MaterialRollHandoverFabric;
use App\Models\DetailMaterialPreparationFabric;

use Illuminate\Support\Facades\Session;

Config::set(['excel.export.calculate' => true]);

class FabricReportMaterialQualityControlController extends Controller
{
    public function index(Request $request)
    {
        return view('fabric_report_material_quality_control.index');
    }

    public function import(Request $request)
    {
        return view('fabric_report_material_quality_control.import');
    }
    
    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id           = ($request->warehouse? $request->warehouse : auth::user()->warehouse);
            $start_date             = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date               = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
            $status                 = $request->status;
            
            if($status == '')
            {
              $fabric_inspections = DB::select(db::raw("SELECT * FROM get_report_material_inspect_new(
                '".$warehouse_id."', '".$start_date."',  '".$end_date."' 
                )"));

                //dd($fabric_inspections);
              // $fabric_inspections = db::table('fabric_inspection_report_new_v')
              //                     ->where('warehouse_id',$warehouse_id)
              //                     ->whereBetween('receiving_date',[$start_date,$end_date])
              //                     ->orderBy('receiving_date','desc');
            }
            elseif($status == 'on progress')
            {
              $fabric_inspections = DB::select(db::raw("SELECT * FROM get_report_material_inspect_new(
                '".$warehouse_id."', '".$start_date."',  '".$end_date."' 
                ) where total_inspected_batch_number < total_batch_number ;"));
              // $fabric_inspections = db::table('fabric_inspection_report_new_v')
              //                     ->where('warehouse_id',$warehouse_id)
              //                     ->whereRaw('total_inspected_batch_number < total_batch_number')
              //                     ->whereBetween('receiving_date',[$start_date,$end_date])
              //                     ->orderBy('receiving_date','desc');
            }
            elseif($status == 'completed')
            {
              $fabric_inspections = DB::select(db::raw("SELECT * FROM get_report_material_inspect_new(
                '".$warehouse_id."', '".$start_date."',  '".$end_date."' 
                ) where total_inspected_batch_number >= total_batch_number ;"));
              // $fabric_inspections = db::table('fabric_inspection_report_new_v')
              //                     ->where('warehouse_id',$warehouse_id)
              //                     ->whereRaw('total_inspected_batch_number >= total_batch_number')
              //                     ->whereBetween('receiving_date',[$start_date,$end_date])
              //                     ->orderBy('receiving_date','desc');
            }
            elseif($status == 'confirm incomplete')
            {
              $fabric_inspections = DB::select(db::raw("SELECT * FROM get_report_material_inspect_new(
                '".$warehouse_id."', '".$start_date."',  '".$end_date."' 
                ) where total_inspected_batch_number != 0 and completed != total_roll_inspected ;"));

              // $fabric_inspections = db::table('fabric_inspection_report_new_v')
              //                     ->where('warehouse_id',$warehouse_id)
              //                     ->where('total_inspected_batch_number', '!=', 0)
              //                     ->whereRaw('completed != total_roll_inspected')
              //                     ->whereBetween('receiving_date',[$start_date,$end_date])
              //                     ->orderBy('receiving_date','desc');
            }
            elseif($status == 'confirm complete')
            {
              $fabric_inspections = DB::select(db::raw("SELECT * FROM get_report_material_inspect_new(
                '".$warehouse_id."', '".$start_date."',  '".$end_date."' 
                ) where completed = total_roll_inspected ;"));

              // $fabric_inspections = db::table('fabric_inspection_report_new_v')
              //                     ->where('warehouse_id',$warehouse_id)
              //                     ->whereRaw('completed = total_roll_inspected')
              //                     ->whereBetween('receiving_date',[$start_date,$end_date])
              //                     ->orderBy('receiving_date','desc');
            }

            return DataTables::of($fabric_inspections)
            ->editColumn('receiving_date',function ($fabric_inspections)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $fabric_inspections->receiving_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('inspection_date',function ($fabric_inspections)
            {
                if($fabric_inspections->inspection_date) return  Carbon::createFromFormat('Y-m-d H:i:s', $fabric_inspections->inspection_date)->format('d/M/Y H:i:s');
                else return null;
                
            })
            ->editColumn('is_handover',function ($fabric_inspections)
            {
                if($fabric_inspections->is_handover) return  'FROM HANDOVER';
                else return 'FROM SUPPLIER';
                
            })
            ->editColumn('warehouse_id',function ($fabric_inspections)
            {
                if($fabric_inspections->warehouse_id == '1000001') return 'Warehouse fabric AOI 1';
                elseif($fabric_inspections->warehouse_id == '1000011') return 'Warehouse fabric AOI 2';
            })
            ->setRowAttr([
                'style' => function($fabric_inspections)
                {
                    if(($fabric_inspections->completed == $fabric_inspections->total_roll_inspected) && $fabric_inspections->total_inspected_batch_number != '0' ) 
                    {
                      return  'background-color: #d9ffde;';
                    }
                    elseif(($fabric_inspections->completed != $fabric_inspections->total_roll_inspected) && $fabric_inspections->total_batch_number != '0')
                    {
                      return  'background-color: #FFB74D;';
                    }
                },
            ])
            ->addColumn('action',function($fabric_inspections){
                return view('fabric_report_material_quality_control._action',[
                    'model'             => $fabric_inspections,
                    'action_view'       => 'action',
                    'detail'            => route('fabricReportMaterialQualityControl.detail',$fabric_inspections->id),
                    'print'             => route('fabricReportMaterialQualityControl.exportDetail',$fabric_inspections->id),
                ]);
            })
            ->rawColumns(['action','style']) 
            ->make(true);
        }
    }

    public function detail($id)
    {
        $header         = db::table('fabric_inspection_report_new_v')->where('id',$id)->first();
        $flag           = Session::get('flag');
        $document_no    = $header->document_no;
        $item_code      = $header->item_code;
        $style          = MaterialRequirement::select(db::raw("substr(job_order, 0, position(':' in job_order))as style"))
        ->whereIn('po_buyer',function($query) use($document_no,$item_code)
        {
            $query->select('po_buyer')
            ->from('allocation_items')
            ->where([
                ['document_no',$document_no],
                ['item_code',$item_code]
            ])
            ->groupby('po_buyer');
        })
        ->groupby(db::raw("substr(job_order, 0, position(':' in job_order))"))
        ->first();

        return view('fabric_report_material_quality_control.detail',compact('header','style','flag'));
    }

    public function dataDetail(Request $request,$id)
    {
        if(request()->ajax()) 
        {
            $monitoring_material_receive_id = $id;
            
            $details    = db::table('detail_material_check_fabric_v')
            ->where('monitoring_receiving_fabric_id',$monitoring_material_receive_id)
            ->orderBy('detail_material_check_fabric_v.created_at','desc');
            
            return DataTables::of($details)
            ->editColumn('created_at',function ($details)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $details->created_at)->format('d/M/Y H:i:s');
            })
            ->addColumn('pic_confirm_qc',function($details) use($id)
            {
              if($details->confirm_date) return $details->pic_confirm_qc.' at '.Carbon::createFromFormat('Y-m-d H:i:s', $details->confirm_date)->format('d/M/Y H:i:s');
              else return null;
              
            })
            ->editColumn('final_result',function($details) use($id){
                return view('fabric_report_material_quality_control._action',[
                  'model'            => $details,
                  'action_view'      => 'final_result'
                ]);
            })
            ->editColumn('actual_width',function($details) use($id)
            {
                return view('fabric_report_material_quality_control._action',[
                  'model'            => $details,
                  'action_view'      => 'actual_width'
                ]);
            })
            ->addColumn('remark',function($details) use($id){
                return $details->final_result;
            })
            ->addColumn('action',function($details) use($id){
                return view('fabric_report_material_quality_control._action',[
                    'model'         => $details,
                    'action_view'   => 'action',
                    'detail'        => route('fabricReportMaterialQualityControl.moreDetail', [$id,$details->id]),
                    'note'          => route('fabricReportMaterialQualityControl.updateNote',[$id,$details->id]),
                ]);
            })
            ->rawColumns(['actual_width','action','final_result','pic_confirm_qc']) 
            ->make(true);
        }
    }

    public function dataDetailOther(Request $request,$id)
    {
        if(request()->ajax()) 
        {
            $monitoring_material_receive_id = $id;
            
            $details    = MaterialStock::where('monitoring_receiving_fabric_id',$monitoring_material_receive_id)
              ->whereNotIn('id',function ($query)
              {
                $query->select('material_stock_id')
                ->from('material_checks')
                ->whereNotNull('material_stock_id');
              });
            
            return DataTables::of($details)
            ->editColumn('created_at',function ($details)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $details->created_at)->format('d/M/Y H:i:s');
            })
            ->make(true);
        }
    }

    public function DetailMaterialCheck(Request $request,$header_id,$id)
    {
        $material_check = MaterialCheck::find($id);
        $detail         = DetailMaterialCheck::where('material_check_id',$id)->get();
        $defect_codes   = Defect::orderBy('code','ASC')->get();
        $jumlah_defect['A'] = $material_check->jumlah_defect_a;
        $jumlah_defect['B'] = $material_check->jumlah_defect_b;
        $jumlah_defect['C'] = $material_check->jumlah_defect_c;
        $jumlah_defect['D'] = $material_check->jumlah_defect_d;
        $jumlah_defect['E'] = $material_check->jumlah_defect_e;
        $jumlah_defect['F'] = $material_check->jumlah_defect_f;
        $jumlah_defect['G'] = $material_check->jumlah_defect_g;
        $jumlah_defect['H'] = $material_check->jumlah_defect_h;
        $jumlah_defect['I'] = $material_check->jumlah_defect_i;
        $jumlah_defect['J'] = $material_check->jumlah_defect_j;
        $jumlah_defect['K'] = $material_check->jumlah_defect_k;
        $jumlah_defect['L'] = $material_check->jumlah_defect_l;
        $jumlah_defect['M'] = $material_check->jumlah_defect_m;
        $jumlah_defect['N'] = $material_check->jumlah_defect_n;
        $jumlah_defect['O'] = $material_check->jumlah_defect_o;
        $jumlah_defect['P'] = $material_check->jumlah_defect_p;
        $jumlah_defect['Q'] = $material_check->jumlah_defect_q;
        $jumlah_defect['R'] = $material_check->jumlah_defect_r;
        $jumlah_defect['S'] = $material_check->jumlah_defect_s;
        $jumlah_defect['T'] = $material_check->jumlah_defect_t;

        $obj = new stdClass();
        $obj->stock = $material_check->materialStock->qty_order;
        $obj->point_1 = $material_check->point_1;
        $obj->point_2 = $material_check->point_2;
        $obj->point_3 = $material_check->point_3;
        $obj->point_4 = $material_check->point_4;
        $obj->percentage = $material_check->percentage;
        $obj->total_point = $material_check->total_point;
        $obj->jumlah_defect = $jumlah_defect;
        
        return view('fabric_report_material_quality_control.detail_point',compact('header_id','material_check','detail','defect_codes','obj'));
    }

    public function updatePoint(Request $request,$header_id,$id)
    {
        if(Session::has('flag')) Session::forget('flag');

        $header         = json_decode($request->header);
        $detail         = json_decode($request->detail_fir);
        $list_deleted   = json_decode($request->list_deleted);
        $material_check = MaterialCheck::find($id);
  
        try 
        {
            DB::beginTransaction();

            if($list_deleted !=null)
            {
                foreach($list_deleted as $i)
                {
                    DetailMaterialCheck::where('id',$i)->delete();
                }
            }
    
            if($detail!=null)
            {
                foreach($detail as $i)
                {
                    DetailMaterialCheck::where('id',$i->id)->update([
                      'start_point_check' => $i->start_point_check,
                      'end_point_check'   => $i->end_point_check,
                      'defect_code'       => $i->defect_code,
                      'defect_value'      => $i->defect_value,
                      'is_selected_1'     => $i->is_selected_1,
                      'is_selected_2'     => $i->is_selected_2,
                      'is_selected_3'     => $i->is_selected_3,
                      'is_selected_4'     => $i->is_selected_4,
                      'multiply'          => $i->multiply,
                      'total_point_defect'=> $i->total_point_defect,
                      'remark'            => trim($i->remark),
                    ]);
                }
            }

            $this->countDefectCode($id);
            $this->calculateFIR($id);
    
            $material_check->point_1          = $header->point_1;
            $material_check->point_2          = $header->point_2;
            $material_check->point_3          = $header->point_3;
            $material_check->point_4          = $header->point_4;
            $material_check->total_point      = $header->total_point;
            $material_check->percentage       = $header->percentage;
            $material_check->final_result     = $header->status;
            $material_check->confirm_date     = null;
            $material_check->confirm_user_id  = null;
            $material_check->save();

            DB::commit();

            Session::flash('flag', 'success');
            return response()->json(200);
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function countDefectCode($id)
    {
        try 
        {
            DB::beginTransaction();

            $defect_codes = Defect::orderby('code','asc')->get();
            $array        = array();

            foreach ($defect_codes as $key => $defect_code) 
            {
              $code               = $defect_code->code;
              $total_defect_code = DetailMaterialCheck::where([
                ['material_check_id',$id],
                ['defect_code',$code],
              ])
              ->sum('total_point_defect');
              
                $obj                = new stdClass();
                $obj->id            = $id;
                $obj->defect_code   = $code;
                $obj->total_defect  = $total_defect_code;
                $array[] = $obj;
            }

            
            foreach ($array as $key => $value) 
            {
                $material_check = MaterialCheck::find($value->id);

                if($value->defect_code == 'A') $material_check->jumlah_defect_a = $value->total_defect;
                else if($value->defect_code == 'B') $material_check->jumlah_defect_b = $value->total_defect;
                else if($value->defect_code == 'C') $material_check->jumlah_defect_c = $value->total_defect;
                else if($value->defect_code == 'D') $material_check->jumlah_defect_d = $value->total_defect;
                else if($value->defect_code == 'E') $material_check->jumlah_defect_e = $value->total_defect;
                else if($value->defect_code == 'F') $material_check->jumlah_defect_f = $value->total_defect;
                else if($value->defect_code == 'G') $material_check->jumlah_defect_g = $value->total_defect;
                else if($value->defect_code == 'H') $material_check->jumlah_defect_h = $value->total_defect;
                else if($value->defect_code == 'I') $material_check->jumlah_defect_i = $value->total_defect;
                else if($value->defect_code == 'J') $material_check->jumlah_defect_j = $value->total_defect;
                else if($value->defect_code == 'K') $material_check->jumlah_defect_k = $value->total_defect;
                else if($value->defect_code == 'L') $material_check->jumlah_defect_l = $value->total_defect;
                else if($value->defect_code == 'M') $material_check->jumlah_defect_m = $value->total_defect;
                else if($value->defect_code == 'N') $material_check->jumlah_defect_n = $value->total_defect;
                else if($value->defect_code == 'O') $material_check->jumlah_defect_o = $value->total_defect;
                else if($value->defect_code == 'P') $material_check->jumlah_defect_p = $value->total_defect;
                else if($value->defect_code == 'Q') $material_check->jumlah_defect_q = $value->total_defect;
                else if($value->defect_code == 'R') $material_check->jumlah_defect_r = $value->total_defect;
                else if($value->defect_code == 'S') $material_check->jumlah_defect_s = $value->total_defect;
                else if($value->defect_code == 'T') $material_check->jumlah_defect_t = $value->total_defect;

                $material_check->save();
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function updateFinalResult(Request $request,$header_id,$id)
    {
        try 
        {
            DB::beginTransaction();

            $material_check                             = MaterialCheck::find($id);
            $material_stock_id                          = $material_check->material_stock_id;
            $qty_reject                                 = sprintf('%0.8f',($material_check->total_formula_1 ? $material_check->total_formula_1 : $material_check->total_formula_2));
            $final_result                               = $request->selected_value;

            $material_check->final_result               = $final_result;
            $material_check->confirm_user_id            = auth::user()->id;
            $material_check->confirm_date               = carbon::now();
            $material_check->save();

            if($final_result == 'REJECT')
            {
                $material_stock                         = MaterialStock::find($material_stock_id);
                $curr                                   = $material_stock->qty_reject_by_inspect;  
                
                $material_stock->qc_result              = 'REJECT BY QC';
                $material_stock->is_reject              = true;
                $material_stock->qty_reject_by_inspect  = sprintf('%0.8f',$curr + $qty_reject);
                $material_stock->save();
            }

            DB::commit();

            return response()->json('success',200);
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function updateNote(Request $request,$header_id,$id)
    {
        try 
        {
            DB::beginTransaction();

            $material_check                     = MaterialCheck::find($id);
            $material_check->note               = trim(strtolower($request->note));
            $material_check->save();

            DB::commit();

            return response()->json('success',200);
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function exportDetail(Request $request,$id)
    {
        $header  = db::table('fabric_inspection_report_new_v')
        ->where('id',$id)
        ->first();

        $details            = MaterialCheck::select('material_checks.*','material_stocks.load_actual','material_stocks.qty_order')
        ->join('material_stocks','material_stocks.id','material_checks.material_stock_id')
        ->whereIn('material_stock_id',function($query) use($id)
        {
            $query->select('id')
            ->from('material_stocks')
            ->where('monitoring_receiving_fabric_id',$id);
        })
        ->orderBy('material_checks.created_at','desc')
        ->get();

        $file_name = 'Fabric Inspection Report';
        return Excel::create($file_name,function($excel) use ($header,$details)
        {
            $excel->sheet('active',function($sheet) use ($header,$details)
            {
                //Header
                $this->setHeaderStyle($sheet);
                $this->setValueToHeader($sheet,$header);
                $this->setStyleDetailHeader($sheet);
                $this->setValueToDetailHeader($sheet);
           
                //Detail Body
                $row = 20;
                foreach ($details as $i)
                {
                    $this->setStyleDetailBody($sheet,$row);
                    $this->setValueToDetailBody($sheet,$i,$row);
                    $row++;
                }
                
                $this->setStyleTotalDetailBody($sheet,$row+1);
                $row = $this->setValueTotalDetailBody($sheet,$row+1,$row);

                $this->setSignature($sheet,$row);
                $sheet->setOrientation('landscape');
            });
        })->export('xlsx');
    }

    public function exportAll(Request $request)
    {
      $warehouse = ($request->warehouse) ? $request->warehouse : auth::user()->warehouse;;
      if($warehouse == '1000011') $filename = 'report_inspect_qc_fab_all_aoi2.csv';
      else if($warehouse == '1000001') $filename = 'report_inspect_qc_fab_all_aoi1.csv';
    
        $file = Config::get('storage.report') . '/' . $filename;
    
        if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';
        
        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function store(Request $request)
    {
        /*
          INI DI PAKAI DIMANA YA
        if($request->hasFile('upload_file')){
            $validator = Validator::make($request->all(), [
              'upload_file' => 'required|mimes:xls'
            ]);
  
            $path   = $request->file('upload_file')->getRealPath();
            $data   = Excel::selectSheets('data_fir')->load($path,function($render){})->get();
            $result = array();
            
            if(!empty($data) && $data->count())
            {
                try 
                {
                    DB::beginTransaction();
                    $confirm_date = carbon::now()->todatetimestring();

                    foreach(json_decode($data) as $key => $value)
                    {
                        $material_check_id          = $value->material_check_id;
                        $final_result               = strtoupper(trim($value->final_result));
                        $confirmation_by_warehouse  = strtoupper(trim($value->confirmation_by_warehouse));
                        $supplier_name              = strtoupper(trim($value->supplier_name));
                        $document_no                = strtoupper(trim($value->document_no));
                        $no_invoice                 = strtoupper(trim($value->no_invoice));
                        $item_code                  = strtoupper(trim($value->item_color_width));
                        $nomor_roll                 = strtoupper(trim($value->no_roll));
                        $batch_number               = strtoupper(trim($value->batch_number));
                        
                        if($confirmation_by_warehouse == 'TRUE' || $confirmation_by_warehouse == 'T')
                        {
                          
                          if($final_result == 'QUARANTINE' || 
                          $final_result == 'RELEASE' || 
                          $final_result == 'SELEKSI PANEL' ||
                          $final_result == 'SPESIAL MARKER' ||
                          $final_result == 'HOLD' ||
                          $final_result == 'REJECT' )
                          {

                            $material_check = MaterialCheck::find($material_check_id);
                            if($material_check)
                            {
                              $material_stock_id                          = $material_check->material_stock_id;
                              $old_final_result                           = $material_check->final_result;
                              $old_confirmation_date                      = $material_check->confirm_date;
                              $qty_reject                                 = sprintf('%0.8f',($material_check->total_formula_1 ? $material_check->total_formula_1 : $material_check->total_formula_2));
                            
                              if($final_result != 'REJECT' && $old_final_result == 'REJECT' && $old_confirmation_date != null)
                              {
                                $obj                  = new stdClass();
                                $obj->supplier_name   = $supplier_name;
                                $obj->document_no     = $document_no;
                                $obj->no_invoice      = $no_invoice;
                                $obj->item_code       = $item_code;
                                $obj->nomor_roll      = $nomor_roll;
                                $obj->batch_number    = $batch_number;
                                $obj->final_result    = $final_result;
                                $obj->confirm_by      = null;
                                $obj->confirm_date    = null;
                                $obj->is_error        = true;
                                $obj->status          = 'Roll ini sudah direject sebelumnya';
                                $result []            = $obj;
                              }else
                              {
                                if($final_result == 'REJECT')
                                {
                                  $material_stock                         = MaterialStock::find($material_stock_id);
                                  $curr                                   = $material_stock->qty_reject_by_inspect;  
                                  
                                  $material_stock->qc_result                        = 'REJECT';
                                  $material_stock->is_reject                        = true;
                                  $material_stock->qty_reject_by_inspect            = sprintf('%0.8f',$curr + $qty_reject);
                                  $material_stock->save();
                                }else
                                {
                                  $material_stock                         = MaterialStock::find($material_stock_id);
                                  $material_stock->qc_result              = $final_result;
                                  $material_stock->save();
                                }
                                

                                $material_check->final_result               = $final_result;
                                $material_check->confirm_user_id            = auth::user()->id;
                                $material_check->confirm_date               = $confirm_date;
                                $material_check->save();
                                
                                $obj                  = new stdClass();
                                $obj->supplier_name   = $supplier_name;
                                $obj->document_no     = $document_no;
                                $obj->no_invoice      = $no_invoice;
                                $obj->item_code       = $item_code;
                                $obj->nomor_roll      = $nomor_roll;
                                $obj->batch_number    = $batch_number;
                                $obj->final_result    = $final_result;
                                $obj->confirm_by      = auth::user()->name;
                                $obj->confirm_date    = $confirm_date;
                                $obj->is_error        = false;
                                $obj->status          = 'success';
                                $result []            = $obj;
                              }
                           }else
                            {
                              $obj                  = new stdClass();
                              $obj->supplier_name   = $supplier_name;
                              $obj->document_no     = $document_no;
                              $obj->no_invoice      = $no_invoice;
                              $obj->item_code       = $item_code;
                              $obj->nomor_roll      = $nomor_roll;
                              $obj->batch_number    = $batch_number;
                              $obj->final_result    = $final_result;
                              $obj->confirm_by      = null;
                              $obj->confirm_date    = null;
                              $obj->is_error        = true;
                              $obj->status          = 'DATA TIDAK DITEMUKAN';
                              $result []            = $obj;
                            }
                             
                          }else
                          {
                            $obj                  = new stdClass();
                            $obj->supplier_name   = $supplier_name;
                            $obj->document_no     = $document_no;
                            $obj->no_invoice      = $no_invoice;
                            $obj->item_code       = $item_code;
                            $obj->nomor_roll      = $nomor_roll;
                            $obj->batch_number    = $batch_number;
                            $obj->final_result    = $final_result;
                            $obj->confirm_by      = null;
                            $obj->confirm_date    = null;
                            $obj->is_error        = true;
                            $obj->status          = 'Status salah, status yang kita punya adalah QUARANTINE,RELEASE,SELEKSI PANEL,SPESIAL MARKER,HOLD,REJECT';
                            $result []            = $obj;
                          }
                        }
                    }

                    DB::commit();
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                return response()->json($result,200);
            }
            
            return response()->json($result,200);
        }
        return response()->json('Import failed, please check again',422);*/
    }

    public function confirmFinalResult(Request $request)
    {
      $list_confirms  = json_decode($request->list_confirm);
      $confirm_date   = carbon::now()->todatetimestring();

      foreach ($list_confirms as $key => $list_confirm) 
      {
          try 
          {
              DB::beginTransaction();
              $final_result                               = strtoupper(trim($list_confirm->selected_value));
              $material_check                             = MaterialCheck::find($list_confirm->id);
              $material_check->final_result               = $final_result;
              $material_check->confirm_user_id            = auth::user()->id;
              $material_check->confirm_date               = $confirm_date;
              $material_check->save();

              $material_stock = MaterialStock::find($material_check->material_stock_id);
              if($final_result =='REJECT')
              {
                $qty_reject                                       = sprintf('%0.8f',($material_check->total_formula_1 ? $material_check->total_formula_1 : $material_check->total_formula_2));
                $curr                                             = $material_stock->qty_reject_by_inspect;  
                $material_stock->is_reject                        = true;
                $material_stock->qty_reject_by_inspect            = sprintf('%0.8f',$curr + $qty_reject);
              }else if($final_result == 'SELEKSI PANEL' || $final_result == 'SPESIAL MARKER')
              {
                  $qty_issue          = sprintf('%0.8f',$list_confirm->qty_issue);
                  if($qty_issue > 0)
                  {
                    // 1. insert internal issue
                    $material_stock_id  = $material_stock->id;
                    $item_id            = $material_stock->item_id;
                    $po_detail_id       = $material_stock->po_detail_id;
                    $c_order_id         = $material_stock->c_order_id;
                    $item_code          = $material_stock->item_code;
                    $supplier_name      = $material_stock->supplier_name;
                    $document_no        = $material_stock->document_no;
                    $c_bpartner_id      = $material_stock->c_bpartner_id;
                    $nomor_roll         = $material_stock->nomor_roll;
                    $uom                = $material_stock->uom;


                    if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                    {
                        $no_packing_list        = null;
                        $no_invoice             = null;
                        $c_orderline_id         = null;
                        $warehouse_id           = $material_stock->warehouse_id;
                    
                    }else
                    {
                        $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                        ->whereNull('material_roll_handover_fabric_id')
                        ->where('po_detail_id',$po_detail_id)
                        ->first();

                        $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : null);
                        $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : null);
                        $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : null);
                        $warehouse_id           = ($material_arrival ? $material_arrival->warehouse_id : $material_stock->warehouse_id) ;
                    }

                    $inventory_erp = Locator::with('area')
                    ->where([
                        ['is_active',true],
                        ['rack','ERP INVENTORY'],
                    ])
                    ->whereHas('area',function ($query) use($warehouse_id)
                    {
                        $query->where('is_active',true);
                        $query->where('warehouse',$warehouse_id);
                        $query->where('name','ERP INVENTORY');
                    })
                    ->first();

                    $reject_erp = Locator::whereHas('area',function ($query) use($warehouse_id)
                    {
                        $query->where('is_active',true);
                        $query->where('warehouse',$warehouse_id);
                        $query->where('name','ERP REJECT');
                    })
                    ->first();

                    $is_movement_exists = MaterialMovement::where([
                        ['from_location',$inventory_erp->id],
                        ['to_destination',$inventory_erp->id],
                        ['from_locator_erp_id',$inventory_erp->area->erp_id],
                        ['to_locator_erp_id',$inventory_erp->area->erp_id],
                        ['is_integrate',false],
                        ['is_active',true],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        ['status','integration-to-inventory-erp'],
                    ])
                    ->first();

                    if(!$is_movement_exists)
                    {
                        $material_movement = MaterialMovement::firstOrCreate([
                            'from_location'         => $inventory_erp->id,
                            'to_destination'        => $inventory_erp->id,
                            'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                            'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                            'is_integrate'          => false,
                            'is_active'             => true,
                            'no_packing_list'       => $no_packing_list,
                            'no_invoice'            => $no_invoice,
                            'status'                => 'integration-to-inventory-erp',
                            'created_at'            => $confirm_date,
                            'updated_at'            => $confirm_date,
                        ]);

                        $material_movement_id = $material_movement->id;
                    }else
                    {
                        $is_movement_exists->updated_at = $confirm_date;
                        $is_movement_exists->save();

                        $material_movement_id = $is_movement_exists->id;
                    }

                    $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                    ->where([
                        ['material_movement_id',$material_movement_id],
                        ['item_id',$item_id],
                        ['warehouse_id',$warehouse_id],
                        ['material_stock_id',$material_stock_id],
                        ['qty_movement',$qty_issue],
                        ['date_movement',$confirm_date],
                        ['is_integrate',false],
                        ['is_active',true],
                    ])
                    ->exists();

                    if(!$is_material_movement_line_exists)
                    {
                        MaterialMovementLine::Create([
                            'material_movement_id'          => $material_movement_id,
                            'material_stock_id'             => $material_stock_id,
                            'item_code'                     => $item_code,
                            'item_id'                       => $item_id,
                            'c_order_id'                    => $c_order_id,
                            'c_orderline_id'                => $c_orderline_id,
                            'c_bpartner_id'                 => $c_bpartner_id,
                            'supplier_name'                 => $supplier_name,
                            'type_po'                       => 1,
                            'is_integrate'                  => false,
                            'uom_movement'                  => $uom,
                            'qty_movement'                  => $qty_issue,
                            'date_movement'                 => $confirm_date,
                            'created_at'                    => $confirm_date,
                            'updated_at'                    => $confirm_date,
                            'date_receive_on_destination'   => $confirm_date,
                            'nomor_roll'                    => $nomor_roll,
                            'warehouse_id'                  => $warehouse_id,
                            'document_no'                   => $document_no,
                            'note'                          => 'PROSES INTEGRASI DIKARNAKAN HASIL QC RESULTNYA '.$final_result,
                            'is_active'                     => true,
                            'user_id'                       => auth::user()->id,
                        ]);
                    }

                    // 2. insert internal movement reject
                    $is_movement_reject_exists = MaterialMovement::where([
                        ['from_location',$inventory_erp->id],
                        ['to_destination',$reject_erp->id],
                        ['from_locator_erp_id',$inventory_erp->area->erp_id],
                        ['to_locator_erp_id',$reject_erp->area->erp_id],
                        ['is_integrate',false],
                        ['is_active',true],
                        ['no_invoice',$no_invoice],
                        ['no_packing_list',$no_packing_list],
                        ['status','integration-to-locator-reject-erp'],
                    ])
                    ->first();

                    if(!$is_movement_reject_exists)
                    {
                        $material_movement_reject = MaterialMovement::firstOrCreate([
                            'from_location'         => $inventory_erp->id,
                            'to_destination'        => $reject_erp->id,
                            'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                            'to_locator_erp_id'     => $reject_erp->area->erp_id,
                            'is_integrate'          => false,
                            'is_active'             => true,
                            'no_invoice'            => $no_invoice,
                            'no_packing_list'       => $no_packing_list,
                            'status'                => 'integration-to-locator-reject-erp',
                            'created_at'            => $confirm_date,
                            'updated_at'            => $confirm_date,
                            'no_packing_list'       => $no_packing_list,
                            'no_invoice'            => $no_invoice,
                        ]);

                        $material_movement_reject_id = $material_movement_reject->id;
                    }else
                    {
                        $is_movement_reject_exists->created_at = $confirm_date;
                        $is_movement_reject_exists->updated_at = $confirm_date;
                        $is_movement_reject_exists->save();

                        $material_movement_reject_id = $is_movement_reject_exists->id;
                    }


                    $is_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                    ->where([
                      ['material_movement_id',$material_movement_reject_id],
                      ['item_id',$item_id],
                      ['warehouse_id',$warehouse_id],
                      ['material_stock_id',$material_stock_id],
                      ['qty_movement',$qty_issue],
                      ['date_movement',$confirm_date],
                      ['is_integrate',false],
                      ['is_active',true],
                    ])
                    ->exists();

                    if(!$is_line_exists)
                    {
                        MaterialMovementLine::Create([
                            'material_movement_id'                      => $material_movement_reject_id,
                            'material_stock_id'                         => $material_stock_id,
                            'item_code'                                 => $item_code,
                            'item_id'                                   => $item_id,
                            'type_po'                                   => 1,
                            'c_order_id'                                => $c_order_id,
                            'c_orderline_id'                            => $c_orderline_id,
                            'document_no'                               => $document_no,
                            'nomor_roll'                                => $nomor_roll,
                            'c_bpartner_id'                             => $c_bpartner_id,
                            'uom_movement'                              => $uom,
                            'qty_movement'                              => $qty_issue,
                            'date_movement'                             => $confirm_date,
                            'created_at'                                => $confirm_date,
                            'updated_at'                                => $confirm_date,
                            'date_receive_on_destination'               => $confirm_date,
                            'note'                                      => 'PROSES INTEGRASI KE LOCATOR REJECT ERP. PENYEBAB REJECTNYA ADALAH '.$final_result.' DILAKUKAN OLEH '.strtoupper(auth::user()->name),
                            'is_active'                                 => true,
                            'is_integrate'                              => false,
                            'warehouse_id'                              => $warehouse_id,
                            'supplier_name'                             => $supplier_name,
                            'is_inserted_to_material_movement_per_size' => false,
                            'user_id'                                   => auth::user()->id,
                        ]);
                    }
                  }
                  
              }

              $material_stock->qc_result = $final_result;
              $material_stock->save();
              
              DB::commit();
          } catch (Exception $e) 
          {
              DB::rollBack();
              $message = $e->getMessage();
              ErrorHandler::db($message);
          }
      }
    }

    public function exportSummary(Request $request)
    {
        $warehouse_id  = ($request->warehouse) ? $request->warehouse : auth::user()->warehouse;
        $start_date = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date." 00:00:00") : Carbon::now()->subDays(30);
        $end_date   = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date." 23:59:59") : Carbon::now();
        $status     = $request->status;
            
            if($status == '')
            {
              $data = db::table('fabric_inspection_report_new_v')
                                  ->where('warehouse_id',$warehouse_id)
                                  ->whereBetween('receiving_date',[$start_date,$end_date])
                                  ->orderBy('receiving_date','desc')
                                  ->get();
            }
            elseif($status == 'on progress')
            {
              $data = db::table('fabric_inspection_report_new_v')
                                  ->where('warehouse_id',$warehouse_id)
                                  ->whereRaw('total_inspected_batch_number < total_batch_number')
                                  ->whereBetween('receiving_date',[$start_date,$end_date])
                                  ->orderBy('receiving_date','desc')
                                  ->get();
            }
            elseif($status == 'completed')
            {
              $data = db::table('fabric_inspection_report_new_v')
                                  ->where('warehouse_id',$warehouse_id)
                                  ->whereRaw('total_inspected_batch_number >= total_batch_number')
                                  ->whereBetween('receiving_date',[$start_date,$end_date])
                                  ->orderBy('receiving_date','desc')
                                  ->get();
            }
            elseif($status == 'confirm incomplete')
            {
              $data = db::table('fabric_inspection_report_new_v')
                                  ->where('warehouse_id',$warehouse_id)
                                  ->where('total_inspected_batch_number', '!=', 0)
                                  ->whereRaw('completed != total_roll_inspected')
                                  ->whereBetween('receiving_date',[$start_date,$end_date])
                                  ->orderBy('receiving_date','desc')
                                  ->get();
            }
            elseif($status == 'confirm complete')
            {
              $data = db::table('fabric_inspection_report_new_v')
                                  ->where('warehouse_id',$warehouse_id)
                                  ->whereRaw('completed = total_roll_inspected')
                                  ->whereBetween('receiving_date',[$start_date,$end_date])
                                  ->orderBy('receiving_date','desc')
                                  ->get();
            }

        return Excel::create('report_summary_fabric_inspection_report',function($excel) use($data, $status){
          $excel->sheet('active',function($sheet) use ($data, $status){
            $sheet->setWidth(array(
              'A'=>5,
              'B'=>20,
              'B'=>23,
              'C'=>27,
              'D'=>18,
              'E'=>30,
              'F'=>21,
              'G'=>20,
            ));
            $sheet->setHeight(array(
              '1'=>20,
            ));
            $sheet->setCellValue('A1','No.');
            $sheet->setCellValue('B1','Po Detail Id');
            $sheet->setCellValue('C1','No PT');
            $sheet->setCellValue('D1','Source Material');
            $sheet->setCellValue('E1','Arrival Warehouse');
            $sheet->setCellValue('F1','Arrival Date');
            $sheet->setCellValue('G1','Inspect Date');
            $sheet->setCellValue('H1','Inspector Name');
            $sheet->setCellValue('I1','Supplier Name');
            $sheet->setCellValue('J1','Po Supplier');
            $sheet->setCellValue('K1','No Invoice');
            $sheet->setCellValue('L1','Item Code');
            $sheet->setCellValue('M1','Color');
            $sheet->setCellValue('N1','Total Batch Number');
            $sheet->setCellValue('O1','Total Inspected Batch Number');
            $sheet->setCellValue('P1','Total Roll');
            $sheet->setCellValue('Q1','Total Yard');
            $sheet->setCellValue('R1','Status');
            $sheet->setCellValue('S1', 'Locator');

            $row=2;
            foreach ($data as $key => $i)
            {
                $sheet->setHeight(array(
                    $row    =>  21,
                ));
                
                $sheet->setCellValue('A'.$row,$key+1);
                $sheet->setCellValue('B'.$row,$i->po_detail_id);
                $sheet->setCellValue('C'.$row,$i->no_pt);
                $sheet->setCellValue('D'.$row,$i->source_material);
                $sheet->setCellValue('E'.$row,$i->warehouse_name);
                $sheet->setCellValue('F'.$row,$i->receiving_date);
                $sheet->setCellValue('G'.$row,$i->inspection_date);
                $sheet->setCellValue('H'.$row,$i->name);
                $sheet->setCellValue('I'.$row,$i->supplier_name);
                $sheet->setCellValue('J'.$row,$i->document_no);
                $sheet->setCellValue('K'.$row,$i->no_invoice);
                $sheet->setCellValue('L'.$row,$i->item_code);
                $sheet->setCellValue('M'.$row,$i->color);
                $sheet->setCellValue('N'.$row,$i->total_batch_number);
                $sheet->setCellValue('O'.$row,$i->total_inspected_batch_number);
                $sheet->setCellValue('P'.$row,$i->total_all_roll);
                $sheet->setCellValue('Q'.$row,$i->total_all_yds);
                $sheet->setCellValue('R'.$row,$status);
                $sheet->setCellValue('S'.$row,$i->last_locator);
                $row++;
            }
          });
        })->export('xlsx');

    }

    public function dataDetailCuttableWidth($id)
    {
        $data = MaterialCheck::find($id);
        return response()->json($data,200);
    }

    public function updateActualWidth(Request $request)
    {
        $data               = MaterialCheck::find($request->id);
        $material_stock_id  = $data->material_stock_id;

        $data->begin_width  = $request->top_width;
        $data->middle_width = $request->middle_width;
        $data->end_width    = $request->end_width;
        $data->actual_width = $request->actual_width;
        $data->save();

        MaterialStock::find($material_stock_id)
        ->update([
            'begin_width'   => $request->top_width,
            'middle_width'  => $request->middle_width,
            'end_width'     => $request->end_width,
            'actual_width'  => $request->actual_width,
        ]);

        DetailMaterialPreparationFabric::where('material_stock_id',$material_stock_id)
        ->update([
            'begin_width'   => $request->top_width,
            'middle_width'  => $request->middle_width,
            'end_width'     => $request->end_width,
            'actual_width'  => $request->actual_width,
        ]);

        MaterialRollHandoverFabric::where('material_stock_id',$material_stock_id)
        ->update([
            'begin_width'   => $request->top_width,
            'middle_width'  => $request->middle_width,
            'end_width'     => $request->end_width,
            'actual_width'  => $request->actual_width,
        ]);

        return response()->json(200);

    }

    static function setHeaderStyle($sheet)
    {
        $sheet->mergeCells('B1:AI2');
  
        //Inspection Date
        $sheet->getRowDimension('3')->setRowHeight(19);
        $sheet->mergeCells('D3:G3');
  
        //Fabric Supplier
        $sheet->getRowDimension('4')->setRowHeight(19);
        $sheet->mergeCells('D4:G4');
  
        //DOC NO FIR
        $sheet->mergeCells('D7:G7');
  
        //INVOICE NO
        $sheet->mergeCells('D9:G9');
  
        //PRODUCT
        $sheet->mergeCells('D11:G11');
  
        //Fabric date received
        $sheet->mergeCells('I3:J3');
        $sheet->mergeCells('K3:M3');
  
        //Fabric received
        $sheet->mergeCells('I4:J4');
  
        //Fabric Inspected
        $sheet->mergeCells('I5:J5');
  
        //PERCENT INSPECTED
        $sheet->mergeCells('I6:J6');
        $sheet->setColumnFormat(array(
          'K6'=>'0.00%'
        ));
  
        //PO NUMBER
        $sheet->mergeCells('I7:J7');
        $sheet->mergeCells('K7:M7');
  
        //STYLE NAME
        $sheet->mergeCells('I9:J9');
        $sheet->mergeCells('K9:M9');
  
        //COLOR
        $sheet->mergeCells('I11:N11');
  
        //Item Desc
        $sheet->mergeCells('O11:R11');
        //Fabric Inspected
  
        //ROLL
  
        //Buyer Brand
        $sheet->mergeCells('N3:O3');

        $sheet->cell('B14:L14', function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('M14', function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('M15:AF15', function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('AG15:AH15', function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('AI14:AJ14', function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('AI18:AJ18', function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('AK14', function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('M16:AF16', function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('M17:AF17', function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('M18:AF18', function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('M19:AF19', function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('AG14:AH14', function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });
    }

    static function setValueToHeader($sheet,$data)
    {
        $document_no    = $data->document_no;
        $item_code      = $data->item_code;
        
        $style          = MaterialRequirement::select(db::raw("substr(job_order, 0, position(':' in job_order))as style"))
        ->whereIn('po_buyer',function($query) use($document_no,$item_code)
        {
            $query->select('po_buyer')
            ->from('allocation_items')
            ->where([
                ['document_no',$document_no],
                ['item_code',$item_code]
            ])
            ->groupby('po_buyer');
        })
        ->groupby(db::raw("substr(job_order, 0, position(':' in job_order))"))
        ->first();
  
  
        $sheet->setCellValue('B1','FABRIC INSPECTION REPORT');
        //Inspection Date
        $sheet->setCellValue('B3','INSPECTION DATE');
        $sheet->setCellValue('C3',':');
        $sheet->setCellValue('D3',$data->inspection_date);
  
        //Fabric Supplier
        $sheet->setCellValue('B4','FABRIC SUPPLIER');
        $sheet->setCellValue('C4',':');
        $sheet->setCellValue('D4',$data->supplier_name);
  
        //DOC NO FIR
        $sheet->setCellValue('B7','DOC NO FIR');
        $sheet->setCellValue('C7',':');
        $sheet->setCellValue('D7',null);
  
        //INVOICE NO
        $sheet->setCellValue('B9','INVOICE NO.');
        $sheet->setCellValue('C9',':');
        $sheet->setCellValue('D9',$data->no_invoice);
  
        //PRODUCT
        $sheet->setCellValue('B11','PRODUCT');
        $sheet->setCellValue('C11',':');
        $sheet->setCellValue('D11',$data->item_code);
  
        //Fabric date received
        $sheet->setCellValue('I3','FABRIC DATE RCVD');
        $sheet->setCellValue('K3',$data->receiving_date);
        
        //FABRIC RECEIVED
        $sheet->setCellValue('I4','FABRIC RECEIVED');
        $sheet->setCellValue('K4',$data->total_all_roll);
  
        $sheet->setCellValue('L4','ROLLS');
        
        $sheet->setCellValue('N4',$data->total_all_yds);
  
        $sheet->setCellValue('O4',$data->uom);
  
        //Fabric Inspected
        $sheet->setCellValue('I5','FABRIC INSPECTED');
        $sheet->setCellValue('K5',$data->total_roll_inspected);
        $sheet->setCellValue('L5','ROLLS');
        $sheet->setCellValue('N5',$data->total_yard);
        $sheet->setCellValue('O5',$data->uom);
  
        //Percent Inspected
        $sheet->setCellValue('I6','PERCENT INSPECT');
        $sheet->setCellValue('K6','=K5/K4');
        //PO NUMBER
        $sheet->setCellValue('I7','PO NUMBER');
        $sheet->setCellValue('K7',$data->document_no);
  
        //STYLE NAME
        $sheet->setCellValue('I9','STYLE NAME');
        $sheet->setCellValue('K9',($style)?$style->style:null);
  
        //COLOR
        $sheet->setCellValue('O11',$data->color);
  
        //ITEM DESC
        $sheet->setCellValue('I11',$data->composition);
  
        //Buyer Brand
        $sheet->setCellValue('N3','BUYER/BRAND');
        $sheet->setCellValue('P3','ADIDAS');
    }

    static function setStyleDetailHeader($sheet)
    {
        //Date OF
        $sheet->mergeCells('B14:C19');
  
        //Batch No
        $sheet->mergeCells('D14:D19');
  
        //Roll No
        $sheet->mergeCells('E14:E19');
  
        //LOT NO
        $sheet->mergeCells('F14:F19');
  
        //N.W.KG
        $sheet->mergeCells('G14:G19');
  
        //STICKER
        $sheet->mergeCells('H14:H19');
  
        //WIDTH ON BARCODE
        $sheet->mergeCells('I14:I19');
  
        //YARD ACTUAL
        $sheet->mergeCells('J14:J19');
  
        //YDS DIFF
        $sheet->mergeCells('K14:K19');
  
        //CUTTABLE WIDTH
        $sheet->mergeCells('L14:L19');
  
        //DEFECTIVE CODE
        $sheet->mergeCells('M14:AF14');
  
        //Baris 14-18
  
        //HURUF kolom 1
  
        //Detail Huruf kolom 1
        $sheet->mergeCells('N15:O15');
        $sheet->mergeCells('N16:O16');
        $sheet->mergeCells('N17:O17');
        $sheet->mergeCells('N18:O18');
  
        //HURUF kolom 2
  
        //Detail Huruf kolom 2
        $sheet->mergeCells('R15:S15');
        $sheet->mergeCells('R16:S16');
        $sheet->mergeCells('R17:S17');
        $sheet->mergeCells('R18:S18');
  
        //HURUF kolom 3
  
        //Detail Huruf kolom 3
        $sheet->mergeCells('V15:W15');
        $sheet->mergeCells('V16:W16');
        $sheet->mergeCells('V17:W17');
        $sheet->mergeCells('V18:W18');
  
        //HURUF kolom 4
  
        //Detail Huruf kolom 4
        $sheet->mergeCells('Z15:AA15');
        $sheet->mergeCells('Z16:AA16');
        $sheet->mergeCells('Z17:AA17');
        $sheet->mergeCells('Z18:AA18');
  
        //HURUF kolom 5
  
        //Detail Huruf kolom 5
        $sheet->mergeCells('AD15:AE15');
        $sheet->mergeCells('AD16:AE16');
        $sheet->mergeCells('AD17:AE17');
        $sheet->mergeCells('AD18:AE18');
  
        //TOTAL
        $sheet->mergeCells('AG14:AH14');
  
        //Linear POINT
        $sheet->mergeCells('AG15:AG19');
  
        //110SQ/YD
        $sheet->mergeCells('AH15:AH19');
  
        //Fabric replacement summary(yards)
        $sheet->mergeCells('AI14:AJ17');
  
        //FORMULA 1
        $sheet->mergeCells('AI18:AI19');
        //FORMULA 2
        $sheet->mergeCells('AJ18:AJ19');
  
        //REMARK
        $sheet->mergeCells('AK14:AK19');
  
        //set coulum width
        $sheet->setWidth('A',5);
        $sheet->setWidth('B',15);
        $sheet->setWidth('C',2);
        $sheet->setWidth('M',7);
        $sheet->setWidth('N',7);
        $sheet->setWidth('O',7);
        $sheet->setWidth('P',7);
        $sheet->setWidth('Q',7);
        $sheet->setWidth('R',7);
        $sheet->setWidth('S',7);
        $sheet->setWidth('T',7);
        $sheet->setWidth('U',7);
        $sheet->setWidth('V',7);
        $sheet->setWidth('W',7);
        $sheet->setWidth('X',7);
        $sheet->setWidth('Y',7);
        $sheet->setWidth('Z',7);
        $sheet->setWidth('AA',7);
        $sheet->setWidth('AB',7);
        $sheet->setWidth('AC',7);
        $sheet->setWidth('AD',7);
        $sheet->setWidth('AE',7);
  
        //Aligment
        $sheet->cells('B14:AK19', function($cells) 
        {
          $cells->setAlignment('center');
          $cells->setValignment('center');
          $cells->setFont(array(
            'family'     => 'Arial',
            'size'       => '10',
          ));
          $cells->setBackground('#bdc3c7');
        });
        $sheet->getStyle('B14:AK19')->getAlignment()->setWrapText(true);
        
    }

    static function setValueToDetailHeader($sheet)
    {
        //Date OF
        $sheet->setCellValue('B14','DATE OF');
  
        //Roll No
        $sheet->setCellValue('D14','ROLL NO');
  
        //BATCH NUMBER
        $sheet->setCellValue('E14','BATCH NUMBER');
  
        //LOT NO
        $sheet->setCellValue('F14','LOT ACTUAL');
  
        //N.W.KG
        $sheet->setCellValue('G14','N.W (KGS)');
  
        //STICKER
        $sheet->setCellValue('H14','QTY ON BARCODE');
  
        //WIDTH ON BARCODE
        $sheet->setCellValue('I14','WIDTH ON BARCODE');
  
        //YARD ACTUAL
        $sheet->setCellValue('J14','YARD ACTUAL QTY');
  
        //YDS DIFF
        $sheet->setCellValue('K14','YDS DIFF');
  
        //CUTTABLE WIDTH
        $sheet->setCellValue('L14','CUTTABLE WIDTH (INCH)');
  
        //DEFECTIVE CODE
        $sheet->setCellValue('M14','DEFECTIVE CODE');
  
        //Huruf Kolom 1
        $sheet->setCellValue('M15','A');
        $sheet->setCellValue('M16','B');
        $sheet->setCellValue('M17','C');
        $sheet->setCellValue('M18','D');
  
        //Detail Kolom 1
        $sheet->setCellValue('N15','THICK YARN');
        $sheet->setCellValue('N16','THIN YARN');
        $sheet->setCellValue('N17','HOLES/TEAR');
        $sheet->setCellValue('N18','BROKEN YARN');
  
        //Huruf Kolom 2
        $sheet->setCellValue('Q15','E');
        $sheet->setCellValue('Q16','F');
        $sheet->setCellValue('Q17','G');
        $sheet->setCellValue('Q18','H');
  
        //Detail Kolom 2
        $sheet->setCellValue('R15','MISSING YARN');
        $sheet->setCellValue('R16','KNOT');
        $sheet->setCellValue('R17','CREASE MARK');
        $sheet->setCellValue('R18','STREAK LINE');
  
        //Huruf Kolom 3
        $sheet->setCellValue('U15','I');
        $sheet->setCellValue('U16','J');
        $sheet->setCellValue('U17','K');
        $sheet->setCellValue('U18','L');
  
        //Detail Kolom 3
        $sheet->setCellValue('V15','SHADE BAR VERT');
        $sheet->setCellValue('V16','SHADE BAR HOT');
        $sheet->setCellValue('V17','DYE SPOT');
        $sheet->setCellValue('V18','STAIN/SOILED');
  
        //Huruf Kolom 4
        $sheet->setCellValue('Y15','M');
        $sheet->setCellValue('Y16','N');
        $sheet->setCellValue('Y17','O');
        $sheet->setCellValue('Y18','P');
  
        //Detail Kolom 4
        $sheet->setCellValue('Z15','FOREIGN YARN');
        $sheet->setCellValue('Z16','DIRTY MARK');
        $sheet->setCellValue('Z17','PRINTING DEFECT');
        $sheet->setCellValue('Z18','NEEDLE LINE');
  
        //Huruf Kolom 5
        $sheet->setCellValue('AC15','Q');
        $sheet->setCellValue('AC16','R');
        $sheet->setCellValue('AC17','S');
        $sheet->setCellValue('AC18','T');
  
        //Detail Kolom 5
        $sheet->setCellValue('AD15','UNEVEN DYE');
        $sheet->setCellValue('AD16','DUST');
        $sheet->setCellValue('AD17','JOIN STICH');
        $sheet->setCellValue('AD18','SKEWING');
  
        //TOTAL
        $sheet->setCellValue('AG14','TOTAL');
        
        //LINEAR POINT
        $sheet->setCellValue('AG15','Linear Point');
  
        //100SQ/YD
        $sheet->setCellValue('AH15','100sq/yd');
  
        //FABRIC REPLACEMENT
        $sheet->setCellValue('AI14','Fabric replacement summary(yards)');
  
        //FORMULA 1
        $sheet->setCellValue('AI18','FORMULA 1');
  
        //FORMULA 2
        $sheet->setCellValue('AJ18','FORMULA 2');
  
        //remark
        $sheet->setCellValue('AK14','REMARK');
  
        //SET NUMBER
        $sheet->setCellValue('M19','A');
        $sheet->setCellValue('N19','B');
        $sheet->setCellValue('O19','C');
        $sheet->setCellValue('P19','D');
        $sheet->setCellValue('Q19','E');
        $sheet->setCellValue('R19','F');
        $sheet->setCellValue('S19','G');
        $sheet->setCellValue('T19','H');
        $sheet->setCellValue('U19','I');
        $sheet->setCellValue('V19','J');
        $sheet->setCellValue('W19','K');
        $sheet->setCellValue('X19','L');
        $sheet->setCellValue('Y19','M');
        $sheet->setCellValue('Z19','N');
        $sheet->setCellValue('AA19','O');
        $sheet->setCellValue('AB19','P');
        $sheet->setCellValue('AC19','Q');
        $sheet->setCellValue('AD19','R');
        $sheet->setCellValue('AE19','S');
        $sheet->setCellValue('AF19','T');
    }

    static function setStyleDetailBody($sheet,$row)
    {
        $sheet->mergeCells('B'.$row.':C'.$row);
  
        $sheet->cells('B'.$row.':AJ'.$row, function($cells) 
        {
          $cells->setAlignment('center');
          $cells->setValignment('center');
          $cells->setFont(array(
            'family'     => 'Arial',
            'size'       => '10',
          ));
        });

        $sheet->cell('B'.$row.':AK'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('AJ'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });
    }

    static function setValueToDetailBody($sheet,$data,$row)
    {
        //Date Of
        $sheet->setCellValue('B'.$row,Carbon::parse($data->created_at)->format('d-m-Y'));
        //Roll No
        $sheet->setCellValue('D'.$row,$data->nomor_roll);
        //Batch Number
        $sheet->setCellValue('E'.$row,$data->batch_number);
        //Lot Actual
        $sheet->setCellValue('F'.$row,$data->load_actual);
        //N.W kg
        $sheet->setCellValue('G'.$row,$data->kg);
        //Sticker Yard
        $sheet->setCellValue('H'.$row,$data->qty_on_barcode);
        //Width On Barcode
        $sheet->setCellValue('I'.$row,$data->width_on_barcode);
        //Yrds Actual Qty
        $sheet->setCellValue('J'.$row,$data->actual_length);
        //Yds Diff
        $sheet->setCellValue('K'.$row,$data->different_yard);
        //Cuttable Width
        $sheet->setCellValue('L'.$row,$data->actual_width);
        //A
        $sheet->setCellValue('M'.$row,$data->jumlah_defect_a);
        //b
        $sheet->setCellValue('N'.$row,$data->jumlah_defect_b);
        //c
        $sheet->setCellValue('O'.$row,$data->jumlah_defect_c);
        //d
        $sheet->setCellValue('P'.$row,$data->jumlah_defect_d);
        //e
        $sheet->setCellValue('Q'.$row,$data->jumlah_defect_e);
        //f
        $sheet->setCellValue('R'.$row,$data->jumlah_defect_f);
        //g
        $sheet->setCellValue('S'.$row,$data->jumlah_defect_g);
        //h
        $sheet->setCellValue('T'.$row,$data->jumlah_defect_h);
        //i
        $sheet->setCellValue('U'.$row,$data->jumlah_defect_i);
        //j
        $sheet->setCellValue('V'.$row,$data->jumlah_defect_j);
        //k
        $sheet->setCellValue('W'.$row,$data->jumlah_defect_k);
        //l
        $sheet->setCellValue('X'.$row,$data->jumlah_defect_l);
        //m
        $sheet->setCellValue('Y'.$row,$data->jumlah_defect_m);
        //n
        $sheet->setCellValue('Z'.$row,$data->jumlah_defect_n);
        //o
        $sheet->setCellValue('AA'.$row,$data->jumlah_defect_o);
        //p
        $sheet->setCellValue('AB'.$row,$data->jumlah_defect_p);
        //q
        $sheet->setCellValue('AC'.$row,$data->jumlah_defect_q);
        //r
        $sheet->setCellValue('AD'.$row,$data->jumlah_defect_r);
        //s
        $sheet->setCellValue('AE'.$row,$data->jumlah_defect_s);
        //t
        $sheet->setCellValue('AF'.$row,$data->jumlah_defect_t);
        //Linear point
        $sheet->setCellValue('AG'.$row,$data->total_linear_point);
        //100sq
        $sheet->setCellValue('AH'.$row,$data->total_yds);
        //formula1
        $sheet->setCellValue('AI'.$row,$data->total_formula_1);
        //formula2
        $sheet->setCellValue('AJ'.$row,$data->total_formula_2);
        //remark
        $sheet->setCellValue('AK'.$row,$data->final_result);
    }

    static function setStyleTotalDetailBody($sheet,$row)
    {

        /*
        ============================================
        Row 1
        ============================================
        */
        //Aligment
        $sheet->cells('B'.$row.':AK'.$row, function($cells) 
        {
          $cells->setAlignment('center');
          $cells->setFont(array(
            'family'     => 'Arial',
            'size'       => '10',
          ));
        });
    
        /**===========================================================
                     END ROW 1
        ==============================================================**/
    
        /*
        ============================================
        Row 2
        ============================================
        */
        $row+=1;
        $sheet->cells('M'.$row.':Af'.$row, function($cells) 
        {
          $cells->setAlignment('center');
        });
        $sheet->setColumnFormat(array(
           'M'.$row.':AF'.$row => '0.00%'
        ));

        //Aligment
        $sheet->cells('B'.$row.':L'.$row, function($cells) 
        {
          $cells->setAlignment('left');
          $cells->setFont(array(
            'family'     => 'Arial',
            'size'       => '10',
          ));
          $cells->setBorder('thin','thin','thin','thin');
        });

        $sheet->mergeCells('AI'.$row.':AJ'.$row);
        $sheet->cell('AI'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cells('L'.$row.':AJ'.$row, function($cells)
        {
          $cells->setAlignment('center');
          $cells->setFont(array(
            'family'     => 'Arial',
            'size'       => '10',
          ));
          $cells->setBorder('thin','thin','thin','thin');
        });
        
        /**===========================================================
                     END ROW 2
        ==============================================================**/
    
          /*
          ============================================
          Row 3
          ============================================
          */
          $row+=1;
          //Aligment
          $sheet->cells('B'.$row.':K'.$row, function($cells) {
            $cells->setAlignment('left');
            $cells->setFont(array(
              'family'     => 'Arial',
              'size'       => '10',
            ));
            $cells->setBorder('thin','thin','thin','thin');
          });
         

          $sheet->cells('L'.$row.':AH'.$row, function($cells) {
            $cells->setAlignment('center');
            $cells->setFont(array(
              'family'     => 'Arial',
              'size'       => '10',
            ));
            $cells->setBorder('thin','thin','thin','thin');
          });
          /**===========================================================
                      END ROW 3
          ==============================================================**/
    
          /*
          ============================================
          Row 4
          ============================================
          */
          $row+=1;
    
          //Aligment
          $sheet->cells('B'.$row.':AB'.$row, function($cells) {
            $cells->setAlignment('left');
            $cells->setFont(array(
              'family'     => 'Arial',
              'size'       => '10',
            ));
            $cells->setBorder('thin','thin','thin','thin');
          });
         

          $sheet->cells('AC'.$row.':AH'.$row, function($cells) {
            $cells->setAlignment('center');
            $cells->setFont(array(
              'family'     => 'Arial',
              'size'       => '10',
            ));
            $cells->setBorder('thin','thin','thin','thin');
          });

         
    
          /**===========================================================
                      END ROW 4
          ==============================================================**/
    
          /*
          ============================================
          Row 5
          ============================================
          */
          $row+=1;
          //Aligment
          $sheet->cells('K'.$row.':AG'.$row, function($cells) {
            $cells->setAlignment('left');
            $cells->setFont(array(
              'family'     => 'Arial',
              'size'       => '10',
            ));
            $cells->setBorder('thin','thin','thin','thin');
          });

        

          
          $sheet->cells('AH'.$row.':AH'.$row, function($cells) {
            $cells->setAlignment('center');
            $cells->setFont(array(
              'family'     => 'Arial',
              'size'       => '10',
            ));
            $cells->setBorder('thin','thin','thin','thin');
          });

          
          /**===========================================================
                      END ROW 5
          ==============================================================**/
    
          /*
          ============================================
          Row 6
          ============================================
          */
          $row+=1;
          $sheet->setColumnFormat(array(
             'AG'.$row => '0.00%'
          ));
          //Aligment
          $sheet->cells('D'.$row.':AG'.$row, function($cells) {
            $cells->setAlignment('left');
            $cells->setFont(array(
              'family'     => 'Arial',
              'size'       => '10',
            ));
            $cells->setBorder('thin','thin','thin','thin');
          });

         
          $sheet->cells('AH'.$row.':AH'.$row, function($cells) {
            $cells->setAlignment('center');
            $cells->setFont(array(
              'family'     => 'Arial',
              'size'       => '10',
            ));
            $cells->setBorder('thin','thin','thin','thin');
          });
    
          /**===========================================================
                      END ROW 6
          ==============================================================**/
    
          /*
          ============================================
          Row 7
          ============================================
          */
          $row+=1;
          //FORMAT PERSENTASE
          $sheet->setColumnFormat(array(
            'AG'.$row => '0.00%'
          ));
          //Aligment
          $sheet->cells('D'.$row.':AG'.$row, function($cells) {
            $cells->setAlignment('left');
            $cells->setFont(array(
              'family'     => 'Arial',
              'size'       => '10',
            ));
            $cells->setBorder('thin','thin','thin','thin');
          });
          $sheet->cells('AH'.$row.':AH'.$row, function($cells) {
            $cells->setAlignment('center');
            $cells->setFont(array(
              'family'     => 'Arial',
              'size'       => '10',
            ));
            $cells->setBorder('thin','thin','thin','thin');
          });
    
          $_dump_row=$row-4;
          $sheet->mergeCells('AI'.$_dump_row.':AK'.$row);
          $sheet->cells('AI'.$_dump_row.':AJ'.$row, function($cells) {
            $cells->setFont(array(
              'family'     => 'Arial',
              'size'       => '20',
              'bold'       =>  true
            ));
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setBorder('thin','thin','thin','thin');
          });
          $sheet->setCellValue('AI'.$_dump_row,'AOI');

          $sheet->cells('AI'.$_dump_row, function($cells) {
            $cells->setBorder('thin','thin','thin','thin');
          });

          $sheet->cells('AK'.$_dump_row.':AK'.$row, function($cells) {
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setBorder('thin','thin','thin','thin');
          });
    
          /**===========================================================
                      END ROW 7
          ==============================================================**/
        
    }

    static function setValueTotalDetailBody($sheet,$row,$currentRow)
    {
        //Devider sebelum total
        $devider_row = $row-1;
        $sheet->mergeCells('B'.$devider_row.':AK'.$devider_row);
        $sheet->cell('B'.$devider_row.':AK'.$devider_row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        /*
        ============================================
        Row 1
        ============================================
        */
        
        $sheet->cell('B'.$row.':AK'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });
        //tootal
        $sheet->setCellValue('B'.$row,'TOTAL');
        //Roll No
        $total_row = $currentRow-20;
        $sheet->setCellValue('D'.$row,$total_row);
        //Lot No
        $sheet->setCellValue('F'.$row,'');
        //NWKGS
        $sheet->setCellValue('G'.$row,'');
        //Yds sticker
        $sheet->setCellValue('H'.$row,'=SUM(H20:H'.$currentRow.')');
        //Yard Actual
        $sheet->setCellValue('J'.$row,'=SUM(J20:J'.$currentRow.')');
        //YDS DIFF
        $sheet->setCellValue('K'.$row,'=SUM(K20:K'.$currentRow.')');
        //A
        $sheet->setCellValue('M'.$row,'=SUM(M20:M'.$currentRow.')');
        //B
        $sheet->setCellValue('N'.$row,'=SUM(N20:N'.$currentRow.')');
        //C
        $sheet->setCellValue('O'.$row,'=SUM(O20:O'.$currentRow.')');
        //D
        $sheet->setCellValue('P'.$row,'=SUM(P20:P'.$currentRow.')');
        //E
        $sheet->setCellValue('Q'.$row,'=SUM(Q20:Q'.$currentRow.')');
        //F
        $sheet->setCellValue('R'.$row,'=SUM(R20:R'.$currentRow.')');
        //G
        $sheet->setCellValue('S'.$row,'=SUM(S20:S'.$currentRow.')');
        //H
        $sheet->setCellValue('T'.$row,'=SUM(T20:T'.$currentRow.')');
        //I
        $sheet->setCellValue('U'.$row,'=SUM(U20:U'.$currentRow.')');
        //J
        $sheet->setCellValue('V'.$row,'=SUM(V20:V'.$currentRow.')');
        //K
        $sheet->setCellValue('W'.$row,'=SUM(W20:W'.$currentRow.')');
        //L
        $sheet->setCellValue('X'.$row,'=SUM(X20:X'.$currentRow.')');
        //M
        $sheet->setCellValue('Y'.$row,'=SUM(Y20:Y'.$currentRow.')');
        //N
        $sheet->setCellValue('Z'.$row,'=SUM(Z20:Z'.$currentRow.')');
        //O
        $sheet->setCellValue('AA'.$row,'=SUM(AA20:AA'.$currentRow.')');
        //P
        $sheet->setCellValue('AB'.$row,'=SUM(AB20:AB'.$currentRow.')');
        //Q
        $sheet->setCellValue('AC'.$row,'=SUM(AC20:AC'.$currentRow.')');
        //R
        $sheet->setCellValue('AD'.$row,'=SUM(AD20:AD'.$currentRow.')');
        //S
        $sheet->setCellValue('AE'.$row,'=SUM(AE20:AE'.$currentRow.')');
        //T
        $sheet->setCellValue('AF'.$row,'=SUM(AF20:AF'.$currentRow.')');
        //Linear Point
        $sheet->setCellValue('AG'.$row,'=SUM(AG20:AG'.$currentRow.')');
        //100Sq/yd
        $sheet->setCellValue('AH'.$row,'=SUM(AH20:AH'.$currentRow.')');
        //Formula 1
        $sheet->setCellValue('AI'.$row,'=SUM(AI20:AI'.$currentRow.')');
        //Formula 2
        $sheet->setCellValue('AJ'.$row,'=SUM(AJ20:AJ'.$currentRow.')');
  
        $sheet->cell('M'.$row.':AJ'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        /*
        ============================================
        Row 2
        ============================================
        */
        $currentRow+=1;
        $row+=1;
        
        $sheet->mergeCells('B'.$row.':J'.$row);
        $sheet->setCellValue('B'.$row,'Percentage per defect');
        //A
        $sheet->setCellValue('M'.$row,'=SUM(M'.$currentRow.'/'.$total_row.')/$J$'.$currentRow);
        //B
        $sheet->setCellValue('N'.$row,'=SUM(N'.$currentRow.'/'.$total_row.')/$J$'.$currentRow);
        //C
        $sheet->setCellValue('O'.$row,'=SUM(O'.$currentRow.'/'.$total_row.')/$J$'.$currentRow);
        //D
        $sheet->setCellValue('P'.$row,'=SUM(P'.$currentRow.'/'.$total_row.')/$J$'.$currentRow);
        //E
        $sheet->setCellValue('Q'.$row,'=SUM(Q'.$currentRow.'/'.$total_row.')/$J$'.$currentRow);
        //F
        $sheet->setCellValue('R'.$row,'=SUM(R'.$currentRow.'/'.$total_row.')/$J$'.$currentRow);
        //G
        $sheet->setCellValue('S'.$row,'=SUM(S'.$currentRow.'/'.$total_row.')/$J$'.$currentRow);
        //H
        $sheet->setCellValue('T'.$row,'=SUM(T'.$currentRow.'/'.$total_row.')/$J$'.$currentRow);
        //I
        $sheet->setCellValue('U'.$row,'=SUM(U'.$currentRow.'/'.$total_row.')/$J$'.$currentRow);
        //J
        $sheet->setCellValue('V'.$row,'=SUM(V'.$currentRow.'/'.$total_row.')/$J$'.$currentRow);
        //K
        $sheet->setCellValue('W'.$row,'=SUM(W'.$currentRow.'/'.$total_row.')/$J$'.$currentRow);
        //L
        $sheet->setCellValue('X'.$row,'=SUM(X'.$currentRow.'/'.$total_row.')/$J$'.$currentRow);
        //M
        $sheet->setCellValue('Y'.$row,'=SUM(Y'.$currentRow.'/'.$total_row.')/$J$'.$currentRow);
        //N
        $sheet->setCellValue('Z'.$row,'=SUM(Z'.$currentRow.'/'.$total_row.')/$J$'.$currentRow);
        //O
        $sheet->setCellValue('AA'.$row,'=SUM(AA'.$currentRow.'/'.$total_row.')/$J$'.$currentRow);
        //P
        $sheet->setCellValue('AB'.$row,'=SUM(AB'.$currentRow.'/'.$total_row.')/$J$'.$currentRow);
        //Q
        $sheet->setCellValue('AC'.$row,'=SUM(AC'.$currentRow.'/'.$total_row.')/$J$'.$currentRow);
        //R
        $sheet->setCellValue('AD'.$row,'=SUM(AD'.$currentRow.'/'.$total_row.')/$J$'.$currentRow);
        //S
        $sheet->setCellValue('AE'.$row,'=SUM(AE'.$currentRow.'/'.$total_row.')/$J$'.$currentRow);
        //T
        $sheet->setCellValue('AF'.$row,'=SUM(AF'.$currentRow.'/'.$total_row.')/$J$'.$currentRow);
        
        $sheet->cell('M'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('N'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('O'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('P'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('Q'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('R'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('S'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('T'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('U'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('V'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('W'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('X'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('Y'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('Z'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('AA'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });


        $sheet->cell('AB'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });


        $sheet->cell('AC'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });


        $sheet->cell('AD'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('AE'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        $sheet->cell('AF'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        //TOTAL REPLACEMENT
        $sheet->mergeCells('AG'.$row.':AH'.$row);
        $sheet->cell('AG'.$row.':AH'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });
        //$sheet->setCellValue('AF'.$row,'=SUM(AG'.$currentRow.'/'.$total_row.')/$I$'.$currentRow);
        $sheet->setCellValue('AG'.$row,'Total Replacement');
        //Fabric REPLACEMENT
        
        $sheet->setCellValue('AI'.$row,'=AI'.$currentRow.'+AJ'.$currentRow);
  
        //SET STYLE BORDER DARI B20 SAMPAI AJ ROW TERAKHIR
        $sheet->setBorder('B14:AJ'.$currentRow, 'thin');
        $sheet->cell('B'.$row.':AK'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });
        //END STYLE BORDER
        
        /*
        ============================================
        Row 3
        ============================================
        */
        $currentRow+=1;
        $row+=1;
  
        //Average RESULT
        $sheet->mergeCells('B'.$row.':J'.$row);
        $sheet->cell('B'.$row.':J'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });
        $sheet->setCellValue('B'.$row,'AVERAGE RESULT :');
        //Hasil AVERAGE
        $_dump_row=$row-2;
        $sheet->setCellValue('AH'.$row,'=AH'.$_dump_row.'/'.'D'.$_dump_row);
        
        $sheet->cell('AH'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        /*
        ============================================
        Row 4
        ============================================
        */
        $row+=1;
        $currentRow+=1;
        //Replacement Calculation Formula
        $sheet->mergeCells('B'.$row.':I'.$row);
        $sheet->cell('B'.$row.':I'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });
        $sheet->setCellValue('B'.$row,'Replacement Calculation Formula');
  
        //Formula 1
        $sheet->mergeCells('J'.$row.':AA'.$row);
        $sheet->cell('J'.$row.':AA'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });
        $sheet->setCellValue('J'.$row,'FORMULA 1 ( POINT LOWER THAN 15 POINTS)');
  
        //Result
        $sheet->mergeCells('AC'.$row.':AH'.$row);
        $sheet->cell('AC'.$row.':AH'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });
        $sheet->setCellValue('AC'.$row,'RESULT');
  
        /*
        ============================================
        Row 5
        ============================================
        */
        
        $row+=1;
        $currentRow+=1;
        //Formula 2
        $sheet->mergeCells('J'.$row.':AA'.$row);
        $sheet->cell('J'.$row.':AA'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });
        $sheet->cell('B'.$row.':I'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });
        $sheet->setCellValue('J'.$row,'FORMULA 2 ( POINT GREATHER  THAN 15 POINTS) = TOTAL POINT(ALL ROLLS) ; 4 X 1.25');
  
        //DEFECT POINT
        $sheet->mergeCells('AC'.$row.':AG'.$row);
        $sheet->cell('AC'.$row.':AG'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });
        $sheet->setCellValue('AC'.$row,'DEFECT  POINT Per Shipment');
  
        $_dump_row=$row-2;
        $sheet->setCellValue('AH'.$row,'=AH'.$_dump_row);
        
        /*
        ============================================
        Row 6
        ============================================
        */
        $row+=1;
        $currentRow+=1;
        //Pass
        $sheet->setCellValue('D'.$row,'PASS');
        $sheet->cell('B'.$row.':C'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });
        
        $sheet->setColumnFormat(array(
          'AH'.$row => '0.00%'
        ));
  
        //Classification
        $sheet->mergeCells('E'.$row.':I'.$row);
        $sheet->cell('E'.$row.':I'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });
        $sheet->setCellValue('E'.$row,'CLASSIFICATION');
  
        //Point Per Linear
        $sheet->mergeCells('J'.$row.':Z'.$row);
        $sheet->cell('J'.$row.':Z'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });
        $sheet->setCellValue('J'.$row,'POINT PER LINEAR ');
  
        //PER % REPLACEMENT
        $sheet->mergeCells('AC'.$row.':AG'.$row);
        $sheet->cell('AC'.$row.':AG'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });
        $sheet->setCellValue('AC'.$row,'PER % REPLACEMENT');
  
        $_dump_row=$row-5;
  
        $sheet->setCellValue('AH'.$row,'=(AI'.$_dump_row.'+AJ'.$_dump_row.')/H'.$_dump_row);
  
        /*
        ============================================
        Row 7
        ============================================
        */
        $row+=1;
        $currentRow+=1;
        $sheet->cell('B'.$row.':C'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });
        //Reject
        $sheet->setCellValue('D'.$row,'REJECT');
        $sheet->setColumnFormat(array(
          'AH'.$row => '0.00%'
        ));
  
        //Classification
        $sheet->mergeCells('E'.$row.':I'.$row);
        $sheet->cell('E'.$row.':I'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });
        $sheet->setCellValue('E'.$row,'GROUP POINTS');
  
        //
        $sheet->mergeCells('J'.$row.':Z'.$row);
        $sheet->cell('J'.$row.':Z'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });

        //YARD SHORTAGE
        $sheet->mergeCells('AC'.$row.':AG'.$row);
        $sheet->cell('AC'.$row.':AG'.$row, function($cell)
        {
          $cell->setBorder('thin','thin','thin','thin');
        });
        $sheet->setCellValue('AC'.$row,'YARD SHORTAGE');
  
        $sheet->setCellValue('AH'.$row,'=SUM(J'.$_dump_row.'-H'.$_dump_row.')/H'.$_dump_row);
  
  
        return $row;
    }

    static function setSignature($sheet,$row)
    {
        //row 1
        $startrow=$row+=10;
        //row 2
        $row+=1;
          //style
        $sheet->mergeCells('AB'.$row.':AD'.$row);
        $sheet->cells('AB'.$row.':AD'.$row, function($cells) {
          $cells->setAlignment('center');
          $cells->setFont(array(
            'family'     => 'Arial',
            'size'       => '10',
          ));
          $cells->setBorder('thin', 'none', 'none', 'none');
        });
        $sheet->mergeCells('AG'.$row.':AI'.$row);
        $sheet->cells('AG'.$row, function($cells) {
          $cells->setAlignment('center');
          $cells->setFont(array(
            'family'     => 'Arial',
            'size'       => '10',
          ));
          $cells->setBorder('thin', 'thick', 'none', 'none');
        });
          //VALUE
        $sheet->setCellValue('AB'.$row,'Inspector');
        $sheet->setCellValue('AG'.$row,'Fabric supervisor');
  
        //Row 3
        $row+=5;
        //style
        $sheet->mergeCells('AB'.$row.':AD'.$row);
        $sheet->cells('AB'.$row.':AD'.$row, function($cells) {
          $cells->setAlignment('center');
          $cells->setFont(array(
            'family'     => 'Arial',
            'size'       => '10',
          ));
          $cells->setBorder('thin', 'none', 'none', 'none');
        });
        $sheet->mergeCells('AG'.$row.':AI'.$row);
        $sheet->cells('AG'.$row, function($cells) {
          $cells->setAlignment('center');
          $cells->setFont(array(
            'family'     => 'Arial',
            'size'       => '10',
          ));
          $cells->setBorder('thin', 'thick', 'none', 'none');
        });
          //VALUE
        $sheet->setCellValue('AB'.$row,'Head of W/H dept');
        $sheet->setCellValue('AG'.$row,'QA Manager');
  
        //style
        $sheet->cells('AB'.$startrow.':AI'.$row, function($cells) {
          $cells->setBorder('thick', 'thick', 'thick', 'thick');
        });
    }

    static function calculateFIR($material_check_id)
    {
        $calculates = DB::select(db::raw("SELECT * FROM get_fir_calculation(
            '".$material_check_id."'
            );"
        ));
  
        foreach ($calculates as $key_2 => $calculate) 
        {
            $material_check = MaterialCheck::find($calculate->id);
            if($calculate->formula_1 > $material_check->qty_on_barcode) $formula_1 = $material_check->qty_on_barcode;
            else $formula_1 = $calculate->formula_1;
            if($calculate->formula_2 > $material_check->qty_on_barcode) $formula_2 = $material_check->qty_on_barcode;
            else $formula_2 = $calculate->formula_2;

            $material_check->total_linear_point = $calculate->total_linear_point;
            $material_check->total_yds          = $calculate->total_yds;
            $material_check->total_formula_1    = $formula_1;
            $material_check->total_formula_2    = $formula_2;
            $material_check->final_result       = $calculate->final_result;
            $material_check->save();
        }
    }

    static function sendEmailOutstandingConfirmation()
    {
        try
        {
            $cc         = ['aditya.eka@aoi.co.id','charis.azwar@aoi.co.id','indra.setyawan@aoi.co.id','sentot@aoi.co.id','silvester.deprianto@aoi.co.id','nicky.puspitasari@aoi.co.id', 'niam.alfiyan@aoi.co.id'];
            $emails     = ['numa.ryan@aoi.co.id','mega.antika@aoi.co.id','iin.sugiarti@aoi.co.id','diah.priatiningsih@aoi.co.id','dino.wicaksono@aoi.co.id','istriana.lab@aoi.co.id'];
            //$cc = ['charis.azwar@aoi.co.id'];
            //$emails = ['charis.azwar@aoi.co.id'];
            
            $data           = DB::select(db::raw("SELECT *  FROM outstanding_confirmation_qc_v")); //where material_check_id in( '816f19a0-099e-11ea-b515-9ffe6c395ca1','0b690ff0-09a0-11ea-8b1c-bf76fbc2001c')
            $data_limit_100 = DB::select(db::raw("SELECT *  FROM outstanding_confirmation_qc_v limit 100;"));
           

            $location = Config::get('storage.email_attachment_qc');
            if (!File::exists($location)) File::makeDirectory($location, 0777, true);
            
            //outstanding_confirmation_qc_v
            $send_date = carbon::now()->format('Ymd');
            $file_name = 'outstanding_confirmation_fir_'.$send_date;
            
            $file = Excel::create($file_name,function($excel) use($data){
                $excel->sheet('data_fir',function($sheet) use($data){
                    $sheet->setCellValue('A1','#');
                    $sheet->setCellValue('B1','MATERIAL_CHECK_ID');
                    $sheet->setCellValue('C1','DATE_FABRIC_RECEIVED');
                    $sheet->setCellValue('D1','DATE_INSPECTED');
                    $sheet->setCellValue('E1','INSPECTOR_NAME');
                    $sheet->setCellValue('F1','WAREHOUSE_NAME');
                    $sheet->setCellValue('G1','DOCUMENT_NO');
                    $sheet->setCellValue('H1','SUPPLIER_CODE');
                    $sheet->setCellValue('I1','SUPPLIER_NAME');
                    $sheet->setCellValue('J1','ITEM_COLOR_WIDTH');
                    $sheet->setCellValue('K1','NO_INVOICE');
                    $sheet->setCellValue('L1','NO_ROLL');
                    $sheet->setCellValue('M1','BATCH_NUMBER');
                    $sheet->setCellValue('N1','ACTUAL_LOT');
                    $sheet->setCellValue('O1','STICKER_YARDS');
                    $sheet->setCellValue('P1','ACTUAL_YARDS');
                    $sheet->setCellValue('Q1','POINT_A');
                    $sheet->setCellValue('R1','POINT_B');
                    $sheet->setCellValue('S1','POINT_C');
                    $sheet->setCellValue('T1','POINT_D');
                    $sheet->setCellValue('U1','POINT_E');
                    $sheet->setCellValue('V1','POINT_F');
                    $sheet->setCellValue('W1','POINT_G');
                    $sheet->setCellValue('X1','POINT_H');
                    $sheet->setCellValue('Y1','POINT_I');
                    $sheet->setCellValue('Z1','POINT_J');
                    $sheet->setCellValue('AA1','POINT_K');
                    $sheet->setCellValue('AB1','POINT_L');
                    $sheet->setCellValue('AC1','POINT_M');
                    $sheet->setCellValue('AD1','POINT_N');
                    $sheet->setCellValue('AE1','POINT_O');
                    $sheet->setCellValue('AF1','POINT_P');
                    $sheet->setCellValue('AG1','POINT_Q');
                    $sheet->setCellValue('AH1','POINT_R');
                    $sheet->setCellValue('AI1','POINT_S');
                    $sheet->setCellValue('AJ1','POINT_T');
                    $sheet->setCellValue('AK1','LINER_POINT');
                    $sheet->setCellValue('AL1','100_SQ_YD');
                    $sheet->setCellValue('AM1','FORMULA_1');
                    $sheet->setCellValue('AN1','FORMULA_2');
                    $sheet->setCellValue('AO1','FINAL_RESULT');
                    $sheet->setCellValue('AP1','CONFIRMATION_BY_WAREHOUSE');

                    $sheet->setAutoSize(true);
                    $sheet->cell('B1', function($cell) {
                        $cell->setBackground('#000000');
                        $cell->setFontColor('#ffffff');
                    });

                    $sheet->cell('AO1', function($cell) {
                      $cell->setBackground('#FFFF00');
                    });

                    $sheet->cell('AP1', function($cell) {
                      $cell->setBackground('#FFFF00');
                    });
                   
                    $row=2;
                    foreach ($data as $key => $datum) 
                    {  
                        $sheet->setCellValue('A'.$row,($key+1));
                        $sheet->setCellValue('B'.$row,$datum->material_check_id);
                        $sheet->setCellValue('C'.$row,$datum->date_fabric_received);
                        $sheet->setCellValue('D'.$row,$datum->date_inspected);
                        $sheet->setCellValue('E'.$row,$datum->inspector_name);
                        $sheet->setCellValue('F'.$row,$datum->warehouse_name);
                        $sheet->setCellValue('G'.$row,$datum->document_no);
                        $sheet->setCellValue('H'.$row,$datum->supplier_code);
                        $sheet->setCellValue('I'.$row,$datum->supplier_name);
                        $sheet->setCellValue('J'.$row,$datum->item_color_width);
                        $sheet->setCellValue('K'.$row,$datum->no_invoice);
                        $sheet->setCellValue('L'.$row,$datum->nomor_roll);
                        $sheet->setCellValue('M'.$row,$datum->batch_number);
                        $sheet->setCellValue('N'.$row,$datum->actual_lot);
                        $sheet->setCellValue('O'.$row,$datum->sticker_yards);
                        $sheet->setCellValue('P'.$row,$datum->actual_yards);
                        $sheet->setCellValue('Q'.$row,$datum->a);
                        $sheet->setCellValue('R'.$row,$datum->b);
                        $sheet->setCellValue('S'.$row,$datum->c);
                        $sheet->setCellValue('T'.$row,$datum->d);
                        $sheet->setCellValue('U'.$row,$datum->e);
                        $sheet->setCellValue('V'.$row,$datum->f);
                        $sheet->setCellValue('W'.$row,$datum->g);
                        $sheet->setCellValue('X'.$row,$datum->h);
                        $sheet->setCellValue('Y'.$row,$datum->i);
                        $sheet->setCellValue('Z'.$row,$datum->j);
                        $sheet->setCellValue('AA'.$row,$datum->k);
                        $sheet->setCellValue('AB'.$row,$datum->l);
                        $sheet->setCellValue('AC'.$row,$datum->m);
                        $sheet->setCellValue('AD'.$row,$datum->n);
                        $sheet->setCellValue('AE'.$row,$datum->o);
                        $sheet->setCellValue('AF'.$row,$datum->p);
                        $sheet->setCellValue('AG'.$row,$datum->q);
                        $sheet->setCellValue('AH'.$row,$datum->r);
                        $sheet->setCellValue('AI'.$row,$datum->s);
                        $sheet->setCellValue('AJ'.$row,$datum->t);
                        $sheet->setCellValue('AK'.$row,$datum->linear_point);
                        $sheet->setCellValue('AL'.$row,$datum->sq_yard);
                        $sheet->setCellValue('AM'.$row,$datum->formula_1);
                        $sheet->setCellValue('AN'.$row,$datum->formula_2);
                        $sheet->setCellValue('AO'.$row,$datum->final_result);
                        $sheet->setCellValue('AP'.$row,null);

                        $sheet->cell('B'.$row, function($cell) {
                            $cell->setBackground('#000000');
                            $cell->setFontColor('#ffffff');
                        });
                        $sheet->cell('AO'.$row, function($cell) {
                          $cell->setBackground('#FFFF00');
                        });
                        $sheet->cell('AP'.$row, function($cell) {
                          $cell->setBackground('#FFFF00');
                        });
                        $row++;
                    }
                    
                    
                });
            })
            ->save('xlsx', $location);
            
            $file = $location.'/'.$file_name.'.xlsx';
            Mail::send('fabric_report_material_quality_control.email', ['total_outstanding' => count($data),'data' => $data_limit_100], function ($message) use ($emails,$cc,$file,$file_name)
            {
                $message->subject('OUTSTANDING CONFIRMATION FIR');
                $message->from('no-reply@wms.aoi.co.id', 'WMS');
                $message->cc($cc);
                $message->to($emails);
                $message->attach($file, array('as' => $file_name.'.xlsx', 'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'));
            });

            File::delete($file);
            
        }catch (Exception $e)
        {
            return response (['status' => false,'errors' => $e->getMessage()]);
        }
    }
}
