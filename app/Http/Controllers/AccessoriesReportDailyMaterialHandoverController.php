<?php namespace App\Http\Controllers;

use DB;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class AccessoriesReportDailyMaterialHandoverController extends Controller
{
    public function index()
    {
        return view('accessories_report_daily_material_out.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            /*
                public function monitoringHandover(Request $request, Builder $htmlBuilder){
                $start_date = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date." 00:00:00") : Carbon::today()->subDays(30);
                $end_date = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date." 23:59:59") : Carbon::now();

                    if($request->ajax()){
                        $monitor_handovers = MaterialSubcont::where([
                            ['warehouse_id',auth::user()->warehouse],
                            ['is_active',true]
                        ])
                        ->whereBetween('created_at', [$start_date, $end_date])
                        ->whereNotNull('warehouse_id')
                        ->whereNull('date_receive')
                        ->orderby('created_at','desc');

                        return DataTables::of($monitor_handovers)
                        ->editColumn('qty_upload',function ($monitor_handovers){
                            return number_format($monitor_handovers->qty_upload, 4, '.', ',');
                        })
                        ->editColumn('barcode',function ($monitor_handovers){
                            $user = auth::user();
                            if($user->hasRole('admin-ict-acc'))
                                return $monitor_handovers->barcode;
                            else
                                return null;
                        })
                        ->make(true);
                    }

                    $html = $htmlBuilder
                    ->addColumn(['data'=>'barcode', 'name'=>'barcode', 'title'=>'BARCODE'])
                    ->addColumn(['data'=>'document_no', 'document_no'=>'style', 'title'=>'PO SUPPLIER'])
                    ->addColumn(['data'=>'po_buyer', 'name'=>'po_buyer', 'title'=>'PO BUYER'])
                    ->addColumn(['data'=>'item_code', 'name'=>'item_code', 'title'=>'KODE ITEM'])
                    ->addColumn(['data'=>'item_desc', 'name'=>'item_desc', 'title'=>'DESKRIPSI ITEM'])
                    ->addColumn(['data'=>'category', 'name'=>'category', 'title'=>'KATEGORI ITEM'])
                    ->addColumn(['data'=>'uom', 'name'=>'uom', 'title'=>'UOM'])
                    ->addColumn(['data'=>'qty_upload', 'name'=>'qty_upload', 'title'=>'QTY']);

                    return view('report.monitoring_handover',compact('html'))
                    ->with('start',$start_date->format('d/m/Y'))
                    ->with('end',$end_date->format('d/m/Y'));

                }


            */
        }
    }

    public function export(Request $request)
    {
        $filename   = 'report_material_receivement';
        $file       = Config::get('storage.report') . '/' . e($filename).'.csv';
        
        if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';

        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }
}
