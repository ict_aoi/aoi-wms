<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;

use App\Models\User;
use App\Models\Item;
use App\Models\PoBuyer;
use App\Models\Supplier;
use App\Models\PoSupplier;
use App\Models\PoBuyerLog;
use App\Models\ItemPurchase;
use App\Models\PurchaseItem;
use App\Models\MappingStocks;
use App\Models\AutoAlocation;
use App\Models\UomConversion;
use App\Models\ReroutePoBuyer;
use App\Models\AutoAllocation;
use App\Models\AllocationItem;
use App\Models\MaterialRollHandoverFabric;
use App\Models\MaterialArrival;
use App\Models\OrderPreferance;
use App\Models\SummaryStockFabric;
use App\Models\MaterialStock;
use App\Models\MaterialPreparation;
use App\Models\DetailMaterialPreparationFabric;
use App\Models\MaterialCheck;
use App\Models\SummaryHandoverMaterial;
use App\Models\MonitoringReceivingFabric;
use App\Models\PurchaseOrderDetail;
use App\Models\MaterialPreparationFabric;
use App\Models\MaterialRequirement;

class ERPController extends Controller
{
    static function uomCronInsert()
    {
        $data = DB::connection('erp')
        ->table('adt_uom_conv')
        ->get();
        
        try
        {
            db::beginTransaction();

            foreach ($data as $key => $value) 
            {
                $is_exists = UomConversion::where([
                    ['item_id',$value->m_product_id],
                    [db::raw('upper(uom_from)'),$value->uom_to],
                    [db::raw('upper(uom_to)'),$value->uomsymbol],
                ])
                ->first();

                if($is_exists)
                {
                    $is_exists->item_code       = $value->product;
                    $is_exists->item_desc       = strtoupper($value->product_name);
                    $is_exists->category        = strtoupper($value->value);
                    $is_exists->uom_from        = strtoupper($value->uom_to);
                    $is_exists->uom_to          = strtoupper($value->uomsymbol);
                    $is_exists->dividerate      = $value->dividerate;
                    $is_exists->multiplyrate    = $value->multiplyrate;
                    $is_exists->save();
                }else
                {
                    UomConversion::firstorCreate([
                        'item_id'       => $value->m_product_id,
                        'item_code'     => $value->product,
                        'item_desc'     => $value->product_name,
                        'category'      => $value->value,
                        'uom_from'      => $value->uom_to,
                        'uom_to'        => $value->uomsymbol,
                        'dividerate'    => $value->dividerate,
                        'multiplyrate'  => $value->multiplyrate,
                    ]);
                }
                
            }

            db::commit();
        }catch (Exception $ex){
            db::rollback();
            $message = $ex->getMessage();
        }
          
    }

    static function supplierCronInsert()
    {
        $data = DB::connection('erp')
        ->table('c_bpartner')
        ->where([
            ['isvendor','Y'],
            ['isactive','Y'],
            ['value','not like',"%IM%"]
        ])
        ->get();
        
        try
        {
            db::beginTransaction();
            
            foreach ($data as $key => $value) 
            {
                $_type      = $value->c_bp_group_id;
                $is_exists  = Supplier::where('c_bpartner_id',$value->c_bpartner_id)->first();
                
                if($_type == '1000003') $type = 'LOCAL';
                else $type = 'IMPORT';

                
                if($is_exists)
                {
                    $is_exists->supplier_code = trim(strtoupper($value->value));
                    $is_exists->supplier_name = trim(strtoupper($value->name));
                    $is_exists->type_supplier = $type;
                    $is_exists->save();
                }else
                {
                    Supplier::firstorCreate([
                        'supplier_code' => trim(strtoupper($value->value)),
                        'supplier_name' => trim(strtoupper($value->name)),
                        'c_bpartner_id' => trim(strtoupper($value->c_bpartner_id)),
                        'type_supplier' => $type
                    ]);
                }
                
            }

            db::commit();
        }catch (Exception $ex){
            db::rollback();
            $message = $ex->getMessage();
        }
    }

    static function poSupplierCronInsert()
    {
        $max_date = carbon::now()->format('ym');
        
        $data = DB::connection('erp')
        ->table('wms_po_supplier')
        ->where('period_po',$max_date)
        ->get();
        
        $po_buyers      = array();
        $array          = array();
        $concatenate    = '';

        try
        {
            db::beginTransaction();

            foreach ($data as $key => $value) 
            {
                $c_order_id     = $value->c_order_id;
                $c_bpartner_id  = $value->c_bpartner_id;
                $created_at     = $value->created_at;
                $document_no    = trim(strtoupper($value->documentno));
                $period_po      = trim(strtoupper($value->period_po));

                if($document_no == 'FREE STOCK')
                {
                    $c_bpartner_id          = 'FREE STOCK';
                    $supplier_code          = 'FREE STOCK';
                    $_mapping_stock         = MappingStocks::where('document_number',$document_no)->first();
                    $type_stock_erp_code    = $_mapping_stock->type_stock_erp_code;
                    $type_stock             = $_mapping_stock->type_stock;
                    
                }else
                {
                    $get_4_digit_from_beginning = substr($document_no,0, 4);
                    $_mapping_stock_4_digt      = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                    if($_mapping_stock_4_digt)
                    {
                        $type_stock_erp_code    = $_mapping_stock_4_digt->type_stock_erp_code;
                        $type_stock             = $_mapping_stock_4_digt->type_stock;
                    }else
                    {
                        $get_5_digit_from_beginning = substr($document_no,0, 5);
                        $_mapping_stock_5_digt      = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                        if($_mapping_stock_5_digt)
                        {
                            $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                            $type_stock             = $_mapping_stock_5_digt->type_stock;
                        }else
                        {
                            $type_stock_erp_code    = null;
                            $type_stock             = null;
                        }
                        
                    }
                } 

                $is_exists  = PoSupplier::where('c_order_id',$c_order_id)->first();
                $supplier   = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();

                if(!$is_exists)
                {
                    $po_supplier = PoSupplier::create([
                        'c_order_id'            => $c_order_id,
                        'c_bpartner_id'         => $c_bpartner_id,
                        'supplier_id'           => ($supplier) ? $supplier->id : null,
                        'integration_date'      => $created_at,
                        'type_stock_erp_code'   => $type_stock_erp_code,
                        'type_stock'            => $type_stock,
                        'document_no'           => $document_no,
                        'periode_po'            => $period_po
                    ]);
                    $po_supplier_id = $po_supplier->id;
                }else
                {
                    $is_exists->c_bpartner_id   = trim(strtoupper($value->c_bpartner_id));
                    $is_exists->supplier_id     = ($supplier) ? $supplier->id : null;
                    $is_exists->save();
                    $po_supplier_id             = $is_exists->id;
                }

                // INSERT ITEMS
                $items = DB::connection('erp')
                ->table('wms_po_per_item')
                ->where('documentno',$document_no)
                ->get();
                
                foreach ($items as $key => $item) 
                {
                    $item_code = trim(strtoupper($item->item));
                    $is_exists = ItemPurchase::where([
                        ['document_no',$document_no],
                        ['item_code',$item_code],
                    ])
                    ->exists();

                    if(!$is_exists)
                    {
                        ItemPurchase::FirstorCreate([
                            'po_supplier_id'    => $po_supplier_id,
                            'document_no'       => $document_no,
                            'item_code'         => $item_code,
                        ]);
                    }   
                    
                }
            }

            db::commit();
        }catch (Exception $ex){
            db::rollback();
            $message = $ex->getMessage();
        }
    }

    static function itemCronInsert()
    {
        $data = DB::connection('erp')
        ->table('wms_master_items')
        ->get();

        try 
        {
            DB::beginTransaction();

            foreach ($data as $key => $value) 
            {
                $item_id          = $value->item_id;
                $item_code        = trim(strtoupper($value->item_code));
                $item_desc        = trim(strtoupper($value->item_desc));
                $color            = trim(strtoupper($value->color));
                $category         = trim(strtoupper($value->category));
                $uom              = trim(strtoupper($value->uom));
                $upc              = trim(strtoupper($value->upc));
                $composition      = trim(strtoupper($value->description));
                $category_name    = trim(strtoupper($value->category_name));
                $width            = trim(strtoupper($value->width));

                $is_exits = Item::where('item_id',$item_id)->first();

                if($is_exits)
                {
                    $is_exits->item_code        = $item_code;
                    $is_exits->item_desc        = $item_desc;
                    $is_exits->color            = $color;
                    $is_exits->category         = $category;
                    $is_exits->uom              = $uom;
                    $is_exits->upc              = $upc;
                    $is_exits->composition      = $composition;
                    $is_exits->category_name    = $category_name;
                    $is_exits->width            = $width;
                    $is_exits->save();
                }else
                {
                    Item::firstorCreate([
                        'item_id'           => $item_id,
                        'item_code'         => $item_code,
                        'item_desc'         => $item_desc,
                        'category'          => $category,
                        'uom'               => $uom,
                        'color'             => $color,
                        'upc'               => $upc,
                        'composition'       => $composition,
                        'category_name'     => $category_name,
                        'width'             => $width
                    ]);
                }
            }

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }


        $data_not_active = DB::select(db::raw("select *
            from items
            where item_id not in (
                select item_id from master_item_erp_v
            );"
        ));

        try 
        {
            DB::beginTransaction();

            foreach ($data_not_active as $key => $value) 
            {
                $item_old           = Item::find($value->id);
                $item_code          = trim(strtoupper($value->item_code));
                
                $material_item_erp  =  DB::connection('erp')
                ->table('wms_master_items')
                ->where('item_code',$item_code)
                ->first();

                if($material_item_erp)
                {
                    $item_id          = $material_item_erp->item_id;
                    $item_code        = trim(strtoupper($material_item_erp->item_code));
                    $item_desc        = trim(strtoupper($material_item_erp->item_desc));
                    $color            = trim(strtoupper($material_item_erp->color));
                    $category         = trim(strtoupper($material_item_erp->category));
                    $uom              = trim(strtoupper($material_item_erp->uom));
                    $upc              = trim(strtoupper($material_item_erp->upc));
                    $composition      = trim(strtoupper($material_item_erp->description));
                    $category_name    = trim(strtoupper($material_item_erp->category_name));
                    $width            = trim(strtoupper($material_item_erp->width));


                    $is_exits = Item::where([
                        ['item_id',$item_id],
                        ['item_id','!=',$item_old->item_id],
                    ])
                    ->first();

                    if($is_exits)
                    {
                        $_item_id   = $is_exits->item_id;
                        $_item_code = $is_exits->item_code;
                        $_item_desc = $is_exits->item_desc;
                        $_color     = $is_exits->color;
                        
                        $item_old->delete();
                    }else
                    {
                        $new_item = Item::firstorCreate([
                            'item_id'           => $item_id,
                            'item_code'         => $item_code,
                            'item_desc'         => $item_desc,
                            'category'          => $category,
                            'uom'               => $uom,
                            'color'             => $color,
                            'upc'               => $upc,
                            'composition'       => $composition,
                            'category_name'     => $category_name,
                            'width'             => $width
                        ]);

                        $_item_id   = $new_item->item_id;
                        $_item_code = $new_item->item_code;
                        $_item_desc = $new_item->item_desc;
                        $_color     = $new_item->color;
                    }



                    MaterialPreparation::where(db::raw('upper(item_code)'),$_item_code)
                    ->update([
                        'item_id' => $_item_id,
                        'item_desc' => $_item_desc
                    ]);
                    
                    MaterialRequirement::where(db::raw('upper(item_code)'),$_item_code)
                    ->update([
                        'item_id' => $_item_id,
                        'item_desc' => $_item_desc
                    ]);
                    
                    SummaryStockFabric::where(db::raw('upper(item_code)'),$_item_code)
                    ->update([
                            'item_id' => $_item_id,
                            'color'   => $_color,
                    ]);

                    $auto_allocations = AutoAllocation::where(db::raw('upper(item_code)'),$_item_code)->get();
                    foreach ($auto_allocations as $key => $auto_allocation) 
                    {
                        $article_no = $auto_allocation->article_no;
                        if($article_no) $item_code = $_item_code.'|'.$article_no;
                        else $item_code = $_item_code;

                        $auto_allocation->item_code         = $item_code;
                        $auto_allocation->item_code_book    = $_item_code;
                        $auto_allocation->item_id_book      = $_item_id;
                        $auto_allocation->save();
                    }

                    AutoAllocation::where(db::raw('upper(item_code_source)'),$_item_code)
                    ->update([
                            'item_id_source' => $_item_id,
                    ]);

                    MaterialArrival::where(db::raw('upper(item_code)'),$_item_code)
                    ->update([
                            'item_id'   => $_item_id,
                            'item_desc' => $_item_code
                    ]);

                    MaterialStock::where(db::raw('upper(item_code)'),$_item_code)
                    ->update([
                            'color'     => $_color,
                            'item_desc' => $_item_desc,
                            'item_id'   => $_item_id,
                    ]);

                    MaterialPreparationFabric::where(db::raw('upper(item_code)'),$_item_code)
                    ->update([
                            'item_id_book' => $_item_id,
                    ]);

                    MaterialPreparationFabric::where(db::raw('upper(item_code_source)'),$_item_code)
                    ->update([
                            'item_id_source' => $_item_id,
                    ]);
                    
                    DetailMaterialPreparationFabric::whereIn('material_stock_id',function($query) use($_item_code,$_color,$_item_id)
                    {
                            $query->select('id')
                            ->from('material_stocks')
                            ->where(db::raw('upper(item_code)'),$_item_code);
                    })
                        ->update([
                            'color'     => $_color,
                            'item_code' => $_item_code,
                            'item_id'   => $_item_id,
                        ]);

                    MaterialCheck::whereNotNull('material_preparation_id')
                    ->whereIn('material_preparation_id',function($query) use($_item_code,$_color,$_item_id,$_item_desc)
                    {
                            $query->select('id')
                            ->from('material_preparations')
                            ->where(db::raw('upper(item_code)'),$_item_code);
                    })
                    ->update([
                            'color'     => $_color,
                            'item_desc' => $_item_desc,
                            //'item_id'   => $_item_id,
                    ]);

                    MaterialCheck::whereNotNull('material_stock_id')
                    ->whereIn('material_stock_id',function($query)  use($_item_code,$_color,$_item_id,$_item_desc)
                    {
                            $query->select('id')
                            ->from('material_stocks')
                            ->where(db::raw('upper(item_code)'),$_item_code);
                    })
                    ->update([
                            'color'     => $_color,
                            'item_desc' => $_item_desc,
                            //'item_id'   => $_item_id,
                    ]);

                    MonitoringReceivingFabric::where(db::raw('upper(item_code)'),$_item_code)
                    ->update([
                            'color'         => $_color,
                            'item_id'       => $_item_id
                    ]);

                    SummaryHandoverMaterial::where(db::raw('upper(item_code)'),$_item_code)
                    ->update([
                            'item_id'     => $_item_id
                    ]);

                    MaterialRollHandoverFabric::where(db::raw('upper(item_code)'),$_item_code)
                    ->update([
                            'color'         => $_color,
                            'item_id'     => $_item_id
                    ]);

                }else
                {
                    $item_old->delete();
                }
               
                
                
            }

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function dailyPurchaseItems()
    {
        $movement_date  = carbon::now();
        $udpateErp = array();
       
        try 
        {
            DB::beginTransaction();

            $purchase_items = DB::connection('erp') //dev_erp //erp
            ->table('rma_wms_planning_alokasi') // rma_wms_planning_v2_cron -->viewnya
            ->where([
                ['isintegrate',false],
                ['status_lc','PR'],
                ['is_additional', 'f'],
            ])
            ->get();
            // dd($purchase_items);

           
            foreach ($purchase_items as $key => $purchase_item)
            {
                
                $c_order_id        = $purchase_item->c_order_id;
                $item_id           = $purchase_item->item_id;
                $c_bpartner_id     = $purchase_item->c_bpartner_id;
                $supplier_name     = strtoupper($purchase_item->supplier_name);
                $item_code         = strtoupper($purchase_item->item_code);
                $document_no       = strtoupper($purchase_item->document_no);
                $po_sample         = strpos($purchase_item->po_buyer, '-S');
                // $po_buyers         = PoBuyer::where('po_buyer', $purchase_item->po_buyer)->where('brand', 'ADIDAS')->exists();
                // if($po_buyers == true)
                // {
                    $purchase_po_buyer = trim(str_replace('-S', '',$purchase_item->po_buyer));
                // }
                // {
                //     $purchase_po_buyer = $purchase_item->po_buyer;
                // }
                $warehouse_id      = $purchase_item->m_warehouse_id;
                $uom               = $purchase_item->uom;
                $category          = $purchase_item->category;
                $qty               = sprintf('%0.8f',$purchase_item->qty);
                $erp_allocation_id = $purchase_item->uuid;
                $is_fabric         = $purchase_item->isfabric;
                if($purchase_item->is_additional == 'f')
                {
                    $is_additional = false;
                }
                else
                {
                    $is_additional = true;
                }
                
                if($warehouse_id == '1000011') $warehouse_name = 'FABRIC AOI 2';
                else if($warehouse_id == '1000001') $warehouse_name = 'FABRIC AOI 1';
                else if($warehouse_id == '1000013') $warehouse_name = 'ACCESSORIES AOI 2';
                else if($warehouse_id == '1000002') $warehouse_name = 'ACCESSORIES AOI 1';

                $reroute            = ReroutePoBuyer::where('old_po_buyer',$purchase_po_buyer)->first();
                if($reroute)
                {
                    $new_po_buyer = $reroute->new_po_buyer;
                    $old_po_buyer = $reroute->old_po_buyer;
                }else
                {
                    $new_po_buyer = $purchase_po_buyer;
                    $old_po_buyer = $purchase_po_buyer;
                }

                $__po_buyer = PoBuyer::where('po_buyer',$new_po_buyer)->first();
                

                if($__po_buyer)
                {
                    $lc_date                    = $__po_buyer->lc_date;
                    $promise_date               = ($__po_buyer->statistical_date ? $__po_buyer->statistical_date : $__po_buyer->promise_date);
                    $season                     = $__po_buyer->season;
                    $type_stock_erp_code        = $__po_buyer->type_stock_erp_code;
                    $cancel_date                = $__po_buyer->cancel_date;

                    if($is_fabric == 'N')
                    {
                        $tgl_date                   = ($__po_buyer->lc_date ? $__po_buyer->lc_date->format('d') : 'LC NOT FOUND');
                        $month_lc                   = ($__po_buyer->lc_date ? $__po_buyer->lc_date->format('my')  : 'LC NOT FOUND');

                        if($warehouse_id == '1000002') $warehouse_sequnce = '001';
                        else if($warehouse_id == '1000013') $warehouse_sequnce = '002';
                    
                        $document_allocation_number = 'PRLC'.$tgl_date.'-'.$month_lc.'-'.$warehouse_sequnce;
                    }
                    
                }else
                {
                    $promise_date               = null;
                    $lc_date                    = null;
                    $season                     = null;
                    $type_stock_erp_code        = null;
                    $cancel_date                = null;
                    $document_allocation_number = null;
                }
                
                if($type_stock_erp_code == 1) $type_stock       = 'SLT';
                else if($type_stock_erp_code == 2) $type_stock  = 'REGULER';
                else if($type_stock_erp_code == 3) $type_stock  = 'PR/SR';
                else if($type_stock_erp_code == 4) $type_stock  = 'MTFC';
                else if($type_stock_erp_code == 5) $type_stock  = 'NB';
                else $type_stock                                = null;

                
                if($cancel_date != null) $status_po_buyer = 'cancel';
                else $status_po_buyer                     = 'active';
                
                $system             = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();
    
                $item               = Item::where('item_id',$item_id)->first();
                $item_desc          = ($item)? strtoupper($item->item_desc): 'data not found';
                $is_exists = null;
                
                if($lc_date >= '2019-08-15' && $category == 'FB')// cut off mulai lc tanggal ini, user baru setuju mau maintain
                {
                    
                        $is_exists = AutoAllocation::whereNotNull('erp_allocation_id')
                        ->where([
                            ['c_order_id',$c_order_id],
                            ['c_bpartner_id',$c_bpartner_id],
                            ['item_id_book',$item_id],
                            ['item_id_source',$item_id],
                            ['po_buyer',$new_po_buyer],
                            ['erp_allocation_id',$erp_allocation_id],
                            ['warehouse_id',$warehouse_id],
                            ['is_fabric',true],
                        ])
                        ->first();

                        if(!$is_exists)
                        {
                            $newAllocation = AutoAllocation::create([
                                'type_stock_erp_code'               => $type_stock_erp_code,
                                'type_stock'                        => $type_stock,
                                'lc_date'                           => $lc_date,
                                'promise_date'                      => $promise_date,
                                'season'                            => $season,
                                'document_no'                       => $document_no,
                                'c_bpartner_id'                     => $c_bpartner_id,
                                'supplier_name'                     => $supplier_name,
                                'po_buyer'                          => $new_po_buyer,
                                'c_order_id'                        => $c_order_id,
                                'item_id_book'                      => $item_id,
                                'item_id_source'                    => $item_id,
                                'old_po_buyer'                      => $old_po_buyer,
                                'item_code_source'                  => $item_code,
                                'item_code_book'                    => $item_code,
                                'item_code'                         => $item_code,
                                'item_desc'                         => $item_desc,
                                'category'                          => $category,
                                'uom'                               => $uom,
                                'warehouse_name'                    => $warehouse_name,
                                'warehouse_id'                      => $warehouse_id,
                                'qty_allocation'                    => $qty,
                                'qty_outstanding'                   => $qty,
                                'qty_allocated'                     => 0,
                                'status_po_buyer'                   => $status_po_buyer,
                                'is_fabric'                         => true,
                                'is_already_generate_form_booking'  => false,
                                'is_upload_manual'                  => false,
                                'is_allocation_purchase'            => true,
                                'user_id'                           => $system->id,
                                'created_at'                        => $movement_date,
                                'updated_at'                        => $movement_date,
                                'erp_allocation_id'                 => $erp_allocation_id,
                                'is_additional'                     => $is_additional,
                            ]);

                            $udpateErp [] = $newAllocation->erp_allocation_id;   
                        }
                        else
                        {
                            $system = User::where([
                                ['name','system'],
                                ['warehouse',$warehouse_id]
                            ])
                            ->first();
                            $old_qty_allocation  = sprintf('%0.8f',$is_exists->qty_allocation);
                            $old_qty_outstanding = sprintf('%0.8f',$is_exists->qty_outstanding);
                            $remark              = $is_exists->remark;
        
                            if($old_qty_allocation/$qty == 1)
                            {
                                $is_exists->erp_allocation_id = $erp_allocation_id;
                                $is_exists->save();
                            }
                            else
                            {
                                $is_exists->qty_allocation    = $old_qty_allocation + $qty;
                                $is_exists->qty_outstanding   = $old_qty_outstanding + $qty;
                                $is_exists->erp_allocation_id = $erp_allocation_id;
                                $is_exists->remark            = $remark. 'PENAMBAHAN QTY ALLOCATION SEBESAR '.$qty;
                                $is_exists->user_id           = $system->id;
                                $is_exists->save();


                            }
                            $udpateErp [] = $erp_allocation_id;      
                    
                    }
                }
                else if($category != 'FB' && ($new_po_buyer != null || $new_po_buyer != ''))
                {
                    $is_exists = AutoAllocation::whereNotNull('erp_allocation_id')
                    ->where([
                        ['c_order_id',$c_order_id],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['item_id_book',$item_id],
                        ['item_id_source',$item_id],
                        ['po_buyer',$new_po_buyer],
                        // ['erp_allocation_id',$erp_allocation_id],
                        ['warehouse_id',$warehouse_id],
                        ['is_fabric',false],
                    ])
                    ->whereNull('deleted_at')
                    ->first();

                    if(!$is_exists)
                    {
                        $newAllocation = AutoAllocation::create([
                            'document_allocation_number'        => $document_allocation_number,
                            'type_stock_erp_code'               => $type_stock_erp_code,
                            'type_stock'                        => $type_stock,
                            'lc_date'                           => $lc_date,
                            'promise_date'                      => $promise_date,
                            'season'                            => $season,
                            'c_order_id'                        => $c_order_id,
                            'document_no'                       => $document_no,
                            'c_bpartner_id'                     => $c_bpartner_id,
                            'supplier_name'                     => $supplier_name,
                            'po_buyer'                          => $new_po_buyer,
                            'old_po_buyer'                      => $old_po_buyer,
                            'item_code_source'                  => $item_code,
                            'item_code_book'                    => $item_code,
                            'item_id_book'                      => $item_id,
                            'item_id_source'                    => $item_id,
                            'item_code'                         => $item_code,
                            'item_desc'                         => $item_desc,
                            'category'                          => $category,
                            'uom'                               => $uom,
                            'warehouse_name'                    => $warehouse_name,
                            'warehouse_id'                      => $warehouse_id,
                            'qty_allocation'                    => $qty,
                            'qty_outstanding'                   => $qty,
                            'qty_allocated'                     => 0,
                            'status_po_buyer'                   => $status_po_buyer,
                            'is_fabric'                         => false,
                            'is_already_generate_form_booking'  => false,
                            'is_upload_manual'                  => false,
                            'is_allocation_purchase'            => true,
                            'created_at'                        => $movement_date,
                            'updated_at'                        => $movement_date,
                            'user_id'                           => $system->id,
                            'erp_allocation_id'                 => $erp_allocation_id,
                            'is_additional'                     => $is_additional,
                        ]);

                        $udpateErp [] = $newAllocation->erp_allocation_id;
                    }else
                    {
                            $system = User::where([
                                ['name','system'],
                                ['warehouse',$warehouse_id]
                            ])
                            ->first();
                            $old_qty_allocation  = sprintf('%0.8f',$is_exists->qty_allocation);
                            $old_qty_outstanding = sprintf('%0.8f',$is_exists->qty_outstanding);
                            $remark              = $is_exists->remark;
        
                            if($old_qty_allocation/$qty == 1)
                            {
                                $is_exists->erp_allocation_id = $erp_allocation_id;
                                $is_exists->save();
                            }
                            else
                            {
                                $is_exists->qty_allocation    = $old_qty_allocation + $qty;
                                $is_exists->qty_outstanding   = $old_qty_outstanding + $qty;
                                $is_exists->erp_allocation_id = $erp_allocation_id;
                                $is_exists->remark            = $remark. 'PENAMBAHAN QTY ALLOCATION SEBESAR '.$qty;
                                $is_exists->user_id           = $system->id;
                                $is_exists->save();
                            }

                            $udpateErp [] = $erp_allocation_id;

                    }
                    
                }

                DB::connection('erp') //dev_erp //erp
                ->table('rma_wms_planning_alokasi')
                ->whereIn('uuid',$udpateErp)
                ->update([
                    'isintegrate' => true,
                    'updated'     => $movement_date,
                    'source'    => 'test'
                ]);

                DB::commit();
                
            }

           
           

        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        $checkAllocations =  DB::table('auto_allocations') //double check alokasi yg dimasukin pake erp_allocation_id
        ->whereIn('erp_allocation_id', $udpateErp)
        ->get();

        $groupByErpId  =  $checkAllocations->groupBy('erp_allocation_id'); //gatau knp kalo di grouping diatas tetep double, jadi tak grouping disini

        foreach ($groupByErpId as $key => $value){

        if(count($value) >1) {

        $id = $value[0]->id;
        $t = $value->where('id','!=', $id)->pluck('id');
        AutoAllocation::whereIn('id', $t)->delete();

        }

        }
       
    }


    // static function dailyPurchaseItems()
    // {
    //     $movement_date  = carbon::now();
     
        
    //     try 
    //     {
    //         DB::beginTransaction();

    //         $purchase_items = DB::connection('erp') //dev_erp //erp
    //         ->table('rma_wms_planning_alokasi') // rma_wms_planning_v2_cron -->viewnya
    //         ->where([
    //             ['isintegrate',false],
    //             ['status_lc','PR'],
    //             ['is_additional', 'f'],
    //         ])->whereIn('document_no', $_document_no)
    //         ->get();

    //         $udpateErp = array();
    //         foreach ($purchase_items as $key => $purchase_item)
    //         {
    //             $c_order_id        = $purchase_item->c_order_id;
    //             $item_id           = $purchase_item->item_id;
    //             $c_bpartner_id     = $purchase_item->c_bpartner_id;
    //             $supplier_name     = strtoupper($purchase_item->supplier_name);
    //             $item_code         = strtoupper($purchase_item->item_code);
    //             $document_no       = strtoupper($purchase_item->document_no);
    //             $po_sample         = strpos($purchase_item->po_buyer, '-S');
    //             // $po_buyers         = PoBuyer::where('po_buyer', $purchase_item->po_buyer)->where('brand', 'ADIDAS')->exists();
    //             // if($po_buyers == true)
    //             // {
    //                 $purchase_po_buyer = trim(str_replace('-S', '',$purchase_item->po_buyer));
    //             // }
    //             // {
    //             //     $purchase_po_buyer = $purchase_item->po_buyer;
    //             // }
    //             $warehouse_id      = $purchase_item->m_warehouse_id;
    //             $uom               = $purchase_item->uom;
    //             $category          = $purchase_item->category;
    //             $qty               = sprintf('%0.8f',$purchase_item->qty);
    //             $erp_allocation_id = $purchase_item->uuid;
    //             $is_fabric         = $purchase_item->isfabric;
    //             if($purchase_item->is_additional == 'f')
    //             {
    //                 $is_additional = false;
    //             }
    //             else
    //             {
    //                 $is_additional = true;
    //             }
                
    //             if($warehouse_id == '1000011') $warehouse_name = 'FABRIC AOI 2';
    //             else if($warehouse_id == '1000001') $warehouse_name = 'FABRIC AOI 1';
    //             else if($warehouse_id == '1000013') $warehouse_name = 'ACCESSORIES AOI 2';
    //             else if($warehouse_id == '1000002') $warehouse_name = 'ACCESSORIES AOI 1';

    //             $reroute            = ReroutePoBuyer::where('old_po_buyer',$purchase_po_buyer)->first();
    //             if($reroute)
    //             {
    //                 $new_po_buyer = $reroute->new_po_buyer;
    //                 $old_po_buyer = $reroute->old_po_buyer;
    //             }else
    //             {
    //                 $new_po_buyer = $purchase_po_buyer;
    //                 $old_po_buyer = $purchase_po_buyer;
    //             }

    //             $__po_buyer = PoBuyer::where('po_buyer',$new_po_buyer)->first();
                

    //             if($__po_buyer)
    //             {
    //                 $lc_date                    = $__po_buyer->lc_date;
    //                 $promise_date               = ($__po_buyer->statistical_date ? $__po_buyer->statistical_date : $__po_buyer->promise_date);
    //                 $season                     = $__po_buyer->season;
    //                 $type_stock_erp_code        = $__po_buyer->type_stock_erp_code;
    //                 $cancel_date                = $__po_buyer->cancel_date;

    //                 if($is_fabric == 'N')
    //                 {
    //                     $tgl_date                   = ($__po_buyer->lc_date ? $__po_buyer->lc_date->format('d') : 'LC NOT FOUND');
    //                     $month_lc                   = ($__po_buyer->lc_date ? $__po_buyer->lc_date->format('my')  : 'LC NOT FOUND');

    //                     if($warehouse_id == '1000002') $warehouse_sequnce = '001';
    //                     else if($warehouse_id == '1000013') $warehouse_sequnce = '002';
                    
    //                     $document_allocation_number = 'PRLC'.$tgl_date.'-'.$month_lc.'-'.$warehouse_sequnce;
    //                 }
                    
    //             }else
    //             {
    //                 $promise_date               = null;
    //                 $lc_date                    = null;
    //                 $season                     = null;
    //                 $type_stock_erp_code        = null;
    //                 $cancel_date                = null;
    //                 $document_allocation_number = null;
    //             }
                
    //             if($type_stock_erp_code == 1) $type_stock       = 'SLT';
    //             else if($type_stock_erp_code == 2) $type_stock  = 'REGULER';
    //             else if($type_stock_erp_code == 3) $type_stock  = 'PR/SR';
    //             else if($type_stock_erp_code == 4) $type_stock  = 'MTFC';
    //             else if($type_stock_erp_code == 5) $type_stock  = 'NB';
    //             else $type_stock                                = null;

                
    //             if($cancel_date != null) $status_po_buyer = 'cancel';
    //             else $status_po_buyer                     = 'active';
                
    //             $system             = User::where([
    //                 ['name','system'],
    //                 ['warehouse',$warehouse_id]
    //             ])
    //             ->first();
    
    //             $item               = Item::where('item_id',$item_id)->first();
    //             $item_desc          = ($item)? strtoupper($item->item_desc): 'data not found';
    //             $is_exists = null;
                
    //             if($lc_date >= '2019-08-15' && $category == 'FB')// cut off mulai lc tanggal ini, user baru setuju mau maintain
    //             {
                    
    //                     $is_exists = AutoAllocation::whereNotNull('erp_allocation_id')
    //                     ->where([
    //                         ['c_order_id',$c_order_id],
    //                         ['c_bpartner_id',$c_bpartner_id],
    //                         ['item_id_book',$item_id],
    //                         ['item_id_source',$item_id],
    //                         ['po_buyer',$new_po_buyer],
    //                         ['erp_allocation_id',$erp_allocation_id],
    //                         ['warehouse_id',$warehouse_id],
    //                         ['is_fabric',true],
    //                     ])
    //                     ->first();

    //                     if(!$is_exists)
    //                     {
    //                         $newAllocation = AutoAllocation::create([
    //                             'type_stock_erp_code'               => $type_stock_erp_code,
    //                             'type_stock'                        => $type_stock,
    //                             'lc_date'                           => $lc_date,
    //                             'promise_date'                      => $promise_date,
    //                             'season'                            => $season,
    //                             'document_no'                       => $document_no,
    //                             'c_bpartner_id'                     => $c_bpartner_id,
    //                             'supplier_name'                     => $supplier_name,
    //                             'po_buyer'                          => $new_po_buyer,
    //                             'c_order_id'                        => $c_order_id,
    //                             'item_id_book'                      => $item_id,
    //                             'item_id_source'                    => $item_id,
    //                             'old_po_buyer'                      => $old_po_buyer,
    //                             'item_code_source'                  => $item_code,
    //                             'item_code_book'                    => $item_code,
    //                             'item_code'                         => $item_code,
    //                             'item_desc'                         => $item_desc,
    //                             'category'                          => $category,
    //                             'uom'                               => $uom,
    //                             'warehouse_name'                    => $warehouse_name,
    //                             'warehouse_id'                      => $warehouse_id,
    //                             'qty_allocation'                    => $qty,
    //                             'qty_outstanding'                   => $qty,
    //                             'qty_allocated'                     => 0,
    //                             'status_po_buyer'                   => $status_po_buyer,
    //                             'is_fabric'                         => true,
    //                             'is_already_generate_form_booking'  => false,
    //                             'is_upload_manual'                  => false,
    //                             'is_allocation_purchase'            => true,
    //                             'user_id'                           => $system->id,
    //                             'created_at'                        => $movement_date,
    //                             'updated_at'                        => $movement_date,
    //                             'erp_allocation_id'                 => $erp_allocation_id,
    //                             'is_additional'                     => $is_additional,
    //                         ]);

    //                         $udpateErp [] = $newAllocation->erp_allocation_id;   
    //                     }
    //                     else
    //                     {
    //                         $system = User::where([
    //                             ['name','system'],
    //                             ['warehouse',$warehouse_id]
    //                         ])
    //                         ->first();
    //                         $old_qty_allocation  = sprintf('%0.8f',$is_exists->qty_allocation);
    //                         $old_qty_outstanding = sprintf('%0.8f',$is_exists->qty_outstanding);
    //                         $remark              = $is_exists->remark;
        
    //                         if($old_qty_allocation/$qty == 1)
    //                         {
    //                             $is_exists->erp_allocation_id = $erp_allocation_id;
    //                             $is_exists->save();
    //                         }
    //                         else
    //                         {
    //                             $is_exists->qty_allocation    = $old_qty_allocation + $qty;
    //                             $is_exists->qty_outstanding   = $old_qty_outstanding + $qty;
    //                             $is_exists->erp_allocation_id = $erp_allocation_id;
    //                             $is_exists->remark            = $remark. 'PENAMBAHAN QTY ALLOCATION SEBESAR '.$qty;
    //                             $is_exists->user_id           = $system->id;
    //                             $is_exists->save();


    //                         }
    //                         $udpateErp [] = $erp_allocation_id;      
                    
    //                 }
    //             }
    //             else if($category != 'FB' && ($new_po_buyer != null || $new_po_buyer != ''))
    //             {
    //                 $is_exists = AutoAllocation::whereNotNull('erp_allocation_id')
    //                 ->where([
    //                     ['c_order_id',$c_order_id],
    //                     ['c_bpartner_id',$c_bpartner_id],
    //                     ['item_id_book',$item_id],
    //                     ['item_id_source',$item_id],
    //                     ['po_buyer',$new_po_buyer],
    //                     // ['erp_allocation_id',$erp_allocation_id],
    //                     ['warehouse_id',$warehouse_id],
    //                     ['is_fabric',false],
    //                 ])
    //                 ->first();

    //                 if(!$is_exists)
    //                 {
    //                     $newAllocation = AutoAllocation::create([
    //                         'document_allocation_number'        => $document_allocation_number,
    //                         'type_stock_erp_code'               => $type_stock_erp_code,
    //                         'type_stock'                        => $type_stock,
    //                         'lc_date'                           => $lc_date,
    //                         'promise_date'                      => $promise_date,
    //                         'season'                            => $season,
    //                         'c_order_id'                        => $c_order_id,
    //                         'document_no'                       => $document_no,
    //                         'c_bpartner_id'                     => $c_bpartner_id,
    //                         'supplier_name'                     => $supplier_name,
    //                         'po_buyer'                          => $new_po_buyer,
    //                         'old_po_buyer'                      => $old_po_buyer,
    //                         'item_code_source'                  => $item_code,
    //                         'item_code_book'                    => $item_code,
    //                         'item_id_book'                      => $item_id,
    //                         'item_id_source'                    => $item_id,
    //                         'item_code'                         => $item_code,
    //                         'item_desc'                         => $item_desc,
    //                         'category'                          => $category,
    //                         'uom'                               => $uom,
    //                         'warehouse_name'                    => $warehouse_name,
    //                         'warehouse_id'                      => $warehouse_id,
    //                         'qty_allocation'                    => $qty,
    //                         'qty_outstanding'                   => $qty,
    //                         'qty_allocated'                     => 0,
    //                         'status_po_buyer'                   => $status_po_buyer,
    //                         'is_fabric'                         => false,
    //                         'is_already_generate_form_booking'  => false,
    //                         'is_upload_manual'                  => false,
    //                         'is_allocation_purchase'            => true,
    //                         'created_at'                        => $movement_date,
    //                         'updated_at'                        => $movement_date,
    //                         'user_id'                           => $system->id,
    //                         'erp_allocation_id'                 => $erp_allocation_id,
    //                         'is_additional'                     => $is_additional,
    //                     ]);

    //                     $udpateErp [] = $newAllocation->erp_allocation_id;
    //                 }else
    //                 {
    //                         $system = User::where([
    //                             ['name','system'],
    //                             ['warehouse',$warehouse_id]
    //                         ])
    //                         ->first();
    //                         $old_qty_allocation  = sprintf('%0.8f',$is_exists->qty_allocation);
    //                         $old_qty_outstanding = sprintf('%0.8f',$is_exists->qty_outstanding);
    //                         $remark              = $is_exists->remark;
        
    //                         if($old_qty_allocation/$qty == 1)
    //                         {
    //                             $is_exists->erp_allocation_id = $erp_allocation_id;
    //                             $is_exists->save();
    //                         }
    //                         else
    //                         {
    //                             $is_exists->qty_allocation    = $old_qty_allocation + $qty;
    //                             $is_exists->qty_outstanding   = $old_qty_outstanding + $qty;
    //                             $is_exists->erp_allocation_id = $erp_allocation_id;
    //                             $is_exists->remark            = $remark. 'PENAMBAHAN QTY ALLOCATION SEBESAR '.$qty;
    //                             $is_exists->user_id           = $system->id;
    //                             $is_exists->save();
    //                         }

    //                         $udpateErp [] = $erp_allocation_id;

    //                 }
                    
    //             }
                
    //         }

    //         DB::commit();
    //         DB::connection('erp') //dev_erp //erp
    //         ->table('rma_wms_planning_alokasi')
    //         ->whereIn('uuid',$udpateErp)
    //         ->update([
    //             'isintegrate' => true,
    //             'updated'     => $movement_date
    //         ]);

    //     } catch (Exception $e) 
    //     {
    //         DB::rollBack();
    //         $message = $e->getMessage();
    //         ErrorHandler::db($message);
    //     }
       
    // }

    static function dailyInsertPurchaseAllocationItems()
    {
        $auto_allocations = AutoAllocation::where([
            ['is_already_generate_form_booking',false],
            ['is_fabric',true],
            ['is_allocation_purchase',true],
        ])
        ->whereNotNull('po_buyer')
        ->whereNull('deleted_at')
        ->orderby('promise_date','asc')
        ->orderby('qty_outstanding','asc')
        ->get();

        foreach ($auto_allocations as $key_2 => $auto_allocation) 
        {
            $auto_allocation_id = $auto_allocation->id;
            $user_id            = $auto_allocation->user_id;
            $document_no        = $auto_allocation->document_no;
            $c_bpartner_id      = $auto_allocation->c_bpartner_id;
            $po_buyer           = $auto_allocation->po_buyer;
            $old_po_buyer       = $auto_allocation->old_po_buyer;
            $item_code          = $auto_allocation->item_code;
            $item_id_book       = $auto_allocation->item_id_book;
            $item_id_source     = $auto_allocation->item_id_source;
            $item_code_source   = $auto_allocation->item_code_source;
            $qty_outstanding    = sprintf('%0.8f',$auto_allocation->qty_outstanding);
            $qty_allocated      = sprintf('%0.8f',$auto_allocation->qty_allocated);
            $warehouse_id       = $auto_allocation->warehouse_id;
            $supplier           = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
            $supplier_name      = ($supplier)? $supplier->supplier_name : null;
            $supplier_code      = ($supplier)? $supplier->supplier_code : null;
            $remark             = $auto_allocation->remark;

            $system                     = User::where([
                ['name','system'],
                ['warehouse',$warehouse_id]
            ])
            ->first();
            
            $_po_buyer                  = PoBuyer::where('po_buyer',$po_buyer)
            ->whereNull('cancel_date')
            ->first();

            if($_po_buyer)
            {
                $material_requirements = MaterialRequirement::where([
                    ['po_buyer',$po_buyer],
                    [db::raw('upper(item_code)'),$item_code],
                ])
                ->orderby('article_no','asc')
                ->orderby('style','asc')
                ->get();
    
                if($qty_outstanding > 0)
                {
                    $count_mr = $material_requirements->count();
                    foreach ($material_requirements as $key => $material_requirement) 
                    {
                        $lc_date        = $material_requirement->lc_date;
                        $item_desc      = strtoupper($material_requirement->item_desc);
                        $uom            = $material_requirement->uom;
                        $category       = $material_requirement->category;
                        $style          = $material_requirement->style;
                        $job_order      = $material_requirement->job_order;
                        $_style         = explode('::',$job_order)[0];
                        $article_no     = $material_requirement->article_no;
                        
                        if($material_requirement->lc_date)
                        {
                            $month_lc                           = $material_requirement->lc_date->format('m');
                            if($month_lc <= '02') $qty_required = sprintf("%0.2f",$material_requirement->qty_required - $material_requirement->qtyAllocated($po_buyer,$item_code,$style,$article_no));
                            else $qty_required                  = sprintf('%0.8f',$material_requirement->qty_required - $material_requirement->qtyAllocated($po_buyer,$item_code,$style,$article_no));
                        }else
                        {
                            $month_lc                           = '04';
                            $qty_required                       = sprintf('%0.8f',$material_requirement->qty_required - $material_requirement->qtyAllocated($po_buyer,$item_code,$style,$article_no));
                        }
                        
                        //if(($key+1) != $count_mr) $__supplied = sprintf('%0.8f',$qty_required);
                        //else $__supplied = sprintf('%0.8f',$qty_outstanding);
                        $__supplied         = sprintf('%0.8f',$qty_required);
                        $_warehouse_erp_id  = $material_requirement->warehouse_id;
    
                        if($_warehouse_erp_id == '1000007' && $warehouse_id == '1000001') $warehouse_production_id      = '1000001';
                        else if($_warehouse_erp_id == '1000010' && $warehouse_id == '1000011') $warehouse_production_id = '1000011';
                        else $warehouse_production_id                                                                   = $warehouse_id;
                        
                        if($qty_required > 0 && $qty_outstanding > 0)
                        {
                            if ($qty_outstanding/$__supplied >= 1) $_supply = $__supplied;
                            else $_supply                                   = $qty_outstanding;
    
                            if($month_lc <= '02') $__supply = sprintf("%0.2f",$_supply);
                            else $__supply                  = sprintf('%0.8f',$_supply);
    
                            //$__supply = sprintf('%0.8f',$_supply);
                        
                            $is_exists = AllocationItem::where([
                                ['auto_allocation_id',$auto_allocation_id],
                                ['item_code',$item_code],
                                ['style',$style],
                                ['po_buyer',$po_buyer],
                                ['qty_booking',$__supply],
                                ['is_additional',false],
                                ['is_from_allocation_fabric',true],
                                ['warehouse_inventory',$warehouse_id] // warehouse inventory
                            ])
                            ->where(function($query){
                                $query->where('confirm_by_warehouse','approved')
                                ->OrWhereNull('confirm_by_warehouse');
                            })
                            ->whereNull('deleted_at')
                            ->exists();
                            
                            if(!$is_exists && $_supply > 0)
                            {
                                $allocation_item = AllocationItem::FirstOrCreate([
                                    'auto_allocation_id'        => $auto_allocation_id,
                                    'lc_date'                   => $lc_date,
                                    'c_bpartner_id'             => $c_bpartner_id,
                                    'supplier_code'             => $supplier_code,
                                    'supplier_name'             => $supplier_name,
                                    'document_no'               => $document_no,
                                    'item_code'                 => $item_code,
                                    'item_id_book'              => $item_id_book,
                                    'item_code_source'          => $item_code_source,
                                    'item_id_source'            => $item_id_source,
                                    'item_desc'                 => $item_desc,
                                    'category'                  => $category,
                                    'uom'                       => $uom,
                                    'job'                       => $job_order,
                                    'style'                     => $style,
                                    '_style'                    => $_style,
                                    'article_no'                => $article_no,
                                    'po_buyer'                  => $po_buyer,
                                    'old_po_buyer_reroute'      => $old_po_buyer,
                                    'warehouse'                 => $warehouse_production_id,
                                    'warehouse_inventory'       => $warehouse_id,
                                    'is_need_to_handover'       => false,
                                    'qty_booking'               => $__supply,
                                    'is_from_allocation_fabric' => true,
                                    'user_id'                   => $user_id,
                                    'confirm_by_warehouse'      => 'approved',
                                    'confirm_date'              => Carbon::now(),
                                    'remark'                    => $remark,
                                    'confirm_user_id'           => $system->id,
                                    'deleted_at'                => null
                                ]);
    
                                //echo $po_buyer.' '.$_supplied.' '.$material_stock_id.'<br/>';
                                //if($is_need_handover == true) $insert_summary_handover [] = $allocation_item->id;
                                $qty_required       -= $_supply;
                                $qty_outstanding    -= $_supply;
                                $qty_allocated      += $_supply;
                            }
                        }
                    }
                }
                
                //echo $po_buyer.' '.$qty_outstanding.' '.$qty_allocated.'<br/>';
                
                if(intVal($qty_outstanding)<= 0)
                {
                    $auto_allocation->is_already_generate_form_booking  = true;
                    $auto_allocation->generate_form_booking             = carbon::now();
                    $auto_allocation->qty_outstanding                   = 0;
                }else
                {
                    $auto_allocation->qty_outstanding                   = sprintf('%0.8f',$qty_outstanding);
                
                }
    
                $auto_allocation->qty_allocated                         = sprintf('%0.8f',$qty_allocated);
                $auto_allocation->save();
            }

            
            
        }
    }
}
