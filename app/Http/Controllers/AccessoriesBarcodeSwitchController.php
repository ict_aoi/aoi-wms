<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use Illuminate\Support\Facades\Session;

use App\Models\Item;
use App\Models\Barcode;
use App\Models\Locator;
use App\Models\Temporary;
use App\Models\PoSupplier;
use App\Models\UomConversion;
use App\Models\MaterialSwitch;
use App\Models\MaterialMovement;
use App\Models\MaterialPreparation;
use App\Models\MaterialRequirement;
use App\Models\MaterialMovementLine;
use App\Models\DetailMaterialPreparation;

class AccessoriesBarcodeSwitchController extends Controller
{
    public function index()
    {
        return view('accessories_material_switch.print.index');
    }
    
    public function create(Request $request){
        $barcode = strtoupper($request->barcode);
        $material_preparation = MaterialPreparation::where([
            ['barcode',$barcode],
            ['warehouse',auth::user()->warehouse],
        ])
        ->whereNull('deleted_at')
        ->first();

        if(!$material_preparation) return response()->json('Material tidak ditemukan',422);
        if(!$material_preparation->is_need_to_be_switch) return response()->json('Material ini tidak ada kebutuhan untuk ditukar',422);
        
        $document_no = strtoupper($material_preparation->document_no);
        $item_code = strtoupper($material_preparation->item_code);
        $po_buyer = strtoupper($material_preparation->po_buyer);
        $category = strtoupper($material_preparation->category);
        $qty_conversion = sprintf('%0.8f',$material_preparation->qty_conversion);
        $uom_conversion = $material_preparation->uom_conversion;

        $sources = MaterialSwitch::where([
            [db::raw('upper(document_no_source)'),$document_no],
            [db::raw('upper(item_code)'),$item_code],
            [db::raw('upper(po_buyer_source)'),$po_buyer],
            ['qty_need_source_outstanding','!=',0]
        ])
        ->whereNull('deleted_at')
        ->get();

        if($sources->count() > 0){
            $note = 'Dipinjamkan untuk';
            $_material_switch_id = array();
            
            foreach ($sources as $key => $source) {
                $_material_switch_id [] = $source->id;
                $qty_switch = sprintf('%0.8f',$source->qty_switch);
                $_qty_need_source = sprintf('%0.8f',$source->_qty_need_source);

                $new_qty_switch = sprintf('%0.8f',$qty_switch - $_qty_need_source);
                if ($qty_conversion/$new_qty_switch >= 1) $_supply = $new_qty_switch;
                else $_supply = $qty_conversion;

                $note .= ' '.$source->po_buyer.' '.$source->qty_switch.'('.$source->uom.') Total yang dipinjamkan '.$_supply.'('.$uom_conversion.'),';
                $qty_conversion -= $qty_switch;
                $source->_qty_need_source = sprintf('%0.8f',$_qty_need_source + $_supply);
                $source->save();
            }

        }else{
            $destinations = MaterialSwitch::where([
                [db::raw('upper(document_no_new)'),$document_no],
                [db::raw('upper(item_code)'),$item_code],
                [db::raw('upper(po_buyer)'),$po_buyer],
                ['qty_need_new_outstanding','!=',0]
            ])
            ->whereNull('deleted_at')
            ->get();

            if($destinations->count() > 0){
                $note = 'Dikembalikan untuk';
                $_material_switch_id = array();
                foreach ($destinations as $key => $destination) {
                    $_material_switch_id [] = $destination->id;
                    $qty_switch = sprintf('%0.8f',$destination->qty_switch);
                    $_qty_need_new = sprintf('%0.8f',$destination->_qty_need_new);

                    $new_qty_switch = sprintf('%0.8f',$qty_switch - $_qty_need_new);
                    if ($qty_conversion/$new_qty_switch >= 1) $_supply = $new_qty_switch;
                    else $_supply = $qty_conversion;

                    $note .= ' '.$destination->po_buyer_source.' '.$destination->qty_switch.'('.$destination->uom.') Total yang dikembalikan '.$_supply.'('.$uom_conversion.'),';
                    $qty_conversion -= $qty_switch;
                    $destination->_qty_need_new = sprintf('%0.8f',$_qty_need_new + $_supply);
                    $destination->save();
                }
            }
        }
        
        $obj = new stdClass();
        $obj->id = $material_preparation->id;
        $obj->material_switch_id = $material_preparation->id;
        $obj->barcode = $material_preparation->barcode;
        $obj->document_no = $document_no;
        $obj->item_code = $item_code;
        $obj->category = $category;
        $obj->po_buyer = $po_buyer;
        $obj->uom = $material_preparation->uom_conversion;
        $obj->qty_switch = $material_preparation->qty_conversion;
        $obj->movement_date = carbon::now()->toDateTimeString();
        $obj->material_switch_id = $_material_switch_id;
        $obj->note = substr($note,0,-1);
        
        return response()->json($obj);
    }

    public function printout(Request $request){
        $validator = Validator::make($request->all(), [
            'material_print_barcode_switches' => 'required|not_in:[]'
        ]);

        if ($validator->passes()){
            $material_print_barcode_switches = json_decode($request->material_print_barcode_switches);

            $locator_switch = Locator::whereHas('area',function($query){
                $query->where([
                    ['warehouse',auth::user()->warehouse],
                    ['name','SWITCH'],
                ]);
            })
            ->where('rack','SWITCH')
            ->first();

            $switch_location = Locator::with('area')
            ->whereHas('area',function ($query){
                $query->where('is_destination',false);
                $query->where('is_active',true);
                $query->where('warehouse',auth::user()->warehouse);
                $query->where('name','SWITCH');
            })
            ->where('rack','PRINT BARCODE')
            ->first();
    
            $receiving_location = Locator::with('area')
            ->whereHas('area',function ($query){
                $query->where('is_destination',false);
                $query->where('is_active',true);
                $query->where('warehouse',auth::user()->warehouse);
                $query->where('name','RECEIVING');
            })
            ->first();

            $array = array();
                  
            $barcodes = array();
            // KURANGI PO SWITCH
            foreach ($material_print_barcode_switches as $key => $value) {
                $id = $value->id;
                $material_switch_id = $value->id;
                $document_no = $value->document_no;
                $item_code = $value->item_code;
                $po_buyer = $value->po_buyer;
                $qty_switch = $value->qty_switch;
                $note = $value->note;
                $movement_date = $value->movement_date;
                $material_preparation = MaterialPreparation::find($id);
                $warehouse_id = $material_preparation->warehouse;
                $last_locator_id = $material_preparation->last_locator_id;
                
                $uom_conversion = UomConversion::where('item_code',$item_code)->first();
                if($uom_conversion) $multiplyrate = $uom_conversion->multiplyrate;
                else $multiplyrate = 1;

                $sources = MaterialSwitch::where([
                    [db::raw('upper(document_no_source)'),$document_no],
                    [db::raw('upper(item_code)'),$item_code],
                    [db::raw('upper(po_buyer_source)'),$po_buyer],
                    ['qty_need_source_outstanding','!=',0]
                ])
                ->whereNull('deleted_at')
                ->get();

                //dd($sources);

                if($sources->count() > 0){
                    $qty_movement = 0;
                    foreach ($sources as $key => $source){
                        $item_id_source = $source->item_id;
                        $c_order_id_source = $source->c_order_id_source;
                        $document_no_source = $source->document_no_source;
                        $c_bpartner_id_source = $source->c_bpartner_id_source;
                        $supplier_name_source = $source->supplier_name_source;
                        $qty_need_source_outstanding = $source->qty_need_source_outstanding;
                        $qty_need_source_switched = $source->qty_need_source_switched;

                        $document_no_new = $source->document_no_new;
                        $c_bpartner_id_new = $source->c_bpartner_id_new;
                        $po_buyer_new = $source->po_buyer;
                        $item_code_new = $source->item_code;
                        $_qty_switch = $source->qty_switch;

                        /*$preparation_news = MaterialPreparation::where([
                            ['c_bpartner_id',$c_bpartner_id_new],
                            ['po_buyer',$po_buyer_new],
                            [db::raw('upper(item_code)'),$item_code_new],
                            [db::raw('upper(document_no)'),$document_no_new],
                        ])
                        ->whereNull('deleted_at')
                        ->get();
                        
                        foreach ($preparation_news as $key_ => $preparation_new){
                            $qty_conversion = $preparation_new->qty_conversion - $preparation_new->qty_switch;
                            if($_qty_switch > 0){
                                if ($qty_switch/$qty_conversion >= 1) $_supply = $qty_conversion;
                                else $_supply = $qty_switch;

                                if($_supply > 0){
                                    $preparation_new->qty_switch = sprintf('%0.8f',$_supply);
                                    $_qty_switch -= $_supply; 
                                    $qty_conversion -= $_supply; 
                                    $preparation_new->save();
                                }
                                
                            }
                        }*/
                        
                        if($qty_switch > 0 && $qty_need_source_outstanding > 0){
                            if ($qty_switch/$qty_need_source_outstanding >= 1) $_supply = $qty_need_source_outstanding;
                            else $_supply = $qty_switch;

                            if($_supply > 0 ){
                                $_new_qty_need_source_outstanding = $qty_need_source_outstanding - $_supply;
                                if($_new_qty_need_source_outstanding <= 0) $new_qty_need_source_outstanding = 0;
                                else $new_qty_need_source_outstanding = $_new_qty_need_source_outstanding;
                                
                                if($new_qty_need_source_outstanding <= 0) $material_preparation->is_need_to_be_switch = false;

                                $obj = new stdClass();
                                $obj->material_switch_id = $material_switch_id;
                                $obj->c_order_id = $c_order_id_source;
                                $obj->c_bpartner_id = $c_bpartner_id_source;
                                $obj->supplier_name = $supplier_name_source;
                                $obj->document_no = $document_no_source;
                                $obj->item_id = $item_id_source;
                                $obj->item_code = $item_code;
                                $obj->po_buyer = $source->po_buyer;
                                $obj->uom = $source->uom;
                                $obj->qty_need = $_supply;
                                $obj->po_buyer_source = $source->po_buyer_source;
                                $obj->warehouse_id = $warehouse_id;
                                $obj->movement_date = $movement_date;
                                $obj->note = 'DIPINJAMKAN DARI PO '.$source->po_buyer_source.'UNTUK PO BUYER '.$source->po_buyer.' TOTAL YANG DIPINJAMKAN ('.$_supply.')';
                                $obj->detail_old_prepartions = $material_preparation->detailMaterialPreparation()->select('material_arrival_id','allocation_item_id')->get();
                                $array [] = $obj;

                                $source->qty_need_source_outstanding = $new_qty_need_source_outstanding;
                                $source->qty_need_source_switched = $qty_need_source_switched + $_supply;
                                $source->save();
                              
                            }

                            $qty_switch -= $_supply;
                            if($material_preparation->qty_borrow) $qty_borrow = $material_preparation->qty_borrow;
                            else $qty_borrow = 0;

                            $material_preparation->qty_borrow = $qty_borrow + $_supply;
                            $material_preparation->ict_log = 'DIPINJAMKAN DARI PO '.$source->po_buyer_source.'UNTUK PO BUYER '.$source->po_buyer.' TOTAL YANG DIPINJAMKAN ('.$_supply.')';
                            $qty_movement += $_supply;
                            if($qty_switch <= 0){
                                $material_preparation->qty_reconversion = 0;
                                $material_preparation->qty_conversion = 0;
                                $material_preparation->deleted_at = carbon::now();
                                $material_preparation->last_locator_id = $locator_switch->id;
                                $material_preparation->last_status_movement = 'switch';
                            }else{
                                $material_preparation->qty_reconversion = sprintf('%0.8f',$qty_switch * $multiplyrate);
                                $material_preparation->qty_conversion = sprintf('%0.8f',$qty_switch);
                                $material_preparation->is_need_to_be_switch = false;
                                $barcodes [] = $material_preparation->id;
                            }
                        }
                    }

                    $movement_switch = MaterialMovement::FirstOrCreate([
                        'from_location' => $last_locator_id,
                        'to_destination' => $locator_switch->id,
                        'po_buyer' => $po_buyer,
                        'is_active' => true,
                        'status' => 'switch',
                        'is_integrate' => false
                    ]);

                    MaterialMovementLine::firstOrCreate([
                        'material_movement_id'    => $movement_switch->id,
                        'material_preparation_id' => $id,
                        'item_code'               => $item_code,
                        'type_po'                 => 2,
                        'qty_movement'            => sprintf('%0.8f',$qty_switch),
                        'date_movement'           => $movement_date,
                        'created_at'              => $movement_date,
                        'updated_at'              => $movement_date,
                        'is_active'               => false,
                        'note'                    => $note,
                        'user_id'                 => Auth::user()->id
                    ]);

                    $material_preparation->save();

                    //echo '<br/>'.$qty_switch;

                }else{
                    $destinations = MaterialSwitch::where([
                        [db::raw('upper(document_no_new)'),$document_no],
                        [db::raw('upper(item_code)'),$item_code],
                        [db::raw('upper(po_buyer)'),$po_buyer],
                        ['qty_need_new_outstanding','!=',0]
                    ])
                    ->whereNull('deleted_at')
                    ->get();

                    if($destinations->count() > 0){
                        $qty_movement = 0;

                        foreach ($destinations as $key => $destination){
                            $item_id_new = $destination->item_id;
                            $c_order_id_new = $destination->c_order_id_new;
                            $document_no_new = $destination->document_no_new;
                            $c_bpartner_id_new = $destination->c_bpartner_id_new;
                            $supplier_name_new = $destination->supplier_name_new;
                            $qty_need_new_outstanding = $destination->qty_need_new_outstanding;
                            $qty_need_new_switched = $destination->qty_need_new_switched;
                            
                            $document_no_source = $destination->document_no_source;
                            $c_bpartner_id_source = $destination->c_bpartner_id_source;
                            $po_buyer_source = $destination->po_buyer_source;
                            $item_code_source = $destination->item_code;
                            $_qty_switch = $destination->qty_switch;

                            /*$preparation_sources = MaterialPreparation::where([
                                ['c_bpartner_id',$c_bpartner_id_source],
                                ['po_buyer',$po_buyer_source],
                                [db::raw('upper(item_code)'),$item_code_source],
                                [db::raw('upper(document_no)'),$document_no_source],
                            ])
                            ->whereNull('deleted_at')
                            ->get();
                            
                            foreach ($preparation_sources as $key_ => $preparation_source){
                                $qty_conversion = $preparation_source->qty_conversion - $preparation_source->qty_switch;
                                if($_qty_switch > 0){
                                    if ($qty_switch/$qty_conversion >= 1) $_supply = $qty_conversion;
                                    else $_supply = $qty_switch;

                                    dd($_supply);
                                    if($_supply > 0){
                                        $preparation_source->qty_switch = sprintf('%0.8f',$_supply);
                                        $_qty_switch -= $_supply; 
                                        $qty_conversion -= $_supply; 
                                        $preparation_source->save();
                                    }
                                    
                                }
                            }*/

                            if($qty_switch > 0 && $qty_need_new_outstanding > 0){
                                if ($qty_switch/$qty_need_new_outstanding >= 1) $_supply = $qty_need_new_outstanding;
                                else $_supply = $qty_switch;

                                if($_supply > 0 ){
                                    $_new_qty_need_new_outstanding = $qty_need_new_outstanding - $_supply;
                                    if($_new_qty_need_new_outstanding <= 0) $new_qty_need_new_outstanding = 0;
                                    else $new_qty_need_new_outstanding = $_new_qty_need_new_outstanding;
                                    
                                    if($qty_need_new_outstanding <= 0) $material_preparation->is_need_to_be_switch = false;

                                    $obj = new stdClass();
                                    $obj->material_switch_id = $material_switch_id;
                                    $obj->c_order_id = $c_order_id_new;
                                    $obj->c_bpartner_id = $c_bpartner_id_new;
                                    $obj->supplier_name = $supplier_name_new;
                                    $obj->document_no = $document_no_new;
                                    $obj->item_id = $item_id_new;
                                    $obj->item_code = $item_code;
                                    $obj->po_buyer = $destination->po_buyer_source;
                                    $obj->uom = $destination->uom;
                                    $obj->qty_need = $_supply;
                                    $obj->warehouse_id = $warehouse_id;
                                    $obj->po_buyer_source = $destination->po_buyer;
                                    $obj->movement_date = $movement_date;
                                    $obj->note = 'DIKEMBALIKAN DARI PO BUYER '.$destination->po_buyer.' UNTUK PO BUYER '.$destination->po_buyer_source.' TOTAL YANG DIKEMBALIKAN ('.$_supply.')';
                                    $obj->detail_old_prepartions = $material_preparation->detailMaterialPreparation()->select('material_arrival_id','allocation_item_id')->get();
                                    $array [] = $obj;

                                    $destination->qty_need_new_outstanding = $new_qty_need_new_outstanding;
                                    $destination->qty_need_new_switched = $qty_need_new_switched + $_supply;
                                    $destination->save();
                                
                                }

                                $qty_switch -= $_supply;
                                if($material_preparation->qty_borrow) $qty_borrow = $material_preparation->qty_borrow;
                                else $qty_borrow = 0;

                                $material_preparation->qty_borrow = $qty_borrow + $_supply;
                                $material_preparation->ict_log = 'DIKEMBALIKAN DARI PO BUYER '.$destination->po_buyer.' UNTUK PO BUYER '.$destination->po_buyer_source.' TOTAL YANG DIKEMBALIKAN ('.$_supply.')';
                            
                                $qty_movement += $_supply;

                                if($qty_switch <= 0){
                                    $material_preparation->qty_reconversion = 0;
                                    $material_preparation->qty_conversion = 0;
                                    $material_preparation->deleted_at = carbon::now();
                                    $material_preparation->last_locator_id = $locator_switch->id;
                                    $material_preparation->last_status_movement = 'switch';
                                }else{
                                    $material_preparation->qty_reconversion = sprintf('%0.8f',$qty_switch * $multiplyrate);
                                    $material_preparation->qty_conversion = sprintf('%0.8f',$qty_switch);
                                    $material_preparation->is_need_to_be_switch = false;
                                    $barcodes [] = $material_preparation->id;
                                }
                            }
                        }

                        $movement_switch = MaterialMovement::FirstOrCreate([
                            'from_location' => $last_locator_id,
                            'to_destination' => $locator_switch->id,
                            'po_buyer' => $po_buyer,
                            'is_active' => true,
                            'status' => 'switch',
                            'is_integrate' => false
                        ]);

                        MaterialMovementLine::firstOrCreate([
                            'material_movement_id'    => $movement_switch->id,
                            'material_preparation_id' => $id,
                            'item_code'               => $item_code,
                            'type_po'                 => 2,
                            'qty_movement'            => sprintf('%0.8f',$qty_switch),
                            'date_movement'           => $movement_date,
                            'created_at'              => $movement_date,
                            'updated_at'              => $movement_date,
                            'is_active'               => false,
                            'note'                    => $note,
                            'user_id'                 => Auth::user()->id
                        ]);

                        $material_preparation->save();
                    }
                }

            }

            //INSERT NEW PO BUYER
            foreach ($array as $key => $value) {
                $material_switch_id = $value->material_switch_id;
                $c_order_id = $value->c_order_id;
                $c_bpartner_id = $value->c_bpartner_id;
                $document_no = $value->document_no;
                $po_buyer = $value->po_buyer;
                $supplier_name = $value->supplier_name;
                $item_id = $value->item_id;
                $item_code = $value->item_code;
                $qty_need = $value->qty_need;
                $uom = $value->uom;
                $warehouse_id = $value->warehouse_id;
                $note = $value->note;
                $detail_old_prepartions = $value->detail_old_prepartions;
                $po_buyer_source = $value->po_buyer_source;
                $movement_date = $value->movement_date;
    
                $material_requirements = MaterialRequirement::where([
                    ['po_buyer',$po_buyer],
                    ['item_code',$item_code],
                ])
                ->get();
    
                $conversion = UomConversion::where([
                    ['item_code',$item_code],
                    ['uom_to',$uom],
                ])
                ->first();
    
                if($conversion)  $multiplyrate = $conversion->multiplyrate;
                else $multiplyrate = 1;
    
                foreach ($material_requirements as $key_2 => $material_requirement) {
                    $item_desc = $material_requirement->item_desc;
                    $category = $material_requirement->category;
                    $article_no = $material_requirement->article_no;
                    $style = $material_requirement->style;
                    $job_order = $material_requirement->job_order;
                    $_style = explode('::',$job_order)[0];
                    $qty_required = $material_requirement->qty_required - $material_requirement->qtyHasPrepared($po_buyer,$item_code,$style);
    
                    if($qty_need > 0){
                        if ($qty_need/$qty_required >= 1) $_supply = $qty_required;
                        else $_supply = $qty_need;
    
                        if($_supply > 0){
                            $is_preparation_exists = MaterialPreparation::where([
                                ['c_bpartner_id',$c_bpartner_id],
                                ['document_no',$document_no],
                                ['po_buyer',$po_buyer],
                                ['item_id',$item_id],
                                ['style',$style],
                                ['warehouse',$warehouse_id],
                                ['article_no',$article_no],
                                ['po_buyer_source',$po_buyer_source],
                                ['po_buyer_source',$_supply],
                                ['last_status_movement','print'],
                            ])
                            ->first();

                            if($is_preparation_exists){
                                $total_switch = $is_preparation_exists->total_switch;
                                $qty_conversion = $is_preparation_exists->qty_conversion;
                                $total_switch = sprintf('%0.8f',$total_switch+$_supply);
                                $new_qty_conversion = sprintf('%0.8f',$qty_conversion+$_supply);
                                $new_qty_reconversion = sprintf('%0.8f',$new_qty_conversion * $multiplyrate);

                                $is_preparation_exists->po_buyer_source = $po_buyer_source;
                                $is_preparation_exists->total_switch = $total_switch;
                                $is_preparation_exists->qty_reconversion = $new_qty_reconversion;
                                $is_preparation_exists->qty_conversion = $new_qty_conversion;
                                $is_preparation_exists->ict_log = 'DIPINJAMKAN DARI PO '.$po_buyer_source.' TOTAL YANG DIPINJAMKAN ('.$total_switch.')';
                                $is_preparation_exists->save();
                                $_material_preparation_id = $is_preparation_exists->id; 
                            }else{
                                $is_exists = MaterialPreparation::where([
                                    ['c_order_id',$c_order_id],
                                    ['c_bpartner_id',$c_bpartner_id],
                                    ['document_no',$document_no],
                                    ['po_buyer',$po_buyer],
                                    ['item_id',$item_id],
                                    ['uom_conversion',$uom],
                                    ['warehouse',$warehouse_id],
                                    ['style',$style],
                                    ['qty_conversion',$_supply],
                                    ['is_backlog',false],
                                    ['po_buyer_source',$po_buyer_source],
                                    ['last_status_movement','print']
                                ])
                                ->exists();
        
                                if(!$is_exists){
                                    $get_barcode = $this->random_code($item_id,$c_bpartner_id,$document_no,$po_buyer,$style,$article_no,$warehouse_id,$_supply,$po_buyer_source);
                                    if($get_barcode){
                                        $so_id                = PoBuyer::where('po_buyer', $po_buyer)->first();
                                        $material_preparation = MaterialPreparation::firstOrCreate([
                                            'barcode' => $get_barcode->barcode,
                                            'referral_code' => $get_barcode->referral_code,
                                            'sequence' => $get_barcode->sequence,
                                            'item_id' => $item_id,
                                            'c_order_id' => $c_order_id,
                                            'c_bpartner_id' => $c_bpartner_id,
                                            'supplier_name' => $supplier_name,
                                            'document_no' => $document_no,
                                            'po_buyer' => $po_buyer,
                                            'uom_conversion' => $uom,
                                            'qty_conversion' =>  sprintf('%0.8f',$_supply),
                                            'qty_reconversion' =>   sprintf('%0.8f',$_supply * $multiplyrate),
                                            'job_order' => $job_order,
                                            'article_no' => $article_no,
                                            'style' => $style,
                                            '_style' => $_style,
                                            'warehouse' => $warehouse_id,
                                            'item_code' => $item_code,
                                            'item_desc' => $item_desc,
                                            'category' => $category,
                                            'is_backlog' => false,
                                            'total_carton' => 1,
                                            'type_po' => 2,
                                            'user_id' => auth::user()->id,
                                            'is_from_switch' => true,
                                            'ict_log' => $note,
                                            'po_buyer_source' => $po_buyer_source,
                                            'total_switch' => $_supply,
                                            'last_status_movement' => 'print',
                                            'last_locator_id' => $switch_location->id,
                                            'last_movement_date' => $movement_date,
                                            'last_user_movement_id' => auth::user()->id,
                                            'so_id' => $so_id->so_id
                                        ]);
            
                                        foreach ($detail_old_prepartions as $key => $detail_old_prepartion) {
                                            DetailMaterialPreparation::firstOrCreate([
                                                'material_preparation_id' => $material_preparation->id,
                                                'material_arrival_id' => $detail_old_prepartion->material_arrival_id,
                                                'allocation_item_id' => $detail_old_prepartion->allocation_item_id,
                                                'user_id' => auth::user()->id
                                            ]);
                                        }

                                        $_material_preparation_id = $material_preparation->id; 
            
                                       
                                    }
                                    
                                }
                            }
                            

                            $material_movement = MaterialMovement::firstorcreate([
                                'from_location' => $switch_location->id,
                                'to_destination' => $receiving_location->id,
                                'is_active' => true,
                                'status' => 'print'
                            ]);

                            MaterialMovementLine::firstorcreate([
                                'material_movement_id'    => $material_movement->id,
                                'material_preparation_id' => $_material_preparation_id,
                                'item_code'               => $material_preparation->item_code,
                                'type_po'                 => $material_preparation->type_po,
                                'qty_movement'            => $_supply,
                                'date_movement'           => $movement_date,
                                'created_at'              => $movement_date,
                                'updated_at'              => $movement_date,
                                'is_active'               => false,
                                'user_id'                 => Auth::user()->id,
                                'note'                    => $note
                            ]);
                            
                            $barcodes [] = $material_preparation->id;
                            $temporary = Temporary::Create([
                                'barcode'       => $po_buyer,
                                'status'        => 'mrp',
                                'user_id'       => Auth::user()->id,
                                'created_at'    => $movement_date,
                                'updated_at'    => $movement_date,
                            ]);
                        }
    
                        $qty_need -= $_supply;
                    }
                }
            }

            return response()->json($barcodes,200);
        }else{
            return response()->json('silahkan scan barcode terlebih dahulu.',422);
        }
    }

    public function showBarcode(Request $request){
        $list_barcodes = json_decode($request->list_barcodes);
        
        $items = MaterialPreparation::orderby('po_buyer','item_code')
        ->where('qty_conversion','>','0')
        ->whereIn('material_preparations.id',$list_barcodes)
        ->get();

       return view('accessories_material_switch.print.reprint_barcode', compact('items'));
    }

    static function random_code($item_id,$c_bpartner_id,$document_no,$po_buyer,$style,$article_no,$warehouse_id,$qty,$po_buyer_source){
         
        $is_preparation_exists = MaterialPreparation::where([
            ['item_id',$item_id],
            ['c_bpartner_id',$c_bpartner_id],
            ['document_no',$document_no],
            ['po_buyer',$po_buyer],
            ['style',$style],
            ['article_no',$article_no],
            ['warehouse',$warehouse_id],
            ['qty_conversion',$qty],
            ['po_buyer_source',$po_buyer_source]
        ])
        ->exists();

        if(!$is_preparation_exists){
            $referral_code = '2S'.Carbon::now()->format('u');
            $sequence = MaterialPreparation::where('referral_code',$referral_code)->max('sequence');;

            if ($sequence == null)
                $sequence = 1;
            else
                $sequence += 1;

            Barcode::firstorCreate([
                'barcode' => $referral_code.''.$sequence,
                'referral_code' => $referral_code,
                'sequence' => $sequence,
            ]);
            
            $obj = new stdClass();
            $obj->barcode = $referral_code.''.$sequence;
            $obj->referral_code = $referral_code;
            $obj->sequence = $sequence;
            return $obj;
        }
        return null;
    }


}
