<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use Illuminate\Support\Facades\Session;

use App\Models\Area;
use App\Models\Locator;

class MasterDataDestinationController extends Controller
{
    public function index()
    {
        $flag = Session::get('flag');
        return view('master_data_destination.index',compact('flag'));
    }

    public function data(Request $request)
    {
        if ($request->ajax()) 
        { 
            $warehouse_id   = ($request->warehouse ? $request->warehouse : auth::user()->warehouse );
            $destinations = Area::where([
                 ['warehouse',$warehouse_id],
                 ['is_active',true],
                 ['is_destination',true],
                 ['name','!=','CANCEL ORDER'],
                 ['name','!=','CANCEL ITEM'],
            ]);

            return DataTables::of($destinations)
            ->addColumn('action', function($destinations){ 
                return view('master_data_destination._action', [
                    'model'     => $destinations,
                    'edit'      => route('destination.edit', $destinations->id),
                    'delete'    => route('destination.delete', $destinations->id)
                ]);
            })    
            ->make(true);
        }
    }
    
    public function create()
    {
        return view('master_data_destination.create');
    }

    public function store(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'name' => 'required',
            'destination' => 'required|not_in:[]'
        ]);

        if ($validator->passes()) 
        {
            if(Session::has('flag')) Session::forget('flag');

            $created_at = Carbon::now()->ToDateTimeString();
            $name       = strtoupper($request->name);
            $is_subcont = ($request->exists('is_subcont') ? true:false);
            $warehouse  = $request->warehouse;
        
            if(area::where(db::raw('upper(name)'),strtoupper($name))
            ->where([
                ['warehouse',$warehouse],
                ['is_destination',true],
                ['is_subcont',$is_subcont]
            ])
            
            ->exists()) return response()->json(['message' => 'Destination Already Exists'],422); 
            
            try
            {
                db::beginTransaction();

                if($warehouse == '1000002' || $warehouse == '1000013') $type_po = 2;
                else if($warehouse == '1000011' || $warehouse == '1000001') $type_po = 1;

                $destination = area::create([
                    'name'              => $name,
                    'warehouse'         => $warehouse,
                    'type_po'           => $type_po,
                    'is_destination'    => true,
                    'is_subcont'        => $is_subcont,
                    'user_id'           => Auth::user()->id,
                    'created_at'        => $created_at,
                    'updated_at'        => $created_at,
                ]);
                
                $locators           = json_decode($request->destination);
                $set_locator        = new Locator();
                $insert_new_locator = array(); 

                foreach ($locators as $key => $locator) 
                {
                    $y_row      = ($locator->y_row)?$locator->y_row:null;
                    $z_column   = ($locator->z_column)?$locator->z_column:null;
                   

                    if($locator->area_id == -1)
                    {
                        Locator::create([
                            'area_id'       => $destination->id,
                            'code'          => $destination->name.'-'.strtoupper($locator->rack).' '.$y_row,
                            'rack'          => strtoupper($locator->rack),
                            'y_row'         => $y_row,
                            'z_column'      => $z_column,
                            'user_id'       => Auth::user()->id,
                            'created_at'    => $created_at,
                            'updated_at'    => $created_at,
                        ]);
                    }
                }
                
                db::commit();
            }catch (Exception $ex)
            {
                db::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message); 
            }
            
            Session::flash('flag', 'success');
            return response()->json(['message' => 'Success'], 200);      
        }else
        {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ],400); 
        }
    }

    public function edit($id)
    {
        $destination = Area::find($id);
        $locators    = $destination->locators()
        ->orderBy('rack','asc')
        ->orderBy('y_row','asc')
        ->get();
       
        return view('master_data_destination.edit',compact('destination','locators'));
    }

    public function update(Request $request, $id)
    {
        $validator =  Validator::make($request->all(), [
            'name' => 'required',
            'destination' => 'required|not_in:[]'
        ]);

        if ($validator->passes()) {
            if(Session::has('flag'))
                Session::forget('flag');
            
            $name = strtoupper($request->name);
            $rows_update = '';
            $flag_update = 0;

            if(area::where(db::raw('upper(name)'),strtoupper($name))->where('id','<>',$id)->where('warehouse',auth::user()->warehouse)->exists())
                return response()->json('TUJUAN SUDAH ADA',422); 
            
            try{
                db::beginTransaction();

                $destination = area::find($id);

                $locators = json_decode($request->destination);
                $set_locator = new Locator();
                
                $insert_new_locator = array(); 
                foreach ($locators as $key => $locator) {
                    $y_row = ($locator->y_row)?$locator->y_row:'null';
                    $z_column = ($locator->z_column)?$locator->z_column:'null';

                    $_y_row = ($locator->y_row)?$locator->y_row:null;
                    $_z_column = ($locator->z_column)?$locator->z_column:null;
                    $code = $destination->name.'-'.strtoupper($locator->rack).' '.$_y_row;
                    $rack = strtoupper($locator->rack);
                    
                    if($locator->area_id == -1){
                        $set_locator->id =  Uuid::generate(4);
                        $set_locator->area_id =  $destination->id;
                        $set_locator->code =  $code;
                        $set_locator->rack =  $rack;
                        $set_locator->y_row = $_y_row;
                        $set_locator->z_column =   $_z_column;
                        $set_locator->user_id =  Auth::user()->id;
                        $set_locator->created_at =  carbon::now();
                        $insert_new_locator [] = $set_locator->attributesToArray();
                    }else{
                        
                        $rows_update .= "(" 
                            ."'". $locator->id . "'," 
                            ."'". $code . "'," 
                            ."'". $rack . "'," 
                            . $y_row . "," 
                            . $z_column. 
                        "),";
                        $flag_update++;
                    }
                   
                }
                
                Locator::insert($insert_new_locator);
                if($flag_update > 0){
                    $rows_update = substr_replace($rows_update, "", -1);
                    db::select(db::raw("
                        update locators as tbl
                        set
                            code = col.code,
                            rack = col.rack,
                            y_row = col.y_row::integer,
                            z_column = col.z_column::integer,
                            updated_at = '".carbon::now()."'
                        from (values
                            " . $rows_update . "
                        ) as col(
                            id,
                            code,
                            rack,
                            y_row,
                            z_column
                        )
                        where col.id = tbl.id;
                    "));
                }
                db::commit();
            }catch (Exception $ex){
                db::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message); 
            }
            
            Session::flash('flag', 'success');
            return response()->json(['message' => 'Success'], 200);      
        }else{
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ],400); 
        }
    }

    public function destroy($id)
    {
       
    }
}
