<?php namespace App\Http\Controllers;

use Auth;
use DB;
use Excel;
use Config;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class AccessoriesReportMaterialQualityControlController extends Controller
{
    public function index()
    {
        return view('accessories_report_material_quality_control.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax())
        {
            $status             = $request->status;
            $warehouse_id       = $request->warehouse;
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();

            $material_checks = db::table('material_check_report')
            ->where('warehouse',$warehouse_id)
            ->whereBetween('inspection_date', [$start_date, $end_date]);

            if($status) $material_checks = $material_checks->where('status',$status);

            return DataTables::of($material_checks)
            ->editColumn('qty_ordered',function ($material_checks)
            {
                if($material_checks->qty_order != 0) return number_format($material_checks->qty_order, 4, '.', ',');
            })
            ->editColumn('qty_release',function ($material_checks)
            {
                return number_format($material_checks->qty_release, 4, '.', ',');
            })
            ->editColumn('qty_reject',function ($material_checks)
            {
                return number_format($material_checks->qty_reject, 4, '.', ',');
            })
            ->editColumn('inspection_date',function ($material_checks)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $material_checks->inspection_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('arrival_date',function ($material_checks)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $material_checks->arrival_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('warehouse',function ($material_checks)
            {
                if($material_checks->warehouse == '1000002') return 'Warehouse accessories AOI 1';
                elseif($material_checks->warehouse == '1000013') return 'Warehouse accessories AOI 2';
            })
            ->make(true);
        }
    }

    public function exportAll(Request $request)
    {

        $warehouse_id = $request->warehouse_id;
        if($warehouse_id == '1000013') $filename = 'report_qc_aoi2.csv';
        else if($warehouse_id == '1000002') $filename = 'report_qc_aoi1.csv';
        
        $file = Config::get('storage.report') . '/' . $filename;

        if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';

        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function export(Request $request)
    {
        $warehouse_id       = ($request->warehouse ? $request->warehouse : auth::user()->warehouse);
        $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(1);
        $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();


        $material_checks = db::table('material_check_report')
            ->where('warehouse',$warehouse_id)
            ->whereBetween('inspection_date', [$start_date, $end_date])->get();

            //dd($material_checks);

            $file_name = 'accessories_report_material_quality_control'.$start_date.' - '.$end_date;
            return Excel::create($file_name,function($excel) use ($material_checks)
            {
                $excel->sheet('active',function($sheet)use($material_checks)
                {
                    $sheet->setCellValue('A1','ARRIVAL_DATE');
                    $sheet->setCellValue('B1','NO_INVOICE');
                    $sheet->setCellValue('C1','NO_PACKING_LIST');
                    $sheet->setCellValue('D1','SUPPLIER_NAME');
                    $sheet->setCellValue('E1','DOCUMENT_NO');
                    $sheet->setCellValue('F1','INSPECTOR_NAME');
                    $sheet->setCellValue('G1','INSPECTION_DATE');
                    $sheet->setCellValue('H1','ITEM_CODE');
                    $sheet->setCellValue('I1','ITEM_DESC');
                    $sheet->setCellValue('J1','CATEGORY');
                    $sheet->setCellValue('K1','PO_BUYER');
                    $sheet->setCellValue('L1','JOB_ORDER');
                    $sheet->setCellValue('M1','ARTICLE_NO');
                    $sheet->setCellValue('N1','STYLE');
                    $sheet->setCellValue('O1','UOM_CONVERSION');
                    $sheet->setCellValue('P1','QTY_ORDER');
                    $sheet->setCellValue('Q1','STATUS');
                    $sheet->setCellValue('R1','QTY_REJECT');
                    $sheet->setCellValue('S1','REMARK');
                    $sheet->setCellValue('T1','WAREHOUSE');
                    $sheet->setCellValue('U1','QTY_RELEASE');
    
                $row=2;
                foreach ($material_checks as $i) 
                {  
                    $sheet->setCellValue('A'.$row,$i->arrival_date);
                    $sheet->setCellValue('B'.$row,$i->no_invoice);
                    $sheet->setCellValue('C'.$row,$i->no_packing_list);
                    $sheet->setCellValue('D'.$row,$i->supplier_name);
                    $sheet->setCellValue('E'.$row,$i->document_no);
                    $sheet->setCellValue('F'.$row,$i->inspector_name);
                    $sheet->setCellValue('G'.$row,$i->inspection_date);
                    $sheet->setCellValue('H'.$row,$i->item_code);
                    $sheet->setCellValue('I'.$row,$i->item_desc);
                    $sheet->setCellValue('J'.$row,$i->category);
                    $sheet->setCellValue('K'.$row,$i->po_buyer);
                    $sheet->setCellValue('L'.$row,$i->job_order);
                    $sheet->setCellValue('M'.$row,$i->article_no);
                    $sheet->setCellValue('N'.$row,$i->style);
                    $sheet->setCellValue('O'.$row,$i->uom_conversion);
                    $sheet->setCellValue('P'.$row,$i->qty_order);
                    $sheet->setCellValue('Q'.$row,$i->status);
                    $sheet->setCellValue('R'.$row,$i->qty_reject);
                    $sheet->setCellValue('S'.$row,$i->remark);
                    if($i->warehouse == '1000002') $warehouse = 'Warehouse accessories AOI 1';
                    elseif($i->warehouse == '1000013') $warehouse = 'Warehouse accessories AOI 2';
                    $sheet->setCellValue('T'.$row,$warehouse);
                    $sheet->setCellValue('U'.$row,$i->qty_release);
                    $row++;
                }
                });
    
            })
            ->export('csv');

    }

    public function exportSummary(Request $request)
    {
        $warehouse_id       = auth::user()->warehouse;
        $start_date         = ($request->_start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->_start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(1);
        $end_date           = ($request->_end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->_end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();


        $material_checks = DB::select(db::raw("SELECT * FROM get_report_material_check_summary(
            '".$warehouse_id."', '".$start_date."',  '".$end_date."' 
            )"));

            //dd($material_checks, $start_date, $end_date);

            $file_name = 'accessories_report_material_quality_control'.$start_date.' - '.$end_date;
            return Excel::create($file_name,function($excel) use ($material_checks)
            {
                $excel->sheet('active',function($sheet)use($material_checks)
                {
                    $sheet->setCellValue('A1','BARCODE');
                    $sheet->setCellValue('B1','SUPPLIER_NAME');
                    $sheet->setCellValue('C1','DOCUMENT_NO');
                    $sheet->setCellValue('D1','ITEM_CODE');
                    $sheet->setCellValue('E1','CATEGORY');
                    $sheet->setCellValue('F1','PO_BUYER');
                    $sheet->setCellValue('G1','RECEIVE DATE');
                    $sheet->setCellValue('H1','ALLOCATION DATE');
                    $sheet->setCellValue('I1','QC DATE');
                    $sheet->setCellValue('J1','JUMLAH BARCODE');
                    $sheet->setCellValue('K1','JUMLAH INSPECT');
                    $sheet->setCellValue('L1','DATE INSPECTED');
                    $sheet->setCellValue('M1','QC_INSPECTOR');
                $row=2;
                foreach ($material_checks as $i) 
                {  
                    $sheet->setCellValue('A'.$row,$i->barcode);
                    $sheet->setCellValue('B'.$row,$i->supplier_name);
                    $sheet->setCellValue('C'.$row,$i->document_no);
                    $sheet->setCellValue('D'.$row,$i->item_code);
                    $sheet->setCellValue('E'.$row,$i->category);
                    $sheet->setCellValue('F'.$row,$i->po_buyer);
                    $sheet->setCellValue('G'.$row,$i->receive_date);
                    $sheet->setCellValue('H'.$row,$i->allocation_date);
                    $sheet->setCellValue('I'.$row,$i->qc_status);
                    $sheet->setCellValue('J'.$row,$i->jumlah_barcode);
                    $sheet->setCellValue('K'.$row,$i->jumlah_inspect);
                    $sheet->setCellValue('L'.$row,$i->date_inspected);
                    $sheet->setCellValue('M'.$row,$i->qc_inspector);
                    $row++;
                }
                });
    
            })
            ->export('csv');

    }
}
