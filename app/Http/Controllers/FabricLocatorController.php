<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Area;
use App\Models\MaterialStock;
use App\Models\DetailMaterialPreparationFabric;

class FabricLocatorController extends Controller
{
    public function index(Request $request)
    {
        $warehouse_id = (isset($request->warehouse) ? $request->warehouse : Auth::user()->warehouse);
        $areas = Area::where([
            ['is_active',true],
            ['warehouse',$warehouse_id],
            ['is_destination',false],
            ['type_po','1'],
        ])
        ->where(function($query)
        {
            $query->where('name','RECEIVING')
            ->orWhere('name','INVENTORY')
            ->orWhere('name','RELAX')
            ->orWhere('name','CONTAINER');
        })
        ->whereIn('id',function($query){
            $query->select('area_id')
            ->from('locators')
            ->groupby('area_id');
        })
        ->orderBy('name','desc')
        ->get();

        $filtering_locator      = null;
        $summary_filter_stock   = null;
        $filter_item_code       = strtoupper(trim($request->item_code));
       
        if($filter_item_code)
        {
            $filtering_locator = MaterialStock::select('locator_id')
            ->where([
                ['warehouse_id',$warehouse_id],
                ['type_po','1'],
                ['is_allocated',false],
                ['is_closing_balance',false],
                ['is_active',true]
            ])
            ->whereNotNull('approval_date')
            ->where('item_code','LIKE',"%$filter_item_code%")
            ->groupby('locator_id');

            $filtering_detail_material_preparation = DetailMaterialPreparationFabric::select('last_locator_id')
            ->where([
                ['warehouse_id',$warehouse_id],
            ])
            ->where('item_code','LIKE',"%$filter_item_code%")
            ->groupby('last_locator_id');


            $filtering_locator = $filtering_detail_material_preparation->union($filtering_locator)
            ->get()
            ->toArray();
        }

       return view('fabric_locator.index',compact('q','locator_id','area','summary_filter_stock','areas','filtering_locator'));
    }

    public function dataSummary(Request $request)
    {
        if(request()->ajax()) 
        {
            $locator_id = $request->locator_id;
            
            $summary = DB::table('summary_stock_locator_v')
            ->where('locator_id',$locator_id)
            ->orderby('upc','desc');
            
            return DataTables::of($summary)
            ->editColumn('total_yard',function ($summary)
            {
                return number_format($summary->total_yard, 5, '.', ',');
            })
            ->make(true);
        }
    }

    public function dataDetailPerPoSupplier(Request $request)
    {
        if(request()->ajax()) 
        {
            $locator_id = $request->locator_id;
            
            $summary = DB::table('summary_stock_supplier_locator_v')
            ->where('locator_id',$locator_id)
            ->orderby('upc','desc');
            
            return DataTables::of($summary)
            ->editColumn('total_yard',function ($summary)
            {
                return number_format($summary->total_yard, 5, '.', ',');
            })
            ->make(true);
        }
    }

    public function dataDetail(Request $request)
    {
        if(request()->ajax()) 
        {
            $locator_id = $request->locator_id;
            
            $detail = DB::table('detial_material_locator_v')
            ->where('locator_id',$locator_id)
            ->orderby('item_code','desc');
            
            return DataTables::of($detail)
            ->editColumn('available_qty',function ($detail)
            {
                return number_format($detail->available_qty, 5, '.', ',');
            })
            ->make(true);
        }
    }
}
