<?php namespace App\Http\Controllers;

use DB;
use \PDF;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
// use App\Pdf\ReportTrf;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;


use App\Models\User;
use App\Models\Lab\MasterTrfTestingLab;
use App\Models\MaterialStock;
use App\Models\Lab\TrfTesting;
use App\Models\Lab\TrfTestingDocument;
use App\Models\Lab\TrfTestingMethods;

class MasterDataFabricTestingController extends Controller
{

    public function index()
    {
        return view('master_data_fabric_testing.index');
    }

    public function data(Request $request)
    {
        if ($request->ajax())
        {

            $fabric_testing = TrfTesting::whereNull('deleted_at')
            ->where('platform', 'WMS')
            ->orderBy('created_at','desc')->get();

            // dd($fabric_testing);

            
            return DataTables::of($fabric_testing)
            ->editColumn('nik',function ($fabric_testing)
            {
                $users = User::where('nik', $fabric_testing->nik)->first();
                return $users->name;
            })
            ->editColumn('lab_location',function ($fabric_testing)
            {
                if($fabric_testing->lab_location == 'AOI1') return 'AOI 1';
                elseif($fabric_testing->lab_location == 'AOI2') return 'AOI 2';
                else return '-';
            })
            ->editColumn('date_information_remark',function ($fabric_testing)
            {
                if($fabric_testing->date_information_remark == 'buy_ready') return 'Buy Ready at '. $fabric_testing->date_information ;
                elseif($fabric_testing->date_information_remark == 'output_sewing') return '1st Output Sewing at '. $fabric_testing->date_information ;
                else return 'PODD at '. $fabric_testing->date_information ;
            })
            ->editColumn('test_required',function ($fabric_testing)
            {
                if($fabric_testing->test_required == 'developtesting_t1') return 'Develop Testing T1';
                elseif($fabric_testing->test_required == 'developtesting_t2') return 'Develop Testing T2';
                elseif($fabric_testing->test_required == 'developtesting_selective') return 'Develop Testing Selective';
                elseif($fabric_testing->test_required == 'bulktesting_m') return '1st Bulk Testing M (Model Level)';
                elseif($fabric_testing->test_required == 'bulktesting_a') return '1st Bulk Testing A (Article Level)';
                elseif($fabric_testing->test_required == 'bulktesting_selective') return '1st Bulk Testing Selective';
                elseif($fabric_testing->test_required == 'reordertesting_m') return 'Re-Order Testing M (Model Level)';
                elseif($fabric_testing->test_required == 'reordertesting_a') return 'Re-Order Testing A (Article Level)';
                elseif($fabric_testing->test_required == 'reordertesting_selective') return 'Re-Order Testing Selective';
                else return 'Re-Test';
            })
            ->addColumn('orderno',function ($fabric_testing)
            {
                $trf_document = TrfTestingDocument::where('trf_id', $fabric_testing->trf_id)->first();
                if($trf_document->document_type == "PO SUPPLIER")
                {
                    return '
                    <b>Barcode</b> : '.$trf_document->barcode_supplier.'<br>
                    <b>PO Supplier</b> : '.$trf_document->document_no.'<br>
                    <b>Supplier Name </b>    : '.$trf_document->manufacture_name.'<br>
                    <b>Item</b>     : '.$trf_document->item.'<br>
                    <b>Color</b>     : '.$trf_document->item.'<br>
                    <b>Composition</b>     : '.$trf_document->fibre_composition.'<br>
                    <b>No Roll</b>     : '.$trf_document->nomor_roll.'<br>
                    <b>Batch</b>     : '.$trf_document->batch_number.'<br>     
                    ';
                }else if($trf_document->document_type == "PO BUYER")
                {
                    return '
                    <b>PO Buyer</b> : '.$trf_document->document_no.'<br>
                    <b>Item</b>     : '.$trf_document->item.'<br>
                    <b>Composition</b>     : '.$trf_document->fibre_composition.'<br>
                    <b>Size</b> : '.$trf_document->size.'<br>
                    <b>Style</b> : '.$trf_document->style.'<br>
                    <b>Article</b> : '.$trf_document->article_no.'<br>
                    <b>Export To</b> : '.$trf_document->export_to.'<br>
                    ';
                }
            })
            ->editColumn('status',function ($fabric_testing)
            {
               
                $absensi = DB::connection('absence_aoi');
               
                if($fabric_testing->status == 'OPEN')
                {
                    return '<span class="label bg-grey-400">open</span>';
                } 
                elseif($fabric_testing->status == 'VERIFIED') 
                {
                    $user_verified = $absensi->table('adt_bbigroup_emp_new')->where('nik', $fabric_testing->verified_lab_by)->first();
                    return '<span class="label bg-blue">verified by '.$user_verified->name.'</span>';
                }
                
                elseif($fabric_testing->status == 'REJECT')
                {
                    $user_rejected =  $absensi->table('adt_bbigroup_emp_new')->where('nik', $fabric_testing->reject_by)->first();

                    return '<span class="label bg-danger">rejected by '.$user_rejected->name.'</span>';
                }
               
                elseif($fabric_testing->status == 'ONPROGRESS') 
                {
                    return '<span class="label bg-success-400">onprogress</span>';
                }
                elseif($fabric_testing->status == 'CLOSED') 
                {
                    return '<span class="label bg-danger">closed</span>';
                }
                else
                {
                    return 'eror';
                }





            })
            ->editColumn('return_test_sample',function ($fabric_testing)
            {
                if($fabric_testing->return_test_sample == 't')
                {
                    return '<span class="label bg-success">YES</span>';
                }else {
                    return '<span class="label bg-danger">NO</span>';
              
                }
            
            })
          
            // ->addColumn('testing_method_id',function ($fabric_testing)
            // {
            //     $trf_methods = TrfTestingMethods::where('trf_id', $fabric_testing->trf_id)->first();
            //     $lab = DB::connection('lab');

            //    $testings = explode(",",$fabric_testing->testing_method_id);
               
            //    $master_method = $lab->table('master_method')
            //    ->select('master_method.method_name')
            //    ->wherein('method_code', $testings)
            //    ->get();
            //    $methods = $master_method->implode('method_name', ', <br> ');
            //     //   dd($methods);
            //    return $methods; 
            
            // })
            
            
            ->addColumn('action', function($fabric_testing) {
                if($fabric_testing->status == 'OPEN' || $fabric_testing->status == 'VERIFIED' || $fabric_testing->status == 'ONPROGRESS' || $fabric_testing->last_status == 'CLOSED')
                {
                    return view('master_data_fabric_testing._action', [
                        'model'         => $fabric_testing,
                        'print_notrf'   => route('masterDataFabricTesting.barcode', $fabric_testing->id),
                        'print_detail'   => route('masterDataFabricTesting.print_detail', $fabric_testing->id),
                    ]);
                }else {
                    return view('master_data_fabric_testing._action', [
                        'model'         => $fabric_testing,
                        'print_detail'   => route('masterDataFabricTesting.print_detail', $fabric_testing->id),
                    ]);
                }
 
            })
            ->rawColumns(['status','action','return_test_sample','orderno'])
            ->make(true);
        }
    }

    public function create()
    {
        $data_lab = DB::connection('lab');
        $buyers = $data_lab->table('master_buyer')->whereNull('deleted_at')->get();

        return view('master_data_fabric_testing.create', compact('buyers'));
    }

    public function getRequirements(Request $request)
    {
        $data_lab = DB::connection('lab');
        $category_specimen = $data_lab->table('get_requirement_testings')
                            ->distinct()
                            ->select('category_specimen')
                            ->wherein('category_specimen',['FABRIC','ACCESORIES','ACCESORIES/TRIM'])
                            ->get();
        return response()->json(['response' => $category_specimen],200);
    }

  

    public function getTypeSpecimen(Request $request)
    {
        $data_lab = DB::connection('lab');
        $data = $data_lab->table('get_requirement_testings')
                            ->distinct()
                            ->select('category')
                            ->where('category_specimen', $request->category_specimen)
                            ->get();
        return response()->json(['response' => $data],200);
    }

  


    public function getPoreference(Request $request)
    {

        if(!$request->po_mo)
        {
            $data = ['null'];
        } else
        {
            $erp = DB::connection('erp');
            $po_mo          = $request->po_mo;
            $asal_specimen  = $request->asal_specimen;

            
                $getpo = $erp->table('bw_wms_testing_lab')
                ->distinct()
                ->select('poreference as po')
                ->where('poreference','like', '%'.$po_mo)
                ->first();


                $data = $erp->table('bw_wms_testing_lab')
                ->distinct()
                ->select('poreference as po','item_code')
                ->where('poreference', $getpo->po)
                ->get();

                $code = 200;
        
            
        }
        // dd($data);
        return response()->json(['response' => $data],$code);
    }

    public function getSize(Request $request)
    {

        if(!$request->po_mo)
        {
            $data = ['null'];
        } else
        {
            $erp = DB::connection('erp');
            $po_mo          = $request->po_mo;
            $asal_specimen  = $request->asal_specimen;
            $item_code      = $request->item_code;
          
            $data = $erp->table('bw_wms_testing_lab')
            ->distinct()
            ->select('size','style','article','color', 'country','description')
            ->where('poreference', $po_mo)
            ->where('item_code', $item_code)
            ->get();
            // dd($data);
            $code=200;
     

     
            
        }
        
        return response()->json(['response' => $data],$code);
    }
  
    public function getStyle(Request $request)
    {

        if(!$request->po_mo)
        {
            $data = ['null'];
        } else
        {
            $erp = DB::connection('erp');
            $po_mo          = $request->po_mo;
            $asal_specimen  = $request->asal_specimen;
            $item_code      = $request->item_code;
            $size           = $request->size;
           
            $data = $erp->table('bw_wms_testing_lab')
            ->distinct()
            ->select('size','style','article','color','country','description')
            ->where('poreference', $po_mo)
            ->where('item_code', $item_code)
            ->where('size', $size)
            ->first();

            // dd($data);


            $code=200;
           
            
            
        }
        
        return response()->json(['response' => $data],$code);
    }

   
    public function save(Request $request)
    {
        $buyer              = $request->buyer;
        $asal_specimen      = "PRODUCTION";
        $category_specimen  = $request->category_specimen;
        $category           = $request->category;
        $factory            = $request->factory;
        $test_required      = $request->test_required;
        $no_trf             = $request->no_trf;
        $radio_date         = $request->radio_date;
        $part_akan_dites    = $request->part_akan_dites;
        $po_mo              = $request->po_mo;
        $size               = $request->size;
        $style              = $request->style;
        $article            = $request->article;
        $color              = $request->color;
        $return_sample      = $request->return_sample;
        $other_buyer        = $request->other_buyer;
        $testing_date       = $request->testing_date;
        $testing_methods    = $request->testing_methods;
        $item               = $request->item;
        $destination        = $request->destination;
        $barcode_fabric     = $request->barcode_fabric;
        $document_no        = $request->document_no;
        $supplier_name      = $request->supplier_name;
        $item_code          = $request->item_code;
        $nomor_roll         = $request->nomor_roll;
        $batch              = $request->batch;
        $composition        = $request->composition;

        // dd($repetation);


        $string_testing_methods = null;   
        if($testing_methods != null)
        {
            $string_testing_methods = implode(",",$testing_methods);
        }
        // dd($asal_specimen);


        $asal_specimen_code = "D";

        if($asal_specimen = "PRODUCTION")
        {
            $asal_specimen_code = "P";
        }else{
            $asal_specimen_code = "D";
        }

        $category_specimen_code = substr($category_specimen,0,1);
        $date = date('Ymd');

        $count = TrfTesting::where('asal_specimen', $asal_specimen)
                                ->where(DB::raw("to_char(created_at,'YYYYMMDD')"),$date)
                                ->where('platform', 'WMS')
                                ->whereNull('deleted_at')
                                ->count();

        
        $count = $count + 1;
        
        $trf_number = "TRF/". $factory ."/" . $date . "-WMS-". $asal_specimen_code . $category_specimen_code . sprintf("%03s", $count);

        $has_dash       = strpos(Auth::user()->nik, "-");
        if($has_dash == false)
        {
            $nik = Auth::user()->nik;
        }else
        {
            $split = explode('-',Auth::user()->nik);
            $nik = $split[0];
        }

        if(auth::user()->warehouse =='1000001')
        {
            $factory_id = '1';
        }else {
            $factory_id = '2';
        }



        try {
            DB::beginTransaction();
            $movement_date = carbon::now();
            
            if($category_specimen == 'FABRIC')
            {
                $testing = TrfTesting::firstorCreate([
                    'trf_id'                => $trf_number,
                    'buyer'                 => $buyer,
                    'lab_location'          => $factory,
                    'nik'                   => $nik,
                    'platform'              => 'WMS',
                    'factory_id'            => $factory_id,
                    'asal_specimen'         => $asal_specimen,
                    'category_specimen'     => $category_specimen,
                    'category'              => $category,
                    'date_information'      => $testing_date,
                    'date_information_remark'=> $radio_date,
                    'test_required'         => $test_required,
                    'part_of_specimen'      => $part_akan_dites,
                    'created_at'            => $movement_date,
                    'updated_at'            => $movement_date,
                    'return_test_sample'    => $return_sample,
                    'status'                => 'OPEN',
                    'is_handover'            => false,
                    'previous_trf_id'          => $no_trf,
                ]);

                $id =  $testing->id;

                TrfTestingDocument::firstorcreate([
                    'trf_id'                => $id,
                    'document_type'         => 'PO SUPPLIER',
                    'document_no'           => $document_no,
                    'manufacture_name'      => $supplier_name,
                    'item'                  => $item_code,
                    'nomor_roll'            => $nomor_roll,
                    'batch_number'          => $batch,
                    'color'                 => $color,
                    'fibre_composition'     => $composition,
                    'barcode_supplier'     => $barcode_fabric,
                    'created_at'            => $movement_date,
                    'updated_at'            => $movement_date,
                ]);
            }else
            {
               

                TrfTesting::firstorCreate([
                    'trf_id'                => $trf_number,
                    'buyer'                 => $buyer,
                    'lab_location'          => $factory,
                    'nik'                   => $nik,
                    'platform'              => 'WMS',
                    'factory_id'            => $factory_id,
                    'asal_specimen'         => $asal_specimen,
                    'category_specimen'     => $category_specimen,
                    'category'              => $category,
                    'date_information'      => $testing_date,
                    'date_information_remark'=> $radio_date,
                    'test_required'         => $test_required,
                    'part_of_specimen'      => $part_akan_dites,
                    'created_at'            => $movement_date,
                    'updated_at'            => $movement_date,
                    'return_test_sample'    => $return_sample,
                    'status'                => 'OPEN',
                    'is_handover'            => false,
                    'previous_trf_id'          => $no_trf,
                ]);

                TrfTestingDocument::firstorcreate([
                    'trf_id'                => $trf_number,
                    'document_type'         => 'PO BUYER',
                    'document_no'           => $po_mo,
                    'size'                  => $size,
                    'item'                  => $item,
                    'style'                 => $style,
                    'article_no'            => $article,
                    'color'                 => $color,
                    'export_to'           => $destination,
                    'fibre_composition'     => $composition,
                    'created_at'            => $movement_date,
                    'updated_at'            => $movement_date,
                ]);

                

            }

            $data_response = [
                'status' => 200,
                'output' => 'insert sucess'
            ];

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                'status' => 422,
                'output' => 'insert error '.$message
            ];
        }
        
       

        return response()->json(['data' => $data_response]);

    }




    public function barcode($id)
    {
        $id = $id;
        $master_testings = TrfTesting::where('id', $id)
                            ->select('trf_id')
                            ->first();

                            // dd($master_testings);

       return view('master_data_fabric_testing.barcode', compact('master_testings'));
    }

    public function print_detail($id)
    {
        $lab = DB::connection('lab');
        $id = $id;
        $data['detil_trf'] = TrfTesting::where('id', $id)
                            ->whereNull('deleted_at')
                            ->first();
        $data['name']      = DB::table('users')->where('nik', $data['detil_trf']->nik)->select('name')->first();

        $testings = explode(",",$data['detil_trf']->testing_method_id);
               
        $data['method'] = $lab->table('master_method')
        ->select('master_method.method_name')
        ->wherein('method_code', $testings)
        ->get();
        // $methods = $master_method->implode('method_name', '\n');

        $pdf = PDF::loadView('master_data_fabric_testing/report_pdf', $data)->setPaper('A4','potrait');

        return $pdf->stream('DetailTRF.pdf');
    }

    public function edit($id){

        $data_lab = DB::connection('lab');
        $master_data_fabric_testing = DB::table('master_trf_testings')
            ->where('master_trf_testings.id', $id)
            ->first();
        $methods_id = explode(',', $master_data_fabric_testing->testing_method_id);
        // dd($master_testing);

        $master_method = $data_lab->table('master_method')
            ->select('method_name')
            ->wherein('id', $methods_id)
            ->get();
        
        
        // $getmethod = json_encode($master_method);
        $methods = $master_method->implode('method_name', ', ');; 
       

        $user = DB::table('users')
            ->where('id', $master_data_fabric_testing->user_id)
            ->first();


        // TODO
        // dd($methods);

        $category_specimen = $data_lab->table('master_requirements')
                            ->distinct()
                            ->select('master_category.category_specimen')
                            ->leftjoin('master_category', 'master_category.id', '=', 'master_requirements.master_category_id' )
                            ->where('master_category.deleted_at', null)
                            ->get();

        $category = $data_lab->table('master_category')
                            ->distinct()
                            ->select('master_category.category')
                            ->where('master_category.deleted_at', null)
                            ->get();
                    
        $list_methods = $data_lab->table('master_method')
                            ->distinct()
                            ->whereNull('deleted_at')
                            ->select('method_code','method_name', 'id')
                            ->get();

        return view('master_data_fabric_testing.edit', compact('master_data_fabric_testing', 'methods', 'user','category_specimen','category','list_methods'));
    }

    public function update(Request $request)
    {
        // dd($request);
        $date = carbon::now()->toDateTimeString();

        try{
            DB::beginTransaction();

            $methods = MasterTrfTestingModel::where('id', $request->id)
                ->update([
                    'no_trf' => $request->no_trf,
                    'factory' => $request->factory,
                    'buyer' => $request->buyer,
                    'asal_specimen' => $request->asal_specimen,
                    'category_specimen' => $request->category_specimen,
                    'type_specimen' => $request->type_specimen,
                    'test_required' => $request->test_required,
                    'previous_trf' => $request->previous_trf,
                    'category' => $request->category,
                    'part_of_specimen' => $request->part_of_specimen,
                    'po_buyer' => $request->po_buyer,
                    'mo' => $request->mo,
                    'size' => $request->size,
                    'style' => $request->style,
                    'article_no' => $request->article_no,
                    'color' => $request->color,
                    'special_request' => $request->special_request,
                    'return_test_sample' => $request->return_test_sample,
                    'update_at' => $date
                ]);

                // 'status' => $request->status,
                // 'master_testing_id' => $request->master_testing_id,
                // 'testing_method_id' => $request->testing_method_id,
                // 'user_id' => $request->user_id,

            DB::commit();
        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        // return response()->json('Upload done',200);
        return redirect()->route('dashboardTrf.index');
    }

    public function getBarcodeFabric(Request $request)
    {
     

        if(!$request->barcode_fabric)
        {
            $data = ['null'];
            $status = 500;
        } else
        {
            $barcode_fabric = $request->barcode_fabric;

            $data = MaterialStock::where('barcode_supplier', $barcode_fabric)
            ->distinct()
            ->leftjoin('items', 'items.item_code','=','material_stocks.item_code')
            ->select('material_stocks.barcode_supplier','material_stocks.item_code','material_stocks.document_no', 'material_stocks.supplier_name','material_stocks.nomor_roll','material_stocks.batch_number','material_stocks.color','items.composition')
            ->whereNull('material_stocks.deleted_at')
            ->where('material_stocks.warehouse_id', auth::user()->warehouse)
            ->where('material_stocks.available_qty','>','0')
            ->first();

            if($data)
            {
                $status = 200;
            }else{
                $status = 500;
            }
        
        }
        
        return response()->json(['response' => $data],$status);
    }

   
}
