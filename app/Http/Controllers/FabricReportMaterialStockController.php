<?php namespace App\Http\Controllers;

use DB;
use Auth;
use View;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Barcode;
use App\Models\MaterialStock;
use App\Models\SummaryStockFabric;
use App\Models\MovementStockHistory;

class FabricReportMaterialStockController extends Controller
{
    public function index(Request $request)
    {
        return view('fabric_report_material_stock.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id           = ($request->warehouse? $request->warehouse : auth::user()->warehouse);
            $type_stock_erp_code    = $request->type_stock_erp_code;
            $summary_stock          = db::table('summary_fabric_v')->where('warehouse_id','LIKE',"%$warehouse_id%")->orderby('available_qty','desc');
            
            if($type_stock_erp_code) $summary_stock = $summary_stock->where('type_stock_erp_code',$type_stock_erp_code);
            
            return DataTables::of($summary_stock)
            ->editColumn('warehouse_id',function ($summary_stock)
            {
                if($summary_stock->warehouse_id == '1000001') return 'Warehouse fabric AOI 1';
                elseif($summary_stock->warehouse_id == '1000011') return 'Warehouse fabric AOI 2';
            })
            ->editColumn('qty_order',function ($summary_stock)
            {
                if($summary_stock->uom == 'PCS')
                {
                    return number_format($summary_stock->qty_order);
                }
                else
                {
                    return number_format($summary_stock->qty_order, 4, '.', ',');
                }
            })
            ->editColumn('available_qty',function ($summary_stock)
            {
            if($summary_stock->uom == 'PCS')
                {
                    return number_format($summary_stock->available_qty);
                }
                else
                {
                    return number_format($summary_stock->available_qty, 4, '.', ',');
                }
            })
            ->setRowAttr([
                'style' => function($summary_stock)
                {
                    if($summary_stock->available_qty > 0) return  'background-color: #d9ffde;';
                },
            ])
            ->addColumn('action',function($summary_stock){
                return view('fabric_report_material_stock._action',[
                    'model'             => $summary_stock,
                    'detail'            => route('fabricReportMaterialStock.detail',$summary_stock->id),
                    'recalculate'       => route('fabricReportMaterialStock.recalculate',$summary_stock->id),
                    'unlocked'          => route('fabricReportMaterialStock.unlockAll', $summary_stock->id),
                ]);
            })
            ->rawColumns(['action','style']) 
            ->make(true);
        }
    }

    public function detail($id)
    {
        $summary_stock_fabric = db::table('summary_fabric_v')->where('id',$id)->first();
        return view('fabric_report_material_stock.detail',compact('summary_stock_fabric'));
    }

    public function dataDetail(Request $request,$id)
    {
        if(request()->ajax()) 
        {
            $summar_stock_fabric_id           = $request->summar_stock_fabric_id;
            
            $material_stocks = db::table('material_stock_per_roll_fabric_v')
            ->where('summary_stock_fabric_id',$summar_stock_fabric_id);
           // ->orderby('available_qty','desc');
           
            return DataTables::of($material_stocks)
            ->editColumn('qty_order',function ($material_stocks)
            {
                if($material_stocks->uom == 'PCS')
                {
                    return number_format($material_stocks->qty_order);
                }
                else
                {
                    return number_format($material_stocks->qty_order, 4, '.', ',');
                }
            })
            ->editColumn('available_qty',function ($material_stocks)
            {
            if($material_stocks->uom == 'PCS')
                {
                    return number_format($material_stocks->available_qty);
                }
                else
                {
                    return number_format($material_stocks->available_qty, 4, '.', ',');
                }
            })
            ->editColumn('orgin_stock',function ($material_stocks)
            {
                return $material_stocks->orgin_stock.' ('.$material_stocks->source.')';
            })
            ->addColumn('action',function($material_stock)
            {
                return view('fabric_report_material_stock._action',[
                    'model'         => $material_stock,
                    'historyModal'  => route('fabricReportMaterialStock.history',$material_stock->material_stock_id),
                    'print'         => route('fabricReportMaterialStock.barcode', $material_stock->material_stock_id),
                    'unlocked'      => route('fabricReportMaterialStock.unlockDetail',$material_stock->material_stock_id),
                ]);
            })
            ->setRowAttr([
                'style' => function($material_stocks) 
                {
                    if( $material_stocks->available_qty == 0 && $material_stocks->is_reject && !$material_stocks->movement_to_locator_reject_date_from_inspect) return  'background-color: #ffdad9';
                    else if($material_stocks->available_qty == 0 && $material_stocks->is_reject_by_lot && !$material_stocks->movement_to_locator_reject_date_from_lot) return  'background-color: #ffdad9';
                    else if($material_stocks->available_qty == 0 && $material_stocks->is_short_roll && !$material_stocks->movement_to_locator_reject_date_from_short_roll) return  'background-color: #ffdad9';
                    else
                    {
                        if($material_stocks->available_qty > 0) return  'background-color: #d9ffde';
                    }
                    
                    
                },
            ])
            ->rawColumns(['action','style']) 
            ->make(true);
        }
    }

    public function history(Request $request,$id)
    {
        $movement_histories = MovementStockHistory::where('material_stock_id',$id)
        ->orderBy('movement_date','asc')
        ->get();
            

        return view('fabric_report_material_stock._history_table',compact('movement_histories'));
    }

    public function barcode(Request $request)
    {
        $material_stocks  = MaterialStock::where('id',$request->id)->get();


        foreach ($material_stocks as $key => $material_stock) 
        {
            $get_barcode        = $this->randomCode();
            $barcode            = $get_barcode->barcode;
            $referral_code      = $get_barcode->referral_code;
            $sequence           = $get_barcode->sequence;
            $_barcode           = $material_stock->barcode_supplier;

            if($_barcode == 'BELUM DI PRINT' ||$_barcode == 'BELUM DIPRINT')
            {
                $material_stock->barcode_supplier   = $barcode;
                $material_stock->referral_code      = $referral_code;
                $material_stock->sequence           = $sequence;
                $material_stock->save();
            }
        }

        return view('fabric_report_material_stock.barcode',compact('material_stocks'));
    }
    
    public function export(Request $request)
    {
        if(auth::user()->hasRole(['admin-ict-fabric','mm-staff']))
        {
            $filename = 'report_stock_fabric.csv';
        }else
        {
            $warehouse = ($request->_warehouse_id ? $request->_warehouse_id : auth::user()->warehouse);
            if($warehouse == '1000011') $filename = 'report_stock_fabric_aoi2.csv';
            else if($warehouse == '1000001') $filename = 'report_stock_fabric_aoi1.csv';
        }
    
        $file = Config::get('storage.report') . '/' . $filename;
        if(!file_exists($file)) return 'File not found';
        
        $resp = response()->download($file);

        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        // $resp->headers->set('Content-Type', 'text/xlsx');
        return $resp;
    }

    public function recalculate($id)
    {
        $summary_stock_fabric = SummaryStockFabric::find($id);
        $detail               = DB::table('summary_vs_stock_v')->where('id',$id) ->first();

        if($summary_stock_fabric && $detail)
        {
            $total_stock         = sprintf('%0.8f',$detail->detail_total_stock);
            $total_reserved_qty  = sprintf('%0.8f',$detail->detail_total_reserved_qty);
            $total_available_qty = sprintf('%0.8f',$detail->detail_total_available_qty);
            $total_arrival_qty   = sprintf('%0.8f',$detail->detail_total_arrival_qty);
            
            $summary_stock_fabric->stock         = $total_stock;
            $summary_stock_fabric->reserved_qty  = $total_reserved_qty;
            $summary_stock_fabric->available_qty = $total_available_qty;
            $summary_stock_fabric->qty_order     = $total_arrival_qty;
            $summary_stock_fabric->save();
        }
        
        return response()->json('success',200);
    }

    public function unlockAll($id)
    {
        MaterialStock::where('summary_stock_fabric_id',$id)
        ->update([
            'last_status'       => Null,
            'ip_address'        => Null,
            'last_date_used'    => Null,
            'last_user_used_id' => Null
        ]);
        return response()->json('success',200);
    }

    public function unlockDetail($id)
    {
        $material_stocks = MaterialStock::find($id);
        if($material_stocks)
        {
            $material_stocks->last_status       = Null;
            $material_stocks->ip_address        = Null;
            $material_stocks->last_date_used    = Null;
            $material_stocks->last_user_used_id = Null;
            $material_stocks->save();
            return response()->json('success',200);
        }else
        {
            return response()->json('Please contact ICT !',422);
        }
    }

    static function randomCode()
    {
        
        $referral_code  = '1S'.Carbon::now()->format('u');
        $sequence       = Barcode::where('referral_code',$referral_code)->max('sequence');

        if ($sequence == null)
            $sequence = 1;
        else
            $sequence += 1;

        Barcode::FirstOrCreate([
            'barcode'       => $referral_code.''.$sequence,
            'referral_code' => $referral_code,
            'sequence'      => $sequence,
        ]);
        
        $obj                = new stdClass();
        $obj->barcode       = $referral_code.''.$sequence;
        $obj->referral_code = $referral_code;
        $obj->sequence      = $sequence;
        return $obj;
        
    }
}
