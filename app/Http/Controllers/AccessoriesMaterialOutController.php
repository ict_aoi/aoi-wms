<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Models\Item;
use App\Models\User;
use App\Models\Area;
use App\Models\Supplier;
use App\Models\PoBuyer;
use App\Models\Locator;
use App\Models\Criteria;
use App\Models\Temporary;
use App\Models\PurchaseItem;
use App\Models\MappingStocks;
use App\Models\UomConversion;
use App\Models\MaterialStock;
use App\Models\MaterialCheck;
use App\Models\ReroutePoBuyer;
use App\Models\AllocationItem;
use App\Models\MaterialExclude;
use App\Models\MaterialArrival;
use App\Models\MaterialSubcont;
use App\Models\MaterialMovement;
use App\Models\DetailMaterialStock;
use App\Models\MaterialRequirement;
use App\Models\MaterialPreparation;
use App\Models\MaterialMovementLine;
use App\Models\DetailMaterialSubcont;
use App\Models\DetailMaterialArrival;
use App\Models\DetailMaterialPreparation;

use App\Http\Controllers\AccessoriesBarcodeMaterialAllocationController As BarcodeAllocation;
use App\Http\Controllers\AccessoriesMaterialStockApprovalController as HistoryStock;
class AccessoriesMaterialOutController extends Controller
{
    public function index(Request $request)
    {
        // $return_array = array();
        // $return_array = json_encode($return_array);
        $return_array = '[]';
        return view('accessories_material_out.index',compact('return_array'));
        // return view('errors.migration');
    }

    public function create(Request $request)
    {
        $barcode            = trim($request->barcode);
        $_warehouse_id      = $request->warehouse_id;

        $has_dash = strpos($barcode, "-");
        if($has_dash == false)
        {
            $_barcode = $barcode;
        }else
        {
            $split      = explode('-',$barcode);
            $_barcode   = $split[0];
        }

        $_temp =  $this->getDataBarcode($barcode,$_warehouse_id);
        
        if(isset($_temp->original))
            return response()->json($_temp->original,422);
        
        return response()->json($_temp,200);
    }

    public function destinationPicklist(Request $request)
    {
        $q          = strtoupper($request->q);
        $is_cancel  = strtoupper($request->is_cancel);
        $warehouese_id = $request->_warehouse_id;

        $lists = Locator::select('locators.id','code','rack','y_row','area_id')
        ->join('areas','areas.id','locators.area_id')
        ->where(function ($query) use ($q){
            $query->where('areas.name','like',"%$q%")
            ->orWhere('code','like',"%$q%")
            ->orWhere(db::raw("concat(rack,' ',y_row)"),'like',"%$q%");
        })
        ->where([
            ['areas.is_active',true],
            ['areas.is_destination',true],
            ['areas.name','<>','FREE STOCK'],
            ['areas.warehouse',$warehouese_id],
            ['locators.is_active',true],
        ])
        ->orderBy('rack','asc')
        ->orderBy('y_row','asc')
        ->paginate('10');

        return view('accessories_material_out._destination_list',compact('lists','is_cancel'));
    }

    static function getDataBarcode($barcode,$_warehouse_id)
    {
        $has_dash = strpos($barcode, "-");

        if($has_dash == false)
        {
            $_barcode = $barcode;
            $flag = 1;
        }else{
            $split = explode('-',$barcode);
            $_barcode = $split[0];
            $flag = 0;
        }

        $detail_array[] = [
            'barcode_supplier' => $barcode
        ];

        $array = array();
       
        if($_barcode =='BELUM DI PRINT') return response()->json('Barcode not printed yet.',422);

        $material_preparation = MaterialPreparation::where([
            ['barcode',strtoupper($_barcode)],
            ['warehouse',$_warehouse_id]
        ])
        ->whereNull('deleted_at')
        ->first();

        if(!$material_preparation) return response()->json('barcode not found',422);
        if($material_preparation->is_need_to_be_switch) return response()->json('Item is book to another Po buyer, please give it to admin to be prepared again(Item ini dipinjamkan untuk po buyer lain, silahkan berikan ke admin untuk di prepare ulang)..',422);
        if($material_preparation->is_moq) return response()->json('Item has MOQ on it, please make adjustments first(Item ini terkena moq, silahkan lakukan adjustment terlebih dahulu).',422);
        if($material_preparation->is_reduce) return response()->json('Qty for this item need REDUCE due reduce qty on bom, please make adjustments first(Item ini terkena reduce, silahkan lakukan adjustment terlebih dahulu).',422);
        if($material_preparation->qc_status == 'HOLD') return response()->json('The QC results for this item are HOLD, please contact QC to find out the actual results (Hasil qc item ini HOLD, silahkan hubungi QC untuk mengetahui hasil aktualnya).',422);
        if($material_preparation->qc_status == 'FULL REJECT') return response()->json('The QC results of this item are reject, the item is not used, please wait for a replacement (Hasil QC item ini ditolak, sudah tidak digunakan silahkan tunggu pengantinya).',422);

        /*if(!MaterialMovementLine::whereHas('movement',function($query)
        {
            $query->where('status','in')
            ->orWhere('status','change');
        })
        ->WhereNotNull('material_preparation_id')
        ->where('material_preparation_id',$material_preparation->id)
        ->exists())
        
        return response()->json('Please Check-in Material First',422);*/

        if($material_preparation->last_status_movement == 'out' 
            || $material_preparation->last_status_movement == 'out-subcont' 
            || $material_preparation->last_status_movement == 'out-handover'
            || $material_preparation->last_status_movement == 'reroute' 
            || $material_preparation->last_status_movement == 'cancel order' 
            || $material_preparation->last_status_movement == 'cancel item')
        {
            $destination = Locator::find($material_preparation->last_locator_id);
            return response()->json('item sudah di supplai ke '.$destination->code,422);
        }

        if($material_preparation->warehouse != $_warehouse_id)
        {
            if($material_preparation->warehouse == 1000002) $_temp      = 'Accessories AOI 1';
            else if($material_preparation->warehouse == 1000013) $_temp = 'Accessories AOI 2';

            return response()->json('Warehouse on barcode '.$_temp.', not same with your warehouse.',422);
        }

        if($material_preparation->total_carton > 1)
        {
            if($flag == 1) return response()->json('Wrong barcode, Total carton more than 1',422);
        }

        $material_preparation_id    = $material_preparation->id;
        $item_desc                  = $material_preparation->item_desc;
        $item_code                  = $material_preparation->item_code;
        $category                   = $material_preparation->category;
        $po_buyer                   = $material_preparation->po_buyer;
        $item_id                    = $material_preparation->item_id;
        $style                      = $material_preparation->style;
        $article_no                 = $material_preparation->article_no;
        $type_po                    = $material_preparation->type_po;
        $uom_conversion             = $material_preparation->uom_conversion;
        $qty_conversion             = $material_preparation->qty_conversion;
        $is_allocation              = $material_preparation->is_allocation;
        $job_order                  = $material_preparation->job_order;
        $total_carton               = $material_preparation->total_carton;
        $po_detail_id               = $material_preparation->po_detail_id;
        $is_backlog                 = $material_preparation->is_backlog;
        $last_locator_id            = $material_preparation->last_locator_id;
        $last_locator_erp_id        = $material_preparation->lastLocator->area->erp_id;
        $last_locator_code          = $material_preparation->lastLocator->code;
        $ict_log                    = $material_preparation->ict_log;
        $document_no                = $material_preparation->document_no;
        $c_order_id                 = $material_preparation->c_order_id;
        $warehouse_inv_id           = $material_preparation->warehouse;
        $flag_error                 = 0;
        $remark_error               = null;

        $material_requirement = MaterialRequirement::where([
            ['po_buyer',$po_buyer],
            ['item_id',$item_id],
            ['style',$style],
            ['article_no',$article_no]
        ])
        ->first();
        
        $is_reroute         = ReroutePoBuyer::where('old_po_buyer',$po_buyer)->exists();
        $master_po_buyer    = PoBuyer::where('po_buyer',$po_buyer)->first();
        $user               = auth::user();
        /*$sotf               = MaterialStock::where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code],
            ['warehouse_id',$warehouse_inv_id],
            ['is_running_stock',false],
            ['is_stock_on_the_fly',true],
            ['source','!=','STOCK ON THE FLY DUE REDUCE'],
        ])
        ->whereNotNull('po_buyer')
        ->first();*/

        if($material_requirement)
        {
            if($material_requirement->warehouse_id)
            {
                $warehouse_place = $material_requirement->warehouse_id;
                if($warehouse_place == '1000007') $_warehouse_place = '1000002';
                else if($warehouse_place == '1000010') $_warehouse_place = '1000013';
                else $_warehouse_place = $_warehouse_id;

                if($_warehouse_place != $_warehouse_id) $is_need_to_handover = true;
                else $is_need_to_handover = false;
            } else
            {
                $is_need_to_handover = false;
                $_warehouse_place = $_warehouse_id;
            }

            $season         = $material_requirement->season ;
            $qty_required   = $material_requirement->qty_required;
        }else
        {
            $_warehouse_place       = $_warehouse_id;
            $is_need_to_handover    = false;
            $season                 = null;
            $qty_required           = null;

            $is_cancel_item         = MaterialRequirement::where([
                ['po_buyer',$po_buyer],
                ['item_id',$item_id],
            ])
            ->exists();

            if(!$is_cancel_item)
            {
                $flag_error     += 1;
                $remark_error   .=  'Item '.$item_code.' for po buyer '.$po_buyer.' is cancel,';
            } 

            
        }
        
        if($master_po_buyer)
        {
            $statistical_date   = ($master_po_buyer)? ($master_po_buyer->statistical_date)? $master_po_buyer->statistical_date->format('d-M-Y') : null : null;
            $is_po_buyer_cancel = ($master_po_buyer->cancel_date) ? true : false;
            if($master_po_buyer->cancel_date)
            {
                $flag_error     += 1;
                $remark_error   .=  'po buyer '.$po_buyer.' is cancel order,';
            } 

        }else
        {
            $statistical_date   = null;
            $is_po_buyer_cancel = false;
        }

         //alokasi subcont / beda whs 
         $erp = DB::connection('erp');
         $sewing_place = $erp->table('bw_report_material_requirement_new_v1')->where([
             ['poreference',$po_buyer],
             ['componentid',$item_id]
         ])
         ->select('warehouse_place')
         ->first();
 
         $sp = null;
         if($sewing_place != null)
         {
             $sp = $sewing_place->warehouse_place;
         }else{
             $sp = null;
         }

        $obj                            = new StdClass();
        $obj->user_division             = $user->hasRole(['admin-ict-acc']) ? true:false;
        $obj->material_movement_line_id = null;
        $obj->material_preparation_id   = $material_preparation_id;
        $obj->_temp                     = $qty_conversion;
        $obj->document_no               = $document_no;
        $obj->c_order_id                = $c_order_id;
        $obj->item_code                 = $item_code;
        $obj->item_desc                 = $item_desc;
        $obj->type_po                   = $type_po;
        $obj->uom_conversion            = $uom_conversion;
        $obj->qty_required              = $qty_required;
        $obj->po_buyer                  = $po_buyer;
        $obj->qty_input                 = $qty_conversion;
        $obj->is_po_buyer_cancel        = $is_po_buyer_cancel;
        $obj->is_need_to_handover       = $is_need_to_handover;
        $obj->is_reroute                = $is_reroute;
        $obj->check_sotf                = false;
        $obj->is_allocation             = $is_allocation;
        $obj->job                       = $job_order;
        $obj->category                  = $category;
        $obj->article_no                = $article_no;
        $obj->style                     = $style;
        $obj->barcode                   = $_barcode;
        $obj->total_carton              = $total_carton;
        $obj->last_locator_id           = $last_locator_id;
        $obj->last_locator_erp_id       = $last_locator_erp_id;
        $obj->last_locator_code         = $last_locator_code;
        $obj->counter                   = 1;
        $obj->warehouse_place           = $_warehouse_place;
        $obj->details                   = $detail_array;
        $obj->season                    = $season;
        $obj->movement_date             = Carbon::now()->toDateTimeString();
        $obj->statistical_date          = $statistical_date;
        $obj->is_error                  = ($flag_error > 0 ? true : false);
        $obj->remark_error              = substr_replace($remark_error, '', -1);
        $obj->sewing_place              = $sp;
        $obj->note                      = '';

        
        if($is_backlog == true)
        {
          if($ict_log == null) $obj->ict_log= strtoupper('Qty '.$qty_conversion.' ('.$uom_conversion.') yang berasal dari po '.$document_no.' adalah backlog');
          else $obj->ict_log= $ict_log;
        }else
        {
          if($ict_log == null) $obj->ict_log = 'WMS NEW';
          else $obj->ict_log = $ict_log;
        }

        return $obj;
    }

    public function store(Request $request)
    {
        $concatenate = '';   

        $_warehouse_id = $request->warehouse_id;
        $cancel_order_destination = Locator::with('area')
        ->whereHas('area',function ($query) use ($_warehouse_id)
        {
            $query->where([
                ['warehouse',$_warehouse_id],
                ['name','CANCEL ORDER'],
                ['is_destination',true]
            ]);

        })
        ->first();
        
        $missing_item_from = Locator::with('area')
        ->whereHas('area',function ($query) use ($_warehouse_id)
        {
            $query->where([
                ['warehouse',$_warehouse_id],
                ['name','MISSING ITEM'],
                ['is_destination',false]
            ]);

        })
        ->first();

        $qc_destination = Locator::with('area')
        ->whereHas('area',function ($query) use ($_warehouse_id)
        {
            $query->where([
                ['warehouse',$_warehouse_id],
                ['name','QC'],
                ['is_destination',true]
            ]);

        })
        ->first();

        $reroute_destination = Locator::with('area')
        ->whereHas('area',function ($query) use ($_warehouse_id)
        {
            $query->where([
                ['warehouse',$_warehouse_id],
                ['name','REROUTE'],
                ['is_destination',true]
            ]);

        })
        ->first();

        $free_stock_destination = Locator::with('area')
        ->whereHas('area',function ($query) use ($_warehouse_id)
        {
            $query->where([
                ['warehouse',$_warehouse_id],
                ['name','FREE STOCK'],
                ['is_destination',true]
            ]);

        })
        ->first();

        $cancel_item_destination = Locator::with('area')
        ->whereHas('area',function ($query) use ($_warehouse_id)
        {
            $query->where([
                ['warehouse',$_warehouse_id],
                ['name','CANCEL ITEM'],
                ['is_destination',true]
            ]);
        })
        ->first();

        $cancel_backlog_destination = Locator::with('area')
        ->where('rack','CANCEL BACKLOG')
        ->whereHas('area',function ($query) use ($_warehouse_id)
        {
            $query->where([
                ['warehouse',$_warehouse_id],
                ['name','CANCEL'],
                ['is_destination',true]
            ]);
        })
        ->first();

        $switch_destination = Locator::with('area')
        ->whereHas('area',function ($query) use ($_warehouse_id)
        {
            $query->where([
                ['warehouse',$_warehouse_id],
                ['name','SWITCH'],
                ['is_destination',true]
            ]);
        })
        ->first();

        $inventory_erp = Locator::with('area')
        ->where([
            ['is_active',true],
            ['rack','ERP INVENTORY'],
        ])
        ->whereHas('area',function ($query) use($_warehouse_id)
        {
            $query->where('is_active',true);
            $query->where('warehouse',$_warehouse_id);
            $query->where('name','ERP INVENTORY');
        })
        ->first();

        $subcont_erp = Locator::with('area')
        ->where([
            ['is_active',true],
            ['rack','SUBCONT'],
        ])
        ->whereHas('area',function ($query) use($_warehouse_id)
        {
            $query->where('is_active',true);
            $query->where('warehouse',$_warehouse_id);
            $query->where('name','SUBCONT');
        })
        ->first();

        /*$handover_destination = Locator::with('area')
        ->whereHas('area',function ($query){
            $query->where([
                ['warehouse',auth::user()->warehouse],
                ['name','HANDOVER'],
                ['is_destination',true]
            ]);
        })
        ->first();*/

        $validator = Validator::make($request->all(), [
            'barcode_products' => 'required|not_in:[]'
        ]);
        if($validator->passes())
        {
            // dd($request->barcode_products);
            $items                          = json_decode($request->barcode_products);

            $delete_temporaries             = array();
            $delete_material_preperations   = array();
            $delete_material_movement_lines = array();
            $insert_cancel_order            = array();
            $insert_cancel_item             = array();
            $insert_subcont                 = array();

            $po_buyer                       = $request->po_buyer;
            $to_destination_id              = $request->_to_destination_id;
            $_destination                   = Locator::find($to_destination_id);
            $flag_cancel_order              = 0;
            $flag_cancel_item               = 0;
            $flag_out_switch                = 0;
            $flag_reroute                   = 0;

            if($_destination)
            {
                if($_destination->area->name != 'HANDOVER' && $_destination->area->name =='SUBCONT')
                {
                    $check_subcont  = true;
                    $check_handover = false;
                }elseif($_destination->area->name == 'HANDOVER')
                {
                    $check_handover = true;
                    $check_subcont  = false;
                }else
                {
                    $check_handover = false;
                    $check_subcont  = false;
                }
            }else
            {
                $check_handover = false;
                $check_subcont  = false;
            }

            try 
            {
                DB::beginTransaction();

                $date = date('Ym');
                $referral_code = MaterialMovement::where(DB::raw("to_char(created_at,'YYYYMM')"),$date)
                ->wherenotnull('document_no_kk')
                ->count();
                if($referral_code == null)
                {
                    $sequence = 1;
                }else{
                    $sequence = $referral_code+1;
                }
                $document_no_out = 'WMS-'.$date.'-'.sprintf("%03s", $sequence).'-ACC';
                // dd($items);

                foreach ($items as $key => $item)
                {
                    $locator_rack_source        = Locator::find($item->last_locator_id);
                    $po_buyer                   =  $item->po_buyer;
                    $material_preparation_id    = $item->material_preparation_id;
                    
                    $_material_preparation      = MaterialPreparation::find($material_preparation_id);
                    // dd($item->note);

                    if(!$_material_preparation->deleted_at)
                    {
                        $c_order_id                                     = $item->c_order_id;
                        $material_movement_line_id                      = $item->material_movement_line_id;
                        $document_no                                    = $item->document_no;
                        $po_detail_id                                   = $_material_preparation->po_detail_id;
                        $item_id                                        = $_material_preparation->item_id;
                        $item_id_source                                 = $_material_preparation->item_id_source;
                        $c_bpartner_id                                  = $_material_preparation->c_bpartner_id;
                        $uom                                            = $_material_preparation->uom_conversion;
                        $supplier_name                                  = $_material_preparation->supplier_name;
                        $item_code                                      = $item->item_code;
                        $from_location                                  = $item->last_locator_id;
                        $from_location_erp_id                           = $item->last_locator_erp_id;
                        $ict_log                                        = $item->ict_log;
                        $type_po                                        = $item->type_po;
                        $warehouse_place                                = $item->warehouse_place;
                        $qty_input                                      = sprintf('%0.8f',$item->qty_input);
                        $is_reroute                                     = $item->is_reroute;
                        $movement_date                                  = $item->movement_date;
                        $is_inserted_to_material_movement_per_size      = true;
                        $notes                                          = $item->note;
                        // dd($notes);

                        if(in_array($po_detail_id,['FREE STOCK','FREE STOCK-CELUP']))
                        {
                            $c_order_id             = 'FREE STOCK';
                            $no_packing_list        = '-';
                            $no_invoice             = '-';
                            $c_orderline_id         = '-';
                        }else
                        {
                            $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                            ->whereNull('material_roll_handover_fabric_id')
                            ->where('po_detail_id',$po_detail_id)
                            ->first();
                            
                            $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                            $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                            $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                        }

                        if($locator_rack_source)
                        {
                            $counter_minus = $locator_rack_source->counter_in;
                            if($counter_minus > 0)
                            {
                                $locator_rack_source->counter_in = $counter_minus - 1;
                                if($counter_minus - 1 == 0)
                                    $locator_rack_source->last_po_buyer = null;
                            }else
                            {
                                $locator_rack_source->counter_in = 0;
                                $locator_rack_source->last_po_buyer = null;
                            }

                            $locator_rack_source->save();
                        }
                        
                        if($cancel_backlog_destination->id == $_destination->id)
                        {
                            $_temp_destination_id                           = $to_destination_id;
                            $_temp_destination_erp_id                       = $_destination->area->erp_id;
                            $movement_status                                = 'cancel backlog';
                            $is_from_cancel_or_reroute                      = false;
                            $date_receive_on_destination                    = $movement_date;
                            $is_inserted_to_material_movement_per_size      = false;
                        }

                        // status ini yg supply ke sewing
                        if($cancel_backlog_destination->id != $_destination->id 
                            && $cancel_order_destination->id != $_destination->id 
                            && $cancel_item_destination->id != $_destination->id 
                            && $qc_destination->id != $_destination->id 
                            && $qc_destination->id != $_destination->id 
                            && $reroute_destination->id != $_destination->id)
                        {

                            $_temp_destination_id                           = $to_destination_id;
                            $_temp_destination_erp_id                       = $_destination->area->erp_id;
                            $movement_status                                = 'out';
                            $is_from_cancel_or_reroute                      = false;
                            $date_receive_on_destination                    = $movement_date;
                            
                        }
                        
                        if($qc_destination->id == $_destination->id)
                        {
                            $_temp_destination_id                           = $to_destination_id;
                            $_temp_destination_erp_id                       = $_destination->area->erp_id;
                            $movement_status                                = 'check';
                            $is_from_cancel_or_reroute                      = false;
                            $date_receive_on_destination                    = $movement_date;
                            $is_inserted_to_material_movement_per_size      = false;
                        }

                        if($check_handover == true)
                        {
                            $_temp_destination_id                       = $to_destination_id;
                            $_temp_destination_erp_id                   = $_destination->area->erp_id;
                            $movement_status                            = 'out-handover';
                            $is_from_cancel_or_reroute                  = false;
                            $date_receive_on_destination                = null;
                            $is_inserted_to_material_movement_per_size  = false;

                            $is_movement_handover_exists = MaterialMovement::where([
                                ['from_location',$inventory_erp->id],
                                ['to_destination',$inventory_erp->id],
                                ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                ['to_locator_erp_id',$inventory_erp->area->erp_id],
                                ['po_buyer',$po_buyer],
                                ['is_integrate',false],
                                ['is_active',true],
                                ['no_packing_list',$no_packing_list],
                                ['no_invoice',$no_invoice],
                                ['status','integration-to-inventory-erp'],
                            ])
                            ->first();
                            
                            if(!$is_movement_handover_exists)
                            {
                                $material_handover_movement = MaterialMovement::firstOrCreate([
                                    'from_location'         => $inventory_erp->id,
                                    'to_destination'        => $inventory_erp->id,
                                    'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                    'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                    'po_buyer'              => $po_buyer,
                                    'is_integrate'          => false,
                                    'is_active'             => true,
                                    'no_packing_list'       => $no_packing_list,
                                    'no_invoice'            => $no_invoice,
                                    'status'                => 'integration-to-inventory-erp',
                                    'created_at'            => $movement_date,
                                    'updated_at'            => $movement_date,
                                ]);
        
                                $material_handover_movement_id = $material_handover_movement->id;
                            }else
                            {
                                $is_movement_handover_exists->updated_at = $movement_date;
                                $is_movement_handover_exists->save();
        
                                $material_handover_movement_id = $is_movement_handover_exists->id;
                            }

                            if($_warehouse_id == '1000013') $_whs_handover  = 'AOI 1';
                            else if($_warehouse_id == '1000002') $_whs_handover  = 'AOI 2';

                            $integration_note = 'BARANG DIKELUARKAN PINDAH TANGAN KE '.$_whs_handover;
                            

                            $is_material_movement_line_handover_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                            ->where([
                                ['material_movement_id',$material_handover_movement_id],
                                ['material_preparation_id',$material_preparation_id],
                                ['qty_movement',-1*$qty_input],
                                ['is_integrate',false],
                                ['is_active',true],
                                ['item_id',$item_id],
                                ['warehouse_id',$_warehouse_id],
                                ['date_movement',$movement_date],
                            ])
                            ->exists();

                            if(!$is_material_movement_line_handover_exists)
                            {
                                MaterialMovementLine::Create([
                                    'material_movement_id'          => $material_handover_movement_id,
                                    'material_preparation_id'       => $material_preparation_id,
                                    'item_code'                     => $item_code,
                                    'item_id'                       => $item_id,
                                    'item_id_source'                => $item_id_source,
                                    'c_order_id'                    => $c_order_id,
                                    'c_orderline_id'                => $c_orderline_id,
                                    'c_bpartner_id'                 => $c_bpartner_id,
                                    'supplier_name'                 => $supplier_name,
                                    'type_po'                       => 2,
                                    'is_integrate'                  => false,
                                    'is_active'                     => true,
                                    'uom_movement'                  => $uom,
                                    'qty_movement'                  => -1*$qty_input,
                                    'date_movement'                 => $movement_date,
                                    'created_at'                    => $movement_date,
                                    'updated_at'                    => $movement_date,
                                    'date_receive_on_destination'   => $movement_date,
                                    'nomor_roll'                    => '-',
                                    'warehouse_id'                  => $_warehouse_id,
                                    'document_no'                   => $document_no,
                                    'note'                          => 'INTEGRASI PINDAH TANGAN, '.$integration_note,
                                    'is_active'                     => true,
                                    'user_id'                       => auth::user()->id,
                                ]);
                            }
                            
                        }
                        if($check_subcont == true)
                        {
                            $_temp_destination_id                           = $to_destination_id;
                            $_temp_destination_erp_id                       = $_destination->area->erp_id;
                            $movement_status                                = 'out-subcont';
                            $is_from_cancel_or_reroute                      = false;
                            $date_receive_on_destination                    = $movement_date;
                            echo $inventory_erp->area->erp_id;
                            $is_movement_subcont_exists = MaterialMovement::where([
                                ['from_location',$inventory_erp->id],
                                ['to_destination',$subcont_erp->id],
                                ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                ['to_locator_erp_id',$subcont_erp->area->erp_id],
                                ['po_buyer',$po_buyer],
                                ['is_integrate',false],
                                ['is_active',true],
                                ['no_packing_list',$no_packing_list],
                                ['no_invoice',$no_invoice],
                                ['status','out-subcont'],
                                ['document_no_out', $document_no_out],
                                ['document_no_kk',$notes],
                                ['document_type', 'subcont']
                            ])
                            ->first();

                            if(!$is_movement_subcont_exists)
                            {
                                $material_subcont_movement = MaterialMovement::firstOrCreate([
                                    'from_location'         => $inventory_erp->id,
                                    'to_destination'        => $subcont_erp->id,
                                    'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                    'to_locator_erp_id'     => $subcont_erp->area->erp_id,
                                    'po_buyer'              => $po_buyer,
                                    'is_integrate'          => false,
                                    'is_active'             => true,
                                    'no_packing_list'       => $no_packing_list,
                                    'no_invoice'            => $no_invoice,
                                    'status'                => 'out-subcont',
                                    'created_at'            => $movement_date,
                                    'updated_at'            => $movement_date,
                                    'document_no_out'       => $document_no_out,
                                    'document_no_kk'        => $notes,
                                    'document_type'         => 'subcont'
                                ]);
        
                                $material_subcont_movement_id = $material_subcont_movement->id;
                            }else
                            {
                                $is_movement_subcont_exists->updated_at = $movement_date;
                                $is_movement_subcont_exists->save();
        
                                $material_subcont_movement_id = $is_movement_subcont_exists->id;
                            }

                            $integration_note ='BARANG DIKELUAKAN KE SUBCONT DENGAN NO KK, '.$ict_log;

                            $is_material_movement_line_subcont_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                            ->where([
                                ['material_movement_id',$material_subcont_movement_id],
                                ['material_preparation_id',$material_preparation_id],
                                ['qty_movement',-1*$qty_input],
                                ['is_integrate',false],
                                ['is_active',true],
                                ['item_id',$item_id],
                                ['warehouse_id',$_warehouse_id],
                                ['date_movement',$movement_date],
                            ])
                            ->exists();

                            if(!$is_material_movement_line_subcont_exists)
                            {
                                MaterialMovementLine::Create([
                                    'material_movement_id'          => $material_subcont_movement_id,
                                    'material_preparation_id'       => $material_preparation_id,
                                    'item_code'                     => $item_code,
                                    'item_id'                       => $item_id,
                                    'item_id_source'                => $item_id_source,
                                    'c_order_id'                    => $c_order_id,
                                    'c_orderline_id'                => $c_orderline_id,
                                    'c_bpartner_id'                 => $c_bpartner_id,
                                    'supplier_name'                 => $supplier_name,
                                    'type_po'                       => 2,
                                    'is_integrate'                  => false,
                                    'is_active'                     => true,
                                    'uom_movement'                  => $uom,
                                    'qty_movement'                  => $qty_input,
                                    'date_movement'                 => $movement_date,
                                    'created_at'                    => $movement_date,
                                    'updated_at'                    => $movement_date,
                                    'date_receive_on_destination'   => $movement_date,
                                    'nomor_roll'                    => '-',
                                    'warehouse_id'                  => $_warehouse_id,
                                    'document_no'                   => $document_no,
                                    'note'                          => 'INTEGRASI SUBCONT, '.$integration_note,
                                    'is_active'                     => true,
                                    'user_id'                       => auth::user()->id,
                                ]);
                            }
                            
                        }

                        if($cancel_order_destination->id == $_destination->id)
                        {
                            $_temp_destination_id                           = $free_stock_destination->id;
                            $_temp_destination_erp_id                       = $_destination->area->erp_id;
                            $movement_status                                = 'cancel order';
                            $is_from_cancel_or_reroute                      = true;
                            $date_receive_on_destination                    = $movement_date;
                            $is_inserted_to_material_movement_per_size      = false;
                            

                            $is_movement_cancel_exists = MaterialMovement::where([
                                ['from_location',$inventory_erp->id],
                                ['to_destination',$inventory_erp->id],
                                ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                ['to_locator_erp_id',$inventory_erp->area->erp_id],
                                ['po_buyer',$po_buyer],
                                ['is_integrate',false],
                                ['is_active',true],
                                ['no_packing_list',$no_packing_list],
                                ['no_invoice',$no_invoice],
                                ['status','integration-to-inventory-erp'],
                            ])
                            ->first();
                            
                            if(!$is_movement_cancel_exists)
                            {
                                $material_cancel_movement = MaterialMovement::firstOrCreate([
                                    'from_location'         => $inventory_erp->id,
                                    'to_destination'        => $inventory_erp->id,
                                    'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                    'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                    'po_buyer'              => $po_buyer,
                                    'is_integrate'          => false,
                                    'is_active'             => true,
                                    'no_packing_list'       => $no_packing_list,
                                    'no_invoice'            => $no_invoice,
                                    'status'                => 'integration-to-inventory-erp',
                                    'created_at'            => $movement_date,
                                    'updated_at'            => $movement_date,
                                ]);
        
                                $material_cancel_movement_id = $material_cancel_movement->id;
                            }else
                            {
                                $is_movement_cancel_exists->updated_at = $movement_date;
                                $is_movement_cancel_exists->save();
        
                                $material_cancel_movement_id = $is_movement_cancel_exists->id;
                            }

                            $integration_note = 'BARANG DIKELUARKAN KARNA CANCEL ORDER';
                            

                            $is_material_movement_line_cancel_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                            ->where([
                                ['material_movement_id',$material_cancel_movement_id],
                                ['material_preparation_id',$material_preparation_id],
                                ['qty_movement',-1*$qty_input],
                                ['item_id',$item_id],
                                ['warehouse_id',$_warehouse_id],
                                ['is_integrate',false],
                                ['is_active',true],
                                ['date_movement',$movement_date],
                            ])
                            ->exists();

                            if(!$is_material_movement_line_cancel_exists)
                            {
                                MaterialMovementLine::Create([
                                    'material_movement_id'          => $material_cancel_movement_id,
                                    'material_preparation_id'       => $material_preparation_id,
                                    'item_code'                     => $item_code,
                                    'item_id'                       => $item_id,
                                    'item_id_source'                => $item_id_source,
                                    'c_order_id'                    => $c_order_id,
                                    'c_orderline_id'                => $c_orderline_id,
                                    'c_bpartner_id'                 => $c_bpartner_id,
                                    'supplier_name'                 => $supplier_name,
                                    'type_po'                       => 2,
                                    'is_integrate'                  => false,
                                    'uom_movement'                  => $uom,
                                    'qty_movement'                  => -1*$qty_input,
                                    'date_movement'                 => $movement_date,
                                    'created_at'                    => $movement_date,
                                    'updated_at'                    => $movement_date,
                                    'date_receive_on_destination'   => $movement_date,
                                    'nomor_roll'                    => '-',
                                    'warehouse_id'                  => $_warehouse_id,
                                    'document_no'                   => $document_no,
                                    'note'                          => 'INTEGRASI CANCEL ORDER, '.$integration_note,
                                    'is_active'                     => true,
                                    'user_id'                       => auth::user()->id,
                                ]);
                            }

                            $insert_cancel_order [] = $item->material_preparation_id;
                            $flag_cancel_order++;
                        }

                        if($cancel_item_destination->id == $_destination->id)
                        {
                            $_temp_destination_id                           = $free_stock_destination->id;
                            $_temp_destination_erp_id                       = $_destination->area->erp_id;
                            $movement_status                                = 'cancel item';
                            $is_from_cancel_or_reroute                      = true;
                            $date_receive_on_destination                    = $movement_date;
                            $is_inserted_to_material_movement_per_size      = false;


                            $is_movement_cancel_exists = MaterialMovement::where([
                                ['from_location',$inventory_erp->id],
                                ['to_destination',$inventory_erp->id],
                                ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                ['to_locator_erp_id',$inventory_erp->area->erp_id],
                                ['po_buyer',$po_buyer],
                                ['is_integrate',false],
                                ['is_active',true],
                                ['no_packing_list',$no_packing_list],
                                ['no_invoice',$no_invoice],
                                ['status','integration-to-inventory-erp'],
                            ])
                            ->first();
                            
                            if(!$is_movement_cancel_exists)
                            {
                                $material_cancel_movement = MaterialMovement::firstOrCreate([
                                    'from_location'         => $inventory_erp->id,
                                    'to_destination'        => $inventory_erp->id,
                                    'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                    'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                    'po_buyer'              => $po_buyer,
                                    'is_integrate'          => false,
                                    'is_active'             => true,
                                    'no_packing_list'       => $no_packing_list,
                                    'no_invoice'            => $no_invoice,
                                    'status'                => 'integration-to-inventory-erp',
                                    'created_at'            => $movement_date,
                                    'updated_at'            => $movement_date,
                                ]);
        
                                $material_cancel_movement_id = $material_cancel_movement->id;
                            }else
                            {
                                $is_movement_cancel_exists->updated_at = $movement_date;
                                $is_movement_cancel_exists->save();
        
                                $material_cancel_movement_id = $is_movement_cancel_exists->id;
                            }

                            $integration_note = 'BARANG DIKELUARKAN KARNA CANCEL ITEM';
                            

                            $is_material_movement_line_cancel_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                            ->where([
                                ['material_movement_id',$material_cancel_movement_id],
                                ['material_preparation_id',$material_preparation_id],
                                ['qty_movement',-1*$qty_input],
                                ['is_integrate',false],
                                ['warehouse_id',$_warehouse_id],
                                ['item_id',$item_id],
                                ['is_active',true],
                                ['date_movement',$movement_date],
                            ])
                            ->exists();
                            
                            if(!$is_material_movement_line_cancel_exists)
                            {
                                MaterialMovementLine::Create([
                                    'material_movement_id'          => $material_cancel_movement_id,
                                    'material_preparation_id'       => $material_preparation_id,
                                    'item_code'                     => $item_code,
                                    'item_id'                       => $item_id,
                                    'item_id_source'                => $item_id_source,
                                    'c_order_id'                    => $c_order_id,
                                    'c_orderline_id'                => $c_orderline_id,
                                    'c_bpartner_id'                 => $c_bpartner_id,
                                    'supplier_name'                 => $supplier_name,
                                    'type_po'                       => 2,
                                    'is_integrate'                  => false,
                                    'uom_movement'                  => $uom,
                                    'qty_movement'                  => -1*$qty_input,
                                    'date_movement'                 => $movement_date,
                                    'created_at'                    => $movement_date,
                                    'updated_at'                    => $movement_date,
                                    'date_receive_on_destination'   => $movement_date,
                                    'nomor_roll'                    => '-',
                                    'warehouse_id'                  => $_warehouse_id,
                                    'document_no'                   => $document_no,
                                    'note'                          => 'INTEGRASI CANCEL ITEM, '.$integration_note,
                                    'is_active'                     => true,
                                    'user_id'                       => auth::user()->id,
                                ]);
                            }
                            

                            $insert_cancel_item []  = $item->material_preparation_id;
                            $flag_cancel_item++;
                        }

                        if($switch_destination->id == $_destination->id)
                        {
                            $_temp_destination_id                           = $free_stock_destination->id;
                            $_temp_destination_erp_id                       = $free_stock_destination->area->erp_id;
                            $movement_status                                = 'out switch';
                            $is_from_cancel_or_reroute                      = true;
                            $is_inserted_to_material_movement_per_size      = false;
                            $date_receive_on_destination                    = $movement_date;


                            $is_movement_cancel_exists = MaterialMovement::where([
                                ['from_location',$inventory_erp->id],
                                ['to_destination',$inventory_erp->id],
                                ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                ['to_locator_erp_id',$inventory_erp->area->erp_id],
                                ['po_buyer',$po_buyer],
                                ['is_integrate',false],
                                ['is_active',true],
                                ['no_packing_list',$no_packing_list],
                                ['no_invoice',$no_invoice],
                                ['status','integration-to-inventory-erp'],
                            ])
                            ->first();
                            
                            if(!$is_movement_cancel_exists)
                            {
                                $material_cancel_movement = MaterialMovement::firstOrCreate([
                                    'from_location'         => $inventory_erp->id,
                                    'to_destination'        => $inventory_erp->id,
                                    'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                    'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                    'po_buyer'              => $po_buyer,
                                    'is_integrate'          => false,
                                    'is_active'             => true,
                                    'no_packing_list'       => $no_packing_list,
                                    'no_invoice'            => $no_invoice,
                                    'status'                => 'integration-to-inventory-erp',
                                    'created_at'            => $movement_date,
                                    'updated_at'            => $movement_date,
                                ]);
        
                                $material_cancel_movement_id = $material_cancel_movement->id;
                            }else
                            {
                                $is_movement_cancel_exists->updated_at = $movement_date;
                                $is_movement_cancel_exists->save();
        
                                $material_cancel_movement_id = $is_movement_cancel_exists->id;
                            }

                            $integration_note = 'BARANG DIKELUARKAN KARNA SWITCH';
                            

                            $is_material_movement_line_cancel_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                            ->where([
                                ['material_movement_id',$material_cancel_movement_id],
                                ['material_preparation_id',$material_preparation_id],
                                ['is_active',true],
                                ['is_integrate',false],
                                ['qty_movement',-1*$qty_input],
                                ['warehouse_id',$_warehouse_id],
                                ['item_id',$item_id],
                                ['date_movement',$movement_date],
                            ])
                            ->exists();

                            if(!$is_material_movement_line_cancel_exists)
                            {
                                MaterialMovementLine::Create([
                                    'material_movement_id'          => $material_cancel_movement_id,
                                    'material_preparation_id'       => $material_preparation_id,
                                    'item_code'                     => $item_code,
                                    'item_id'                       => $item_id,
                                    'item_id_source'                => $item_id_source,
                                    'c_order_id'                    => $c_order_id,
                                    'c_orderline_id'                => $c_orderline_id,
                                    'c_bpartner_id'                 => $c_bpartner_id,
                                    'supplier_name'                 => $supplier_name,
                                    'type_po'                       => 2,
                                    'is_active'                     => true,
                                    'is_integrate'                  => false,
                                    'uom_movement'                  => $uom,
                                    'qty_movement'                  => -1*$qty_input,
                                    'date_movement'                 => $movement_date,
                                    'created_at'                    => $movement_date,
                                    'updated_at'                    => $movement_date,
                                    'date_receive_on_destination'   => $movement_date,
                                    'nomor_roll'                    => '-',
                                    'warehouse_id'                  => $_warehouse_id,
                                    'document_no'                   => $document_no,
                                    'note'                          => 'INTEGRASI SWITCH, '.$integration_note,
                                    'is_active'                     => true,
                                    'user_id'                       => auth::user()->id,
                                ]);
                            }
                           

                            $insert_switch []               = $item->material_preparation_id;
                            $flag_out_switch++;
                        }

                        if($reroute_destination->id == $_destination->id)
                        {
                            $_temp_destination_id                           = $free_stock_destination->id;
                            $_temp_destination_erp_id                       = $free_stock_destination->area->erp_id;
                            $movement_status                                = 'reroute';
                            $is_from_cancel_or_reroute                      = true;
                            $is_inserted_to_material_movement_per_size      = false;
                            $date_receive_on_destination                    = $movement_date;

                            $insert_reroute [] = 
                            [
                                'material_preparation_id'   => $item->material_preparation_id,
                                'item_code'                 => $item->item_code,
                                'item_desc'                 => $item->item_desc,
                                'type_po'                   => $item->type_po,
                                'uom'                       => $item->uom_conversion,
                                'category'                  => $item->category,
                                'qty_carton'                => $item->total_carton,
                                'is_allocation'             => $item->is_allocation,
                                'qty_input'                 => $item->qty_input,
                                'system_note'               => 'REROUTE'
                            ];
                            $flag_reroute++;
                        }

                        if($check_subcont == false)
                        {
                            $is_movement_exists = MaterialMovement::where([
                                ['from_location',$from_location],
                                ['from_locator_erp_id',$from_location_erp_id],
                                ['to_destination',$_temp_destination_id],
                                ['to_locator_erp_id',$_temp_destination_erp_id],
                                ['po_buyer',$po_buyer],
                                ['is_integrate',false],
                                ['is_active',true],
                                ['no_packing_list',$no_packing_list],
                                ['no_invoice',$no_invoice],
                                ['status',$movement_status],
                            ])
                            ->first();
                            
                            if(!$is_movement_exists)
                            {
                                $movement_out = MaterialMovement::firstOrCreate([
                                    'from_location'         => $from_location,
                                    'from_locator_erp_id'   => $from_location_erp_id,
                                    'to_destination'        => $_temp_destination_id,
                                    'to_locator_erp_id'     => $_temp_destination_erp_id,
                                    'po_buyer'              => $po_buyer,
                                    'is_integrate'          => false,
                                    'is_active'             => true,
                                    'status'                => $movement_status,
                                    'created_at'            => $movement_date,
                                    'updated_at'            => $movement_date,
                                ]);
    
                                $material_movement_id = $movement_out->id;
                            }else
                            {
                                $is_movement_exists->updated_at = $movement_date;
                                $is_movement_exists->save();
    
                                $material_movement_id = $is_movement_exists->id;
                            }
                       

                      

                        if(($check_handover == true || $check_subcont == true ))
                        {
                            if($_warehouse_id == '1000013') $_whs_handover  = 'AOI 1';
                            else if($_warehouse_id == '1000002') $_whs_handover  = 'AOI 2';

                            if($check_handover == true) $_system_note = 'BARANG DIKELUAKAN PINDAH TANGAN KE '.$_whs_handover.', '.$ict_log;
                            else if($check_subcont == true) $_system_note = 'BARANG DIKELUAKAN KE SUBCONT, '.$ict_log;

                                
                            $insert_subcont [] = 
                            [
                                'material_movement_id'      => $material_movement_id,
                                'material_preparation_id'   => $material_preparation_id
                            ];

                        }else
                        {
                            if($is_reroute) $_system_note = 'CHECKOUT RE-ROUTE, '.$ict_log;
                            
                            if($_temp_destination_id == $cancel_backlog_destination->id) $_system_note = 'CANCEL BACKLOG, '.$ict_log;
                            else if($_temp_destination_id == $qc_destination->id) $_system_note = 'BARANG DI KELUARKAN UNTUK PENGECEKAN QC, '.$ict_log;
                            else
                            {
                                $_system_note = 'BARANG DI KELUARKAN KE INHOUSE, '.$ict_log;
                            } 

                        }

                            $is_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                            ->where([
                                'material_movement_id'      => $material_movement_id,
                                'material_preparation_id'   => $material_preparation_id,
                                'qty_movement'              => $qty_input,
                                'date_movement'             => $movement_date,
                                'item_id'                   => $item_id,
                                'warehouse_id'              => $_warehouse_id,
                                'is_integrate'              => false,
                                'is_active'                 => true,
                                'note'                      => $_system_note,
                                'user_id'                   => Auth::user()->id
                            ])
                            ->exists();
    
                            if(!$is_line_exists)
                            {
                                $new_material_movement_line = MaterialMovementLine::create([
                                    'material_movement_id'                                          => $material_movement_id,
                                    'material_preparation_id'                                       => $material_preparation_id,
                                    'item_id'                                                       => $item_id,
                                    'item_id_source'                                                => $item_id_source,
                                    'item_code'                                                     => $item_code,
                                    'type_po'                                                       => 2,
                                    'uom_movement'                                                  => $uom,
                                    'qty_movement'                                                  => $qty_input,
                                    'c_order_id'                                                    => $c_order_id,
                                    'c_orderline_id'                                                => $c_orderline_id,
                                    'c_bpartner_id'                                                 => $c_bpartner_id,
                                    'supplier_name'                                                 => $supplier_name,
                                    'nomor_roll'                                                    => '-',
                                    'warehouse_id'                                                  => $_warehouse_id,
                                    'document_no'                                                   => $document_no,
                                    'date_movement'                                                 => $movement_date,
                                    'created_at'                                                    => $movement_date,
                                    'updated_at'                                                    => $movement_date,
                                    'is_integrate'                                                  => false,
                                    'is_active'                                                     => true,
                                    'is_inserted_to_material_movement_per_size'                     => $is_inserted_to_material_movement_per_size,
                                    'parent_id'                                                     => $material_movement_line_id,
                                    'note'                                                          => $_system_note,
                                    'date_receive_on_destination'                                   => $date_receive_on_destination,
                                    'user_id'                                                       => Auth::user()->id
                                ]);
    
                                $_material_preparation->last_material_movement_line_id = $new_material_movement_line->id;
                                $_material_preparation->save();
                            }
    
                        }
                        
                       
                        Temporary::Create([
                            'barcode'           => $po_buyer,
                            'status'            => 'mrp',
                            'user_id'           => Auth::user()->id,
                            'created_at'        => $movement_date,
                            'updated_at'        => $movement_date,
                        ]);

                        $delete_material_preperations [] = 
                        [
                            'material_preparation_id'               => $material_preparation_id,
                            'is_from_cancel_or_reroute'             => $is_from_cancel_or_reroute,
                            'last_locator_id'                       => $_temp_destination_id,
                            'last_movement_status'                  => $movement_status,
                            'last_movement_date'                    => $movement_date,
                            'last_user_movement_id'                 => Auth::user()->id,
                            'ict_log'                               => $ict_log,
                        ];
                        
                        $delete_material_movement_lines [] = $material_movement_line_id;
                        $concatenate .= "'" .$material_preparation_id."',";

                    }
                }

                $concatenate = substr_replace($concatenate, '', -1);
                if($concatenate !='')
                {
                    DB::select(db::raw("SELECT * FROM delete_duplicate_movement(array[".$concatenate."]);"));
                }
                
                $this->deleteMaterialPreparation($delete_material_preperations,$_warehouse_id);
            
                if($flag_cancel_order > 0) $this->insertCancelOrder($insert_cancel_order);
                if($flag_cancel_item > 0) $this->insertCancelItem($insert_cancel_item);
                if($flag_out_switch > 0) $this->insertSwitch($insert_switch,$_warehouse_id);
                if(count($insert_subcont)>0) $this->insertSubcont($insert_subcont);
                
                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            return response()->json(200);
        }else
        {
            return response()->json('Data not found.',422);
        }
    }

    public function import()
    {
        return view('accessories_material_out.import',compact('return_array'));
    }

    public function downloadFormUpload()
    {
        return Excel::create('upload_preparation',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','MATERIAL_PREPARATION_ID');
                $sheet->setCellValue('B1','DESTINATION_ID');
                $sheet->setCellValue('C1','REMARK');
                $sheet->setColumnFormat(array(
                    'A' => '@'
                ));
            });
            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }

    public function upload(Request $request)
    {
        // perlu di cek ulang
        /*if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
              'upload_file' => 'required|mimes:xls'
            ]);

            $concatenate                    = '';   
            $path                           = $request->file('upload_file')->getRealPath();
            $data                           = Excel::selectSheets('active')->load($path,function($render){})->get();
            $result                         = array();
            $delete_temporaries             = array();
            $delete_material_preperations   = array();
            $delete_material_movement_lines = array();
            $insert_cancel_order            = array();
            $insert_cancel_item             = array();
            $insert_subcont                 = array();
            $flag_cancel_order              = 0;
            $flag_cancel_item               = 0;
            $flag_out_switch                = 0;
            $flag_reroute                   = 0;
            
            if(!empty($data) && $data->count())
            {
                try 
                {
                    DB::beginTransaction();
                    $movement_date = carbon::now()->todatetimestring();

                    foreach($data as $key => $value)
                    {
                        $material_preparation_id    = $value->material_preparation_id;
                        $destination_id             = $value->destination_id;
                        $ict_log                    = strtoupper(trim($value->remark));
                        
                        $material_preparation       = MaterialPreparation::find($material_preparation_id);
                        $warehouse_id               = $material_preparation->warehouse;
                        
                        $cancel_order_destination = Locator::with('area')
                        ->whereHas('area',function ($query) use($warehouse_id)
                        {
                            $query->where([
                                ['warehouse',$warehouse_id],
                                ['name','CANCEL ORDER'],
                                ['is_destination',true]
                            ]);
                
                        })
                        ->first();
                        
                        $missing_item_from = Locator::with('area')
                        ->whereHas('area',function ($query) use($warehouse_id)
                        {
                            $query->where([
                                ['warehouse',$warehouse_id],
                                ['name','MISSING ITEM'],
                                ['is_destination',false]
                            ]);
                
                        })
                        ->first();
                
                        $qc_destination = Locator::with('area')
                        ->whereHas('area',function ($query)use($warehouse_id)
                        {
                            $query->where([
                                ['warehouse',$warehouse_id],
                                ['name','QC'],
                                ['is_destination',true]
                            ]);
                
                        })
                        ->first();
                
                        $reroute_destination = Locator::with('area')
                        ->whereHas('area',function ($query)use($warehouse_id)
                        {
                            $query->where([
                                ['warehouse',$warehouse_id],
                                ['name','REROUTE'],
                                ['is_destination',true]
                            ]);
                
                        })
                        ->first();
                
                        $free_stock_destination = Locator::with('area')
                        ->whereHas('area',function ($query)use($warehouse_id)
                        {
                            $query->where([
                                ['warehouse',$warehouse_id],
                                ['name','FREE STOCK'],
                                ['is_destination',true]
                            ]);
                
                        })
                        ->first();
                
                        $cancel_item_destination = Locator::with('area')
                        ->whereHas('area',function ($query)use($warehouse_id)
                        {
                            $query->where([
                                ['warehouse',$warehouse_id],
                                ['name','CANCEL ITEM'],
                                ['is_destination',true]
                            ]);
                        })
                        ->first();
                
                        $cancel_backlog_destination = Locator::with('area')
                        ->where('rack','CANCEL BACKLOG')
                        ->whereHas('area',function ($query)use($warehouse_id)
                        {
                            $query->where([
                                ['warehouse',$warehouse_id],
                                ['name','CANCEL'],
                                ['is_destination',true]
                            ]);
                        })
                        ->first();
                
                        $switch_destination = Locator::with('area')
                        ->whereHas('area',function ($query)use($warehouse_id)
                        {
                            $query->where([
                                ['warehouse',$warehouse_id],
                                ['name','SWITCH'],
                                ['is_destination',true]
                            ]);
                        })
                        ->first();
                
                        $destination                = Locator::find($destination_id);

                        if($destination)
                        {
                            if($destination->area->name != 'HANDOVER' && $destination->area->name =='SUBCONT')
                            {
                                $check_subcont  = true;
                                $check_handover = false;
                            }elseif($destination->area->name == 'HANDOVER')
                            {
                                $check_handover = true;
                                $check_subcont  = false;
                            }else
                            {
                                $check_handover = false;
                                $check_subcont  = false;
                            }
                        }else
                        {
                            $check_handover = false;
                            $check_subcont  = false;
                        }

                        if(!$material_preparation->deleted_at)
                        {
                            $item_code                                  = $material_preparation->item_code;
                            $qty_input                                  = $material_preparation->qty_conversion;
                            $po_buyer                                   = $material_preparation->po_buyer;
                            $c_order_id                                 = $material_preparation->c_order_id;
                            $material_movement_line_id                  = $material_preparation->material_movement_line_id;
                            $document_no                                = $material_preparation->document_no;
                            $item_id                                    = $material_preparation->item_id;
                            $item_code                                  = $material_preparation->item_code;
                            $from_location                              = $material_preparation->last_locator_id;
                            $locator_rack_source                        = Locator::find($from_location);
                            $type_po                                    = $material_preparation->type_po;
                            $qty_input                                  = sprintf('%0.8f',$material_preparation->qty_conversion);
                            $is_reroute                                 = $material_preparation->is_reroute;
                            $is_inserted_to_material_movement_per_size  = true;

                            if($locator_rack_source)
                            {
                                $counter_minus = $locator_rack_source->counter_in;
                                if($counter_minus > 0)
                                {
                                    $locator_rack_source->counter_in = $counter_minus - 1;
                                    if($counter_minus - 1 == 0)
                                        $locator_rack_source->last_po_buyer = null;
                                }else
                                {
                                    $locator_rack_source->counter_in = 0;
                                    $locator_rack_source->last_po_buyer = null;
                                }
    
                                $locator_rack_source->save();
                            }
                            
                            if($cancel_backlog_destination->id == $destination_id)
                            {
                                $_temp_destination_id           = $destination_id;
                                $movement_status                = 'cancel backlog';
                                $is_from_cancel_or_reroute      = false;
                                $date_receive_on_destination    = $movement_date;
                            }
    
                            if($cancel_backlog_destination->id != $destination_id
                                && $cancel_order_destination->id != $destination_id
                                && $cancel_item_destination->id != $destination_id
                                && $qc_destination->id != $destination_id
                                && $qc_destination->id != $destination_id
                                && $reroute_destination->id != $destination_id)
                            {
    
                                $_temp_destination_id           = $destination_id;
                                $movement_status                = 'out';
                                $is_from_cancel_or_reroute      = false;
                                $date_receive_on_destination    = $movement_date;
                            }
    
                            if($qc_destination->id == $destination_id)
                            {
                                $_temp_destination_id           = $destination_id;
                                $movement_status                = 'check';
                                $is_from_cancel_or_reroute      = false;
                                $date_receive_on_destination    = $movement_date;
                            }
    
                            if($check_handover == true)
                            {
                                $_temp_destination_id           = $destination_id;
                                $movement_status                = 'out-handover';
                                $is_from_cancel_or_reroute      = false;
                                $date_receive_on_destination    = null;
                            }
    
                            if($check_subcont == true)
                            {
                                $_temp_destination_id           = $destination_id;
                                $movement_status                = 'out-subcont';
                                $is_from_cancel_or_reroute      = false;
                                $date_receive_on_destination    = null;
                            }
    
                            if($cancel_order_destination->id == $destination_id)
                            {
                                $_temp_destination_id           = $free_stock_destination->id;
                                $movement_status                = 'cancel order';
                                $is_from_cancel_or_reroute      = true;
                                $date_receive_on_destination    = $movement_date;
                                
                                $insert_cancel_order [] = $material_preparation_id;
                                $flag_cancel_order++;
                            }
    
                            if($cancel_item_destination->id == $destination_id)
                            {
                                $_temp_destination_id           = $free_stock_destination->id;
                                $movement_status                = 'cancel item';
                                $is_from_cancel_or_reroute      = true;
                                $date_receive_on_destination    = $movement_date;
    
                                $insert_cancel_item []  = $material_preparation_id;
                                $flag_cancel_item++;
                            }
    
                            if($switch_destination->id == $destination_id)
                            {
                                $_temp_destination_id           = $free_stock_destination->id;
                                $movement_status                = 'out switch';
                                $is_from_cancel_or_reroute      = true;
                                $date_receive_on_destination    = $movement_date;
    
                                $insert_switch []               = $material_preparation_id;
                                $flag_out_switch++;
                            }
    
                            if($reroute_destination->id == $destination_id)
                            {
                                $_temp_destination_id           = $free_stock_destination->id;
                                $movement_status                = 'reroute';
                                $is_from_cancel_or_reroute      = true;
                                $date_receive_on_destination    = $movement_date;
    
                                $insert_reroute [] = 
                                [
                                    'material_preparation_id'   => $material_preparation_id,
                                    'item_code'                 => $material_preparation->item_code,
                                    'item_desc'                 => $material_preparation->item_desc,
                                    'type_po'                   => $material_preparation->type_po,
                                    'uom'                       => $material_preparation->uom_conversion,
                                    'category'                  => $material_preparation->category,
                                    'qty_carton'                => $material_preparation->total_carton,
                                    'is_allocation'             => $material_preparation->is_allocation,
                                    'qty_input'                 => $material_preparation->qty_conversion,
                                    'system_note'               => 'REROUTE'
                                ];
                                $flag_reroute++;
                            }
    
                            $movement_out = MaterialMovement::firstOrCreate([
                                'from_location'     => $from_location,
                                'to_destination'    => $_temp_destination_id,
                                'po_buyer'          => $po_buyer,
                                'is_integrate'      => false,
                                'is_active'         => true,
                                'status'            => $movement_status,
                                'created_at'        => $movement_date,
                                'updated_at'        => $movement_date,
                            ]);
    
                            Temporary::Create([
                                'barcode'           => $po_buyer,
                                'status'            => 'mrp',
                                'user_id'           => Auth::user()->id,
                                'created_at'        => $movement_date,
                                'updated_at'        => $movement_date,
                            ]);
    
                            if(($check_handover == true || $check_subcont == true ))
                            {
                                if(auth::user()->warehouse='1000013') $_whs_handover  = 'AOI 1';
                                else if(auth::user()->warehouse='1000002') $_whs_handover  = 'AOI 2';
    
                                if($check_handover == true) $_system_note = 'BARANG DIKELUAKAN PINDAH TANGAN KE '.$_whs_handover.', '.$ict_log;
                                else if($check_subcont == true) $_system_note = 'BARANG DIKELUAKAN KE SUBCONT, '.$ict_log;
    
                                $insert_subcont [] = 
                                [
                                    'material_movement_id'      => $movement_out->id,
                                    'material_preparation_id'   => $material_preparation_id
                                ];
    
                            }else
                            {
                                if($is_reroute) $_system_note = 'CHECKOUT RE-ROUTE, '.$ict_log;
                                
                                
                                if($_temp_destination_id == $cancel_backlog_destination->id) $_system_note = 'CANCEL BACKLOG, '.$ict_log;
                                else if($_temp_destination_id == $qc_destination->id) $_system_note = 'BARANG DI KELUARKAN UNTUK PENGECEKAN QC, '.$ict_log;
                                else{
                                    $_system_note = 'BARANG DI KELUARKAN KE INHOUSE, '.$ict_log;
                                } 
    
                            }
    
                            $is_line_exists = MaterialMovementLine::where([
                                'material_movement_id'      => $movement_out->id,
                                'material_preparation_id'   => $material_preparation_id,
                                'item_code'                 => $item_code,
                                'type_po'                   => $type_po,
                                'qty_movement'              => $qty_input,
                                'date_movement'             => $movement_date,
                                'is_active'                 => true,
                                'parent_id'                 => null,
                                'note'                      => $_system_note,
                                'user_id'                   => Auth::user()->id
                            ])
                            ->exists();
    
                            if(!$is_line_exists)
                            {
                                MaterialMovementLine::firstOrCreate([
                                    'material_movement_id'        => $movement_out->id,
                                    'material_preparation_id'     => $material_preparation_id,
                                    'item_id'                     => $item_id,
                                    'item_code'                   => $item_code,
                                    'type_po'                     => $type_po,
                                    'qty_movement'                => $qty_input,
                                    'date_movement'               => $movement_date,
                                    'created_at'                  => $movement_date,
                                    'updated_at'                  => $movement_date,
                                    'is_active'                   => true,
                                    'parent_id'                   => null,
                                    'note'                        => $_system_note,
                                    'date_receive_on_destination' => $date_receive_on_destination,
                                    'user_id'                     => Auth::user()->id
                                ]);
                            }
    
                            
    
                            $delete_material_preperations [] = 
                            [
                                'material_preparation_id'   => $material_preparation_id,
                                'is_from_cancel_or_reroute' => $is_from_cancel_or_reroute,
                                'last_locator_id'           => $_temp_destination_id,
                                'last_movement_status'      => $movement_status,
                                'last_movement_date'        => $movement_date,
                                'last_user_movement_id'     => Auth::user()->id,
                                'ict_log'                   => $ict_log,
                            ];
                            
                            $obj = new stdClass();
                            $obj->barcode       = $material_preparation->barcode;
                            $obj->document_no   = $material_preparation->document_no;
                            $obj->po_buyer      = $material_preparation->po_buyer;
                            $obj->item_code     = $material_preparation->item_code;
                            $obj->uom           = $material_preparation->uom_conversion;
                            $obj->qty           = $material_preparation->qty_conversion;
                            $obj->is_error      = false;
                            $obj->status        = 'success';
                            $result []          = $obj;
                            
                            
                            $delete_material_movement_lines [] = $material_movement_line_id;
                            $concatenate .= "'" .$material_preparation_id."',";
    
                        }
                    }

                    $concatenate = substr_replace($concatenate, '', -1);
                    if($concatenate !='')
                    {
                        DB::select(db::raw("SELECT * FROM delete_duplicate_movement(array[".$concatenate."]);"));
                    }
                    
                    $this->deleteMaterialPreparation($delete_material_preperations,null); // ini belum
            
                    if($flag_cancel_order > 0) $this->insertCancelOrder($insert_cancel_order);
                    if($flag_cancel_item > 0) $this->insertCancelItem($insert_cancel_item);
                    if($flag_out_switch > 0) $this->insertSwitch($insert_switch,null);
                    if(count($insert_subcont)>0) $this->insertSubcont($insert_subcont);

                    DB::commit();
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                return response()->json($result,200);
            }
            
            return response()->json($result,200);
        }*/

        return response()->json('Import failed, please check again',422);
    }

    static function deleteMaterialPreparation($delete_material_preperations,$_warehouse_id)
    {
        $qc_destination = Locator::with('area')
        ->whereHas('area',function ($query) use ($_warehouse_id)
        {
            $query->where([
                ['warehouse',$_warehouse_id],
                ['name','QC'],
                ['is_destination',true]
            ]);

        })
        ->first();
      
        try 
        {
            DB::beginTransaction();
            
            foreach ($delete_material_preperations as $key => $value) 
            {
                $material_prepartion = MaterialPreparation::find($value['material_preparation_id']);

                if($value['last_locator_id'] != $qc_destination->id)
                {
                    $material_prepartion->deleted_at = carbon::now();

                    DetailMaterialPreparation::where('material_preparation_id',$value['material_preparation_id'])
                    ->update([
                        'deleted_at' => carbon::now(),
                        'updated_at' => carbon::now(),
                    ]);
                }

                if($value['last_locator_id'] == $qc_destination->id)
                {
                    $material_prepartion->qc_status = 'HOLD';
                    if($material_prepartion->is_from_handover == false)
                    {
                        $material_check = MaterialCheck::where('material_preparation_id',$material_prepartion->id)
                        ->where('is_from_metal_detector',false)
                        ->whereNull('deleted_at')
                        ->first();

                        if($material_check)
                        {
                            $material_check->status = 'HOLD';
                            $material_check->save();
                        }
                    }
                    else
                    {
                        $material_check = MaterialCheck::where('barcode_preparation',$material_prepartion->barcode)
                        ->where('is_from_metal_detector',false)
                        ->whereNull('deleted_at')
                        ->first();

                        if($material_check)
                        {
                            $material_check->deleted_at = Carbon::now();
                            $material_check->save();
                        }

                    }
                }

                $material_prepartion->is_from_cancel_or_reroute = $value['is_from_cancel_or_reroute'];
                $material_prepartion->last_status_movement      = $value['last_movement_status'];
                $material_prepartion->last_locator_id           = $value['last_locator_id'];
                $material_prepartion->last_movement_date        = $value['last_movement_date'];
                $material_prepartion->last_user_movement_id     = $value['last_user_movement_id'];
                $material_prepartion->ict_log                   = $value['ict_log'];
                $material_prepartion->save();
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function insertCancelOrder($insert_cancel_order)
    {
        $stock_accessories      = array();
        $detail_material_stock_ids = array();
        $upload_date            = Carbon::now()->toDateTimeString();
        $material_preparations  = MaterialPreparation::select('id','po_detail_id','last_user_movement_id','document_no','c_order_id','c_bpartner_id','supplier_name','po_buyer','item_id','item_code','type_po','warehouse','uom_conversion','is_stock_already_created','total_carton','qty_conversion','style','article_no','is_from_barcode_bom')
        ->whereIn('id',$insert_cancel_order)
        ->groupby('id','po_detail_id','document_no','c_order_id','c_bpartner_id','last_user_movement_id','supplier_name','po_buyer','item_id','item_code','type_po','warehouse','uom_conversion','is_stock_already_created','total_carton','qty_conversion','style','article_no','is_from_barcode_bom')
        ->get();
        
        try 
        {
            DB::beginTransaction();

            foreach ($material_preparations as $key => $material_preparation)
            {
                $material_preparation_id            = $material_preparation->id;
                $po_detail_id                       = $material_preparation->po_detail_id;
                $document_no                        = $material_preparation->document_no;
                $c_order_id                         = $material_preparation->c_order_id;
                $c_bpartner_id                      = $material_preparation->c_bpartner_id;
                $last_user_movement_id              = $material_preparation->last_user_movement_id;
                $po_buyer                           = $material_preparation->po_buyer;
                $item_id                            = $material_preparation->item_id;
                $item_code                          = strtoupper($material_preparation->item_code);
                $type_po                            = $material_preparation->type_po;
                $warehouse                          = $material_preparation->warehouse;
                $uom                                = $material_preparation->uom_conversion;
                $style                              = $material_preparation->style;
                $article_no                         = $material_preparation->article_no;
                $qty_carton                         = $material_preparation->total_carton;
                $is_from_barcode_bom                = $material_preparation->is_from_barcode_bom;
                $warehouse_inv_id                   = $material_preparation->warehouse;

                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_inv_id]
                ])
                ->first();
        
                $free_stock_destination = Locator::with('area')
                ->whereHas('area',function ($query) use ($warehouse_inv_id)
                {
                    $query->where([
                        ['warehouse',$warehouse_inv_id],
                        ['name','FREE STOCK'],
                        ['is_destination',true]
                    ]);
        
                })
                ->first();


                $is_stock_already_created           = MaterialStock::where([
                    ['c_order_id',$c_order_id],
                    ['po_buyer',$po_buyer],
                    ['item_code',$item_code],
                    ['warehouse_id',$warehouse_inv_id],
                    ['is_stock_on_the_fly',true],
                    ['is_running_stock',false],
                ])
                ->whereNotNull('po_buyer')
                ->exists();
                
                $qty_input                          = sprintf('%0.8f',$material_preparation->qty_conversion);
                $supplier_name                      = trim(strtoupper($material_preparation->supplier_name));

                $material_arrival           = MaterialArrival::where([
                    ['po_detail_id',$po_detail_id],
                    ['warehouse_id',$warehouse_inv_id],
                ])
                ->whereNull('material_subcont_id')
                ->first();

                $uom_source = ($material_arrival ? $material_arrival->uom : $uom);

                if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
                {
                    $c_order_id                     = 'FREE STOCK';
                    $c_bpartner_id                  = 'FREE STOCK';
                    $supplier_code                  = 'FREE STOCK';
                    $mapping_stock_id               = null;
                    $type_stock_erp_code            = '2';
                    $type_stock                     = 'REGULER';
                    
                }else
                {
                    if($is_from_barcode_bom)
                    {
                        $supplier_code              = 'FREE STOCK';
                        if($po_buyer)
                        {
                            $master_po_buyer        = PoBuyer::where('po_buyer',$po_buyer)->first();
                            $mapping_stock_id       = null;
                            $type_stock_erp_code    = ($master_po_buyer) ? $master_po_buyer->type_stock_erp_code : null;
                            $type_stock             = ($master_po_buyer) ? $master_po_buyer->type_stock : null;
                        }else
                        {
                            $mapping_stock_id       = null;
                            $type_stock_erp_code    = '2';
                            $type_stock             = 'REGULER';
                        }
                    }else
                    {
                        $_supplier                      = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                        if($_supplier) $supplier_code   = $_supplier->supplier_code;
                        else $supplier_code             = null;

                        $get_4_digit_from_beginning     = substr($document_no,0, 4);
                        $_mapping_stock_4_digt          = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                        
                        if($_mapping_stock_4_digt)
                        {
                            $mapping_stock_id           = $_mapping_stock_4_digt->id;
                            $type_stock_erp_code        = $_mapping_stock_4_digt->type_stock_erp_code;
                            $type_stock                 = $_mapping_stock_4_digt->type_stock;
                        
                        }else
                        {
                            $get_5_digit_from_beginning = substr($document_no,0, 5);
                            $_mapping_stock_5_digt      = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                            
                            if($_mapping_stock_5_digt)
                            {
                                $mapping_stock_id       = $_mapping_stock_5_digt->id;
                                $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                                $type_stock             = $_mapping_stock_5_digt->type_stock;
                            }else
                            {
                                if($po_buyer)
                                {
                                    $master_po_buyer        = PoBuyer::where('po_buyer',$po_buyer)->first();
                                    $mapping_stock_id       = null;
                                    $type_stock_erp_code    = ($master_po_buyer) ? $master_po_buyer->type_stock_erp_code : null;
                                    $type_stock             = ($master_po_buyer) ? $master_po_buyer->type_stock : null;
                                }else
                                {
                                    $mapping_stock_id       = null;
                                    $type_stock_erp_code    = null;
                                    $type_stock             = null;
                                }
                            }
                            
                        }
                    }
                } 

                $is_exists = MaterialStock::where([
                    'document_no'               => $material_preparation->document_no,
                    'c_order_id'                => $material_preparation->c_order_id,
                    'c_bpartner_id'             => $c_bpartner_id,
                    'locator_id'                => $free_stock_destination->id,
                    'po_buyer'                  => $po_buyer,
                    'item_id'                   => $item_id,
                    'item_code'                 => $item_code,
                    'type_po'                   => $type_po,
                    'warehouse_id'              => $warehouse,
                    'source'                    => 'CANCEL ORDER',
                    'uom'                       => $uom,
                    'is_material_others'        => true,
                    'is_closing_balance'        => false,
                    'is_stock_already_created'  => $is_stock_already_created,
                    'type_stock_erp_code'       =>  $type_stock_erp_code
                ])
                ->whereNull('approval_date');

                if($po_detail_id) $is_exists = $is_exists->where('po_detail_id',$po_detail_id);

                $is_exists = $is_exists->first();

                $_item                          = Item::where(db::raw('upper(item_code)'),$item_code)->first();
                $upc_item                       = ($_item)? $_item->upc : 'MASTER ITEM TIDAK DITEMUKAN';
                $item_desc                      = ($_item)? $_item->item_desc : 'MASTER ITEM TIDAK DITEMUKAN';
                $category                       = ($_item)? $_item->category : 'MASTER ITEM TIDAK DITEMUKAN';
                $color                          = ($_item)? $_item->color : 'MASTER ITEM TIDAK DITEMUKAN';

                if(!$is_exists)
                {
                    $material_stock = MaterialStock::FirstorCreate([
                        'mapping_stock_id'              => $mapping_stock_id,
                        'type_stock_erp_code'           => $type_stock_erp_code,
                        'po_detail_id'                  => $po_detail_id,
                        'type_stock'                    => $type_stock,
                        'c_order_id'                    => $c_order_id,
                        'c_bpartner_id'                 => $c_bpartner_id,
                        'supplier_code'                 => $supplier_code,
                        'supplier_name'                 => $supplier_name,
                        'document_no'                   => $document_no,
                        'po_buyer'                      => $po_buyer,
                        'locator_id'                    => $free_stock_destination->id,
                        'item_id'                       => $item_id,
                        'item_code'                     => $item_code,
                        'item_desc'                     => $item_desc,
                        'category'                      => $category,
                        'type_po'                       => $type_po,
                        'warehouse_id'                  => $warehouse,
                        'uom'                           => $uom,
                        'uom_source'                    => $uom_source,
                        'qty_carton'                    => $qty_carton,
                        'stock'                         => $qty_input,
                        'reserved_qty'                  => 0,
                        'available_qty'                 => $qty_input,
                        'is_active'                     => true,
                        'is_material_others'            => true,
                        'is_closing_balance'            => false,
                        'created_at'                    => $upload_date,
                        'updated_at'                    => $upload_date,
                        'is_stock_already_created'      => $is_stock_already_created,
                        'source'                        => 'CANCEL ORDER',
                        'upc_item'                      => $upc_item,
                        'color'                         => $color,
                        'user_id'                       => $last_user_movement_id
                    ]);
                    $material_stock_id                  = $material_stock->id;

                    $detail_material_stock = DetailMaterialStock::FirstOrCreate([
                        'material_preparation_id'   => $material_preparation_id,
                        'material_stock_id'         => $material_stock_id,
                        'remark'                    => strtoupper('CANCEL ORDER DARI PO BUYER '.$material_stock->po_buyer.', STYLE '.$style.' DAN ARTICLE '.$article_no),
                        'uom'                       => $uom,
                        'qty'                       => $qty_input,
                        'user_id'                   => $material_stock->user_id,
                        'created_at'                => $material_stock->created_at,
                        'updated_at'                => $material_stock->updated_at,
                    ]);

                    $detail_material_stock_ids [] = $detail_material_stock->id;

                }else
                {
                    $material_stock_id          = $is_exists->id;
                    $detail_material_stock = DetailMaterialStock::Create([
                        'material_preparation_id'   => $material_preparation_id,
                        'material_stock_id'         => $material_stock_id,
                        'remark'                    => strtoupper('CANCEL ORDER DARI PO BUYER '.$is_exists->po_buyer.', STYLE '.$style.' DAN ARTICLE '.$article_no),
                        'uom'                       => $uom,
                        'qty'                       => $qty_input,
                        'user_id'                   => $last_user_movement_id,
                        'created_at'                => $upload_date,
                        'updated_at'                => $upload_date,
                    ]);

                    $detail_material_stock_ids [] = $detail_material_stock->id;

                }
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        //approve stock
        foreach($detail_material_stock_ids as $key => $detail_material_stock_id)
        {
            HistoryStock::itemApprove($detail_material_stock_id, 'detail_material_stocks');
        }
    }
    
    static function insertCancelItem($insert_cancel_item)
    {
        $stock_accessories = array();
        $detail_material_stock_ids = array();
        $upload_date       = Carbon::now()->toDateTimeString();

        $material_preparations  = MaterialPreparation::select('id','po_detail_id','last_user_movement_id','document_no','c_order_id','c_bpartner_id','supplier_name','po_buyer','item_id','item_code','type_po','warehouse','uom_conversion','is_stock_already_created','total_carton','qty_conversion','style','article_no','is_from_barcode_bom')
        ->whereIn('id',$insert_cancel_item)
        ->groupby('id','po_detail_id','document_no','c_order_id','c_bpartner_id','supplier_name','last_user_movement_id','po_buyer','item_id','item_code','type_po','warehouse','uom_conversion','is_stock_already_created','total_carton','qty_conversion','style','article_no','is_from_barcode_bom')
        ->get();
        
        try 
        {
            DB::beginTransaction();
            
            foreach ($material_preparations as $key => $material_preparation)
            {
                $material_preparation_id            = $material_preparation->id;
                $document_no                        = $material_preparation->document_no;
                $po_detail_id                       = $material_preparation->po_detail_id;
                if($document_no == 'FREE STOCK')
                {
                    $c_order_id                    = 'FREE STOCK';
                }
                else
                {
                    $c_order_id                     = $material_preparation->c_order_id;
                }
                $c_bpartner_id                      = $material_preparation->c_bpartner_id;
                $po_buyer                           = $material_preparation->po_buyer;
                $item_id                            = $material_preparation->item_id;
                $last_user_movement_id              = $material_preparation->last_user_movement_id;
                $item_code                          = strtoupper($material_preparation->item_code);
                $type_po                            = $material_preparation->type_po;
                $warehouse                          = $material_preparation->warehouse;
                $uom                                = $material_preparation->uom_conversion;
                $style                              = $material_preparation->style;
                $article_no                         = $material_preparation->article_no;
                $qty_carton                         = $material_preparation->total_carton;
                $is_from_barcode_bom                = $material_preparation->is_from_barcode_bom;
                $warehouse_inv_id                   = $material_preparation->warehouse;
                $is_stock_already_created           = MaterialStock::where([
                    ['c_order_id',$c_order_id],
                    ['po_buyer',$po_buyer],
                    ['item_code',$item_code],
                    ['warehouse_id',$warehouse_inv_id],
                    ['is_stock_on_the_fly',true],
                    ['is_running_stock',false],
                ])
                ->whereNotNull('po_buyer')
                ->exists();
                //$is_stock_already_created           = $material_preparation->is_stock_already_created;
                $qty_input                          = sprintf('%0.8f',$material_preparation->qty_conversion);
                $supplier_name                      = trim(strtoupper($material_preparation->supplier_name));

                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_inv_id]
                ])
                ->first();
        
                $free_stock_destination = Locator::with('area')
                ->whereHas('area',function ($query) use($warehouse_inv_id)
                {
                    $query->where([
                        ['warehouse',$warehouse_inv_id],
                        ['name','FREE STOCK'],
                        ['is_destination',true]
                    ]);
        
                })
                ->first();

                $material_arrival           = MaterialArrival::where([
                    ['po_detail_id',$po_detail_id],
                    ['warehouse_id',$warehouse_inv_id],
                ])
                ->whereNull('material_subcont_id')
                ->first();

                $uom_source = ($material_arrival ? $material_arrival->uom : $uom);

                if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
                {
                    $c_bpartner_id                  = 'FREE STOCK';
                    $supplier_code                  = 'FREE STOCK';
                    $mapping_stock_id               = null;
                    $type_stock_erp_code            = '2';
                    $type_stock                     = 'REGULER';
                    
                }else
                {
                    if($is_from_barcode_bom)
                    {
                        $supplier_code              = 'FREE STOCK';
                        if($po_buyer)
                        {
                            $master_po_buyer        = PoBuyer::where('po_buyer',$po_buyer)->first();
                            $mapping_stock_id       = null;
                            $type_stock_erp_code    = ($master_po_buyer) ? $master_po_buyer->type_stock_erp_code : '2';
                            $type_stock             = ($master_po_buyer) ? $master_po_buyer->type_stock : 'REGULER';
                        }else
                        {
                            $mapping_stock_id       = null;
                            $type_stock_erp_code    = '2';
                            $type_stock             = 'REGULER';
                        }
                    }else
                    {
                        $_supplier                      = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                        if($_supplier) $supplier_code   = $_supplier->supplier_code;
                        else $supplier_code             = null;

                        $get_4_digit_from_beginning     = substr($document_no,0, 4);
                        $_mapping_stock_4_digt          = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                        
                        if($_mapping_stock_4_digt)
                        {
                            $mapping_stock_id           = $_mapping_stock_4_digt->id;
                            $type_stock_erp_code        = $_mapping_stock_4_digt->type_stock_erp_code;
                            $type_stock                 = $_mapping_stock_4_digt->type_stock;
                        
                        }else
                        {
                            $get_5_digit_from_beginning = substr($document_no,0, 5);
                            $_mapping_stock_5_digt      = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                            
                            if($_mapping_stock_5_digt)
                            {
                                $mapping_stock_id       = $_mapping_stock_5_digt->id;
                                $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                                $type_stock             = $_mapping_stock_5_digt->type_stock;
                            }else
                            {
                                if($po_buyer)
                                {
                                    $master_po_buyer        = PoBuyer::where('po_buyer',$po_buyer)->first();
                                    $mapping_stock_id       = null;
                                    $type_stock_erp_code    = ($master_po_buyer) ? $master_po_buyer->type_stock_erp_code : null;
                                    $type_stock             = ($master_po_buyer) ? $master_po_buyer->type_stock : null;
                                }else
                                {
                                    $mapping_stock_id       = null;
                                    $type_stock_erp_code    = null;
                                    $type_stock             = null;
                                }
                            }
                            
                        }
                    }
                } 

                $is_exists = MaterialStock::where([
                    'document_no'               => $material_preparation->document_no,
                    'c_order_id'                => $material_preparation->c_order_id,
                    'c_bpartner_id'             => $c_bpartner_id,
                    'locator_id'                => $free_stock_destination->id,
                    'po_buyer'                  => $po_buyer,
                    'item_id'                   => $item_id,
                    'item_code'                 => $item_code,
                    'type_po'                   => $type_po,
                    'warehouse_id'              => $warehouse,
                    'source'                    => 'CANCEL ITEM',
                    'uom'                       => $uom,
                    'is_material_others'        => true,
                    'is_closing_balance'        => false,
                    'is_stock_already_created'  => $is_stock_already_created,
                    'type_stock_erp_code'       => $type_stock_erp_code
                ])
                ->whereNull('approval_date');

                if($po_detail_id) $is_exists = $is_exists->where('po_detail_id',$po_detail_id);
                $is_exists = $is_exists->first();

                $_item                          = Item::where(db::raw('upper(item_code)'),$item_code)->first();
                $upc_item                       = ($_item)? $_item->upc : 'MASTER ITEM TIDAK DITEMUKAN';
                $item_desc                      = ($_item)? $_item->item_desc : 'MASTER ITEM TIDAK DITEMUKAN';
                $category                       = ($_item)? $_item->category : 'MASTER ITEM TIDAK DITEMUKAN';
                $color                          = ($_item)? $_item->color : 'MASTER ITEM TIDAK DITEMUKAN';

                if(!$is_exists)
                {
                    $material_stock = MaterialStock::FirstorCreate([
                        'mapping_stock_id'              => $mapping_stock_id,
                        'type_stock_erp_code'           => $type_stock_erp_code,
                        'type_stock'                    => $type_stock,
                        'po_detail_id'                  => $po_detail_id,
                        'c_order_id'                    => $c_order_id,
                        'c_bpartner_id'                 => $c_bpartner_id,
                        'supplier_code'                 => $supplier_code,
                        'supplier_name'                 => $supplier_name,
                        'document_no'                   => $document_no,
                        'po_buyer'                      => $po_buyer,
                        'locator_id'                    => $free_stock_destination->id,
                        'item_id'                       => $item_id,
                        'item_code'                     => $item_code,
                        'item_desc'                     => $item_desc,
                        'category'                      => $category,
                        'type_po'                       => $type_po,
                        'warehouse_id'                  => $warehouse,
                        'uom'                           => $uom,
                        'uom_source'                    => $uom_source,
                        'qty_carton'                    => $qty_carton,
                        'stock'                         => $qty_input,
                        'reserved_qty'                  => 0,
                        'available_qty'                 => $qty_input,
                        'is_active'                     => true,
                        'is_material_others'            => true,
                        'is_closing_balance'            => false,
                        'created_at'                    => $upload_date,
                        'updated_at'                    => $upload_date,
                        'is_stock_already_created'      => $is_stock_already_created,
                        'source'                        => 'CANCEL ITEM',
                        'upc_item'                      => $upc_item,
                        'color'                         => $color,
                        'user_id'                       => $last_user_movement_id
                    ]);

                    $material_stock_id                  = $material_stock->id;

                    $detail_material_stock = DetailMaterialStock::Create([
                        'material_preparation_id'   => $material_preparation_id,
                        'material_stock_id'         => $material_stock_id,
                        'remark'                    => strtoupper('CANCEL ITEM DARI PO BUYER '.$material_stock->po_buyer.', STYLE '.$style.' DAN ARTICLE '.$article_no),
                        'uom'                       => $uom,
                        'qty'                       => $qty_input,
                        'user_id'                   => $material_stock->user_id,
                        'created_at'                => $material_stock->created_at,
                        'updated_at'                => $material_stock->updated_at,
                    ]);

                    $detail_material_stock_ids [] = $detail_material_stock->id;

                }else
                {
                    $material_stock_id          = $is_exists->id;
                    $detail_material_stock      = DetailMaterialStock::Create([
                        'material_preparation_id'   => $material_preparation_id,
                        'material_stock_id'         => $material_stock_id,
                        'remark'                    => strtoupper('CANCEL ITEM DARI PO BUYER '.$is_exists->po_buyer.', STYLE '.$style.' DAN ARTICLE '.$article_no),
                        'uom'                       => $uom,
                        'qty'                       => $qty_input,
                        'user_id'                   => $last_user_movement_id,
                        'created_at'                => $upload_date,
                        'updated_at'                => $upload_date,
                    ]);

                    $detail_material_stock_ids [] = $detail_material_stock->id;

                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        //approve stock
        foreach($detail_material_stock_ids as $key => $detail_material_stock_id)
        {
            HistoryStock::itemApprove($detail_material_stock_id, 'detail_material_stocks');
        }
       
    }

    static function insertSwitch($insert_switch,$_warehouse_id)
    {
       $system = User::where([
            ['name','system'],
            ['warehouse',$_warehouse_id]
        ])
        ->first();

        $free_stock_destination = Locator::with('area')
        ->whereHas('area',function ($query) use ($_warehouse_id)
        {
            $query->where([
                ['warehouse',$_warehouse_id],
                ['name','FREE STOCK'],
                ['is_destination',true]
            ]);

        })
        ->first();

        $stock_accessories = array();
        $upload_date       = Carbon::now()->toDateTimeString();

        $material_preparations  = MaterialPreparation::select('id','po_detail_id','last_user_movement_id','document_no','c_order_id','c_bpartner_id','supplier_name','po_buyer','item_id','item_code','type_po','warehouse','uom_conversion','is_stock_already_created','total_carton','qty_conversion','style','article_no','is_from_barcode_bom')
        ->whereIn('id',$insert_switch)
        ->groupby('id','po_detail_id','document_no','c_order_id','c_bpartner_id','last_user_movement_id','supplier_name','po_buyer','item_id','item_code','type_po','warehouse','uom_conversion','is_stock_already_created','total_carton','qty_conversion','style','article_no','is_from_barcode_bom')
        ->get();
        
        try 
        {
            DB::beginTransaction();
            
            foreach ($material_preparations as $key => $material_preparation)
            {
                $material_preparation_id            = $material_preparation->id;
                $document_no                        = $material_preparation->document_no;
                $po_detail_id                       = $material_preparation->po_detail_id;
                $c_order_id                         = $material_preparation->c_order_id;
                $c_bpartner_id                      = $material_preparation->c_bpartner_id;
                $last_user_movement_id              = $material_preparation->last_user_movement_id;
                $po_buyer                           = $material_preparation->po_buyer;
                $item_id                            = $material_preparation->item_id;
                $item_code                          = strtoupper($material_preparation->item_code);
                $type_po                            = $material_preparation->type_po;
                $warehouse                          = $material_preparation->warehouse;
                $uom                                = $material_preparation->uom_conversion;
                $style                              = $material_preparation->style;
                $article_no                         = $material_preparation->article_no;
                $qty_carton                         = $material_preparation->total_carton;
                $is_from_barcode_bom                = $material_preparation->is_from_barcode_bom;
                $warehouse_inv_id                   = $material_preparation->warehouse;
                $is_stock_already_created           = MaterialStock::where([
                    ['po_buyer',$po_buyer],
                    ['item_code',$item_code],
                    ['warehouse_id',$warehouse_inv_id],
                    ['is_stock_on_the_fly',true],
                    ['is_running_stock',false],
                ])
                ->whereNotNull('po_buyer')
                ->exists();
                //$is_stock_already_created           = $material_preparation->is_stock_already_created;
                $qty_input                          = sprintf('%0.8f',$material_preparation->qty_conversion);
                $supplier_name                      = trim(strtoupper($material_preparation->supplier_name));

                if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
                {
                    $c_bpartner_id                  = 'FREE STOCK';
                    $supplier_code                  = 'FREE STOCK';
                    $mapping_stock_id               = null;
                    $type_stock_erp_code            = '2';
                    $type_stock                     = 'REGULER';
                    
                }else
                {
                    if($is_from_barcode_bom)
                    {
                        $supplier_code              = 'FREE STOCK';
                        if($po_buyer)
                        {
                            $master_po_buyer        = PoBuyer::where('po_buyer',$po_buyer)->first();
                            $mapping_stock_id       = null;
                            $type_stock_erp_code    = ($master_po_buyer) ? $master_po_buyer->type_stock_erp_code : '2';
                            $type_stock             = ($master_po_buyer) ? $master_po_buyer->type_stock : 'REGULER';
                        }else
                        {
                            $mapping_stock_id       = null;
                            $type_stock_erp_code    = '2';
                            $type_stock             = 'REGULER';
                        }
                    }else
                    {
                        $_supplier                      = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                        if($_supplier) $supplier_code   = $_supplier->supplier_code;
                        else $supplier_code             = null;

                        $get_4_digit_from_beginning     = substr($document_no,0, 4);
                        $_mapping_stock_4_digt          = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                        
                        if($_mapping_stock_4_digt)
                        {
                            $mapping_stock_id           = $_mapping_stock_4_digt->id;
                            $type_stock_erp_code        = $_mapping_stock_4_digt->type_stock_erp_code;
                            $type_stock                 = $_mapping_stock_4_digt->type_stock;
                        
                        }else
                        {
                            $get_5_digit_from_beginning = substr($document_no,0, 5);
                            $_mapping_stock_5_digt      = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                            
                            if($_mapping_stock_5_digt)
                            {
                                $mapping_stock_id       = $_mapping_stock_5_digt->id;
                                $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                                $type_stock             = $_mapping_stock_5_digt->type_stock;
                            }else
                            {
                                if($po_buyer)
                                {
                                    $master_po_buyer        = PoBuyer::where('po_buyer',$po_buyer)->first();
                                    $mapping_stock_id       = null;
                                    $type_stock_erp_code    = ($master_po_buyer) ? $master_po_buyer->type_stock_erp_code : null;
                                    $type_stock             = ($master_po_buyer) ? $master_po_buyer->type_stock : null;
                                }else
                                {
                                    $mapping_stock_id       = null;
                                    $type_stock_erp_code    = null;
                                    $type_stock             = null;
                                }
                            }
                            
                        }
                    }
                } 

                $is_exists = MaterialStock::where([
                    'document_no'               => $material_preparation->document_no,
                    'c_order_id'                => $material_preparation->c_order_id,
                    'c_bpartner_id'             => $c_bpartner_id,
                    'locator_id'                => $free_stock_destination->id,
                    'po_buyer'                  => $po_buyer,
                    'item_id'                   => $item_id,
                    'item_code'                 => $item_code,
                    'type_po'                   => $type_po,
                    'warehouse_id'              => $warehouse,
                    'source'                    => 'OUT SWITCH',
                    'uom'                       => $uom,
                    'is_material_others'        => true,
                    'is_closing_balance'        => false,
                    'is_stock_already_created'  => $is_stock_already_created,
                    'type_stock_erp_code'       => $type_stock_erp_code
                ])
                ->whereNull('approval_date');

                if($po_detail_id) $is_exists = $is_exists->where('po_detail_id',$po_detail_id);
                $is_exists = $is_exists->first();

                $_item                          = Item::where(db::raw('upper(item_code)'),$item_code)->first();
                $upc_item                       = ($_item)? $_item->upc : 'MASTER ITEM TIDAK DITEMUKAN';
                $item_desc                      = ($_item)? $_item->item_desc : 'MASTER ITEM TIDAK DITEMUKAN';
                $category                       = ($_item)? $_item->category : 'MASTER ITEM TIDAK DITEMUKAN';
                $color                          = ($_item)? $_item->color : 'MASTER ITEM TIDAK DITEMUKAN';

                if(!$is_exists)
                {
                    $material_stock = MaterialStock::FirstorCreate([
                        'mapping_stock_id'              => $mapping_stock_id,
                        'type_stock_erp_code'           => $type_stock_erp_code,
                        'type_stock'                    => $type_stock,
                        'po_detail_id'                  => $po_detail_id,
                        'c_order_id'                    => $c_order_id,
                        'c_bpartner_id'                 => $c_bpartner_id,
                        'supplier_code'                 => $supplier_code,
                        'supplier_name'                 => $supplier_name,
                        'document_no'                   => $document_no,
                        'po_buyer'                      => $po_buyer,
                        'locator_id'                    => $free_stock_destination->id,
                        'item_id'                       => $item_id,
                        'item_code'                     => $item_code,
                        'item_desc'                     => $item_desc,
                        'category'                      => $category,
                        'type_po'                       => $type_po,
                        'warehouse_id'                  => $warehouse,
                        'uom'                           => $uom,
                        'qty_carton'                    => $qty_carton,
                        'stock'                         => $qty_input,
                        'reserved_qty'                  => 0,
                        'available_qty'                 => $qty_input,
                        'is_active'                     => true,
                        'is_material_others'            => true,
                        'is_closing_balance'            => false,
                        'is_stock_already_created'      => $is_stock_already_created,
                        'source'                        => 'OUT SWITCH',
                        'upc_item'                      => $upc_item,
                        'created_at'                    => $upload_date,
                        'updated_at'                    => $upload_date,
                        'color'                         => $color,
                        'user_id'                       => Auth::user()->id
                    ]);

                    $material_stock_id                  = $material_stock->id;
                    DetailMaterialStock::Create([
                        'material_preparation_id'   => $material_preparation_id,
                        'material_stock_id'         => $material_stock_id,
                        'remark'                    => strtoupper('SWITCH DARI PO BUYER '.$material_stock->po_buyer.', STYLE '.$style.' DAN ARTICLE '.$article_no),
                        'uom'                       => $uom,
                        'qty'                       => $qty_input,
                        'user_id'                   => $material_stock->user_id,
                        'created_at'                => $material_stock->created_at,
                        'updated_at'                => $material_stock->updated_at,
                    ]);
                }else
                {
                    $material_stock_id          = $is_exists->id;
                    DetailMaterialStock::Create([
                        'material_preparation_id'   => $material_preparation_id,
                        'material_stock_id'         => $material_stock_id,
                        'remark'                    => strtoupper('SWITCH DARI PO BUYER '.$is_exists->po_buyer.', STYLE '.$style.' DAN ARTICLE '.$article_no),
                        'uom'                       => $uom,
                        'qty'                       => $qty_input,
                        'user_id'                   => $last_user_movement_id,
                        'created_at'                => $upload_date,
                        'updated_at'                => $upload_date,
                    ]);
                }


                
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
       
    }

    static function insertSubcont($insert_subcont)
    {
        try 
        {
            DB::beginTransaction();

            $material_movement_id       = array();
            $delete_material_arrivals   = array();
            $movement_date              = carbon::now()->toDateTimeString();

            foreach ($insert_subcont as $key_subcont => $data_subcont) 
            {
                $material_preparation = MaterialPreparation::find($data_subcont['material_preparation_id']);
                $material_arrival = MaterialArrival::select('qty_ordered','item_id','c_order_id','c_orderline_id')
                ->where([
                    ['item_code',$material_preparation->item_code],
                    ['c_bpartner_id',$material_preparation->c_bpartner_id],
                    ['document_no',$material_preparation->document_no],
                ])
                ->groupby('qty_ordered','item_id','c_order_id','c_orderline_id')
                ->first();

                $material_movement       = MaterialMovement::find($data_subcont['material_movement_id']);
                $warehouse_destination   = $material_movement->destinationLocator->z_column;
                $material_movement_line  = MaterialMovementLine::where([
                    ['material_movement_id',$material_movement->id],
                    ['material_preparation_id',$material_preparation->id],
                ])
                ->first();

                $material_subcont = MaterialSubcont::firstOrCreate([
                    'po_detail_id'              => $material_preparation->po_detail_id,
                    'barcode'                   => $material_preparation->barcode,
                    'item_id'                   => $material_preparation->item_id,
                    'item_code'                 => $material_preparation->item_code,
                    'po_buyer'                  => $material_preparation->po_buyer,
                    'item_desc'                 => $material_preparation->item_desc,
                    'supplier_name'             => $material_preparation->supplier_name,
                    'document_no'               => $material_preparation->document_no,
                    'c_order_id'                => $material_preparation->c_order_id,
                    'c_bpartner_id'             => $material_preparation->c_bpartner_id,
                    'warehouse_id'              => $warehouse_destination,
                    'category'                  => $material_preparation->category,
                    'uom'                       => $material_preparation->uom_conversion, // ini sudah di konversikan
                    'type_po'                   => 2,
                    'qty_carton'                => $material_preparation->total_carton,
                    'qty_ordered'               => ($material_arrival)? $material_arrival->qty_ordered : null,
                    'qty_upload'                => $material_preparation->qty_conversion, // ini sudah di konversikan
                    'is_active'                 => true,
                    'created_at'                => $movement_date,
                    'updated_at'                => $movement_date,
                    'user_id'                   => Auth::user()->id,
                    'material_movement_line_id' => $material_movement_line->id,
                ]);

                if($material_preparation->total_carton > 1)
                {
                    $cartons = ($material_preparation->total_carton);

                    for ($i=0; $i < $cartons ; $i++) 
                    {
                        DetailMaterialSubcont::Create([
                            'material_subcont_id'   => $material_subcont->id,
                            'barcode'               => $material_preparation->barcode.'-'.($i+1),
                            'qty'                   => $material_preparation->qty_conversion,
                            'user_id'               => Auth::user()->id
                        ]);
                    }

                }else
                {
                    DetailMaterialSubcont::Create([
                        'material_subcont_id'   => $material_subcont->id,
                        'barcode'               => $material_preparation->barcode,
                        'qty'                   => $material_preparation->qty_conversion,
                        'user_id'                => Auth::user()->id
                    ]);
                }


            }
            
            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

    }

    static function insertSubcontSchedular()
    {
        $material_movement_lines = MaterialMovementLine::whereHas('movement',function($query){
            $query->where('status','out-handover');
        })
        ->where('is_active',true)
        ->whereNull('date_receive_on_destination')
        ->get();

        foreach ($material_movement_lines as $material_movement_line) 
        {
            $material_preparation = MaterialPreparation::find($material_movement_line->material_preparation_id);

            if($material_preparation)
            {
                $material_arrival = MaterialArrival::select('qty_ordered','item_id','c_order_id','c_orderline_id')
                ->where([
                    ['item_id',$material_preparation->item_id],
                    ['c_order_id',$material_preparation->c_order_id],
                ])
                ->groupby('qty_ordered','item_id','c_order_id','c_orderline_id')
                ->first();

                $material_subcont = MaterialSubcont::firstOrCreate([
                    'barcode'           => $material_preparation->barcode,
                    'item_id'           => ($material_arrival)? $material_arrival->item_id : null,
                    'item_code'         => $material_preparation->item_code,
                    'po_buyer'          => $material_preparation->po_buyer,
                    'item_desc'         => $material_preparation->item_desc,
                    'supplier_name'     => $material_preparation->supplier_name,
                    'document_no'       => $material_preparation->document_no,
                    'c_order_id'        => ($material_arrival)? $material_arrival->c_order_id : null,
                    'c_orderline_id'    => ($material_arrival)? $material_arrival->c_orderline_id : null,
                    'c_bpartner_id'     => $material_preparation->c_bpartner_id,
                    'warehouse_id'      => $material_movement_line->movement->destinationLocator->z_column,
                    'category'          => $material_preparation->category,
                    'uom'               => $material_preparation->uom_conversion,
                    'type_po'           => 2,
                    'qty_carton'        => $material_preparation->total_carton,
                    'qty_ordered'       => ($material_arrival)? $material_arrival->qty_ordered : 0,
                    'qty_upload'        => $material_preparation->qty_conversion,
                    'is_active'         => true,
                    'user_id'           => 1
                ]);

                if($material_preparation->qty_carton > 1)
                {
                    $cartons = ($material_preparation->qty_carton);

                    for ($i=0; $i < $cartons ; $i++) 
                    {
                        DetailMaterialSubcont::Create([
                            'material_subcont_id'   => $material_subcont->id,
                            'barcode'               => $material_preparation->barcode.'-'.($i+1),
                            'qty'                   => $material_preparation->qty_conversion,
                            'user_id'               => 1
                        ]);
                    }

                }else
                {
                    DetailMaterialSubcont::Create([
                        'material_subcont_id'   => $material_subcont->id,
                        'barcode'               => $material_preparation->barcode,
                        'qty'                   => $material_preparation->qty_conversion,
                        'user_id'               => 1
                    ]);
                }
            }
        }
    }

    public function importCarton()
    {
        return view('accessories_material_out.import_carton');
    }
    public function downloadFormUploadCarton()
    {
        return Excel::create('upload_out_carton',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','PO_BUYER');
                $sheet->setCellValue('B1','ITEM_CODE');
                $sheet->setCellValue('C1','WAREHOUSE');
                $sheet->setCellValue('D1','REMARK');
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                    'C' => '@',
                    'D' => '@'
                ));
            });
            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }
    public function uploadCarton(Request $request)
    {
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
              'upload_file' => 'required|mimes:xls'
            ]);

            $path                           = $request->file('upload_file')->getRealPath();
            $data                           = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                try 
                {
                    DB::beginTransaction();
                    $movement_date = carbon::now()->todatetimestring();
                    $date_receive_on_destination = carbon::now()->todatetimestring();

                    foreach($data as $key => $value)
                    {
                        $item_code      = trim($value->item_code);
                        $po_buyer       = trim($value->po_buyer);
                        $warehouse_name = trim($value->warehouse);
                        $remark         = $value->remark;

                        if($warehouse_name == 'ACCESSORIES AOI 1')
                        {
                            $warehouse_id = '1000002';
                        }
                        elseif($warehouse_name == 'ACCESSORIES AOI 2')
                        {
                            $warehouse_id = '1000013';
                        }
                        else
                        {
                            $warehouse_id = null;
                        }
                        if($warehouse_id)
                        {

                            $_system_note  = $remark.',BARANG DI KELUARKAN KE PACKING '.$warehouse_name;

                            $material_preparations = MaterialPreparation::where([
                            ['po_buyer', $po_buyer],
                            ['item_code', $item_code],
                            ['warehouse', $warehouse_id],
                            ['category', 'CT']
                            ])
                            ->whereNull('deleted_at')
                            ->get();

                            $locator = Locator::where([
                                ['rack','DISTRIBUSI']
                            ])
                            ->whereHas('area',function($query) use ($warehouse_id){
                                $query->where('warehouse',$warehouse_id);
                            })
                            ->first();

                            $destination_id = $locator->id;

                            
                            if(count($material_preparations) > 0)
                            {

                                foreach($material_preparations as $key => $material_preparation)
                            {
                                $material_preparation_id = $material_preparation->id;
                                $po_detail_id            = $material_preparation->po_detail_id;
                                $item_id                 = $material_preparation->item_id;
                                $document_no             = $material_preparation->document_no;
                                $type_po                 = $material_preparation->type_po;
                                $qty_input               = $material_preparation->qty_conversion;
                                $last_locator_id         = $material_preparation->last_locator_id;
                                $from_location           = $material_preparation->last_locator_id;
                                $from_location_erp_id    = $material_preparation->lastLocator->area->erp_id;
                                $item_id_source          = $material_preparation->item_id_source;
                                $c_order_id              = $material_preparation->c_order_id;
                                $c_bpartner_id           = $material_preparation->c_bpartner_id;
                                $supplier_name           = $material_preparation->supplier_name;
                                $warehouse_id            = $material_preparation->warehouse;

                                if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                                {
                                    $c_order_id             = 'FREE STOCK';
                                    $no_packing_list        = '-';
                                    $no_invoice             = '-';
                                    $c_orderline_id         = '-';
                                }else
                                {
                                    $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                    ->whereNull('material_roll_handover_fabric_id')
                                    ->where('po_detail_id',$po_detail_id)
                                    ->first();
                                    
                                    $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                    $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                    $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                                }

                                if($material_preparation->barcode == 'BELUM DI PRINT')
                                {
                                    $get_barcode   = BarcodeAllocation::randomCode();
                                    $barcode       = $get_barcode->barcode;
                                    $referral_code = $get_barcode->referral_code;
                                    $sequence      = $get_barcode->sequence;
                                }
                                else
                                {
                                    $barcode       = $material_preparation->barcode;
                                    $referral_code = $material_preparation->referral_code;
                                    $sequence      = $material_preparation->sequence;
                                }

                                $movement_out = MaterialMovement::firstOrCreate([
                                    'from_location'       => $from_location,
                                    'to_destination'      => $destination_id,
                                    'from_locator_erp_id' => $from_location_erp_id,
                                    'to_locator_erp_id'   => $locator->area->erp_id,
                                    'po_buyer'            => $po_buyer,
                                    'is_integrate'        => false,
                                    'is_active'           => true,
                                    'status'              => 'out',
                                    'created_at'          => $movement_date,
                                    'updated_at'          => $movement_date,
                                    'no_packing_list'     => $no_packing_list,
                                    'no_invoice'          => $no_invoice,
                                ]);

                                Temporary::Create([
                                    'barcode'           => $po_buyer,
                                    'status'            => 'mrp',
                                    'user_id'           => Auth::user()->id,
                                    'created_at'        => $movement_date,
                                    'updated_at'        => $movement_date,
                                ]);

                                $is_line_exists = MaterialMovementLine::where([
                                    'material_movement_id'      => $movement_out->id,
                                    'material_preparation_id'   => $material_preparation_id,
                                    'item_code'                 => $item_code,
                                    'type_po'                   => $type_po,
                                    'qty_movement'              => $qty_input,
                                    'date_movement'             => $movement_date,
                                    'is_active'                 => true,
                                    'parent_id'                 => null,
                                    'is_integrate'              => false,
                                    'note'                      => $_system_note,
                                    'user_id'                   => Auth::user()->id
                                ])
                                ->exists();

                                if(!$is_line_exists)
                                {
                                    $material_movement_line = MaterialMovementLine::firstOrCreate([
                                        'material_movement_id'                      => $movement_out->id,
                                        'material_preparation_id'                   => $material_preparation_id,
                                        'item_id'                                   => $item_id,
                                        'item_code'                                 => $item_code,
                                        'c_order_id'                                => $c_order_id,
                                        'c_bpartner_id'                             => $c_bpartner_id,
                                        'supplier_name'                             => $supplier_name,
                                        'c_orderline_id'                            => $c_orderline_id,
                                        'type_po'                                   => $type_po,
                                        'qty_movement'                              => $qty_input,
                                        'date_movement'                             => $movement_date,
                                        'created_at'                                => $movement_date,
                                        'updated_at'                                => $movement_date,
                                        'is_integrate'                              => false,
                                        'is_active'                                 => true,
                                        'parent_id'                                 => null,
                                        'note'                                      => $_system_note,
                                        'date_receive_on_destination'               => $movement_date,
                                        'nomor_roll'                                => '-',
                                        'warehouse_id'                              => $warehouse_id,
                                        'document_no'                               => $document_no,
                                        'is_inserted_to_material_movement_per_size' => true,
                                        'user_id'                                   => Auth::user()->id
                                    ]);
                                }

                                $material_preparation->last_status_movement           = 'out';
                                $material_preparation->last_material_movement_line_id = $material_movement_line->id;
                                $material_preparation->last_movement_date             = $movement_date;
                                $material_preparation->deleted_at                     = $movement_date;
                                $material_preparation->last_locator_id                = $destination_id;
                                $material_preparation->barcode                        = $barcode;
                                $material_preparation->referral_code                  = $referral_code;
                                $material_preparation->sequence                       = $sequence;
                                $material_preparation->save();


                                $obj = new stdClass();
                                $obj->barcode       = $material_preparation->barcode;
                                $obj->document_no   = $material_preparation->document_no;
                                $obj->po_buyer      = $material_preparation->po_buyer;
                                $obj->item_code     = $material_preparation->item_code;
                                $obj->uom           = $material_preparation->uom_conversion;
                                $obj->qty           = $material_preparation->qty_conversion;
                                $obj->is_error      = false;
                                $obj->status        = 'success';
                                $result []          = $obj;

                            }
                            }
                            else
                            {
                                $obj = new stdClass();
                                $obj->barcode       = null;
                                $obj->document_no   = null;
                                $obj->po_buyer      = $po_buyer;
                                $obj->item_code     = $item_code;
                                $obj->uom           = null;
                                $obj->qty           = null;
                                $obj->is_error      = true;
                                $obj->status        = 'Data Tidak Ditemukan';
                                $result []          = $obj;
                            }
                            

                        }
                        else
                        {
                            $obj = new stdClass();
                            $obj->barcode       = null;
                            $obj->document_no   = null;
                            $obj->po_buyer      = $po_buyer;
                            $obj->item_code     = $item_code;
                            $obj->uom           = null;
                            $obj->qty           = null;
                            $obj->is_error      = true;
                            $obj->status        = 'Warehouse Tidak Ditemukan';
                            $result []          = $obj;
                        }
                    }
                    DB::commit();
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

            }
            return response()->json($result,200);
        }
        return response()->json('Import failed, please check again',422);
    }
    public function importOut()
    {
        return view('accessories_material_out.import_out');
    }
    public function downloadFormUploadOut()
    {
        return Excel::create('upload_preparation',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','PO_BUYER');
                $sheet->setCellValue('B1','ITEM_CODE');
                $sheet->setCellValue('C1','WAREHOUSE');
                $sheet->setCellValue('D1','REMARK');
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                    'C' => '@'
                ));
            });
            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }
    public function uploadOut(Request $request)
    {
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
              'upload_file' => 'required|mimes:xls'
            ]);

            $path                           = $request->file('upload_file')->getRealPath();
            $data                           = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                try 
                {
                    DB::beginTransaction();
                    $movement_date = carbon::now()->todatetimestring();
                    $date_receive_on_destination = carbon::now()->todatetimestring();

                    foreach($data as $key => $value)
                    {
                        //$material_prepartion_id = $value->material_preparation_id;
                        $item_code      = trim($value->item_code);
                        $po_buyer       = trim($value->po_buyer);
                        $warehouse_name = trim($value->warehouse);
                        $remark         = $value->remark;

                        if($warehouse_name == 'ACC AOI 1')
                        {
                            $warehouse_id = '1000002';
                        }
                        elseif($warehouse_name == 'ACC AOI 2')
                        {
                            $warehouse_id = '1000013';
                        }
                        else
                        {
                            $warehouse_id = null;
                        }


                        $_system_note  = $remark.',BARANG DI KELUARKAN KE DENGAN UPLOAD OUT ';

                        $material_preparations = MaterialPreparation::where([
                        ['po_buyer', $po_buyer],
                        ['item_code', $item_code],
                        ['warehouse', $warehouse_id]
                        ])
                        ->whereNull('deleted_at')
                        ->get();
                        
                        if(count($material_preparations) > 0)
                        {
                            foreach($material_preparations as $key => $material_preparation)
                            {

                                $warehouse_id            = $material_preparation->warehouse;

                                $locator = Locator::where([
                                    ['rack','DISTRIBUSI']
                                ])
                                ->whereHas('area',function($query) use ($warehouse_id){
                                    $query->where('warehouse',$warehouse_id);
                                })
                                ->first();

                                $destination_id = $locator->id;


                                $material_preparation_id = $material_preparation->id;
                                $po_detail_id            = $material_preparation->po_detail_id;
                                $po_buyer                = $material_preparation->po_buyer;
                                $item_code               = $material_preparation->item_code;
                                $document_no             = $material_preparation->document_no;
                                $item_id                 = $material_preparation->item_id;
                                $uom_conversion          = $material_preparation->uom_conversion;
                                $type_po                 = $material_preparation->type_po;
                                $qty_input               = $material_preparation->qty_conversion;
                                $last_locator_id         = $material_preparation->last_locator_id;
                                $from_location           = $material_preparation->last_locator_id;
                                $from_location_erp_id    = $material_preparation->lastLocator->area->erp_id;
                                $item_id_source          = $material_preparation->item_id_source;
                                $c_order_id              = $material_preparation->c_order_id;
                                $c_bpartner_id           = $material_preparation->c_bpartner_id;
                                $supplier_name           = $material_preparation->supplier_name;

                                if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                                {
                                    $c_order_id             = 'FREE STOCK';
                                    $no_packing_list        = '-';
                                    $no_invoice             = '-';
                                    $c_orderline_id         = '-';
                                }else
                                {
                                    $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                    ->whereNull('material_roll_handover_fabric_id')
                                    ->where('po_detail_id',$po_detail_id)
                                    ->first();

                                    $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                    $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                    $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                                }

                                if($material_preparation->barcode == 'BELUM DI PRINT')
                                {
                                    $get_barcode   = BarcodeAllocation::randomCode();
                                    $barcode       = $get_barcode->barcode;
                                    $referral_code = $get_barcode->referral_code;
                                    $sequence      = $get_barcode->sequence;
                                }
                                else
                                {
                                    $barcode       = $material_preparation->barcode;
                                    $referral_code = $material_preparation->referral_code;
                                    $sequence      = $material_preparation->sequence;
                                }

                                $is_movement_exists = MaterialMovement::where([
                                    ['from_location',$from_location],
                                    ['to_destination',$locator->id],
                                    ['from_locator_erp_id',$from_location_erp_id],
                                    ['to_locator_erp_id',$locator->area->erp_id],
                                    ['po_buyer',$material_preparation->po_buyer],
                                    ['is_complete_erp',false],
                                    ['is_active',true],
                                    ['no_packing_list', $no_packing_list],
                                    ['no_invoice', $no_invoice],
                                    ['is_integrate',false],
                                    ['status','out'],
                                ])
                                ->first();
                
                                if(!$is_movement_exists)
                                {
                                    $movement_out = MaterialMovement::firstOrCreate([
                                        'from_location'       => $from_location,
                                        'to_destination'      => $destination_id,
                                        'from_locator_erp_id' => $from_location_erp_id,
                                        'to_locator_erp_id'   => $locator->area->erp_id,
                                        'po_buyer'            => $po_buyer,
                                        'is_integrate'        => false,
                                        'is_active'           => true,
                                        'status'              => 'out',
                                        'created_at'          => $movement_date,
                                        'updated_at'          => $movement_date,
                                        'no_packing_list'     => $no_packing_list,
                                        'no_invoice'          => $no_invoice,
                                    ]);

                                    $material_movement_out_id = $movement_out->id;
                                }else
                                {
                                    $is_movement_exists->updated_at = $movement_date;
                                    $is_movement_exists->save();

                                    $material_movement_out_id = $is_movement_exists->id;
                                }

                                Temporary::Create([
                                    'barcode'           => $po_buyer,
                                    'status'            => 'mrp',
                                    'user_id'           => Auth::user()->id,
                                    'created_at'        => $movement_date,
                                    'updated_at'        => $movement_date,
                                ]);

                                $is_line_exists = MaterialMovementLine::where([
                                    'material_movement_id'      => $material_movement_out_id,
                                    'material_preparation_id'   => $material_preparation_id,
                                    'item_code'                 => $item_code,
                                    'type_po'                   => $type_po,
                                    'qty_movement'              => $qty_input,
                                    'date_movement'             => $movement_date,
                                    'is_active'                 => true,
                                    'is_integrate'              => false,
                                    'is_integrate'              => false,
                                    'note'                      => $_system_note,
                                    'user_id'                   => Auth::user()->id
                                ])
                                ->exists();

                                if(!$is_line_exists)
                                {
                                    $material_movement_line = MaterialMovementLine::firstOrCreate([
                                        'material_movement_id'                      => $material_movement_out_id,
                                        'material_preparation_id'                   => $material_preparation_id,
                                        'item_id'                                   => $item_id,    
                                        'item_code'                                 => $item_code,
                                        'c_order_id'                                => $c_order_id,
                                        'c_bpartner_id'                             => $c_bpartner_id,
                                        'supplier_name'                             => $supplier_name,
                                        'c_orderline_id'                            => $c_orderline_id,
                                        'type_po'                                   => $type_po,
                                        'uom_movement'                              => $uom_conversion,
                                        'qty_movement'                              => $qty_input,
                                        'date_movement'                             => $movement_date,
                                        'created_at'                                => $movement_date,
                                        'updated_at'                                => $movement_date,
                                        'is_integrate'                              => false,
                                        'is_active'                                 => true,
                                        'parent_id'                                 => null,
                                        'note'                                      => $_system_note,
                                        'date_receive_on_destination'               => $movement_date,
                                        'nomor_roll'                                => '-',
                                        'warehouse_id'                              => $warehouse_id,
                                        'document_no'                               => $document_no,
                                        'is_inserted_to_material_movement_per_size' => true,
                                        'user_id'                                   => Auth::user()->id
                                    ]);
                                }

                                $material_preparation->last_status_movement           = 'out';
                                $material_preparation->last_material_movement_line_id = $material_movement_line->id;
                                $material_preparation->last_movement_date             = $movement_date;
                                $material_preparation->deleted_at                     = $movement_date;
                                $material_preparation->last_locator_id                = $destination_id;
                                $material_preparation->barcode                        = $barcode;
                                $material_preparation->referral_code                  = $referral_code;
                                $material_preparation->sequence                       = $sequence;
                                $material_preparation->save();


                                $obj = new stdClass();
                                $obj->barcode       = $material_preparation->barcode;
                                $obj->document_no   = $material_preparation->document_no;
                                $obj->po_buyer      = $material_preparation->po_buyer;
                                $obj->item_code     = $material_preparation->item_code;
                                $obj->uom           = $material_preparation->uom_conversion;
                                $obj->qty           = $material_preparation->qty_conversion;
                                $obj->is_error      = false;
                                $obj->status        = 'success';
                                $result []          = $obj;

                                }
                        }
                        else
                        {
                            $obj = new stdClass();
                            $obj->barcode       = null;
                            $obj->document_no   = null;
                            $obj->po_buyer      = $po_buyer;
                            $obj->item_code     = $item_code;
                            $obj->uom           = null;
                            $obj->qty           = null;
                            $obj->is_error      = true;
                            $obj->status        = 'Data Tidak Ditemukan';
                            $result []          = $obj;
                        }
                    }
                    DB::commit();
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

            }
            return response()->json($result,200);
        }
        return response()->json('Import failed, please check again',422);

    }

}