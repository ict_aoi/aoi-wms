<?php
namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use StdClass;
use Validator;

use App\Models\Item;
use App\Models\Barcode;
use App\Models\PoBuyer;
use App\Models\Locator;
use App\Models\DetailMaterialStock;
use App\Models\MovementStockHistory;
use App\Models\HistoryMaterialStockOpname;
use App\Models\AutoAllocation;
use App\Models\MaterialStock;
use App\Models\HistoryAutoAllocations;
use App\Models\MaterialPreparationFabric;
use App\Models\DetailMaterialPreparationFabric;
use App\Models\PoSupplier;
use App\Http\Controllers\FabricMaterialStockApprovalController as HistoryStock;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Yajra\Datatables\Datatables;

class FabricReportMaterialBalanceMarkerController extends Controller
{
    public function index()
    {
        return view('fabric_report_material_balance_marker.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $app_env = Config::get('app.env');
            if($app_env == 'live')
            {
                $data_cdms = DB::connection('cdms');
            }

            // $cutting_date       = $request->cutting_date;
            $start_date             = ($request->start_date) ? Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d') : Carbon::today()->subDays(7);
            $end_date               = ($request->end_date) ? Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d') : Carbon::today()->addDays(7);
            
            if($request->factory_id == null){
                $data = DB::select(db::raw("SELECT * FROM get_balance_marker('$start_date','$end_date')"));
            }
            else{
                $data = DB::select(db::raw("SELECT * FROM get_balance_marker('$start_date','$end_date') where factory_id = '$request->factory_id'"));
            }

            return DataTables::of($data)
            ->editColumn('factory_id',function ($data)
            {
                if( $data->factory_id == '1') return 'Warehouse Fabric Aoi 1';
                else if ( $data->factory_id == '2') return 'Warehouse Fabric Aoi 2';
                else return '-';
            })
            ->editColumn('persen_prepare',function ($data)
            {
                if( $data->persen_prepare == null) {
                    return '-';
                }
                else {
                    return $data->persen_prepare;
                }
            })
            ->editColumn('status',function ($data)
            {
                if($data->status == 'confirm')
                {
                    return "CONFIRMED BY MM";
                }
            })
            ->editColumn('factory_id',function ($data)
            {
                if($data->factory_id == '1')
                {
                    return "AOI 1";
                }
                else
                {
                    return "AOI 2";
                }
            })
            // ->addColumn('fabric_prepared',function($data) {
                
            //     if($data->balance<0){

            //         $qty_prepared = DB::table('integration_whs_to_cutting')
            //         ->where('style',$data->style)
            //         ->where('planning_date', $data->cutting_date)
            //         ->where('article_no',$data->articleno)
            //         ->where('part_no',$data->part_no)
            //         ->sum('qty_prepared');

            //         $percentage = ($qty_prepared/$data->actual_marker_prod)*100;
            //         return number_format($percentage,2).'%';
            //     }
            //     else{
            //         return '';
            //     }
            // })
            ->addColumn('action',function($data)
            {
                if(auth::user()->hasRole('admin-ict-fabric'))
                {
                    return view('fabric_report_material_balance_marker._action',[
                        'model'   => $data,
                        'confirm' => route('fabricReportMaterialBalanceMarker.confirm',['id' => $data->id, 'part_no' => $data->part_no, 'marker_release_detail_id' => $data->marker_release_detail_id]),
                        'reject'  => route('fabricReportMaterialBalanceMarker.reject',['id' => $data->id, 'part_no' => $data->part_no, 'marker_release_detail_id' => $data->marker_release_detail_id]),
                        'confirm_save' => route('fabricReportMaterialBalanceMarker.confirmSaving',['id' => $data->id, 'part_no' => $data->part_no, 'marker_release_detail_id' => $data->marker_release_detail_id]),
                    ]);
                }
                elseif(auth::user()->hasRole('mm-staff'))
                {
                    return view('fabric_report_material_balance_marker._action',[
                        'model'   => $data,
                        'confirm' => route('fabricReportMaterialBalanceMarker.confirm',['id' => $data->id, 'part_no' => $data->part_no, 'marker_release_detail_id' => $data->marker_release_detail_id]),
                        'reject'  => route('fabricReportMaterialBalanceMarker.reject',['id' => $data->id, 'part_no' => $data->part_no, 'marker_release_detail_id' => $data->marker_release_detail_id]),
                    ]);

                }
                else
                {
                    return view('fabric_report_material_balance_marker._action',[
                        'model'   => $data,
                        'confirm_save' => route('fabricReportMaterialBalanceMarker.confirmSaving',['id' => $data->id, 'part_no' => $data->part_no, 'marker_release_detail_id' => $data->marker_release_detail_id]),
                    ]);

                }
            })
            ->setRowAttr([
                'style' => function($data)
                {
                    if(round($data->balance, 4) < 0)  return  'background-color: #fab1b1;';
                },
            ])
            ->rawColumns(['action']) 
            ->make(true);
        }
    }

    public function confirm(Request $request)
    {
        $app_env = Config::get('app.env');
        if($app_env == 'live')
        {
            $data_cdms = DB::connection('cdms');
        }
        
        
        $balance_marker               = $data_cdms->table('jaz_balance_marker')
        ->where('id', $request->id)
        ->where('part_no', $request->part_no)
        ->first();

        $qty_prepared = DB::table('integration_whs_to_cutting')
        ->where('style',$balance_marker->style)
        ->where('planning_date', $balance_marker->cutting_date)
        ->where('article_no',$balance_marker->articleno)
        ->where('part_no',$balance_marker->part_no)
        // ->whereIn('po_buyer', explode(",",$data->po_buyer))
        ->sum('qty_prepared');

        $percentage = ($qty_prepared/$balance_marker->actual_marker_prod)*100;

        if($percentage < 90){
            
            return response()->json('Qty Prepared Belum 90%!',422);
            // return response()->json(['message' => 'Qty Prepared Belum 90%!'], 422);
        }

        $data = new stdClass();
    
        $data->id           = $balance_marker->id;
        $data->marker_release_detail_id = $balance_marker->marker_release_detail_id;
        $data->cutting_date = $balance_marker->cutting_date;
        $list_po_buyer      = explode(',',$balance_marker->po_buyer);
        $po_buyer           = PoBuyer::whereIn('po_buyer', $list_po_buyer)
                            ->orderBy('promise_date', 'desc')
                            ->first();

        $qty_allocated = AutoAllocation::where([
                        ['po_buyer', $po_buyer->po_buyer],
                        ['item_id_book', $balance_marker->item_id_book],
                        ['warehouse_id', '1000011'],
                        ['is_balance_marker', true],])
                        ->whereNull('deleted_at')
                        ->sum('qty_allocation');

        $data->po_buyer       = $po_buyer->po_buyer;
        $data->lc_date        = $po_buyer->lc_date;
        $data->promise_date   = $po_buyer->promise_date;
        $data->item_code      = $balance_marker->item_code;
        $data->item_code_book = $balance_marker->item_code_book;
        $data->item_id        = $balance_marker->item_id;
        $data->item_id_book   = $balance_marker->item_id_book;
        $data->balance        = sprintf('%0.8f',($balance_marker->balance * -1) - $qty_allocated);
        $data->remark         = $balance_marker->remark;
        $data->planning_date  = $balance_marker->cutting_date;
        $data->style          = $balance_marker->style;
        $data->articleno      = $balance_marker->articleno;
        return response()->json($data, 200);

    }

    public function confirmSaving(Request $request)
    {
        $app_env = Config::get('app.env');
        if($app_env == 'live')
        {
            $data_cdms = DB::connection('cdms');
        }
        
        
        $saving               = $data_cdms->table('jaz_balance_marker')
        ->where('id', $request->id)
        ->where('part_no', $request->part_no)
        ->first();

        $data = new stdClass();
    
        $data->id                       = $saving->id;
        $data->factory_id               = ($saving->factory_id == '1')?'1000001':'1000011';
        $data->marker_release_detail_id = $saving->marker_release_detail_id;
        $data->cutting_date             = $saving->cutting_date;
        $list_po_buyer                  = explode(',',$saving->po_buyer);
        $po_buyer                       = PoBuyer::whereIn('po_buyer', $list_po_buyer)
                                        ->orderBy('promise_date', 'desc')
                                        ->first();

        $data->po_buyer       = $po_buyer->po_buyer;
        $data->lc_date        = $po_buyer->lc_date;
        $data->promise_date   = $po_buyer->promise_date;
        $data->item_code      = $saving->item_code;
        $data->item_code_book = $saving->item_code_book;
        $data->item_id        = $saving->item_id;
        $data->item_id_book   = $saving->item_id_book;
        $data->balance        = $saving->balance;
        $data->remark         = $saving->remark;
        $data->planning_date  = $saving->cutting_date;
        $data->style          = $saving->style;
        $data->articleno      = $saving->articleno;
        return response()->json($data, 200);

    }

    public function getPoSupplierPickList(Request $request)
    {
        $q           = trim(strtoupper($request->q));

        if(auth::user()->warehouse == '1000013' || auth::user()->warehouse == '1000002' )
        {
            $lists      = db::table('po_suppliers_v')
            ->where('document_no','like',"%$q%")
            ->paginate(10);
        }else if(auth::user()->warehouse == '1000011' || auth::user()->warehouse == '1000001' )
        {
            $lists      = db::table('po_supplier_fabric_v')
            ->where('document_no','like',"%$q%")
            ->paginate(10);
        }

        return view('fabric_report_material_balance_marker._document_no_list',compact('lists'));
    }

    function randomCode()
    {
        $referral_code  = '1S'.Carbon::now()->format('u');
        $sequence       = Barcode::where('referral_code',$referral_code)->max('sequence');

        if ($sequence == null)
            $sequence = 1;
        else
            $sequence += 1;

        Barcode::FirstOrCreate([
            'barcode'       => $referral_code.''.$sequence,
            'referral_code' => $referral_code,
            'sequence'      => $sequence,
        ]);
        
        $obj = new stdClass();
        $obj->barcode       = $referral_code.''.$sequence;
        $obj->referral_code = $referral_code;
        $obj->sequence      = $sequence;
        return $obj;
    }

    public function showBarcode(Request $request)
    {
        $id         = trim($request->id,'"');
        $splits_id  =  explode(',',$id);
        $array      = array();
        foreach ($splits_id as $key => $value) {
            $array [] = $value;
        }

        $material_stocks = MaterialStock::whereIn('barcode_supplier',$array)->where('is_material_others',true)->get();

        return view('fabric_report_material_balance_marker.barcode',compact('material_stocks'));
   
    }

    public function confirmStore(Request $request)
    {
        //list datanya 
        $marker_release_detail_id   = $request->marker_release_detail_id;
        $item_id_source   = $request->item_id_source;
        $item_id_book     = $request->item_id_book;
        $document_no      = $request->document_no;
        $item_code        = $request->item_code;
        $item_code_source = $request->item_code_source;
        $planning_date    = $request->planning_date;
        $po_buyer         = $request->po_buyer;
        $warehouse_id     = '1000011';
        $qty_allocation   = $request->qty_allocation;
        $booking_number   = $request->booking_number;
        $remark           = $request->remark;
        $cbpartner_id     = $request->cbpartner_id;
        $item_id_book     = $request->item_id_book;
        $c_bpartner_id    = $request->c_bpartner_id;
        $supplier_name    = $request->supplier_name;
        $lc_date          = $request->lc_date;
        $promise_date     = $request->promise_date;
        $po_supplier      = PoSupplier::where('document_no', $document_no)->first();
        $c_order_id       = $po_supplier->c_order_id;
        $style            = $request->style;
        $article_no       = $request->article_no;
        $qty_allocation   = $request->qty_allocation;
        $lc_date          = $request->lc_date;
        $type_stock       = $request->type_stock;
        $type_stock_erp_code = $request->type_stock_erp_code;
        $so_id               = PoBuyer::where('po_buyer', $po_buyer)->first();

        //insert ke auto alokasi 

        try
        {
        db::beginTransaction();
        
        $movement_date = Carbon::now();

        $auto_allocation = AutoAllocation::FirstOrCreate([
            'document_allocation_number'        => $booking_number,
            //'lc_date'                           => $lc_date,
            'c_order_id'                        => $c_order_id,
            'season'                            => '-',
            'document_no'                       => $document_no,
            'c_bpartner_id'                     => $c_bpartner_id,
            'supplier_name'                     => $supplier_name,
            'po_buyer'                          => $po_buyer,
            'old_po_buyer'                      => $po_buyer,
            'item_code_source'                  => $item_code_source,
            'item_id_source'                    => $item_id_source,
            'item_id_book'                      => $item_id_book,
            'item_code'                         => $item_code,
            'item_code_book'                    => $item_code,
            'article_no'                        => $article_no,
            'item_desc'                         => '-',
            'category'                          => 'FB',
            'uom'                               => 'YDS',
            'warehouse_name'                    => '-',
            'warehouse_id'                      => $warehouse_id,
            'qty_allocation'                    => $qty_allocation,
            'qty_outstanding'                   => 0,
            'qty_allocated'                     => $qty_allocation,
            'is_fabric'                         => true,
            'remark'                            => 'UPLOAD MANUAL'.($remark ? ', '.$remark : null),
            'remark_update'                     => ($remark ? $remark : null),
            'is_already_generate_form_booking'  => false,
            'is_upload_manual'                  => true,
            'is_balance_marker'                 => true,
            'type_stock'                        => $type_stock,
            'type_stock_erp_code'               => $type_stock_erp_code,
            //'promise_date'                      => $promise_date,
            'status_po_buyer'                   => 'active',
            'user_id'                           => auth::user()->id,
            'created_at'                        => $movement_date,
            'updated_at'                        => $movement_date,
            'so_id'                             => $so_id->so_id
        ]);

        $note = 'TERJADI PENAMBAHAN ALOKASI';
        $note_code = '1';

        HistoryAutoAllocations::FirstOrCreate([
            'auto_allocation_id'        => $auto_allocation->id,
            'c_bpartner_id_new'         => $c_bpartner_id,
            'supplier_name_new'         => $supplier_name,
            'document_no_new'           => $document_no,
            'item_id_new'               => $item_id_book,
            'item_code_new'             => $item_code,
            'item_source_id_new'        => $item_id_source,
            'item_code_source_new'      => $item_code_source,
            'po_buyer_new'              => $po_buyer,
            'uom_new'                   => 'YDS',
            'qty_allocated_new'         => $qty_allocation,
            'warehouse_id_new'          => $warehouse_id,
            'type_stock_new'            => $type_stock,
            'type_stock_erp_code_new'   => $type_stock_erp_code,
            'note_code'                 => $note_code,
            'note'                      => $note,
            'operator'                  => $qty_allocation,
            'remark'                    => $remark,
            'is_integrate'              => true,
            'user_id'                   => auth::user()->id
        ]);

        //tambahan ke material preparation fabric

        $is_material_preparation_exists = MaterialPreparationFabric::where([
            ['c_order_id',$c_order_id],
            ['article_no',$article_no],
            ['warehouse_id',$warehouse_id],
            ['item_id_book',$item_id_book],
            ['item_id_source',$item_id_source],
            ['planning_date',$planning_date],
            ['is_piping',false],
            ['_style',$style],
            ['is_balance_marker', true],
            ['total_reserved_qty', $qty_allocation]
        ])
        ->first();

        if(!$is_material_preparation_exists)
        {
            $material_preparation_fabric = MaterialPreparationFabric::firstorcreate([
                'c_bpartner_id'         => $c_bpartner_id,
                'supplier_name'         => $supplier_name,
                'document_no'           => $document_no,
                'po_buyer'              => $po_buyer,    
                'item_code'             => $item_code,
                'item_code_source'      => $item_code_source,
                'c_order_id'            => $c_order_id,
                'item_id_book'          => $item_id_book,
                'item_id_source'        => $item_id_source,
                'uom'                   => 'YDS',
                'total_reserved_qty'    => sprintf('%0.8f',$qty_allocation),
                'total_qty_outstanding' => sprintf('%0.8f',$qty_allocation),
                'planning_date'         => $planning_date,
                'is_piping'             => false,
                'article_no'            => $article_no,
                '_style'                => $style,
                'is_from_additional'    => false,
                'is_balance_marker'     => true,
                'preparation_date'      => null,
                'preparation_by'        => null,
                'warehouse_id'          => $warehouse_id,
                'user_id'               => auth::user()->id,
                'created_at'            => $movement_date,
                'updated_at'            => $movement_date,
                'auto_allocation_id'       => $auto_allocation->id,
            ]);
        }

        //update status confirm di cmds
        $app_env = Config::get('app.env');
            if($app_env == 'live')
            {
                $data_cdms = DB::connection('cdms');
            }
            
            $data               = $data_cdms->table('marker_release_details')
            ->where('id', $marker_release_detail_id)
            ->update(['status' => 'confirm']);

        db::commit();
        }catch (Exception $ex){
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }


    }

    public function confirmSavingStore(Request $request)
    {
        $return_print           = '';
        $marker_release_detail_id   = $request->_marker_release_detail_id;
        $item_id  = $request->_item_id_source;
        $item_id_book     = $request->_item_id_book;
        $c_order_id      = $request->_document_no;
        $item_code        = $request->_item_code;
        $item_code_source = $request->_item_code_source;
        $planning_date    = $request->_planning_date;
        $po_buyer         = $request->_po_buyer;
        $warehouse_id     = '1000011';
        $roll_number      = $request->nomor_roll;
        $remark           = $request->_remark;
        $cbpartner_id     = $request->_cbpartner_id;
        $c_bpartner_id    = $request->_c_bpartner_id;
        $supplier_name    = $request->_supplier_name;
        $lc_date          = $request->_lc_date;
        $promise_date     = $request->_promise_date;
        $c_order_id      = $request->_document_no;
        $po_supplier      = PoSupplier::where('c_order_id', $c_order_id)->first();
        $document_no       = $po_supplier->document_no;
        $supplier_code       = $po_supplier->supplier_code;
        $style            = $request->_style;
        $article_no       = $request->_article_no;
        $qty   = $request->_qty_saving;
        $lc_date          = $request->_lc_date;
        $type_stock       = $request->_type_stock;
        $type_stock_erp_code = $request->_type_stock_erp_code;

        $area_receive_fabric = Locator::whereHas('area',function ($query) use ($warehouse_id)
        {
            $query->where('name','RECEIVING')
            ->where('warehouse',$warehouse_id);
        })
        ->first();

        $approval_date          = carbon::now()->toDateTimeString();
        $list_approval_ids       = array();

                $item_desc                      = '';
                $color                          = '';
                $upc                            = '';
                $category                       = 'FB';
                $uom                            = '';
                $mapping_stock_id               = null;
                
                $source                         = '';
                $batch_number                   = null;
                $actual_width                   = null;
                $actual_lot                     = null;

                $get_barcode                    = $this->randomCode();
                $barcode                        = $get_barcode->barcode;
                $referral_code                  = $get_barcode->referral_code;
                $sequence                       = $get_barcode->sequence;

                try 
                {
                    DB::beginTransaction();
                    
                    $material_stock = MaterialStock::FirstOrCreate([
                        'locator_id'            => $area_receive_fabric->id,
                        'barcode_supplier'      => $barcode,
                        'referral_code'         => $referral_code,
                        'sequence'              => $sequence,
                        'document_no'           => $document_no,
                        'supplier_code'         => $supplier_code,
                        'supplier_name'         => $supplier_name,
                        'c_bpartner_id'         => $c_bpartner_id,
                        'c_order_id'            => $c_order_id,
                        'item_id'               => $item_id,
                        'item_code'             => $item_code,
                        'item_desc'             => $item_desc,
                        'color'                 => $color,
                        'upc_item'              => $upc,
                        'category'              => $category,
                        'type_po'               => 1,
                        'warehouse_id'          => $warehouse_id,
                        'uom'                   => $uom,
                        'qty_carton'            => 1,
                        'stock'                 => $qty,
                        'nomor_roll'            => $roll_number,
                        'batch_number'          => $batch_number,
                        'begin_width'           => $actual_width,
                        'middle_width'          => $actual_width,
                        'end_width'             => $actual_width,
                        'actual_width'          => $actual_width,
                        'load_actual'           => $actual_lot,
                        'reserved_qty'          => 0,
                        'available_qty'         => $qty,
                        'qty_order'             => $qty,
                        'qty_arrival'           => $qty,
                        'is_active'             => true,
                        'is_material_others'    => true,
                        //'approval_date'         => $approval_date,
                        //'approval_user_id'      => Auth::user()->id,
                        'source'                => $source,
                        'user_id'               => Auth::user()->id,
                        'type_stock'            => $type_stock,
                        'mapping_stock_id'      => $mapping_stock_id,
                        'type_stock_erp_code'   => $type_stock_erp_code,
                        'created_at'            => $approval_date,
                        'updated_at'            => $approval_date,

                    ]);

                    DetailMaterialStock::FirstOrCreate([
                        'material_stock_id'     => $material_stock->id,
                        'remark'                => 'DI DAPATKAN SAVNG CDMS',
                        'uom'                   => $material_stock->uom,
                        'qty'                   => $material_stock->available_qty,
                        'user_id'               => $material_stock->user_id,
                        'created_at'            => $material_stock->created_at,
                        'updated_at'            => $material_stock->updated_at,
                    ]);

                    MovementStockHistory::FirstOrCreate([
                        'material_stock_id' => $material_stock->id,
                        'from_location'     => $material_stock->locator_id,
                        'to_destination'    => $material_stock->locator_id,
                        'uom'               => $material_stock->uom,
                        'qty'               => $material_stock->available_qty,
                        'movement_date'     => $material_stock->created_at,
                        'status'            => 'SAVING CDMS',
                        'user_id'           => $material_stock->user_id
                    ]);

                    $history_material_stock_opname = HistoryMaterialStockOpname::create([
                        'material_stock_id'     => $material_stock->id,
                        'locator_old_id'        => $material_stock->locator_id,
                        'locator_new_id'        => $material_stock->locator_id,
                        'available_stock_old'   => '0',
                        'available_stock_new'   => $material_stock->available_qty,
                        'operator'              => $material_stock->available_qty,
                        'source'                => 'new',
                        'note'                  => $material_stock->source,
                        'sto_note'              => $material_stock->source,
                        'sto_date'              => $material_stock->created_at,
                        'user_id'               => $material_stock->user_id,
                    ]);

                    $list_approval_ids [] = $history_material_stock_opname->id;

                    $return_print                   .= $material_stock->barcode_supplier.',';
                    $summary_stock_fabrics []        = $material_stock->id;
                    $counter_in                      = $area_receive_fabric->counter_in;
                    $area_receive_fabric->counter_in = $counter_in + 1;
                    $area_receive_fabric->save();

                    //$this->insertSummaryStockFabric($summary_stock_fabrics);
                    DB::commit();
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

        foreach($list_approval_ids as $key => $list_approval_id)
        {
            HistoryStock::itemApprove($list_approval_id);
        }

        Session::flash('flag', 'success');
        return response()->json(substr($return_print,0,-1),200);

    }

    public function export(Request $request)
    {
        $app_env = Config::get('app.env');
            if($app_env == 'live')
            {
                $data_cdms = DB::connection('cdms');
            }

            $start_date             = ($request->start_date) ? Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d') : Carbon::today()->subDays(7);
            $end_date               = ($request->end_date) ? Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d') : Carbon::today()->addDays(7);

            // $cutting_date       = $request->cutting_date;
            
            $balance_marker = DB::select(db::raw("SELECT * FROM get_balance_marker('$start_date','$end_date')"));

            // $balance_marker               = $data_cdms->table('jaz_balance_marker')
            // ->where('cutting_date', $cutting_date)
            // ->get();

        $file_name = 'balance_marker_planning_from'.$start_date.'to'.$end_date;
        return Excel::create($file_name,function($excel) use ($balance_marker){
            $excel->sheet('active',function($sheet)use($balance_marker)
            {
                $sheet->setCellValue('A1','MARKER RELEASE ID');
                $sheet->setCellValue('B1','WAHEHOUSE');
                $sheet->setCellValue('C1','PLANNING DATE');
                $sheet->setCellValue('D1','STYLE');
                $sheet->setCellValue('E1','ARTICLE NO');
                $sheet->setCellValue('F1','PO BUYER');
                $sheet->setCellValue('G1','ITEM CODE BOOK');
                $sheet->setCellValue('H1','ITEM CODE SOURCE');
                $sheet->setCellValue('I1','BALANCE');
                $sheet->setCellValue('J1','STATUS');
                $sheet->setCellValue('K1','REMARK');
            
            $row=2;
            foreach ($balance_marker as $i) 
            {  
                $sheet->setCellValue('A'.$row,$i->marker_release_detail_id);
                if($i->factory_id == '1')
                {
                    $warehouse_name     = 'AOI 1';
                }
                else
                {
                    $warehouse_name     = 'AOI 2';
                }
                $sheet->setCellValue('B'.$row,$warehouse_name);
                $sheet->setCellValue('C'.$row,$i->cutting_date);
                $sheet->setCellValue('D'.$row,$i->style);
                $sheet->setCellValue('E'.$row,$i->articleno);
                $sheet->setCellValue('F'.$row,$i->po_buyer);
                $sheet->setCellValue('G'.$row,$i->item_code_book);
                $sheet->setCellValue('H'.$row,$i->item_code);
                $sheet->setCellValue('I'.$row,$i->balance);
                if($i->status == 'confirm')
                {
                    $name = 'CONFIRMED BY MM';
                }
                else
                {
                    $name ='';   
                }
                $sheet->setCellValue('J'.$row,$name);
                $sheet->setCellValue('K'.$row,$i->remark);
                $row++;
            }
            });

        })
        ->export('xlsx');
    }

    public function createAllocation(Request $request)
    {
        return view('fabric_report_material_balance_marker.create_allocation');
    }

    public function downloadCreateAllocation()
    {
        return Excel::create('upload_alokasi_balance_marker',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','CDMS_ID');
                $sheet->setCellValue('B1','PLANNING_DATE');
                $sheet->setCellValue('C1','DOCUMENT_NO');
                $sheet->setCellValue('D1','PO_BUYER');
                $sheet->setCellValue('E1','STYLE');
                $sheet->setCellValue('F1','ARTICLE_NO');
                $sheet->setCellValue('G1','ITEM_CODE_SOURCE');
                $sheet->setCellValue('H1','ITEM_CODE_BOOK');
                $sheet->setCellValue('I1','QTY');
                $sheet->setCellValue('J1','REMARK');
                $sheet->setCellValue('H1','BOOKING NUMBER');
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                    'C' => '@'
                ));
            });
            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }

    public function uploadCreateAllocation(Request $request)
    {
        $array = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                //dd($data);
                foreach ($data as $key => $value) 
                {
                    $marker_release_detail_id = $value->marker_release_detail_id;
                    $document_no              = $value->document_no;
                    $item_code                = $value->item_code_source;//kalau mau d lock pakai book form upload tambahin item code book
                    $item_code_source         = $value->item_code_source;
                    $_item_id_source          = Item::where('item_code', $item_code_source)->first();
                    $_item_id_book            = Item::where('item_code', $item_code)->first();
                    $item_id_source           = $_item_id_source->item_id;
                    $item_id_book             = $_item_id_book->item_id;
                    $planning_date            = $value->planning_date;
                    $po_buyer                 = $value->po_buyer;
                    $warehouse_id             = '1000011';
                    $booking_number           = $value->booking_number;
                    $remark                   = $value->remark;
                    $po_supplier              = PoSupplier::where('document_no', $document_no)->first();
                    $c_bpartner_id             = $po_supplier->c_bpartner_id;
                    $c_order_id               = $po_supplier->c_order_id;
                    $supplier_name            = $po_supplier->supplier_name;
                    $style                    = $value->style;
                    $article_no               = $value->article_no;
                    $qty_allocation           = $value->qty;
                    $data_po_buyer            = PoBuyer::where('po_buyer', $po_buyer)->first();
                    $lc_date                  = $data_po_buyer->lc_date;
                    $promise_date             = $data_po_buyer->promise_date;
                    $type_stock               = 'REGULER';
                    $type_stock_erp_code      = '2';
                    $so_id                    = PoBuyer::where('po_buyer', $po_buyer)->first();

                    try
                    {
                    db::beginTransaction();
                    
                    $movement_date = Carbon::now();
            
                    $auto_allocation = AutoAllocation::FirstOrCreate([
                        'document_allocation_number'        => $booking_number,
                        //'lc_date'                           => $lc_date,
                        'c_order_id'                        => $c_order_id,
                        'season'                            => '-',
                        'document_no'                       => $document_no,
                        'c_bpartner_id'                     => $c_bpartner_id,
                        'supplier_name'                     => $supplier_name,
                        'po_buyer'                          => $po_buyer,
                        'old_po_buyer'                      => $po_buyer,
                        'item_code_source'                  => $item_code_source,
                        'item_id_source'                    => $item_id_source,
                        'item_id_book'                      => $item_id_book,
                        'item_code'                         => $item_code,
                        'item_code_book'                    => $item_code,
                        'article_no'                        => $article_no,
                        'item_desc'                         => '-',
                        'category'                          => 'FB',
                        'uom'                               => 'YDS',
                        'warehouse_name'                    => '-',
                        'warehouse_id'                      => $warehouse_id,
                        'qty_allocation'                    => $qty_allocation,
                        'qty_outstanding'                   => 0,
                        'qty_allocated'                     => $qty_allocation,
                        'is_fabric'                         => true,
                        'remark'                            => 'UPLOAD MANUAL'.($remark ? ', '.$remark : null),
                        'remark_update'                     => ($remark ? $remark : null),
                        'is_already_generate_form_booking'  => false,
                        'is_upload_manual'                  => true,
                        'is_balance_marker'                 => true,
                        'type_stock'                        => $type_stock,
                        'type_stock_erp_code'               => $type_stock_erp_code,
                        //'promise_date'                      => $promise_date,
                        'status_po_buyer'                   => 'active',
                        'user_id'                           => auth::user()->id,
                        'created_at'                        => $movement_date,
                        'updated_at'                        => $movement_date,
                        'so_id'                             => $so_id->so_id
                    ]);
            
                    $note = 'TERJADI PENAMBAHAN ALOKASI';
                    $note_code = '1';
            
                    HistoryAutoAllocations::FirstOrCreate([
                        'auto_allocation_id'        => $auto_allocation->id,
                        'c_bpartner_id_new'         => $c_bpartner_id,
                        'supplier_name_new'         => $supplier_name,
                        'document_no_new'           => $document_no,
                        'item_id_new'               => $item_id_book,
                        'item_code_new'             => $item_code,
                        'item_source_id_new'        => $item_id_source,
                        'item_code_source_new'      => $item_code_source,
                        'po_buyer_new'              => $po_buyer,
                        'uom_new'                   => 'YDS',
                        'qty_allocated_new'         => $qty_allocation,
                        'warehouse_id_new'          => $warehouse_id,
                        'type_stock_new'            => $type_stock,
                        'type_stock_erp_code_new'   => $type_stock_erp_code,
                        'note_code'                 => $note_code,
                        'note'                      => $note,
                        'operator'                  => $qty_allocation,
                        'remark'                    => $remark,
                        'is_integrate'              => true,
                        'user_id'                   => auth::user()->id
                    ]);
            
                    //tambahan ke material preparation fabric
            
                    $is_material_preparation_exists = MaterialPreparationFabric::where([
                        ['c_order_id',$c_order_id],
                        ['article_no',$article_no],
                        ['warehouse_id',$warehouse_id],
                        ['item_id_book',$item_id_book],
                        ['item_id_source',$item_id_source],
                        ['planning_date',$planning_date],
                        ['is_piping',false],
                        ['_style',$style],
                    ])
                    ->first();
            
                    if(!$is_material_preparation_exists)
                    {
                        $material_preparation_fabric = MaterialPreparationFabric::firstorcreate([
                            'auto_allocation_id'    => $auto_allocation->id,
                            'c_bpartner_id'         => $c_bpartner_id,
                            'supplier_name'         => $supplier_name,
                            'document_no'           => $document_no,
                            'po_buyer'              => $po_buyer,    
                            'item_code'             => $item_code,
                            'item_code_source'      => $item_code_source,
                            'c_order_id'            => $c_order_id,
                            'item_id_book'          => $item_id_book,
                            'item_id_source'        => $item_id_source,
                            'uom'                   => 'YDS',
                            'total_reserved_qty'    => sprintf('%0.8f',$qty_allocation),
                            'total_qty_outstanding' => sprintf('%0.8f',$qty_allocation),
                            'planning_date'         => $planning_date,
                            'is_piping'             => false,
                            'article_no'            => $article_no,
                            '_style'                => $style,
                            'is_from_additional'    => false,
                            'is_balance_marker'     => true,
                            'preparation_date'      => null,
                            'preparation_by'        => null,
                            'warehouse_id'          => $warehouse_id,
                            'user_id'               => auth::user()->id,
                            'created_at'            => $movement_date,
                            'updated_at'            => $movement_date
                        ]);
                    }
            
                    //update status confirm di cmds
                    $app_env = Config::get('app.env');
                        if($app_env == 'live')
                        {
                            $data_cdms = DB::connection('cdms');
                        }
                        
                        $data               = $data_cdms->table('marker_release_details')
                        ->where('id', $marker_release_detail_id)
                        ->update(['status' => 'confirm']);
            
                    db::commit();
                    }catch (Exception $ex){
                        db::rollback();
                        $message = $ex->getMessage();
                        ErrorHandler::db($message);
                    }
            
                }
            }
        }

    }

    public function getPoSupplierSaving(Request $request)
    {
        $planning_date = $request->planning_date;
        $style         = $request->style;
        $article_no    = $request->article_no;
        $warehouse_id = $request->warehouse_id;
        
        $list_document_no = MaterialPreparationFabric::where([
            ['warehouse_id', $warehouse_id],
            ['_style', $style],
            ['article_no', $article_no],
            ['planning_date', $planning_date],
        ])->pluck('document_no', 'c_order_id')->all();

        return response()->json([
            'list_document_no'    => $list_document_no 
        ],200);
    }

    public function getNomorRollSaving(Request $request)
    {
        $planning_date = $request->planning_date;
        $style         = $request->style;
        $article_no    = $request->article_no;
        $warehouse_id = $request->warehouse_id;
        $c_order_id  = $request->c_order_id;
        
        $material_preparation_id = MaterialPreparationFabric::where([
            ['warehouse_id', $warehouse_id],
            ['_style', $style],
            ['article_no', $article_no],
            ['planning_date', $planning_date],
            ['c_order_id', $c_order_id]
        ])->pluck('id')->all();

        $list_nomor_roll = MaterialStock::whereIn('id', function($query) use($material_preparation_id)
        {
            $query->select('material_stock_id')
            ->from('detail_material_preparation_fabrics')
            ->whereIn(
            'material_preparation_fabric_id', $material_preparation_id
            );
        })
        ->pluck('nomor_roll')->all();

        return response()->json([
            'list_nomor_roll'    => $list_nomor_roll 
        ],200);
        
    }
}
