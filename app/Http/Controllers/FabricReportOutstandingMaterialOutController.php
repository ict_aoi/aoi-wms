<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;

use App\Models\Item;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class FabricReportOutstandingMaterialOutController extends Controller
{
    public function index()
    {
        return view('fabric_report_outstanding_material_out.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {

            $planning_date       = $request->planning_date;
            $is_additional       = $request->is_additional;
            if($is_additional == 'reguler')
            {
                $is_additional = false;
            }
            else
            {
                $is_additional = true;
            }
            $warehouse_id           = Auth::user()->warehouse;

            if(!$is_additional)
            {
                $data               = db::table('outstanding_scan_out_fabric_v')
                ->where('planning_date', '>=', $planning_date)
                ->where('warehouse_id', $warehouse_id)
                ->where('is_additional', false)
                ->get();

            }
            else
            {
                $data               = db::table('outstanding_scan_out_fabric_v')
                ->where('created_at', '>=', $planning_date)
                ->where('warehouse_id', $warehouse_id)
                ->where('is_additional', true)
                ->get();

            }

            return DataTables::of($data)
            ->editColumn('warehouse_id',function ($data)
            {
                if( $data->warehouse_id == '1000001') return 'Warehouse Fabric Aoi 1';
                else if ( $data->warehouse_id == '1000011') return 'Warehouse Fabric Aoi 2';
                else return '-';
            })
            ->make(true);
        }
    }

    public function export(Request $request)
    {
        $planning_date       = ( $request->planning_date ? Carbon::createFromFormat('d/m/Y', $request->planning_date)->format('Y-m-d') : null);;
        $warehouse_id           = Auth::user()->warehouse;
        $is_additional       = $request->is_additional;
        if($is_additional == 'reguler')
            {
                $is_additional = false;
            }
            else
            {
                $is_additional = true;
            }

        if(!$is_additional)
        {
            $data               = db::table('outstanding_scan_out_fabric_v')
            ->where('planning_date', '>=', $planning_date)
            ->where('warehouse_id', $warehouse_id)
            ->where('is_additional', false)
            ->get();

        }
        else
        {
            $data               = db::table('outstanding_scan_out_fabric_v')
            ->where('created_at', '>=', $planning_date)
            ->where('warehouse_id', $warehouse_id)
            ->where('is_additional', true)
            ->get();

        }


        $file_name = 'outstanding_material_out_dari_planning_'.$planning_date;
        return Excel::create($file_name,function($excel) use ($data){
            $excel->sheet('active',function($sheet)use($data)
            {
                $sheet->setCellValue('A1','TANGGAL PLANNING');
                $sheet->setCellValue('B1','WAREHOUSE');
                $sheet->setCellValue('C1','IS ADDITIONAL');
                $sheet->setCellValue('D1','STYLE');
                $sheet->setCellValue('E1','ARTICLE NO');
                $sheet->setCellValue('F1','DOCUMENT NO');
                $sheet->setCellValue('G1','BARCODE');
                $sheet->setCellValue('H1','ITEM CODE');
                $sheet->setCellValue('I1','ITEM CODE SOURCE');
                $sheet->setCellValue('J1','UOM');
                $sheet->setCellValue('K1','QTY PREPARE');
                $sheet->setCellValue('L1','PREPARATION DATE');
                $sheet->setCellValue('M1','IS PIPING');

            
            $row=2;
            foreach ($data as $i) 
            {  
                $sheet->setCellValue('A'.$row,$i->planning_date);
                if( $i->warehouse_id == '1000001')  $warehouse ='Warehouse Fabric Aoi 1';
                else if ( $i->warehouse_id == '1000011') $warehouse ='Warehouse Fabric Aoi 2';
                $sheet->setCellValue('B'.$row,$warehouse);
                $sheet->setCellValue('C'.$row,$i->is_additional);
                $sheet->setCellValue('D'.$row,$i->style);
                $sheet->setCellValue('E'.$row,$i->article_no);
                $sheet->setCellValue('F'.$row,$i->document_no);
                $sheet->setCellValue('G'.$row,$i->barcode);
                $sheet->setCellValue('H'.$row,$i->item_code);
                $sheet->setCellValue('I'.$row,$i->item_code_source);
                $sheet->setCellValue('J'.$row,$i->uom);
                $sheet->setCellValue('K'.$row,$i->qty_prepare);
                $sheet->setCellValue('L'.$row,$i->preparation_date);
                $sheet->setCellValue('M'.$row,$i->is_piping);
                $row++;
            }
            });

        })
        ->export('xlsx');

    }
}
