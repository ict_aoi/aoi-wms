<?php

use \App\Http\I18lFilter;
use Carbon\Carbon;

if (!function_exists('i18l_date'))
{
	function i18l_date($value, $pattern = array(), $locale = null, $timezone = null) {
		return I18lFilter::date($value, $pattern, $locale, $timezone);
	}
}

if (!function_exists('i18l_currency'))
{
	function i18l_currency($value, $locale = null) {
		$a = str_replace("IDR", "IDR ", I18lFilter::currency($value, $locale));
		$a = str_replace("SGD", "SGD ", $a);
		return $a;
	}
}

if (!function_exists('i18l_number'))
{
	function i18l_number($value, $pattern = null, $locale = null) {
		return I18lFilter::number($value, $pattern, $locale);
	}
}

if (!function_exists('cart'))
{
	function cart() {
		return App\Http\Controllers\CartController::getCart();
	}
}

if (!function_exists('action_cached'))
{
	function action_cached($timestamp, $action, $param) {
		$url = action($action, $param);

		$query = parse_url($url, PHP_URL_QUERY);

		if ($query) {
		    $url .= '&_cached=';
		} else {
		    $url .= '?_cached=';
		}

		return $url . $timestamp->format('YmdHi');
	}
}

if (!function_exists('blogs_footer'))
{
	function blogs_footer() {
		$blogs = Blog::where('publish_state', 'PUB')
				->orderBy('publish_at', 'desc')
				->get()->take(2);

		return $blogs;
	}
}

if (!function_exists('blogs_date'))
{
	function blogs_date($str,$opt) {
		$day = Carbon::createFromFormat('Y-m-d H:i:s', $str)->day;
		$monthNum = Carbon::createFromFormat('Y-m-d H:i:s', $str)->month;
		$dateObj   = DateTime::createFromFormat('!m', $monthNum);
		$monthName = $dateObj->format('F');
		$monthName = strtoupper(substr($monthName, 0, 3));
		if($opt == 'day'){
			return $day;
		}else if($opt == 'month'){
			return $monthName;
		}else{
			return '';
		}
	}
}

if (!function_exists('blogs_content_footer'))
{
	function blogs_content_footer($str) {
		$cleaned = clean($str);
		$rest = substr($cleaned, 0, 55);
		$pos = strrpos($rest, ' ');
		$rest = substr($rest, 0, $pos);

		return $rest;
	}
}

if (!function_exists('blogs_content'))
{
	function blogs_content($str) {
	    if (strlen($str) > 512) {
            $rest = substr($str, 0, 512);
            $pos1 = strrpos($rest, '. ');
            $rest = substr($rest, 0, $pos1+1);
        }
		else {
            $rest = $str;
        }

		return $rest;
	}
}

if (!function_exists('blogs_content_home'))
{
	function blogs_content_home($str) {
	    if (strlen($str) > 350) {
            $rest = substr($str, 0, 350);
            $pos1 = strrpos($rest, '. ');
            $rest = substr($rest, 0, $pos1+1);
        }
		else {
            $rest = $str;
        }

		return $rest;
	}
}

if (!function_exists('substr_act_msg'))
{
	function substr_act_msg($str) {
		$rest = substr($str, 0, 40);
		$pos1 = strrpos($rest, ' ');
		$rest = substr($rest, 0, $pos1);

		return $rest;
	}
}

if (!function_exists('randomCode'))
{
	function randomCode($panjang=null)
	{
		if($panjang == null)
			$panjang = 5;

		$karakter= 'abcdefghijklmnopqrstuvwxyz1234567890';
		$string = '';
		for ($i = 0; $i < $panjang; $i++) {
			$pos = rand(0, strlen($karakter)-1);
			$string .= $karakter{$pos};
		}
		return $string;
	}
}

if (!function_exists('segment')) {
	function segment($id=null)
	{
		$segment_uri = explode('/',$_SERVER['REQUEST_URI']);
		if ($segment_uri[0] == "" && $segment_uri[1] == "") {
			return false;
		} else {
			return $segment_uri[$id];
		}
	}
}
