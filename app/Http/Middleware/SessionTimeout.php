<?php namespace App\Http\Middleware;

use Auth;
use Closure;
use Carbon\Carbon;
use Illuminate\Session\Store;

use App\Models\User;

class SessionTimeout
{
    protected $session;
    protected $timeout=86400;
    public function __construct(Store $session){
        $this->session=$session;
    }
    
    public function handle($request, Closure $next)
    {
        if(!$this->session->has('lastActivityTime'))
            $this->session->put('lastActivityTime',time());
        elseif(time() - $this->session->get('lastActivityTime') > $this->getTimeOut()){
            $this->session->forget('lastActivityTime');
            $user = Auth::user();
            $user->session_id = null;
            $user->last_logout = carbon::now();
            $user->save();
            Auth::logout();
            
            return redirect('/login')->withErrors(['Selama 30 Menit kamu diem2 bae, lagi ngopi ya?. login ulang sana']);
        }
        $this->session->put('lastActivityTime',time());
        return $next($request);
    }

    protected function getTimeOut()
    {
        return (env('TIMEOUT')) ?: $this->timeout;
    }
}
